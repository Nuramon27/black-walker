//! Contains a collection of Strings intended to be presented
//! to the user.

use chrono::Weekday;

/// Returns the short german name of `weekday`.
pub fn weekday_short_german(weekday: Weekday) -> &'static str {
    match weekday {
        Weekday::Mon => "Mo",
        Weekday::Tue => "Di",
        Weekday::Wed => "Mi",
        Weekday::Thu => "Do",
        Weekday::Fri => "Fr",
        Weekday::Sat => "Sa",
        Weekday::Sun => "So",
    }
}

/// Returns the long german name of `weekday`.
pub fn weekday_long_german(weekday: Weekday) -> &'static str {
    match weekday {
        Weekday::Mon => "Montag",
        Weekday::Tue => "Dienstag",
        Weekday::Wed => "Mittwoch",
        Weekday::Thu => "Donnerstag",
        Weekday::Fri => "Freitag",
        Weekday::Sat => "Samstag",
        Weekday::Sun => "Sonntag",
    }
}

/// Returns the short german name of `month`.
///
/// # Panics
///
/// Panics if `month` is out of range, i.e. not in the range `1..=12`.
pub fn monthname_short_german(month: u32) -> &'static str {
    match month {
        1 => "Jan.",
        2 => "Feb.",
        3 => "März",
        4 => "Apr.",
        5 => "Mai",
        6 => "Juni",
        7 => "Juli",
        8 => "Aug.",
        9 => "Sept.",
        10 => "Okt.",
        11 => "Nov.",
        12 => "Dez.",
        _ => panic!("Month out of range (1..=12)"),
    }
}
