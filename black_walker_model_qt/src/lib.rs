#![allow(clippy::or_fun_call)]
#![allow(clippy::unit_arg)]
#![allow(clippy::clone_on_copy)]
#![allow(clippy::match_bool)]
#![allow(clippy::zero_prefixed_literal)]
#![allow(clippy::single_match)]
#![deny(clippy::dbg_macro)]
// Removed as soon as we have proper error handling
#![allow(clippy::match_wild_err_arm)]
#![deny(broken_intra_doc_links)]

#[allow(unused_parens)]
#[allow(clippy::missing_safety_doc)]
#[allow(clippy::wrong_self_convention)]
#[allow(clippy::unused_unit)]
/// The functions necessary on the ffi-boundary.
pub mod interface;
#[macro_use]
pub mod model;
mod background;
mod macro_collection;
mod prelude;
mod strings;
mod feature;