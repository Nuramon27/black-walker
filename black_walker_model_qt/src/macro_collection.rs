//! Macros that facilitate frequent tasks

/// Implements the trait [`BeginEndSignal`](crate::model::BeginEndSignal)
/// for the emitter `$elem` by calling its methods `begin_cmd_changed`
/// and `end_cmd_changed` respectively.
///
/// ## Usage
///
/// `impl_begin_end_signals{emitter}`
#[macro_export]
macro_rules! impl_begin_end_signals {
    ($elem:ty) => {
        impl $crate::model::BeginEndSignal for $elem {
            fn signal_begin_cmd(&mut self) {
                self.begin_cmd_changed()
            }
            fn signal_end_cmd(&mut self) {
                self.end_cmd_changed()
            }
        }
    };
}

/// Applies `$func` to the `$field` of the [`Diary`](black_walker_logic::Diary), which is obtained
/// from `$contact`.
///
/// The variant with five fields does even more: It first uses
/// `$getter_func` with the argument `$getter_spec` in order to obtain
/// a part specified by `$getter_spec`, and then applies `$func`
/// to this part (if possible -- `$getter_spec` is expected to return
/// an `Option` or a `Result`).
///
/// # Example
///
/// Using this macro, the following
/// ```
/// use black_walker_logic::prelude::*;
/// use black_walker_model_qt::apply;
///
/// # struct Contact {
/// #     diary: Diary
/// # }
/// #
/// let mut cnt = Some(Contact { diary: Diary::new() });
///
/// assert_eq!(
///     apply!(cnt, TagForest; TagForest::get, "Personen"; |tree| Some(tree.len())),
///     Some(1)
/// );
/// ```
///
/// is equivalent to the verbose code
///
/// ```
/// use black_walker_logic::prelude::*;
/// use black_walker_model_qt::apply;
///
/// # struct Contact {
/// #     diary: Diary
/// # }
/// #
/// let mut cnt = Some(Contact { diary: Diary::new() });
///
/// let tagtree = cnt.as_mut()
///     .unwrap()
///     .diary
///     .tag
///     .read()
///     .unwrap();
/// assert_eq!(
///     tagtree.get("Personen").map(TagTree::len),
///     Some(1)
/// );
#[macro_export]
macro_rules! apply {
    ($contact:expr, $field:ty; $func:expr) => {{
        let mut diary = &mut $contact.as_mut()
            .unwrap()
            .diary;
        let part = black_walker_logic::read_unstaged!(diary, $field).unwrap();
        $func(&part)
    }};
    ($contact:expr, $field:ty; $getter_func:expr, $getter_spec:expr; $func:expr) => {
        $crate::apply!($contact, $field; |p| $getter_func(p, $getter_spec).and_then($func))
    }
}
