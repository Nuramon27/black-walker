use std::sync::mpsc::Receiver;
use std::sync::Arc;

use black_walker_logic::model::Emitter;
use black_walker_logic::operate::search::Search;
use black_walker_logic::DreamID;

use super::dream_list::DreamListQt;
use super::Contact;
use super::Pipelines;
use crate::interface::*;
use crate::{impl_begin_end_signals, query};

impl_begin_end_signals! {SearchQtEmitter}

impl Emitter for SearchQtEmitter {
    fn emit(&mut self) {
        self.wakeup_changed()
    }
}

pub struct SearchQt {
    emit: SearchQtEmitter,
    dream_list: DreamListQt,
    search_rec: Option<Receiver<Vec<DreamID>>>,
    search_further: Arc<()>,
    cnt: Option<Contact<SearchQtEmitter>>,
}

impl SearchQt {
    pub fn set_pipe(&mut self, pipe: Pipelines) {
        self.cnt = Some(Contact::from_pipe(self.emit.clone(), pipe.clone()));
        self.dream_list.set_pipe(pipe);
    }
}

impl SearchQtTrait for SearchQt {
    fn new(emit: SearchQtEmitter, dream_list: DreamListQt) -> Self {
        SearchQt {
            emit,
            dream_list,
            search_rec: None,
            search_further: Arc::new(()),
            cnt: None,
        }
    }
    fn emit(&mut self) -> &mut SearchQtEmitter {
        &mut self.emit
    }
    fn dream_list(&self) -> &DreamListQt {
        &self.dream_list
    }
    fn dream_list_mut(&mut self) -> &mut DreamListQt {
        &mut self.dream_list
    }

    fn search(&mut self, condition: String) -> () {
        let (query, rec, further) = Search::new(condition);
        query!(self.cnt, self.emit.clone(), query);

        self.search_rec = Some(rec);
        self.search_further = further;
    }
    fn await_results(&mut self) -> () {
        if let Some(rec) = self.search_rec.take() {
            if let Ok(result) = rec.recv() {
                self.dream_list.set_ids(result);
            }
        }
    }

    #[doc(hidden)]
    fn begin_cmd(&self) -> bool {
        true
    }
    #[doc(hidden)]
    fn end_cmd(&self) -> bool {
        true
    }
    #[doc(hidden)]
    fn wakeup(&self) -> bool {
        true
    }
}
