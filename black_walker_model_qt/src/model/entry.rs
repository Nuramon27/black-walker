//! Contains the models controlling the [`EntrySystem`](black_walker_logic::EntrySystem).

use std::cmp::Ordering;
use std::sync::RwLock;

use black_walker_logic::prelude::*;

use super::{Contact, Pipelines};
use crate::impl_begin_end_signals;

use crate::interface::*;

impl_begin_end_signals! {EntryMasterEmitter}
impl_begin_end_signals! {EntryQtEmitter}

/// The `EntryMaster` governs the [`EntryQt`]s for the various entry
/// categories.
///
/// Currently there are only two entry categories: "Art" and "Technik".
pub struct EntryMaster {
    emit: EntryMasterEmitter,
    cnt: Option<Contact<EntryMasterEmitter>>,
    kind_system: EntryQt,
    technique_system: EntryQt,
}

impl EntryMaster {
    pub fn set_contact(&mut self, pipe: Pipelines) {
        self.kind_system.set_contact(pipe.clone());
        self.technique_system.set_contact(pipe.clone());
        self.cnt = Some(Contact::from_pipe(self.emit.clone(), pipe));
    }
    /// Evaluates `func` on the [`EntrySystem`](black_walker_logic::EntrySystem).
    fn query<F: FnOnce(&EntrySystem) -> T, T>(&self, func: F) -> T {
        let set = RwLock::read(
            &self
                .cnt
                .as_ref()
                .expect("Contact has not been set")
                .diary
                .entr,
        )
        .expect("Worker thread has hung up");
        func(&set)
    }
    fn query_settings<F: FnOnce(&GS) -> T, T>(&self, func: F) -> T {
        let set = RwLock::read(
            &self
                .cnt
                .as_ref()
                .expect("Contact has not been set")
                .diary
                .gl,
        )
        .expect("Worker thread has hung up");
        func(&set)
    }
}

impl EntryModel for EntryMaster {
    fn begin_reset(&mut self) {
        self.kind_system.begin_reset();
        self.technique_system.begin_reset();
    }
    fn end_reset(&mut self) {
        self.kind_system.end_reset();
        self.technique_system.end_reset();
    }
    fn text_added(&mut self, category: &str, entry: String) {
        self.kind_system.text_added(category, entry.clone());
        self.technique_system.text_added(category, entry);
    }
}

impl EntryMasterTrait for EntryMaster {
    fn new(
        emit: EntryMasterEmitter,
        mut kind_system: EntryQt,
        mut technique_system: EntryQt,
    ) -> Self {
        kind_system.category = "Art".to_string();
        technique_system.category = "Technik".to_string();
        EntryMaster {
            emit,
            cnt: None,
            kind_system,
            technique_system,
        }
    }
    fn emit(&mut self) -> &mut EntryMasterEmitter {
        &mut self.emit
    }
    #[doc(hidden)]
    fn begin_cmd(&self) -> bool {
        true
    }
    #[doc(hidden)]
    fn end_cmd(&self) -> bool {
        true
    }
    fn kind_system(&self) -> &EntryQt {
        &self.kind_system
    }
    fn kind_system_mut(&mut self) -> &mut EntryQt {
        &mut self.kind_system
    }
    fn technique_system(&self) -> &EntryQt {
        &self.technique_system
    }
    fn technique_system_mut(&mut self) -> &mut EntryQt {
        &mut self.technique_system
    }
    /// Get the start of the range `category`.
    fn range_u_start(&self, category: String) -> i32 {
        self.query(|set| set.range_u(&category).map(|r| r.0))
            .unwrap_or_else(|| self.query_settings(|gl| gl.slider_range_default.start))
    }
    /// Get the end of the range `category`.
    fn range_u_end(&self, category: String) -> i32 {
        self.query(|set| set.range_u(&category).map(|r| r.1))
            .unwrap_or_else(|| self.query_settings(|gl| gl.slider_range_default.end))
    }
}

/// A model that represents one category of Textual Entries.
pub struct EntryQt {
    emit: EntryQtEmitter,
    model: EntryQtList,
    cnt: Option<Contact<EntryQtEmitter>>,
    /// The category which is represented by this `EntryQt`.
    category: String,
    /// The list of entries in this category.
    list: Vec<String>,
}

impl EntryQt {
    pub fn set_contact(&mut self, pipe: Pipelines) {
        self.cnt = Some(Contact::from_pipe(self.emit.clone(), pipe));
        self.initialize();
    }
    fn initialize(&mut self) {
        self.model.begin_reset_model();
        let entrytown = self.cnt.as_ref().unwrap().diary.entr.read().unwrap();
        self.list.clear();
        self.list.shrink_to_fit();
        if let Some(entryset) = entrytown.texts(&self.category) {
            for entry in entryset {
                self.list.push(entry.to_string());
            }
        }
        self.list.sort_by(|lhs, rhs| cmp_coll(lhs, rhs));
        self.model.end_reset_model();
    }
    /// Evaluates `func` on the [`EntrySystem`](black_walker_logic::EntrySystem).
    pub fn query<F: FnOnce(&EntrySystem) -> T, T>(&self, func: F) -> T {
        let set = RwLock::read(
            &self
                .cnt
                .as_ref()
                .expect("Contact has not been set")
                .diary
                .entr,
        )
        .expect("Worker thread has hung up");
        func(&set)
    }
}

impl EntryModel for EntryQt {
    fn begin_reset(&mut self) {}
    fn end_reset(&mut self) {
        self.initialize();
    }
    fn text_added(&mut self, category: &str, entry: String) {
        if self.category == category {
            let index = self
                .list
                .iter()
                .enumerate()
                .find(|(_, elem)| cmp_coll(*elem, &entry) == Ordering::Greater)
                .map(|(i, _)| i)
                .unwrap_or(self.list.len());
            self.model.begin_insert_rows(index, index);
            self.list.insert(index, entry);
            self.model.end_insert_rows();
        }
    }
}

impl EntryQtTrait for EntryQt {
    fn new(emit: EntryQtEmitter, model: EntryQtList) -> EntryQt {
        EntryQt {
            emit,
            model,
            cnt: None,
            category: String::new(),
            list: Vec::new(),
        }
    }
    fn emit(&mut self) -> &mut EntryQtEmitter {
        &mut self.emit
    }
    fn row_count(&self) -> usize {
        self.list.len()
    }
    /// Return the entry at `index`.
    fn value(&self, index: usize) -> &str {
        self.list.get(index).map(String::as_str).unwrap_or_default()
    }

    #[doc(hidden)]
    fn begin_cmd(&self) -> bool {
        true
    }
    #[doc(hidden)]
    fn end_cmd(&self) -> bool {
        true
    }
}
