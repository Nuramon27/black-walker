use std::convert::TryFrom;
use std::ops::Range;
use std::sync::mpsc;
use std::sync::mpsc::Receiver;
use std::sync::Arc;

use unicode_segmentation::UnicodeSegmentation;

use black_walker_logic::model::{Emitter, Query};
use black_walker_logic::operate::suggestion::{
    Allowed, SearchEntries, SearchEntryCategories, SearchResult, SearchTagCategories, SearchTags,
};
use black_walker_logic::parse::{
    suggestion::SuggestionQuery, suggestion_fragment, EnterType, EntityType,
};
use black_walker_logic::settings;

use super::master::Master;
use super::{BeginEndSignal, Contact};
use crate::interface::*;

impl BeginEndSignal for SuggestionQtEmitter {
    fn signal_begin_cmd(&mut self) {
        self.wakeup_changed()
    }
    fn signal_end_cmd(&mut self) {
        self.wakeup_changed()
    }
}

impl Emitter for SuggestionQtEmitter {
    fn emit(&mut self) {
        self.wakeup_changed()
    }
}

#[derive(Debug, Clone, Default, PartialEq, Eq, Hash)]
struct LineEditState {
    cursor: usize,
    text: String,
}

impl LineEditState {
    pub fn new(cursor: usize, text: String) -> LineEditState {
        LineEditState { cursor, text }
    }
}

pub struct SuggestionQt {
    emit: SuggestionQtEmitter,
    model: SuggestionQtList,
    category: Option<String>,
    to_suggest: EntityType,
    enter_type: EnterType,
    allowed: Allowed,
    state: LineEditState,
    fragment_range: Range<usize>,
    suggestions: Vec<String>,
    sugg_rec: Receiver<SearchResult>,
    search_further: Arc<()>,
    cnt: Option<Contact<SuggestionQtEmitter>>,
}

impl SuggestionQt {
    fn settings(&self) -> settings::Suggestion {
        match self.cnt.as_ref().map(|cnt| cnt.diary.gl.read()) {
            Some(Ok(gl)) => gl.suggestion,
            _ => settings::Suggestion::default(),
        }
    }
    /// Convenience function which sends search `query` down to the worker,
    /// stores `rec` as the Receiver for results and `further` as the
    /// connection.
    fn send_search<C: Query + 'static>(
        &mut self,
        (query, rec, further): (C, Receiver<SearchResult>, Arc<()>),
    ) {
        self.sugg_rec = rec;
        self.search_further = further;
        self.cnt.as_ref().unwrap().query(self.emit.clone(), query);
    }
}

/// Returns the byte index of the `i`-th grapheme cluster in `s`.
///
/// The returned result is then safe to use for indexing into a `String`.
///
/// If `i` is greater than the number of grapheme clusters in `s`,
/// `None` is returned.
///
/// This function is used to convert between indices obtained from Qt
/// and indices usable on Rust Strings.
///
/// The inverse of this function is [`byte_index_to_char_index`]
///
/// ## Examples
///
/// In the following example, every Latin character is one byte long,
/// but every greek character occupies two bytes.
/// ```
/// use black_walker_model_qt::model::suggestion::char_index_to_byte_index;
///
/// assert_eq!(char_index_to_byte_index("Hello World", 3), Some(3));
/// assert_eq!(char_index_to_byte_index("νυχτερίδα", 3), Some(6));
/// ```
pub fn char_index_to_byte_index(s: &str, i: usize) -> Option<usize> {
    if i == 0 {
        return Some(0);
    }

    if let Some(res) = s.grapheme_indices(true).nth(i).map(|(byteidx, _)| byteidx) {
        Some(res)
    } else {
        // i - 1 is sound because the first line ensures i > 0.
        if s.char_indices().nth(i - 1).is_some() {
            Some(s.len())
        } else {
            None
        }
    }
}

/// Returns the character index that corresponds to `byte_index`.
///
/// This is the index in a hypothetical
/// list of characters in `s`, that corresponds to the character
/// occupying the `byte_index`-th byte.
///
/// In other words:
/// Given the byte index of the n-th grapheme cluster in `s`, this will return n.
///
/// If `byte_index` is no character boundary in `s` or greater than the length
/// of `s`, this will return `None` (so this function won't panic).
///
/// This function is used to convert between indices obtained from Rust Strings
/// and indices usable on Qts `QString`s.
///
/// The inverse of this function is [`char_index_to_byte_index`]
///
/// ## Examples
///
/// In the following example, every Latin character is one byte long,
/// but every greek character occupies two bytes.
/// ```
/// use black_walker_model_qt::model::suggestion::byte_index_to_char_index;
///
/// assert_eq!(byte_index_to_char_index("Hello World", 3), Some(3));
/// assert_eq!(byte_index_to_char_index("νυχτερίδα", 6), Some(3));
/// assert_eq!(byte_index_to_char_index("νυχτερίδα", 5), None);
/// ```
pub fn byte_index_to_char_index(s: &str, byte_index: usize) -> Option<usize> {
    if byte_index == s.len() {
        return s.graphemes(true).enumerate().last().map(|(i, _)| i + 1);
    }
    if !s.is_char_boundary(byte_index) {
        return None;
    }
    match s
        .grapheme_indices(true)
        .enumerate()
        .find(|(_, (byteidx, _))| *byteidx >= byte_index)
    {
        Some((i, (byteidx, _))) => {
            if byteidx == byte_index {
                Some(i)
            } else {
                None
            }
        }
        None => None,
    }
}

#[allow(clippy::reversed_empty_ranges)]
impl SuggestionQtTrait for SuggestionQt {
    fn new(emit: SuggestionQtEmitter, model: SuggestionQtList) -> Self {
        SuggestionQt {
            emit,
            model,
            category: None,
            to_suggest: EntityType::Tag,
            enter_type: EnterType::TAGSWITHGROUP,
            allowed: Allowed::all(),
            state: LineEditState::default(),
            fragment_range: 0..0,
            suggestions: Vec::new(),
            sugg_rec: mpsc::channel().1,
            search_further: Arc::new(()),
            cnt: None,
        }
    }
    fn emit(&mut self) -> &mut SuggestionQtEmitter {
        &mut self.emit
    }
    fn allowed(&self) -> u8 {
        self.allowed.bits()
    }
    fn set_allowed(&mut self, value: u8) {
        self.allowed = Allowed::from_bits_truncate(value);
    }
    fn category(&self) -> &str {
        self.category.as_deref().unwrap_or("")
    }
    fn set_category(&mut self, value: String) {
        self.category = if value.is_empty() { None } else { Some(value) };
    }
    fn enter_type(&self) -> u8 {
        self.enter_type.bits()
    }
    fn set_enter_type(&mut self, value: u8) {
        self.enter_type = EnterType::from_bits_truncate(value)
    }
    fn to_suggest(&self) -> u8 {
        self.to_suggest as u8
    }
    /// ## Panics
    ///
    /// If `value` is not a valid discriminant of `EntityType`.
    /// The valid discriminants are currently the range 0 to 3.
    fn set_to_suggest(&mut self, value: u8) {
        self.to_suggest = EntityType::try_from(value).unwrap();
    }
    fn wakeup(&self) -> bool {
        true
    }
    fn suggest(&mut self, fragment: String, cursor: u32) {
        let settings = self.settings();
        let cursor = char_index_to_byte_index(&fragment, cursor as usize).unwrap_or(fragment.len());
        self.state = LineEditState::new(cursor, fragment);
        let frag = suggestion_fragment(&self.state.text, self.to_suggest, self.enter_type, cursor);
        if frag.fragment.graphemes(true).count()
            < usize::from(settings.entered_chars_for_suggestions)
        {
            self.model.begin_reset_model();
            self.suggestions = Vec::new();
            self.model.end_reset_model();
            return;
        }
        match frag.meta {
            SuggestionQuery::Tag { category, group } => self.send_search(SearchTags::new(
                self.category.clone().or(category),
                frag.fragment,
                group,
                self.allowed,
                settings,
            )),
            SuggestionQuery::TagGroup { category, group } => self.send_search(SearchTags::new(
                category,
                frag.fragment,
                group,
                self.allowed & Allowed::GROUP,
                settings,
            )),
            SuggestionQuery::Entry => self.send_search(SearchEntries::new(
                self.category.clone(),
                frag.fragment,
                settings,
            )),
            SuggestionQuery::TagCategory => {
                self.send_search(SearchTagCategories::new(frag.fragment, settings))
            }
            SuggestionQuery::EntryCategory => {
                self.send_search(SearchEntryCategories::new(frag.fragment, settings))
            }
        }
        self.fragment_range = frag.range;
    }
    fn row_count(&self) -> usize {
        self.suggestions.len()
    }
    fn name(&self, index: usize) -> String {
        self.suggestions
            .get(index)
            .cloned()
            .unwrap_or(String::new())
    }
    /// Fetches suggestions from the receiver
    fn await_suggestions(&mut self) {
        match self.sugg_rec.try_recv() {
            Ok(suggestions) => {
                self.model.begin_reset_model();
                self.suggestions = suggestions.into();
                self.model.end_reset_model();
            }
            Err(_) => (),
        }
    }
    /// Returns the character index of the beginning of the fragment
    /// in which `cursor` is placed in `fragment`
    fn fragment_start(&mut self, fragment: String, cursor: u32) -> u32 {
        let new_state = LineEditState::new(cursor as usize, fragment);
        if self.state != new_state {
            self.state = new_state;
            let frag = suggestion_fragment(
                &self.state.text,
                self.to_suggest,
                self.enter_type,
                char_index_to_byte_index(&self.state.text, self.state.cursor as usize)
                    .unwrap_or(self.state.text.len()),
            );
            self.fragment_range = frag.range;
        }
        byte_index_to_char_index(&self.state.text, self.fragment_range.start).unwrap_or(0) as u32
    }
    /// Returns the character index of the end of the fragment
    /// in which `cursor` is placed in `fragment`
    fn fragment_end(&mut self, fragment: String, cursor: u32) -> u32 {
        let new_state = LineEditState::new(cursor as usize, fragment);
        if self.state != new_state {
            self.state = new_state;
            let frag = suggestion_fragment(
                &self.state.text,
                self.to_suggest,
                self.enter_type,
                char_index_to_byte_index(&self.state.text, self.state.cursor as usize)
                    .unwrap_or(self.state.text.len()),
            );
            self.fragment_range = frag.range;
        }
        byte_index_to_char_index(&self.state.text, self.fragment_range.end).unwrap_or(0) as u32
    }
    fn contains(&self, s: String) -> bool {
        self.suggestions.contains(&s)
    }
}

/// Gives the contact from `masterptr` to `suggestionptr`.
///
/// # Safety
///
/// All pointers must be valid.
#[no_mangle]
pub unsafe extern "C" fn init_suggestion(masterptr: *mut Master, suggestionptr: *mut SuggestionQt) {
    let suggestion = &mut *suggestionptr;
    let master = &mut *masterptr;
    suggestion.cnt = Some(Contact::from_pipe(
        suggestion.emit().clone(),
        master.contact().pipe.clone(),
    ));
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn byte_index_to_char_index_last() {
        assert_eq!(byte_index_to_char_index("Elf", 2), Some(2));
        assert_eq!(byte_index_to_char_index("Elf", 3), Some(3));
        assert_eq!(byte_index_to_char_index("μυν", 4), Some(2));
        assert_eq!(byte_index_to_char_index("μυν", 6), Some(3));

        assert_eq!(byte_index_to_char_index("Elf", 4), None);
        assert_eq!(byte_index_to_char_index("μυν", 7), None);
    }

    #[test]
    fn byte_index_to_char_index_first() {
        assert_eq!(byte_index_to_char_index("Hello World", 0), Some(0));
        assert_eq!(byte_index_to_char_index("μυν", 0), Some(0));
    }

    #[test]
    fn char_index_to_byte_index_last() {
        assert_eq!(char_index_to_byte_index("Elf", 2), Some(2));
        assert_eq!(char_index_to_byte_index("Elf", 3), Some(3));
        assert_eq!(char_index_to_byte_index("μυν", 2), Some(4));
        assert_eq!(char_index_to_byte_index("μυν", 3), Some(6));

        assert_eq!(char_index_to_byte_index("Elf", 4), None);
        assert_eq!(char_index_to_byte_index("μυν", 4), None);
    }

    #[test]
    fn char_index_to_byte_index_first() {
        assert_eq!(char_index_to_byte_index("Hello World", 0), Some(0));
        assert_eq!(char_index_to_byte_index("μυν", 0), Some(0));
    }
}
