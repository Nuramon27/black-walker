//! Contains the overall Objects handling the total coordination
//! of the parts of Black Walker.

//#[cfg(any(test, feature = "test-helper"))]
#[cfg(feature = "test-helper")]
pub mod test_setup;
mod master_init;

use std::cell::RefCell;
use std::collections::VecDeque;
use std::convert::TryFrom;
use std::path::PathBuf;
use std::rc::Rc;
use std::sync::mpsc::{Receiver, Sender};
use std::sync::{mpsc, Arc, RwLock};
use std::thread;
use std::thread::JoinHandle;

use black_walker_logic::diary::SaveState;
use black_walker_logic::model::Emitter;
use black_walker_logic::model::{Model, Narrator, NotifyEnd, Operation, ToNotify};
use black_walker_logic::operate::{core::Undoable, store::export_json::ExportJson};
use black_walker_logic::operate::{all::ClearDiary, misc::NoOp};
use black_walker_logic::prelude::*;
use black_walker_logic::store;
use black_walker_logic::worker;
use black_walker_logic::DiaryRecord;

use crate::feature::undo::Record;
use crate::background::BackGroundOperation;
use crate::interface::*;
use super::dream_form::DreamFormQt;
use super::entry::EntryMaster;
use super::list::DreamListMaster;
use super::search::SearchQt;
use super::settings::SettingsQt;
use super::tagtree::TagTreeMaster;
use super::{BeginEndSignal, Contact, Pipelines};

/// A dummy type for implementing the [`Model`](black_walker_logic::model::Model)
/// trait on.
pub struct PhantomModel {}

impl Model for PhantomModel {
    type DreamListModel = DreamListMaster;
    type DreamForm = DreamFormQt;
    type TagTree = TagTreeMaster;
    type EntryModel = EntryMaster;
}

pub struct MasterLeaveEmitter {
    emit: MasterEmitter,
}

impl Emitter for MasterEmitter {
    fn emit(&mut self) {
        self.wakeup_changed()
    }
}

impl Emitter for MasterLeaveEmitter {
    fn emit(&mut self) {
        self.emit.wakeup_leave_changed();
    }
}

impl BeginEndSignal for MasterEmitter {
    fn signal_begin_cmd(&mut self) {
        self.notify_begin_cmd_changed()
    }
    fn signal_end_cmd(&mut self) {
        self.notify_end_cmd_changed()
    }
}

pub struct Master {
    emit: MasterEmitter,
    dream_form_qt: DreamFormQt,
    dream_list_master: DreamListMaster,
    tag_tree_master: TagTreeMaster,
    entry_master: EntryMaster,
    search_qt: SearchQt,
    settings_qt: SettingsQt,
    _end_ser: Sender<Box<dyn NotifyEnd<PhantomModel>>>,
    end_rec: Receiver<Box<dyn NotifyEnd<PhantomModel>>>,
    begin_queue: Rc<RefCell<VecDeque<Narrator<PhantomModel>>>>,
    //worker_handle: Option<JoinHandle<()>>,
    background_worker_handle: Option<JoinHandle<()>>,
    background_ser: Sender<BackGroundOperation>,
    cnt: Contact<MasterEmitter>,
    rec: Option<Receiver<Operation<PhantomModel>>>,
    query_rec: Receiver<()>,
    query_ser: Sender<()>,
}

impl Drop for Master {
    /// Signals the [`worker`](black_walker_logic::worker::work)
    /// thread to finish execution.
    fn drop(&mut self) {
        self.cnt
            .pipe
            .ser
            .send(Operation::Finish)
            .expect("Worker thread has hung up.");
        if self.background_ser.send(BackGroundOperation::Quit).is_ok() {
            if let Some(background_worker_handle) = self.background_worker_handle.take() {
                match background_worker_handle.join() {
                    Ok(()) => (),
                    Err(_) => (),
                }
            }
        }
        /*if let Some(worker_handle) = self.worker_handle.take() {
            match worker_handle.join() {
                Ok(()) => (),
                Err(_) => (),
            }
        }*/
    }
}

impl Master {
    /// Construct the Master from a given `diary`
    /// initializing all the Objects it contains with this diary
    /// and corresponding senders.
    fn from_diary(
        mut emit: MasterEmitter,
        mut dream_form_qt: DreamFormQt,
        mut dream_list_master: DreamListMaster,
        mut entry_master: EntryMaster,
        mut search_qt: SearchQt,
        mut settings_qt: SettingsQt,
        mut tag_tree_master: TagTreeMaster,
        diary: Diary,
    ) -> Master {
        let (background_ser, background_worker_handle) = Self::spawn_worker(diary.clone());

        let (ser, rec) = mpsc::channel();
        let (end_ser, end_rec) = mpsc::channel();
        let begin_queue = Rc::new(RefCell::new(VecDeque::new()));
        let history = Arc::new(RwLock::new(Record::new(diary)));
        let pipe = Pipelines::new(
            Arc::clone(&history),
            ser,
            Rc::clone(&begin_queue),
            end_ser.clone(),
        );
        let dream_form_contact = Contact::from_pipe(dream_form_qt.emit().clone(), pipe.clone());
        dream_form_qt.set_md(dream_form_contact);
        let dream_list_contact = Contact::from_pipe(dream_list_master.emit().clone(), pipe.clone());
        dream_list_master.set_contact(dream_list_contact);
        let tag_tree_contact = Contact::from_pipe(tag_tree_master.emit().clone(), pipe.clone());
        tag_tree_master.set_contact(tag_tree_contact);
        /*let worker_handle = {
            let end_ser = end_ser.clone();
            let wakeup = Box::new(emit.clone());
            let diary = diary.clone();
            thread::spawn(move || {
                worker::work(diary, rec, end_ser, wakeup)
            })
        };*/
        entry_master.set_contact(pipe.clone());
        search_qt.set_pipe(pipe.clone());
        let settings_contact = Contact::from_pipe(settings_qt.emit().clone(), pipe.clone());
        settings_qt.set_contact(settings_contact);
        let (query_ser, query_rec) = mpsc::channel();

        Master {
            emit: emit.clone(),
            dream_form_qt,
            dream_list_master,
            tag_tree_master,
            entry_master,
            search_qt,
            settings_qt,
            begin_queue: begin_queue.clone(),
            _end_ser: end_ser,
            end_rec,
            //worker_handle: Some(worker_handle),
            background_worker_handle: Some(background_worker_handle),
            background_ser,
            cnt: Contact::from_pipe(emit, pipe),
            rec: Some(rec),
            query_rec,
            query_ser,
        }
    }
    fn spawn_worker(diary: Diary) -> (Sender<BackGroundOperation>, JoinHandle<()>) {
        let (ser, rec) = mpsc::channel();
        let worker_handle = thread::Builder::new()
            .name("Background".to_string())
            .spawn(move || {
                crate::background::work(diary, rec)
                // Allowed panic because we are at startup
                // and there is now way around failure
            })
            .expect("Background thread could not be started");

        (ser, worker_handle)
    }
    /// Obtain the `Contact` of the `Master`.
    pub fn contact(&self) -> &Contact<MasterEmitter> {
        &self.cnt
    }
}

impl MasterTrait for Master {
    /// Constructs a new `Master`.
    ///
    /// In a debug build, this uses an empty diary.
    ///
    /// In a release build, this tries to load a diary from the standard
    /// location (to be found in the bootstrap file, which is expected in the same
    /// directory as the BlackWalker executable). Otherwise it uses an empty diary.
    fn new(
        emit: MasterEmitter,
        dream_form_qt: DreamFormQt,
        dream_list_master: DreamListMaster,
        entry_master: EntryMaster,
        search_qt: SearchQt,
        settings_qt: SettingsQt,
        tag_tree_master: TagTreeMaster,
    ) -> Master {
        if let Err(e) = Self::init_panic_logging() {
            eprintln!("Could not initialize logging:\n{:?}", e);
        }
        black_walker_logic::elements::tag::names::collation::init_collator();
        #[cfg(feature = "search")]
        black_walker_logic::search::collation::init_collator();
        #[cfg(debug_assertions)]
        let diary = Diary::new();
        #[cfg(not(debug_assertions))]
        let diary = match store::read(None) {
            Ok(diary_cmd) => diary_cmd.into_diary(),
            Err(_) => Diary::new(),
        };
        Master::from_diary(
            emit,
            dream_form_qt,
            dream_list_master,
            entry_master,
            search_qt,
            settings_qt,
            tag_tree_master,
            diary,
        )
    }
    fn emit(&mut self) -> &mut MasterEmitter {
        &mut self.emit
    }
    fn dream_form_qt(&self) -> &DreamFormQt {
        &self.dream_form_qt
    }
    fn dream_form_qt_mut(&mut self) -> &mut DreamFormQt {
        &mut self.dream_form_qt
    }
    fn dream_list_master(&self) -> &DreamListMaster {
        &self.dream_list_master
    }
    fn dream_list_master_mut(&mut self) -> &mut DreamListMaster {
        &mut self.dream_list_master
    }
    fn entry_master(&self) -> &EntryMaster {
        &self.entry_master
    }
    fn entry_master_mut(&mut self) -> &mut EntryMaster {
        &mut self.entry_master
    }
    fn tag_tree_master(&self) -> &TagTreeMaster {
        &self.tag_tree_master
    }
    fn tag_tree_master_mut(&mut self) -> &mut TagTreeMaster {
        &mut self.tag_tree_master
    }
    fn search_qt(&self) -> &SearchQt {
        &self.search_qt
    }
    fn search_qt_mut(&mut self) -> &mut SearchQt {
        &mut self.search_qt
    }
    fn settings_qt(&self) -> &SettingsQt {
        &self.settings_qt
    }
    fn settings_qt_mut(&mut self) -> &mut SettingsQt {
        &mut self.settings_qt
    }
    /// Fetch a [`Narrator`](black_walker_logic::model::Narrator) from the
    /// [`begin_queue`](Master::begin_queue) and notify the Models
    /// according to this `Narrator`.
    ///
    /// Usually you should call this function directly after enqueuing a simple
    /// `Narrator` on the `begin_queue`, however for the case of accidental oversights,
    /// this fetches all notifiers found in the queue.
    fn begin_cmd(&mut self) {
        while let Some(cmd) = RefCell::borrow_mut(&self.begin_queue).pop_front() {
            if cmd.whom_begin().intersects(ToNotify::DREAMFORM) {
                cmd.notify_dreamform_begin(&mut self.dream_form_qt);
            }
            if cmd.whom_begin().intersects(ToNotify::DREAMLIST) {
                cmd.notify_dreamlist_begin(&mut self.dream_list_master);
            }
            if cmd.whom_begin().intersects(ToNotify::TAGTREE) {
                cmd.notify_tagtree_begin(&mut self.tag_tree_master);
            }
            if cmd.whom_begin().intersects(ToNotify::ENTRYSET) {
                cmd.notify_entry_begin(&mut self.entry_master);
            }
        }
    }

    /// Fetch a [`NotifyEnd`](black_walker_logic::model::NotifyEnd) trait object from the
    /// [`end_rec`](Master::end_rec) and notify the Models
    /// according to this `Narrator`.
    ///
    /// Usually you should call this function directly after sending a single
    /// notifier on your instance of the [`end_ser`](Pipelines::end_ser),
    /// however for the case of accidental oversights,
    /// this fetches all notifiers found in the reciever.
    fn end_cmd(&mut self) {
        while let Ok(cmd) = self.end_rec.try_recv() {
            if cmd.whom_end().intersects(ToNotify::DREAMFORM) {
                cmd.notify_dreamform_end(&mut self.dream_form_qt);
            }
            if cmd.whom_end().intersects(ToNotify::DREAMLIST) {
                cmd.notify_dreamlist_end(&mut self.dream_list_master);
            }
            if cmd.whom_end().intersects(ToNotify::TAGTREE) {
                cmd.notify_tagtree_end(&mut self.tag_tree_master);
            }
            if cmd.whom_end().intersects(ToNotify::ENTRYSET) {
                cmd.notify_entry_end(&mut self.entry_master);
            }
        }
    }
    #[doc(hidden)]
    fn wakeup(&self) -> bool {
        true
    }
    #[doc(hidden)]
    fn wakeup_leave(&self) -> bool {
        true
    }
    /// Tells whether the diary is in a modified state.
    fn modified(&self) -> u8 {
        self.cnt.diary.state.load().into()
    }
    /// Resets the whole diary.
    fn clear(&mut self) {
        self.cnt.execute(Undoable::from(ClearDiary::new()));
    }
    #[doc(hidden)]
    fn notify_begin_cmd(&self) -> bool {
        true
    }
    #[doc(hidden)]
    fn notify_end_cmd(&self) -> bool {
        true
    }
    #[doc(hidden)]
    fn leave_signal(&self) -> bool {
        true
    }
    /// Currently not in use
    fn begin_leave(&mut self) {
        let emit = self.emit.clone();
        self.cnt.query(
            MasterLeaveEmitter { emit },
            NoOp {
                ser: self.query_ser.clone(),
            },
        );
    }
    /// Currently not in use
    fn leave(&mut self) {
        self.query_rec
            .recv()
            .expect("Could not receive leave signal.");
        self.emit.leave_signal_changed();
    }

    /// Saves the diary to the standard location, returning
    /// whether the operation was successfull.
    fn save_to_std(&mut self) -> bool {
        if store::write(&self.cnt.diary, None).is_ok() {
            self.cnt.diary.state.store(SaveState::Unmodified);
            true
        } else {
            false
        }
    }
    /// Saves a copy of the diary to `path`, returning whether the operation
    /// was successfull.
    ///
    /// This is useful if you want to save a backup copy of the diary to a
    /// special location without changing the standard path.
    fn save(&mut self, path: String) -> bool {
        store::write(&self.cnt.diary, Some(path.into())).is_ok()
    }
    fn export_diary_json(&mut self, path: String) {
        let (query, _, _further) = ExportJson::new(path.into());
        self.cnt.query(self.emit.clone(), query);
    }
    /// Loads the diary from `path`.
    fn load_from(&mut self, path: String) -> bool {
        match store::read(Some(path.into())) {
            Ok(cmd) => {
                self.cnt.execute(Undoable::from(cmd));
                true
            }
            Err(_) => false,
        }
    }
    /// Loads the diary from `path` but restores the old location of the diary.
    ///
    /// This is intended for loading backups. If the old location was empty,
    /// it is not restored.
    fn load_backup(&mut self, path: String) -> bool {
        let old_location = self.settings_qt.location();
        match store::read(Some(path.clone().into())) {
            Ok(cmd) => {
                self.cnt.execute(Undoable::from(cmd));
                if old_location.is_empty() {
                    let path = PathBuf::from(path);
                    if let (Some(filename), Some(parent)) =
                        (path.file_name().and_then(|s| s.to_str()), path.parent())
                    {
                        if parent.ends_with("Backup") {
                            if let Some(parentparent) = parent.parent() {
                                let backup_regexp =
                                    &black_walker_logic::store::backup::BACKUP_REG_EXP;
                                if let Some(name) = backup_regexp
                                    .captures(filename)
                                    .and_then(|c| c.name("name"))
                                {
                                    let mut settings = self.cnt.diary.gl.write().unwrap();
                                    settings.diary_location =
                                        black_walker_logic::settings::DiaryLocation::try_from(
                                            parentparent.join(format!("{}.toml", name.as_str())),
                                        )
                                        .ok();
                                }
                            }
                        }
                    }
                } else {
                    self.settings_qt.set_location(old_location);
                }
                true
            }
            Err(_) => false,
        }
    }

    #[cfg(feature = "stat")]
    fn pretty_print_diary(&mut self, path: String) -> bool {
        self._pretty_print_diary(path)
    }
    #[cfg(not(feature = "stat"))]
    fn pretty_print_diary(&mut self, _path: String) -> bool { true }

}

#[cfg(feature = "stat")]
mod pretty_print {
    use std::fs::OpenOptions;
    use std::io::{BufWriter, Write};

    use black_walker_logic::stat::pretty_print;

    use super::*;

    impl Master {
        pub fn _pretty_print_diary(&mut self, path: String) -> bool {
            if let Ok(destination) = OpenOptions::new()
                .write(true)
                .create(true)
                .truncate(true)
                .open(&path)
            {
                let mut tmpbuffer = BufWriter::new(destination);
                pretty_print::pretty_print(
                    &pretty_print::Settings::default(),
                    &mut tmpbuffer,
                    &mut self.cnt.diary,
                )
                .is_ok()
                    && tmpbuffer.flush().is_ok()
            } else {
                false
            }
        }
    }
}

/// The structure handling communication on the worker thread.
///
/// This struct is intended to live on the [worker thread](black_walker_logic::worker::work)
/// and:
///
/// * send back signals (via `WorkerMaster::emit::wakeup_changed`)
/// and messages (via [`WorkerMaster::model_notifier`]) to the [`Master`].
///
/// * receive operations via [`WorkerMaster::rec`].
pub struct WorkerMaster {
    /// The emitter
    emit: WorkerMasterEmitter,
    /// The complete history of the Diary.
    history: Option<Arc<RwLock<DiaryRecord<PhantomModel>>>>,
    /// The receiver for [`Operation`](black_walker_logic::model::Operation)s.
    rec: Option<Receiver<Operation<PhantomModel>>>,
    /// The sender for End notifications.
    model_notifier: Option<Sender<Box<dyn NotifyEnd<PhantomModel>>>>,
}

impl Emitter for WorkerMasterEmitter {
    fn emit(&mut self) {
        self.wakeup_changed()
    }
}

/// Gives the history and important Senders/Receivers from `masterptr` to `workerptr`.
///
/// # Safety
///
/// All pointers must be valid.
#[no_mangle]
pub unsafe extern "C" fn init_worker_master(masterptr: *mut Master, workerptr: *mut WorkerMaster) {
    let worker = &mut *workerptr;
    let master = &mut *masterptr;
    worker.history = Some(Arc::clone(&master.cnt.pipe.history));
    worker.rec = Some(master.rec.take().unwrap());
    worker.model_notifier = Some(master._end_ser.clone());
}

impl WorkerMasterTrait for WorkerMaster {
    /// Construct a new `WorkerMaster` that has yet to be initialized
    /// with [`init_worker_master`].
    fn new(emit: WorkerMasterEmitter) -> Self {
        WorkerMaster {
            emit,
            history: None,
            rec: None,
            model_notifier: None,
        }
    }
    fn emit(&mut self) -> &mut WorkerMasterEmitter {
        &mut self.emit
    }
    #[doc(hidden)]
    fn wakeup(&self) -> bool {
        true
    }
    /// Start the worker thread, beginning to receive
    /// [`Operation`](black_walker_logic::model::Operation)s.
    fn work(&mut self) {
        worker::work(
            Arc::clone(self.history.as_ref().unwrap()),
            self.rec.take().unwrap(),
            self.model_notifier.take().unwrap(),
            Box::new(self.emit.clone()),
        );
    }
}
