use std::convert::TryFrom;

use chrono::{NaiveDate, NaiveDateTime, NaiveTime};

use black_walker_logic::elements::night::SleepSection;
use black_walker_logic::operate::night::SetNight;
use black_walker_logic::prelude::*;

use super::{Contact, Pipelines};
use crate::interface::*;
use crate::{apply, execute, impl_begin_end_signals};

impl_begin_end_signals! { NightQtEmitter }

pub struct NightQt {
    emit: NightQtEmitter,
    cnt: Option<Contact<NightQtEmitter>>,
    night: NaiveDate,
    night_sections: Vec<SleepSection>,
}

impl NightQt {
    pub fn set_contact(&mut self, pipe: Pipelines) {
        self.cnt = Some(Contact::from_pipe(self.emit.clone(), pipe));
    }

    fn initialize(&mut self) {
        self.night_sections = self
            .query_night(self.night.into(), |night| {
                Some(night.sleep_sections().clone())
            })
            .unwrap_or_default();
    }

    fn expand_night_sections(&mut self, length: usize) {
        while self.night_sections.len() < length {
            let last_time = self
                .night_sections
                .last()
                // Won't panic because `night_setcions` is non-empty
                .unwrap()
                .end();
            self.night_sections
                .push(SleepSection::from_basic(last_time, last_time));
        }
    }

    fn query<F: FnOnce(&NightList) -> T, T>(&mut self, func: F) -> T {
        apply!(self.cnt, NightList; func)
    }

    fn query_night<F: FnOnce(&Night) -> Option<T>, T>(
        &mut self,
        date: RevDate,
        func: F,
    ) -> Option<T> {
        self.query(|nightlist| nightlist.get(&date).and_then(func))
    }

    pub fn open_night_direct(&mut self, date: NaiveDate) -> bool {
        self.night = date;
        self.initialize();

        true
    }
}

impl NightQtTrait for NightQt {
    fn new(emit: NightQtEmitter) -> Self {
        NightQt {
            emit,
            cnt: None,
            night: chrono::offset::Local::today().naive_local(),
            night_sections: Vec::new(),
        }
    }
    fn emit(&mut self) -> &mut NightQtEmitter {
        &mut self.emit
    }
    fn display_text(&self, index: u32) -> String {
        self.night_sections
            .get(index as usize)
            .map(|section| {
                format!(
                    "{}–{}",
                    section.start().format("%H:%M").to_string(),
                    section.end().format("%H:%M").to_string(),
                )
            })
            .unwrap_or_default()
    }
    fn fall_asleep(&self, index: u32) -> String {
        self.night_sections
            .get(index as usize)
            .map(|section| section.start().format("%H:%M:%S").to_string())
            .unwrap_or_default()
    }
    fn wake_up(&self, index: u32) -> String {
        self.night_sections
            .get(index as usize)
            .map(|section| section.end().format("%H:%M:%S").to_string())
            .unwrap_or_default()
    }
    fn set_fall_asleep(&mut self, index: u32, hour: u32, minute: u32) {
        let time = associate_time_to_day(NaiveTime::from_hms(hour, minute, 0), self.night);
        if let Some(night_section) = self.night_sections.get_mut(index as usize) {
            night_section.set_start(time);
        } else {
            self.expand_night_sections(index as usize);
            self.night_sections
                .push(SleepSection::from_basic(time, time));
        }
    }
    fn set_wake_up(&mut self, index: u32, hour: u32, minute: u32) {
        let time = associate_time_to_day(NaiveTime::from_hms(hour, minute, 0), self.night);
        if let Some(night_section) = self.night_sections.get_mut(index as usize) {
            night_section.set_end(time);
        } else {
            self.expand_night_sections(index as usize);
            self.night_sections
                .push(SleepSection::from_basic(time, time));
        }
    }
    fn ease_of_fall_asleep(&self, index: u32) -> i32 {
        self.night_sections
            .get(index as usize)
            .and_then(|section| section.ease_of_fallasleep())
            .map(|a| a as i32)
            .unwrap_or(-1)
    }
    fn set_ease_of_fall_asleep(&mut self, index: u32, value: i32) {
        if let Some(night_section) = self.night_sections.get_mut(index as usize) {
            if let Ok(value) = u32::try_from(value) {
                night_section.set_ease_of_fallasleep(Some(value));
            } else {
                night_section.set_ease_of_fallasleep(None);
            }
        }
    }
    fn nr_sections(&self) -> u32 {
        self.night_sections.len() as u32
    }
    fn open_night(&mut self, year: i32, month: u32, day: u32) -> bool {
        let date = NaiveDate::from_ymd(year, month, day);
        self.open_night_direct(date)
    }
    fn save_night(&mut self) {
        let cmd = SetNight::new(
            self.night,
            Night::from_sleep_sections(self.night_sections.clone()),
        );
        execute!(self.cnt, cmd);
    }

    #[doc(hidden)]
    fn begin_cmd(&self) -> bool {
        true
    }
    #[doc(hidden)]
    fn end_cmd(&self) -> bool {
        true
    }
}

/// Associates a time in a night either to `day` or to the previous
/// day, depending on how late the time is.
///
/// This is based on heuristics about when normal people tend
/// to go to bed. Currently times earlier than 15:00 are associated
/// with the current day, later times with the previous day.
fn associate_time_to_day(time: NaiveTime, day: NaiveDate) -> NaiveDateTime {
    if time < NaiveTime::from_hms(15, 00, 00) {
        NaiveDateTime::new(day, time)
    } else {
        NaiveDateTime::new(day - chrono::Duration::days(1), time)
    }
}
