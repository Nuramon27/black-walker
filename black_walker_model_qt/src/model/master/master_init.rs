/// The functions which initialize the [`Master`] at startup of the application.

use std::io;

use super::Master;

impl Master {
    #[cfg(any(not(feature = "log_panic"), debug_assertions))]
    pub fn init_panic_logging() -> Result<(), MasterInitError> { Ok(()) }
}

/// An Error which may occurr during initialization of the [`Master`]
#[derive(Debug)]
pub enum MasterInitError {
    /// The logfile could not be opened
    OpenLogFile(io::Error),
    #[cfg(feature = "log_panic")]
    /// The logfile could not be set as the target for logging
    SetLogger(log::SetLoggerError)
}

impl From<io::Error> for MasterInitError {
    fn from(value: io::Error) -> Self {
        Self::OpenLogFile(value)
    }
}

#[cfg(feature = "log_panic")]
#[allow(unused)]
pub mod log_panics {
    use std::io;
    use std::fs;
    use std::io::Write;

    use simplelog::{WriteLogger, LevelFilter, Config};

    use super::super::Master;
    use super::MasterInitError;

    /// A Wrapper around a Writer which flushes after every write.
    ///
    /// This is necessary in order to log panic-messages to a file,
    /// since in black-walker, we must set 'panic = "abbort"' because of FFI.
    /// Due to this, after a panic the program is immediately aborted, so
    /// the writer which should write the message to the logfile never
    /// gets dropped properly and thus not flushed.
    struct WriterFlushAlways<W: Write> {
        writer: W
    }

    impl<W: Write> WriterFlushAlways<W> {
        fn new(writer: W) -> Self {
            Self { writer }
        }
    }

    impl<W: Write> Write for WriterFlushAlways<W> {
        fn write(&mut self, buf: &[u8]) -> io::Result<usize> {
            let res = self.writer.write(buf)?;
            self.writer.flush()?;
            Ok(res)
        }
        fn flush(&mut self) -> io::Result<()> {
            self.writer.flush()
        }
    }

    impl Master {
        #[cfg(not(debug_assertions))]
        /// Sets a panic-hook which writes panics to the file "log.txt"
        /// instead of printing the to stderr.
        pub fn init_panic_logging() -> Result<(), MasterInitError> {
            let log_file = fs::OpenOptions::new()
                .create(true)
                .append(true)
                .open("log.txt")?;
            let log_file = io::BufWriter::new(log_file);
            let log_file = WriterFlushAlways::new(log_file);
            WriteLogger::init(LevelFilter::Error, Config::default(), log_file)?;
            log_panics::init();
            Ok(())
        }
    }

    impl From<log::SetLoggerError> for MasterInitError {
        fn from(value: log::SetLoggerError) -> Self {
            Self::SetLogger(value)
        }
    }
}