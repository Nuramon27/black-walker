use std::ffi;

use black_walker_logic::operate::{core::Undoable, tests::standard_diary_cmd};

use crate::model::master::Master;

#[no_mangle]
pub unsafe extern "C" fn test_setup_standard_diary(ptr: *mut Master) {
    let (tagcmd, dreamcmd) = standard_diary_cmd();
    let m = &mut *ptr;
    for it in tagcmd {
        m.cnt.enqueue(Undoable::from(it));
    }
    for it in dreamcmd {
        m.cnt.enqueue(Undoable::from(it));
    }
}

#[no_mangle]
unsafe extern "C" fn write_err(err: *mut String, val: *const libc::c_char) {
    let err: &mut String = &mut *err;
    match ffi::CStr::from_ptr(val).to_str() {
        Ok(s) => err.extend(s.chars()),
        Err(_) => (),
    };
}
