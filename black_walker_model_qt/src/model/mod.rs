//! The model module contains the implementation of modules for the Qt-GUI
//! written in C++.
//!
//! All models are contained in a [`Master`](master::Master) model, which
//! takes care of initializing them and distributing means of communication
//! like senders and receivers.
//!
//! # Best practices for models:
//!
//! * Functions called by Qt may only panic in two cases: When they are not
//! initialized or when a Mutex/Sender/Receiver etc. is poisoned. If they cannot
//! obtain the value asked for, they should silently return a default value.
//! _This may change once `Option` and `Result` types are possible on the
//! ffi boundary_.
//!
//! * Every model should contain a `query` function which evaluates
//! a function on the represented part of the [`Diary`](black_walker_logic::Diary).
//! The function taken should return a plain value (not an Option).
//!
//! * If the represented part of the Diary consists of multiple instances (like the
//! [`TagForest`](black_walker_logic::TagForest)), there may be a corresponding
//! query-function (like `query_tree`) which takes as an additional argument a value
//! identifying the instance of the Diary-part (like a tag category). Here the funtion
//! taken should return an Option, hence the query-function should do the same.

use std::cell::RefCell;
use std::collections::VecDeque;
use std::rc::Rc;
use std::sync::mpsc::Sender;
use std::sync::{Arc, RwLock};

use black_walker_logic::model::{Emitter, Narrator, NarratorT, NotifyEnd, Operation, Query};
use black_walker_logic::prelude::*;
use black_walker_logic::DiaryRecord;

pub mod dream_form;
pub mod entry;
pub mod list;
pub mod master;
pub mod night;
pub mod search;
pub mod settings;
pub mod suggestion;
pub mod tagtree;

#[doc(hidden)]
pub use dream_form::*;
#[doc(hidden)]
pub use entry::*;
#[doc(hidden)]
pub use list::*;
#[doc(hidden)]
pub use master::*;
#[doc(hidden)]
pub use night::*;
#[doc(hidden)]
pub use search::*;
#[doc(hidden)]
pub use settings::*;
#[doc(hidden)]
pub use suggestion::*;
#[doc(hidden)]
pub use tagtree::*;

/// The `BeginEndSignal` trait represents the possibility to inform somebody
/// that a command is about to be executed
/// (via [`signal_begin_cmd`](BeginEndSignal::signal_begin_cmd))
/// or has just finished executing (via [`signal_end_cmd`](BeginEndSignal::signal_end_cmd))
///
/// Information about the actual command is not meant to be transferred by these
/// methods. Instead they work as 'wakeup'-calls in order to notify
/// that there is a  command. But this command has been sent to the receiving
/// instance before calling `signal_begin_cmd` or `signal_end_cmd` via a different
/// channel (usually a `std::sync::mpsc::Sender` or a `VecDeque`).
pub trait BeginEndSignal {
    /// Signal that there is a command about to be executed.
    fn signal_begin_cmd(&mut self);
    /// Signal that the execution of a command has been finished.
    fn signal_end_cmd(&mut self);
}

/// The exchangeable parts of a [`Contact`].
///
/// These fields are needed in every `Contact` and can simply
/// be passed by clone. They are mainly sender or equivalent structures
/// for sending commands or notifications.
#[derive(Debug, Clone)]
pub struct Pipelines {
    /// A reference to the whole history of the diary.
    pub history: Arc<RwLock<DiaryRecord<PhantomModel>>>,
    /// A `Sender` for sending commands to the worker thread.
    pub ser: Sender<Operation<PhantomModel>>,
    /// A queue for sending commands to the instance that cares about the
    /// Notification of the beginning of commands.
    ///
    /// See [`enqueue`](Contact::enqueue) for the process of Notification
    /// and execution.
    pub begin_queue: Rc<RefCell<VecDeque<Narrator<PhantomModel>>>>,
    /// A sender for sending commands to the instance that cares about the
    /// Notification of the end of commands.
    pub end_ser: Sender<Box<dyn NotifyEnd<PhantomModel>>>,
}

#[derive(Debug)]
/// The `Contact` struct collects the main parts necessary for enqueuing commands
/// and notifying qt-models about their execution.
///
/// It contains an [emitter](Contact::emit) providing to basic Qt-signals
/// as well as a direct instance of the [diary](Contact::diary)
/// and some primitives for sending notifications and Commands in the
/// [`pipe`](Contact::pipe).
pub struct Contact<E: BeginEndSignal> {
    /// An emmiter that can send signals about the beginning and end of
    /// execution of a command.
    pub emit: E,
    /// The exchangeable parts of the `Contact`.
    pub pipe: Pipelines,
    /// The [`Diary`](black_walker_logic::Diary) on which the commands will be executed.
    pub diary: Diary,
}

impl Pipelines {
    /// Constructs a new `Pipelines` instance.
    pub fn new(
        history: Arc<RwLock<DiaryRecord<PhantomModel>>>,
        ser: Sender<Operation<PhantomModel>>,
        begin_queue: Rc<RefCell<VecDeque<Narrator<PhantomModel>>>>,
        end_ser: Sender<Box<dyn NotifyEnd<PhantomModel>>>,
    ) -> Pipelines {
        Pipelines {
            history,
            ser,
            begin_queue,
            end_ser,
        }
    }
}

impl<E: BeginEndSignal> Contact<E> {
    /// Constructs a new command with emmitter `emit`, and `pipe`.
    pub fn from_pipe(emit: E, pipe: Pipelines) -> Contact<E> {
        let diary = RwLock::read(&pipe.history).unwrap().as_receiver().clone();
        Contact { emit, diary, pipe }
    }

    /// Constructs a new `Contact` with emmitter `emit` and a pipe
    /// consisting of `history`, `ser`, `begin_queue` and `end_ser`.
    pub fn new(
        emit: E,
        history: Arc<RwLock<DiaryRecord<PhantomModel>>>,
        ser: Sender<Operation<PhantomModel>>,
        begin_queue: Rc<RefCell<VecDeque<Narrator<PhantomModel>>>>,
        end_ser: Sender<Box<dyn NotifyEnd<PhantomModel>>>,
    ) -> Contact<E> {
        let diary = RwLock::read(&history).unwrap().as_receiver().clone();
        Contact {
            emit,
            diary,
            pipe: Pipelines {
                history,
                ser,
                begin_queue,
                end_ser,
            },
        }
    }
    /// Enqueues `cmd` to be executed and notified about.
    ///
    /// Every time a command is enqueued, a `clone`
    /// of it will be pushed to `begin_queue`. Then `emit.signal_begin_cmd`
    /// will be executed in order to notify someone that there is a beginning
    /// command to process. The instance who recieves this command will
    /// then take care of notifying models about its beginning.
    ///
    /// Finally, the command is sent down [`ser`](Pipelines::ser).
    pub fn enqueue<C: NarratorT<PhantomModel> + Clone + 'static>(&mut self, cmd: C) {
        RefCell::borrow_mut(&self.pipe.begin_queue).push_back(Box::new(cmd.clone()));
        self.diary.set_modified();
        self.emit.signal_begin_cmd();
        self.pipe
            .ser
            .send(Operation::Narr(Box::new(cmd)))
            .expect("Could not enqueue command.");
    }

    /// Executes `cmd` in the current thread.
    ///
    /// Before the command is executed, a `clone`
    /// of it will be pushed to `begin_queue`. Then `emit.signal_begin_cmd`
    /// will be executed in order to notify someone that there is a beginning
    /// command to process. The instance who recieves this command will
    /// then take care of notifying models about its beginning.
    ///
    /// Then the command is executed.
    ///
    /// If the execution has been successful, the resulting notifier
    /// is sent down the `end_ser` channel and `emit.signal_end_cmd`
    /// es executed.
    pub fn execute<C: NarratorT<PhantomModel> + Clone + 'static>(&mut self, cmd: C) {
        RefCell::borrow_mut(&self.pipe.begin_queue).push_back(Box::new(cmd.clone()));
        self.diary.set_modified();
        self.emit.signal_begin_cmd();
        match RwLock::write(&self.pipe.history)
            .expect("GUI thread has hung up")
            .apply(cmd)
        {
            Ok(res) => self.pipe.end_ser.send(res).expect("GUI thread has hung up"),
            Err(CmdError::Uncritical(e, f)) => eprintln!("Error: {}, {}", e, f),
            Err(e) => panic!("Critical error {:?}", e),
        };
        self.emit.signal_end_cmd();
    }

    /// Enqueues the Query `qu` to be executed and return its result.
    ///
    /// The Query `qu` will simply be sent down the channel.
    pub fn query<T: Emitter + 'static, Q: Query + 'static>(&self, callback: T, qu: Q) {
        self.pipe
            .ser
            .send(Operation::Query(Box::new(callback), Box::new(qu)))
            .expect("Could not enqueue query.");
    }
}

/// Represents an action of notification to be taken,
/// like a simple `begin_insert_rows` or a `begin_reset_model`.
///
/// For a detailed explanation, see [`Nesting`]
#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub enum NotifyAction {
    /// Do nothing
    Nothing,
    /// Do the normal action of notification that informs
    /// views about happening changes.
    ///
    /// I.e. when you are inserting rows, emit `begin_insert_rows`.
    Normal,
    /// Do a `begin_reset_model` or `end_reset_model`.
    ResetModel,
}

/// Controls the nesting of notifications
///
/// When a Qt Model emits a change notification like `begin_insert_rows`,
/// this should be followed by a corresponding end notification like `end_insert_rows`.
///
/// If however in the meantime another change happens -- like a second row
/// being inserted -- one cannot simply emit a second `begin_insert_rows` because
/// Qt won't be able to track these changes. Instead, one will want to emit
/// a `begin_reset_model` in order to be allowed to do anything one wants to do.
///
/// The `Nesting` struct tracks the nesting of such notifications. Its nesting
/// level can be increased by calling [`nest`](Nesting::nest) and decreased
/// with [`unnest`](Nesting::unnest). These methods emit a [`NotifyAction`]
/// that informs about the necessary notification for this change, i.e.
/// whether the _normal_ notification like `begin_insert_rows`/`end_insert_rows` can be sent
/// or a `begin_reset_model`/`end_reset_model` is necessary.
///
/// ## Examples
///
/// ```
/// use black_walker_model_qt::model::{Nesting, NotifyAction};
///
/// let mut n = Nesting::new();
/// assert_eq!(n.nest(), NotifyAction::Normal);
/// assert_eq!(n.nest(), NotifyAction::ResetModel);
/// assert_eq!(n.unnest(), NotifyAction::Nothing);
/// assert_eq!(n, Nesting::ResetModel(0));
/// assert_eq!(n.unnest(), NotifyAction::ResetModel);
/// ```
#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub enum Nesting {
    /// The `Nesting` is clean, i.e. there is no pending notification
    Clean,
    /// There is exactly one pending notification
    Single,
    /// A reset model is going on. The single field gives
    /// the level of nested notifications.
    ResetModel(u32),
}

impl Default for Nesting {
    fn default() -> Nesting {
        Nesting::Clean
    }
}

impl Nesting {
    /// Construct a [`Clean`](Nesting::Clean) `Nesting`.
    pub fn new() -> Nesting {
        Nesting::Clean
    }
    /// Increase the nesting level and ask for the necessary action
    /// of notification.
    ///
    /// *See also:* [`unnest`](Nesting::unnest)
    pub fn nest(&mut self) -> NotifyAction {
        match self {
            Nesting::Clean => {
                *self = Nesting::Single;
                NotifyAction::Normal
            }
            Nesting::Single => {
                *self = Nesting::ResetModel(1);
                NotifyAction::ResetModel
            }
            Nesting::ResetModel(ref mut level) => {
                *level += 1;
                NotifyAction::Nothing
            }
        }
    }
    /// Decrease the nesting level and ask for the necessary action
    /// of notification.
    ///
    /// *See also:* [`nest`](Nesting::nest)
    pub fn unnest(&mut self) -> NotifyAction {
        match self {
            Nesting::Clean => NotifyAction::Nothing,
            Nesting::Single => {
                *self = Nesting::Clean;
                NotifyAction::Normal
            }
            Nesting::ResetModel(0) => {
                *self = Nesting::Clean;
                NotifyAction::ResetModel
            }
            Nesting::ResetModel(ref mut level) => {
                *level -= 1;
                NotifyAction::Nothing
            }
        }
    }
}

#[macro_export]
macro_rules! query {
    ( $contact: expr, $callback: expr, $query: expr ) => {
        $contact
            .as_mut()
            .expect("Contact not initialized")
            .query($callback, $query);
    };
}

#[macro_export]
/// Short-hand for retrieving a reference to the $contact,
/// unwrapping it, converting $cmd into an Undoable
/// and enqueuing it to the contact.
///
/// $contact: An Option<Contact>
/// $cmd: An `Applyable`
macro_rules! enqueue {
    ( $contact: expr, $cmd: expr ) => {
        $contact
            .as_mut()
            .expect("Contact not initialized")
            .enqueue(Undoable::from($cmd));
    };
}

#[macro_export]
/// Short-hand for retrieving a reference to the $contact,
/// unwrapping it, converting $cmd into an Undoable
/// and executing it by the contact.
///
/// $contact: An Option<Contact>
/// $cmd: An `Applyable`
macro_rules! execute {
    ( $contact: expr, $cmd: expr ) => {
        $contact
            .as_mut()
            .expect("Contact not initialized")
            .execute(black_walker_logic::operate::core::Undoable::from($cmd))
    };
}

#[cfg(test)]
mod tests {
    use std::cell::RefCell;
    use std::collections::VecDeque;
    use std::rc::Rc;
    use std::sync::mpsc::{Receiver, Sender};
    use std::sync::{mpsc, Arc, RwLock};

    use chrono::NaiveDate;
    use crate::feature::undo::Record;

    use black_walker_logic::model::{Narrator, NotifyEnd, Operation};
    use black_walker_logic::prelude::*;

    use black_walker_logic::operate::dream::CreateDream;

    use super::{Contact, Nesting, NotifyAction};
    use crate::prelude::*;

    #[derive(Debug)]
    struct TestEmitter {
        pub begin_queue: Rc<RefCell<VecDeque<Narrator<PhantomModel>>>>,
        pub rec: Receiver<Operation<PhantomModel>>,
        pub begin: u32,
        pub end: u32,
    }
    impl BeginEndSignal for Rc<RefCell<TestEmitter>> {
        fn signal_begin_cmd(&mut self) {
            RefCell::borrow_mut(&RefCell::borrow(self).begin_queue)
                .pop_front()
                .expect("Notification has not been put to the queue in time.");
            assert!(
                RefCell::borrow(self).rec.try_recv().is_err(),
                "Command has been sent to early."
            );
            RefCell::borrow_mut(self).begin += 1;
        }
        fn signal_end_cmd(&mut self) {
            RefCell::borrow_mut(self).end += 1;
        }
    }
    #[test]
    fn enqueue_cmd() {
        let (ser, rec) = mpsc::channel();
        let begin_queue = Rc::new(RefCell::new(VecDeque::new()));
        let end_ser: Sender<Box<dyn NotifyEnd<PhantomModel>>> = mpsc::channel().0;
        let emit = Rc::new(RefCell::new(TestEmitter {
            begin_queue: Rc::clone(&begin_queue),
            rec,
            begin: 0,
            end: 0,
        }));
        let diary = Diary::default();
        let mut c = Contact::new(
            Rc::clone(&emit),
            Arc::new(RwLock::new(Record::new(diary))),
            ser.clone(),
            begin_queue.clone(),
            end_ser.clone(),
        );
        c.enqueue(Undoable::from(CreateDream::new(
            Dream::from_oblig("Flying".to_string(), "Trübtraum".to_string()),
            NaiveDate::from_ymd(2019, 03, 02),
        )));

        assert_eq!(RefCell::borrow(&emit).begin, 1);
        assert_eq!(RefCell::borrow(&emit).end, 0);

        // Maybe in the future we will test for the exact command,
        // but currently the methods or traits necessary for that do not seem
        // to be available.
        assert!(
            RefCell::borrow_mut(&begin_queue).pop_front().is_none(),
            "Queue is not empty."
        );
        RefCell::borrow(&emit)
            .rec
            .try_recv()
            .expect("Narrator has not been sent.");
    }

    #[test]
    fn execute_cmd() {
        let (ser, rec) = mpsc::channel();
        let begin_queue = Rc::new(RefCell::new(VecDeque::new()));
        let (end_ser, _end_rec) = mpsc::channel();
        let emit = Rc::new(RefCell::new(TestEmitter {
            begin_queue: Rc::clone(&begin_queue),
            rec,
            begin: 0,
            end: 0,
        }));
        let diary = Diary::default();
        let mut c = Contact::new(
            Rc::clone(&emit),
            Arc::new(RwLock::new(Record::new(diary))),
            ser.clone(),
            begin_queue.clone(),
            end_ser.clone(),
        );
        c.execute(Undoable::from(CreateDream::new(
            Dream::from_oblig("Flying".to_string(), "Trübtraum".to_string()),
            NaiveDate::from_ymd(2019, 03, 02),
        )));

        assert_eq!(RefCell::borrow(&emit).begin, 1);
        assert_eq!(RefCell::borrow(&emit).end, 1);

        // Maybe in the future we will test for the exact command,
        // but currently the methods or traits necessary for that do not seem
        // to be available.
        assert!(
            RefCell::borrow_mut(&begin_queue).pop_front().is_none(),
            "Queue is not empty."
        );
        assert!(
            RefCell::borrow(&emit).rec.try_recv().is_err(),
            "Erroneously a narrator has been sent."
        )
    }

    #[test]
    fn nesting_simple() {
        let mut n = Nesting::new();
        assert_eq!(n, Nesting::default());
        assert_eq!(n.nest(), NotifyAction::Normal);
        assert_eq!(n, Nesting::Single);
        assert_eq!(n.unnest(), NotifyAction::Normal);
    }

    #[test]
    fn nesting_reset_model() {
        let mut n = Nesting::new();
        n.nest();
        assert_eq!(n.nest(), NotifyAction::ResetModel);
        assert_eq!(n, Nesting::ResetModel(1));
        assert_eq!(n.nest(), NotifyAction::Nothing);
        assert_eq!(n, Nesting::ResetModel(2));
        assert_eq!(n.unnest(), NotifyAction::Nothing);
        assert_eq!(n, Nesting::ResetModel(1));
        assert_eq!(n.unnest(), NotifyAction::Nothing);
        assert_eq!(n, Nesting::ResetModel(0));
        assert_eq!(n.unnest(), NotifyAction::ResetModel);
        assert_eq!(n, Nesting::Clean);
    }
}
