use std::collections::{HashMap, HashSet};
use std::convert::TryFrom;
use std::iter::FromIterator;
use std::sync::RwLock;

use chrono::{NaiveDate, NaiveTime};
use num_enum::{IntoPrimitive, TryFromPrimitive};

use black_walker_logic::elements::tag;
use black_walker_logic::model::DreamFormModel;
use black_walker_logic::operate::{
    dream::{CreateDream, SetDream},
    tag::CreateTag,
};
use black_walker_logic::parse;
use black_walker_logic::prelude::*;
use black_walker_logic::sync::prelude::*;
use black_walker_logic::{lock, stage_diary};

use super::night::NightQt;
use super::{BeginEndSignal, Contact};
use crate::execute;

use crate::interface::*;

#[repr(u8)]
#[derive(
    Debug,
    Clone,
    Copy,
    PartialEq,
    Eq,
    Hash,
    IntoPrimitive,
    TryFromPrimitive
)]
enum OpenType {
    New = 0x0,
    Read = 0x1,
    Write = 0x2,
}

impl Default for OpenType {
    fn default() -> Self {
        OpenType::New
    }
}

impl BeginEndSignal for DreamFormQtEmitter {
    fn signal_begin_cmd(&mut self) {
        self.begin_cmd_changed()
    }
    fn signal_end_cmd(&mut self) {
        self.end_cmd_changed()
    }
}

pub struct DreamFormQt {
    emit: DreamFormQtEmitter,
    id: Option<DreamID>,
    is_open: bool,
    date: NaiveDate,
    draft: Dream,
    tag: TagList,
    night_qt: NightQt,
    tagline: String,
    /// Stores the fully specified tags from the tagline.
    ///
    /// The first part contains a command ready to create the tag,
    /// the second part does to things: The `Option` indicates whether
    /// it as a direct tag of the dream or only a group specification
    /// (which shall only be created and not listed as a tag of the dream),
    /// and the `OccurMode` maybe contained by the Option indicates
    /// the strength of the tag (recall that this information cannot be
    /// stored in the `CreateTag` command since this should create the tag
    /// with only its basename.)
    known_tags: Vec<(CreateTag, Option<tag::OccurMode>)>,
    unknown_tag_model: TagList,
    cnt: Option<Contact<DreamFormQtEmitter>>,
}

impl DreamFormModel for DreamFormQt {
    fn begin_reset(&mut self) {}
    fn end_reset(&mut self) {
        self.id = None;
        self.date = chrono::Local::today().naive_local();
        self.emit.date_changed();
        self.draft = Dream::default();
        self.tag.initialize_with_standard_categories();
        self.tagline.clear();
        self.tagline.shrink_to_fit();
        self.known_tags.clear();
        self.known_tags.shrink_to_fit();
        self.unknown_tag_model.initialize_empty();
    }
    fn ids_changed(&mut self, changes: &[(DreamID, DreamID)]) {
        for (old_id, new_id) in changes {
            if self.id == Some(*old_id) {
                self.id = Some(*new_id);
                self.date = new_id.date;
                self.emit.date_changed();
                // This break is important so we do not fall in circles
                // of changing our id to id A, but then changing
                // id A to id B again, because A itself is contained
                // in `changes` as an old id.
                break;
            }
        }
    }
    fn dream_removed(&mut self, id: DreamID) {
        if self.id == Some(id) {
            self.id = None;
        }
    }
    fn give_id(&mut self, id: DreamID) {
        if self.is_open && self.id.is_none() {
            self.date = id.date;
            self.emit.date_changed();
            self.id = Some(id);
            self.emit.has_id_changed();
        }
    }
}

impl DreamFormQtTrait for DreamFormQt {
    fn new(
        emit: DreamFormQtEmitter,
        night_qt: NightQt,
        tag: TagList,
        unknown_tag_model: TagList,
    ) -> DreamFormQt {
        //let draft = Rc::new(RefCell::new(Dream::default()));
        DreamFormQt {
            emit,
            id: None,
            is_open: false,
            date: chrono::Local::today().naive_local(),
            draft: Dream::default(),
            tag,
            night_qt,
            tagline: String::new(),
            known_tags: Vec::new(),
            unknown_tag_model,
            cnt: None,
        }
    }
    fn emit(&mut self) -> &mut DreamFormQtEmitter {
        &mut self.emit
    }
    fn has_id(&self) -> bool {
        self.id.is_some()
    }
    fn clear(&mut self) {
        self.is_open = true;
        self.id = None;
        self.date = chrono::Local::today().naive_local();
        self.draft = Dream::default();
        self.tag.initialize_with_standard_categories();
    }
    fn close(&mut self) {
        self.is_open = false;
        self.id = None;
    }
    fn date(&self) -> String {
        self.date.format("%Y-%m-%d").to_string()
    }
    fn set_date(&mut self, value: String) {
        if let Ok(date) = NaiveDate::parse_from_str(&value, "%Y-%m-%d") {
            self.date = date;
            self.night_qt.open_night_direct(self.date);
            self.emit.date_changed();
        }
    }
    fn kind(&self) -> String {
        self.draft
            .get_entry_text("Art")
            .cloned()
            .unwrap_or_default()
    }
    fn set_kind(&mut self, value: String) {
        self.draft.set_entry_text("Art", value);
        self.emit.kind_changed();
    }
    fn name(&self) -> &str {
        &self.draft.name
    }
    fn set_name(&mut self, value: String) {
        self.draft.name = value;
        self.emit.name_changed();
    }
    fn text(&self) -> &str {
        &self.draft.text
    }
    fn set_text(&mut self, value: String) {
        self.draft.text = value;
    }
    fn notes_note(&self, year: i32, month: u32, day: u32) -> String {
        self.draft
            .notes
            .get(&NaiveDate::from_ymd(year, month, day))
            .map(|note| note.note.clone())
            .unwrap_or_default()
    }
    fn notes_place(&self, year: i32, month: u32, day: u32) -> String {
        self.draft
            .notes
            .get(&NaiveDate::from_ymd(year, month, day))
            .map(|note| note.place.clone())
            .unwrap_or_default()
    }
    fn notes_time(&self, year: i32, month: u32, day: u32) -> String {
        self.draft
            .notes
            .get(&NaiveDate::from_ymd(year, month, day))
            .map(|note| note.time.format("%H:%M:%S").to_string())
            .unwrap_or_default()
    }
    fn set_notes(
        &mut self,
        notes: String,
        place: String,
        hour: u32,
        minute: u32,
        second: u32,
        year: i32,
        month: u32,
        day: u32,
    ) {
        let date = NaiveDate::from_ymd(year, month, day);
        let notes = notes.trim();
        let place = place.trim();
        if notes.is_empty() {
            self.draft.notes.remove(&date);
            self.emit.nr_notes_changed();
        } else {
            let nr_notes_changed = !self.draft.notes.contains_key(&date);
            self.draft.notes.insert(
                date,
                Note {
                    time: NaiveTime::from_hms(hour, minute, second),
                    place: place.to_owned(),
                    note: notes.to_owned(),
                },
            );
            if nr_notes_changed {
                self.emit.nr_notes_changed();
            }
        }
    }
    fn nr_notes(&self) -> u32 {
        self.draft.notes.len() as u32
    }
    fn note_date(&self, index: u32) -> String {
        self.draft
            .notes
            .keys()
            .nth(index as usize)
            .map(|date| date.format("%Y-%m-%d").to_string())
            .unwrap_or_default()
    }
    fn tag(&self) -> &TagList {
        &self.tag
    }
    fn tag_mut(&mut self) -> &mut TagList {
        &mut self.tag
    }
    fn night_qt(&self) -> &NightQt {
        &self.night_qt
    }
    fn night_qt_mut(&mut self) -> &mut NightQt {
        &mut self.night_qt
    }
    fn unknown_tag_model(&self) -> &TagList {
        &self.unknown_tag_model
    }
    fn unknown_tag_model_mut(&mut self) -> &mut TagList {
        &mut self.unknown_tag_model
    }
    fn entry_text(&self, category: String) -> String {
        self.draft
            .get_entry_text(&category)
            .cloned()
            .unwrap_or_default()
    }
    fn entry_time(&self, category: String) -> String {
        self.draft
            .get_entry_time(&category)
            .map(|t| t.format("%H:%M:%S").to_string())
            .unwrap_or_default()
    }
    fn entry_date(&self, category: String) -> String {
        self.draft
            .get_entry_date(&category)
            .map(|t| t.format("%Y-%m-%d").to_string())
            .unwrap_or_default()
    }
    fn entry_uint(&self, category: String) -> i32 {
        self.draft
            .get_entry_uint(&category)
            .map(|a| a as i32)
            .unwrap_or(-1)
    }
    fn entry_int(&self, category: String) -> i32 {
        self.draft.get_entry_int(&category).unwrap_or_default()
    }
    fn entry_bool(&self, category: String) -> bool {
        self.draft.get_entry_bool(&category)
    }
    fn set_entry_text(&mut self, category: String, value: String) {
        self.draft.set_entry_text(&category, value);
    }
    fn set_entry_time(&mut self, category: String, hour: u32, minute: u32, second: u32) {
        self.draft
            .set_entry_time(&category, NaiveTime::from_hms(hour, minute, second));
    }
    fn set_entry_date(&mut self, category: String, year: i32, month: u32, day: u32) {
        self.draft
            .set_entry_date(&category, NaiveDate::from_ymd(year, month, day));
    }
    fn set_entry_uint(&mut self, category: String, value: i32) {
        if let Ok(value) = u32::try_from(value) {
            self.draft.set_entry_uint(&category, value);
        } else {
            self.draft.remove_entry_uint(&category);
        }
    }
    fn set_entry_int(&mut self, category: String, value: i32) {
        self.draft.set_entry_int(&category, value);
    }
    fn set_entry_bool(&mut self, category: String, value: bool) {
        self.draft.set_entry_bool(&category, value);
    }
    fn add_tag(&mut self, category: String, tagname: String) {
        self.draft.add_tag(&category, tagname.into());
    }
    fn set_tag_line(&mut self, text: String) {
        self.tagline = text
    }
    /// Evaluates the input `self` has recieved and sets up a draft of the dream
    /// to create, as well as collecting tags and groups that are not yet
    /// specified.
    fn interpret_dream(&mut self) {
        self.draft.set_all_tags(HashMap::new());
        let mut unknown_tags: Vec<(Option<String>, TagSpec)> = Vec::new();
        let mut unknown_groups: Vec<(Option<String>, TagSpec)> = Vec::new();
        {
            let tagforest = self.cnt.as_ref().unwrap().diary.tag.read().unwrap();

            // Evaluate the tags, dividing them into
            // fully specified ones and unknown ones.
            let tags_in_fields = self.tag.get_tags();
            for (category, tags) in tags_in_fields {
                for tagname in tags {
                    if tagforest
                        .get(&category)
                        .map(|tree| tree.node_exists(tagname.base_name()))
                        .unwrap_or(false)
                    {
                        self.draft.add_tag(&category, tagname);
                    } else {
                        unknown_tags.push((Some(category.clone()), tagname));
                    }
                }
            }

            // Do the same to the tags in the tagline.
            let (tags_in_tagline, groups_in_tagline) =
                parse::tag::specified_tags(&self.tagline, parse::EnterType::TAGSWITHGROUP);
            for tag_in_tagline in tags_in_tagline {
                if let (Some(category), tagname, Some(groupname)) = tag_in_tagline {
                    if tagforest
                        .get(&category)
                        .map(|tree| tree.node_exists(tagname.base_name()))
                        .unwrap_or(false)
                    {
                        self.draft.add_tag(&category, tagname);
                    } else {
                        self.known_tags.push((
                            CreateTag::new(category, tagname.base_name().to_owned(), groupname),
                            Some(tagname.strength()),
                        ));
                    }
                } else if let Some(category) = tag::forest_contains_tag_in_one_category(
                    &tagforest,
                    tag_in_tagline.1.base_name(),
                ) {
                    self.draft.add_tag(category, tag_in_tagline.1);
                } else {
                    unknown_tags.push((tag_in_tagline.0, tag_in_tagline.1));
                }
            }
            for group_in_tagline in groups_in_tagline {
                if let (Some(category), tagname, Some(groupname)) = group_in_tagline {
                    if !tagforest
                        .get(&category)
                        .map(|tree| tree.node_exists(tagname.base_name()))
                        .unwrap_or(false)
                    {
                        self.known_tags.push((
                            CreateTag::new(category, tagname.into_base_name(), groupname),
                            None,
                        ));
                    }
                } else if tag::forest_contains_tag_in_one_category(
                    &tagforest,
                    group_in_tagline.1.base_name(),
                )
                .is_none()
                {
                    unknown_groups.push((group_in_tagline.0, group_in_tagline.1));
                }
            }
        }
        self.unknown_tag_model
            .set_unknown_tags(unknown_tags, unknown_groups);
    }
    /// Initializes the dreamform with the dream specified
    /// by the given date and `idx`.
    ///
    /// Returns whether the dream could be loaded correctly.
    fn open_dream(&mut self, year: i32, month: u32, day: u32, idx: u32) -> bool {
        let dreamlist = self.cnt.as_ref().unwrap().diary.dl.read().unwrap();

        let date = NaiveDate::from_ymd(year, month, day);
        self.is_open = if let Some(dream) = dreamlist.get(DreamID::new(date, idx)).cloned() {
            self.id = Some(dream.id());
            self.date = date;
            self.draft = dream.d;
            self.tag.set_tags_of_dream(self.draft.get_all_tags());
            self.tagline.clear();
            self.known_tags.clear();
            self.unknown_tag_model.initialize_empty();
            true
        } else {
            false
        };

        self.is_open
    }
    fn save_dream(&mut self) {
        for known_tag in self.known_tags.drain(..) {
            if let Some(strength) = known_tag.1 {
                self.draft.add_tag(
                    known_tag.0.category(),
                    TagSpec::new(known_tag.0.name().to_owned(), strength),
                );
            }
            execute!(self.cnt, known_tag.0);
        }

        if let Some(id) = self.id {
            let cmd = if self.date == id.date {
                Undoable::from(SetDream::new(id, self.draft.clone()))
            } else {
                Undoable::from(SetDream::with_new_date(id, self.date, self.draft.clone()))
            };
            execute!(self.cnt, cmd);
        } else {
            let cmd = Undoable::from(CreateDream::new(self.draft.clone(), self.date));
            execute!(self.cnt, cmd);
        }
    }
    #[doc(hidden)]
    fn begin_cmd(&self) -> bool {
        true
    }
    #[doc(hidden)]
    fn end_cmd(&self) -> bool {
        true
    }
}

impl DreamFormQt {
    pub fn md(&mut self) -> &mut Contact<DreamFormQtEmitter> {
        self.cnt.as_mut().unwrap()
    }
    pub fn set_md(&mut self, md: Contact<DreamFormQtEmitter>) {
        self.tag.set_diary(md.diary.clone());
        self.unknown_tag_model.set_diary(md.diary.clone());
        self.night_qt.set_contact(md.pipe.clone());
        self.cnt = Some(md);
    }
}

#[derive(Default, Clone, PartialEq, Eq, Hash)]
struct TagListItem {
    category: Option<String>,
    value: String,
    is_group: bool,
}

impl TagListItem {
    pub fn new(category: String) -> Self {
        TagListItem {
            category: Some(category),
            value: String::new(),
            is_group: false,
        }
    }
    pub fn category_or_empty(&self) -> &str {
        if let Some(cat) = &self.category {
            cat
        } else {
            ""
        }
    }
}

pub struct TagList {
    emit: TagListEmitter,
    model: TagListList,
    diary: Option<Diary>,
    list: Vec<TagListItem>,
}

impl TagList {
    pub fn initialize_empty(&mut self) {
        self.model.begin_reset_model();
        self.list.clear();
        /*for standard_tag_category in self.query_settings(|set| set.standard_tag_categories().clone()) {
            self.list.push(TagListItem::new(standard_tag_category));
        }*/
        self.model.end_reset_model();
    }
    pub fn initialize_with_standard_categories(&mut self) {
        self.model.begin_reset_model();
        self.list.clear();
        for standard_tag_category in
            self.query_settings(|set| set.standard_tag_categories().clone())
        {
            self.list.push(TagListItem::new(standard_tag_category));
        }
        self.model.end_reset_model();
    }
    pub fn set_diary(&mut self, diary: Diary) {
        self.diary = Some(diary);
    }
    fn query_settings<F: FnOnce(&GS) -> T, T>(&self, func: F) -> T {
        let set = RwLock::read(&self.diary.as_ref().expect("Contact has not been set").gl)
            .expect("Worker thread has hung up");
        func(&set)
    }
    pub fn get_tags(&self) -> HashMap<String, HashSet<TagSpec>> {
        let mut res = HashMap::new();
        for item in self.list.iter() {
            res.insert(
                item.category_or_empty().to_string(),
                HashSet::from_comma_sep(&item.value),
            );
        }
        res
    }
    pub fn set_tags_of_dream(&mut self, tags: &HashMap<String, HashSet<TagSpec>>) {
        self.model.begin_reset_model();
        self.list.clear();
        let categories = Vec::from_iter(tags.keys().cloned());
        let categories =
            self.query_settings(|set| set.standard_categories.sort_tag_categories(categories));
        let mut diary = self.diary.as_mut().unwrap();
        let mut stage = stage_diary!(diary; TAG);
        let tagforest = lock!(stage; TagForest).0.unwrap();
        for category in categories {
            self.list.push(TagListItem {
                value: if let (Some(tags), Some(tagtree)) =
                    (tags.get(&category), tagforest.get(&category))
                {
                    let mut tags = Vec::from_iter(tags);
                    tag::sort_by_relevance(tagtree, &mut tags);
                    tags.comma_sep()
                } else {
                    String::new()
                },
                category: Some(category),
                is_group: false,
            });
        }
    }
    pub fn set_unknown_tags(
        &mut self,
        tags: Vec<(Option<String>, TagSpec)>,
        groups: Vec<(Option<String>, TagSpec)>,
    ) {
        self.model.begin_reset_model();
        self.list.clear();
        for (category, tag) in tags {
            self.list.push(TagListItem {
                value: tag.to_string(),
                category,
                is_group: false,
            });
        }
        for (category, group) in groups {
            self.list.push(TagListItem {
                value: group.to_string(),
                category,
                is_group: true,
            });
        }
        self.model.end_reset_model();
    }
}

impl TagListTrait for TagList {
    fn new(emit: TagListEmitter, model: TagListList) -> TagList {
        TagList {
            emit,
            model,
            diary: None,
            list: Vec::new(),
        }
    }
    fn emit(&mut self) -> &mut TagListEmitter {
        &mut self.emit
    }
    fn row_count(&self) -> usize {
        self.list.len()
    }
    fn category(&self, index: usize) -> &str {
        &self
            .list
            .get(index)
            .map(|a| a.category_or_empty())
            .unwrap_or_default()
    }
    fn set_category(&mut self, index: usize, v: String) -> bool {
        if let Some(item) = self.list.get_mut(index) {
            item.category = if v.is_empty() { None } else { Some(v) };
            true
        } else {
            false
        }
    }
    fn value(&self, index: usize) -> &str {
        &self
            .list
            .get(index)
            .map(|a| a.value.as_str())
            .unwrap_or_default()
    }
    fn is_group(&self, index: usize) -> bool {
        self.list.get(index).map(|a| a.is_group).unwrap_or(false)
    }
    fn set_value(&mut self, index: usize, v: String) -> bool {
        if let Some(item) = self.list.get_mut(index) {
            item.value = v;
            true
        } else {
            false
        }
    }
    fn push_row(&mut self) {
        self.model
            .begin_insert_rows(self.list.len(), self.list.len() + 1);
        self.list.push(TagListItem::default());
        self.model.end_insert_rows();
    }
    fn pop_row(&mut self) {
        if !self.list.is_empty() {
            self.model.begin_remove_rows(self.list.len() - 1, 1);
            self.list.pop();
            self.model.end_remove_rows();
        }
    }
    fn append_group(&mut self, category: String, groupname: String) {
        self.model.begin_insert_rows(self.list.len(), 1);
        self.list.push(TagListItem {
            value: groupname,
            category: if category.is_empty() {
                None
            } else {
                Some(category)
            },
            is_group: true,
        });
        self.model.end_insert_rows();
    }
}
