//! Contains the [`DreamListMaster`] as the
//! [`DreamListModel`](black_walker_logic::model::DreamListModel) for the
//! Qt-GUI written in C++.
//!
//! The `DreamListMaster` administrates the
//! [`DreamList`](black_walker_logic::DreamList)
//! and [`NightList`](black_walker_logic::NightList) and especially
//! takes care of the models [`DreamListQt`] and [`NightListQt`] displaying it.

use std::iter::FromIterator;

use chrono::NaiveDate;

use black_walker_logic::operate::dream::*;
use black_walker_logic::operate::night::*;
use black_walker_logic::prelude::*;

use super::{Contact, Nesting, NotifyAction};

use crate::interface::*;
use crate::{execute, impl_begin_end_signals};

pub mod dream_list;
pub mod night_list;

pub use dream_list::DreamListQt;
pub use night_list::NightListQt;

impl_begin_end_signals! {DreamListMasterEmitter}

/// The governor over the [`DreamListQt`] and the [`NightListQt`].
///
/// The dreamlist and the nightlst are currently treated a bit asymmertically:
/// The dreamlist's fields are usually mutated directly by the `DreamListMaster`,
/// while it calls dedicated functions on the nightlist in order to mutate it.
pub struct DreamListMaster {
    emit: DreamListMasterEmitter,
    dream_list_qt: DreamListQt,
    night_list_qt: NightListQt,
    cnt: Option<Contact<DreamListMasterEmitter>>,
}

impl DreamListModel for DreamListMaster {
    fn begin_reset(&mut self) {
        self.dream_list_qt.model.begin_reset_model();
        self.night_list_qt.model.begin_reset_model();
    }
    fn end_reset(&mut self) {
        self.initialize();
        self.night_list_qt.model.end_reset_model();
    }
    fn begin_create_dream(&mut self, date: NaiveDate) {
        let (total_index, idxofnewdream) =
            Self::indices_of_new_dream(&self.dream_list_qt.indextoid, date);
        self.dream_list_qt.notify_begin(
            |list| list.begin_insert_rows(total_index, total_index),
            DreamListQtList::end_insert_rows,
        );
        self.dream_list_qt
            .indextoid
            .insert(total_index, DreamID::new(date, idxofnewdream));
        // If the dream is the first one in this night and the night has
        // not already been created as a night without dreams,
        // a new night has to bee inserted
        if idxofnewdream == 0 && !self.night_list_qt.nights.contains(&date.into()) {
            self.begin_create_night_pure(date)
        }
    }
    fn begin_create_night_pure(&mut self, date: NaiveDate) {
        if let Some((newnightidx, _)) = self
            .night_list_qt
            .nights
            .iter()
            .enumerate()
            .find(|&(_, &x)| x > date)
        {
            self.night_list_qt
                .model
                .begin_insert_rows(newnightidx, newnightidx);
            self.night_list_qt.nights.insert(newnightidx, date.into());
            self.night_list_qt.model.end_insert_rows();
        } else {
            self.night_list_qt.model.begin_insert_rows(
                self.night_list_qt.nights.len(),
                self.night_list_qt.nights.len(),
            );
            self.night_list_qt.nights.push(date.into());
            self.night_list_qt.model.end_insert_rows();
        }
    }
    fn end_create_dream(&mut self, _date: NaiveDate) {
        self.dream_list_qt
            .notify_end(DreamListQtList::end_insert_rows);
    }
    fn end_create_night_pure(&mut self, _date: NaiveDate) {}
    fn begin_remove_dream(&mut self, id: DreamID) {
        let index_of_id;
        if let Some(index) = Self::index_of_id(&self.dream_list_qt.indextoid, id) {
            self.dream_list_qt.notify_begin(
                |list| list.begin_remove_rows(index, index),
                DreamListQtList::end_remove_rows,
            );
            index_of_id = Some(index);
        } else {
            index_of_id = None;
        }

        // The index that must be removed is the last index in the night of `id`.
        // This is simply the index, that a newly inserted dream in that night would have,
        // minus one.
        let mut index_to_remove =
            Self::indices_of_new_dream(&self.dream_list_qt.indextoid, id.date).0;
        if index_to_remove > 0 {
            index_to_remove -= 1;
        // This would mean a new dream is inserted on the top position, but
        // that can only happen if the new dream has a yet non-existing date.
        // So this is a dead branch.
        } else {
            return;
        }
        self.dream_list_qt.indextoid.remove(index_to_remove);

        if let (Some(index), Some(nightindex)) = (
            index_of_id,
            self.night_list_qt.index_of_night(id.date.into()),
        ) {
            if self
                .night_list_qt
                .query_night(id.date.into(), |_| Some(()))
                .is_none()
                && self.nightindex_is_to_remove(id, index)
            {
                self.night_list_qt
                    .model
                    .begin_remove_rows(nightindex, nightindex);
                self.night_list_qt.nights.remove(nightindex);
                self.night_list_qt.model.end_remove_rows();
            }
        }
    }
    fn end_remove_dream(&mut self, id: DreamID) {
        self.dream_list_qt
            .notify_end(DreamListQtList::end_remove_rows);
        if let Some(nightindex) = self.night_list_qt.index_of_night(id.date.into()) {
            self.night_list_qt
                .model
                .data_changed(nightindex, nightindex);
        }
    }
    fn begin_remove_night_information(&mut self, date: NaiveDate) {
        if self
            .dream_list_qt
            .query(|dreamlist| dreamlist.dream_count(date) == 0)
        {
            if let Some(nightindex) = self.night_list_qt.index_of_night(date.into()) {
                self.night_list_qt
                    .model
                    .begin_remove_rows(nightindex, nightindex);
                // Won't panic because nighindex is valid.
                self.night_list_qt.nights.remove(nightindex);
                self.night_list_qt.model.end_remove_rows();
            }
        }
    }
    fn end_remove_night_information(&mut self, _date: NaiveDate) {}
    /// ## Panics
    ///
    /// This function may panic if the id `from` does not exist or
    /// if no dream can be moved to the id `to`.
    fn begin_move_dream(&mut self, from: DreamID, newdate: NaiveDate, newidx: Option<u32>) {
        let to = match newidx {
            Some(idx) => DreamID::new(newdate, idx),
            None => DreamID::new(
                newdate,
                Self::indices_of_new_dream(&self.dream_list_qt.indextoid, newdate).1,
            ),
        };
        if let Some((oldindex, newindex)) =
            Self::move_ids(&mut self.dream_list_qt.indextoid, from, to)
        {
            if oldindex != newindex {
                self.dream_list_qt.notify_begin(
                    |list| {
                        list.begin_move_rows(
                            oldindex,
                            oldindex,
                            // `newindex` is the index of `to` in the new indextoid,
                            // but Qt wants to know the indices in the old indextoid,
                            // so if they differ, they must be corrected.
                            newindex + if from < to { 1 } else { 0 },
                        )
                    },
                    DreamListQtList::end_move_rows,
                );
            }
            // Instead of somehow moving and changig the night to the date
            // of `to`, simply remove the old one and insert the new one if necessary.
            if Self::nightindex_is_to_remove(
                &self,
                from,
                // `oldindex` is the index of `from` in the old indextoid,
                // `nightindex_to_remove` needs the index which `from` would have
                // in the current `indextoid`, so it must be corrected.
                oldindex + if to < from { 1 } else { 0 },
            ) && self
                .night_list_qt
                .query_night(from.date.into(), |_| Some(()))
                .is_none()
            {
                if let Some(nightindex_to_remove) =
                    self.night_list_qt.index_of_night(from.date.into())
                {
                    self.night_list_qt
                        .model
                        .begin_remove_rows(nightindex_to_remove, nightindex_to_remove);
                    self.night_list_qt.nights.remove(nightindex_to_remove);
                    self.night_list_qt.model.end_remove_rows();
                }
            }

            if let Some(nightindex_to_insert) = Self::nightindex_to_insert(&self, from) {
                if !self.night_list_qt.nights.contains(&newdate.into()) {
                    self.night_list_qt
                        .model
                        .begin_insert_rows(nightindex_to_insert, nightindex_to_insert);
                    self.night_list_qt
                        .nights
                        .insert(nightindex_to_insert, to.date.into());
                    self.night_list_qt.model.end_insert_rows();
                }
            }
        }
    }
    fn end_move_dream(&mut self, oldid: DreamID, newdate: NaiveDate, _newidx: Option<u32>) {
        self.dream_list_qt
            .notify_end(DreamListQtList::end_move_rows);
        if let Some(nightindex) = self.night_list_qt.index_of_night(oldid.date.into()) {
            self.night_list_qt
                .model
                .data_changed(nightindex, nightindex);
        }
        if let Some(nightindex) = self.night_list_qt.index_of_night(newdate.into()) {
            self.night_list_qt
                .model
                .data_changed(nightindex, nightindex);
        }
    }
    fn begin_set_dream(&mut self, _id: DreamID) {}
    fn end_set_dream(&mut self, id: DreamID) {
        let index = DreamListMaster::index_of_id(&self.dream_list_qt.indextoid, id);
        if let Some(index) = index {
            self.dream_list_qt.model.data_changed(index, index);
        }
    }
}

impl DreamListMaster {
    /// Returns the index on which `id` can be found in
    /// `indextoid`.
    ///
    /// `None` is returned if the index cannot be found.
    fn index_of_id(indextoid: &[DreamID], id: DreamID) -> Option<usize> {
        indextoid
            .iter()
            .enumerate()
            .find(|&(_, &x)| x == id)
            .map(|(index, _)| index)
    }

    /// Returns the position which a newly created dream in `date`
    /// will have.
    ///
    /// The dream will be created as the last dream in the night given by `date`,
    /// so its id will be composed of `date` and the number of dreams
    /// currently in `date`.
    ///
    /// The first part of the result of this function is the "total" index
    /// the dream will have in [`indextoid`](DreamListQt::indextoid). The second
    /// part will be the second part of its id, i.e. its index in `date`.
    fn indices_of_new_dream(indextoid: &[DreamID], date: NaiveDate) -> (usize, u32) {
        let mut total_index = indextoid.len();
        let mut lastidxindate = None;
        for (i, it) in indextoid.iter().enumerate() {
            if it.date == date {
                lastidxindate = Some(it.idx);
            }
            if it.date < date {
                total_index = i;
                break;
            }
        }
        let idxofnewdream = match lastidxindate {
            Some(i) => i + 1,
            None => 0,
        };
        (total_index, idxofnewdream)
    }
    /// Checks whether a night for the given date exists in the `night_list_qt`.
    ///
    /// If a dream in a new night is inserted, maybe its
    /// night must also be created. This function checks whether there
    /// is already an entry for this night in the `night_list_qt`.
    /// If not, it returns the index at which the night must be inserted.
    fn nightindex_to_insert(&self, id: DreamID) -> Option<usize> {
        if !self.night_list_qt.nights.contains(&id.date.into()) {
            self.night_list_qt
                .nights
                .iter()
                .enumerate()
                .find(|&(_, &x)| x > id.date)
                .map(|(nightindex, _)| nightindex)
        } else {
            None
        }
    }
    /// Checks whether an id with a given date exists.
    ///
    /// If the last dream of a night is removed, its night must also be removed
    /// from the nightlist. This function checks whether there still is an id
    /// in [`indextoid`](DreamListQt::indextoid)
    /// with the same date as `id`.
    /// If _false_, it returns the index of the corresponding night
    /// in [`NightListQt::nights`], otherwise it returns `None`.
    ///
    /// In order to save the time needed to search for `id` in
    /// [`indextoid`](DreamListQt::indextoid), it wants its index to be passed
    /// as the parameter `index`.
    ///
    /// This function is intended to be used _after_ a dream has been
    /// removed in order to find out whether a night has to be removed, too.
    fn nightindex_is_to_remove(&self, id: DreamID, index: usize) -> bool {
        (if index >= 1 {
            self.dream_list_qt
                .indextoid
                .get(index - 1)
                .map(|x| x.date != id.date)
                .unwrap_or(true)
        } else {
            true
        }) && self
            .dream_list_qt
            .indextoid
            .get(index)
            .map(|x| x.date != id.date)
            .unwrap_or(true)
            && self
                .dream_list_qt
                .indextoid
                .get(index + 1)
                .map(|x| x.date != id.date)
                .unwrap_or(true)
    }
    /// Removes the id `from` and inserts id `to` on the correct position.
    ///
    /// `indextoid` is mutated in way like when the id `from` is changed
    /// to `to` and moved to the new corresponding position. To and from
    /// are unequal, this function returns the positions `from` had
    /// in the old `indextoid` (before mutating it) and `to` has
    /// now in the new `indextoid`.
    ///
    /// If `to == from`, no move takes place so `None` is returned.
    ///
    /// ## Panics
    ///
    /// This function may panic if the id `from` does not exist or
    /// if no dream can be moved to the id `to`.
    fn move_ids(
        indextoid: &mut Vec<DreamID>,
        from: DreamID,
        to: DreamID,
    ) -> Option<(usize, usize)> {
        if to != from {
            let oldindex = Self::index_of_id(indextoid, from)?;
            // If the move takes place in the same date, `indextoid` must not be mutated
            // because only the underlying dreams change, but not the existing
            // ids.
            //
            // Otherwise, from the date of `from` the id with the highest idx
            // must be removed, as ids following `from` are decremented anyway
            // to close the gap.
            //
            // The insertion to be done is exactly like the insertion necessary
            // when a new dream is inserted at the date of `to`.
            if to.date != from.date {
                let to_remove = oldindex
                    + indextoid[oldindex..]
                        .iter()
                        .enumerate()
                        .find(|(_, id)| id.date < from.date)
                        .map(|(i, _)| i)
                        .unwrap_or(indextoid[oldindex..].len())
                    - 1;
                indextoid.remove(to_remove);
                let (index_to_insert, idx_to_insert) =
                    Self::indices_of_new_dream(indextoid, to.date);
                indextoid.insert(index_to_insert, DreamID::new(to.date, idx_to_insert));
            }
            let newindex = Self::index_of_id(indextoid, to)
                // Won't panic because the insertion in the line above ensures
                // that the id `to` exists in indextoid.
                .expect("Id to which Dream should be moved has not been inserted.");
            Some((oldindex, newindex))
        } else {
            None
        }
    }
    /// Returns the underlying `Contact`.
    pub fn contact(&mut self) -> &mut Contact<DreamListMasterEmitter> {
        self.cnt.as_mut().unwrap()
    }
    pub fn set_contact(&mut self, con: Contact<DreamListMasterEmitter>) {
        let dream_list_qt_emitter = self.dream_list_qt.emit.clone();
        self.dream_list_qt.cnt = Some(Contact::from_pipe(dream_list_qt_emitter, con.pipe.clone()));
        self.night_list_qt.diary = Some(con.diary.clone());
        self.cnt = Some(con);
        self.initialize();
    }
    /// Populates [`DreamListQt::indextoid`] and calls [`NightListQt::initialize`] on
    /// the nightlist.
    fn initialize(&mut self) {
        self.dream_list_qt.model.begin_reset_model();
        self.dream_list_qt.indextoid = Vec::from_iter(
            self.cnt
                .as_ref()
                .unwrap()
                .diary
                .dl
                .read()
                .expect("Given Contact contains a poisoned Diary!")
                .get_list()
                .keys()
                .copied(),
        );
        self.night_list_qt.initialize();
        self.dream_list_qt.model.end_reset_model();
    }
}

impl DreamListMasterTrait for DreamListMaster {
    fn new(
        emit: DreamListMasterEmitter,
        dream_list_qt: DreamListQt,
        night_list_qt: NightListQt,
    ) -> Self {
        DreamListMaster {
            emit,
            dream_list_qt,
            night_list_qt,
            cnt: None,
        }
    }
    fn emit(&mut self) -> &mut DreamListMasterEmitter {
        &mut self.emit
    }
    /// Deletes the dream displayed at index `idx`.
    fn delete_dream(&mut self, idx: i32) {
        if let Some(&id) = self.dream_list_qt.indextoid.get(idx as usize) {
            execute!(self.cnt, RemoveDream::new(id));
        }
    }
    /// Deletes meta information of the night under index `idx`.
    fn delete_night_information(&mut self, idx: i32) {
        if let Some(&night) = self.night_list_qt.nights.get(idx as usize) {
            execute!(self.cnt, SetNight::remove_night(night.into()))
        }
    }
    fn dream_list_qt(&self) -> &DreamListQt {
        &self.dream_list_qt
    }
    fn dream_list_qt_mut(&mut self) -> &mut DreamListQt {
        &mut self.dream_list_qt
    }
    fn night_list_qt(&self) -> &NightListQt {
        &self.night_list_qt
    }
    fn night_list_qt_mut(&mut self) -> &mut NightListQt {
        &mut self.night_list_qt
    }
    #[doc(hidden)]
    fn begin_cmd(&self) -> bool {
        true
    }
    #[doc(hidden)]
    fn end_cmd(&self) -> bool {
        true
    }
}

#[cfg(test)]
mod tests {
    use chrono::NaiveDate;

    use super::*;

    #[test]
    fn dreamlist_find_index() {
        let indextoid = vec![
            DreamID::new(NaiveDate::from_ymd(2000, 01, 04), 0),
            DreamID::new(NaiveDate::from_ymd(2000, 01, 02), 0),
            DreamID::new(NaiveDate::from_ymd(2000, 01, 02), 1),
            DreamID::new(NaiveDate::from_ymd(2000, 01, 02), 2),
            DreamID::new(NaiveDate::from_ymd(2000, 01, 01), 0),
            DreamID::new(NaiveDate::from_ymd(2000, 01, 01), 1),
        ];
        for i in 0..indextoid.len() {
            assert_eq!(
                DreamListMaster::index_of_id(&indextoid, indextoid[i]).expect(&format!(
                    "Coud not find any index when searching for index {} ({})",
                    i, indextoid[i]
                )),
                i,
                "Could not find index {}",
                i
            );
        }
        assert!(
            DreamListMaster::index_of_id(
                &indextoid,
                DreamID::new(NaiveDate::from_ymd(2001, 01, 01), 0)
            )
            .is_none(),
            "Incorrectly found a date over all other dates."
        );
        assert!(
            DreamListMaster::index_of_id(
                &indextoid,
                DreamID::new(NaiveDate::from_ymd(2000, 01, 03), 0)
            )
            .is_none(),
            "Incorrectly found a nonexisting date in the middle."
        );
        assert!(
            DreamListMaster::index_of_id(
                &indextoid,
                DreamID::new(NaiveDate::from_ymd(1999, 01, 01), 0)
            )
            .is_none(),
            "Incorrectly found a date below all other dates."
        );
    }
    #[test]
    fn indices_of_new_dream() {
        let dates = [
            NaiveDate::from_ymd(2000, 01, 03),
            NaiveDate::from_ymd(2000, 01, 02),
            NaiveDate::from_ymd(2000, 01, 02),
            NaiveDate::from_ymd(2000, 01, 02),
            NaiveDate::from_ymd(2000, 01, 01),
            NaiveDate::from_ymd(2000, 01, 01),
            NaiveDate::from_ymd(1999, 01, 01),
            NaiveDate::from_ymd(1999, 01, 01),
        ];
        let idxes = vec![0, 0, 1, 2, 0, 1, 0, 1];
        let mut indextoid = Vec::new();
        for (date, idx) in dates.iter().zip(idxes.into_iter()) {
            indextoid.push(DreamID::new(*date, idx));
        }
        let newdreamidxes = [1, 3, 3, 3, 2, 2, 2, 2];
        let newdreamtotalindexes = [1, 4, 4, 4, 6, 6, 8, 8];
        for i in 0..indextoid.len() {
            assert_eq!(
                DreamListMaster::indices_of_new_dream(&indextoid, dates[i]),
                (newdreamtotalindexes[i], newdreamidxes[i]),
                "Could not find new index of dream in existing date {} (run {})",
                dates[i],
                i
            );
        }
        assert_eq!(
            DreamListMaster::indices_of_new_dream(&indextoid, NaiveDate::from_ymd(1999, 01, 02)),
            (6, 0),
            "Could not find new index of dream in nonexisting date {}",
            NaiveDate::from_ymd(1999, 01, 02)
        );
        assert_eq!(
            DreamListMaster::indices_of_new_dream(&indextoid, NaiveDate::from_ymd(1998, 01, 01)),
            (8, 0),
            "Could not find new index of dream in nonexisting date {}",
            NaiveDate::from_ymd(1998, 01, 01)
        );
        assert_eq!(
            DreamListMaster::indices_of_new_dream(&indextoid, NaiveDate::from_ymd(2001, 01, 01)),
            (0, 0),
            "Could not find new index of dream in nonexisting date {}",
            NaiveDate::from_ymd(2001, 01, 01)
        );
    }

    #[test]
    fn move_ids() {
        let dates = [
            NaiveDate::from_ymd(2000, 01, 03),
            NaiveDate::from_ymd(2000, 01, 02),
            NaiveDate::from_ymd(2000, 01, 02),
            NaiveDate::from_ymd(2000, 01, 02),
            NaiveDate::from_ymd(2000, 01, 01),
            NaiveDate::from_ymd(2000, 01, 01),
            NaiveDate::from_ymd(1999, 02, 01),
            NaiveDate::from_ymd(1999, 02, 01),
            NaiveDate::from_ymd(1999, 02, 01),
            NaiveDate::from_ymd(1999, 02, 01),
            NaiveDate::from_ymd(1999, 01, 01),
            NaiveDate::from_ymd(1999, 01, 01),
        ];
        let idxes = vec![0, 0, 1, 2, 0, 1, 0, 1, 2, 3, 0, 1];

        let to_move_to = [
            DreamID::new(NaiveDate::from_ymd(2000, 01, 02), 0),
            DreamID::new(NaiveDate::from_ymd(2000, 01, 02), 1),
            DreamID::new(NaiveDate::from_ymd(2000, 01, 02), 3),
            DreamID::new(NaiveDate::from_ymd(1999, 01, 01), 1),
            DreamID::new(NaiveDate::from_ymd(1999, 01, 01), 2),
        ];
        let expect_remove = [0, 0, 0, 0, 0];
        let expect_insert = [3, 3, 3, 11, 11];
        let expect_newindices = [0, 1, 3, 10, 11];
        for (i, it) in to_move_to.iter().enumerate() {
            let mut indextoid = Vec::new();
            for (&date, &idx) in dates.iter().zip(idxes.iter()) {
                indextoid.push(DreamID::new(date, idx));
            }
            let mut expectoid = indextoid.clone();
            expectoid.remove(expect_remove[i]);
            let mut newid = indextoid[expect_insert[i]];
            newid.idx += 1;
            expectoid.insert(expect_insert[i], newid);

            assert_eq!(
                DreamListMaster::move_ids(&mut indextoid, DreamID::new(dates[0], idxes[0]), *it),
                Some((0, expect_newindices[i])),
                "Wrong indices on run {}",
                i
            );
            assert_eq!(indextoid, expectoid, "Ids not correctly moved on run {}", i);
        }

        let to_move_to = [
            DreamID::new(NaiveDate::from_ymd(2000, 01, 02), 0),
            DreamID::new(NaiveDate::from_ymd(2000, 01, 02), 1),
            DreamID::new(NaiveDate::from_ymd(2000, 01, 02), 3),
            DreamID::new(NaiveDate::from_ymd(1999, 01, 02), 0),
            DreamID::new(NaiveDate::from_ymd(1999, 01, 01), 1),
            DreamID::new(NaiveDate::from_ymd(1999, 01, 01), 2),
        ];
        let expect_remove = [9, 9, 9, 9, 9, 9];
        let expect_insert = [4, 4, 4, 9, 11, 11];
        let expect_newindices = [1, 2, 4, 9, 10, 11];
        for (i, it) in to_move_to.iter().enumerate() {
            let mut indextoid = Vec::new();
            for (&date, &idx) in dates.iter().zip(idxes.iter()) {
                indextoid.push(DreamID::new(date, idx));
            }
            let mut expectoid = indextoid.clone();
            expectoid.remove(expect_remove[i]);
            let mut newid = indextoid[expect_insert[i] - 1];
            if newid.date == to_move_to[i].date {
                newid.idx += 1;
            } else {
                newid = to_move_to[i];
            }
            if i >= 4 {
                newid = DreamID::new(dates[11], 2);
            }
            expectoid.insert(expect_insert[i], newid);

            assert_eq!(
                DreamListMaster::move_ids(&mut indextoid, DreamID::new(dates[7], idxes[7]), *it),
                Some((7, expect_newindices[i])),
                "Wrong indices on run {}",
                i
            );
            assert_eq!(indextoid, expectoid, "Ids not correctly moved on run {}", i);
        }
    }
}
