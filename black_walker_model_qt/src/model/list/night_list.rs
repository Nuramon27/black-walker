//! Contains the [`NightListQt`].

use chrono::Datelike;

use black_walker_logic::prelude::*;
use black_walker_logic::sync::prelude::*;
use black_walker_logic::{lock, stage_diary};
use black_walker_logic::{DreamList, Night, NightList, RevDate};

use crate::interface::*;
use crate::strings;

/// Displays the list of nights.
///
/// There are two reasons a night might appear in the `NightListQt`:
///
/// * There is a dream in that night.
///
/// * The night lies in the [`NightList`](black_walker_logic::NightList),
/// i.e. there is special information available for that night.
pub struct NightListQt {
    pub(super) emit: NightListQtEmitter,
    pub(super) model: NightListQtList,
    /// The list of nights
    pub(super) nights: Vec<RevDate>,
    pub(super) diary: Option<Diary>,
}

impl NightListQt {
    /// Does not emit `begin_reset_model` or `end_reset_model`, this has to
    /// be ensured by the caller.
    pub(super) fn initialize(&mut self) {
        let mut diary = self.diary.as_mut().unwrap();
        let mut stage = stage_diary!(diary; DL, NL;);
        if let (Ok(dreamlist), Ok(nightlist)) = lock!(stage; DreamList, NightList;) {
            let mut nights: Vec<RevDate> = dreamlist
                .iter()
                .map(|(id, _)| id.date.into())
                .chain(nightlist.keys().copied())
                .collect();
            nights.sort();
            nights.dedup();
            self.nights = nights;
        } else {
            panic!("Worker thread disconnected")
        };
    }
    /// Evaluates `func` on the `NightList`, returning the result.
    pub(super) fn query<F: FnOnce(&NightList) -> T, T>(&self, func: F) -> T {
        let nightlist = self
            .diary
            .as_ref()
            .unwrap()
            .nl
            .read()
            .expect("Worker thread disconnected");
        func(&nightlist)
    }
    /// Evaluates `func` on the night found in the `NightList` on `date`.
    pub(super) fn query_night<F: FnOnce(&Night) -> Option<T>, T>(
        &self,
        date: RevDate,
        func: F,
    ) -> Option<T> {
        self.query(|nl| nl.get(&date).and_then(func))
    }
    /// Evaluates `func` on the night found at `index`.
    #[allow(unused)]
    pub(super) fn query_list<F: FnOnce(&Night) -> Option<T>, T>(
        &self,
        index: usize,
        func: F,
    ) -> Option<T> {
        self.nights
            .get(index)
            .and_then(|date| self.query_night(*date, func))
    }
    /// Returns the index `date` has in [`nights`](NightListQt::nights).
    pub(super) fn index_of_night(&self, date: RevDate) -> Option<usize> {
        self.nights.iter().position(|x| *x == date)
    }
}

impl NightListQtTrait for NightListQt {
    fn new(emit: NightListQtEmitter, model: NightListQtList) -> Self {
        NightListQt {
            emit,
            model,
            nights: Vec::new(),
            diary: None,
        }
    }
    fn emit(&mut self) -> &mut NightListQtEmitter {
        &mut self.emit
    }
    fn row_count(&self) -> usize {
        self.nights.len()
    }
    /// Returns the calendar week of the night at `index`.
    fn k_w(&self, index: usize) -> u32 {
        if let Some(night) = self.nights.get(index) {
            night.d.iso_week().week()
        } else {
            0
        }
    }
    /// Returns the night at `index` pretty printed.
    fn nacht(&self, index: usize) -> String {
        self.nights
            .get(index)
            .map(|d| {
                format!(
                    "{} {} {}",
                    d.d.day(),
                    strings::monthname_short_german(d.d.month()),
                    d.d.year()
                )
            })
            .unwrap_or_default()
    }
    /// Returns the night at `index` in format "YYYY-MM-DD".
    fn date_formal(&self, index: usize) -> String {
        self.nights
            .get(index)
            .map(|d| d.d.format("%Y-%m-%d").to_string())
            .unwrap_or_default()
    }
    /// Returns the number of dreams in the night at `index`.
    fn traeume(&self, index: usize) -> u32 {
        if let Some(&night) = self.nights.get(index) {
            self.diary
                .as_ref()
                .unwrap()
                .dl
                .read()
                .expect("Worker thread has been disconnected")
                .dream_count(night.into())
        } else {
            0
        }
    }
    /// Returns the weekday of the night at `index`.
    fn wochentag(&self, index: usize) -> String {
        if let Some(night) = self.nights.get(index) {
            strings::weekday_long_german(night.d.weekday()).to_string()
        } else {
            String::new()
        }
    }
}
