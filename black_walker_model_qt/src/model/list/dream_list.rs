//! Contains the [`DreamListQt`].

#![allow(clippy::trivial_regex)]

use std::iter::FromIterator;
use std::sync::mpsc::Receiver;
use std::sync::Arc;

use chrono::{Datelike, NaiveDate};
use serde::{Deserialize, Serialize};

use black_walker_logic::elements::tag;
use black_walker_logic::operate::dream::*;
use black_walker_logic::operate::search::SearchDreamsWithTag;
use black_walker_logic::prelude::*;
use black_walker_logic::sync::prelude::*;
use black_walker_logic::{lock, stage_diary};

use lazy_static::lazy_static;
use regex::{Regex, RegexBuilder};

use super::{Contact, Nesting, NotifyAction};
use crate::interface::*;
use crate::model::master::Master;
use crate::model::Pipelines;
use crate::strings;
use crate::{execute, impl_begin_end_signals, query};

#[derive(Debug, Clone, PartialEq, Eq, Hash, Serialize, Deserialize)]
struct DreamIdList {
    ids: Vec<DreamID>,
}

impl black_walker_logic::model::Emitter for DreamListQtEmitter {
    fn emit(&mut self) {
        self.wakeup_changed();
    }
}

impl_begin_end_signals! {DreamListQtEmitter}

lazy_static! {
    static ref ENDS_WITH_DREAM_REG_EX: Regex = RegexBuilder::new("traum$")
        .case_insensitive(true)
        .build()
        .unwrap();
}

/// Displays the list of dreams.
pub struct DreamListQt {
    pub(super) emit: DreamListQtEmitter,
    pub(super) model: DreamListQtList,
    /// Holds the information which id is to be displayed at a
    /// certain index.
    pub(super) indextoid: Vec<DreamID>,
    pub(super) cnt: Option<Contact<DreamListQtEmitter>>,
    pub(super) nesting: Nesting,
    dreams_with_tag_rec: Option<Receiver<Vec<DreamID>>>,
    dreams_with_tag_further: Arc<()>,
}

impl DreamListQt {
    pub fn set_pipe(&mut self, pipe: Pipelines) {
        self.cnt = Some(Contact::from_pipe(self.emit.clone(), pipe));
    }
    /// Evaluates `f` on the `DreamList`, returning its result.
    pub(super) fn query<F: FnOnce(&DreamList) -> T, T>(&self, f: F) -> T {
        let dreamlist = self
            .cnt
            .as_ref()
            // Won't panic because diary must always be valid.
            .expect("DreamListQt not fully initialized: No Contact has been passed!")
            .diary
            .dl
            .read()
            .expect("Diary is poisoned");
        f(&dreamlist)
    }
    /// Evaluates `f` on the `DreamItem` at `index` if such a dream exists,
    /// otherwise returns `None`.
    pub(super) fn query_list<F: FnOnce(&DreamItem) -> Option<T>, T>(
        &self,
        index: usize,
        f: F,
    ) -> Option<T> {
        if let Some(&id) = self.indextoid.get(index) {
            self.query(|dl| dl.get(id).and_then(f))
        } else {
            None
        }
    }
    /// Emit notification signal `normal_func` and store it in `self.nesting`,
    /// with opposite notification `opposite_func`.
    pub(super) fn notify_begin<F: FnOnce(&mut DreamListQtList), G: FnOnce(&mut DreamListQtList)>(
        &mut self,
        normal_func: F,
        opposite_func: G,
    ) {
        match self.nesting.nest() {
            NotifyAction::Normal => normal_func(&mut self.model),
            NotifyAction::ResetModel => {
                opposite_func(&mut self.model);
                self.model.begin_reset_model();
            }
            NotifyAction::Nothing => (),
        }
    }
    /// Emit notification signal `normal_func` and decrease the nesting.
    pub(super) fn notify_end<F: FnOnce(&mut DreamListQtList)>(&mut self, normal_func: F) {
        match self.nesting.unnest() {
            NotifyAction::Normal => normal_func(&mut self.model),
            NotifyAction::ResetModel => self.model.end_reset_model(),
            NotifyAction::Nothing => (),
        }
    }

    pub fn set_ids(&mut self, ids: Vec<DreamID>) {
        self.model.begin_reset_model();
        self.indextoid = ids;
        self.model.end_reset_model();
    }

    pub fn present_tags(&self, index: usize, category: &str) -> Option<String> {
        // Todo: diary should not be cloned here.
        let mut diary = self.cnt.as_ref().unwrap().diary.clone();
        let mut stage = stage_diary!(diary; DL, TAG);
        // Incorrect clippy warning. The let binding is necessary
        // because of lifetime problems.
        #[allow(clippy::let_and_return)]
        let res = if let (Some(id), (Ok(dreamlist), Ok(tagforest))) = (
            self.indextoid.get(index),
            lock!(stage; DreamList, TagForest),
        ) {
            let dream = dreamlist.get(*id)?;
            let mut tags = Vec::from_iter(dream.d.get_tags(&category)?);
            tag::sort_by_relevance(tagforest.get(category)?, &mut tags);
            Some(tags.comma_sep())
        } else {
            None
        };
        res
    }

    /// Returns a short version of the dream kind described by `s`.
    ///
    /// I.e. "Klartraum" is replaced by "KT" and so on.
    fn short_version_of_kind(s: &str) -> String {
        if s == "Klartraum" {
            "KT"
        } else if s == "Trübtraum" {
            "TT"
        } else if s == "Präluzider Traum" {
            "PT"
        } else if ENDS_WITH_DREAM_REG_EX.is_match(s)
            && !s.contains(' ')
            && s.chars().take(2).all(char::is_alphabetic)
        {
            return s.chars().take(2).chain(std::iter::once('T')).collect();
        } else {
            s
        }
        .to_string()
    }
}

impl DreamListQtTrait for DreamListQt {
    fn new(emit: DreamListQtEmitter, model: DreamListQtList) -> Self {
        DreamListQt {
            emit,
            model,
            indextoid: Vec::new(),
            cnt: None,
            nesting: Nesting::new(),
            dreams_with_tag_rec: None,
            dreams_with_tag_further: Arc::new(()),
        }
    }
    fn emit(&mut self) -> &mut DreamListQtEmitter {
        &mut self.emit
    }
    fn clear(&mut self) {
        self.set_ids(Vec::new());
    }
    fn row_count(&self) -> usize {
        self.indextoid.len()
    }
    fn personen(&self, index: usize) -> String {
        self.present_tags(index, "Personen").unwrap_or_default()
    }
    fn orte(&self, index: usize) -> String {
        self.present_tags(index, "Orte").unwrap_or_default()
    }
    fn plaetze(&self, index: usize) -> String {
        self.present_tags(index, "Plätze").unwrap_or_default()
    }
    fn traumzeichen(&self, index: usize) -> String {
        self.present_tags(index, "Traumzeichen").unwrap_or_default()
    }
    fn gegenstaende(&self, index: usize) -> String {
        self.present_tags(index, "Gegenstände").unwrap_or_default()
    }
    fn essen_und_getraenke(&self, index: usize) -> String {
        self.present_tags(index, "Essen & Getränke")
            .unwrap_or_default()
    }
    fn emotionen(&self, index: usize) -> String {
        self.present_tags(index, "Emotionen").unwrap_or_default()
    }
    /// Returns the date of the dream at `index` in pretty printed format.
    fn date(&self, index: usize) -> String {
        self.query_list(index, |d| {
            Some(format!(
                "{} {} {} {}",
                strings::weekday_short_german(d.date().weekday()),
                d.date().day(),
                strings::monthname_short_german(d.date().month()),
                d.date().year()
            ))
        })
        .unwrap_or_default()
    }
    /// Returns the date of the dream at `index` in format "YYYY-MM-DD".
    fn date_formal(&self, index: usize) -> String {
        self.query_list(index, |d| Some(d.date().format("%Y-%m-%d").to_string()))
            .unwrap_or_default()
    }
    /// Returns the [`idx`](black_walker_logic::DreamID::idx) parto of the
    /// dream at `index`.
    fn idx(&self, index: usize) -> u32 {
        self.query_list(index, |d| Some(d.idx()))
            .unwrap_or_default()
    }
    fn kind(&self, index: usize) -> String {
        self.query_list(index, |d| {
            d.d.get_entry_text("Art")
                .map(|s| Self::short_version_of_kind(s))
        })
        .unwrap_or_default()
    }
    /// Returns the name of the dream at `index`.
    fn name(&self, index: usize) -> String {
        self.query_list(index, |d| Some(d.d.name.clone()))
            .unwrap_or_default()
    }
    /// Returns the Toml representation of the [`DreamID`](black_walker_logic::DreamID)
    /// at `index`.
    fn toml(&self, index: usize) -> String {
        self.indextoid
            .get(index)
            .map(|id| toml::to_string(id).ok())
            .flatten()
            .unwrap_or_default()
    }
    /// Checks whether the [`Dream`](black_walker_logic::DreamID)
    /// from the [`DreamID`](black_walker_logic::DreamID) specified
    /// by `source` (via Toml) can be dropped onto `target`.
    ///
    /// A dream specified by `source` can be dropped onto index `target`
    /// if it fullfills the following conditions:
    ///
    /// * `source` is a valid toml-specification of a `DreamIdList` with at least one element
    ///
    /// * `target` is an existing index or equal to the number of dreams
    ///
    /// * the dream on target or the one preceding it
    ///   has the same date as `source`
    fn can_drop_toml(&self, source: String, target: i64) -> bool {
        if let Ok(source_ids) = toml::from_str::<DreamIdList>(&source) {
            let target_id = if 0 <= target && (target as usize) < self.indextoid.len() {
                self.indextoid[target as usize]
            } else if let Some(target_id) = self.indextoid.last() {
                *target_id
            } else {
                return false;
            };
            if let Some(&source_id) = source_ids.ids.first() {
                source_id.date == target_id.date
                    || self
                        .indextoid
                        .get((target - 1) as usize)
                        .map(|pre_target_id| {
                            pre_target_id.date == source_id.date && target_id.idx == 0
                        })
                        .unwrap_or(true)
            } else {
                false
            }
        } else {
            false
        }
    }
    /// Drops the [`Dream`](black_walker_logic::DreamID)
    /// from the [`DreamID`](black_walker_logic::DreamID) specified
    /// by `source` (via Toml) onto `target`.
    ///
    /// In case of succes, this triggers a
    /// [`MoveDream`](black_walker_logic::operate::dream::MoveDream)
    /// command.
    ///
    /// Returns whether the drop could be completed.
    fn drop_toml(&mut self, source: String, target: i64) -> bool {
        if !self.can_drop_toml(source.clone(), target) {
            return false;
        }
        if let Ok(ids) = toml::from_str::<DreamIdList>(&source) {
            let target_id = if 0 <= target && (target as usize) < self.indextoid.len() {
                self.indextoid[target as usize]
            } else if let Some(target_id) = self.indextoid.last() {
                *target_id
            } else {
                return false;
            };
            if let Some(source_id) = ids.ids.first() {
                let cmd = if source_id.date == target_id.date {
                    MoveDream::from_drag_drop(source_id.date, source_id.idx, target_id.idx)
                } else {
                    // If the dream is dropped right after the end of a date
                    // section, we cannot simply take the id of the place where
                    // it was dropped, since it would have the wrong date.
                    if let Some(pre_target_id) = self.indextoid.get((target - 1) as usize) {
                        MoveDream::from_drag_drop(
                            source_id.date,
                            source_id.idx,
                            pre_target_id.idx + 1,
                        )
                    } else {
                        // In this case, self.indextoid[target] exists, but self.indextoid[target-1]
                        // does not, so we must be at the beginning, i.e. target == 0.
                        // But we can only land in that branch if the id before
                        // self.indextoid[target] has the same date as source_id.
                        // So this branch is actually dead. But nevertheless we
                        // emit the usual command:
                        MoveDream::from_drag_drop(source_id.date, source_id.idx, target_id.idx)
                    }
                };
                if let Some(cmd) = cmd {
                    execute!(self.cnt, cmd);
                    true
                } else {
                    false
                }
            } else {
                false
            }
        } else {
            false
        }
    }
    /// Computes the position of the first dream on `date`
    /// in the list.
    fn compute_row_of_night(&self, year: i32, month: u32, day: u32) -> u32 {
        let date = NaiveDate::from_ymd(year, month, day);
        self.indextoid
            .iter()
            .position(|id| *id >= DreamID::new(date, 0))
            .unwrap_or(0) as u32
    }
    /// Sends out a query to search for dreams containing tag `tagname`
    /// of `category`.
    fn search_dreams_with_tag(&mut self, category: String, tagname: String) {
        let (query, rec, further) =
            SearchDreamsWithTag::new(category, TagSpec::from(tagname).into_base_name());
        query!(self.cnt, self.emit.clone(), query);
        self.dreams_with_tag_rec = Some(rec);
        self.dreams_with_tag_further = further;
    }
    /// Receives the result of a search for dreams containing a certain
    /// tag and displays exactly the dreams in this result.
    ///
    /// Intended to be used with
    /// [`search_dreams_with_tag`](DreamListQtTrait::search_dreams_with_tag).
    fn receive_dreams_with_tag(&mut self) {
        if let Some(rec) = self.dreams_with_tag_rec.as_ref() {
            while let Ok(dreams_with_tag) = rec.try_recv() {
                self.model.begin_reset_model();
                self.indextoid = dreams_with_tag;
                self.model.end_reset_model();
            }
        }
    }
    #[doc(hidden)]
    fn begin_cmd(&self) -> bool {
        true
    }
    #[doc(hidden)]
    fn end_cmd(&self) -> bool {
        true
    }
    #[doc(hidden)]
    fn wakeup(&self) -> bool {
        true
    }
}

/// Initializes `dreamlist` taking the [`Contact`] from `master`.
///
/// # Safety
///
/// All pointers must be valid.
#[no_mangle]
pub unsafe fn initialize_dreamlist_master(dreamlist: *mut DreamListQt, master: *mut Master) {
    let dreamlist = &mut *dreamlist;
    let master = &mut *master;
    let emit = dreamlist.emit.clone();
    dreamlist.cnt = Some(Contact::from_pipe(emit, master.contact().pipe.clone()));
}

#[cfg(test)]
mod tests {
    #[test]
    fn ends_with_dream_regex() {
        assert!(super::ENDS_WITH_DREAM_REG_EX.is_match("RC-Traum"));
    }
}
