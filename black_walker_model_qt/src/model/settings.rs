//! Contains the [`SettingsQt`] model.

use std::convert::TryInto;
use std::path::PathBuf;

use std::sync::RwLock;

use black_walker_logic::settings::GS;

use super::Contact;
use crate::impl_begin_end_signals;
use crate::interface::*;

impl_begin_end_signals! {SettingsQtEmitter}

/// The model by which [settings](GS) of Black Walker can be queried.
pub struct SettingsQt {
    emit: SettingsQtEmitter,
    cnt: Option<Contact<SettingsQtEmitter>>,
}

impl SettingsQt {
    /// Evaluates `func` on the [`GS`] of the [`Diary`](black_walker_logic::Diary).
    fn query<F: FnOnce(&GS) -> T, T>(&self, func: F) -> T {
        let set = RwLock::read(
            &self
                .cnt
                .as_ref()
                .expect("Contact has not been set")
                .diary
                .gl,
        )
        .expect("Worker thread has hung up");
        func(&set)
    }
    /// Applies `func` to the [`GS`] of the [`Diary`](black_walker_logic::Diary)
    /// in order to change settings.
    ///
    /// Keep in mind that at the moment most settings cannot be
    /// changed at runtime.
    fn set<F: FnOnce(&mut GS)>(&self, func: F) {
        let mut set = RwLock::write(
            &self
                .cnt
                .as_ref()
                .expect("Contact has not been set")
                .diary
                .gl,
        )
        .expect("Worker thread has hung up");
        func(&mut set)
    }
}

impl SettingsQt {
    pub fn set_contact(&mut self, contact: Contact<SettingsQtEmitter>) {
        self.cnt = Some(contact);
    }
}

impl SettingsQtTrait for SettingsQt {
    fn new(emit: SettingsQtEmitter) -> Self {
        SettingsQt { emit, cnt: None }
    }
    fn emit(&mut self) -> &mut SettingsQtEmitter {
        &mut self.emit
    }
    /// Returns the [standard diary location](GS::diary_location).
    fn location(&self) -> String {
        self.query(|set| {
            if let Some(location) = &set.diary_location {
                location
                    .full_path()
                    .to_str()
                    .unwrap_or_default()
                    .to_string()
            } else {
                String::new()
            }
        })
    }
    fn place_last_used(&self) -> String {
        self.query(|set| set.place_last_used.clone())
    }
    fn set_place_last_used(&mut self, value: String) {
        self.set(|set| set.place_last_used = value)
    }
    /// Sets the [standard diary location](GS::diary_location) to `value`.
    fn set_location(&mut self, value: String) {
        let mut set = self.cnt.as_ref().unwrap().diary.gl.write().unwrap();
        set.diary_location = PathBuf::from(value).try_into().ok();
    }
    #[doc(hidden)]
    fn sizes_notify(&self) -> bool {
        true
    }
    #[doc(hidden)]
    fn begin_cmd(&self) -> bool {
        true
    }
    #[doc(hidden)]
    fn end_cmd(&self) -> bool {
        true
    }
    /// Returns the number of [standard tag categories](GS::standard_tag_categories).
    fn nr_standard_tag_categories(&self) -> u32 {
        self.query(|gl| gl.standard_tag_categories().len() as usize) as u32
    }
    /// Returns the `index`-th [standard tag categories](GS::standard_tag_categories).
    fn standard_tag_category(&self, index: u32) -> String {
        self.query(|gl| {
            gl.standard_tag_categories()
                .get(index as usize)
                .map(String::clone)
                .unwrap_or_default()
        })
    }
    fn searchwithgroups(&self) -> bool {
        self.query(|gl| gl.search.searchwithgroups)
    }
    fn set_searchwithgroups(&mut self, value: bool) {
        self.set(|gl| gl.search.searchwithgroups = value)
    }
    fn searchweaktags(&self) -> bool {
        self.query(|gl| gl.search.searchweaktags)
    }
    fn set_searchweaktags(&mut self, value: bool) {
        self.set(|gl| gl.search.searchweaktags = value)
    }

    /// Returns the `i`-th part of the size of the window `name`.
    ///
    /// The parts are ordered _[width, height].
    ///
    /// # Panics
    ///
    /// If `i >= 2`.
    fn size_window(&self, name: String, i: u8) -> u32 {
        self.query(|gl| gl.sizes.size_window(&name, i.into()))
            .unwrap_or(0)
    }

    /// Sets the size of the window `name` to [`width`, `height`].
    ///
    /// The parts are ordered _[x-pos, y-pos, width, height].
    fn set_sizes_window(&mut self, name: String, width: u32, height: u32) {
        self.set(|gl| gl.sizes.set_sizes_window(name, [width, height]));
        //self.emit.sizes_notify_changed();
    }
    /// Returns the `i`-th column size of the widget `name`.
    fn size_column(&self, name: String, i: u32) -> u32 {
        self.query(|gl| gl.sizes.size_column(&name, i as usize))
    }
    /// Sets the `i`-th column size of the widget `name` to `value`.
    fn set_size_column(&mut self, name: String, i: u32, value: u32) {
        self.set(|gl| gl.sizes.set_size_column(name, i as usize, value));
        //self.emit.sizes_notify_changed();
    }
    /// Deletes all column sizes of the widget `name` from index `from` on.
    fn crop_size_column_to_default(&mut self, name: String, from: u32) {
        self.set(|gl| gl.sizes.crop_size_column_to_default(name, from as usize));
        //self.emit.sizes_notify_changed();
    }
    /// Sets the default column size of the widget `name` to `value`
    /// and delete all sizes from `from` on.
    fn set_size_column_default(&mut self, name: String, from: u32, value: u32) {
        self.set(|gl| gl.sizes.set_size_column_default(name, from as usize, value));
        //self.emit.sizes_notify_changed();
    }
}
