//! Contains the Models that administrate the
//! [`TagForest`].
use std::cmp::Ordering;
use std::collections::HashMap;
use std::convert::TryFrom;

use black_walker_logic::prelude::*;
use black_walker_logic::sync::prelude::*;
use black_walker_logic::{lock, read_single, stage_diary};

use black_walker_logic::operate::tag::{
    ChangeTagCategory, CreateCategory, CreateTag, DeleteCategory, DeleteTag, ReparentTag, SetTag,
};

use crate::interface::*;
use crate::{apply, execute, impl_begin_end_signals};

use super::{Contact, Nesting, NotifyAction};
use ffi_creation::TagTreeQtWeak;
use tagtreeqt::TagNamesToml;

pub mod ffi_creation;
pub mod tagtreeqt;

pub use ffi_creation::{TagTreeQtCppSide, TagTreeQtGuard};
pub use tagtreeqt::TagTreeQt;

impl_begin_end_signals!(TagTreeMasterEmitter);

/// The `TagTreeMaster` manages the models representing the tagtrees
/// for the different categories of tags (see [`TagTreeQt`]).
///
/// It keeps track of models being currently in use.
///
/// # Panics
///
/// Any method except [`set_contact`](TagTreeMaster::set_contact) may panic if it is called
/// before a Contact has been set.
pub struct TagTreeMaster {
    emit: TagTreeMasterEmitter,
    model: TagTreeMasterList,
    /// The models that are currently in use.
    active_models: HashMap<String, TagTreeQtWeak>,
    /// The list of standard_tag_categories.
    standard_tag_categories: Vec<String>,
    /// The list of tag categories.
    ///
    /// The first items in this list will always be the
    /// [`standard_tag_categories`](TagTreeMaster::standard_tag_categories).
    categories: Vec<String>,
    /// The contact of this `TagTreeMaster`.
    cnt: Option<Contact<TagTreeMasterEmitter>>,
}

impl TagTreeMaster {
    /// Returns the index `category` would have if it was inserted into the list of
    /// categories.
    ///
    /// If `category` is already present in this list, this returns just its index.
    fn index_in_categories(&self, category: &str) -> usize {
        if let Some((index, _)) = self
            .standard_tag_categories
            .iter()
            .enumerate()
            .find(|(_, c)| c.as_str() == category)
        {
            index
        } else {
            self.categories[self.standard_tag_categories.len()..]
                .iter()
                .enumerate()
                .find(|(_, c)| cmp_coll(c.as_str(), category) == Ordering::Greater)
                .map_or(self.categories.len(), |(index, _)| {
                    index + self.standard_tag_categories.len()
                })
        }
    }
    /// Returns the contact of this `TagTreeMaster`
    pub fn contact(&mut self) -> &mut Contact<TagTreeMasterEmitter> {
        self.cnt.as_mut().unwrap()
    }
    /// Sets the contact of this `TagTreeMaster`.
    ///
    /// ## Panics
    ///
    /// If the tagtree in the diary contained in `con` is poisoned.
    pub fn set_contact(&mut self, mut con: Contact<TagTreeMasterEmitter>) {
        {
            let mut stage = stage_diary!(con.diary; TAG, SET);
            let tagtree =
                read_single!(stage, TagForest).expect("Given Contact contains a poisened Diary!");
            let categories: Vec<String> = tagtree.keys().cloned().collect();
            self.standard_tag_categories = read_single!(stage, GS)
                .expect("Given Contact contains a poisened Settings struct!")
                .standard_tag_categories()
                .clone();
            self.set_categories(categories);
        }
        self.cnt = Some(con);
    }
    /// Initializes the `TagTreeMaster`, deleting models of
    /// Categories that do not exist anymore and (Re)Initializes all
    /// [`TagTreeQt`]s currently in use.
    fn initialize(&mut self) {
        let mut diary = self.cnt.as_ref().unwrap().diary.clone();
        let mut stage = stage_diary!(diary; TAG, SET);
        if let (Ok(tagforest), Ok(settings)) = lock!(stage; TagForest, GS) {
            let new_categories: Vec<String> = tagforest.keys().map(ToString::to_string).collect();
            let mut categories_to_delete = Vec::new();
            for (cat_name, modelref) in &self.active_models {
                if !new_categories.contains(cat_name) {
                    categories_to_delete.push(cat_name.clone());
                } else if let Some(mut model) = modelref.upgrade() {
                    model.model.begin_reset_model();
                    model.initialize();
                    model.model.end_reset_model();
                }
            }
            for it in categories_to_delete {
                self.active_models.remove(&it);
            }
            self.model.begin_reset_model();
            self.standard_tag_categories = settings.standard_tag_categories().clone();
            self.set_categories(new_categories);
            self.model.end_reset_model();
        };
    }

    /// Evaluates `func` on the `TagForest`, returning the result.
    fn query<F: FnOnce(&TagForest) -> T, T>(&self, func: F) -> T {
        let tagforest = self
            .cnt
            .as_ref()
            .expect("Contact has not been set")
            .diary
            .tag
            .read()
            .expect("TagForest is poisoned");
        func(&tagforest)
    }
    /// Evaluates `func` on the `TagTree` for `category`, returning the result
    /// if this category exists.
    fn query_tree<F: FnOnce(&TagTree) -> Option<T>, T>(
        &self,
        category: &str,
        func: F,
    ) -> Option<T> {
        self.query(|tag| tag.get(category).and_then(func))
    }

    /// Sets [`self.categories`](TagTreeMaster::categories) to the union
    /// of the [standard tag categories](GS::standard_tag_categories) and `categories`.
    ///
    /// Always use this function for setting the list of categories,
    /// as it inserts them in the correct order: First the standard
    /// tag categories in their specific order, then the rest alphabetically
    /// ordered.
    fn set_categories(&mut self, categories: Vec<String>) {
        self.categories = self.standard_tag_categories.clone();
        for it in categories {
            if !self.categories.contains(&it) {
                self.categories.insert(self.index_in_categories(&it), it);
            }
        }
    }
    /// Returns the model managing `category` if such one is currently in use.
    ///
    /// Otherwise, `None` is returned.
    fn get_model_of_category(&mut self, category: &str) -> Option<*mut TagTreeQtGuard> {
        if let Some(guard) = self
            .active_models
            .get(category)
            .and_then(TagTreeQtWeak::upgrade)
        {
            Some(Box::into_raw(Box::new(guard)))
        } else {
            None
        }
    }
    /// Sets the model belonging to `category` to `tag_tree_qt`.
    ///
    /// # Safety
    ///
    /// `tag_tree_qt` must point to a valid [`TagTreeQt`].
    unsafe fn set_model_of_category(
        &mut self,
        category: &str,
        tag_tree_qt_cpp_side: *mut TagTreeQtCppSide,
        tag_tree_qt_rust_side: *mut TagTreeQt,
    ) -> *mut TagTreeQtGuard {
        {
            let tag_tree_qt: &mut TagTreeQt = &mut *tag_tree_qt_rust_side;
            tag_tree_qt.set_category(category.to_string());
            let tag_tree_qt_emitter = tag_tree_qt.emit.clone();
            tag_tree_qt.set_contact(Contact::from_pipe(
                tag_tree_qt_emitter,
                self.cnt.as_ref().unwrap().pipe.clone(),
            ));
        }
        let (weakguard, guard) = TagTreeQtGuard::new(tag_tree_qt_cpp_side, tag_tree_qt_rust_side);
        self.active_models.insert(category.to_string(), weakguard);

        Box::into_raw(Box::new(guard))
    }
    /// Emits a `data_changed` signal for the parent chain of tag `name` in `category`,
    /// skipping the tag `name` itself.
    fn notify_parents_of_tag(&mut self, category: &str, name: &TagStr) {
        if let Some(model) = self
            .active_models
            .get_mut(category)
            .and_then(TagTreeQtWeak::get_mut)
        {
            apply!(self.cnt, TagForest; TagForest::get, category; |tagtree| -> Option<()> {
                if let Some(parent_iter) = tagtree.parent_chain(name) {
                    for parent in parent_iter.skip(1) {
                        if let Some(Some(index)) = model.cache.get_rev(parent.name()) {
                            model.model.data_changed(*index, *index);
                        }
                    }
                }
                None
            });
        }
    }
}

impl TagTreeModel for TagTreeMaster {
    fn begin_reset(&mut self) {
        self.model.begin_reset_model();
    }
    fn end_reset(&mut self) {
        self.initialize();
    }
    fn begin_reset_category(&mut self, category: &str) {
        if let Some(model) = self
            .active_models
            .get_mut(category)
            .and_then(TagTreeQtWeak::get_mut)
        {
            model.model.begin_reset_model();
        }
    }
    fn end_reset_category(&mut self, category: &str) {
        if let Some(model) = self
            .active_models
            .get_mut(category)
            .and_then(TagTreeQtWeak::get_mut)
        {
            //model.initialize();
            model.model.end_reset_model();
        }
    }
    fn begin_set_tag(&mut self, _category: &str, _name: &TagStr) {}
    fn end_set_tag(&mut self, category: &str, name: &TagStr) {
        if let Some(model) = self
            .active_models
            .get_mut(category)
            .and_then(TagTreeQtWeak::get_mut)
        {
            if let Some(Some(index)) = model.cache.get_rev(name) {
                model.model.data_changed(*index, *index);
            }
        }
        self.notify_parents_of_tag(category, name);
    }
    fn begin_rename_tag(&mut self, _category: &str, _old_name: &TagStr, _new_name: &TagStr) {}
    fn end_rename_tag(&mut self, category: &str, old_name: &TagStr, new_name: &TagStr) {
        if let Some(model) = self
            .active_models
            .get_mut(category)
            .and_then(TagTreeQtWeak::get_mut)
        {
            if let Some(Some(index)) = model.cache.take_rev(old_name) {
                if model.cache.contains_rev(new_name) {
                    return;
                }
                model.cache.insert(Some(index), new_name.to_owned());
                model.model.data_changed(index, index);
            }
        }
    }
    fn begin_create_tag(&mut self, category: &str, name: &TagStr, parent: &TagStr) {
        if let Some(model) = self
            .active_models
            .get_mut(category)
            .and_then(TagTreeQtWeak::get_mut)
        {
            // Insert the tag into the caches of `model`.
            let new_id = model.id_of_new_tag(name);
            model.cache.insert(new_id, name.to_owned());
            if let Some(&parent_id) = model.cache.get_rev(parent) {
                apply!(self.cnt, TagForest; TagForest::get, category; |cat| {
                    if let Some(row) = cat.children_count_of(parent) {
                        model.notify_begin(
                            |tree| tree.begin_insert_rows(parent_id, row, row),
                            TagTreeQtTree::end_insert_rows,
                        );
                    };
                    Some(())
                });
            }
        }
    }
    fn end_create_tag(&mut self, category: &str, name: &TagStr, _parent: &TagStr) {
        if let Some(model) = self
            .active_models
            .get_mut(category)
            .and_then(TagTreeQtWeak::get_mut)
        {
            if !model.cache.contains_rev(name) {
                model
                    .cache
                    .insert(model.id_of_new_tag(name), name.to_owned());
            }
            model.notify_end(TagTreeQtTree::end_insert_rows);
        }
        self.notify_parents_of_tag(category, name);
    }
    fn begin_remove_tag(&mut self, category: &str, name: &TagStr) {
        if let Some(model) = self
            .active_models
            .get_mut(category)
            .and_then(TagTreeQtWeak::get_mut)
        {
            // Emit the remove-row-signal, obtaining the parent from
            // `TagTreeQt::parent` and the row number from `TagTreeQt::row`.
            if let Some(&Some(id)) = model.cache.get_rev(name) {
                let parent_id = model.parent(id);
                let row = model.row(id);
                model.notify_begin(
                    |tree| tree.begin_remove_rows(parent_id, row, row),
                    TagTreeQtTree::end_remove_rows,
                );
                // Remove the tag and all its children from the caches
                // of `model`.
                apply!(self.cnt, TagForest; TagForest::get, category; |tagtree| {
                    if let Some(subview) = tagtree.subview(name) {
                        for tag in &subview {
                            model.cache.remove_rev(tag.name());
                        }
                    };
                    Some(())
                });
            }
        }
    }
    fn end_remove_tag(&mut self, category: &str, _name: &TagStr) {
        if let Some(model) = self
            .active_models
            .get_mut(category)
            .and_then(TagTreeQtWeak::get_mut)
        {
            model.notify_end(TagTreeQtTree::end_remove_rows);
        }
    }
    fn begin_reparent_tag(&mut self, category: &str, name: &TagStr, newparent: &TagStr) {
        if let Some(model) = self
            .active_models
            .get_mut(category)
            .and_then(TagTreeQtWeak::get_mut)
        {
            if let (Some(&Some(id)), Some(&newparent_id)) =
                (model.cache.get_rev(name), model.cache.get_rev(newparent))
            {
                let oldparent_id = model.parent(id);
                let oldrow = model.row(id);
                apply!(self.cnt, TagForest; TagForest::get, category; |cat| {
                    if let Some(newrow) =
                        cat.children_count_of(newparent)
                    {
                        model.notify_begin(
                            |tree| {
                                tree.begin_move_rows(
                                    oldparent_id,
                                    oldrow,
                                    oldrow,
                                    newparent_id,
                                    newrow,
                                )
                            },
                            TagTreeQtTree::end_move_rows,
                        );
                    };
                    Some(())
                });
            }
        }
    }
    fn end_reparent_tag(&mut self, category: &str, name: &TagStr, _newparent: &TagStr) {
        if let Some(model) = self
            .active_models
            .get_mut(category)
            .and_then(TagTreeQtWeak::get_mut)
        {
            model.notify_end(TagTreeQtTree::end_move_rows);
        }
        self.notify_parents_of_tag(category, name);
    }
    fn begin_create_category(&mut self, category: &str) {
        // The process of creating a category can be completely
        // carried out in this function, so we can
        // emit `begin_insert_rows` and `end_insert_rows` directly here.
        let category = category.to_string();
        if !self.categories.contains(&category) {
            let row = self.index_in_categories(&category);
            self.model.begin_insert_rows(row, row);
            self.categories.insert(row, category);
            self.model.end_insert_rows();
        }
    }
    fn end_create_category(&mut self, _category: &str) {}
    fn begin_remove_category(&mut self, category: &str) {
        // The process of removing a category can be completely
        // carried out in this function, so we can
        // emit `begin_remove_rows` and `end_remove_rows` directly here.
        if let Some((index, _)) = self
            .categories
            .iter()
            .enumerate()
            .find(|(_, c)| c.as_str() == category)
        {
            self.model.begin_remove_rows(index, index);
            self.categories.remove(index);
            self.model.end_remove_rows();
        }
    }
    fn end_remove_category(&mut self, _category: &str) {}
}

impl TagTreeMasterTrait for TagTreeMaster {
    fn new(emit: TagTreeMasterEmitter, model: TagTreeMasterList) -> Self {
        TagTreeMaster {
            emit,
            model,
            active_models: HashMap::new(),
            standard_tag_categories: Vec::new(),
            categories: Vec::new(),
            cnt: None,
        }
    }
    fn emit(&mut self) -> &mut TagTreeMasterEmitter {
        &mut self.emit
    }
    #[doc(hidden)]
    fn begin_cmd(&self) -> bool {
        true
    }
    #[doc(hidden)]
    fn end_cmd(&self) -> bool {
        true
    }
    /// Returns the number of categories.
    fn row_count(&self) -> usize {
        self.categories.len()
    }
    /// Returns the `index`-th category.
    fn category(&self, index: usize) -> String {
        self.categories
            .get(index)
            .map_or(String::new(), |x| x.clone())
    }
    /// Returns the total number of tags in `category`.
    fn tag_count(&mut self, category: String) -> u64 {
        apply!(self.cnt, TagForest; TagForest::get, &category; |cat| Some(cat.len())).unwrap_or(0)
            as u64
    }
    /// Creates the new tags specified by the comma-separated `taglist`
    /// in `category` under `group`.
    ///
    /// `group` is created if it does not yet exist.
    fn create_new_tags(&mut self, category: String, group: String, taglist: String) {
        let group = TagSpec::from(group).into_base_name();
        let tagnames: Vec<TagName> = Vec::from_comma_sep(&taglist)
            .into_iter()
            .map(|tagname: String| TagSpec::from(tagname).into_base_name())
            .collect();

        let create_group = apply!(
            self.cnt, TagForest; TagForest::get, &category;
            |cat| Some(!cat.node_exists(&group))
        )
        .unwrap_or(false);
        if create_group {
            execute!(
                self.cnt,
                CreateTag::new(category.clone(), group.clone(), TagName::root())
            );
        }
        for tagname in tagnames {
            execute!(
                self.cnt,
                CreateTag::new(category.clone(), tagname, group.clone())
            );
        }
    }
    /// Deletes tag `name` in `category`.
    ///
    /// If `with_children` is true, all children are deleted as well.
    fn delete_tag(&mut self, category: String, name: String, with_children: bool) {
        let name = if let Ok(name) = TagName::try_from(name) {
            name
        } else {
            return;
        };
        if with_children {
            execute!(self.cnt, DeleteTag::new_with_children(category, name));
        } else if let Some((pre_cmd, cmd)) =
            self.query(|forest| DeleteTag::new_without_children(category, name, forest))
        {
            for pre_cmd in pre_cmd {
                execute!(self.cnt, pre_cmd);
            }
            execute!(self.cnt, cmd);
        }
    }
    /// Returns the description of tag `name` in `category`.
    fn tag_description(&self, category: String, name: String) -> String {
        self.query_tree(&category, |tagtree| {
            tagtree
                .get(&name)
                .map(|node| node.content.description.clone())
        })
        .unwrap_or_default()
    }
    /// Sets the description of tag `name` in `category` to `description`.
    fn set_tag_description(&mut self, category: String, name: String, description: String) {
        if let Ok(name) = TagName::try_from(name) {
            let cmd = SetTag::new(category, name, Tag { description });
            execute!(self.cnt, cmd);
        }
    }
    /// Changes the category of `tagname` on `old_category` to `new_category`.
    fn change_tag_category(&mut self, old_category: String, new_category: String, tagname: String) {
        if let Ok(tagname) = TagName::try_from(tagname) {
            let cmd = ChangeTagCategory::new(old_category, new_category, tagname);
            execute!(self.cnt, cmd);
        }
    }
    /// Creates the new tag category `name`.
    fn create_category(&mut self, name: String) {
        execute!(self.cnt, CreateCategory::new(name));
    }
    /// Deletes the category `name`.
    ///
    /// This is only possible if the category is already empty (up to the root tag).
    fn delete_category(&mut self, name: String) {
        execute!(self.cnt, DeleteCategory::new(name));
    }
    /// Returns whether `category` contains at least one tag specified
    /// in the comma-separated `taglist`.
    ///
    /// The first existing tag is returned.
    fn one_tag_exists(&self, category: String, taglist: String) -> String {
        let tags = Vec::from_comma_sep(&taglist)
            .into_iter()
            .filter_map(|tagname: String| TagName::try_from(tagname).ok());
        self.query_tree(&category, |cat| {
            for it in tags {
                if cat.node_exists(&it) {
                    return Some(it.to_string());
                }
            }
            None
        })
        .unwrap_or_default()
    }
    /// Returns whether tag `tagname` exists in `category`.
    fn tag_exists(&self, category: String, tagname: String) -> bool {
        self.query_tree(&category, |cat| Some(cat.node_exists(&tagname)))
            .unwrap_or(false)
    }
    /// Returns whether `category` exists.
    fn tag_category_exists(&self, category: String) -> bool {
        self.categories.contains(&category)
    }
    fn reparent_tag(&mut self, category: String, source: String, new_parent: String) -> bool {
        println!("{}, {}, {}", category, source, new_parent);
        if !self.query_tree(&category, |tagtree| Some(tagtree.node_exists(&source) && tagtree.node_exists(&new_parent))).unwrap_or(false) {
            return false;
        }

        if let (Ok(source), Ok(new_parent)) = (TagName::try_from(source), TagName::try_from(new_parent)) {
            let cmd = ReparentTag::new(category, source, new_parent);
            execute!(self.cnt, cmd);
            true
        } else {
            false
        }
    }
    /// Moves the tag specified by `source` via a toml-representation of a
    /// [`TagNamesToml`] to the new parent `target`.
    fn reparent_tag_toml(&mut self, source: String, target: String) -> bool {
        if let (Ok(spec), Ok(target)) = (
            toml::de::from_str::<TagNamesToml>(&source),
            TagName::try_from(target),
        ) {
            for name in spec.names {
                let cmd = ReparentTag::new(spec.category.clone(), name, target.to_owned());
                execute!(self.cnt, cmd);
            }
            true
        } else {
            false
        }
    }
}
