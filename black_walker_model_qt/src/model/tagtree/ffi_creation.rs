//! This module contains the unsafe functions needed for the creation of
//! a [`TagTreeQt`](super::TagTreeQt) on the ffi boundary.

use std::mem;
use std::ops::{Deref, DerefMut};
use std::rc::{Rc, Weak};

use super::{TagTreeMaster, TagTreeQt};

pub struct TagTreeQtCppSide {
    #[allow(unused)]
    phantom: [u8; 0],
}

#[repr(C)]
/// A generic option for a mutable pointer to `T` that has C-representation.
pub struct MCOptionPtrMut<T> {
    data: *mut T,
    some: bool,
}

/*impl<T> From<MCOptionPtrMut<T>> for Option<*mut T> {
    fn from(t: MCOptionPtrMut<T>) -> Option<*mut T> {
        if t.some {
            Some(t.data)
        } else {
            None
        }
    }
}*/

impl<T> From<Option<*mut T>> for MCOptionPtrMut<T> {
    fn from(t: Option<*mut T>) -> MCOptionPtrMut<T> {
        if let Some(v) = t {
            MCOptionPtrMut {
                data: v,
                some: true,
            }
        } else {
            MCOptionPtrMut {
                data: std::ptr::null_mut(),
                some: false,
            }
        }
    }
}

/// A guard collecting the rust and C++ side of a [`TagTreeQt`].
///
/// This is the unsafe ffi-equivalent of an
/// `Rc<UnsafeCell<(TagTreeQt, TagTreeQtCppSide)>>`. You can construct it
/// from its components via [`TagTreeQtGuard::new`], get references
/// to the contained data via its `Deref` and `DerefMut` implementations
/// and finally you __need to drop it manually via [`tag_tree_qt_guard_free`].__
#[repr(C)]
pub struct TagTreeQtGuard {
    pub(super) refcnt: Rc<()>,
    pub(super) cpp_side: *mut TagTreeQtCppSide,
    pub(super) rust_side: *mut TagTreeQt,
}

impl TagTreeQtGuard {
    /// Constructs a new `TagTreeQtGuard` containing the `rust_side`
    /// and `cpp_side` of a [`TagTreeQt`].
    ///
    /// For convenience, a downgraded version is returned at the same
    /// time as a [`TagTreeQtWeak`].
    ///
    /// # Safety
    ///
    /// * All pointers must be valid.
    ///
    /// * `rust_side` must be the true rust side
    /// of the Object held by `cpp_side`.
    pub(super) unsafe fn new(
        cpp_side: *mut TagTreeQtCppSide,
        rust_side: *mut TagTreeQt,
    ) -> (TagTreeQtWeak, TagTreeQtGuard) {
        let refcnt = Rc::new(());
        (
            TagTreeQtWeak {
                refcnt: Rc::downgrade(&refcnt),
                cpp_side,
                rust_side,
            },
            TagTreeQtGuard {
                refcnt,
                cpp_side,
                rust_side,
            },
        )
    }
    /// Downgrades this `TagTreeQtGuard` to a `TagTreeQtWeak`
    /// which may or may not contain a vaild [`TagTreeQt`].
    #[allow(unused)]
    pub(super) fn downgrade(&mut self) -> TagTreeQtWeak {
        TagTreeQtWeak {
            refcnt: Rc::downgrade(&self.refcnt),
            cpp_side: self.cpp_side,
            rust_side: self.rust_side,
        }
    }
}

impl Deref for TagTreeQtGuard {
    type Target = TagTreeQt;
    fn deref(&self) -> &TagTreeQt {
        unsafe { &*self.rust_side }
    }
}

impl DerefMut for TagTreeQtGuard {
    fn deref_mut(&mut self) -> &mut TagTreeQt {
        unsafe { &mut *self.rust_side }
    }
}

#[no_mangle]
/// Returns the C++-side of the tagtreeqt held by `this`.
///
/// No reference counting is done.
///
/// This should be renamed, as calling `free` is _not_ the opposite of calling `acquire`.
///
/// # Safety
///
/// All pointers must be valid. `this` must hold a valid pointer to a C++-side
/// of a `TagTreeQt`.
pub unsafe extern "C" fn tag_tree_qt_guard_acquire(
    this: *mut TagTreeQtGuard,
) -> *mut TagTreeQtCppSide {
    let this = &*this;
    this.cpp_side
}

/// Completely drops the tagtree held by `ptr`, using `destructor` in order to drop its
/// C++ side.
///
/// Since a `TagTreeQt` is composed out of an object allocated by C++ and another one
/// allocated by Rust, we need to call the correspnding destructors for both
/// parts. This can only done via a helper function `destructor` which calls the
/// destructor on the C++-side.
///
/// # Safety
///
/// All pointers must be valid. The parameter `destructor` must do nothing else than
/// calling the destructor of the C++ side of `ptr`.
#[no_mangle]
pub unsafe extern "C" fn tag_tree_qt_guard_free(
    ptr: *mut TagTreeQtGuard,
    destructor: extern "C" fn(*mut TagTreeQtCppSide),
) {
    {
        let this = &mut *ptr;
        this.refcnt = Rc::new(());
        destructor(this.cpp_side);
    }
    mem::drop(Box::from_raw(ptr));
}

/// A weak guard collecting the rust and C++ side of a [`TagTreeQt`].
///
/// This is the unsafe ffi-equivalent of a
/// `Weak<UnsafeCell<(TagTreeQt, TagTreeQtCppSide)>>. It can be obtained
/// from a [`TagTreeQtGuard`] via [`TagTreeQtGuard::downgrade`].
pub(super) struct TagTreeQtWeak {
    pub(super) refcnt: Weak<()>,
    pub(super) cpp_side: *mut TagTreeQtCppSide,
    pub(super) rust_side: *mut TagTreeQt,
}

impl TagTreeQtWeak {
    #[allow(dead_code)]
    /// Get a reference to the contained `TagTreeQt` if the guard is still
    /// valid.
    pub(super) fn get(&self) -> Option<&TagTreeQt> {
        if self.refcnt.upgrade().is_some() {
            unsafe { Some(&*self.rust_side) }
        } else {
            None
        }
    }
    /// Get a mutable reference to the contained `TagTreeQt` if the guard is still
    /// valid.
    pub(super) fn get_mut(&mut self) -> Option<&mut TagTreeQt> {
        if self.refcnt.upgrade().is_some() {
            unsafe { Some(&mut *self.rust_side) }
        } else {
            None
        }
    }
    /// Try to upgrade the weak guard into a full [`TagTreeQtGuard`]
    /// if it is still valid.
    pub(super) fn upgrade(&self) -> Option<TagTreeQtGuard> {
        if let Some(refcnt) = self.refcnt.upgrade() {
            Some(TagTreeQtGuard {
                refcnt,
                cpp_side: self.cpp_side,
                rust_side: self.rust_side,
            })
        } else {
            None
        }
    }
}

/// Tries to get the tagtreemodel for `category`, returning `None`
/// such one is not currently in use.
///
/// # Safety
///
/// * `this` must be valid.
///
/// * `category` must be a valid pointer to a null-terminated utf-8 string.
#[no_mangle]
pub unsafe extern "C" fn tagtree_get_model_of_category(
    this: *mut TagTreeMaster,
    category: *const libc::c_char,
) -> MCOptionPtrMut<TagTreeQtGuard> {
    let this = &mut *this;
    if let Ok(cat) = std::ffi::CStr::from_ptr(category).to_str() {
        this.get_model_of_category(cat).into()
    } else {
        None.into()
    }
}

/// Sets the tagtreemodel which is composed of `tag_tree_qt_cpp_side`
/// and `tag_tree_qt_rust_side` as the active model for `category`
/// in `this`.
///
/// This model is borrowed once and a pointer to the corresponding guard is returned.
/// Destruct this guard properly after being done with its use.
///
/// # Safety
///
/// * All pointers must be valid
///
/// # Panics
///
/// Panics if `category` is not valid Utf-8.
#[no_mangle]
pub unsafe extern "C" fn tagtree_set_model_of_category(
    this: *mut TagTreeMaster,
    category: *const libc::c_char,
    tag_tree_qt_cpp_side: *mut TagTreeQtCppSide,
    tag_tree_qt_rust_side: *mut TagTreeQt,
) -> *mut TagTreeQtGuard {
    let this = &mut *this;
    if let Ok(cat) = std::ffi::CStr::from_ptr(category).to_str() {
        this.set_model_of_category(cat, tag_tree_qt_cpp_side, tag_tree_qt_rust_side)
    } else {
        panic!("Non Utf-8 string given as category!")
    }
}
