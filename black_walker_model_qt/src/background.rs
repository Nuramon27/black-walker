//! Contains the functions which are used by the background thread.

use std::sync::mpsc::{
    Receiver,
    RecvTimeoutError::{Disconnected, Timeout},
};

use black_walker_logic::prelude::*;
use black_walker_logic::read_unstaged;
#[allow(unused_imports)]
use black_walker_logic::store::backup;

/// An operation to be performed by the background thread.
pub enum BackGroundOperation {
    /// Shut down the background thread.
    Quit,
}

/// The main loop of the backgrount thread.
///
/// Makes regular backups of the `diary` with frequency
/// [`GS::backup_interval](black_walker_logic::GS::backup_interval).
/// Otherwise waits for [`BackGroundOperation`]s on `rec`.
pub fn work(mut diary: Diary, rec: Receiver<BackGroundOperation>) {
    let backup_interval = {
        let settings = read_unstaged!(diary, GS).unwrap();
        settings.backup_interval
    };
    loop {
        match rec.recv_timeout(backup_interval) {
            Ok(op) => match op {
                BackGroundOperation::Quit => {
                    break;
                }
            },
            Err(e) => match e {
                Timeout => {
                    #[cfg(not(debug_assertions))]
                    match backup::store_backup(&mut diary, chrono::offset::Utc::now().naive_local())
                    {
                        Ok(()) => (),
                        Err(_) => (),
                    }
                }
                Disconnected => {
                    break;
                }
            },
        }
    }
}
