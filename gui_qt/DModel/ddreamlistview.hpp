#ifndef DDREAMLISTVIEW_HPP
#define DDREAMLISTVIEW_HPP


#include <QTreeView>


#include <QPoint>
#include <QModelIndex>

class QWidget;

#include <QString>
#include <QStringList>

class QContextMenuEvent;

class QMenu;


#include "zdef.hpp"

class DreamListMaster;
class DreamListQt;


class DDreamListView : public QTreeView
{
    Q_OBJECT

    /**
     * @brief Stores the index on which a contextMenuEvent has been requested
     * for findClickedTag();
     */
    QModelIndex lastMenuRequest;
    /**
     * @brief The DDreamListModel to which the view is connected.
     */
    DreamListMaster *dreamlistmodel;

public:
    DDreamListView(DreamListMaster *model, QWidget *parent = nullptr);

    DreamListQt *dreamListModel() const;

protected:
    virtual void contextMenuEvent(QContextMenuEvent *event);

public:
    virtual void setDreamListModel(DreamListMaster *newmodel);

private slots:
    void deleteDream();
};



#endif // DDREAMLISTVIEW_HPP
