#include <bdg/Bindings.h>

#include <QDate>

QModelIndex DreamListQt::computeModelIndexOfNight(QDate night) const
{
    quint32 row = computeRowOfNight(night.year(), night.month(), night.day());
    return createIndex(row, 0);
}

QHash<int, QByteArray> DreamListQt::roleNames() const {
    QHash<int, QByteArray> names = QAbstractItemModel::roleNames();
    names.insert(Qt::UserRole + 0, "Emotionen");
    names.insert(Qt::UserRole + 1, QString::fromUcs4(U"Gegenst\x00e4" "nde").toUtf8());
    names.insert(Qt::UserRole + 2, "Orte");
    names.insert(Qt::UserRole + 3, "Personen");
    names.insert(Qt::UserRole + 4, QString::fromUcs4(U"Pl\x00e4" "tze").toUtf8());
    names.insert(Qt::UserRole + 5, QString::fromUcs4(U"Essen & Getr\x00e4" "nke").toUtf8());
    names.insert(Qt::UserRole + 6, "Traumzeichen");
    names.insert(Qt::UserRole + 7, "Datum");
    names.insert(Qt::UserRole + 8, "date_formal");
    names.insert(Qt::UserRole + 9, "idx");
    names.insert(Qt::UserRole + 10, "Art");
    names.insert(Qt::UserRole + 11, "Name");
    names.insert(Qt::UserRole + 12, "toml");
    return names;
}

void DreamListQt::initHeaderData() {
    m_headerData.insert(qMakePair(0, Qt::DisplayRole), QVariant("Datum"));
    m_headerData.insert(qMakePair(1, Qt::DisplayRole), QVariant("Name"));
    m_headerData.insert(qMakePair(2, Qt::DisplayRole), QVariant("Art"));
    m_headerData.insert(qMakePair(3, Qt::DisplayRole), QVariant("Personen"));
    m_headerData.insert(qMakePair(4, Qt::DisplayRole), QVariant("Orte"));
    m_headerData.insert(qMakePair(5, Qt::DisplayRole), QVariant(QString::fromUcs4(U"Pl\x00e4" "tze")));
    m_headerData.insert(qMakePair(6, Qt::DisplayRole), QVariant("Traumzeichen"));
    m_headerData.insert(qMakePair(7, Qt::DisplayRole), QVariant(QString::fromUcs4(U"Gegenst\x00e4" "nde")));
    m_headerData.insert(qMakePair(8, Qt::DisplayRole), QVariant(QString::fromUcs4(U"Essen & Getr\x00e4" "nke")));
    m_headerData.insert(qMakePair(9, Qt::DisplayRole), QVariant("Emotionen"));
}
