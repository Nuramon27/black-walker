#ifndef TAGTREEMODEL_HPP
#define TAGTREEMODEL_HPP

#include <QObject>


#include <bdg/Bindings.h>
#include <ae.hpp>

class TagTreeQtGuard;

struct MCOptionPtrMut_TagTreeQtGuard {
    TagTreeQtGuard *data;
    bool some;
};

extern "C" MCOptionPtrMut_TagTreeQtGuard tagtree_get_model_of_category(TagTreeMaster::Private *self, const char *category);
extern "C" TagTreeQtGuard *tagtree_set_model_of_category(TagTreeMaster::Private *self, const char *category, TagTreeQt *cpp_side_cpp_side, TagTreeQt::Private *tag_tree_qt_rust_side);

/**
 *  @brief Obtains the tagtreemodel which belongs to category from master.
 *
 * A new model is created if none is readily available.
 */
TagTreeQtGuard *tagtree_model_of_category(TagTreeMaster *master, const QString &category);

#endif // TAGTREEMODEL_HPP
