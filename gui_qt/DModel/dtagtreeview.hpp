#ifndef DTAGTREEVIEW_HPP
#define DTAGTREEVIEW_HPP


#include <QTreeView>


#include <QPoint>
#include <QModelIndex>

class QWidget;

#include <QString>
#include <QStringList>

class QContextMenuEvent;

class QMenu;


class DTagTreeProxyModel;

#include <bdg/Bindings.h>
class TagTreeQtGuard;

extern "C" TagTreeQt *tag_tree_qt_guard_acquire(TagTreeQtGuard *ptr);
extern "C" void tag_tree_qt_guard_free(TagTreeQtGuard *ptr, void (*destructorr)(TagTreeQt*));
void tag_tree_qt_guard_free_simple(TagTreeQtGuard *ptr);


/**
 * @brief The DTagTreeView class displays a DTagTreeModel.
 *
 * It is mostly identical with its base class QTreeView, but provides contextMenus
 * when rightclicking on tags. With the contextMenus one can currently delete a tag or
 * move it to another category.
 */
class DTagTreeView : public QTreeView
{
    Q_OBJECT

    /**
     * @brief Stores the index on which a contextMenuEvent has been requested
     * for findClickedTag();
     */
    QModelIndex lastMenuRequest;
    TagTreeQtGuard *tagtreemodel;
    DTagTreeProxyModel *tagtreeproxy;

public:
    /**
     * @brief Contructs a new DTagTreeView with parent parent.
     */
    DTagTreeView(TagTreeQtGuard *model, QWidget *parent = nullptr);
    virtual ~DTagTreeView();

    TagTreeQtGuard *tagTreeGuard() const;

protected:
    virtual void contextMenuEvent(QContextMenuEvent *event);

public:
signals:
    /**
     * @brief Signals that the user wants to change the category of the tag tagname.
     */
    void demandChangeCategory(QString tagname);
    /**
     * @brief Signals that the user wants to reparent the tag tagname
     */
    void demandReparentTag(QString tagname);
    /**
     * @brief Signals that the user wants to remove the tag tagname
     */
    void demandDeleteTag(QString tagname);
    /**
     * @brief Signals to #worker that it should search for dreams containing the selected tag.
     */
    void demandSearchDreamsWithTag(QString tagname);
    /**
     * @brief Signals that the user wants to edit the tag tagname
     */
    void demandEditTag(QString tagname);

public slots:
    void setTagTreeModel(TagTreeQtGuard *new_model);
};

#endif // DTAGTREEVIEW_HPP
