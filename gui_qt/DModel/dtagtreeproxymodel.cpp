#include "dtagtreeproxymodel.hpp"

#include <bdg/Bindings.h>

DTagTreeProxyModel::DTagTreeProxyModel(QObject *parent) :
    QSortFilterProxyModel(parent)
{

}

void DTagTreeProxyModel::setTagTreeModel(TagTreeQt *new_model)
{
    beginResetModel();
    model = new_model;
    QSortFilterProxyModel::setSourceModel(model);
    endResetModel();
}

bool DTagTreeProxyModel::lessThan(const QModelIndex &source_left, const QModelIndex &source_right) const
{
    if (model == nullptr)
        return true;

    return model->lessThan(source_left.internalId(), source_right.internalId(), source_left.column());
}
