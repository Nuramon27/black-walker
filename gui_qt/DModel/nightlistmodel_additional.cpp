#include <bdg/Bindings.h>


QHash<int, QByteArray> NightListQt::roleNames() const {
    QHash<int, QByteArray> names = QAbstractItemModel::roleNames();
    names.insert(Qt::UserRole + 0, "KW");
    names.insert(Qt::UserRole + 1, "Nacht");
    names.insert(Qt::UserRole + 2, QString::fromUcs4(U"Tr\x00e4" "ume").toUtf8());
    names.insert(Qt::UserRole + 3, "Wochentag");
    names.insert(Qt::UserRole + 4, "dateFormal");
    return names;
}

void NightListQt::initHeaderData() {
    m_headerData.insert(qMakePair(0, Qt::DisplayRole), QVariant("Wochentag"));
    m_headerData.insert(qMakePair(1, Qt::DisplayRole), QVariant("Nacht"));
    m_headerData.insert(qMakePair(2, Qt::DisplayRole), QVariant(QString::fromUcs4(U"Tr\xe4" "ume")));
    m_headerData.insert(qMakePair(3, Qt::DisplayRole), QVariant("KW"));
}
