#ifndef DTAGTREEPROXYMODEL_HPP
#define DTAGTREEPROXYMODEL_HPP

#include <QSortFilterProxyModel>


class TagTreeQt;


class DTagTreeProxyModel : public QSortFilterProxyModel
{
    TagTreeQt *model;

public:
    DTagTreeProxyModel(QObject *parent = nullptr);

    void setTagTreeModel(TagTreeQt *new_model);

protected:
    virtual bool lessThan(const QModelIndex &source_left, const QModelIndex &source_right) const override;
};

#endif // DTAGTREEPROXYMODEL_HPP
