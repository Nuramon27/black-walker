#include "dtagtreeview.hpp"


#include <QContextMenuEvent>

#include <QTreeView>

#include <QMenu>
#include <QAction>


#include "dtagtreeproxymodel.hpp"


#include <bdg/Bindings.h>

void tag_tree_qt_guarddtor(TagTreeQt *to_destruct)
{
    delete to_destruct;
}

void tag_tree_qt_guard_free_simple(TagTreeQtGuard *ptr)
{
    tag_tree_qt_guard_free(ptr, tag_tree_qt_guarddtor);
}

DTagTreeView::DTagTreeView(TagTreeQtGuard *model, QWidget *parent) :
    QTreeView(parent),
    lastMenuRequest(),
    tagtreemodel(model),
    tagtreeproxy(new DTagTreeProxyModel(this))
{
    TagTreeQt *ttqt = tag_tree_qt_guard_acquire(model);
    tagtreeproxy->setTagTreeModel(ttqt);
    setModel(tagtreeproxy);
    setAutoScrollMargin(48);
}

DTagTreeView::~DTagTreeView()
{
    tag_tree_qt_guard_free_simple(tagtreemodel);
}


//MEMBER FUNCTIONS
TagTreeQtGuard *DTagTreeView::tagTreeGuard() const
{
    return tagtreemodel;
}

void DTagTreeView::contextMenuEvent(QContextMenuEvent *event)
{
    event->accept();
    lastMenuRequest = indexAt(event->pos());
    if (!lastMenuRequest.isValid())
        return;
    QMenu contextMenu(this);

    contextMenu.addAction(QStringLiteral("Tr\x00E4" "ume anzeigen"),
                          this, [=](){
        if (lastMenuRequest.isValid())
            emit demandSearchDreamsWithTag(model()->data(lastMenuRequest, Qt::DisplayRole).toString());

        lastMenuRequest = QModelIndex();
    });
    contextMenu.addAction(QStringLiteral("In Gruppe verschieben"),
                          this, [=](){
        if (lastMenuRequest.isValid())
            emit demandReparentTag(model()->data(lastMenuRequest, Qt::DisplayRole).toString());

        lastMenuRequest = QModelIndex();
    });
    contextMenu.addAction(QString::fromUcs4(U"Kategorie \x00C4" "ndern"),
                          this, [=](){
        if (lastMenuRequest.isValid())
            emit demandChangeCategory(model()->data(lastMenuRequest, Qt::DisplayRole).toString());

        lastMenuRequest = QModelIndex();
    });
    contextMenu.addAction(QString::fromUcs4(U"Tag l\x00F6" "schen"),
                          this, [=](){
        if (lastMenuRequest.isValid())
            emit demandDeleteTag(model()->data(lastMenuRequest, Qt::DisplayRole).toString());

        lastMenuRequest = QModelIndex();
    });
    contextMenu.addAction(QStringLiteral("Tag bearbeiten"),
            this, [=](){
        if (lastMenuRequest.isValid())
            emit demandEditTag(model()->data(lastMenuRequest, Qt::DisplayRole).toString());

        lastMenuRequest = QModelIndex();
    });
    contextMenu.exec(mapToGlobal(event->pos()));
}

//PUBLIC SLOTS
void DTagTreeView::setTagTreeModel(TagTreeQtGuard *new_model)
{
    tag_tree_qt_guard_free_simple(tagtreemodel);
    tagtreemodel = new_model;
    TagTreeQt *ttqt = tag_tree_qt_guard_acquire(new_model);
    setModel(ttqt);
}
