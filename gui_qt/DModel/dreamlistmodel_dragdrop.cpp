#include <bdg/Bindings.h>


#include <QMimeData>

QStringList DreamListQt::mimeTypes() const
{
    return QStringList({"text/plain"});
}

Qt::DropActions DreamListQt::supportedDragActions() const
{
    return Qt::MoveAction;
}


Qt::DropActions DreamListQt::supportedDropActions() const
{
    return Qt::MoveAction;
}


Qt::ItemFlags DreamListQt::flags(const QModelIndex &index) const
{
    if (index.isValid())
        return (Qt::ItemIsSelectable | Qt::ItemIsEnabled
                | Qt::ItemIsDragEnabled | Qt::ItemIsDropEnabled);
    else
        return Qt::ItemIsDropEnabled;
}

QMimeData *DreamListQt::mimeData(const QModelIndexList &indexes) const
{
    QMimeData *data = new QMimeData();
    QStringList ids;

    /* Obtain the tags lying under indexes
     * */
    for (int i = 0; i < indexes.size(); ++i)
    {
        if (indexes.at(i).isValid() && indexes.at(i).column() == 0)
        {
            ids << "[[ids]]\n";
            ids << toml(indexes.at(i).row());
        }
    }

    /* Encode them as latex command
     * */
    data->setText(ids.join('\n'));

    return data;
}

bool DreamListQt::canDropMimeData(const QMimeData *data, Qt::DropAction action,
                                    int row, int column, const QModelIndex &parent) const
{
    Q_UNUSED(column)
    if (action != Qt::MoveAction)
        return false;

    if (!(data->hasFormat(QStringLiteral("text/plain")) || data->hasFormat(QStringLiteral("text/x-latex"))))
        return false;

    /* Ensure that only valid rows are used. Especially if drops directly on
     * an item (i.e. row == -1) should be prevented
     * */
    if (row < 0 || row > rowCount())
        return false;

    if (parent.isValid())
        return false;

    QString text(data->text());
    return canDropToml(text, row);
}

bool DreamListQt::dropMimeData(const QMimeData *data, Qt::DropAction action,
                                 int row, int column, const QModelIndex &parent)
{
    /* Check if the data may be dropped here
     * */
    if (!canDropMimeData(data, action, row, column, parent))
        return false;

    if (action == Qt::IgnoreAction)
        return true;

    QString source(data->text());
    return dropToml(source, row);
}
