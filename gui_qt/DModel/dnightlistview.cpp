#include "dnightlistview.hpp"

#include <QMenu>
#include <QContextMenuEvent>

#include <bdg/Bindings.h>

DNightListView::DNightListView(DreamListMaster *model, QWidget *parent) :
    QTreeView(parent),
    dreamlistmodel(model)
{
    setModel(dreamlistmodel->night_list_qt());
}


void DNightListView::contextMenuEvent(QContextMenuEvent *event)
{
    lastMenuRequest = indexAt(event->pos());
    if (!lastMenuRequest.isValid())
        return;
    QMenu contextMenu(this);

    contextMenu.addAction(QString::fromUcs4(U"Nachtinformationen l\x00F6" "schen"),
                          this, &DNightListView::deleteNightInformation);
    contextMenu.exec(mapToGlobal(event->pos()));
    event->accept();
}

void DNightListView::setDreamListModel(DreamListMaster *newmodel)
{
    dreamlistmodel = newmodel;
    setModel(dreamlistmodel->night_list_qt());
}

void DNightListView::deleteNightInformation()
{
    if (lastMenuRequest.isValid())
    {
        dreamlistmodel->deleteNightInformation(lastMenuRequest.row());
    }
    lastMenuRequest = QModelIndex();
}
