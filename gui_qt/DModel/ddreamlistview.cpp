#include "ddreamlistview.hpp"


#include <QContextMenuEvent>

#include <QMenu>
#include <QAction>


#include "aglobalsettings.hpp"

#include <bdg/Bindings.h>


DDreamListView::DDreamListView(DreamListMaster *model, QWidget *parent) :
    QTreeView(parent),
    lastMenuRequest(),
    dreamlistmodel(model)
{
    setModel(model->dream_list_qt());
    setDragEnabled(true);
    setAcceptDrops(true);
    setSelectionMode(QAbstractItemView::SingleSelection);
    setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
    setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);
}

void DDreamListView::contextMenuEvent(QContextMenuEvent *event)
{
    event->accept();
    lastMenuRequest = indexAt(event->pos());
    if (!lastMenuRequest.isValid())
        return;
    QMenu contextMenu(this);

    contextMenu.addAction(QString::fromUcs4(U"Traum l\x00F6" "schen"),
                          this, &DDreamListView::deleteDream);
    contextMenu.exec(mapToGlobal(event->pos()));
}

DreamListQt *DDreamListView::dreamListModel() const
{
    return dreamlistmodel->dream_list_qt();
}

void DDreamListView::setDreamListModel(DreamListMaster *newmodel)
{
    dreamlistmodel = newmodel;
    QTreeView::setModel(newmodel->dream_list_qt());
}


//PRIVATE SLOTS
void DDreamListView::deleteDream()
{
    if (lastMenuRequest.isValid())
        dreamlistmodel->delete_dream(lastMenuRequest.row());

    lastMenuRequest = QModelIndex();
}
