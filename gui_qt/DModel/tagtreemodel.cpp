#include "tagtreemodel.hpp"

TagTreeQtGuard *tagtree_model_of_category(TagTreeMaster *master, QString const& category)
{
    MCOptionPtrMut_TagTreeQtGuard model_try = tagtree_get_model_of_category(master->m_d, category.toUtf8().data());
    if (model_try.some)
    {
        return model_try.data;
    }
    else
    {
        TagTreeQt *tag_tree_qt = new TagTreeQt(nullptr);
        TagTreeQtGuard *res =  tagtree_set_model_of_category(master->m_d, category.toUtf8().data(), tag_tree_qt, tag_tree_qt->m_d);
        return res;
    }
}

QHash<int, QByteArray> TagTreeQt::roleNames() const {
    QHash<int, QByteArray> names = QAbstractItemModel::roleNames();
    names.insert(Qt::UserRole + 0, "Name");
    names.insert(Qt::UserRole + 1, "ParentName");
    names.insert(Qt::UserRole + 2, "Anzahl");
    names.insert(Qt::UserRole + 3, "Erwähnt");
    return names;
}

void TagTreeQt::initHeaderData() {
    m_headerData.insert(qMakePair(0, Qt::DisplayRole), QVariant("Name"));
    m_headerData.insert(qMakePair(1, Qt::DisplayRole), QVariant("Anzahl"));
    m_headerData.insert(qMakePair(2, Qt::DisplayRole), QVariant("Erwähnt"));
}
