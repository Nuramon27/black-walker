#ifndef DNIGHTLISTVIEW_HPP
#define DNIGHTLISTVIEW_HPP

#include <QTreeView>

class QContextMenuEvent;

class DreamListMaster;


class DNightListView : public QTreeView
{
    Q_OBJECT

    /**
     * @brief Stores the index on which a contextMenuEvent has been requested
     * for findClickedTag();
     */
    QModelIndex lastMenuRequest;
    /**
     * @brief The DDreamListModel to which the view is connected.
     */
    DreamListMaster *dreamlistmodel;

public:
    DNightListView(DreamListMaster *model, QWidget *parent = nullptr);

protected:
    virtual void contextMenuEvent(QContextMenuEvent *event);

public:
    virtual void setDreamListModel(DreamListMaster *newmodel);

private slots:
    void deleteNightInformation();
};

#endif // DNIGHTLISTVIEW_HPP
