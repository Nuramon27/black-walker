#include <bdg/Bindings.h>


#include <QMimeData>


#include "zdef.hpp"


QStringList TagTreeQt::mimeTypes() const
{
    return QStringList({"text/plain"});
}

Qt::DropActions TagTreeQt::supportedDragActions() const
{
    return Qt::MoveAction;
}


Qt::DropActions TagTreeQt::supportedDropActions() const
{
    return Qt::MoveAction;
}

Qt::ItemFlags TagTreeQt::flags(const QModelIndex &index) const
{
    if (index.isValid())
    {
        switch (index.column())
        {
        case 0:
            return (Qt::ItemIsSelectable | Qt::ItemIsEnabled
                    | ((editable())? Qt::ItemIsEditable : Qt::NoItemFlags)
                    | Qt::ItemIsDragEnabled | Qt::ItemIsDropEnabled);
        case 1:
        case 2:
            return (Qt::ItemIsSelectable | Qt::ItemIsEnabled
                    | Qt::ItemIsDragEnabled | Qt::ItemIsDropEnabled);
        default:
            return (Qt::ItemIsDropEnabled);
        }
    }
    else
        return (Qt::ItemIsDropEnabled);
}

QMimeData *TagTreeQt::mimeData(const QModelIndexList &indexes) const
{
    QMimeData *data = new QMimeData();
    QStringList tags;

    /* Obtain the tags lying under indexes
     * */
    for (int i = 0; i < indexes.size(); ++i)
    {
        if (indexes.at(i).isValid() && indexes.at(i).column() == 0)
        {
            tags << Name(indexes.at(i));
        }
    }

    /* Encode them as latex command
     * */
    data->setText(QStringLiteral("category = \"") + Category() + QStringLiteral("\"\nnames = [\"")
                 + tags.join(QStringLiteral("\", \"")) + QStringLiteral("\"]\n"));

    return data;
}

bool TagTreeQt::canDropMimeData(const QMimeData *data, Qt::DropAction action,
                                    int row, int column, const QModelIndex &parent) const
{
    if (action != Qt::MoveAction)
        return false;

    if (!(data->hasFormat(QStringLiteral("text/plain"))))
        return false;

    if (parent.isValid() && row != -1)
        return false;

    QString text(data->text());
    return canDropToml(text, parent.internalId());
}

bool TagTreeQt::dropMimeData(const QMimeData *data, Qt::DropAction action,
                                 int row, int column, const QModelIndex &parent)
{
    if (action == Qt::IgnoreAction)
        return true;

    /* Check if the data may be dropped here
     * */
    if (!canDropMimeData(data, action, row, column, parent))
        return false;

    QString source(data->text());
    return dropToml(source, parent.internalId());
}
