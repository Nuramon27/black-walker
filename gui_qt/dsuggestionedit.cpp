#include "dsuggestionedit.hpp"


#include <QCompleter>

#include <QFocusEvent>

#include <QStringListModel>
#include <QListView>


#include <QRegExp>
#include <QRegExpValidator>
#include <QStringBuilder>


DSuggestionEdit::DSuggestionEdit(Master *master,
                                 AE::DEntityType type, AE::DEnterTypeFlags mode,
                                 AE::DAllowedFlags allowedentities,
                                 QWidget *parent, QString newcategory) :
    QLineEdit(parent),
    itext(),
    icursorposition(0),
    suggestion_model(new SuggestionQt(this))
{
    init_suggestion(master->m_d, suggestion_model->m_d);
    suggestion_model->setEnter_type(static_cast<quint8>(mode));
    suggestion_model->setTo_suggest(static_cast<quint8>(type));
    suggestion_model->setAllowed(static_cast<quint8>(allowedentities));
    suggestion_model->setCategory(newcategory);
    /* We use a QCompleter suggestioncomp that is connected to this QLineEdit for displaying
     * the suggestions we found, but not for searching for the suggestions -- this is done
     * by worker
     *
     * So what we do is we construct a QCompleter and set its CompletionMode to unfiltered,
     * so it displays all possible completions we give to it. But instead of giving a static list
     * of tags or entries to it, we provide a QStringListModel suggestionmodel that we handle ourselves.
     *
     * Every time suggestions are demanded, we feed the QStringListModel with the suggestions worker
     * has found and let the suggestioncomp display them. It then displays exactly those
     * found suggestions, as we set its mode to unfiltered.
     * */
    suggestioncomp = new QCompleter(suggestion_model);
    suggestioncomp->setCompletionMode(QCompleter::UnfilteredPopupCompletion);
    setCompleter(suggestioncomp);

    QString forbiddenchars(QStringLiteral("\\\\,;:\"\'{}\\$%&"));
    if (mode & AE::DEnterType::COMMASEP)
        forbiddenchars.remove(QChar(','));
    if (mode & AE::DEnterType::ANDSEP)
        forbiddenchars.remove(QChar('&'));
    if (mode & AE::DEnterType::MULTIPLEUNITS)
        forbiddenchars.remove(QChar(';'));
    if (mode & (AE::DEnterTypeFlags(AE::DEnterType::WITHCATEGORY) | AE::DEnterTypeFlags(AE::DEnterType::WITHGROUPS)))
        forbiddenchars.remove(QChar(':'));
    if (mode & AE::DEnterType::CONDITION)
    {
        forbiddenchars.remove(QChar(':'));
        forbiddenchars.remove(QChar('>'));
        forbiddenchars.remove(QChar('='));
    }
    switch(type)
    {
    case AE::DEntityType::TAGCATEGORY:
    case AE::DEntityType::ENTRYCATEGORY:
        forbiddenchars += QStringLiteral("&|\\^(==)!\\(\\)\\[\\]");
        break;
    default:;
    }
    setValidator(new QRegExpValidator(QRegExp(QStringLiteral("^[^") % forbiddenchars % QStringLiteral("]*$")),
                                      this));

    connect(this, &DSuggestionEdit::textEdited, this, &DSuggestionEdit::loadSuggestions);
    connect(suggestion_model, &SuggestionQt::wakeupChanged, suggestion_model, &SuggestionQt::await_suggestions);
    connect(suggestion_model, &SuggestionQt::modelReset, suggestioncomp, [=](){suggestioncomp->complete();});

    /*QObject::connect(suggestioncomp, qOverload<QString const &>(&QCompleter::activated),
                     this, &DSuggestionEdit::complete);
    QObject::connect(suggestioncomp, qOverload<QString const &>(&QCompleter::highlighted),
                     this, &DSuggestionEdit::chooseCompletion);*/
}

DSuggestionEdit::~DSuggestionEdit()
{
    delete suggestion_model;
}

///MEMBER FUNCTIONS
void DSuggestionEdit::focusInEvent(QFocusEvent *event)
{
    QLineEdit::focusInEvent(event);
    /* The QCompleter connects automatically slots that insert chosen suggestions
     * every time we recieve focus. But we want to customize this behaviour:
     * If AE::DEnterType is COMMASEP, when the user chooses a suggestion
     * only the entity that is currently edited shall be replaced
     * by the chosen suggestion (QCompleter would replace the whole text).
     *
     * So we disconnect those signals and instead connect our own signals,
     * namely complete and chooseCompletion.
     * */
    suggestioncomp->disconnect(SIGNAL(activated(QString const &)));
    suggestioncomp->disconnect(SIGNAL(activated(QModelIndex const &)));
    suggestioncomp->disconnect(SIGNAL(highlighted(QString const &)));
    suggestioncomp->disconnect(SIGNAL(highlighted(QModelIndex const &)));
    //disconnect(suggestioncomp->popup(), 0, this, 0);

    connect(suggestioncomp, QOverload<QString const &>::of(&QCompleter::activated),
                this, &DSuggestionEdit::complete);
    connect(suggestioncomp, QOverload<QString const &>::of(&QCompleter::highlighted),
                this, &DSuggestionEdit::chooseCompletion);
}

void DSuggestionEdit::focusOutEvent(QFocusEvent *event)
{
    QLineEdit::focusOutEvent(event);
    disconnect(suggestioncomp, QOverload<QString const &>::of(&QCompleter::activated),
                this, &DSuggestionEdit::complete);
    disconnect(suggestioncomp, QOverload<QString const &>::of(&QCompleter::highlighted),
                this, &DSuggestionEdit::chooseCompletion);
}


///SLOTS
void DSuggestionEdit::loadSuggestions()
{
    itext = text();
    icursorposition = cursorPosition();
    suggestion_model->suggest(text(), static_cast<quint32>(cursorPosition()));
}

void DSuggestionEdit::complete(const QString &chosensuggestion)
{
    QString currenttext = text();
    int beginofentity = static_cast<int>(suggestion_model->fragment_start(currenttext, static_cast<quint32>(cursorPosition())));
    int endofentity = static_cast<int>(suggestion_model->fragment_end(currenttext, static_cast<quint32>(cursorPosition())));

    currenttext.replace(beginofentity, endofentity-beginofentity, chosensuggestion);

    QString addendum = (AE::DEnterTypeFlags(suggestion_model->enter_type()) & AE::DEnterType::COMMASEP)? QStringLiteral(", ") : QString();
    itext = QString();
    icursorposition = cursorPosition();
    setText(currenttext + addendum);
    setCursorPosition(beginofentity + chosensuggestion.length() + addendum.length());
}

void DSuggestionEdit::chooseCompletion(const QString &chosensuggestion)
{
    if (!suggestion_model->contains(chosensuggestion))
    {
        setText(itext);
        setCursorPosition(icursorposition);
        return;
    }

    QString currenttext = text();
    int beginofentity = static_cast<int>(suggestion_model->fragment_start(currenttext, static_cast<quint32>(cursorPosition())));
    int endofentity = static_cast<int>(suggestion_model->fragment_end(currenttext, static_cast<quint32>(cursorPosition())));

    currenttext.replace(beginofentity, endofentity-beginofentity, chosensuggestion);

    setText(currenttext);
    setSelection(beginofentity, chosensuggestion.length());
}

void DSuggestionEdit::setCategory(const QString &newcategory)
{
    suggestion_model->setCategory(newcategory);
}
