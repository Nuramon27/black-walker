#include "ddreamform.hpp"


#include <QFont>

#include <QPushButton>
#include <QCheckBox>
#include <QLineEdit>
#include <QTextEdit>
#include <QTimeEdit>
#include <QComboBox>
#include <QLabel>
#include <QGroupBox>

#include <QMessageBox>

#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QGridLayout>
#include <QStackedWidget>
#include <QTabWidget>


#include <mdateeditopt.hpp>
#include <mspinboxslider.hpp>


#include "aglobalsettings.hpp"

#include <bdg/Bindings.h>

#include <DDialogs/dunknowntagsdialog.hpp>
#include <DDialogs/dnightdialog.hpp>

#include <DTagEdit/dabstracttagedit.hpp>
#include <DTagEdit/dtagedit.hpp>
#include <DTagEdit/dstatictagedit.hpp>
#include "dsuggestionedit.hpp"
#include <Enum/dallowed.hpp>
#include <Enum/dentitytype.hpp>
#include <Enum/dentertype.hpp>



DDreamForm::DDreamForm(Master *newmaster, QWidget *parent) :
    QWidget(parent),
    checkboxnames({
        QStringLiteral("Land"),
        QStringLiteral("Stadt"),
        QStringLiteral("Drinnen"),
        QString::fromUcs4(U"Drau\x00DF" "en"),
        QStringLiteral("Tag"),
        QStringLiteral("Nacht")
    }),
    slidernames({
        QString::fromUcs4(U"Intensit\x00e4" "t"),
        QStringLiteral("Erinnerung"),
        QString::fromUcs4(U"L\x00e4" "nge"),
        QStringLiteral("Phantastischkeit"),
        QStringLiteral("Angst"),
        QString::fromUcs4(U"Pers\x00f6" "nliche Relevanz")
    }),
    lucidslidernames({
        QString::fromUcs4(U"Stabilit\x00e4" "t"),
        QStringLiteral("Klarheit"),
    }),
    type(OpenType::NEW),
    master(newmaster),
    model(newmaster->dream_form_qt()),
    tagtree_master(newmaster->tag_tree_master())
{
    //Create the buttons
    okbutton = new QPushButton(QString::fromUcs4(U"Speichern && Schlie\xDF" "en"), this);
    savebutton = new QPushButton("Zwischenspeichern", this);
    changebutton = new QPushButton("Bearbeiten", this);
    closebutton = new QPushButton(QString::fromUcs4(U"Schlie\xDF" "en"), this);
    okorclosebutton = new QStackedWidget(this);
    okorclosebutton->addWidget(closebutton);
    okorclosebutton->addWidget(okbutton);
    saveorchangebutton = new QStackedWidget(this);
    saveorchangebutton->addWidget(changebutton);
    saveorchangebutton->addWidget(savebutton);
    cancelbutton = new QPushButton("Abbrechen", this);

    //Create the edits for editing the entries
    datelabel = new QLabel("Datum:", this);
    namelabel = new QLabel("Titel:", this);
    kindlabel = new QLabel("Art:", this);
    techniquelabel = nullptr;
    nightsectionlabel = new QLabel(QStringLiteral("Schlafphase: "));
    sliderlabels = QList<QLabel*>({
        new QLabel(QString::fromUcs4(U"Intensit\x00e4" "t")),
        new QLabel(QStringLiteral("Erinnerung")),
        new QLabel(QString::fromUcs4(U"L\x00e4" "nge")),
        new QLabel(QStringLiteral("Phantastischkeit")),
        new QLabel(QStringLiteral("Angst")),
        new QLabel(QStringLiteral("Pers. Relevanz")),
    });
    lucidsliderlabels = QList<QLabel*>({
        nullptr,
        nullptr,
    });
    authored_place_label = new QLabel(QStringLiteral("Verfasst in: "));
    authored_date_label = new QLabel(QStringLiteral("Verfasst am: "));
    authored_time_label = new QLabel(QStringLiteral("Verfasst um: "));
    textlabel = new QLabel("Text:", this);
    noteslabel = new QLabel("Anmerkungen:", this);

    dateedit = new MDateEditOpt(QDate::currentDate(), this);
    dateedit->setCalendarPopup(true);
    nameentry = new QLineEdit(this);
    nameentry->setValidator(new QRegExpValidator(
                                QRegExp(QStringLiteral("^[^\\\\\"\'{}\\$%]*$")),
                                nameentry));
    kindcombobox = new QComboBox(this);
    kindcombobox->setEditable(true);
    kindcombobox->setValidator(
                new QRegExpValidator(QRegExp(QStringLiteral("^[^\\\\,;:\"\'{}\\$%]*$")),
                                     kindcombobox));
    kindcombobox->setCurrentText(AGlobalSettings::darkdreamstring);
    kindcombobox->lineEdit()->setPlaceholderText(QString("Benutzerdefiniert"));
    connect(kindcombobox->lineEdit(), &QLineEdit::editingFinished,
            kindcombobox->lineEdit(), &QLineEdit::returnPressed);
    AE::connect_combobox_with_model(*kindcombobox, *master->entry_master()->kind_system());
    nightsection_combobox = new QComboBox();
    // Initializes the sightsection_combobox;
    react_to_date_change();
    editnight_button = new QPushButton(QStringLiteral("Nacht bearbeiten"));
    techniqueentry = nullptr;
    sliders = QHash<QString, MSpinBoxSlider*>();
    auto entrymaster = master->entry_master();
    for (auto slidername = slidernames.cbegin(); slidername != slidernames.cend(); ++slidername)
    {
        sliders.insert(*slidername, new MSpinBoxSlider(
            entrymaster->rangeUStart(*slidername), entrymaster->rangeUEnd(*slidername),
            Qt::Horizontal, this
        ));
    }
    for (auto slidername = lucidslidernames.cbegin(); slidername != lucidslidernames.cend(); ++slidername)
    {
        lucidsliders.insert(*slidername, nullptr);
    }

    authored_date_edit = new MDateEditOpt(QDate::currentDate());
    authored_time_edit = new QTimeEdit(QTime::currentTime());
    authored_place_edit = new QLineEdit(master->settings_qt()->placeLastUsed());

    setTabOrder(dateedit, nameentry);
    setTabOrder(nameentry, kindcombobox);
    setTabOrder(kindcombobox, techniqueentry);

    /* For every standart tag category insert a tag edit
     * */
    SettingsQt *set_model = master->settings_qt();
    for (int i = 0; i < set_model->nrStandardTagCategories(); ++i)
    {
        tagentries.append(new DStaticTagEdit(master, set_model->standardTagCategory(i), this));
        if (i >= 1)
            setTabOrder(tagentries[i-1]->getTagEntry(), tagentries[i]->getTagEntry());
    }
    /* Finally append one custom tag edit
     * */
    tagentries.append(new DTagEdit(master, this));
    model->tag()->pushRow();
    if (tagentries.size() >= 2)
        setTabOrder(tagentries.at(tagentries.size()-1)->getTagEntry(), tagentries.last()->getTagEntry());
    taggroup = new QGroupBox("Tags", this);
    setTabOrder(techniqueentry, taggroup);

#if defined(Q_OS_LINUX)
    QFont font;
    QFont::insertSubstitutions(QStringLiteral("Linux Libertine O"),
                               QStringList({"Vollkorn", "DejaVu Serif", "Times New Roman", "Times", "FreeSerif"}));
    font.setStyleHint(QFont::Serif, QFont::PreferOutline);
    font.setPointSize(14);
    font.setFamily(QStringLiteral("Linux Libertine O"));
#elif defined(Q_OS_WIN)
    QFont font;
    QFont::insertSubstitutions(QStringLiteral("Linux Libertine O"),
                               QStringList({"Vollkorn", "Times New Roman", "Times", "DejaVu Serif"}));
    font.setStyleHint(QFont::Serif, QFont::PreferOutline);
    font.setPointSize(14);
    font.setFamily(QStringLiteral("Linux Libertine O"));
#endif //OS
    textentry = new QTextEdit(this);
    textentry->setFont(font);
    notesdateedit = new MDateEditOpt(QDate::currentDate());
    notestimeedit = new QTimeEdit(QTime::currentTime());
    notesplaceedit = new QLineEdit();
    addnotesbutton = new QPushButton(QStringLiteral("Neue Anmerkung"));
    notescombobox = new QComboBox();
    notesentry = new QTextEdit(this);
    notesentry->setFont(font);


    entrybox = new QGridLayout();
    entrybox->addWidget(datelabel, 0, 0);
    entrybox->addWidget(namelabel, 1, 0);
    entrybox->addWidget(kindlabel, 2, 0);
    entrybox->addWidget(nightsectionlabel, 3, 0);
    entrybox->addWidget(editnight_button, 4, 0, 1, 4);
    {
        int nr = 0;
        for (auto slider = sliderlabels.begin(); slider != sliderlabels.end(); ++slider, ++nr)
        {
            addSliderLabelToBox(*slider, nr, SliderType::Normal);
        }
    }
    entrybox->addWidget(dateedit, 0, 1, 1, 3);
    entrybox->addWidget(nameentry, 1, 1, 1, 3);
    entrybox->addWidget(kindcombobox, 2, 1, 1, 3);
    entrybox->addWidget(nightsection_combobox, 3, 1, 1, 3);
    {
        int nr = 0;
        for (auto slidername = slidernames.begin(); slidername != slidernames.end(); ++slidername, ++nr)
        {
            addSliderToBox(sliders.value(*slidername), nr, SliderType::Normal);
        }
    }
    entrygroup = new QGroupBox(QStringLiteral("Details"));
    entrygroup->setLayout(entrybox);

    tagbox = new QGridLayout();
    /* for the tagentries, we use the addToGrid function, which distributes them to columns
     * 0 to 2 of the tagbox
     * */
    for(int i = 0; i < tagentries.size(); ++i)
        tagentries[i]->addToGrid(tagbox, i, 0);
    taggroup->setLayout(tagbox);
    generaltaglabel = new QLabel("Tags", this);
    generaltagentry = new DSuggestionEdit(master,
                                          AE::DEntityType::TAG,
                                          AE::DEnterType::TAGSWITHGROUP,
                                          AE::DAllowed::LEAF,
                                          this);


    checkboxbox = new QHBoxLayout();
    for (auto cbname = checkboxnames.begin(); cbname != checkboxnames.end(); ++cbname)
    {
        auto cb = new QCheckBox(*cbname);
        checkboxes.append(cb);
        checkboxbox->addWidget(cb);
    }

    leftbox = new QVBoxLayout();
    leftbox->addWidget(entrygroup);
    leftbox->addStretch();
    rightbox = new QVBoxLayout();
    rightbox->addWidget(taggroup);
    rightbox->addStretch();
    rightbox->addLayout(checkboxbox);

    entitybox = new QHBoxLayout();
    entitybox->addLayout(leftbox, 0x80);
    entitybox->addLayout(rightbox, 0x80);

    textbox = new QVBoxLayout();
    textbox->addWidget(textlabel);
    textbox->addWidget(textentry);

    notescontrolbox = new QHBoxLayout();
    notescontrolbox->addWidget(notescombobox, 10);
    notescontrolbox->addWidget(notesdateedit, 7);
    notescontrolbox->addWidget(notestimeedit, 7);
    notescontrolbox->addWidget(notesplaceedit, 25);
    notescontrolbox->addWidget(addnotesbutton, 25);
    notesbox = new QVBoxLayout();
    notesbox->addWidget(noteslabel);
    notesbox->addLayout(notescontrolbox);
    notesbox->addWidget(notesentry);

    entitywidget = new QWidget();
    entitywidget->setLayout(entitybox);
    textwidget = new QWidget();
    textwidget->setLayout(textbox);
    textwidget->setContentsMargins(0x40, 0x04, 0x40, 0x04);
    noteswidget = new QWidget();
    noteswidget->setLayout(notesbox);
    noteswidget->setContentsMargins(0x40, 0x04, 0x40, 0x04);

    contentwidget = new QTabWidget();
    contentwidget->addTab(entitywidget, QStringLiteral("Grundlegendes"));
    contentwidget->addTab(textwidget, QStringLiteral("Text"));
    contentwidget->addTab(noteswidget, QStringLiteral("Anmerkungen"));

    generaltagbox = new QHBoxLayout();
    generaltagbox->addWidget(generaltaglabel);
    generaltagbox->addWidget(generaltagentry);

    buttonbox = new QHBoxLayout();
    buttonbox->addWidget(cancelbutton);
    buttonbox->addStretch();
    buttonbox->addWidget(saveorchangebutton);
    buttonbox->addWidget(okorclosebutton);

    authored_box = new QGridLayout();
    authored_box->addWidget(authored_date_label, 0, 0);
    authored_box->addWidget(authored_time_label, 0, 2);
    authored_box->addWidget(authored_place_label, 0, 4);
    authored_box->addWidget(authored_date_edit, 0, 1);
    authored_box->addWidget(authored_time_edit, 0, 3);
    authored_box->addWidget(authored_place_edit, 0, 5);
    rightbox->addLayout(authored_box);

    mainbox = new QVBoxLayout();
    mainbox->addWidget(contentwidget, 1);
    mainbox->addLayout(generaltagbox, 0);
    mainbox->addLayout(buttonbox, 0);

    connect(model, &DreamFormQt::begin_cmdChanged, master, &Master::begin_cmd);
    connect(model, &DreamFormQt::end_cmdChanged, master, &Master::end_cmd);

    connect(model->night_qt(), &NightQt::begin_cmdChanged, master, &Master::begin_cmd);
    connect(model->night_qt(), &NightQt::end_cmdChanged, master, &Master::end_cmd);
    connect(model, &DreamFormQt::has_idChanged, this, [this]() {
        if (model->has_id())
        {
            if (this->type == OpenType::NEW)
                this->setMode(OpenType::WRITE);
        }
        else
        {
            this->setMode(OpenType::NEW);
        }
    });
    connect(dateedit, &QDateEdit::dateChanged, this, &DDreamForm::react_to_date_change);

    // This ensures that the displayed note is always the one of the date
    // presented in the `notesdateedit`.
    connect(notesdateedit, &QDateEdit::dateChanged, this, &DDreamForm::reactToDateChange);
    connect(notescombobox, QOverload<int>::of(&QComboBox::currentIndexChanged), this, &DDreamForm::reactToNoteChange);
    connect(addnotesbutton, &QPushButton::clicked, this, &DDreamForm::addNote);

    connect(editnight_button, &QPushButton::clicked, this, &DDreamForm::edit_night);
    /* When the user finishes editing the last tag edit, he may want to enter further
     * tags, so the addTagEntry slot is called
     * */
    connect(tagentries.last()->getTagEntry(), &QLineEdit::editingFinished,
                     this, &DDreamForm::addTagEntry);
    /* The close button shall toggle the closesignal
     * */
    connect(closebutton, &QPushButton::clicked,
                     this, &DDreamForm::closesignal);
    //The changebutton shall change the mode
    connect(changebutton, &QPushButton::clicked, this, &DDreamForm::swapMode);
    /* The visibility status of the lucidity sliders shall always be adapted to the
     * content of the kidncombobox
     * */
    connect(kindcombobox, &QComboBox::currentTextChanged, this, &DDreamForm::setLucidStatus);
    //The okbutton shall save the dream and close the input dialog
    connect(okbutton, &QPushButton::clicked, this, &DDreamForm::interpretandclose);
    //The savebutton shall only save the dream
    connect(savebutton, &QPushButton::clicked,
                     this, &DDreamForm::interpret);
    //The cancelbutton shall ask if the user wants to close the dialog
    connect(cancelbutton, &QPushButton::clicked, this, &DDreamForm::cancelweak);


    setLayout(mainbox);

    setMode(OpenType::READ);
}

DDreamForm::~DDreamForm()
{
    for (int i = 0; i < tagentries.size(); ++i)
        delete tagentries[i];
}


///MEMBER FUNCTIONS
bool DDreamForm::islucid(const QString &string)
{
    return string == QStringLiteral("Klartraum")
            || string == AGlobalSettings::preluziddreamstring
            || string == QStringLiteral("RC-Traum");
}

void DDreamForm::setupTagEntries()
{
    /* We have to ensure that again the first tagentries are filled
     * with the AGlobalSettings::standardTagCategories() and then a single
     * custom tag edit follows.
     * */
    int i = 0;
    ///Currently not perfect, not able to handle changes of the standard tag categories
    /* At first we reset the standard tag edits: We run through all standatd tag categories
     * and set the category of each tag entry to the corresponding tag edit and clear its entry.
     * But if a tag entry for a given standard tag category does not exist, it is created.
     * */
    for (i = 0; i < master->settings_qt()->nrStandardTagCategories(); ++i)
    {
        /* If there is a standard tag category, for which yet no tag entry exists
         * a new one is created.
         * */
        if (i >= tagentries.size())
        {
            tagentries.append(new DStaticTagEdit(master, tagtree_master->category(i), this));
            tagentries.last()->addToGrid(tagbox, i, 0);
        }
        else
        {
            tagentries[i]->setCategory(tagtree_master->category(i));
            tagentries[i]->getTagEntry()->clear();
        }
    }
    if (i >= tagentries.size())
    {
        tagentries.append(new DTagEdit(master, this));
        tagentries.last()->addToGrid(tagbox, i, 0);
    }
    /* We first remove all further tag edits
     * */
    while (tagentries.size() > master->settings_qt()->nrStandardTagCategories()+1)
    {
        tagentries.last()->removeFromGrid(tagbox);
        delete tagentries.last();
        tagentries.removeLast();
    }
    tagentries.last()->getTagEntry()->clear();
    tagentries.last()->setCategory(QString());
    disconnect(tagentries.last()->getTagEntry(), &QLineEdit::editingFinished,
               this, &DDreamForm::addTagEntry);
    connect(tagentries.last()->getTagEntry(), &QLineEdit::editingFinished,
               this, &DDreamForm::addTagEntry);
}

void DDreamForm::setLucidSliders(bool lucidity)
{
    if (lucidity)
    {
        if (techniquelabel == nullptr)
        {
            techniquelabel = new QLabel("Technik:", this);
            entrybox->addWidget(techniquelabel, 5, 0);
        }
        if (techniqueentry == nullptr)
        {
            techniqueentry = new QComboBox(this);
            techniqueentry->setEditable(true);
            techniqueentry->setValidator(
                        new QRegExpValidator(QRegExp(QStringLiteral("^[^\\\\,;:\"\'{}\\$%]*$")),
                                             techniqueentry));
            techniqueentry->lineEdit()->setPlaceholderText(QString("Benutzerdefiniert"));
            connect(techniqueentry->lineEdit(), &QLineEdit::editingFinished,
                    techniqueentry->lineEdit(), &QLineEdit::returnPressed);
            AE::connect_combobox_with_model(*techniqueentry, *master->entry_master()->technique_system());
            techniqueentry->setCurrentText(itechnique);
            entrybox->addWidget(techniqueentry, 5, 1, 1, 3);
        }
        {
            auto label = lucidsliderlabels.begin();
            auto entrymaster = master->entry_master();
            int nr = 0;
            for (auto slidername = lucidslidernames.begin(); slidername != lucidslidernames.end(); ++slidername, ++label, ++nr)
            {
                if (*label == nullptr && lucidsliders.value(*slidername) == nullptr)
                {
                    *label = new QLabel(*slidername);
                    lucidsliders[*slidername] = new MSpinBoxSlider(
                        entrymaster->rangeUStart(*slidername),
                        entrymaster->rangeUEnd(*slidername),
                        Qt::Horizontal
                    );
                    addSliderLabelToBox(*label, nr, SliderType::Lucid);
                    addSliderToBox(lucidsliders.value(*slidername), nr, SliderType::Lucid);
                    lucidsliders[*slidername]->setValue(model->entryUint(*slidername));
                }
            }
        }


        switch(type)
        {
        case OpenType::NEW:
        case OpenType::WRITE:
            break;
        case OpenType::READ:
            techniqueentry->setEnabled(false);
            for (auto slider = lucidsliders.begin(); slider != lucidsliders.end(); ++slider)
            {
                (*slider)->setEnabled(false);
            }
        }
    }
    else
    {
        if (techniquelabel != nullptr)
        {
            entrybox->removeWidget(techniquelabel);
            delete techniquelabel;
            techniquelabel = nullptr;
        }
        if (techniqueentry != nullptr)
        {
            itechnique = techniqueentry->currentText();
            entrybox->removeWidget(techniqueentry);
            delete techniqueentry;
            techniqueentry = nullptr;
        }
        auto label = lucidsliderlabels.begin();
        for (auto slider = lucidsliders.begin(); slider != lucidsliders.end(); ++slider, ++label)
        {
            if (*slider != nullptr)
            {
                entrybox->removeWidget(*slider);
                delete *slider;
                *slider = nullptr;
                entrybox->removeWidget(*label);
                delete *label;
                *label = nullptr;
            }
        }
    }
}

void DDreamForm::addSliderLabelToBox(QLabel *label, int nr, DDreamForm::SliderType ty)
{
    switch (ty)
    {
    case SliderType::Normal:
        entrybox->addWidget(label, 6 + nr % 3, (nr/3)*2);
        break;
    case SliderType::Lucid:
        entrybox->addWidget(label, 9, nr*2);
        break;
    }
}

void DDreamForm::addSliderToBox(MSpinBoxSlider *slider, int nr, DDreamForm::SliderType ty)
{
    switch (ty)
    {
    case SliderType::Normal:
        entrybox->addWidget(slider, 6 + nr % 3, 1+(nr/3)*2);
        break;
    case SliderType::Lucid:
        entrybox->addWidget(slider, 9, 1+nr*2);
        break;
    }
}

DDreamForm::OpenType DDreamForm::getMode() const
{
    return type;
}

void DDreamForm::setupForNewDream()
{
    type = OpenType::NEW;
    model->clear();
    saveorchangebutton->setCurrentIndex(1);
    okorclosebutton->setCurrentIndex(1);
    dateedit->setSelectedSection(QDateTimeEdit::DaySection);
    dateedit->setTime(QTime::currentTime());
    nameentry->clear();
    kindcombobox->setCurrentText(AGlobalSettings::darkdreamstring);
    auto entrymaster = master->entry_master();
    for (auto slider = sliders.begin(); slider != sliders.end(); ++slider)
    {
        (*slider)->setValue(entrymaster->rangeUStart(slider.key()));
        (*slider)->setEnabled(true);
    }
    itechnique = QString();
    istability = 0;
    ilucidity = 0;
    contentwidget->setCurrentIndex(0);
    for (auto cb = checkboxes.begin(); cb != checkboxes.end(); ++cb)
    {
        (*cb)->setChecked(false);
        (*cb)->setEnabled(true);
    }

    authored_date_edit->setDate(QDate::currentDate());
    authored_time_edit->setTime(QTime::currentTime());
    authored_place_edit->setText(master->settings_qt()->placeLastUsed());

    setupTagEntries();
    generaltagentry->clear();

    //Clear the large text edits
    textentry->clear();
    notesentry->clear();
    notescombobox->clear();
    populateNoteCombobox();
    notesdateedit->setDate(QDate::currentDate());
    notestimeedit->setTime(QTime::currentTime());

    //Enable all entries for editing
    dateedit->setEnabled(true);
    nameentry->setEnabled(true);
    kindcombobox->setEnabled(true);
    nightsection_combobox->setEnabled(true);
    editnight_button->setEnabled(true);
    for (int i = 0; i < tagentries.size(); ++i)
        tagentries[i]->setReadOnly(false);
    generaltagentry->setReadOnly(false);
    textentry->setReadOnly(false);
    notesdateedit->setEnabled(true);
    notestimeedit->setEnabled(true);
    notesplaceedit->setEnabled(true);
    addnotesbutton->setEnabled(true);
    notesentry->setReadOnly(false);
    authored_date_edit->setReadOnly(false);
    authored_time_edit->setReadOnly(false);
    authored_place_edit->setReadOnly(false);

    dateedit->setFocus();
}

void DDreamForm::passDream()
{
    model->setDate(dateedit->date().toString(Qt::ISODate));
    model->setName(nameentry->text());
    model->setKind(kindcombobox->currentText());

    model->setText(textentry->toPlainText());
    {
        QDate current_note_date = QDate::fromString(notescombobox->currentText(), Qt::ISODate);
        if (current_note_date.isValid())
        {
            model->setNotes(
                        notesentry->toPlainText(),
                        notesplaceedit->text(),
                        notestimeedit->time().hour(), notestimeedit->time().minute(), notestimeedit->time().second(),
                        current_note_date.year(), current_note_date.month(), current_note_date.day()
                        );
        }
    }
    model->setEntryUint(QStringLiteral("Schlafphase"), nightsection_combobox->currentIndex());
    for (auto slider = sliders.begin(); slider != sliders.end(); ++slider)
    {
        model->setEntryUint(slider.key(), (*slider)->getValue());
    }
    auto cbname = checkboxnames.begin();
    for (auto cb = checkboxes.begin(); cb != checkboxes.end(); ++cb, ++cbname)
    {
        model->setEntryBool(*cbname, (*cb)->isChecked());
    }

    if (!authored_place_edit->text().isEmpty())
    {
        model->setEntryTime(QStringLiteral("Verfasst um"), authored_time_edit->time().hour(),
                            authored_time_edit->time().minute(),
                            authored_time_edit->time().second());
        model->setEntryDate(QStringLiteral("Verfasst am"), authored_date_edit->date().year(),
                            authored_date_edit->date().month(),
                            authored_date_edit->date().day());
        model->setEntryText(QStringLiteral("Verfasst in"), authored_place_edit->text());
    }

    if (techniqueentry != nullptr)
        model->setEntryText("Technik", techniqueentry->currentText());
    for (auto slider = lucidsliders.begin(); slider != lucidsliders.end(); ++slider)
    {
        if (*slider != nullptr)
            model->setEntryUint(slider.key(), (*slider)->getValue());
    }

    TagList *tag_model = model->tag();
    for (auto i = 0; i < tagentries.size(); ++i)
    {
        tag_model->setCategory(i, tagentries.at(i)->getCategory());
        tag_model->setValue(i, tagentries.at(i)->getTagEntry()->text());
    }
    model->setTagLine(generaltagentry->text());
}

void DDreamForm::presentDream()
{
    //setupForNewDream();
    //Set the text of the entry fields
    dateedit->setDate(QDate::fromString(model->date(), Qt::ISODate));
    nameentry->setText(model->name());
    kindcombobox->setCurrentText(model->entryText(QStringLiteral("Art")));
    react_to_date_change();
    nightsection_combobox->setCurrentIndex(model->entryUint(QStringLiteral("Schlafphase")));
    for (auto slider = sliders.begin(); slider != sliders.end(); ++slider)
    {
        (*slider)->setValue(model->entryUint(slider.key()));
    }
    auto cbname = checkboxnames.begin();
    for (auto cb = checkboxes.begin(); cb != checkboxes.end(); ++cb, ++cbname)
    {
        (*cb)->setChecked(model->entryBool(*cbname));
    }
    setLucidSliders(islucid(model->entryText("Art")));
    if (islucid(model->entryText("Art")))
    {
        techniqueentry->clear();
        AE::connect_combobox_with_model(*techniqueentry, *master->entry_master()->technique_system());
        techniqueentry->setCurrentText(model->entryText("Technik"));
        for (auto slider = lucidsliders.begin(); slider != lucidsliders.end(); ++slider)
        {
            if (*slider != nullptr)
                (*slider)->setValue(model->entryUint(slider.key()));
        }
    }

    if (!model->entryDate("Verfasst am").isEmpty())
        authored_date_edit->setDate(QDate::fromString(model->entryDate("Verfasst am"), Qt::ISODate));
    else
        authored_date_edit->setDate(QDate::fromString(model->date(), Qt::ISODate));
    if (!model->entryTime("Verfasst um").isEmpty())
        authored_time_edit->setTime(QTime::fromString(model->entryTime("Verfasst um"), Qt::ISODate));
    else
        authored_time_edit->setTime(QTime(0, 0));

    authored_place_edit->setText(model->entryText("Verfasst in"));

    generaltagentry->clear();

    /* Now we insert the tags of newdream into the corresponding tag entry
     * At first we scan the entries for the standard tag categories and set their text
     * to the corresponding tags.
     *
     * Then we scan the further "NonStandard" tags of the dream and insert their
     * tags into the custom tag entries.
     * */
    SettingsQt *settings = master->settings_qt();
    TagList *tag_model = model->tag();
    unsigned int i = 0;
    for (; i < settings->nrStandardTagCategories(); ++i)
    {
        // Only the tags need to be inserted. The category already has been
        // set to the correct value in `setupForNewDream`.
        tagentries[i]->getTagEntry()->setText(tag_model->value(i));
    }
    for (; i < tag_model->rowCount(); ++i)
    {
        //We insert the tags into the last custom tag entry
        tagentries.last()->setCategory(tag_model->category(i));
        tagentries.last()->getTagEntry()->setText(tag_model->value(i));
        /* And insert a new tag entry, so that always one empty custom tag edit is left
         * */
        disconnect(tagentries.last()->getTagEntry(), &QLineEdit::editingFinished,
                   this, &DDreamForm::addTagEntry);
        tagentries.append(new DTagEdit(master, this));
        tagentries.last()->addToGrid(tagbox, tagentries.size()-1, 0);
        connect(tagentries.last()->getTagEntry(), &QLineEdit::editingFinished,
                   this, &DDreamForm::addTagEntry);
    }

    textentry->setPlainText(model->text());
    notesentry->clear();
    notescombobox->clear();
    populateNoteCombobox();
}


///SLOTS
void DDreamForm::swapMode()
{
    switch (type)
    {
    case OpenType::NEW:
    case OpenType::WRITE:
        setMode(OpenType::READ);
        break;
    case OpenType::READ:
        setMode(OpenType::WRITE);
        break;
    }
}

void  DDreamForm::setMode(OpenType newtype)
{
    switch (newtype)
    {
    case OpenType::READ:
        for (int i = 0; i < tagentries.size(); ++i)
            tagentries[i]->setReadOnly(true);
        switch(type)
        {
        case OpenType::NEW:
        case OpenType::WRITE:
            dateedit->setEnabled(false);
            dateedit->setSelectedSection(QDateTimeEdit::NoSection);
            saveorchangebutton->setCurrentIndex(0);
            okorclosebutton->setCurrentIndex(0);
            nameentry->setEnabled(false);
            kindcombobox->setEnabled(false);
            nightsection_combobox->setEnabled(false);
            editnight_button->setEnabled(false);
            if (techniqueentry != nullptr)
                techniqueentry->setEnabled(false);
            for (auto slider = sliders.begin(); slider != sliders.end(); ++slider)
            {
                (*slider)->setEnabled(false);
            }
            for (auto slider = lucidsliders.begin(); slider != lucidsliders.end(); ++slider)
            {
                if (*slider != nullptr)
                    (*slider)->setEnabled(false);
            }
            for (auto cb = checkboxes.begin(); cb != checkboxes.end(); ++cb)
            {
                (*cb)->setEnabled(false);
            }
            generaltagentry->setReadOnly(true);
            textentry->setReadOnly(true);
            notesdateedit->setEnabled(false);
            notestimeedit->setEnabled(false);
            notesplaceedit->setEnabled(false);
            addnotesbutton->setEnabled(false);
            notesentry->setReadOnly(true);
            authored_date_edit->setReadOnly(true);
            authored_time_edit->setReadOnly(true);
            authored_place_edit->setReadOnly(true);
        case OpenType::READ:;
        }
        break;
    case OpenType::WRITE:
    case OpenType::NEW:
        for (int i = 0; i < tagentries.size(); ++i)
            tagentries[i]->setReadOnly(false);
        switch (type)
        {
        case OpenType::READ:
            dateedit->setEnabled(true);
            dateedit->setSelectedSection(QDateTimeEdit::NoSection);
            saveorchangebutton->setCurrentIndex(1);
            okorclosebutton->setCurrentIndex(1);
            nameentry->setEnabled(true);
            kindcombobox->setEnabled(true);
            nightsection_combobox->setEnabled(true);
            editnight_button->setEnabled(true);
            if (techniqueentry != nullptr)
                techniqueentry->setEnabled(true);
            for (auto slider = sliders.begin(); slider != sliders.end(); ++slider)
            {
                (*slider)->setEnabled(true);
            }
            for (auto slider = lucidsliders.begin(); slider != lucidsliders.end(); ++slider)
            {
                if (*slider != nullptr)
                    (*slider)->setEnabled(true);
            }
            for (auto cb = checkboxes.begin(); cb != checkboxes.end(); ++cb)
            {
                (*cb)->setEnabled(true);
            }
            generaltagentry->setReadOnly(false);
            textentry->setReadOnly(false);
            notesdateedit->setEnabled(true);
            notestimeedit->setEnabled(true);
            notesplaceedit->setEnabled(true);
            addnotesbutton->setEnabled(true);
            notesentry->setReadOnly(false);
            authored_date_edit->setReadOnly(false);
            authored_time_edit->setReadOnly(false);
            authored_place_edit->setReadOnly(false);
            break;
        case OpenType::NEW:;
        case OpenType::WRITE:;
        }
        dateedit->setFocus();
        break;
    }
    type = newtype;
}

bool DDreamForm::interpret()
{
    if (type == OpenType::READ)
        return false;

    passDream();
    if (!createunknownTags())
        return false;

    model->save_dream();

    return true;
}

bool DDreamForm::interpretandclose()
{
    if (interpret())
    {
        emit closesignal();
        setMode(OpenType::READ);
        return true;
    }
    else
        return false;
}

bool DDreamForm::createunknownTags()
{
    model->interpret_dream();

    TagList *unknown_tag_model = model->unknown_tag_model();
    if (unknown_tag_model->rowCount() > 0)
    {
        DDialogs::DUnknownTagsDialog tagenterdialog(master, unknown_tag_model);

        if (tagenterdialog.exec())
        {
            AE::QStringMulHash specified_tags = tagenterdialog.getNewTags();
            for(auto it = specified_tags.keyBegin(); it != specified_tags.keyEnd(); ++it)
            {
                auto tags = specified_tags.values(*it);
                for (auto jt = tags.cbegin(); jt != tags.cend(); ++jt)
                    model->add_tag(*it, *jt);
            }
            return true;
        }
        else
            return false;
    }
    else
        return true;
}

void DDreamForm::openDream(QDate date, quint32 idx, OpenType mode)
{
    setupTagEntries();
    if (model->open_dream(date.year(), date.month(), date.day(), idx))
    {
        presentDream();
        setMode(mode);
    }
}

void DDreamForm::close()
{
    model->close();
    setMode(OpenType::READ);
}

void DDreamForm::cancelweak()
{
    if (type == OpenType::NEW || type == OpenType::WRITE)
    {
        QMessageBox::StandardButton button =
                QMessageBox::question(this, "Abbrechen",
                                      QStringLiteral("Wollen Sie wirklich das Bearbeiten des Traumes abbrechen"
                                                     " und m\xF6" "glicherweise ungespeicherte Daten verwerfen?"),
                                      QMessageBox::Yes | QMessageBox::No, QMessageBox::No);

        if (button == QMessageBox::Yes)
        {
            emit closesignal();
        }
    }
    else
        emit closesignal();
}

void DDreamForm::addTagEntry()
{
    /* If there are no tagentries, something is wrong and we should do nothing
     * */
    if (tagentries.size() == 0 || type == OpenType::READ)
        return;

    /* Check, wether the last two tagentries are empty.
     * If they are, the last tagentry is removed.
     *
     * One Custom tag edit should always stay, so we do not only have to check before,
     * whether there are at least two tagentries, but also, whether
     * there are currently more than two custom tagentries (these are tagentries with index
     * higher or equal than sumberofstatictagentries).
     * */
    if (tagentries.size() > std::max(1u, master->settings_qt()->nrStandardTagCategories()+1))
    {
        if (tagentries.last()->getTagEntry()->text().isEmpty()
                && tagentries.at(tagentries.size()-2)->getTagEntry()->text().isEmpty())
        {
            //Remove and delete the tagentry
            model->tag()->popRow();
            tagentries.last()->removeFromGrid(tagbox);
            tagentries.last()->deleteLater();
            tagentries.removeLast();
            /* Connect the new last ones editingFinished signal, so that it can toggle
             * the insertion of a new tagEntry.
             * */
            connect(tagentries.last()->getTagEntry(), &QLineEdit::editingFinished, this,  &DDreamForm::addTagEntry);
            return;
        }
    }
    if (!tagentries.last()->getTagEntry()->text().isEmpty())
    {
        /* Append the new tagentry and
         * connect the new last ones editingFinished signal, so that it can toggle
         * the insertion of a new tagEntry.
         * */
        disconnect(tagentries.last()->getTagEntry(), &QLineEdit::editingFinished, this, &DDreamForm::addTagEntry);
        model->tag()->pushRow();
        tagentries.append(new DTagEdit(master, this));
        tagentries.last()->addToGrid(tagbox, tagentries.size(), 0);
        tagentries.last()->getTagEntry()->setFocus();
        connect(tagentries.last()->getTagEntry(), &QLineEdit::editingFinished, this,  &DDreamForm::addTagEntry);
    }
}

void DDreamForm::react_to_date_change()
{
    model->setDate(dateedit->date().toString(Qt::ISODate));

    NightQt *night_qt = model->night_qt();
    auto oldindex = nightsection_combobox->currentIndex();
    nightsection_combobox->clear();
    if (night_qt->nrSections() > 0)
    {
        for (quint32 i = 0; i < night_qt->nrSections(); ++i)
        {
            nightsection_combobox->addItem(night_qt->displayText(i));
        }
        nightsection_combobox->setCurrentIndex(std::min(oldindex, nightsection_combobox->count()-1));
    }
    else
    {
        nightsection_combobox->addItem(QStringLiteral("unbekannt"));
        nightsection_combobox->setCurrentIndex(0);
    }
}

void DDreamForm::edit_night()
{
    DNightDialog dialog(*model->night_qt(), master->settings_qt(), master->entry_master(), this);
    dialog.exec();
    react_to_date_change();
}

void DDreamForm::setLucidStatus()
{
    setLucidSliders(islucid(kindcombobox->currentText()));
}

void DDreamForm::setNewSliderRanges()
{
    auto entrymaster = master->entry_master();
    for (auto slider = sliders.begin(); slider != sliders.end(); ++slider)
    {
        (*slider)->setMinimum(entrymaster->rangeUStart(slider.key()));
        (*slider)->setMaximum(entrymaster->rangeUEnd(slider.key()));
    }
    for (auto slider = lucidsliders.begin(); slider != lucidsliders.end(); ++slider)
    {
        if (*slider != nullptr)
        {
            (*slider)->setMinimum(entrymaster->rangeUStart(slider.key()));
            (*slider)->setMaximum(entrymaster->rangeUEnd(slider.key()));
        }
    }
}

void DDreamForm::displayNotesOfCurrentDate()
{
    QDate new_date = QDate::fromString(notescombobox->currentText(), Qt::ISODate);
    if (new_date.isValid())
    {
        disconnect(notesdateedit, nullptr, this, nullptr);

        notesdateedit->setDate(new_date);
        notesentry->setPlainText(model->notesNote(new_date.year(), new_date.month(), new_date.day()));
        QTime notestime = QTime::fromString(model->notesTime(new_date.year(), new_date.month(), new_date.day()), Qt::ISODate);
        if (notestime.isValid())
            notestimeedit->setTime(notestime);
        else
            notestimeedit->setTime(QTime::currentTime());
        notesplaceedit->setText(model->notesPlace(new_date.year(), new_date.month(), new_date.day()));

        connect(notesdateedit, &QDateEdit::dateChanged, this, &DDreamForm::reactToDateChange);
    }
}

void DDreamForm::reactToNoteChange()
{
    if (type != OpenType::READ)
    {
        QDate old_date = notesdateedit->date();
        model->setNotes(
                    notesentry->toPlainText(),
                    notesplaceedit->text(),
                    notestimeedit->time().hour(), notestimeedit->time().minute(), notestimeedit->time().second(),
                    old_date.year(), old_date.month(), old_date.day()
                    );
    }

    displayNotesOfCurrentDate();
}

void DDreamForm::reactToDateChange(QDate date){
    QDate old_date = QDate::fromString(notescombobox->currentText(), Qt::ISODate);
    QString old_notes = model->notesNote(old_date.year(), old_date.month(), old_date.day());
    QTime old_time = QTime::fromString(model->notesTime(old_date.year(), old_date.month(), old_date.day()), Qt::ISODate);
    QString old_place = model->notesPlace(old_date.year(), old_date.month(), old_date.day());
    if (!old_time.isValid())
        old_time = QTime(0, 0, 0);

    if (type != OpenType::READ)
    {
        model->setNotes(
                    QString(), QString(), 0, 0, 0,
                    old_date.year(), old_date.month(), old_date.day()
                    );
        model->setNotes(
                    old_notes, old_place, old_time.hour(), old_time.minute(), old_time.second(),
                    date.year(), date.month(), date.day()
                    );
    }
    notescombobox->setItemText(notescombobox->currentIndex(), date.toString(Qt::ISODate));
}

void DDreamForm::populateNoteCombobox()
{
    disconnect(notescombobox, nullptr, this, nullptr);
    QString currentDate = notescombobox->currentText();
    notescombobox->clear();
    for (int i = 0; i < model->nrNotes(); ++i)
    {
        notescombobox->addItem(model->noteDate(i));
    }

    if (notescombobox->findText(currentDate) != -1)
    {
        notescombobox->setCurrentText(currentDate);
    }

    if (notescombobox->count() == 0)
        notescombobox->addItem(QDate::currentDate().toString(Qt::ISODate));

    displayNotesOfCurrentDate();

    connect(notescombobox, QOverload<int>::of(&QComboBox::currentIndexChanged), this, &DDreamForm::reactToNoteChange);
}

void DDreamForm::addNote()
{
    QDate date = QDate::currentDate();
    while (notescombobox->findText(date.toString(Qt::ISODate)) != -1)
        date = date.addDays(1);
    QString date_as_text = date.toString(Qt::ISODate);
    if (date.isValid() && notescombobox->findText(date_as_text) == -1)
    {
        notescombobox->addItem(date_as_text);
        notescombobox->setCurrentText(date_as_text);
    }
}
