#ifndef SCOPEDTHREAD_HPP
#define SCOPEDTHREAD_HPP


#include <QThread>


class ScopedThread: public QThread
{
    Q_OBJECT

public:
    explicit ScopedThread(QObject *parent = nullptr);
    virtual ~ScopedThread();
};

#endif // SCOPEDTHREAD_HPP
