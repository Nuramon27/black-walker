#include "dabstractsearchform.hpp"


#include <QSignalMapper>

#include <QPushButton>
#include <QComboBox>

#include <QHBoxLayout>
#include <QVBoxLayout>

#include "aglobalsettings.hpp"

QStringList const conditionaloperatornames = QStringList({"==", "&", "|", "XOR"});

DAbstractSearchForm::DAbstractSearchForm(QWidget *parent, bool first, bool withplus) :
    QWidget(parent)
{
    if (!first)
    {
        compositioncombobox = new QComboBox(this);
        compositioncombobox->addItems(conditionaloperatornames);
        compositioncombobox->setCurrentText("&");
    }
    else
        compositioncombobox = nullptr;

    if (withplus)
    {
        newsearchformbutton = new QPushButton(QString('+'), this);
        /*openbracketbutton = new QPushButton(QString('('), this);
        closebracketbutton = new QPushButton(QString(')'), this);*/
    }
    else
    {
        newsearchformbutton = nullptr;
        /*openbracketbutton = nullptr;
        closebracketbutton = nullptr;*/
    }

    childrenbox = new QVBoxLayout();

    if (withplus)
    {
        buttonbox = new QHBoxLayout();
        buttonbox->addWidget(newsearchformbutton);
        /*buttonbox->addWidget(openbracketbutton);
        buttonbox->addWidget(closebracketbutton);*/
    }
    else
        buttonbox = nullptr;

    mainbox = new QHBoxLayout();
    if (!first)
        mainbox->addWidget(compositioncombobox, 0x30);
    else
        mainbox->addStretch(0x30);

    mainbox->addLayout(childrenbox, 0xC0);
    if (withplus)
        mainbox->addLayout(buttonbox, 0x10);
    else
        mainbox->addStretch(0x10);

    setLayout(mainbox);

    if (withplus)
    {
        QObject::connect(newsearchformbutton, SIGNAL(clicked()), this, SIGNAL(newleaf()));
        /*QObject::connect(openbracketbutton, SIGNAL(clicked()), this, SIGNAL(openbracket()));
        QObject::connect(closebracketbutton, SIGNAL(clicked()), this, SIGNAL(closebracket()));*/
    }
}

DAbstractSearchForm::~DAbstractSearchForm()
{
}



///MEMBER FUNCTIONS
/*MBasicInstance::MCondition::Comp DAbstractSearchForm::getComposition() const
{
    if (compositioncombobox == nullptr)
        return MBasicInstance::DDreamCondition::NONE;
    else
        return MBasicInstance::DDreamCondition::allcompositions.value(compositioncombobox->currentText());
}*/

QString DAbstractSearchForm::getCompositionString() const
{
    if (compositioncombobox == nullptr)
        return QString();
    else
         return compositioncombobox->currentText();
}
