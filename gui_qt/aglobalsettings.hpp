#ifndef GSOBALSETTINGS_HPP
#define GSOBALSETTINGS_HPP


/**
 * @file
 *
 * @brief The aglobalsettings.h file contains the definition of the AGlobalSettings class as well as the declaration
 * of one global instance ::GlobalSettings of this class.
 * */


#include <QRegExp>
#include <QDir>
#include <QString>
#include <QStringBuilder>


#include "zdef.hpp"

/**
 * @page SpecGlobalSettings The GlobalSettings.txt file
 *
 * @section GSGeneral General structure
 *
 * The settings of the Black Walker application are stored in a file called GlobalSettings.txt
 * that lies in the same directory as the BlackWalker executable. This file must be
 * utf-8 encoded! If it is not, becaviour is undefined. The file consists of specifications
 * of settings in the form
 *
 * [settingname] = [value]
 *
 * separated by semicola,
 * where settingname is the name of the setting under which it is stored in the AGlobalSettings class,
 * and value is the value  of the setting in a format specifyed below in @ref GSFormat.
 * The number of spaces or "\n" characters around the equals
 * sign and the semicolon is arbitrary.
 *
 * Each AGlobalSettings file must specify the version of the Black Walker application that created it
 * (or at least whith which it is compatible, e.g. if it was created by human) in the same format,
 * where settingname is "version" and value the versionstring. If that version specification
 * misses, version 1.0 is assumed.
 *
 * @section GSFormat How to format the setting values
 *
 * If the setting corresponding to settingname has one of the types
 * bool, QChar, QString, QDate, QTime, QDateTime
 * or an arithmetic type, value must be given as the output of the QVariant::toString() function.
 *
 * If the setting has type QStringList, the value must be given as a comma separated list
 * of the contained QStrings in the correct order. These strings must not contain commata
 * themselves!
 * */


/**
 * @brief The AGlobalSettings class stores all settings for the application, like the path to the dream
 * diary that is currently opened or the #standardTagCategories.
 *
 * These settings are stored in #setting as an AE::QVarHash that maps a QString (the name of the setting) to
 * a QVariant. A setting of a given name can be retrieved with get(QString const &) and set
 * by set(QString const &, QVariant const &).
 *
 * In order to provide name checks, there is the enumeration AGlobalSettings::SetName, that provides one enum value
 * for every setting, and the QHash #setnames that maps these enum values to the corresponding
 * names of the settings as QStrings. Bot for conveniently accessing the settings,
 * there are also overloads of the @link get(SetName) const get()@endlink
 * and @link set(SetName, QVariant const &)
 * set()@endlink
 * functions that expect the setting name as a SetName.
 *
 * But there is no reason to not set own settings not listed in AGlobalSettings::SetName
 * and always access them by their string.
 */
class AGlobalSettings
{
public:
    /**
     * @brief A QRegExp that matches one or more ocurrences of
     * a comma and an arbitrary number of preceding or following spaces or newline characters.
     */
    static QRegExp const commaregexp;
    /**
     * @brief A QRegExp that matches one or more ocurrences of
     * a semicolon and an arbitrary number of preceding or following spaces or newline characters.
     */
    static QRegExp const semicolonregexp;
    /**
     * @brief A QRegExp that matches one or more ocurrences of
     * a doubledot and an arbitrary number of preceding or following spaces.
     */
    static QRegExp const ddotregexp;
    /**
     * @brief A QRegExp that matches one or more ocurrences of
     * an '&' character and an arbitrary number of preceding or following spaces.
     */
    static QRegExp const andregexp;
    /**
     * @brief A QRegExp that matches one or more ocurrences of
     * an '=' character and an arbitrary number of preceding or following spaces or newline characters.
     */
    static QRegExp const equalregexp;
    static QString const darkdreamstring;
    static QString const preluziddreamstring;
    static QString const backstring;

    static QRegExp const forbiddencharsentrycategory;
    static QRegExp const forbiddencharsentry;
    static QRegExp const forbiddencharstagcategory;
    static QRegExp const forbiddencharstag;
};

#endif // GSOBALSETTINGS_HPP
