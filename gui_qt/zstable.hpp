#ifndef ZSTABLE_HPP
#define ZSTABLE_HPP


/**
 * @file
 *
 * @brief The zstable.h header file contains all headers that shall be precompiled
 * */

#include <QtGlobal>

#include <QDate>
#include <QTime>
#include <QDateTime>

#include <QPair>
#include <QList>
#include <QLinkedList>
#include <QHash>
#include <QMultiHash>
#include <QMap>
#include <QMultiMap>
#include <QSet>
#include <QString>
#include <QStringList>

#include <QString>
#include <QStringList>
#include <QStringBuilder>

#include <QDir>
#include <QFile>
#include <QFileInfo>
#include <QTextStream>

#include <QObject>

#include <QWidget>

#include <QSignalMapper>

#include <QLabel>
#include <QPushButton>
#include <QLineEdit>
#include <QTextEdit>
#include <QSpinBox>
#include <QDateEdit>
#include <QCheckBox>
#include <QComboBox>
#include <QRadioButton>

#include <QTreeView>
#include <QHeaderView>

#include <QMessageBox>
#include <QDialog>
#include <QFileDialog>

#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QGridLayout>
#include <QStackedWidget>
#include <QTabWidget>


#endif // ZSTABLE_HPP
