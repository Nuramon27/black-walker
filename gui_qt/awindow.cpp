#include "awindow.hpp"


#include <QDir>
#include <QFileInfo>
#include <QStringBuilder>
#include <QTimer>

#include <QCloseEvent>

#include <QPushButton>

#include <QMenu>
#include <QMenuBar>
#include <QAction>

#include <QTreeView>
#include <QHeaderView>

#include <QStackedWidget>
#include <QHBoxLayout>
#include <QVBoxLayout>

#include <QFileDialog>
#include <QMessageBox>

#include <QThread>


#include "Enum/dallowed.hpp"
#include "Enum/savestate.hpp"
#include "aglobalsettings.hpp"

#include <DDialogs/dtagadministration.hpp>
#include <DDialogs/daboutdialog.hpp>
#include <DDialogs/dsearchdialog.hpp>

#include <DModel/ddreamlistview.hpp>
#include <DModel/dnightlistview.hpp>
#include "ddreamform.hpp"

#include <bdg/Bindings.h>



//Temporary workaround
//#if defined (Q_OS_LINUX)
//#    include <QEventLoop>
//#endif //Q_OS_LINUX


AWindow::AWindow(Master *new_master, QWidget *parent)
    : QMainWindow(parent),
      selectedNight(),
      master(new_master)
{
    settings = master->settings_qt();
    setWindowTitle(QStringLiteral("Black Walker"));
    /* Make several types known to the Qt-Meta-Object-System
     * */
    qRegisterMetaType<AE::DAllowedFlags>("AE::DAllowedFlags");


    filemenu = menuBar()->addMenu("Datei");
    extrasmenu = menuBar()->addMenu("Extras");

    saveaction = filemenu->addAction(QStringLiteral("Sichern"),
                                     this,
                                     &AWindow::save);
    savetonewplaceaction = filemenu->addAction(QStringLiteral("An anderem Ort sichern"),
                                               this,
                                               &AWindow::saveToNewPlace);
    savecopytonewplaceaction = filemenu->addAction(QStringLiteral("Kopie sichern"),
                                                   this,
                                                   SLOT(saveCopyToNewPlace()));
    openBackupaction = filemenu->addAction(QStringLiteral("Backup laden"),
                                           this,
                                           SLOT(loadBackup()));
    createnewdiaryaction = filemenu->addAction("Neues Traumtagebuch erstellen",
                                               this,
                                               &AWindow::createNewDiary);
    openNewDiaryaction = filemenu->addAction(QString::fromUcs4(U"Traumtagebuch \xF6" "ffnen"),
                                             this,
                                             &AWindow::selectDiaryPath);

    searchaction = extrasmenu->addAction("Suche", this, &AWindow::search);
    pretty_print_action = extrasmenu->addAction("Tagebuch drucken",
                          this,
                            &AWindow::prettyPrint);
    exportjsonaction = extrasmenu->addAction("Als JSON exportieren",
                                             this,
                                             &AWindow::exportJson);
    //settingsaction = extrasmenu->addAction("Einstellungen");
    tagadminaction = extrasmenu->addAction("Tagverwaltung");
    aboutaction = extrasmenu->addAction(QString::fromUcs4(U"\x00DC" "ber"),
                                        this,
                                        SLOT(showAbout()));

    nightlistview = new DNightListView(master->dream_list_master(), this);
    nightlistheader = new QHeaderView(Qt::Horizontal, this);
    nightlistview->setHeader(nightlistheader);
    nightlistview->setSizeAdjustPolicy(QAbstractScrollArea::AdjustToContents);
    nightlistview->setSelectionMode(QAbstractItemView::SingleSelection);
    nightlistview->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

    dreamlistview = new DDreamListView(master->dream_list_master());
    dreamlistview->setDragEnabled(true);
    dreamlistview->setAcceptDrops(true);
    dreamlistheader = new QHeaderView(Qt::Horizontal, this);
    dreamlistview->setHeader(dreamlistheader);
    dreamlistview->setSizeAdjustPolicy(QAbstractScrollArea::AdjustToContents);
    dreamlistview->setSelectionMode(QAbstractItemView::SingleSelection);
    dreamlistview->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

    newdreambutton = new QPushButton("Neuer Traum", this);
    newdreambutton->setMinimumHeight(0x30);
    dreaminput = new DDreamForm(master, this);
    dreaminputstackwidget = new QStackedWidget(this);
    dreaminputstackwidget->setMinimumHeight(0x30);
    dreaminputstackwidget->setMaximumHeight(0x30);
    dreaminputstackwidget->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Ignored);
    dreaminputstackwidget->addWidget(newdreambutton);
    dreaminputstackwidget->addWidget(dreaminput);

    nightoverviewbox = new QHBoxLayout();
    nightoverviewbox->addWidget(nightlistview, 0x40);
    nightoverviewbox->addWidget(dreamlistview, 0xC0);


    AWidget = new QWidget(this);

    mainbox = new QVBoxLayout();
    mainbox->addLayout(nightoverviewbox, 0x50);
    mainbox->addWidget(dreaminputstackwidget, 0xB0);

    AWidget->setLayout(mainbox);

    setCentralWidget(AWidget);
    AE::setSizeWidget(settings, this, QStringLiteral("AWindow"));
    AE::setColumnSizes(settings, dreamlistview, QStringLiteral("DDreamList"), 4 + settings->nrStandardTagCategories());
    AE::setColumnSizes(settings, nightlistview, QStringLiteral("DNightList"), 4);


    connect(nightlistview->selectionModel(), &QItemSelectionModel::currentRowChanged,
                     this, &AWindow::changeSelectedNight);
    connect(dreamlistview, &DDreamListView::activated,
                     this, &AWindow::selectDreamToRead);
    connect(newdreambutton, &QPushButton::clicked,
                     this, &AWindow::newdream);
    connect(dreaminput, &DDreamForm::closesignal,
                     this, &AWindow::closeDreamInput);
    connect(tagadminaction, &QAction::triggered,
                     this, &AWindow::administrateTags);
    connect(settings, &SettingsQt::sizes_notifyChanged, this, &AWindow::setSizesToSettings);


}

AWindow::~AWindow()
{
    delete dreamlistview;
    delete nightlistview;
    //delete nightlist;
    delete dreaminput;
}

void AWindow::resizeEvent(QResizeEvent *event)
{
    Q_UNUSED(event)
    storeSizesToSettings();
}

void AWindow::closeEvent(QCloseEvent *event)
{
    /* There are two reasons why we might not close the window immediately,
     * but first ask the user whether he wants to save changes:
     * There are changes on the dream diary or there is a dream currently opened for editing
     * (which may, but must not contain unsaved changes)
     * */
    if (static_cast<AE::SaveState>(master->modified()) == AE::SaveState::Modified
            || (dreaminputstackwidget->currentIndex() == 1
                && (dreaminput->getMode() == DDreamForm::OpenType::WRITE
                    || dreaminput->getMode() == DDreamForm::OpenType::NEW)))
    {
        QMessageBox::StandardButton button(
                    QMessageBox::question(this, QString::fromUcs4(U"Ungespeicherte \xC4"
                                                                  "nderungen"),
                                          QStringLiteral("Sichern?"),
                                          QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel,
                                          QMessageBox::Save));
        if (button == QMessageBox::Save)
        {
            if (save())
                event->accept();
            else
                event->ignore();
        }
        else if (button == QMessageBox::Discard)
            event->accept();
        else if (button == QMessageBox::Cancel)
            event->ignore();
        else
            event->ignore();
    }
    else
        event->accept();
}

QSize AWindow::sizeHint() const
{
    return QSize(1024, 640);
}


///SLOTS
void AWindow::search()
{
    DDialogs::DSearchDialog searchDialog(master, this);

    searchDialog.exec();
}

void AWindow::newdream()
{
    dreaminputstackwidget->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Minimum);
    dreaminput->setupForNewDream();
    dreaminputstackwidget->setMinimumHeight(dreaminputstackwidget->minimumSizeHint().height());
    dreaminputstackwidget->setMaximumHeight(0x500);
    dreaminputstackwidget->setCurrentIndex(1);
}

void AWindow::changeSelectedNight(const QModelIndex &newnight)
{
    NightListQt *night_list_qt = master->dream_list_master()->night_list_qt();
    if (newnight.row() >= night_list_qt->rowCount() || newnight.row() < 0)
        return;

    /* The night that has been clicked is the night at newnight.row().
     * */
    selectedNight = QDate::fromString(night_list_qt->dateFormal(newnight.row()), Qt::ISODate);
    /*if (!(GlobalSettings.get(AGlobalSettings::DREAMSTOSHOWBEFORE).toUInt() == 0xFFFFU
            || GlobalSettings.get(AGlobalSettings::DREAMSTOSHOWAFTER).toUInt() == 0xFFFFU))
    {
        if (newnight.row() < nightlist->getNight().size())
            dreamlist->setDream(worker.dreamsAround(selectedNight,
                                                    GlobalSettings.get(AGlobalSettings::DREAMSTOSHOWBEFORE).toUInt(),
                                                    GlobalSettings.get(AGlobalSettings::DREAMSTOSHOWAFTER).toUInt()));
    }*/
    if (selectedNight.isValid())
    {
        QModelIndex index = master->dream_list_master()->dream_list_qt()->computeModelIndexOfNight(selectedNight);
        scrolldreamlistTo(index);
        dreamlistview->setCurrentIndex(index);
    }
}

void AWindow::scrolldreamlistTo(const QModelIndex &newnight)
{
    dreamlistview->scrollTo(newnight, QAbstractItemView::PositionAtCenter);
}

void AWindow::selectDreamToRead(QModelIndex const &index)
{
    auto dreamlist = dreamlistview->dreamListModel();
    QDate date = QDate::fromString(dreamlist->date_formal(index.row()), Qt::ISODate);
    quint32 idx = dreamlist->idx(index.row());
    if (dreaminput->getMode() != DDreamForm::OpenType::READ)
        return;

    dreaminputstackwidget->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Minimum);
    dreaminput->openDream(date, idx, DDreamForm::OpenType::READ);
    dreaminputstackwidget->setMinimumHeight(dreaminputstackwidget->minimumSizeHint().height());
    dreaminputstackwidget->setMaximumHeight(0x500);
    dreaminputstackwidget->setCurrentIndex(1);
}

void AWindow::closeDreamInput()
{
    dreaminputstackwidget->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Ignored);
    dreaminputstackwidget->setCurrentIndex(0);
    dreaminputstackwidget->setMinimumHeight(0x30);
    dreaminputstackwidget->setMaximumHeight(0x30);
    dreaminput->close();
}

void AWindow::save_failure_dialog()
{
    QMessageBox::critical(this, QStringLiteral("Fehler beim Speichern"),
                          QStringLiteral("Das Traumtagebuch konnte nicht gespeichert werden"));
}

void AWindow::load_failure_dialog()
{
    QMessageBox::critical(this, QStringLiteral("Fehler beim Laden"),
                          QStringLiteral("Das Traumtagebuch konnte nicht geladen werden"));
}

bool AWindow::save()
{
    storeSizesToSettings();
    /* If there is a dream currently opened for editing, we store this in the dreamlist,
     * so that it is not lost.
     * */
    if (dreaminputstackwidget->currentIndex() == 1
            && (dreaminput->getMode() == DDreamForm::OpenType::WRITE
                || dreaminput->getMode() == DDreamForm::OpenType::NEW))
        dreaminput->interpret();

    /* If there is a valid stardard diary location, we save the diary to this location.
     * */
    if (settings->location().isEmpty())
    {
        return saveToNewPlace();
    }
    else
    {
        if (master->saveToStd())
        {
            return true;
        }
        else
        {
            save_failure_dialog();
            return false;
        }
    }
}

void AWindow::selectDiaryPath()
{
    if (static_cast<AE::SaveState>(master->modified()) == AE::SaveState::Modified && !(settings->location().isEmpty()))
    {
        QMessageBox::StandardButton button =
                QMessageBox::question(this, QStringLiteral("Aktuelles Traumtagebuch sichern"),
                                      QStringLiteral("Soll das aktuelle Traumtagebuch in den Pfad ")
                                      % settings->location()
                                      % QStringLiteral(" gesichert werden?")
                                      % QString::fromUcs4(U" Andernfalls gehen die letzten \xC4" "nderungen M\xF6"
                                                       "glicherweise verloren."),
                                      QMessageBox::Yes | QMessageBox::No | QMessageBox::Cancel,
                                      QMessageBox::Yes);
        if (button == QMessageBox::Yes)
            save();
        if (button == QMessageBox::Cancel)
            return;
    }

    QString newpath =
            QFileDialog::getOpenFileName(this, QStringLiteral("Traumtagebuch laden"),
                                     #ifdef BLACKWALKER_DEBUG
                                         QStringLiteral("/home/Manuel/tmp/black_walker_play"),
                                     #else
                                         QDir::homePath(),
                                     #endif // BLACKWALKER_DEBUG
                                         QStringLiteral("Alle Dateien (*);;Toml Dateien (*.toml)"));

    if (!newpath.isEmpty())
    {
        settings->setLocation(newpath);
        if (!master->loadFrom(newpath))
        {
            load_failure_dialog();
        }
    }
    dreaminput->setNewSliderRanges();
    setSizesToSettings();
}

void AWindow::createNewDiary()
{
    if (static_cast<AE::SaveState>(master->modified()) == AE::SaveState::Modified)
    {
        QMessageBox::StandardButton button =
                QMessageBox::question(this, QStringLiteral("Aktuelles Traumtagebuch sichern"),
                                      QStringLiteral("Soll das aktuelle Traumtagebuch in den Pfad ")
                                      % settings->location()
                                      % QStringLiteral(" gesichert werden?")
                                      % QString::fromUcs4(U"Andernfalls gehen die letzten \xC4" "nderungen M\xF6"
                                                          "glicherweise verloren."),
                                      QMessageBox::Yes | QMessageBox::No,
                                      QMessageBox::Yes);
        if (button == QMessageBox::Yes)
            save();
    }

    QString newpath =
            QFileDialog::getSaveFileName(this, QStringLiteral("Wo Soll Ihr Traumtagebuch gespeichert werden?"),
                                         QDir::homePath(),
                                         QStringLiteral("Toml-Dateien (*.toml);;Alle Dateien (*)"));

    if (!newpath.isEmpty())
    {
        master->clear();
        settings->setLocation(newpath);
        if (!settings->location().isEmpty())
            save();
    }
    dreaminput->setNewSliderRanges();
}

bool AWindow::saveToNewPlace()
{
    QString newpath =
            QFileDialog::getSaveFileName(this, QStringLiteral("Wo Soll Ihr Traumtagebuch gespeichert werden?"),
                                         QDir::homePath(),
                                         QStringLiteral("Toml-Dateien (*.toml);;Alle Dateien (*)"));

    if (!newpath.isEmpty())
    {
        settings->setLocation(newpath);
        if (!settings->location().isEmpty())
            return save();
    }
    return false;
}

bool AWindow::saveCopyToNewPlace()
{
    QString newpath =
            QFileDialog::getSaveFileName(this, QStringLiteral("Wo Soll Ihr Traumtagebuch gespeichert werden?"),
                                         QDir::homePath(),
                                         QStringLiteral("Toml-Dateien (*.toml);;Alle Dateien (*)"));

    if (!newpath.isEmpty())
    {
        if (master->save(newpath))
        {
            return true;
        }
        else
        {
            save_failure_dialog();
            return false;
        }
    }
    return false;
}

bool AWindow::prettyPrint()
{
    QString newpath =
            QFileDialog::getSaveFileName(this, QStringLiteral("Wo Soll Ihr Traumtagebuch gespeichert werden?"),
                                         QDir::homePath(),
                                         QStringLiteral("Tex-Dateien (*.tex);;Alle Dateien (*)"));

    if (newpath.isEmpty()) {
        return false;
    }

    if (master->prettyPrintDiary(newpath))
    {
        return true;
    }
    else
    {
        save_failure_dialog();
        return false;
    }
}

bool AWindow::exportJson()
{
    QString newpath =
            QFileDialog::getSaveFileName(this, QStringLiteral("Wohin soll Ihr Traumtagebuch exportiert werden?"),
                                         QDir::homePath(),
                                         QStringLiteral("Json-Dateien (*.json);;Alle Dateien (*)"));

    if (newpath.isEmpty()) {
        return false;
    }

    master->exportDiaryJson(newpath);
    return true;
}

void AWindow::loadBackup()
{
    if (static_cast<AE::SaveState>(master->modified()) == AE::SaveState::Modified && !(settings->location().isEmpty()))
    {
        QMessageBox::StandardButton button =
                QMessageBox::question(this, QStringLiteral("Aktuelles Traumtagebuch sichern"),
                                      QStringLiteral("Soll das aktuelle Traumtagebuch in den Pfad ")
                                      % settings->location()
                                      % QStringLiteral(" gesichert werden?")
                                      % QString::fromUcs4(U" Andernfalls gehen die letzten \xC4" "nderungen M\xF6"
                                                       "glicherweise verloren."),
                                      QMessageBox::Yes | QMessageBox::No | QMessageBox::Cancel,
                                      QMessageBox::Yes);
        if (button == QMessageBox::Yes)
            save();
        if (button == QMessageBox::Cancel)
            return;
    }

    QString newpath =
            QFileDialog::getOpenFileName(this, QStringLiteral("Traumtagebuch laden"),
                                         QDir::homePath(),
                                         QStringLiteral("Alle Dateien (*);;Toml-Dateien (*.toml)"));

    if (!newpath.isEmpty())
    {
        if (!master->loadBackup(newpath))
        {
            load_failure_dialog();

        }
    }
    dreaminput->setNewSliderRanges();
    setSizesToSettings();
}

void AWindow::administrateTags()
{
    DDialogs::DTagAdministration admin(master, QString("Personen"), this);
    admin.exec();
}

void AWindow::showAbout()
{
    DDialogs::DAboutDialog diag(this);

    diag.exec();
}

void AWindow::updateDreamList()
{
#if(0)
    if (!nightlistview->selectionModel()->selectedIndexes().isEmpty())
    {
        /* At first we have to convert the selected Indices in the nightlistview
         * to a night.
         * This night lies at the position selectedIndexes().first().row in
         * the list of nights stored in nightlist (if there are more nights selected,
         * we only respect the first one).
         * */
        QList<MRevDate> nights(nightlist->getNight());
        int row = nightlistview->selectionModel()->selectedIndexes().first().row();
        if (row >= 0 && row < nights.size())
            selectedNight = nights.at(row);
    }

    /* The found night is given to dreamlist
     * */
    /*dreamlist->recieveBeginResetDreamList();
    dreamlist->recieveEndResetDreamList(
               worker.dreamsAround(selectedNight,
                                    GlobalSettings.get(AGlobalSettings::DREAMSTOSHOWBEFORE).toUInt(),
                                    GlobalSettings.get(AGlobalSettings::DREAMSTOSHOWAFTER).toUInt()));*/
    dreamlist->selectNight(selectedNight);
#endif//0
}

void AWindow::setSizesToSettings()
{
    AE::setSizeWidget(settings, this, QStringLiteral("AWindow"));
    AE::setColumnSizes(settings, dreamlistview, QStringLiteral("DDreamList"), 4 + settings->nrStandardTagCategories());
    AE::setColumnSizes(settings, nightlistview, QStringLiteral("DNightList"), 4);
}

void AWindow::storeSizesToSettings()
{
    AE::storeSizeWidget(settings, this, QStringLiteral("AWindow"));
    AE::storeColumnSizes(settings, dreamlistview, QStringLiteral("DDreamList"), 4 + settings->nrStandardTagCategories());
    AE::storeColumnSizes(settings, nightlistview, QStringLiteral("DNightList"), 4);
}
