#ifndef MDATEEDITOPT_HPP
#define MDATEEDITOPT_HPP


#include <QDateEdit>
#include <QLineEdit>


class QDate;

class QFocusEvent;
class QMouseEvent;


/**
 * @brief The MDateEditOpt class is nothing else than a QDateEdit with one tiny difference:
 * Every time the user clicks on the Widget or gives focus to it, the day section is selected
 * so that the user can immediealtely start entering his date.
 */
class MDateEditOpt : public QDateEdit
{
    Q_OBJECT
public:
    MDateEditOpt(QDate const &date, QWidget *parent = nullptr);
    ~MDateEditOpt() {}

protected:
    virtual void focusInEvent(QFocusEvent *event);
    //virtual void mousePressEvent(QMouseEvent *event);

protected slots:
    void selectDaySection();
};


/**
 * @brief The MLineEditOpt class is a QLineEdit with an additional clicked() signal.
 */
class MLineEditOpt : public QLineEdit
{
    Q_OBJECT
public:
    explicit MLineEditOpt(QWidget *parent = nullptr);

signals:
    void clicked();

protected:
    virtual void mousePressEvent(QMouseEvent *event);
};

#endif // MDATEEDITOPT_HPP
