#include "mdateeditopt.hpp"


#include <QDate>

#include <QDateTimeEdit>

#include <QFocusEvent>
#include <QMouseEvent>


MDateEditOpt::MDateEditOpt(QDate const &date, QWidget *parent) :
    QDateEdit(date, parent)
{
    MLineEditOpt *thislineEdit = new MLineEditOpt();
    setLineEdit(thislineEdit);
    connect(thislineEdit, SIGNAL(clicked()), this, SLOT(selectDaySection()));
}

void MDateEditOpt::focusInEvent(QFocusEvent *event)
{
    QDateEdit::focusInEvent(event);
    setSelectedSection(QDateTimeEdit::DaySection);
}

/*void MDateEditOpt::mousePressEvent(QMouseEvent *event)
{
    QDateEdit::mousePressEvent(event);
    setSelectedSection(QDateTimeEdit::DaySection);
}*/

void MDateEditOpt::selectDaySection()
{
    setSelectedSection(QDateTimeEdit::DaySection);
}


MLineEditOpt::MLineEditOpt(QWidget *parent) :
    QLineEdit(parent)
{
}

void MLineEditOpt::mousePressEvent(QMouseEvent *event)
{
    QLineEdit::mousePressEvent(event);
    emit clicked();
}
