#include "dsearchform.hpp"


#include <QSignalMapper>

#include <QVBoxLayout>


#include "aglobalsettings.hpp"

#include "bdg/Bindings.h"


#include "dsearchformleaf.hpp"


DSearchForm::DSearchForm(Master *new_master, QWidget *parent, bool first, bool withplus) :
    DAbstractSearchForm(parent, first, withplus),
    master(new_master)
{
    children += new DSearchFormLeaf(master, this, true);
    childrenmapper = new QSignalMapper(this);
    childrenmapper->setMapping(children.last(), children.last());
    childrenbox->addWidget(children.last());

    connect(children.last(), SIGNAL(newleaf()), childrenmapper, SLOT(map()));
    connect(childrenmapper, SIGNAL(mapped(QWidget*)), this, SLOT(addchild(QWidget*)));
}

DSearchForm::DSearchForm(Master *new_master, QStringList selectedEntities,
                         QWidget *parent, bool first, bool withplus) :
    DAbstractSearchForm(parent, first, withplus),
    master(new_master)
{
    childrenmapper = new QSignalMapper(this);
    for (int i = 0; i < selectedEntities.size(); ++i)
    {
        DSearchFormLeaf *newchild(new DSearchFormLeaf(master, this, (i == 0)));
        newchild->selectEntity(selectedEntities.at(i));
        childrenmapper->setMapping(newchild, newchild);
        childrenbox->addWidget(newchild);
        connect(newchild, SIGNAL(newleaf()), childrenmapper, SLOT(map()));
        children += newchild;
    }

    connect(childrenmapper, SIGNAL(mapped(QWidget*)), this, SLOT(addchild(QWidget*)));
}

DSearchForm::~DSearchForm()
{
}


///MEMBER FUNCTIONS
bool DSearchForm::isEmpty() const
{
    bool res = true;

    //The DSearchForm is empty if all children are empty
    for (DSearchFormListcit it = children.cbegin(); it != children.cend(); ++it)
        if (!(*it)->isEmpty())
            res = false;

    return res;
}

QString DSearchForm::getCondition(SearchSettings settings) const
{
    QString res;
    /* For every child, we recieve its condition and embrace it in brackets.
     *
     * If a child is not the first child, this bracket is preceded by its Composition,
     * */
    for (DSearchFormListcit it = children.constBegin(); it != children.constEnd(); ++it)
    {
        if (!(*it)->isEmpty())
        {
            /* If res is still empty, this is the first nonempty child.
             *
             * Otherwise there is something the new condition has to be composed with,
             * so we hale to add a Composition
             * */
            if (res.isEmpty())
            {
                res += QString(" (")
                        += (*it)->getCondition(settings)
                        += QString(") ");
            }
            else
            {
                res += (*it)->getCompositionString()
                        += QString(" (")
                        += (*it)->getCondition(settings)
                        += QString(") ");
            }

        }
    }

    return res;
}


///SLOTS
void DSearchForm::addchild(QWidget *after)
{
    DAbstractSearchForm *newchild = new DSearchFormLeaf(master, this);
    DSearchFormListit it;
    for (it = children.begin(); it != children.end() && *it != after; ++it);
    ++it;
    children.insert(it, newchild);
    childrenmapper->setMapping(newchild, newchild);
    childrenbox->insertWidget(childrenbox->indexOf(after) + 1, newchild);

    QObject::connect(newchild, SIGNAL(newleaf()), childrenmapper, SLOT(map()));
}

void DSearchForm::removechild(QWidget *toremove)
{
    childrenbox->removeWidget(toremove);
    children.removeAll(reinterpret_cast<DAbstractSearchForm*>(toremove));
}
