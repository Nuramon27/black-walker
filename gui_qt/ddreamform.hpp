#ifndef DDREAMFORM_HPP
#define DDREAMFORM_HPP


#include <QWidget>


#include <QList>
#include <QString>

class QPushButton;
class QCheckBox;
class QLineEdit;
class QTextEdit;
class QTimeEdit;
class QComboBox;
class QLabel;
class QGroupBox;

class QHBoxLayout;
class QVBoxLayout;
class QGridLayout;
class QStackedWidget;
class QTabWidget;


#include "zdef.hpp"

class MDateEditOpt;
class MSpinBoxSlider;

class DAbstractTagEdit;
class DSuggestionEdit;

class Master;
class DreamFormQt;
class TagTreeMaster;


/**
 * @brief With the DDreamForm the user can enter a dream.
 *
 * It contains several entries where information like date, title and tags of the dream can be entered,
 * as well as a #textentry, where the actual content is entered and a #notesentry,
 * where the user can place annotations on the dream.
 *
 * The entered dream can be obtained by calling getDream().
 */
class DDreamForm : public QWidget
{
    Q_OBJECT

    enum class SliderType
    {
        Normal,
        Lucid
    };

    QStringList checkboxnames;
    QStringList slidernames;
    QStringList lucidslidernames;

public:
    /**
     * @brief The OpenType enum provides names for the several ways for opening
     * a DDreamForm.
     *
     * One can open a dream input dialog for inserting a new dream, for editing an existing dream
     * or for only reading from a dream without editting being allowed.
     */
    enum class OpenType: quint8
    {
        NEW   = 0x0,///< Insert a new dream. When setting this mode, all entries are cleared
        READ  = 0x1,///< Only read a dream. When setting this mode, it can only be read, but no editting is possible
        WRITE = 0x2///< Edit an existing dream
    };

private:
    /**
     * @brief The OpenType in which the Dream Input Dialog is opened.
     */
    OpenType type;
    /**
     * @brief Saves the dream and closes the input dialog
     */
    QPushButton *okbutton;
    /**
     * @brief Saves the dream, but leaves the input dialog open
     */
    QPushButton *savebutton;
    /**
     * @brief Closes the input dialog quietly when being in read only mode.
     */
    QPushButton *closebutton;
    /**
     * @brief By clicking this button, the user can change into editing mode
     */
    QPushButton *changebutton;
    QPushButton *cancelbutton;
    QStackedWidget *okorclosebutton;
    QStackedWidget *saveorchangebutton;

    QLabel *datelabel, *namelabel, *kindlabel, *techniquelabel, *nightsectionlabel, *fallasleeplabel, *wakeuplabel, *generaltaglabel,
    *textlabel, *noteslabel;
    QList<QCheckBox*> checkboxes;
    QList<QLabel*> sliderlabels;
    QList<QLabel*> lucidsliderlabels;
    QHash<QString, MSpinBoxSlider*> sliders;
    QHash<QString, MSpinBoxSlider*> lucidsliders;
    MDateEditOpt *dateedit;
    QLineEdit *nameentry;
    QComboBox *techniqueentry;
    QComboBox *kindcombobox;
    QTimeEdit *fallasleepentry, *wakeupentry;
    QLabel *authored_date_label, *authored_time_label, *authored_place_label;
    MDateEditOpt *authored_date_edit;
    QTimeEdit *authored_time_edit;
    QLineEdit *authored_place_edit;
    QComboBox *nightsection_combobox;
    QPushButton *editnight_button;
    int istability, ilucidity;
    QString itechnique;
    QComboBox *notescombobox;
    MDateEditOpt *notesdateedit;
    QTimeEdit *notestimeedit;
    QLineEdit *notesplaceedit;
    QPushButton *addnotesbutton;
    QTextEdit *textentry, *notesentry;
    /**
     * @brief A list of instances of DAbstractTagEdit where tags can be entered.
     *
     * It starts with some @link DStaticTagEdit static tag edits @endlink where tags of the most frequently
     * used tag categories may be entered. Then it contains one or more @link DTagEdit DTagEdits @endlink
     * where tags of arbatrary categories may be entered
     */
    QList<DAbstractTagEdit *> tagentries;
    /**
     * @brief A simle QLineEdit in which the user may enter a semicolon separated list of tag units.
     *
     * Each tag unit begins with a category name and optionally one or more group names, each followed by a double dot.
     * Then in the tag unit one or more tags are listed and separated by commata.
     */
    DSuggestionEdit *generaltagentry;
    /**
     * @brief Contains all #tagentries
     */
    QGroupBox *entrygroup, *taggroup;

    QWidget *entitywidget, *textwidget, *noteswidget;
    QTabWidget *contentwidget;

    QGridLayout *authored_box;
    QHBoxLayout *entitybox;
    QGridLayout *entrybox, *tagbox;
    QVBoxLayout *leftbox, *rightbox;
    QHBoxLayout *checkboxbox;
    QVBoxLayout *textbox;
    QHBoxLayout *notescontrolbox;
    QVBoxLayout *notesbox;
    QHBoxLayout *generaltagbox;
    QHBoxLayout *buttonbox;
    QVBoxLayout *mainbox;

    AE::QStringMulHash readytags;

    Master *master;
    DreamFormQt *model;
    TagTreeMaster *tagtree_master;

public:
    /**
     * @brief Constructs the DDreamForm with #worker object newworker and parent parent.
     */
    explicit DDreamForm(Master *newmaster, QWidget *parent = nullptr);
    /**
     * @brief Destructs the DDreamForm
     * */
    ~DDreamForm();


private:
    /**
     * @brief Returns whether string is the name of a lucid dream kind (like Klartraum)
     */
    static bool islucid(QString const &string);
    /**
     * @brief Sets the categories of the tag entries to the corresponding
     * standatd tag categories and leaves only one custom tag edit.
     */
    void setupTagEntries();
    /**
     * @brief Adapts the visibility status of #techniqueentry, #stabilityslider, #lucidityslider
     * to lucidity.
     */
    void setLucidSliders(bool lucidity);

    /**
     * @brief Add label to the #entrybox as the description of the nr-th slider.
     */
    void addSliderLabelToBox(QLabel *label, int nr, SliderType ty);
    /**
     * @brief Add slider to the #entrybox as the nr-th slider.
     */
    void addSliderToBox(MSpinBoxSlider *slider, int nr, SliderType ty);
    void displayNotesOfCurrentDate();

public:
    OpenType getMode() const;
    /**
     * @brief Clears all entries and sets #type to OpenType::NEW, so that a new dream can be entered
     */
    void setupForNewDream();

signals:
    /**
     * @brief Signals that the user has entered and specfiyed a new tag, so that it shall be created
     *
     * Gives the category, name and group of the tag.
     *
     * Connected to the AWorker::createTag() slot of #worker.
     */
    void createNewKnownTagSignal(QString category, QString tag, QString group);
    /**
     * @brief Signals that the DDreamForm should be closed
     */
    void closesignal();

private slots:
    /**
     * @brief If the current Mode is OpenType::WRITE or OpenType::NEW, change the mode to OpenType::READ,
     * if the current mode is OpenType::READ, change it to OpenType::WRITE
     */
    void swapMode();
    /**
     * @brief Checks, whether the last two #tagentries are empty. If they are, the last
     * tagentry is deleted and removed. If the last tagentry is not empty,
     * a new tagentry is appended.
     */
    void addTagEntry();
    /**
     * @brief Sets the visibility status of the stability- and luciditysliders
     * in dependenc of the kind that is currently selected in the #kindcombobox
     */
    void setLucidStatus();

public slots:
    /**
     * @brief Set the open mode to newtype.
     *
     * By this function, the mode can only be set to READ, or WRITE,
     * but not to new, as this would not make sense, because if there is already a dream, the mode of displaying
     * this dream cannot just be changed to NEW. Use setupForNewDream() for this.
     */
    void setMode(OpenType newtype);
    /**
     * @brief Interprets the contents of the input dialog, creates a DDream with this content
     * and emits the newDreamCreated or dreamChanged signal, depending on the mode.
     *
     * If there are unspecifyed tags in the input dialog, the DUnknownTagsDialog is opened.
     */
    bool interpret();
    /**
     * @brief Calls interpret() and emits the closesignal() if the dream has successfully been saved
     */
    bool interpretandclose();
    /**
     * @brief Interprets the content of #generaltagentry and obtains the tags that are entered in it.
     * Afterwards displays a dialog in which the user can specify the tags and tag groups
     * that remain "unknown", this means that he has not entered the group or the category of tha tag.
     *
     * The user can enter a semicolon separated of tag groups as specified in #generaltagentry.
     * If he only enters tags in a tag unit, all of these tags that do not already exist in any category are unknown,
     * as their category is not specifyed.
     * If he also enters groups, the first group in the row is unknown if it does not already exist
     * (i.e. the group after the first
     * double dot), as its group is not specified. All further tags and tag groups are known,
     * as their groups are specifyed. So either they already exist, then we only have to store them in #readytags,
     * in order to later be able to add them to the entered dream (we do not want to search again for
     * their category). Or they do not exist, then they have to be created; but as their group is known,
     * they also can be fully created, which is done by sending createNewKnownTagSignal(), which is
     * connected to the AWorker::createTag() slot of #worker.
     */
    void passDream();
    void presentDream();
    bool createunknownTags();
    void openDream(QDate date, quint32 idx, OpenType mode);
    void close();
    /**
     * @brief Asks the user if he really wants to close the input dialog, if he does, emits the closesignal.
     */
    void cancelweak();
    void react_to_date_change();
    void edit_night();
    void setNewSliderRanges();
    void reactToNoteChange();
    void reactToDateChange(QDate date);
    void populateNoteCombobox();
    void addNote();
};

#endif // DDREAMFORM_HPP
