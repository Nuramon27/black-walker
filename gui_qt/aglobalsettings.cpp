#include "aglobalsettings.hpp"


#include <utility>


#include <QApplication>
#include <QFile>
#include <QTextStream>



QRegExp const AGlobalSettings::commaregexp("([\\s\n]*,[\\s\n]*)+");
QRegExp const AGlobalSettings::semicolonregexp("([\\s\n]*;[\\s\n]*)+");
QRegExp const AGlobalSettings::ddotregexp("(\\s*:\\s*)+");
QRegExp const AGlobalSettings::andregexp("(\\s*&\\s*)+");
QRegExp const AGlobalSettings::equalregexp("([\\s\n]*=[\\s\n]*)+");

QString const AGlobalSettings::darkdreamstring(QString::fromUcs4(U"Tr\xFC" "btraum"));
QString const AGlobalSettings::preluziddreamstring(QString::fromUcs4(U"Pr\xE4" "luzider Traum"));
QString const AGlobalSettings::backstring(QString::fromUcs4(U"Zur\xFC" "ck"));

QRegExp const AGlobalSettings::forbiddencharsentrycategory("\\\\,;:<>\"\'{}\\$%" "&|\\^(==)!\\(\\)\\[\\]");
QRegExp const AGlobalSettings::forbiddencharsentry("\\\\,;:<>\"\'{}\\$%");
QRegExp const AGlobalSettings::forbiddencharstagcategory("\\\\,;:<>\"\'{}\\$%" "&|\\^(==)!\\(\\)\\[\\]");
QRegExp const AGlobalSettings::forbiddencharstag("\\\\,;:<>\"\'{}\\$%");

