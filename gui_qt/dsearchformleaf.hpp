#ifndef DSEARCHFORMLEAF_HPP
#define DSEARCHFORMLEAF_HPP


#include <dabstractsearchform.hpp>


#include <QString>

class QLineEdit;
class QComboBox;

class QHBoxLayout;


#include "zdef.hpp"
class Master;

class DSuggestionEdit;


/**
 * @brief The DSearchFormLeaf class is an interface to ente a single condition.
 *
 * Such a condition consists of a condition entity, which may be a tag or a dream entry together with a comparison,
 * like contains or is equal to (represented by its name and an operator like == or contains), and one or more
 * values, separated by logical operators (representey by there value enclosed in quotation marks).
 * The resulting condition is then
 * the conditon entity compared with the values and connected by the corresponding logical operators.
 *
 * If e.g. the condition entity is "Places contain" and the values are "London, Munich & Berlin",
 * then the resulting condition is
 * Places contain "London" OR Places contain Munich AND Places contain Berlin
 */
class DSearchFormLeaf : public DAbstractSearchForm
{
    Q_OBJECT

    QComboBox *dreamentrycombobox;
    DSuggestionEdit *conditionentry;

    QHBoxLayout *entrybox;

    Master *master;

public:
    /**
     * @brief Constructs a new DSearchFormLeaf with worker object newworker.
     *
     * @sa DAbstractSearchForm::DAbstractSearchForm(QWidget, bool, bool)
     */
    DSearchFormLeaf(Master *new_master, QWidget *parent = nullptr, bool first = false);
    /**
     * @brief Destructs the DSearchFormLeaf
     */
    virtual ~DSearchFormLeaf();

    /**
     * @brief Returns whether the #conditionentry is empty.
     */
    virtual bool isEmpty() const;
    /**
     * @brief Interprets and returns the entered condition
     */
    virtual QString getCondition(SearchSettings settings) const;
    /**
     * @brief Sets entity as preselected category in the dreamentrycombobox
     */
    void selectEntity(QString const &entity);
};

#endif // DSEARCHFORMLEAF_HPP
