#include "mspinboxslider.hpp"


#include <QString>

#include <QSlider>
#include <QSpinBox>
#include <QBoxLayout>
#include <QHBoxLayout>
#include <QVBoxLayout>


MSpinBoxSlider::MSpinBoxSlider(QWidget *parent) :
    QWidget(parent)
{
    slider = new QSlider(Qt::Horizontal);
    spinbox = new QSpinBox();

    mainbox = new QHBoxLayout();

    mainbox->addWidget(slider);
    mainbox->addWidget(spinbox);

    setLayout(mainbox);

    QObject::connect(slider, SIGNAL(valueChanged(int)), spinbox, SLOT(setValue(int)));
    QObject::connect(slider, SIGNAL(valueChanged(int)), this, SIGNAL(valueChanged(int)));
    QObject::connect(spinbox, SIGNAL(valueChanged(int)), slider, SLOT(setValue(int)));
    QObject::connect(spinbox, SIGNAL(valueChanged(int)), this, SIGNAL(valueChanged(int)));
    QObject::connect(slider, SIGNAL(sliderReleased()), this, SLOT(emiteditingFinished()));
    QObject::connect(spinbox, SIGNAL(editingFinished()), this, SLOT(emiteditingFinished()));
}

MSpinBoxSlider::MSpinBoxSlider(int minimum, int maximum, Qt::Orientation orientation, QWidget *parent) :
    QWidget(parent)
{
    slider = new QSlider(orientation);
    slider->setMinimum(minimum);
    slider->setMaximum(maximum);
    spinbox = new QSpinBox();
    spinbox->setMinimum(minimum);
    spinbox->setMaximum(maximum);

    if(orientation == Qt::Horizontal)
        mainbox = new QHBoxLayout();
    else
        mainbox = new QVBoxLayout();

    mainbox->addWidget(slider);
    mainbox->addWidget(spinbox);

    setLayout(mainbox);

    QObject::connect(slider, SIGNAL(valueChanged(int)), spinbox, SLOT(setValue(int)));
    QObject::connect(slider, SIGNAL(valueChanged(int)), this, SIGNAL(valueChanged(int)));
    QObject::connect(spinbox, SIGNAL(valueChanged(int)), slider, SLOT(setValue(int)));
    QObject::connect(spinbox, SIGNAL(valueChanged(int)), this, SIGNAL(valueChanged(int)));
    QObject::connect(slider, SIGNAL(sliderReleased()), this, SLOT(emiteditingFinished()));
    QObject::connect(spinbox, SIGNAL(editingFinished()), this, SLOT(emiteditingFinished()));
}

MSpinBoxSlider::~MSpinBoxSlider()
{
    delete slider;
    delete spinbox;
    delete mainbox;
}

int MSpinBoxSlider::getValue() const
 {
    return spinbox->value();
 }

int MSpinBoxSlider::getMinimum() const
{
    return slider->minimum();
}

int MSpinBoxSlider::getMaximum() const
{
    return slider->maximum();
}

int MSpinBoxSlider::getSingleStep() const
{
    return slider->singleStep();
}

int MSpinBoxSlider::getPageStep() const
{
    return slider->pageStep();
}

int MSpinBoxSlider::getDisplayIntegerBase() const
{
    return spinbox->displayIntegerBase();
}

QString MSpinBoxSlider::getSuffix() const
{
    return spinbox->suffix();
}

Qt::Orientation MSpinBoxSlider::getOrientation() const
{
    return slider->orientation();
}

void MSpinBoxSlider::setMinimum(int minimum)
{
    slider->setMinimum(minimum);
    spinbox->setMinimum(minimum);
}

void MSpinBoxSlider::setMaximum(int maximum)
{
    slider->setMaximum(maximum);
    spinbox->setMaximum(maximum);
}

void MSpinBoxSlider::setRange(int minimum, int maximum)
{
    slider->setMinimum(minimum);
    spinbox->setMinimum(minimum);

    slider->setMaximum(maximum);
    spinbox->setMaximum(maximum);
}

void MSpinBoxSlider::setSingleStep(int step)
{
    slider->setSingleStep(step);
    spinbox->setSingleStep(step);
}

void MSpinBoxSlider::setPageStep(int step)
{
    slider->setPageStep(step);
}

void MSpinBoxSlider::setDisplayIntegerBase(int base)
{
    spinbox->setDisplayIntegerBase(base);
}

void MSpinBoxSlider::setSuffix(QString const &suffix)
{
    spinbox->setSuffix(suffix);
}

void MSpinBoxSlider::setOrientation(Qt::Orientation orientation)
{
    int minimum = slider->minimum();
    int maximum = slider->maximum();
    /* If the orientation shall change, the mainbox must be constructed anew
     * and that means we have to construct the whole widget anew
     * */
    if (slider->orientation() != orientation)
    {
        delete slider;
        delete spinbox;
        delete mainbox;
        slider = new QSlider(orientation);
        slider->setMinimum(minimum);
        slider->setMaximum(maximum);
        spinbox = new QSpinBox();
        spinbox->setMinimum(minimum);
        spinbox->setMaximum(maximum);

        if(orientation == Qt::Horizontal)
            mainbox = new QHBoxLayout();
        else
            mainbox = new QVBoxLayout();

        mainbox->addWidget(slider);
        mainbox->addWidget(spinbox);

        QObject::connect(slider, SIGNAL(valueChanged(int)), spinbox, SLOT(setValue(int)));
        QObject::connect(slider, SIGNAL(valueChanged(int)), this, SIGNAL(valueChanged(int)));
        QObject::connect(spinbox, SIGNAL(valueChanged(int)), slider, SLOT(setValue(int)));
        QObject::connect(spinbox, SIGNAL(valueChanged(int)), this, SIGNAL(valueChanged(int)));
        QObject::connect(slider, SIGNAL(sliderReleased()), this, SLOT(emiteditingFinished()));
        QObject::connect(spinbox, SIGNAL(editingFinished()), this, SLOT(emiteditingFinished()));
    }
}


///SLOTs
void MSpinBoxSlider::emiteditingFinished()
{
    emit editingFinished(spinbox->value());
}

void MSpinBoxSlider::setValue(int value)
{
    /* If the setValue-function of a spinbox is used, it will emit its value changed signal,
     * which would lead in this case to repaeted callings of this SLOT everytime a value is changed.
     * So we first have to block all Signals and then change values, then allow signals again
     * */
    this->blockSignals(true);
    slider->setValue(value);
    spinbox->setValue(value);
    this->blockSignals(false);
}
