#include "sight_dsuggestionedit.hpp"

#include <QCoreApplication>
#include <QApplication>

#include <bdg/Bindings.h>
#include <DDialogs/dtagadministration.hpp>

#include <scopedthread.hpp>
#include <generalemitter.hpp>
#include <tests/testutil.hpp>
#include <dsuggestionedit.hpp>

SightDSuggestionEdit::SightDSuggestionEdit(Master *master) :
master(master){

}
void SightDSuggestionEdit::do_test_sight_suggestionedit() {
    test_setup_standard_diary(master->m_d);
}

extern "C" qint32 test_sight_dsuggestionedit(int argc, char *argv[], void* err)
{
    QApplication a(argc, argv);
    QCoreApplication::setOrganizationName(QStringLiteral("Manuel Simon"));
    QCoreApplication::setApplicationName(QStringLiteral("Black Walker"));
    QCoreApplication::setApplicationVersion(QStringLiteral("1.1.21"));
    QFont font;
    QFont::insertSubstitutions(QStringLiteral("Noto Sans"),
                               QStringList({"Arial", "Helvetica", "DejaVu Sans", "FreeSans"}));
    font.setStyleHint(QFont::SansSerif, QFont::PreferOutline);
    font.setPointSize(10);
    font.setFamily(QStringLiteral("Noto Sans"));
    QGuiApplication::setFont(font);
    ScopedThread workerthread;
    int ret;
    {
        GeneralEmitter emitter(workerthread, a);


        SightDSuggestionEdit testemitter(&emitter.master);
        DSuggestionEdit w(
                    &emitter.master,
                    AE::DEntityType::TAG,
                    AE::DEnterType::TAGSWITHGROUP,
                    AE::DAllowed::ALL,
                    nullptr,
                    QStringLiteral("Personen")
                );
        //DDreamListView w(emitter.master.dream_list_master());
        QObject::connect(&testemitter, &SightDSuggestionEdit::emt, &testemitter,
                         &SightDSuggestionEdit::do_test_sight_suggestionedit, Qt::QueuedConnection);
        emit testemitter.emt();
        w.show();
        ret = a.exec();
        if (ret != 0) return ret;
    }

    return 0;
}
