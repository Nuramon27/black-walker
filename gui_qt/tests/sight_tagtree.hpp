#ifndef SIGHT_TAGTREE_HPP
#define SIGHT_TAGTREE_HPP

#include <QCoreApplication>
#include <QApplication>

#include <bdg/Bindings.h>
#include <DModel/ddreamlistview.hpp>

class Master;


class SightTagTree: public QObject {
    Q_OBJECT
    Master *master;
public:
    SightTagTree(Master *master);
public slots:
    void do_test_sight_tagtree();
signals:
    void emt();
};


#endif // SIGHT_TAGTREE_HPP
