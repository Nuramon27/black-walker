#include "dreamlist.hpp"

#include <QCoreApplication>
#include <QApplication>

#include <bdg/Bindings.h>
#include <DModel/ddreamlistview.hpp>

#include <scopedthread.hpp>
#include <generalemitter.hpp>
#include <tests/testutil.hpp>

DreamListTest::DreamListTest(Master *master) :
master(master){

}
void DreamListTest::do_test_standard_diary() {
    test_setup_standard_diary(master->m_d);
}

extern "C" qint32 test_standard_diary(int argc, char *argv[], void* err)
{
    QApplication a(argc, argv);
    QCoreApplication::setOrganizationName(QStringLiteral("Manuel Simon"));
    QCoreApplication::setApplicationName(QStringLiteral("Black Walker"));
    QCoreApplication::setApplicationVersion(QStringLiteral("1.1.21"));
    QFont font;
    QFont::insertSubstitutions(QStringLiteral("Noto Sans"),
                               QStringList({"Arial", "Helvetica", "DejaVu Sans", "FreeSans"}));
    font.setStyleHint(QFont::SansSerif, QFont::PreferOutline);
    font.setPointSize(10);
    font.setFamily(QStringLiteral("Noto Sans"));
    QGuiApplication::setFont(font);

    ScopedThread workerthread;
    int ret;
    {
        GeneralEmitter emitter(workerthread, a);



        DreamListTest testemitter(&emitter.master);
        DDreamListView w(emitter.master.dream_list_master());
        QObject::connect(&testemitter, &DreamListTest::emt, &testemitter,
                         &DreamListTest::do_test_standard_diary, Qt::QueuedConnection);
        emit testemitter.emt();
        emit emitter.finish();
        ret = a.exec();
        if (ret != 0) return ret;
        DreamListQt *dl = emitter.master.dream_list_master()->dream_list_qt();
        RUST_ASSERT_EQ(dl->data(dl->index(0, 0)), QVariant(QStringLiteral("1054-07-06")), err)
        RUST_ASSERT(
            dl->data(dl->index(0, 1)) == QVariant(QStringLiteral("Flying")), err
        )
        RUST_ASSERT_EQ(dl->data(dl->index(0, 2)), QVariant(QStringLiteral("Lucid Dream")), err)
        RUST_ASSERT(
            (dl->data(dl->index(0, 3)) == QVariant(QStringLiteral("Michael Jackson, God"))) ||
            (dl->data(dl->index(0, 3)) == QVariant(QStringLiteral("God, Michael Jackson"))),
            err
        )
        RUST_ASSERT_EQ(dl->data(dl->index(1, 1)), QVariant("Albus Dumbledore"), err)
        RUST_ASSERT_EQ(dl->data(dl->index(2, 1)), QVariant("Landscape"), err)
        RUST_ASSERT_EQ(dl->data(dl->index(4, 1)), QVariant("Bat"), err)
    }

    return 0;
}
