#ifndef TESTUTIL_HPP
#define TESTUTIL_HPP


#include <QString>


#include <bdg/Bindings.h>


extern "C" void write_err(void* err, const char *val);

extern "C" void test_setup_standard_diary(Master::Private *ptr);

void qstring_to_char(QString str, void* err);

#define RUST_ASSERT(x, err) if(!(x)) { \
    qstring_to_char(QString("Assertion failed in line ") \
    + QString::number(__LINE__) + QString(", file ") + QString(__FILE__), \
    err); \
    return -1; }
#define RUST_ASSERT_EQ(left, right, err) if(left != right) { \
    qstring_to_char(QString("Assertion failed in line ") \
    + QString::number(__LINE__) + QString(" : Left != Right\n"), \
    err); \
    return -1; }

#endif // TESTUTIL_HPP
