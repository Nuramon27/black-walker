#include "sight_dreamlist.hpp"

#include <QCoreApplication>
#include <QApplication>

#include <bdg/Bindings.h>
#include <DModel/ddreamlistview.hpp>

#include <scopedthread.hpp>
#include <generalemitter.hpp>
#include <tests/testutil.hpp>
#include <asetup.hpp>

SightDreamList::SightDreamList(Master *master) :
master(master){

}
void SightDreamList::do_test() {
    test_setup_standard_diary(master->m_d);
}

extern "C" qint32 test_sight_dreamlist(int argc, char *argv[], void* err)
{
    QApplication a(argc, argv);
    setup_meta_qt();
    ScopedThread workerthread;
    int ret;
    {

        GeneralEmitter emitter(workerthread, a);


        SightDreamList testemitter(&emitter.master);
        DDreamListView w(emitter.master.dream_list_master());
        QObject::connect(&testemitter, &SightDreamList::emt, &testemitter,
                         &SightDreamList::do_test, Qt::QueuedConnection);
        emit testemitter.emt();
        w.show();
        ret = a.exec();
        if (ret != 0) return ret;
    }

    return 0;
}
