#ifndef SIGHT_AWINDOW_HPP
#define SIGHT_AWINDOW_HPP


#include <QObject>

#include <QCoreApplication>
#include <QApplication>

#include <bdg/Bindings.h>
#include <awindow.hpp>


class SightAWindow: public QObject {
    Q_OBJECT
    Master *master;
public:
    SightAWindow(Master *master);
public slots:
    void do_test();
signals:
    void emt();
};

#endif // SIGHT_AWINDOW_HPP
