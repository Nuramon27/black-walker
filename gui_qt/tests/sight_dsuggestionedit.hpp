#ifndef SIGHT_DSUGGESTIONEDIT_HPP
#define SIGHT_DSUGGESTIONEDIT_HPP

#include <QCoreApplication>
#include <QApplication>

#include <bdg/Bindings.h>
#include <DModel/ddreamlistview.hpp>

class Master;


class SightDSuggestionEdit: public QObject {
    Q_OBJECT
    Master *master;
public:
    SightDSuggestionEdit(Master *master);
public slots:
    void do_test_sight_suggestionedit();
signals:
    void emt();
};

#endif // SIGHT_DSUGGESTIONEDIT_HPP
