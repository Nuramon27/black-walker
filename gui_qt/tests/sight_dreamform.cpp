#include "sight_dreamform.hpp"

#include <QCoreApplication>
#include <QApplication>

#include <bdg/Bindings.h>
#include <ddreamform.hpp>

#include <scopedthread.hpp>
#include <generalemitter.hpp>
#include <tests/testutil.hpp>
#include <asetup.hpp>

SightDreamForm::SightDreamForm(Master *master) :
master(master){

}
void SightDreamForm::do_test() {
    test_setup_standard_diary(master->m_d);
}

extern "C" qint32 test_sight_dreamform(int argc, char *argv[], void* err)
{
    QApplication a(argc, argv);
    setup_meta_qt();
    ScopedThread workerthread;
    int ret;
    {

        GeneralEmitter emitter(workerthread, a);


        SightDreamForm testemitter(&emitter.master);
        DDreamForm w(&emitter.master);
        QObject::connect(&testemitter, &SightDreamForm::emt, &testemitter,
                         &SightDreamForm::do_test, Qt::QueuedConnection);
        emit testemitter.emt();
        w.show();
        ret = a.exec();
        if (ret != 0) return ret;
    }

    return 0;
}
