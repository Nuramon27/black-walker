#ifndef SIGHT_DREAMFORM_HPP
#define SIGHT_DREAMFORM_HPP


#include <QObject>

#include <QCoreApplication>
#include <QApplication>

#include <bdg/Bindings.h>
#include <DModel/ddreamlistview.hpp>


class SightDreamForm: public QObject {
    Q_OBJECT
    Master *master;
public:
    SightDreamForm(Master *master);
public slots:
    void do_test();
signals:
    void emt();
};

#endif // SIGHT_DREAMFORM_HPP
