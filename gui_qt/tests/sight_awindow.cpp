#include "sight_awindow.hpp"

#include <QCoreApplication>
#include <QApplication>

#include <bdg/Bindings.h>
#include <ddreamform.hpp>

#include <scopedthread.hpp>
#include <generalemitter.hpp>
#include <tests/testutil.hpp>
#include <asetup.hpp>

SightAWindow::SightAWindow(Master *master) :
master(master){

}
void SightAWindow::do_test() {
    test_setup_standard_diary(master->m_d);
}

extern "C" qint32 test_sight_awindow(int argc, char *argv[], void* err)
{
    QApplication a(argc, argv);
    setup_meta_qt();
    ScopedThread workerthread;
    int ret;
    {

        GeneralEmitter emitter(workerthread, a);


        SightAWindow testemitter(&emitter.master);
        AWindow w(&emitter.master);
        QObject::connect(&testemitter, &SightAWindow::emt, &testemitter,
                         &SightAWindow::do_test, Qt::QueuedConnection);
        emit testemitter.emt();
        w.showMaximized();
        ret = a.exec();
        if (ret != 0) return ret;
    }

    return 0;
}
