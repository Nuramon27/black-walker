#ifndef SIGHT_DREAMLIST_HPP
#define SIGHT_DREAMLIST_HPP

#include <QObject>

#include <QCoreApplication>
#include <QApplication>

#include <bdg/Bindings.h>
#include <DModel/ddreamlistview.hpp>


class SightDreamList: public QObject {
    Q_OBJECT
    Master *master;
public:
    SightDreamList(Master *master);
public slots:
    void do_test();
signals:
    void emt();
};

#endif // SIGHT_DREAMLIST_HPP
