#include <QCoreApplication>
#include <QApplication>

#include <bdg/Bindings.h>
#include <DModel/ddreamlistview.hpp>

class Master;


class DreamListTest: public QObject {
    Q_OBJECT
    Master *master;
public:
    DreamListTest(Master *master);
    virtual ~DreamListTest() {}
public slots:
    void do_test_standard_diary();
Q_SIGNALS:
    void emt();
};
