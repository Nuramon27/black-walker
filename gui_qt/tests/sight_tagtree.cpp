#include "sight_tagtree.hpp"

#include <QCoreApplication>
#include <QApplication>

#include <bdg/Bindings.h>
#include <DDialogs/dtagadministration.hpp>

#include <scopedthread.hpp>
#include <generalemitter.hpp>
#include <tests/testutil.hpp>

SightTagTree::SightTagTree(Master *master) :
master(master){

}
void SightTagTree::do_test_sight_tagtree() {
    test_setup_standard_diary(master->m_d);
}

extern "C" qint32 test_sight_tagtree(int argc, char *argv[], void* err)
{
    QApplication a(argc, argv);
    QCoreApplication::setOrganizationName(QStringLiteral("Manuel Simon"));
    QCoreApplication::setApplicationName(QStringLiteral("Black Walker"));
    QCoreApplication::setApplicationVersion(QStringLiteral("1.1.21"));
    QFont font;
    QFont::insertSubstitutions(QStringLiteral("Noto Sans"),
                               QStringList({"Arial", "Helvetica", "DejaVu Sans", "FreeSans"}));
    font.setStyleHint(QFont::SansSerif, QFont::PreferOutline);
    font.setPointSize(10);
    font.setFamily(QStringLiteral("Noto Sans"));
    QGuiApplication::setFont(font);
    //qstring_to_char(QStringLiteral("Begin test"), err);
    ScopedThread workerthread;
    int ret;
    {
        GeneralEmitter emitter(workerthread, a);


        SightTagTree testemitter(&emitter.master);
        DDialogs::DTagAdministration w(&emitter.master, QStringLiteral("Traumzeichen"), nullptr);
        //DDreamListView w(emitter.master.dream_list_master());
        QObject::connect(&testemitter, &SightTagTree::emt, &testemitter,
                         &SightTagTree::do_test_sight_tagtree, Qt::QueuedConnection);
        emit testemitter.emt();
        w.show();
        ret = a.exec();
        if (ret != 0) return ret;
    }

    return 0;
}
