#include "testutil.hpp"


#include <QByteArray>


void qstring_to_char(QString str, void* err)
{
    QByteArray bytes = str.toUtf8();
    write_err(err, bytes.data());
}
