#include "dsearchformleaf.hpp"


#include <QSizePolicy>
#include <QRegExp>
#include <QRegExpValidator>


#include <QLineEdit>
#include <QComboBox>

#include <QHBoxLayout>


#include "aglobalsettings.hpp"

#include "bdg/Bindings.h"

#include "dsuggestionedit.hpp"


DSearchFormLeaf::DSearchFormLeaf(Master *new_master, QWidget *parent, bool first) :
    DAbstractSearchForm(parent, first),
    master(new_master)
{
    dreamentrycombobox = new QComboBox(this);
    //dreamentrycombobox->addItems({"Name", "Datum", "Text", "Anmerkungen"});
    //dreamentrycombobox->addItems(worker.getAllEntrynames());
    dreamentrycombobox->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Preferred);
    QStringList allTagnames(AE::getAllTagCategories(master->tag_tree_master()));
    allTagnames.prepend(QStringLiteral(""));
    dreamentrycombobox->addItems(allTagnames);

    conditionentry =
            new DSuggestionEdit(master, AE::DEntityType::TAG,
                                AE::DEnterTypeFlags() | AE::DEnterType::ANDSEP | AE::DEnterType::COMMASEP | AE::DEnterType::CONDITION,
                                AE::DAllowed::ALL, this, QString());
    conditionentry->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Preferred);
    /*conditionentry->setValidator(new QRegExpValidator(QRegExp(QStringLiteral("^[^;\"\']*$")),
                                                      conditionentry));*/

    entrybox = new QHBoxLayout();
    entrybox->addWidget(dreamentrycombobox);
    entrybox->addWidget(conditionentry);
    childrenbox->addLayout(entrybox);

    connect(dreamentrycombobox, SIGNAL(currentTextChanged(QString)),
            conditionentry, SLOT(setCategory(QString)));
}

DSearchFormLeaf::~DSearchFormLeaf()
{
}



///MEMBER FUNCTIONS
bool DSearchFormLeaf::isEmpty() const
{
    return conditionentry->text().isEmpty();
}

QString wrap_in_mark(QString s, QChar pre)
{
    return QString(pre) % QChar('\"') % s % QChar('\"');
}

QString DSearchFormLeaf::getCondition(SearchSettings settings) const
{
    if (isEmpty())
        return QString();
    else
    {
        QString leftside = dreamentrycombobox->currentText();
        QString res(conditionentry->text().trimmed());

        /* If the field asks for a name, text or note, then there are no things
         * like ::, > or ! and we can just replace every enity (separated by a ',' or a '&')
         * with [leftside]> "[entity]" and replace ',' with '|'.
         * */
        if (leftside == QStringLiteral("Name") ||
                leftside == QStringLiteral("Text") || leftside == QStringLiteral("Anmerkungen"))
        {
            res = res.split(AGlobalSettings::commaregexp,
                            QString::SkipEmptyParts).join("\" | #"
                                                          + leftside + " > c\"");
            res = res.split(AGlobalSettings::andregexp,
                            QString::SkipEmptyParts).join("\" & #"
                                                          + leftside + " > c\"");
            res = res.trimmed();

            return "#" + leftside + " > c\"" + res + "\"";
        }

        /* If the field asks for a tag (or "any" tag) we must first obtain every entity
         * (separated by ',' or '&') and then investigate it carefully for prefixes and
         * suffixes.
         * */
        if (master->tag_tree_master()->tagCategoryExists(leftside) || leftside == QStringLiteral(""))
        {
            /* At first we obtain the parts separated by a comma.
             * */
            QStringList res1 = res.split(AGlobalSettings::commaregexp,
                                         QString::SkipEmptyParts);
            QStringList res2;
            for (int i = 0; i < res1.size(); ++i)
            {
                /* ires2 will contain the conditions yet examined for this part.
                 * */
                QStringList ires2;
                /* And then for each of these parts we obtain the parts
                 * separated by a '&'.
                 * */
                QStringList ires1 = res1.at(i).split(AGlobalSettings::andregexp,
                                         QString::SkipEmptyParts);
                for (int j = 0; j < ires1.size(); ++j)
                {
                    //s is the current entity
                    QString s = ires1.at(j);
                    /* prestring will contain the suffix for the tag category,
                     * i.e. ':', '::', ':>' or '::>'
                     * */
                    QString prestring = (settings.withgroups)? QStringLiteral("::") : QStringLiteral(":");
                    /* no will contain the prefix for the condition,
                     * i.e. a '!' if the entity starts with a '!'.
                     * */
                    QString no = QString();
                    if (s.startsWith('!'))
                    {
                        no.append('!');
                        /* Every time we find a suffix we remove it from s as it is now
                         * evaluated.
                         * */
                        s.remove(0, 1);
                    }
                    if (s.startsWith("="))
                    {
                        if (settings.withgroups)
                            prestring.remove(0, 1);
                        s.remove(0, 1);
                    }
                    if (s.startsWith(":"))
                    {
                        if (!settings.withgroups)
                            prestring.append(":");
                        s.remove(0, 1);
                    }
                    if (s.startsWith(">"))
                    {
                        prestring.append(">");
                        s.remove(0, 1);
                    }

                    s = s.trimmed();
                    /* Finally we investigate the suffix of the entity.
                     * If it is a '=', we only search for the strong form,
                     * if it is a '*', we only search for the weak form,
                     * if it is nothing, we search for both forms.
                     * */
                    if (s.endsWith('='))
                    {
                        s.chop(1);
                        ires2.append(no + "#" + wrap_in_mark(leftside, 't') + prestring + wrap_in_mark(s, 'c'));
                    }
                    else if (!s.endsWith("*"))
                    {
                        if (settings.withweak)
                        {
                            ires2.append(no + "(" + "#" + wrap_in_mark(leftside, 't') + prestring + ' ' + wrap_in_mark(s, 'c') + " | "
                                      + "#" + wrap_in_mark(leftside, 't') + prestring + ' ' + wrap_in_mark(s + '*', 'c') + ")");
                        }
                        else
                        {
                            ires2.append(no + "#" + wrap_in_mark(leftside, 't') + prestring + wrap_in_mark(s, 'c'));
                        }
                    }
                    else
                    {
                        ires2.append(no + "#" + wrap_in_mark(leftside, 't') + prestring + wrap_in_mark(s, 'c'));
                    }
                }

                /* Finally we join the parts connected with the right logical operator.
                 * */
                res2 += ires2.join(" & ");
            }
            return res2.join(" | ");

        }
        /* For entry categorie we do the same as for name, text or notes,
         * but here we do not search for entries that contain the given string
         * but for entries that are equal to the given string,
         * so the operator is a '==' and not a '>'.
         * */
        /*else if (master->entry_master()->entryCategoryExists(leftside))
        {
            res = res.split(AGlobalSettings::commaregexp,
                            QString::SkipEmptyParts).join("\" | "
                                                          + leftside + " == \"");
            res = res.split(AGlobalSettings::andregexp,
                            QString::SkipEmptyParts).join("\" & "
                                                          + leftside + " == \"");
            res = res.trimmed();
            return leftside + " == \"" + res + "\"";
        }*/
        else
        {
            return leftside + "# == " + wrap_in_mark(res, 'c');
        }
    }
}

void DSearchFormLeaf::selectEntity(const QString &entity)
{
    dreamentrycombobox->setCurrentText(entity);
    conditionentry->setCategory(entity);
}
