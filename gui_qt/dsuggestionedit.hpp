#ifndef DSUGGESTIONEDIT_HPP
#define DSUGGESTIONEDIT_HPP


#include <QLineEdit>


#include <QFlags>
#include <QStringList>

class QCompleter;

class QFocusEvent;

class QStringListModel;
class QListView;


#include "zdef.hpp"

#include <Enum/dallowed.hpp>
#include <Enum/dentitytype.hpp>
#include <Enum/dentertype.hpp>


#include <bdg/Bindings.h>


extern "C" void init_suggestion(Master::Private *masterptr, SuggestionQt::Private *suggestionptr);

/**
 * @brief The DSuggestionEdit class is a special QLineEdit that can be used for entering tags or entries
 * of dreams but als tag or entry categories and provides suggestions for its input,
 * based on the tags, entries or categories that are registered in an AWorker object #worker.
 *
 * The type of input -- tag, entry, category (summarized as entities)
 * -- is specified by giving an AE::DEntityType as argument to
 * the constructor. But there are also different AE::DEnterTypes that specify how the entities are entered,
 * e.g. if only one entity may be entered or a comma separated list of entities.
 *
 * In some applications the user is expected to only enter groups of tags, but no tags that are
 * directly used as tags in the dreams. This means nothing else as that only tags are searched that
 * have children.
 *
 * So if the Suggestion Edit shall contain tags, then one can also specify which types of tags are allowed,
 * i.e. if only groups are allowed, or only leafs or any tags. This can be given as an instance of the
 * enum Allowed that is passed to the contructor.
 *
 * A category in which the DSuggestionEdit shall search can be given with setCategory(),
 * if no category is set it searches in all categories.
 */
class DSuggestionEdit : public QLineEdit
{
    Q_OBJECT

public:



private:
    QString itext;
    int icursorposition;
    /**
     * @brief The QCompleter that is in this case only used for presenting the suggestions,
     * not for doing any searches for suggestions.
     */
    QCompleter *suggestioncomp;

    SuggestionQt *suggestion_model;

public:
    /**
     * @brief Constructs a new DSuggestionEdit with #worker object newworker and parent parent.
     * @param newtype The type of suggestions that shall be displayed, i.e. tags or entries
     * @param newmode The AE::DEnterType of the suggestion edit, e.g. if several tags may be entered
     * comma separated or if groups can be given for the tags
     * @param newallowedentities If tags shall be entered in the suggestionedit,
     * this specifies whether all tags shall be suggested or only groups or only leaf tags
     * without children
     * @param newcategory The tag or entry catgeory from which shall be suggested.
     * If empty, suggestions are taken out of all categories.
     */
    DSuggestionEdit(Master *master,
                    AE::DEntityType newtype, AE::DEnterTypeFlags newmode, AE::DAllowedFlags newallowedentities,
                    QWidget *parent = nullptr, QString newcategory = QString());
    /**
     * @brief Destructs the DSuggestionEdit
     * */
    ~DSuggestionEdit();

protected:
    /**
     * @brief Connects several signals so that the Suggestion edit recieves suggestions
     * and the information that a suggestion was selected ...
     *
     * Connects the AWorker::searchSuggestionfinished() signal to presentSuggestions()
     * so that the suggestions edit recievies suggestion results.
     *
     * Furthermore reconnects the QCompleter::highlighted() and QCompleter::activated()
     * to complete() and chooseCombletion(), as we have to customize the way
     * in which tho choosing of a suggestions affects the content in the QLineEdit
     * (especially when comma separated tags may be entered, we only have to replace the
     * tag that is currently edited by the suggestion, not the whole text)
     * @param event
     */
    virtual void focusInEvent(QFocusEvent *event);
    /**
     * @brief Disconnects several signals, especially the AWorker::searchSuggestionfinished(),
     * so that the suggestion edit only recieves (and displays) suggestions when it
     * has focus (otherwise when typing in one suggestion edit anywhere,
     * all suggestion edits would present the loaded suggestions)
     */
    virtual void focusOutEvent(QFocusEvent *event);

private slots:
    /**
     * @brief Interprets the content of the Line Edit and loads suggestions
     * for the entity that is currently endited.
     */
    void loadSuggestions();
    /**
     * @brief Replaces the entity that is currently edited with suggestion
     */
    void complete(QString const &chosensuggestion);
    /**
     * @brief Replaces the entity that is currently edited with suggestion and selects it
     */
    void chooseCompletion(QString const &chosensuggestion);

public slots:
    /**
     * @brief Sets the #category to newcategory
     */
    void setCategory(QString const &newcategory);
};

#endif // DSUGGESTIONEDIT_HPP
