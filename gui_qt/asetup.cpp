#include "asetup.hpp"

#include <iostream>

#include <QCoreApplication>
#include <QGuiApplication>
#include <QApplication>

#include <QFont>
#include <QIcon>
#include <QPixmap>


#include <scopedthread.hpp>
#include <generalemitter.hpp>

void setup_meta_qt()
{
    Q_INIT_RESOURCE(resource);
    QCoreApplication::setOrganizationName(QStringLiteral("Manuel Simon"));
    QCoreApplication::setApplicationName(QStringLiteral("Black Walker"));
    QCoreApplication::setApplicationVersion(QStringLiteral("2.4.0"));
    QFont font;
    font.insertSubstitutions(QStringLiteral("Noto Sans"),
                               QStringList({"Arial", "Helvetica", "DejaVu Sans", "FreeSans"}));
    font.setStyleHint(QFont::SansSerif, QFont::PreferOutline);
    font.setPointSize(11);
    font.setFamily(QStringLiteral("Noto Sans"));
    QApplication::setStyle(QStringLiteral("Fusion"));
    QGuiApplication::setFont(font);
    QPixmap icon(QStringLiteral(":/icon/appicon128x128.png"));
    QGuiApplication::setWindowIcon(icon);
}
