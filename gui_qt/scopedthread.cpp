#include "scopedthread.hpp"


ScopedThread::ScopedThread(QObject *parent) :
    QThread(parent)
{
}

ScopedThread::~ScopedThread()
{
    quit();
    wait();
}
