#ifndef DENTITYTYPE_HPP
#define DENTITYTYPE_HPP


namespace AE
{
/**
 * @brief The DEntityType enum is used to specify the type of entity (tag or entry) that can
 * be entered in the DSuggestionEdit.
 */
enum class DEntityType : unsigned char
{
    TAG = 0x00,///< The DSuggestionEdit contains tags
    ENTRY = 0x01,///< The DSuggestionEdit contains entries
    TAGCATEGORY = 0x02,///< The Edit contains tag categories
    ENTRYCATEGORY = 0x03,///< The Edit contains entry categories
};


}

#endif // DENTITYTYPE_HPP
