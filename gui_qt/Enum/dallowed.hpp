#ifndef DALLOWED_HPP
#define DALLOWED_HPP


#include <QFlags>


#include "zdef.hpp"


namespace AE
{
/**
 * @brief The DAllowed enum specifies wich types of tags are expected in the QLineEdit:
 * Only groups of tags, i.e. tags that have children, or only leaf tags that have no children
 */
enum class DAllowed : unsigned char
{
    NONE = 0x0,///< Nothing is allowed
    LEAF = 0x1,///< Tags without children (i.e. leafs in the tag tree) are allowed
    GROUP = 0x2,///< Tags with children are allowed
    ALL = 0x3///< Convenience typedef for LEAF | GROUP, anything is allowed
};

typedef QFlags<DAllowed> DAllowedFlags;


}


Q_DECLARE_METATYPE(AE::DAllowedFlags)


#endif // DALLOWED_HPP
