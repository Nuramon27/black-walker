#ifndef DENTERTYPE_HPP
#define DENTERTYPE_HPP


#include <QtGlobal>


namespace AE
{
/**
 * @brief The DEnterType enum specifies how the entities are entered, i.e. if only one entity
 * shall be entered, or a comma separeted list or more complex applications.
 */
enum class DEnterType : unsigned char
{
    NONE = 0x0,///< Only one singel entity shall be entered, without category etc.
    COMMASEP = 0x4U,///< Multiple comma separated entities are allowed
    ANDSEP = 0x8U,
    /**
     * @brief A Category can directly entered in the QLineEdit, otherwise it must be specifyed externally
     * */
    WITHCATEGORY = 0x10U,///< A Category can be entered, followed by a double dot
    WITHGROUPS = 0x20U,///< A descending list of groups can be entered, each followed by a double dot
    /**
     * @brief Multiple units of tags, possibly comma separated, possibly together with a category,
     * possibly with groups, can be entered -- separated by semicola
     * */
    MULTIPLEUNITS = 0x40U,
    CONDITION = 0x80U,
    /**
     * @brief Convenience Typedef for COMMASEP | WITHCATEGORY | WITHGROUPS | MULTIPLEUNITS
     * */
    TAGSWITHGROUP = 0x40U | 0x20U | 0x10U | 0x4U
};

typedef QFlags<DEnterType> DEnterTypeFlags;


}

#endif // DENTERTYPE_HPP
