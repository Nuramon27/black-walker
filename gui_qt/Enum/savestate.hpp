#ifndef SAVESTATE_HPP
#define SAVESTATE_HPP


namespace AE
{
/**
 * @brief A modification state of the Diary
 */
enum class SaveState : unsigned char
{
    Unmodified = 0x00,///< The diary is unmodified since the last change.
    Modified = 0x01,///< The diary has been modified since the last change.
    New = 0x02,///< The diary is completely new and empty, there as not yet been a single change.
};


}

#endif // SAVESTATE_HPP
