#include <QApplication>

#include "awindow.hpp"

#include "generalemitter.hpp"
#include "asetup.hpp"

#include <scopedthread.hpp>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    setup_meta_qt();

    ScopedThread workerthread;
    int ret;
    {
        GeneralEmitter emitter(workerthread, a);
        AWindow w(&emitter.master);
        w.showMaximized();

        ret = a.exec();
    }
    return ret;
}
