#ifndef AE_HPP
#define AE_HPP


class QChar;
#include <QHash>
#include <QMultiHash>
#include <QSet>
class QString;
class QStringList;
class QTextStream;
class QVariant;

class QWidget;
class QTreeView;
class QAbstractItemModel;
class QComboBox;


class DTag;
class SettingsQt;
class TagTreeMaster;

/**
 * @brief The AE namespace contains functions and definitions that are useful throughout the
 * application, such as getNextLatexInstruction() and getTextUntilLatexInstruction() for parsing
 * latex files.
 */
namespace AE
{
/**
 * @brief A set of Strings. Used in AWorker for storing entry values of a fixed category.
 */
typedef QSet<QString> QStringSet;
/**
 * @brief An iterator for a QStringSet
 */
typedef QSet<QString>::iterator QStringSetit;
/**
 * @brief A const iterator for a QStringSet
 */
typedef QSet<QString>::const_iterator QStringSetcit;
/**
 * @brief A QHash that connects a QString with a QStringSet. Used in AWorker for storing the set
 * of all entries of all categories.
 */
typedef QHash<QString, QStringSet > QStringSetHash;
/**
 * @brief An iterator for a QStringSetHash
 */
typedef QHash<QString, QStringSet >::iterator QStringSetHashit;
/**
 * @brief A const iterator for a QStringSetHash
 */
typedef QHash<QString, QStringSet >::const_iterator QStringSetHashcit;
/**
 * @brief A QHash that connects a QString with one other QString. Used in DDream to store its entries
 * of all categories.
 */
typedef QHash<QString, QString> QStringHash;
/**
 * @brief An iterator for a QStringHash
 */
typedef QHash<QString, QString>::iterator QStringHashit;
/**
 * @brief A const iterator for a QStringHash
 */
typedef QHash<QString, QString>::const_iterator QStringHashcit;
/**
 * @brief A QMultiHash that connects a QString with many other QStrings. Used in DDream to store its
 * tags of all categories.
 */
typedef QMultiHash<QString, QString> QStringMulHash;
/**
 * @brief An iterator for a QStringMulHash
 */
typedef QMultiHash<QString, QString>::iterator QStringMulHashit;
/**
 * @brief A const iterator for a QStringMulHash
 */
typedef QMultiHash<QString, QString>::const_iterator QStringMulHashcit;
/**
 * @brief A QHash that connects a QString with a QVariant. Used for storing settings.
 */
typedef QHash<QString, QVariant> QVarHash;
/**
 * @brief An iterator for a QVarHash
 */
typedef QHash<QString, QVariant>::iterator QVarHashit;
/**
 * @brief A const iterator for a QVarHash
 */
typedef QHash<QString, QVariant>::const_iterator QVarHashcit;



/**
 * @brief A QHash, that connects a name of a Tag (of a fixed category) with its DTag.
 */
typedef QHash<QString, DTag> DTagPtrHash;
/**
 * @brief An iterator for DTagPtrHash
 */
typedef QHash<QString, DTag>::iterator DTagPtrHashit;
/**
 * @brief A const iterator for DTagPtrHash
 */
typedef QHash<QString, DTag>::const_iterator DTagPtrHashcit;
/**
 * @brief A QHash, that connects a category of tags with the DTagPtrHash of tags in this category.
 * Used for storing tha table of all tags used in dreams.
 */
typedef QHash<QString, DTagPtrHash > DTagTree;
/**
 * @brief An iterator to DTagTree
 */
typedef QHash<QString, DTagPtrHash >::iterator DTagTreeit;
/**
 * @brief A const iterator to DTagTree
 */
typedef QHash<QString, DTagPtrHash >::const_iterator DTagTreecit;

void setSizeWidget(SettingsQt const *settings_qt, QWidget *widget, QString const &name);
void storeSizeWidget(SettingsQt *settings_qt, QWidget const *widget, QString const &name);
void setColumnSizes(SettingsQt const *settings_qt, QTreeView *view, QString const &name, quint32 nr);
void storeColumnSizes(SettingsQt *settings_qt, QTreeView const *view, QString const &name, quint32 nr);

QStringList getStandardTagCategories(SettingsQt *settings_qt);
QStringList getAllTagCategories(TagTreeMaster *tag_tree_master);

void set_combobox_with_model(QComboBox &box, QAbstractItemModel const &model);
void connect_combobox_with_model(QComboBox &box, QAbstractItemModel const &model);

/**
 * @brief Returns whether c is a character that marks the end of a Latex Instruction
 */
bool isEndOfLatexInst(QChar c);
/**
 * @brief Reads stream until it finds a latex instruction. Returns the first instruction it finds
 * together with its arguments
 * @return The first element of the QStringList is the name of the instruction it found,
 * without the leading \\. The other elements are the arguments in the order in which they appeared.
 * Never returns an empty List, if no instruction is found, a List only containning an enmpty string is returned.
 *
 * After applying this function, the stream will be positioned directly after the last closing '}'
 */
QStringList getNextLatexInstruction(QTextStream &stream);
/**
 * @brief Returns the latex instruction that begins at the current point in stream and returns it
 * together with its arguments
 * @return The first element of the QStringList is the name of the instruction it found,
 * without the leading \\. The other elements are the arguments in the order in which they appeared.
 * Never returns an empty List, if there is no instruction, a List only containing an enmpty string is returned.
 *
 * stream should point to the place after the leading \\ of the instruction. This function does not
 * check whether theres really a \\, it simply returns anything until it reaches the
 * end of a latex instruction or a } without a following {
 *
 * After applying this function, the stream will be positioned directly after the last closing '}'
 */
QStringList getCurrLatexInstruction(QTextStream &stream);
/**
 * @brief Reads a Latex Argument from stream
 * @deprecated
 */
QString getLatexArgument(QTextStream &stream);
/**
 * @brief Returns all text in stream until the instruction inst with arguments arg appears
 *
 * If arg is empty, it checks only for the appearance of the instruction, whether it has arguments
 * or not. If not, the arguments of the instruction must exactly match those in arg
 *
 * After applying this function, the stream will be positioned directly after the last closing '}'
 * of the given Instruction
 */
QString getTextUntilLatexInstruction(QTextStream &stream, QString const &inst,
                                     QStringList const &arg = QStringList());
}

#endif // AE_HPP
