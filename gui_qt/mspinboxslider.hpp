#ifndef MSPINBOXSLIDER_HPP
#define MSPINBOXSLIDER_HPP




#include <QWidget>

#include <QtGlobal>

class QString;
class QSlider;
class QSpinBox;
class QBoxLayout;


/**
 * @brief The MSpinBoxSlider class A synchronized slider and spinbox.
 *
 * Every movement of the slider affects tha value of the spinbox
 * and every value change of the spinbox affexts the position of the slider.
 */
class MSpinBoxSlider : public QWidget
{
    Q_OBJECT
    QSlider *slider;
    QSpinBox *spinbox;

    QBoxLayout *mainbox;

public:
    explicit MSpinBoxSlider(QWidget *parent = nullptr);
    MSpinBoxSlider(int minimum, int maximum, Qt::Orientation orientation = Qt::Horizontal, QWidget *parent = nullptr);
    ~MSpinBoxSlider();

    /**
     * @brief getValue Returns the actual value of the spinboxslider.
     * @return
     */
    int getValue() const;
    /**
     * @brief getMinimum Returns the minimum value of the spinboxslider.
     * @return
     */
    int getMinimum() const;
    /**
     * @brief getMaximum Returns the maximum value of the spinboxslider.
     * @return
     */
    int getMaximum() const;
    /**
     * @brief getSingleStep Returns the single step of the spinboxslider.
     * @return
     */
    int getSingleStep() const;
    /**
     * @brief getPageStep Returns the page step of the slider.
     * @return
     */
    int getPageStep() const;
    /**
     * @brief getDisplayIntegerBase Returns the integer base of the spinbox.
     * @return
     */
    int getDisplayIntegerBase() const;
    /**
     * @brief getSuffix Returns the suffix of the spinbox.
     * @return
     */
    QString getSuffix() const;
    /**
     * @brief getOrientation Returns the orientation of the slider->
     * @return
     */
    Qt::Orientation getOrientation() const;

    /**
     * @brief setMinimum Sets the minimum value of the spinboxslider.
     * @param minimum
     */
    void setMinimum(int minimum);
    /**
     * @brief setMaximum Sets the maximum value of the spinboxslider.
     * @param maximum
     */
    void setMaximum(int maximum);
    /**
     * @brief setRange Sets the value range of the spinboxslider.
     * @param minimum
     * @param maximum
     */
    void setRange(int minimum, int maximum);
    /**
     * @brief setSingleStep Sets the single step of the spinboxslider.
     * @param step
     */
    void setSingleStep(int step);
    /**
     * @brief setPageStep Sets the page step of the slider->
     * @param step
     */
    void setPageStep(int step);
    /**
     * @brief setDisplayIntegerBase Sets the integer base of the spinbox.
     * @param base
     */
    void setDisplayIntegerBase(int base);
    /**
     * @brief setSuffix Sets the suffix of the spinbox.
     * @param suffix
     */
    void setSuffix(QString const &suffix);
    /**
     * @brief setOrientation Sets the orientation of the spinboxslider
     * @param orientation
     *
     * In a horizontal oriented spinboxslider the slider is at the left and the spinbox at the right.
     * In a vertical oriented spinboxslider the slider is above and the spinbox yonder.
     */
    void setOrientation(Qt::Orientation orientation);

signals:
    /**
     * @brief valueChanged Is emitted everytime the value of the spinboxslider changes
     * @param value
     *
     * The signal is not emitted when the setValue slot is called.
     */
    void valueChanged(int value);
    /**
     * @brief editingFinished Is emitted when the spinbox emits it's editingFinished signal
     * of the slider emits it's sliderReleased signal
     * @param value
     */
    void editingFinished(int value);

private slots:
    /**
     * @brief emiteditingFinished emits the editingFinished Signal with the current value
     */
    void emiteditingFinished();

public slots:
    /**
     * @brief setValue Sets the value of the spinboxslider.
     * @param value
     */
    void setValue(int value);
};

#endif // MSPINBOXSLIDER_HPP
