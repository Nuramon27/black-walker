#ifndef DNIGHTDIALOG_HPP
#define DNIGHTDIALOG_HPP

#include <QDialog>

#include <QList>
#include <QTime>

class QLabel;
class QPushButton;
class QTimeEdit;
class MSpinBoxSlider;

class QVBoxLayout;
class QHBoxLayout;
class QGridLayout;

class NightQt;
class SettingsQt;
class EntryMaster;

struct SleepPhaseEdit
{
    QLabel *wakeup_label;
    QTimeEdit *wakeup_entry;
    QLabel *fallasleep_label;
    QTimeEdit *fallasleep_entry;
    QLabel *ease_of_fallasleep_label;
    MSpinBoxSlider *ease_of_fallasleep_slider;

    SleepPhaseEdit(QTime start_time, EntryMaster *entrymaster);
    ~SleepPhaseEdit();

    void add_to_grid(QGridLayout *grid, int index);
    void remove_from_grid(QGridLayout *grid);
};


class DNightDialog : public QDialog
{
    QList<SleepPhaseEdit*> sleep_phase_edit;

    QPushButton *ok_button;
    QPushButton *cancel_button;
    QPushButton *add_button;
    QPushButton *remove_button;

    QGridLayout *sleep_phase_box;
    QHBoxLayout *button_box;
    QVBoxLayout *mainbox;

    NightQt &night_model;
    SettingsQt *settings_qt;
    EntryMaster *entrymaster;
public:
    DNightDialog(NightQt &new_night_model, SettingsQt *new_settings_qt, EntryMaster *new_entrymaster, QWidget *parent = nullptr);
    virtual ~DNightDialog();

private:
    void push_sleep_phase();
    void pop_sleep_phase();
    void try_push_sleep_phase();
    void save_night();
    void present_night();
};

#endif // DNIGHTDIALOG_HPP
