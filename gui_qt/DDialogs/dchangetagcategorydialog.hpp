#ifndef DCHANGETAGCATEGORYDIALOG_HPP
#define DCHANGETAGCATEGORYDIALOG_HPP


#include <QDialog>


#include <QString>

class QWidget;
class QLabel;
class QLineEdit;
class QPushButton;

class QVBoxLayout;
class QHBoxLayout;

#include <dsuggestionedit.hpp>

class Master;


namespace DDialogs
{
/**
 * @brief The DChangeTagCategoryDialog class lets the user enter a name of a category.
 *
 * This entered name can be obtained with getNewCategory().
 */
class DChangeTagCategoryDialog : public QDialog
{
    Q_OBJECT

    bool _create_category;

    QLabel *newcategorylabel;
    DSuggestionEdit *newcategoryentry;
    QPushButton *okbutton;
    QPushButton *cancelbutton;

    QVBoxLayout *mainbox;
    QHBoxLayout *buttonbox;
    QHBoxLayout *categorybox;

    Master *master;

public:
    /**
     * @brief Constructs a new DChangeTagCategoryDialog with parent parent.
     */
    DChangeTagCategoryDialog(Master *newmaster, QWidget *parent = nullptr);
    /**
     * @brief Destructs the DChangeTagCategoryDialog.
     * */
    ~DChangeTagCategoryDialog();

    /**
     * @brief Returns true if the created category does not exist and the user has confirmed
     * that he wants to create it.
     */
    bool create_category() const;
    /**
     * @brief Returns the entered category
     */
    QString getNewCategory() const;

private slots:
    void complete();
};


}

#endif // DCHANGETAGCATEGORYDIALOG_HPP
