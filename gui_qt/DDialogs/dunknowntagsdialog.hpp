#ifndef DUNKNOWNTAGSDIALOG_HPP
#define DUNKNOWNTAGSDIALOG_HPP


#include <QDialog>


#include <QList>
#include <QSet>
#include <QHash>
#include <QString>

class QSignalMapper;

class QLabel;
class QPushButton;
class QComboBox;

class QHBoxLayout;
class QVBoxLayout;
class QGridLayout;


#include <zdef.hpp>

class Master;
class TagTreeMaster;
class TagList;
class DSuggestionEdit;


namespace DDialogs
{
/**
 * @brief In the DUnknownTagsDialog the user can enter categories and groups of tags that were
 * left unspecifyed when entering the dream.
 *
 * It consists of a list of rows, in which the user can specify tags.
 * Each row begins with a #categorycombobox, in which the user can specify the category, which
 * is followed by a #taglabel, in which the name of the tag to specify stands.
 * By clicking a plus button at the end of the row, the user can specify
 * the group of the tag in this row too, i.e. put it into another group.
 */
class DUnknownTagsDialog : public QDialog
{
    Q_OBJECT
    /**
     * @brief The default button of the DDreamInputDialog, which creates the tags.
     */
    QPushButton *okbutton;
    QPushButton *rejectbutton;

    QLabel *categoryheader, *tagheader, *groupheader;
    QList<QLabel *> taglabel, grouplabel;
    QList<QComboBox *> categorycombobox;
    QList<DSuggestionEdit *> groupentry;
    QList<QPushButton *> specifygroupbutton;
    /**
     * @brief Stores for each tag for which a group is specifyed which is unknown,
     * the index of the entry where the group of this tag can be specifyed
     */
    QHash<int, int> tagtogroup;
    /**
     * @brief Stores all tags that shall be specifyed in this Dialog together with their category;
     */
    QSet<QString> tagstospecify;

    QGridLayout *tagbox;
    QHBoxLayout *buttonbox;
    QVBoxLayout *mainbox;

    QSignalMapper *tagmapper;
    QSignalMapper *specifygroupmapper;

    Master *master;
    TagList *model;

public:
    /**
     * @brief Constructs a new DUnknownTagsDialog, demanding Specification of
     * the tags unknowntags und the groups unknowngroups.
     */
    DUnknownTagsDialog(Master *newmaster, TagList *newmodel, QWidget *parent = nullptr);
    /**
     * @brief Destructs the DUnknownTagsDialog
     * */
    ~DUnknownTagsDialog();

private:
    /**
     * @brief Returns the text in the label at position index, whether there is a grouplabel or
     * a taglabel
     *
     * index must be nonnegative and lower than textlabel.size() + grouplabel.size().
     */
    QString textAtIndex(int index) const;

protected:
    void resizeEvent(QResizeEvent *event);

public:
    /**
     * @brief Returns the specified tags together with there category
     * @return Only the tags in #taglabel are returned, as they are the ones that shall be inserted into a
     * dream. The groups in #grouplabel should only be registered, eventually together with their group,
     * but they shall not be tags of a new dream.
     */
    AE::QStringMulHash getNewTags() const;

signals:
    /**
     * @brief Signal to #worker, that a new tag with category, name tag und parent group
     * shall be created
     */
    void createNewKnownTagSignal(QString category, QString tag, QString group);

public slots:
    /**
     * @brief Gives signals to #worker in order to create the specifyed tags und groups.
     */
    void createEnteredTags();
    /**
     * @brief Appends a new row where the user can specify the group at index.
     *
     * The new row is not creared if the group at index already exists or if there is currently a row where
     * this group is specifyed.
     */
    void newEntryForGroup(int index);
    /**
     * @brief Adapts category and tagname of the row in which the group at index can be specifyed
     */
    void adaptEntryForGroup(int index);
};


}

#endif // DUNKNOWNTAGSDIALOG_HPP
