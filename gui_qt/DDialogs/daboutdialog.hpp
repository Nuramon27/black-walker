#ifndef DABOUTDIALOG_HPP
#define DABOUTDIALOG_HPP


#include <QDialog>


class QLabel;
class QPushButton;

class QHBoxLayout;
class QVBoxLayout;


namespace DDialogs
{
class DAboutDialog : public QDialog
{
    Q_OBJECT

    QLabel *aboutlabel;
    QPushButton *okbutton, *copyrightbutton, *warrantybutton, *aboutqtbutton;

    QHBoxLayout *buttonbox;
    QVBoxLayout *mainbox;

public:
    DAboutDialog(QWidget *parent = nullptr);

public slots:
    void showcopyright();
    void showwarranty();
    void showaboutqt();
};


}

#endif // DABOUTDIALOG_HPP
