#ifndef DSEARCHDIALOG_HPP
#define DSEARCHDIALOG_HPP


#include <QDialog>


class QSize;
class QString;
class QModelIndex;

class QLabel;
class QCheckBox;
class QPushButton;
class QLineEdit;

class QTreeView;
class QHeaderView;

class QGroupBox;

class QHBoxLayout;
class QVBoxLayout;
class QGridLayout;


#include <zdef.hpp>

class DSuggestionEdit;
class DDreamListModel;
class DSearchForm;

class SearchQt;
class DreamListQt;
class Master;


namespace DDialogs
{
/**
 * @brief in the DSearchDialog, the user can search for dreams by giving arbitrarily complex conditions.
 *
 * It contains a tree of DAbstractTagEdits beginning with #generalForm, in which the user can enter
 * arbitrary conditions. This tree is initialized with the standard tags.
 *
 * Furthermore there are entries for entering criteria for most entries of DDream.
 *
 * By clicking the search button, the searchDreamSignal() is emitted, so that the AWorker object
 * searches for dreams meeting the given condition. Its signal AWorker::searchDreamfinished()
 * is catched by the slot recievesearchDream(), which presents the search results in the DSearchDialog.
 *
 * The entered condition can be retrieved with getCondition().
 *
 * The search results are displayed in a QTreeView using a DDreamListModel. By clicking on a dream
 * the user can view it in a DDreamInputDialog.
 */
class DSearchDialog : public QDialog
{
    Q_OBJECT

    QCheckBox *withgroupcheckbox, *weaktagscheckbox;
    /**
     * @brief Clicking this button cancels the dialog
     */
    QPushButton *rejectbutton;
    /**
     * @brief The default button of the QDialog, which starts the search
     */
    QPushButton *searchbutton;

    QLabel *namelabel, *kindlabel, *techniquelabel, *textlabel;
    QLineEdit *nameentry, *textentry;
    DSuggestionEdit *kindentry, *techniqueentry;
    QGroupBox *entrygroup, *generalgroup;

    QTreeView *resultview;
    QHeaderView *resultheader;

    /**
     * @brief The root of the @link DAbstractSearchForm DAbstractSearchForms @endlink where the
     * search conditions can be entered.
     */
    DSearchForm *generalForm;

    QHBoxLayout *withgroupbox;
    QGridLayout *entrybox;
    QVBoxLayout *entrymetabox;
    QVBoxLayout *generalbox;
    QHBoxLayout *searchbox;
    QHBoxLayout *buttonbox;
    QVBoxLayout *mainbox;

    SearchQt *search_qt;
    DreamListQt *resultmodel;
    Master *master;

public:
    /**
     * @brief Constructs the DSearchDialog with worker object AWorker and parent parent.
     */
    explicit DSearchDialog(Master *new_master, QWidget *parent = nullptr);
    /**
     * @brief Destructs the DSearchDialog
     * */
    virtual ~DSearchDialog() override;

private:
    virtual QSize sizeHint() const override;

    /**
     * @brief Returns the entered condition as a condition string that can be interpreted by
     * MBascInstance::DDreamCondition.
     */
    QString getCondition() const;

signals:
    /**
     * @brief Signals #worker to start a search for dreams meeting condition.
     */
    void searchDreamSignal(QString condition);

public slots:
    /**
     * @brief Catches the entered condition and emits searchDreamSignal().
     */
    void searchDream();
    /**
     * @brief Opens a DDreamInputDialog that displays the dream under index.
     */
    void presentDream(QModelIndex const &index);
};


}

#endif // DSEARCHDIALOG_HPP
