#include "dcreatetagsdialog.hpp"


#include <QStringBuilder>

#include <QLabel>
#include <QPushButton>

#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QGridLayout>

#include <QMessageBox>


#include <dsuggestionedit.hpp>
#include <DTagEdit/dstatictagedit.hpp>
#include <Enum/dallowed.hpp>
#include <Enum/dentitytype.hpp>
#include <Enum/dentertype.hpp>



namespace DDialogs
{
DCreateTagsDialog::DCreateTagsDialog(Master *newmaster, const QString &newcategory, QWidget *parent) :
    QDialog(parent),
    master(newmaster)
{
    setWindowTitle(QStringLiteral("Neue Tags erstellen"));
    headerlabel = new QLabel(
                QStringLiteral("Geben Sie unten links die Namen der neuen Tags "
                               "(oder Taggruppen) durch Kommas getrennt ein "
                               "und unten rechts die Gruppe der Neuen Tags."));
    headerlabel->setWordWrap(true);
    categoryexpllabel = new  QLabel(QStringLiteral("Kategorie"));
    tagnamelabel = new QLabel(QStringLiteral("Namen"));
    grouplabel = new QLabel(QStringLiteral("Gruppe"));

    okbutton = new QPushButton("Erstellen", this);
    okbutton->setDefault(true);
    rejectbutton = new QPushButton(QStringLiteral("Abbrechen"), this);

    tagentry = new DStaticTagEdit(master, newcategory, this);
    tagentry->tagentry->setMinimumWidth(0x1C0);
    groupentry = new DSuggestionEdit(master,
                                     AE::DEntityType::TAG,
                                     AE::DEnterType::NONE,
                                     AE::DAllowed::GROUP,
                                     this, newcategory);


    entrybox = new QGridLayout();
    entrybox->setHorizontalSpacing(32);
    entrybox->addWidget(categoryexpllabel, 0, 0);
    entrybox->addWidget(tagnamelabel, 0, 1);
    entrybox->addWidget(grouplabel, 0, 2);
    tagentry->addToGrid(entrybox, 1, 0);
    entrybox->addWidget(groupentry, 1, 2);

    buttonbox = new QHBoxLayout();
    buttonbox->addWidget(rejectbutton);
    buttonbox->addWidget(okbutton);

    mainbox = new QVBoxLayout();
    mainbox->addWidget(headerlabel);
    mainbox->addLayout(entrybox);
    mainbox->addLayout(buttonbox);

    tagentry->tagentry->setFocus();


    setLayout(mainbox);


    connect(okbutton, &QPushButton::clicked, this, &DCreateTagsDialog::interpret);
    connect(rejectbutton, &QPushButton::clicked, this, &DCreateTagsDialog::reject);
}

DCreateTagsDialog::~DCreateTagsDialog()
{
}


QString DCreateTagsDialog::getTagList() const
{
    return tagentry->text().trimmed();
}

QString DCreateTagsDialog::getGroup() const
{
    return groupentry->text().trimmed();
}

void DCreateTagsDialog::interpret()
{
    QString newtags(tagentry->text());
    QString existing_tag = master->tag_tree_master()->one_tag_exists(tagentry->getCategory(), newtags);
    if (!existing_tag.isEmpty())
    {
        QMessageBox::critical(this, QString::fromUcs4(U"Ung\xFC" "ltige Eingabe"),
                              QStringLiteral("Der Tag ")
                              % existing_tag % QStringLiteral(" existiert bereits"));
        return;
    }

    accept();
}


}
