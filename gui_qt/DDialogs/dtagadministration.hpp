#ifndef DTAGADMINISTRATION_HPP
#define DTAGADMINISTRATION_HPP


#include <QDialog>

class QPushButton;
class QLabel;
class QComboBox;
class QCheckBox;

class QHeaderView;
class QTreeView;

class QHBoxLayout;
class QVBoxLayout;


#include <zdef.hpp>
class AWorker;

class TagTreeMaster;
class TagTreeQtGuard;
//class DTagTreeView;
//class DDreamListModel;
//class DDreamListView;

#include <gui_qt/DModel/tagtreemodel.hpp>
#include <gui_qt/DModel/dtagtreeview.hpp>
#include <gui_qt/DModel/ddreamlistview.hpp>
#include <gui_qt/DDialogs/ddreamswithtagdialog.hpp>

namespace DDialogs
{
/**
 * @brief The DTagAdministration class lets the user administrate tags.
 *
 * New tags can be created (managed by the function createNewTags),
 * tags can be moved to other groups with a drag and drop mechanism, and their name can be changed
 * in all dreams.
 */
class DTagAdministration : public QDialog
{
    Q_OBJECT

    /**
     * @brief The default button of the DTagAdministration
     */
    QPushButton *okbutton;
    QPushButton *rejectbutton;

    QLabel *categorylabel;
    QComboBox *categorycombobox;
    QPushButton *createnewtagbutton, *deletecategorybutton;
    QCheckBox *editablecheckbox;

    Master *master;
    TagTreeMaster *tagtree_master;
    QHeaderView *header;
    DTagTreeView *view;

    QHBoxLayout *createnewtagbox, *categorybox, *editablecheckboxbox, *buttonbox;
    QVBoxLayout *mainbox;

public:
    DTagAdministration(Master *new_master, const QString &newcategory, QWidget *parent = nullptr);
    ~DTagAdministration();

    /**
     * @brief Returns the category that is currently selected.
     */
    QString currentCategory() const;

private slots:
    void setCategory(QString newcategory);
    void initializeModel(QString category, TagTreeQtGuard *guard);
    /**
     * @brief Opens a DDialogs::DCreateTagsDialog in which the user can specify one or
     * more new tags to create
     */
    void createNewTags();
    /**
     * @brief Opens a DDialogs::DChangeTagCategory dialog in which the user
     * can enter a new category for the tag tagname in the currently selected category.
     */
    void changeTagCategory(QString tagname);
    void reparentTag(QString tagname);
    /**
     * @brief Signals that the tag tagname shall be deleted.
     * @param tagname Name of the tag
     * @param withChildren Tells whether only the tag itself shall be deleted
     * or all of its childen too.
     * @attention Removing a tag with its children is currently not implemented,
     * calling this function with withchildren set to true will result in nothing be done.
     */
    void deleteTag(QString tagname);
    /**
     * @brief Opens a small dialog to edit the tag tagname.
     */
    void editTag(QString tagname);
    void updateDeletecategorybutton();
    /**
     * @brief Deletes the category that is currently selected by sending
     * demandDeleteCategory() to #worker.
     */
    void deleteCurrentCategory();
    void searchDreamsWithTag(QString const &tagname);
};


}

#endif // DTAGADMINISTRATION_HPP
