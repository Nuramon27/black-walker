#ifndef DDREAMINPUTDIALOG_HPP
#define DDREAMINPUTDIALOG_HPP


#include <QDialog>


class QVBoxLayout;


#include <zdef.hpp>
class Master;
class SettingsQt;

#include <ddreamform.hpp>


namespace DDialogs
{
/**
 * @brief The DDreamInputDialog class wraps a QDialog around the DDreamForm.
 *
 * It does nothing more than displaying the DDreamForm, stored in #form. If #form emits
 * its DDreamForm::closesignal(), the dialog is accepted.
 */
class DDreamInputDialog : public QDialog
{
    Q_OBJECT

    DDreamForm form;
    QVBoxLayout *mainbox;

    SettingsQt *settings_qt;

public:
    /**
     * @brief Contructs a new DDreamInputDialog, displaying the dream with id,
     * with parent parent. It gives newworker to #form as worker object.
     */
    DDreamInputDialog(Master *master, QDate date, quint32 idx, QWidget *parent = nullptr);
    virtual ~DDreamInputDialog();

protected:
    void resizeEvent(QResizeEvent *event);
};


}

#endif // DDREAMINPUTDIALOG_HPP
