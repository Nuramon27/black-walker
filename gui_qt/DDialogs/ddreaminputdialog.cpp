#include "ddreaminputdialog.hpp"


#include <QVBoxLayout>


#include <bdg/Bindings.h>


namespace DDialogs
{
DDreamInputDialog::DDreamInputDialog(Master *master, QDate date, quint32 idx, QWidget *parent) :
    QDialog(parent),
    form(master),
    settings_qt(master->settings_qt())
{
    setWindowTitle(QStringLiteral("Traum bearbeiten"));
    form.openDream(date, idx, DDreamForm::OpenType::READ);

    mainbox = new QVBoxLayout();
    mainbox->addWidget(&form);

    setLayout(mainbox);

    connect(&form, &DDreamForm::closesignal, this, &QDialog::accept);

    AE::setSizeWidget(settings_qt, this, QStringLiteral("DDreamInputDialog"));
}

DDreamInputDialog::~DDreamInputDialog()
{
}

void DDreamInputDialog::resizeEvent(QResizeEvent *event)
{
    Q_UNUSED(event)
    AE::storeSizeWidget(settings_qt, this, QStringLiteral("DDreamInputDialog"));
}


}
