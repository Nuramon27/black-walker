#ifndef DSETTINGSDIALOG_HPP
#define DSETTINGSDIALOG_HPP


#include <QDialog>


/**
 * @brief The DDialogs namespace contains all Dialogs that may be used by the application.
 */
namespace DDialogs
{
/**
 * @brief In the DSettingsDialog, the user can choose global settings for the application.
 */
class DSettingsDialog : public QDialog
{
    Q_OBJECT

public:
    /**
     * @brief Constructs the DSettingsDialog with parent parent.
     */
    explicit DSettingsDialog(QWidget *parent = nullptr);
    /**
     * Destructs the Settings Dialog
     * */
    ~DSettingsDialog();
};

}

#endif // DSETTINGSDIALOG_HPP
