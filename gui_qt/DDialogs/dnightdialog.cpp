#include "dnightdialog.hpp"

#include <QLabel>
#include <QPushButton>
#include <QTimeEdit>

#include <QHBoxLayout>
#include <QVBoxLayout>


#include <bdg/Bindings.h>

#include <QLabel>
#include <mspinboxslider.hpp>


#include <zdef.hpp>


SleepPhaseEdit::SleepPhaseEdit(QTime start_time, EntryMaster *entrymaster) {
    fallasleep_label = new QLabel(QStringLiteral("Eingeschlafen"));
    fallasleep_entry = new QTimeEdit(start_time);
    wakeup_label = new QLabel(QStringLiteral("Aufgewacht"));
    wakeup_entry = new QTimeEdit(start_time);
    ease_of_fallasleep_label = new QLabel(QStringLiteral("Einschlafleichtigkeit"));
    ease_of_fallasleep_slider = new MSpinBoxSlider(
        entrymaster->rangeUStart(QStringLiteral("Einschlafleichtigkeit")),
        entrymaster->rangeUEnd(QStringLiteral("Einschlafleichtigkeit"))
    );
    ease_of_fallasleep_slider->setMinimumWidth(0xE0);
}
SleepPhaseEdit::~SleepPhaseEdit() {
    delete fallasleep_label;
    delete fallasleep_entry;
    delete wakeup_label;
    delete wakeup_entry;
    delete ease_of_fallasleep_label;
    delete ease_of_fallasleep_slider;
}

void SleepPhaseEdit::add_to_grid(QGridLayout *grid, int index)
{
    grid->addWidget(fallasleep_label, index, 0);
    grid->addWidget(fallasleep_entry, index, 1);
    grid->addWidget(wakeup_label, index, 2);
    grid->addWidget(wakeup_entry, index, 3);
    grid->addWidget(ease_of_fallasleep_label, index, 4);
    grid->addWidget(ease_of_fallasleep_slider, index, 5);
}

void SleepPhaseEdit::remove_from_grid(QGridLayout *grid)
{
    grid->removeWidget(fallasleep_label);
    grid->removeWidget(fallasleep_entry);
    grid->removeWidget(wakeup_label);
    grid->removeWidget(wakeup_entry);
    grid->removeWidget(ease_of_fallasleep_label);
    grid->removeWidget(ease_of_fallasleep_slider);
}

DNightDialog::DNightDialog(NightQt &new_night_model, SettingsQt *new_settings_qt, EntryMaster *new_entrymaster, QWidget *parent) :
    QDialog(parent),
    remove_button(nullptr),
    sleep_phase_box(),
    night_model(new_night_model),
    settings_qt(new_settings_qt),
    entrymaster(new_entrymaster)
{
    setWindowTitle(QStringLiteral("Nacht bearbeiten"));
    ok_button = new QPushButton(QStringLiteral("Ok"));
    ok_button->setDefault(true);
    cancel_button = new QPushButton(QStringLiteral("Cancel"));

    add_button = new QPushButton(QString::fromUcs4(U"Abschnitt hinzuf\xfc" "gen"));

    button_box = new QHBoxLayout();
    button_box->addWidget(cancel_button);
    button_box->addWidget(ok_button);

    sleep_phase_box = new QGridLayout();

    mainbox = new QVBoxLayout();
    mainbox->addLayout(sleep_phase_box);
    mainbox->addWidget(add_button);
    mainbox->addLayout(button_box);

    setLayout(mainbox);

    push_sleep_phase();
    present_night();

    connect(ok_button, &QPushButton::clicked, this, &DNightDialog::save_night);
    connect(cancel_button, &QPushButton::clicked, this, &DNightDialog::reject);
    connect(add_button, &QPushButton::clicked, this, &DNightDialog::push_sleep_phase);
}

DNightDialog::~DNightDialog()
{
}

void DNightDialog::push_sleep_phase()
{
    if (sleep_phase_edit.size() > 1)
    {
        sleep_phase_box->removeWidget(remove_button);
        delete remove_button;
    }

    QTime last_time;
    if (sleep_phase_edit.size() > 0) {
        last_time = sleep_phase_edit.last()->wakeup_entry->time();
    } else {
        last_time = QTime();
    }
    auto new_sleep_phase = new SleepPhaseEdit(last_time, entrymaster);
    new_sleep_phase->add_to_grid(sleep_phase_box, sleep_phase_edit.size());
    setTabOrder(new_sleep_phase->fallasleep_entry, new_sleep_phase->wakeup_entry);
    if (!sleep_phase_edit.isEmpty())
        setTabOrder(sleep_phase_edit.last()->wakeup_entry, new_sleep_phase->fallasleep_entry);
    setTabOrder(new_sleep_phase->wakeup_entry, add_button);
    new_sleep_phase->fallasleep_entry->setFocus();
    sleep_phase_edit.push_back(new_sleep_phase);

    if (sleep_phase_edit.size() > 1)
    {
        remove_button = new QPushButton(QStringLiteral("-"));
        sleep_phase_box->addWidget(remove_button, sleep_phase_edit.size()-1, 6);
        connect(remove_button, &QPushButton::clicked, this, &DNightDialog::pop_sleep_phase);
    }

}

void DNightDialog::pop_sleep_phase()
{
    if (sleep_phase_edit.size() <= 1)
        return;

    if (sleep_phase_edit.size() > 1)
    {
        sleep_phase_box->removeWidget(remove_button);
        delete remove_button;
    }

    if (sleep_phase_edit.size() <= 1) {
        return;
    }

    auto old_sleep_phase = sleep_phase_edit.takeLast();

    old_sleep_phase->remove_from_grid(sleep_phase_box);
    delete old_sleep_phase;
    setTabOrder(sleep_phase_edit.last()->wakeup_entry, add_button);

    if (sleep_phase_edit.size() > 1)
    {
        remove_button = new QPushButton(QStringLiteral("-"));
        sleep_phase_box->addWidget(remove_button, sleep_phase_edit.size()-1, 6);
        connect(remove_button, &QPushButton::clicked, this, &DNightDialog::pop_sleep_phase);
    }
}


void DNightDialog::save_night()
{
    for (quint32 i = 0; i < sleep_phase_edit.size(); ++i)
    {
        QTime fallasleep = sleep_phase_edit.at(i)->fallasleep_entry->time();
        QTime wakeup = sleep_phase_edit.at(i)->wakeup_entry->time();
        night_model.setFallAsleep(i, fallasleep.hour(), fallasleep.minute());
        night_model.setWakeUp(i, wakeup.hour(), wakeup.minute());
        auto ease_of_fallasleep = sleep_phase_edit.at(i)->ease_of_fallasleep_slider->getValue();
        if (!(night_model.easeOfFallAsleep(i) == 0 && ease_of_fallasleep == 0))
        {
            night_model.setEaseOfFallAsleep(i, ease_of_fallasleep);
        }
    }
    night_model.saveNight();

    accept();
}

void DNightDialog::present_night()
{
    quint32 i = 0;
    for (; i < night_model.nrSections(); ++i)
    {
        while (i >= sleep_phase_edit.size())
        {
            push_sleep_phase();
        }
        QTime time_fallasleep = QTime::fromString(night_model.fallAsleep(i), Qt::ISODate);
        if (time_fallasleep.isValid())
            sleep_phase_edit.at(i)->fallasleep_entry->setTime(time_fallasleep);

        QTime time_wakeup = QTime::fromString(night_model.wakeUp(i), Qt::ISODate);
        if (time_wakeup.isValid())
            sleep_phase_edit.at(i)->wakeup_entry->setTime(time_wakeup);

        sleep_phase_edit.at(i)->ease_of_fallasleep_slider->setValue(night_model.easeOfFallAsleep(i));
    }
    while (i < sleep_phase_edit.size() && i >= 1)
    {
        pop_sleep_phase();
    }
}
