#include "ddreamswithtagdialog.hpp"


#include <QLabel>
#include <QPushButton>
#include <QHeaderView>

#include <QHBoxLayout>
#include <QVBoxLayout>


#include <bdg/Bindings.h>

#include <DModel/ddreamlistview.hpp>
#include <DDialogs/ddreaminputdialog.hpp>


namespace DDialogs
{
DDreamsWithTagDialog::DDreamsWithTagDialog(Master *new_master, QString category, QString tagname, QWidget *parent) :
    QDialog(parent),
    master(new_master)
{
    setWindowTitle(QString::fromUcs4(U"Tr\x00E4") + "ume mit Tag " + tagname + " in " + category);
    //toplabel = new QLabel("Tag " + tagname + " in " + category);

    okbutton = new QPushButton("OK");

    //Create the model and view for presenting the dreams that contain a tag.
    dreamlist_qt = new DreamListQt();
    initialize_dreamlist_master(dreamlist_qt->m_d, master->m_d);
    dreamlistview = new QTreeView();
    dreamlistview->setModel(dreamlist_qt);
    dreamlistview->setDragEnabled(false);
    dreamlistview->setAcceptDrops(false);
    dreamlistheader = new QHeaderView(Qt::Horizontal, this);
    dreamlistview->setHeader(dreamlistheader);
    dreamlistview->setSizeAdjustPolicy(QAbstractScrollArea::AdjustToContents);
    dreamlistview->setSelectionMode(QAbstractItemView::SingleSelection);
    dreamlistview->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);


    buttonbox = new QHBoxLayout();
    buttonbox->addWidget(okbutton);

    mainbox = new QVBoxLayout();
    //mainbox->addWidget(toplabel);
    mainbox->addWidget(dreamlistview);
    mainbox->addLayout(buttonbox);

    setLayout(mainbox);


    connect(okbutton, SIGNAL(clicked(bool)),
            this, SLOT(accept()));
    connect(dreamlist_qt, &DreamListQt::wakeupChanged,
            this, [this](){
        this->dreamlist_qt->receiveDreamsWithTag();
    });
    connect(dreamlistview, SIGNAL(activated(QModelIndex)),
            this, SLOT(presentDream(QModelIndex)));

    dreamlist_qt->searchDreamsWithTag(category, tagname);

    auto settings_qt = master->settings_qt();
    AE::setSizeWidget(settings_qt, this, QStringLiteral("DDreamsWithTagDialog"));
    AE::setColumnSizes(settings_qt, dreamlistview, QStringLiteral("DDreamList"), 4 + settings_qt->nrStandardTagCategories());
}

DDreamsWithTagDialog::~DDreamsWithTagDialog()
{
    AE::storeColumnSizes(master->settings_qt(), dreamlistview, QStringLiteral("DDreamList"), 4 + master->settings_qt()->nrStandardTagCategories());
    delete dreamlist_qt;
}

void DDreamsWithTagDialog::resizeEvent(QResizeEvent *event)
{
    Q_UNUSED(event)
    AE::storeSizeWidget(master->settings_qt(), this, QStringLiteral("DDreamsWithTagDialog"));
}

void DDreamsWithTagDialog::presentDream(QModelIndex index)
{
    QDate date = QDate::fromString(dreamlist_qt->date_formal(index.row()), Qt::ISODate);
    if (date.isValid())
    {
        DDialogs::DDreamInputDialog dialog(master,
                                       date,
                                       dreamlist_qt->idx(index.row()), this);

        dialog.exec();
    }
}


}
