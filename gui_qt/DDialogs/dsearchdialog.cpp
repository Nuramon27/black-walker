#include "dsearchdialog.hpp"


#include <QSize>
#include <QString>
#include <QModelIndex>

#include <QLabel>
#include <QCheckBox>
#include <QPushButton>
#include <QLineEdit>

#include <QTreeView>
#include <QHeaderView>

#include <QGroupBox>

#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QGridLayout>

#include <QMessageBox>


#include <aglobalsettings.hpp>

#include <dsuggestionedit.hpp>
#include <DModel/ddreamlistview.hpp>
#include <dsearchform.hpp>
#include "ddreaminputdialog.hpp"
#include <Enum/dallowed.hpp>
#include <Enum/dentitytype.hpp>
#include <Enum/dentertype.hpp>


namespace DDialogs
{
DSearchDialog::DSearchDialog(Master *new_master, QWidget *parent) :
    QDialog(parent),
    search_qt(new_master->search_qt()),
    resultmodel(new_master->search_qt()->dreamList()),
  master(new_master)
{
    auto settings = master->settings_qt();
    setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    setWindowTitle(QStringLiteral("Suchen"));

    withgroupcheckbox = new QCheckBox(QStringLiteral("Kind-Tags mitsuchen"));
    withgroupcheckbox->setChecked(settings->searchwithgroups());
    weaktagscheckbox = new QCheckBox(QString::fromUcs4(U"Erw\xe4" "hnte Tags mitsuchen"));
    weaktagscheckbox->setChecked(settings->searchweaktags());
    rejectbutton = new QPushButton("Beenden");
    searchbutton = new QPushButton("Suche");
    searchbutton->setDefault(true);

    namelabel = new QLabel(QStringLiteral("Titel"));
    kindlabel = new QLabel(QStringLiteral("Art"));
    techniquelabel = new QLabel(QStringLiteral("Technik"));
    textlabel = new QLabel(QStringLiteral("Text"));
    nameentry = new QLineEdit();
    kindentry = new DSuggestionEdit(master,
                                    AE::DEntityType::ENTRY,
                                    AE::DEnterTypeFlags() | AE::DEnterType::COMMASEP,
                                    AE::DAllowed::ALL,
                                    nullptr, QStringLiteral("Art"));
    techniqueentry = new DSuggestionEdit(master,
                                         AE::DEntityType::ENTRY,
                                         AE::DEnterTypeFlags() | AE::DEnterType::COMMASEP,
                                         AE::DAllowed::ALL,
                                         nullptr, QStringLiteral("Technik"));
    textentry = new QLineEdit();

    resultview = new QTreeView();
    search_qt->dreamList()->clear();
    resultview->setModel(search_qt->dreamList());
    resultheader = new QHeaderView(Qt::Horizontal);
    resultview->setHeader(resultheader);
    resultview->setMinimumHeight(0x80);
    resultview->setMinimumWidth(0x400);
    resultview->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    resultview->setSizeAdjustPolicy(QAbstractScrollArea::AdjustToContents);
    resultview->setSelectionMode(QAbstractItemView::SingleSelection);
    AE::setSizeWidget(settings, this, QStringLiteral("DSearchDialog"));
    AE::setColumnSizes(settings, resultview, QStringLiteral("DDreamList"), 4 + settings->nrStandardTagCategories());

    QStringList standardTagCaterogries(AE::getStandardTagCategories(master->settings_qt()));
    standardTagCaterogries.prepend(QStringLiteral(""));
    generalForm = new DSearchForm(master, standardTagCaterogries, this, true, false);

    withgroupbox = new QHBoxLayout();
    withgroupbox->addStretch();
    withgroupbox->addWidget(weaktagscheckbox);
    withgroupbox->addWidget(withgroupcheckbox);

    entrybox = new QGridLayout();
    entrybox->addWidget(namelabel, 0, 0);
    entrybox->addWidget(kindlabel, 1, 0);
    entrybox->addWidget(techniquelabel, 2, 0);
    entrybox->addWidget(textlabel, 3, 0);
    entrybox->addWidget(nameentry, 0, 1);
    entrybox->addWidget(kindentry, 1, 1);
    entrybox->addWidget(techniqueentry, 2, 1);
    entrybox->addWidget(textentry, 3, 1);

    generalbox = new QVBoxLayout();
    generalbox->addWidget(generalForm);

    entrygroup = new QGroupBox();
    entrygroup->setLayout(entrybox);
    generalgroup = new QGroupBox();
    generalgroup->setLayout(generalbox);

    entrymetabox = new QVBoxLayout();
    entrymetabox->addWidget(entrygroup);
    entrymetabox->addStretch();

    searchbox = new QHBoxLayout();
    searchbox->addLayout(entrymetabox);
    searchbox->addWidget(generalgroup);

    buttonbox = new QHBoxLayout();
    buttonbox->addWidget(rejectbutton);
    buttonbox->addWidget(searchbutton);

    mainbox = new QVBoxLayout();
    mainbox->addLayout(withgroupbox);
    mainbox->addLayout(searchbox, 0);
    mainbox->addLayout(buttonbox, 0);
    mainbox->addWidget(resultview, 0xA0);

    QObject::connect(rejectbutton, SIGNAL(clicked(bool)), this, SLOT(reject()));
    QObject::connect(searchbutton, SIGNAL(clicked(bool)), this, SLOT(searchDream()));
    QObject::connect(search_qt, &SearchQt::wakeupChanged,
                     search_qt, &SearchQt::awaitResults);
    QObject::connect(resultview, SIGNAL(doubleClicked(QModelIndex)), this, SLOT(presentDream(QModelIndex)));

    setLayout(mainbox);
}

DSearchDialog::~DSearchDialog()
{
    auto settings = master->settings_qt();
    AE::storeSizeWidget(settings, this, QStringLiteral("DSearchDialog"));
    AE::storeColumnSizes(settings, resultview, QStringLiteral("DDreamList"), 4 + settings->nrStandardTagCategories());
    settings->setSearchwithgroups(withgroupcheckbox->isChecked());
    settings->setSearchweaktags(weaktagscheckbox->isChecked());
}
QSize DSearchDialog::sizeHint() const
{
    //return QSize(QDialog::sizeHint().width(), 0x180);
    return QDialog::sizeHint();
}

QString DSearchDialog::getCondition() const
{
    /* We have set the condition together from the single entries.
     * Between each two atomic conditions there should be a &,
     * furthermore each atomic condition should be enclosed in brackets.
     *
     * So what we do is: Place Brackets before and after each nonempty
     * atomic condition, and an & before each atomic condition.
     * Only in front of the first one there should be none, so as long
     * as condition is empty, we do not insert & chars
     * */
    QString condition;
    if (!nameentry->text().isEmpty())
    {
        QString res(nameentry->text());
        QString leftside(QStringLiteral("Name"));
        res = res.split(AGlobalSettings::commaregexp,
                        QString::SkipEmptyParts).join("\" | #"
                                                      + leftside + " > c\"");
        res = res.split(AGlobalSettings::andregexp,
                        QString::SkipEmptyParts).join("\" & #"
                                                      + leftside + " > c\"");
        res = res.trimmed();
        res = "#" + leftside + " > c\"" + res + "\"";

        if (!condition.isEmpty())
            condition += QStringLiteral(" & ");

        condition += QStringLiteral("(") + res + QStringLiteral(")");
    }
    if (!kindentry->text().isEmpty())
    {
        QString res(kindentry->text());
        QString leftside(QStringLiteral("Art"));
        res = res.split(AGlobalSettings::commaregexp,
                        QString::SkipEmptyParts).join("\" | #"
                                                      + leftside + " > c\"");
        res = res.split(AGlobalSettings::andregexp,
                        QString::SkipEmptyParts).join("\" & #"
                                                      + leftside + " > c\"");
        res = res.trimmed();
        res = "#" + leftside + " > c\"" + res + "\"";

        if (!condition.isEmpty())
            condition += QStringLiteral(" & ");

        condition += QStringLiteral("(") + res + QStringLiteral(")");
    }
    if (!techniqueentry->text().isEmpty())
    {
        QString res(techniqueentry->text());
        QString leftside(QStringLiteral("Technik"));
        res = res.split(AGlobalSettings::commaregexp,
                        QString::SkipEmptyParts).join("\" | "
                                                      + leftside + "# == c\"");
        res = res.split(AGlobalSettings::andregexp,
                        QString::SkipEmptyParts).join("\" & "
                                                      + leftside + "# == c\"");
        res = res.trimmed();
        res = leftside + "# == c\"" + res + "\"";

        if (!condition.isEmpty())
            condition += QStringLiteral(" & ");

        condition += QStringLiteral("(") + res + QStringLiteral(")");
    }
    if (!textentry->text().isEmpty())
    {
        QString res(textentry->text());
        QString leftside(QStringLiteral("Text"));
        res = res.split(AGlobalSettings::commaregexp,
                        QString::SkipEmptyParts).join("\" | #"
                                                      + leftside + " > c\"");
        res = res.split(AGlobalSettings::andregexp,
                        QString::SkipEmptyParts).join("\" & #"
                                                      + leftside + " > c\"");
        res = res.trimmed();
        res = "#" + leftside + " > c\"" + res + "\"";

        if (!condition.isEmpty())
            condition += QStringLiteral(" & ");

        condition += QStringLiteral("(") + res + QStringLiteral(")");
    }
    if (!condition.isEmpty())
        condition = QChar('(') + condition + QChar(')');

    /* After having been gone through the entry conditions,
     * we retrieve the condition of the generalForm and insert it
     * if it is not empty
     * */
    QString generalCondition(generalForm->getCondition(SearchSettings{
       withgroupcheckbox->isChecked(),
       weaktagscheckbox->isChecked()
    }));
    if (!generalCondition.isEmpty())
    {
        if (!condition.isEmpty())
            condition += QStringLiteral(" & ");
        condition += generalCondition;
    }

    return condition;
}


///SLOTS
void DSearchDialog::searchDream()
{
    search_qt->search(getCondition());
}

void DSearchDialog::presentDream(const QModelIndex &index)
{
    QDate date = QDate::fromString(resultmodel->date_formal(index.row()), Qt::ISODate);
    if (date.isValid())
    {
        DDreamInputDialog dialog(master, date, resultmodel->idx(index.row()), this);

        dialog.exec();
    }
}


}
