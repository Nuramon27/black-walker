#ifndef DCREATETAGSDIALOG_HPP
#define DCREATETAGSDIALOG_HPP


#include <QDialog>

#include <QString>

class QLabel;
class QPushButton;

class QHBoxLayout;
class QVBoxLayout;
class QGridLayout;


#include <zdef.hpp>

class DSuggestionEdit;
class DStaticTagEdit;

class Master;



namespace DDialogs
{
/**
 * @brief The DCreateTagsDialog class lets the user create new tags and give a group for them.
 */
class DCreateTagsDialog : public QDialog
{
    Q_OBJECT

    QLabel *headerlabel, *categoryexpllabel, *tagnamelabel, *grouplabel;
    /**
     * @brief The default button of the QDialog
     */
    QPushButton *okbutton;
    QPushButton *rejectbutton;

    /**
     * @brief The static Tag edit where the user can enter the names of the new tags
     * as a comma separated list
     */
    DStaticTagEdit *tagentry;
    /**
     * @brief The line edit where the user can enter the group of the new tag
     */
    DSuggestionEdit *groupentry;

    QGridLayout *entrybox;
    QHBoxLayout *buttonbox;
    QVBoxLayout *mainbox;

    Master *master;

public:
    /**
     * @brief Constructs a new DCreateTagsDialog with #worker object newworker,
     * parent parent, where a tag in newcategory can be created.
     */
    DCreateTagsDialog(Master *newmaster, QString const &newcategory, QWidget *parent = nullptr);
    /**
     * @brief Destructs the DCreateTagsDialog
     * */
    ~DCreateTagsDialog();

    /**
     * @brief Returns the content of the #tagentry.
     */
    QString getTagList() const;
    /**
     * @brief Returns the group of the created tags
     */
    QString getGroup() const;

private slots:
    /**
     * @brief Checks whether one of the entered tags alreadey exists.
     * If not, the dialog is accepted. If a tag already exists, a message box appears
     * and the dialog remains open.
     */
    void interpret();
};


}

#endif // DCREATETAGSDIALOG_HPP
