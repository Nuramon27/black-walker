#include "dchangetagcategorydialog.hpp"


#include <QLabel>
#include <QLineEdit>
#include <QPushButton>

#include <QMessageBox>

#include <QVBoxLayout>
#include <QHBoxLayout>


namespace DDialogs
{
DChangeTagCategoryDialog::DChangeTagCategoryDialog(Master *newmaster, QWidget *parent) :
    QDialog(parent),
    _create_category(false),
    master(newmaster)
{
    setWindowTitle(QStringLiteral("Kategorie \x00C4" "ndern"));

    okbutton = new QPushButton(QStringLiteral("OK"));
    okbutton->setDefault(true);
    cancelbutton = new QPushButton(QStringLiteral("Abbrechen"));

    newcategorylabel = new QLabel(QStringLiteral("Neue Kategorie: "));
    newcategoryentry = new DSuggestionEdit(master, AE::DEntityType::TAGCATEGORY,
                                           AE::DEnterType::NONE, AE::DAllowed::NONE);

    buttonbox = new QHBoxLayout();
    buttonbox->addWidget(cancelbutton);
    buttonbox->addWidget(okbutton);

    categorybox = new QHBoxLayout();
    categorybox->addWidget(newcategorylabel);
    categorybox->addWidget(newcategoryentry);

    mainbox = new QVBoxLayout();
    mainbox->addLayout(categorybox);
    mainbox->addLayout(buttonbox);

    setLayout(mainbox);

    connect(okbutton, &QPushButton::clicked, this, &DChangeTagCategoryDialog::complete);
    connect(cancelbutton, &QPushButton::clicked, this, &QDialog::reject);
}

DChangeTagCategoryDialog::~DChangeTagCategoryDialog()
{
}


//MEMBER FUNCTIONS
bool DChangeTagCategoryDialog::create_category() const
{
    return _create_category;
}

QString DChangeTagCategoryDialog::getNewCategory() const
{
    return newcategoryentry->text();
}


//PRIVATE SLOTS
void DChangeTagCategoryDialog::complete()
{
    if (master->tag_tree_master()->tagCategoryExists(newcategoryentry->text()))
        accept();
    else
    {
        if (QMessageBox::question(this, "Neue Kategorie erstellen",
                              "Die Kategorie " + newcategoryentry->text()
                              + " existiert nicht. Wollen sie sie erstellen?",
                              QMessageBox::Yes,
                              QMessageBox::No | QMessageBox::Default | QMessageBox::Escape)
            == QMessageBox::Yes)
        {
            _create_category = true;
            accept();
        }
    }
}


}
