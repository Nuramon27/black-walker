#include "dtagadministration.hpp"


#include <QPushButton>
#include <QLabel>
#include <QComboBox>
#include <QCheckBox>
#include <QRadioButton>
#include <QTextEdit>

#include <QHeaderView>
#include <QTreeView>

#include <QHBoxLayout>
#include <QVBoxLayout>

#include <QMessageBox>


#include <bdg/Bindings.h>

#include "dcreatetagsdialog.hpp"
#include "dchangetagcategorydialog.hpp"
#include <gui_qt/DModel/tagtreemodel.hpp>
#include <gui_qt/DModel/dtagtreeview.hpp>
#include <gui_qt/DModel/ddreamlistview.hpp>
//#include <gui_qt/DDialogs/ddreamswithtagdialog.hpp>


namespace DDialogs
{
DTagAdministration::DTagAdministration(Master *new_master, QString const &newcategory, QWidget *parent) :
    QDialog(parent),
    deletecategorybutton(nullptr),
    master(new_master),
    tagtree_master(new_master->tag_tree_master())
{
    setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    setWindowTitle(QStringLiteral("Tags verwalten"));

    okbutton = new QPushButton("OK", this);
    okbutton->setDefault(true);
    rejectbutton = new QPushButton(QStringLiteral("Zur\x00fc" "ck"), this);

    categorylabel = new QLabel("Kategorie", this);
    categorycombobox = new QComboBox(this);
    categorycombobox->setModel(tagtree_master);
    categorycombobox->setCurrentText(newcategory);

    createnewtagbutton = new QPushButton(QStringLiteral("Neue Tags erstellen"), this);
    editablecheckbox = new QCheckBox(QStringLiteral("Tags Editieren"), this);
    editablecheckbox->setChecked(false);

    TagTreeQtGuard *guard = tagtree_model_of_category(tagtree_master, newcategory);
    initializeModel(newcategory, guard);
    view = new DTagTreeView(guard, this);
    view->setSelectionMode(QAbstractItemView::SingleSelection);
    view->setDragEnabled(true);
    view->setAcceptDrops(true);
    view->viewport()->setAcceptDrops(true);
    view->setDropIndicatorShown(true);
    view->setDragDropMode(QAbstractItemView::InternalMove);
    header = new QHeaderView(Qt::Horizontal, this);
    view->setHeader(header);
    view->resizeColumnToContents(0);
    view->setSizeAdjustPolicy(QAbstractScrollArea::AdjustToContents);
    view->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Expanding);
    view->setSortingEnabled(true);
    view->sortByColumn(0, Qt::AscendingOrder);


    createnewtagbox = new QHBoxLayout();
    createnewtagbox->addWidget(createnewtagbutton);
    createnewtagbox->addStretch();
    updateDeletecategorybutton();

    editablecheckboxbox = new QHBoxLayout();
    editablecheckboxbox->addWidget(editablecheckbox);
    editablecheckboxbox->addStretch();

    categorybox = new QHBoxLayout();
    categorybox->addWidget(categorylabel);
    categorybox->addWidget(categorycombobox);

    buttonbox = new QHBoxLayout();
    buttonbox->addWidget(rejectbutton);
    buttonbox->addWidget(okbutton);

    mainbox = new QVBoxLayout();
    mainbox->addLayout(categorybox);
    mainbox->addLayout(createnewtagbox);
    mainbox->addWidget(view);
    mainbox->addLayout(editablecheckboxbox);
    mainbox->addLayout(buttonbox);


    setLayout(mainbox);
    AE::setSizeWidget(master->settings_qt(), this, QStringLiteral("DTagAdministration"));
    AE::setColumnSizes(master->settings_qt(), view, QStringLiteral("DTagAdministration"), 3);

    connect(okbutton, &QPushButton::clicked, this, &DTagAdministration::accept);
    connect(rejectbutton, &QPushButton::clicked, this, &DTagAdministration::reject);
    connect(categorycombobox, &QComboBox::currentTextChanged, this, &DTagAdministration::setCategory);
    //connect(editablecheckbox, &QCheckBox::clicked, ?);
    //connect(view, &DTagTreeView::demandSearchDreamsWithTag, this, &DTagAdministration::searchDreamsWithTag);
    connect(createnewtagbutton, &QPushButton::clicked, this, &DTagAdministration::createNewTags);
    connect(view, &DTagTreeView::demandSearchDreamsWithTag, this, &DTagAdministration::searchDreamsWithTag);
    connect(view, &DTagTreeView::demandReparentTag, this, &DTagAdministration::reparentTag);
    connect(view, &DTagTreeView::demandChangeCategory, this, &DTagAdministration::changeTagCategory);
    connect(view, &DTagTreeView::demandDeleteTag, this, &DTagAdministration::deleteTag);
    connect(view, &DTagTreeView::demandEditTag, this, &DTagAdministration::editTag);
    /*connect(okbutton, SIGNAL(clicked(bool)), this, SLOT(accept()));
    connect(rejectbutton, SIGNAL(clicked(bool)), this, SLOT(reject()));
    connect(categorycombobox, SIGNAL(currentTextChanged(QString)),
            model, SLOT(setCategory(QString)));
    connect(categorycombobox, SIGNAL(currentTextChanged(QString)),
            this, SLOT(updateDeletecategorybutton()));
    connect(editablecheckbox, SIGNAL(clicked(bool)), model, SLOT(setEditable(bool)));
    connect(createnewtagbutton, SIGNAL(clicked(bool)), this, SLOT(createNewTags()));
    connect(view, SIGNAL(demandSearchDreamsWithTag(QString)),
            this, SLOT(searchDreamsWithTag(QString)));
    connect(view, SIGNAL(demandChangeCategory(QString)), this, SLOT(changeTagCategory(QString)));
    connect(view, SIGNAL(demandDeleteTag(QString)), this, SLOT(deleteTag(QString)));
    connect(this, SIGNAL(demandChangeTagCategoryInAllDreams(QString,QString,QString)),
            &worker, SLOT(changeTagCategoryInAllDreams(QString,QString,QString)));
    connect(this, SIGNAL(demandDeleteTagInAllDreams(QString,QString,bool)),
            &worker, SLOT(deleteTagInAllDreams(QString,QString,bool)));
    connect(this, SIGNAL(demandDeleteCategory(QString)),
            &worker, SLOT(deleteCategory(QString)));
    connect(&worker, SIGNAL(tagDeleted(QString,QString)),
            this, SLOT(updateDeletecategorybutton()));*/
}

DTagAdministration::~DTagAdministration()
{
    AE::storeSizeWidget(master->settings_qt(), this, QStringLiteral("DTagAdministration"));
    AE::storeColumnSizes(master->settings_qt(), view, QStringLiteral("DTagAdministration"), 3);
}

QString DTagAdministration::currentCategory() const
{
    return categorycombobox->currentText();
}

void DTagAdministration::setCategory(QString newcategory)
{
    {
        TagTreeQtGuard *oldguard = view->tagTreeGuard();
        TagTreeQt *model = tag_tree_qt_guard_acquire(oldguard);
        disconnect(model, nullptr, this, nullptr);
        disconnect(model, nullptr, master, nullptr);
        disconnect(editablecheckbox, nullptr, model, nullptr);
    }

    TagTreeQtGuard *guard = tagtree_model_of_category(tagtree_master, newcategory);
    view->setTagTreeModel(guard);

    initializeModel(newcategory, guard);

    updateDeletecategorybutton();
}

void DTagAdministration::initializeModel(QString category, TagTreeQtGuard *guard)
{
    TagTreeQt *model = tag_tree_qt_guard_acquire(guard);
    model->setCategory(category);
    connect(model, &TagTreeQt::modelReset, this, &DTagAdministration::updateDeletecategorybutton);
    connect(model, &TagTreeQt::rowsInserted, this, &DTagAdministration::updateDeletecategorybutton);
    connect(model, &TagTreeQt::rowsRemoved, this, &DTagAdministration::updateDeletecategorybutton);
    connect(model, &TagTreeQt::begin_cmdChanged, master, &Master::begin_cmd);
    connect(model, &TagTreeQt::end_cmdChanged, master, &Master::end_cmd);
    model->setEditable(editablecheckbox->isChecked());
    connect(editablecheckbox, &QCheckBox::toggled, model, &TagTreeQt::setEditable);
}

void DTagAdministration::createNewTags()
{
    DCreateTagsDialog dialog(master, currentCategory(), this);

    if (dialog.exec())
    {
        tagtree_master->createNewTags(currentCategory(), dialog.getGroup(), dialog.getTagList());
        if (deletecategorybutton != nullptr)
        {
            createnewtagbox->removeWidget(deletecategorybutton);
            delete deletecategorybutton;
            deletecategorybutton = nullptr;
        }
    }
}

void DTagAdministration::reparentTag(QString tagname)
{
    QDialog diag(this);
    diag.setWindowTitle(QStringLiteral("In Gruppe verschieben"));

    auto *enterbox = new QHBoxLayout();
    enterbox->addWidget(new QLabel(QStringLiteral("Neue Gruppe")));
    DSuggestionEdit *group_edit = new DSuggestionEdit(
                master, AE::DEntityType::TAG,
                AE::DEnterType::NONE,
                AE::DAllowed::ALL,
                this,
                categorycombobox->currentText()
                );
    enterbox->addWidget(group_edit);

    auto buttonbox = new QHBoxLayout();
    auto cancelbutton = new QPushButton(QStringLiteral("Abbrechen"));
    auto okbutton = new QPushButton(QStringLiteral("Ok"));
    buttonbox->addWidget(cancelbutton);
    buttonbox->addWidget(okbutton);

    QVBoxLayout *mainbox = new QVBoxLayout();
    mainbox->addLayout(enterbox);
    mainbox->addLayout(buttonbox);

    diag.setLayout(mainbox);

    connect(cancelbutton, &QPushButton::clicked, &diag, &QDialog::reject);
    connect(okbutton, &QPushButton::clicked, &diag, &QDialog::accept);
    okbutton->setDefault(true);


    if (diag.exec())
    {
        if (!tagtree_master->reparentTag(categorycombobox->currentText(), tagname, group_edit->text().trimmed()))
        {
            QMessageBox::critical(this, QString::fromUcs4(U"Ung\xFC" "ltige Eingabe"),
                                  QString::fromUcs4(U"Der eingegebene Tag existiert nicht"));
        }
    }
}

void DTagAdministration::changeTagCategory(QString tagname)
{
    DDialogs::DChangeTagCategoryDialog diag(master, this);

    if (diag.exec())
    {
        if (diag.create_category())
        {
            tagtree_master->createCategory(diag.getNewCategory());
        }
        tagtree_master->changeTagCategory(currentCategory(), diag.getNewCategory(), tagname);
    }
}

void DTagAdministration::deleteTag(QString tagname)
{
    QDialog question(this);
    question.setWindowTitle(QString::fromUcs4(U"Tag l\x00F6" "schen?"));
    QLabel *text = new QLabel(QString::fromUcs4(U"Wollen sie den Tag \x201E") + tagname
                              + QString::fromUcs4(U"\x201C wirklich l\x00F6" "schen?"));
    QRadioButton *choice1 = new QRadioButton(QStringLiteral("Mit allen Kindtags (noch nicht implementiert)")),
            *choice2 = new QRadioButton(QStringLiteral("Nur einzelnen Tag"));
    choice2->setChecked(true);
    QPushButton *okbutton, *cancelbutton;
    okbutton = new QPushButton(QStringLiteral("OK"));
    cancelbutton = new QPushButton(QStringLiteral("Abbrechen"));
    QVBoxLayout *mainbox = new QVBoxLayout();
    QHBoxLayout *buttonbox = new QHBoxLayout();

    buttonbox->addWidget(cancelbutton);
    buttonbox->addWidget(okbutton);

    mainbox->addWidget(text);
    mainbox->addWidget(choice1);
    mainbox->addWidget(choice2);
    mainbox->addLayout(buttonbox);

    question.setLayout(mainbox);

    cancelbutton->setDefault(true);

    QObject::connect(cancelbutton, SIGNAL(clicked()), &question, SLOT(reject()));
    QObject::connect(okbutton, SIGNAL(clicked()), &question, SLOT(accept()));

    if (question.exec())
    {
        bool withchildren = choice1->isChecked();
        tagtree_master->deleteTag(currentCategory(), tagname, withchildren);
    }
}

void DTagAdministration::editTag(QString tagname)
{
    QDialog dialog(this);

    QLabel *desc_label = new QLabel(QStringLiteral("Beschreibung"));
    QTextEdit *desc_edit = new QTextEdit();
    desc_edit->setPlainText(tagtree_master->tagDescription(currentCategory(), tagname));

    QPushButton *okbutton = new QPushButton(QStringLiteral("Ok"));
    okbutton->setDefault(true);
    QPushButton *cancelbutton = new QPushButton(QStringLiteral("Abbrechen"));

    QHBoxLayout *editbox = new QHBoxLayout();
    editbox->addWidget(desc_label);
    editbox->addWidget(desc_edit);

    QHBoxLayout *buttonbox = new QHBoxLayout();
    buttonbox->addWidget(cancelbutton);
    buttonbox->addWidget(okbutton);

    QVBoxLayout *mainbox = new QVBoxLayout();
    mainbox->addLayout(editbox);
    mainbox->addLayout(buttonbox);

    dialog.setLayout(mainbox);

    connect(okbutton, &QPushButton::clicked, &dialog, &QDialog::accept);
    connect(cancelbutton, &QPushButton::clicked, &dialog, &QDialog::reject);

    if (dialog.exec())
    {
        tagtree_master->setTagDescription(currentCategory(), tagname, desc_edit->toPlainText());
    }
}

void DTagAdministration::updateDeletecategorybutton()
{
    if (tagtree_master->tag_count(currentCategory()) <= 1)
    {
        if (deletecategorybutton == nullptr)
        {
            deletecategorybutton = new QPushButton(QString::fromUcs4(U"Kategorie l\x00F6" "schen"));
            createnewtagbox->addWidget(deletecategorybutton);
            connect(deletecategorybutton, &QPushButton::clicked,
                    this, &DTagAdministration::deleteCurrentCategory);
        }
    }
    else if (deletecategorybutton != nullptr)
    {
        createnewtagbox->removeWidget(deletecategorybutton);
        delete deletecategorybutton;
        deletecategorybutton = nullptr;
    }
}

void DTagAdministration::deleteCurrentCategory()
{
    QString categorytodelete = currentCategory();
    tagtree_master->deleteCategory(categorytodelete);
}

void DTagAdministration::searchDreamsWithTag(QString const &tagname)
{
    QString category = currentCategory();
    DDreamsWithTagDialog dialog(master, category, tagname, this);
    dialog.exec();
}

}

