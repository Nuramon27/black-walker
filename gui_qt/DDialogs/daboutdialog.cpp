#include "daboutdialog.hpp"


#include <QtGlobal>
#include <QCoreApplication>

#include <QLabel>
#include <QPushButton>

#include <QHBoxLayout>
#include <QVBoxLayout>

#include <QFile>
#include <QTextStream>

#include <QMessageBox>


namespace DDialogs
{
DAboutDialog::DAboutDialog(QWidget *parent) :
    QDialog(parent)
{
    setWindowTitle(QStringLiteral("\x00DC" "ber"));

    aboutlabel = new QLabel(QString(
                                "BlackWalker (Version " + QCoreApplication::applicationVersion()
                                + ") Copyright 2017 Manuel Simon\n\n"
                                "This program is based on Qt Version " + QString(QT_VERSION_STR) +"\n"
                                "provided under the GNU Lesser General Public License Version 3.0\n"
                                "For Details click 'About Qt'\n"
                                "This program comes with ABSOLUTELY NO WARRANTY; for details click 'Warranty'.\n"
                                "This is free software, and you are welcome to redistribute it\n"
                                "under certain conditions; click 'Copyright' for details."));
    okbutton = new QPushButton(QStringLiteral("OK"));
    okbutton->setDefault(true);
    copyrightbutton = new QPushButton(QStringLiteral("Copyright"));
    warrantybutton = new QPushButton(QStringLiteral("Warranty"));
    aboutqtbutton = new QPushButton(QStringLiteral("About Qt"));

    buttonbox = new QHBoxLayout();
    buttonbox->addWidget(copyrightbutton);
    buttonbox->addWidget(warrantybutton);
    buttonbox->addWidget(aboutqtbutton);
    buttonbox->addStretch();
    buttonbox->addWidget(okbutton);

    mainbox = new QVBoxLayout();
    mainbox->addWidget(aboutlabel);
    mainbox->addLayout(buttonbox);

    setLayout(mainbox);

    connect(okbutton, SIGNAL(clicked(bool)), this, SLOT(accept()));
    connect(copyrightbutton, SIGNAL(clicked(bool)), this, SLOT(showcopyright()));
    connect(warrantybutton, SIGNAL(clicked(bool)), this, SLOT(showwarranty()));
    connect(aboutqtbutton, SIGNAL(clicked(bool)), this, SLOT(showaboutqt()));
}


void DAboutDialog::showcopyright()
{
    QFile gplfile(QStringLiteral("gpl-3.0.txt"));

    if (!gplfile.open(QFile::ReadOnly | QFile::Text))
    {
        return;
    }

    QTextStream stream(&gplfile);
    QMessageBox::information(this, QStringLiteral("Copyright Notice"),
                             stream.readAll(),
                             QMessageBox::Ok, QMessageBox::Ok);

    gplfile.close();
}

void DAboutDialog::showwarranty()
{
    QMessageBox::
            information(
                this, QStringLiteral("Copyright Notice"),
                QString(
                    "15. Disclaimer of Warranty.\n\n"

                    "THERE IS NO WARRANTY FOR THE PROGRAM, TO THE EXTENT PERMITTED BY APPLICABLE LAW.\n"
                    "EXCEPT WHEN OTHERWISE STATED IN WRITING THE COPYRIGHT HOLDERS AND/OR OTHER PARTIES\n"
                    "PROVIDE THE PROGRAM “AS IS” WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED"
                    "OR IMPLIED,\n"
                    "INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS\n"
                    "FOR A PARTICULAR PURPOSE. THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE\n"
                    "PROGRAM IS WITH YOU. SHOULD THE PROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF\n"
                    "ALL NECESSARY SERVICING, REPAIR OR CORRECTION."),
                QMessageBox::Ok, QMessageBox::Ok);
}

void DAboutDialog::showaboutqt()
{
    QMessageBox::aboutQt(this, QStringLiteral("About Qt"));
}


}
