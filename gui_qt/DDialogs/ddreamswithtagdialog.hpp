#ifndef DDREAMSWITHTAGDIALOG_HPP
#define DDREAMSWITHTAGDIALOG_HPP


#include <QDialog>


#include <QModelIndex>

class QLabel;
class QPushButton;
class QHeaderView;
class QTreeView;

class QHBoxLayout;
class QVBoxLayout;


#include <zdef.hpp>
#include <bdg/Bindings.h>

extern "C" void initialize_dreamlist_master(DreamListQt::Private *dreamlist, Master::Private *master);


namespace DDialogs
{
class DDreamsWithTagDialog : public QDialog
{
    Q_OBJECT

    QLabel *toplabel;
    QPushButton *okbutton;

    QHeaderView *dreamlistheader;
    QTreeView *dreamlistview;

    QHBoxLayout *buttonbox;
    QVBoxLayout *mainbox;

    DreamListQt *dreamlist_qt;
    Master *master;
public:
    DDreamsWithTagDialog(Master *new_master, QString category, QString tagname, QWidget *parent = nullptr);
    virtual ~DDreamsWithTagDialog();

protected:
    void resizeEvent(QResizeEvent *event);


signals:
    /**
     * @brief Signals #worker to start a search for dreams meeting condition.
     */
    void searchDreamSignal(QString condition);

private slots:
    void presentDream(QModelIndex index);
};


}

#endif // DDREAMSWITHTAGDIALOG_HPP
