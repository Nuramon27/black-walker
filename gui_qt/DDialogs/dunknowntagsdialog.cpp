#include "dunknowntagsdialog.hpp"


#include <QStringBuilder>

#include <QSignalMapper>

#include <QLabel>
#include <QPushButton>
#include <QComboBox>

#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QGridLayout>

#include <QMessageBox>


#include <aglobalsettings.hpp>

#include <dsuggestionedit.hpp>
#include <Enum/dallowed.hpp>
#include <Enum/dentitytype.hpp>
#include <Enum/dentertype.hpp>

#include <bdg/Bindings.h>


namespace DDialogs
{
DUnknownTagsDialog::DUnknownTagsDialog(Master *newmaster, TagList *newmodel, QWidget *parent) :
    QDialog(parent),
    master(newmaster),
    model(newmodel)
{
    setWindowTitle(QStringLiteral("Tags Spezifizieren"));

    categoryheader = new QLabel("Kategorie", this);
    tagheader = new QLabel("Tag", this);
    groupheader = new QLabel("Gruppe", this);

    tagbox = new QGridLayout();
    tagbox->addWidget(categoryheader, 0, 0);
    tagbox->addWidget(tagheader, 0, 1);
    tagbox->addWidget(groupheader, 0, 2);
    tagmapper = new QSignalMapper(this);
    specifygroupmapper = new QSignalMapper(this);
    TagTreeMaster *tag_model = master->tag_tree_master();
    int i = 0;
    for (; i < model->rowCount(); ++i)
    {
        if (model->isGroup(i))
            break;
        taglabel += new QLabel(model->value(i), this);
        categorycombobox += new QComboBox(this);
        categorycombobox.last()->setModel(tag_model);
        categorycombobox.last()->setInsertPolicy(QComboBox::InsertAlphabetically);
        categorycombobox.last()->setEditable(true);
        categorycombobox.last()->setCurrentText(model->category(i));
        groupentry += new DSuggestionEdit(master,
                                          AE::DEntityType::TAG,
                                          AE::DEnterType::NONE,
                                          AE::DAllowed::GROUP,
                                          this,
                                          categorycombobox.last()->currentText());
        specifygroupbutton += new QPushButton("+", this);

        tagbox->addWidget(categorycombobox.last(), i+1, 0);
        tagbox->addWidget(taglabel.last(), i+1, 1);
        tagbox->addWidget(groupentry.last(), i+1, 2);
        tagbox->addWidget(specifygroupbutton.last(), i+1, 3);

        connect(categorycombobox.last(), SIGNAL(currentTextChanged(QString)),
                groupentry.last(), SLOT(setCategory(QString)));
        /* Create a mapper in order to be able to obtain an index in the grid
         * of tag entries from the groupentry which emits an editingFinished signal
         * */
        specifygroupmapper->setMapping(specifygroupbutton.last(), i);
        connect(specifygroupbutton.last(), SIGNAL(clicked(bool)), specifygroupmapper, SLOT(map()));
    }
    for (; i < model->rowCount(); ++i)
    {
        grouplabel += new QLabel(model->value(i), this);
        categorycombobox += new QComboBox(this);
        categorycombobox.last()->setModel(tag_model);
        categorycombobox.last()->setInsertPolicy(QComboBox::InsertAlphabetically);
        categorycombobox.last()->setEditable(true);
        categorycombobox.last()->setCurrentText(model->category(i));
        groupentry += new DSuggestionEdit(master,
                                          AE::DEntityType::TAG,
                                          AE::DEnterType::NONE,
                                          AE::DAllowed::GROUP,
                                          this,
                                          categorycombobox.last()->currentText());
        specifygroupbutton += new QPushButton("+", this);

        tagbox->addWidget(categorycombobox.last(), i+1, 0);
        tagbox->addWidget(grouplabel.last(), i+1, 1);
        tagbox->addWidget(groupentry.last(), i+1, 2);
        tagbox->addWidget(specifygroupbutton.last(), i+1, 3);

        connect(categorycombobox.last(), SIGNAL(currentTextChanged(QString)),
                groupentry.last(), SLOT(setCategory(QString)));
        specifygroupmapper->setMapping(specifygroupbutton.last(), i);
        QObject::connect(specifygroupbutton.last(), SIGNAL(clicked(bool)), specifygroupmapper, SLOT(map()));
    }

    okbutton = new QPushButton("OK", this);
    okbutton->setDefault(true);
    rejectbutton = new QPushButton(AGlobalSettings::backstring, this);


    buttonbox = new QHBoxLayout();
    buttonbox->addWidget(rejectbutton);
    buttonbox->addWidget(okbutton);

    mainbox = new QVBoxLayout();
    mainbox->addLayout(tagbox);
    mainbox->addLayout(buttonbox);

    setLayout(mainbox);
    AE::setSizeWidget(master->settings_qt(), this, QStringLiteral("DUnknownTagsDialog"));

    QObject::connect(tagmapper, QOverload<int>::of(&QSignalMapper::mapped), this, &DUnknownTagsDialog::adaptEntryForGroup);
    QObject::connect(specifygroupmapper, QOverload<int>::of(&QSignalMapper::mapped), this, &DUnknownTagsDialog::newEntryForGroup);
    QObject::connect(okbutton, &QPushButton::clicked, this, &DUnknownTagsDialog::createEnteredTags);
    QObject::connect(rejectbutton, &QPushButton::clicked, this, &DUnknownTagsDialog::reject);
}

DUnknownTagsDialog::~DUnknownTagsDialog()
{
}


///MEBER FUNCTIONS
QString DUnknownTagsDialog::textAtIndex(int index) const
{
    return (index < taglabel.size())? taglabel.at(index)->text().trimmed()
                                    : grouplabel.at(index - taglabel.size())->text().trimmed();
}

void DUnknownTagsDialog::resizeEvent(QResizeEvent *event)
{
    Q_UNUSED(event)
    AE::storeSizeWidget(master->settings_qt(), this, QStringLiteral("DUnknownTagsDialog"));
}

AE::QStringMulHash DUnknownTagsDialog::getNewTags() const
{
    AE::QStringMulHash res;

    for (int i = 0; i < taglabel.size(); ++i)
        res.insert(categorycombobox.at(i)->currentText(), taglabel.at(i)->text().trimmed());

    return res;
}


///SLOTS
void DUnknownTagsDialog::createEnteredTags()
{
    /* At first we need to check if the user has given a category for every tag.
     *
     * allcategoriesset will stay true as long as for every scanned tag a category has been set.
     * */
    bool allcategoriesset = true;
    for (int i = 0; i < categorycombobox.size() && allcategoriesset; ++i)
        allcategoriesset = !categorycombobox.at(i)->currentText().isEmpty();

    if (!allcategoriesset)
    {
        QMessageBox::critical(this, QString::fromUcs4(U"Ung\xFC" "ltige Eingabe"),
                              QString::fromUcs4(U"Bitte geben Sie f\xFC" "r jedes Tag eine Kategorie an."));
        return;
    }
    /* Before we create the tags with the groups the user has entered, we need to check that the input is valid.
     *
     * This means that the given groups do not contain cicles, that means that no tag is in a group,
     * that is a subgroup of its own.
     *
     * We do this by first creating the QHash tagtoindestable, which stores for every category used
     * in the dialog a QHash<QString, int>. The keys of this hash are tags (i.e. the texts of the tag and
     * group labels) and its values are the index of the label which contains this tag
     * (continuing at the group labels with the index taglabel.size(), i.e. grouplabel[0]
     * here has the index taglabel.size()).
     * So with this QHash we can obtain the index of an entry row from the name of the
     * tag which is specifyed in this row.
     *
     * Then we do the actual check for circles in the tags. When doing this, we also create the last of
     * QStringLists taggroupsascending, which saves the order in which the tags should be inserted,
     * i.e. the highest level group at first and the leaf tags at last.
     *
     * It stores one list of QStringList for every path which is specifyed in the dialog,
     * where a path is a sequence of tags with one tag being the parent of the next one
     * (like Moembris, Familie, Jonathan or Game of Thrones, Wall, Samwell Tarly).
     * These QStringLists contain the corresponding path and at last the category the tags belong to.
     *
     *
     * With this list we can finally create the tags with their groups in the correct order
     * */
    QHash<QString, QHash<QString, int> > tagtoindexstable;
    QList<QStringList> taggroupsascending;
    /* We first insert every pair of a tagname with its index into the
     * QHash in tagtoindexstable for the corresponding category
     * */
    for (int i = 0; i < taglabel.size() + grouplabel.size(); ++i)
        tagtoindexstable[categorycombobox.at(i)->currentText()].insert(textAtIndex(i), i);

    /* We create o copy of tagtoindexstable in order to modify it without loosing
     * the information
     * */
    QHash<QString, QHash<QString, int> > tagtoindex(tagtoindexstable);

    bool circleexists = false;
    /* Now we check for cicles.
     * */
    for (int i = 0; i < taglabel.size() + grouplabel.size() && !circleexists; ++i)
    {
        /* For every specifyed tag, we first save its category
         *  and a reference to the corresponding QHash in tagtoindex
         * */
        QString category = categorycombobox.at(i)->currentText();
        QHash<QString, int> &categoryhash(tagtoindex[category]);
        if (categoryhash.contains(textAtIndex(i)))
        {
            /* We create a new QStringList in taggroupsascending for this path
             * already ending with the name of the category
             * */
            taggroupsascending.append(QStringList({QString(category)}));
            //Save the starting point
            QString starttag(textAtIndex(i));
            /* Now we walk through the path. j will mark the current point
             * in the path.
             *
             * In every step we first
             * add the current tag to the QStringList in taggroupsascending
             * corresponding to the current path.
             *
             * Then we check if the categoryhash contains the group
             * of the current path. If not, the path is ended and we leave the loop by setting further to false.
             * If yes, we can walk further in the path by setting j to the index of the
             * group of the current path.
             *
             * But if we then land on the starting point again, we have walked in a circle,
             * so we present an error message that demands the user to correct his input,
             * do not create any tags and let the user go back to the dialog
             * */
            int j = i;
            bool further = true;
            do
            {
                //Insert the current tag into the path
                taggroupsascending.last().prepend(textAtIndex(j));
                //Check whether the group of the current path is also specifyed
                if (categoryhash.contains(groupentry.at(j)->text().trimmed()))
                {
                    //Walk on in the path
                    j = categoryhash.value(groupentry.at(j)->text().trimmed());
                    //Check if we are at the starting point again
                    if (textAtIndex(j) == starttag)
                    {
                        QMessageBox::critical(this, QString::fromUcs4(U"Ung\xFC" "ltige Eingabe"),
                                              QString::fromUcs4(U"Die eingegebenen Tags enthalten eine Kreisstruktur"));
                        return;
                    }
                }
                else
                    further = false;
            } while (further);

            /* Finally we mark all tags we walked through by removing them from
             * thecategoryhash.
             *
             * This is the reason we needed a copy of tagtoindexstable,
             * with which we can do what we want.
             * */
            for (j = 0; j < taggroupsascending.last().size()-1; ++j)
                categoryhash.remove(taggroupsascending.last().at(j));
        }
    }

    /* Finally we can create the tags by going through
     * the lists in tagggroupsascending in the correct order.
     * */
    TagTreeMaster *tag_model = master->tag_tree_master();
    for (int i = 0; i < taggroupsascending.size(); ++i)
    {
        for (int j = 0; j < taggroupsascending.at(i).size()-1; ++j)
            /* The last itom in a path in taggroupsascending specifies the category
             * We obtain the group directly from the groupentry,
             * not by using the preceding string in the path,
             * as for one group many children could be specifyed.
             * Then only the first one would follow its parent,
             * the others would be starting points of different paths
             * and appear in another QStringList of taggroupsascending (but a later one,
             * so the order is still right).
             * */
            tag_model->createNewTags(taggroupsascending.at(i).last(),
                     groupentry.at(tagtoindexstable[taggroupsascending.at(i).last()].
                     value(taggroupsascending.at(i).at(j)))->text().trimmed(),
                    taggroupsascending.at(i).at(j));
    }

    accept();
}

void DUnknownTagsDialog::newEntryForGroup(int index)
{
    /* We recieved the signal to append a row where the group at index
     * can be specifyed.
     *
     * If the groupentry which should be specifyed is empty, we do not append such a row.
     * */
    if (groupentry.at(index)->text().trimmed().isEmpty())
        return;

    TagTreeMaster *tag_model = master->tag_tree_master();
    QString category(categorycombobox.at(index)->currentText());
    /* If there is already an entry where the group can be specifyed, we have nothing to do.
     * */
    if (!tagtogroup.contains(index))
    {
        /* If the tag is incidentally already specifyed in the unknowntagsdialog,
         * we also do not want to insert a new row to specify it again,
         * so we search all tagentries whether their tag and their category
         * is equal to the group at index.
         * */
        bool tagdoesnotyetexist = true;
        for (int i = 0; i < taglabel.size() + grouplabel.size(); ++i)
            if (categorycombobox.at(i)->currentText() == category &&
                    textAtIndex(i) == groupentry.at(index)->text().trimmed())
                tagdoesnotyetexist = false;

        /* If the tag does also not exist in worker, we can really append the new row.
         * */
        if (tagdoesnotyetexist
                && !tag_model->tagExists(category,
                                    groupentry.at(index)->text().trimmed()))
        {
            /* We create a new categorycombobox, a grouplabel and a groupentry,
             * its plus button and add everything to the tagbox on the correct position.
             * */
            categorycombobox += new QComboBox(this);
            categorycombobox.last()->addItem(category);
            grouplabel += new QLabel(groupentry.at(index)->text().trimmed(), this);
            /* In order to remember where the group at index is specifyed,
             * we insert it to tagstospecify.
             * */
            tagstospecify.insert(groupentry.at(index)->text().trimmed());
            groupentry += new DSuggestionEdit(master,
                                             AE::DEntityType::TAG,
                                             AE::DEnterType::NONE,
                                             AE::DAllowed::GROUP,
                                             this,
                                             categorycombobox.last()->currentText());
            specifygroupbutton += new QPushButton("+", this);

            tagbox->addWidget(categorycombobox.last(), groupentry.size()-1+1, 0);
            tagbox->addWidget(grouplabel.last(), groupentry.size()-1+1, 1);
            tagbox->addWidget(groupentry.last(), groupentry.size()-1+1, 2);
            tagbox->addWidget(specifygroupbutton.last(), groupentry.size()-1+1, 3);

            /* We synchronize the category of the new row with the suggestionedits
             * */
            connect(categorycombobox.last(), SIGNAL(currentTextChanged(QString)),
                    groupentry.last(), SLOT(setCategory(QString)));
            /* When the new plus button is clicked, the newEntryforGroup slot
             * shall be called with the index of the new row
             * */
            specifygroupmapper->setMapping(specifygroupbutton.last(), groupentry.size() - 1);
            QObject::connect(specifygroupbutton.last(), SIGNAL(clicked(bool)), specifygroupmapper, SLOT(map()));

            /* Furthermore the text in the groupentry at index must be synchronized
             * with the grouplabel of the new row, and the two categorycomboboxes
             * must be synchronized.
             * */
            tagmapper->setMapping(groupentry.at(index), index);
            tagmapper->setMapping(categorycombobox.at(index), index);
            QObject::connect(groupentry.at(index), SIGNAL(editingFinished()), tagmapper, SLOT(map()));
            QObject::connect(categorycombobox.at(index), SIGNAL(currentIndexChanged(int)),
                             tagmapper, SLOT(map()));

            tagtogroup.insert(index, groupentry.size()-1);
        }
    }
}

void DUnknownTagsDialog::adaptEntryForGroup(int index)
{
    QString category(categorycombobox.at(index)->currentText());
    if (tagtogroup.contains(index))
    {
        groupentry[tagtogroup[index]]->setEnabled(true);
        if (master->tag_tree_master()->tagExists(category, groupentry.at(index)->text())
                || groupentry.at(index)->text().isEmpty())
            groupentry[tagtogroup[index]]->setEnabled(false);

        categorycombobox[tagtogroup.value(index)]->clear();
        categorycombobox[tagtogroup.value(index)]->addItem(category);
        grouplabel[tagtogroup.value(index) - taglabel.size()]->setText(groupentry.at(index)->text());
    }
}


}
