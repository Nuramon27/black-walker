#ifndef AWINDOW_HPP
#define AWINDOW_HPP


#include <QMainWindow>


#include <QString>
#include <QStringList>
#include <QLinkedList>
#include <QSet>
#include <QMultiHash>

class QCloseEvent;

class QPushButton;

class QMenu;
class QMenuBar;
class QAction;

class QTreeView;
class QHeaderView;

class QStackedWidget;
class QHBoxLayout;
class QVBoxLayout;



#include "zdef.hpp"

class Master;
class SettingsQt;
class DDreamListView;
class DNightListView;
class DNightListModel;
class DDreamForm;


/**
 * @brief The AWindow class is the main window of the application.
 *
 * It contains the AWorker object #worker which does all calculations in the QThread #workthread.
 *
 * @sa AWorker, AGlobalSettings, DDialogs
 */
class AWindow : public QMainWindow
{
    Q_OBJECT

    QDate selectedNight;

    QMenu *filemenu, *extrasmenu;
    QAction *saveaction, *openNewDiaryaction,
    *openBackupaction,
    *aboutaction,
    *savetonewplaceaction,
    *exportjsonaction,
    *savecopytonewplaceaction, *createnewdiaryaction,
    *settingsaction,
    *searchaction, *pretty_print_action, *tagadminaction;
    QPushButton *newdreambutton;
    QStackedWidget *dreaminputstackwidget;
    QWidget *AWidget;
    DNightListView *nightlistview;
    DDreamListView *dreamlistview;
    QHeaderView *nightlistheader, *dreamlistheader;
    DDreamForm *dreaminput;

    QHBoxLayout *nightoverviewbox;
    QVBoxLayout *mainbox;

    /**
     * @brief The thread in which the #worker operates
     */
    QThread *workthread;
    QThread *backgroundthread;

    /**
     * @brief The main worker object of the Application.
     *
     * It does all calculations like inserting dreams, searching for dreams or calculating statistics.
     * The worker object operates in the QThread #workthread.
     */
    Master *master;
    SettingsQt *settings;


public:
    /**
     * @brief Constructs a new AWindow with parent parent.
     *
     * There is no reason that an application shall only contain one instance of the AWindow class
     * several main Windows can exist concurrently, even in different threads. The only instance they
     * share will be GlobalSettings (of the AGlobalSettings class) but this class is thread-safe.
     */
    AWindow(Master *new_master, QWidget *parent = nullptr);
    /**
     * @brief Stops the #workthread and destructs the AWindow object.
     * */
    ~AWindow();


protected:
    /**
     * @brief Controls the closing of the window. This means that if there are unsaved changes
     * (or even a dream currently open for editing) the user is asked whether he wants to save the changes
     */
    virtual void closeEvent(QCloseEvent *event);
    /**
     * @brief Stores the new size to the settings
     */
    virtual void resizeEvent(QResizeEvent *event);

    QSize sizeHint() const;

private slots:
    /**
     * @brief Opens the DDialogs::DSearchDialog
     */
    void search();
    /**
     * @brief Opens the DDreamForm and inserts the entered dream into the database.
     */
    void newdream();
    /**
     * @brief Sets the night of #dreamlist to the date lying in #nightlist under the QModelIndex
     * newnight.
     */
    void changeSelectedNight(QModelIndex const &newnight);
    /**
     * @brief Scrolls the #dreamlist to the date lying in #nightlist under the QModelIndex
     * newnight.
     */
    void scrolldreamlistTo(QModelIndex const &newnight);
    /**
     * @brief Tries to open the dream lying in #dreamlist under the QModelIndex index in the #dreaminput.
     *
     * If there is currently no dream opened for editing in #dreaminput, the dream is opened, otherwise
     * nothing is done.
     */
    void selectDreamToRead(const QModelIndex &index);
    /**
     * @brief Closes the #dreaminput and displays the #newdreambutton instead.
     */
    void closeDreamInput();
    /**
     * @brief Shows an error dialog about failure when saving the Diary
     */
    void save_failure_dialog();
    /**
     * @brief Shows an error dialog about failure when loading the Diary.
     */
    void load_failure_dialog();
    /**
     * @brief Saves the diary to the standard diary location specifyed in AGlobalSettings
     */
    bool save();
    /**
     * @brief Displays a file dialog and lets the user specify a new standard diary location
     * of an existing diary.
     *
     * The diary on this location is opened.
     */
    void selectDiaryPath();
    /**
     * @brief Displays a file dialog and lets the user specify a location where to create a new diary.
     *
     * If he does, the curent #worker is completely cleared.
     */
    void createNewDiary();
    /**
     * @brief Displays a file dialog and lets the user specify a new standard diary location.
     * The current diary is saved to that new location.
     */
    bool saveToNewPlace();
    /**
     * @brief Displays a file dialog and lets the user specify a location where to save a copy
     * of the diary. The standard diary location is not changed.
     */
    bool saveCopyToNewPlace();
    bool prettyPrint();
    /**
     * @brief exportJson Displays a file dialog and lets the user specify a location where to save
     * the diary in JSON-format.
     * @return
     */
    bool exportJson();
    /**
     * @brief Displays a file dialog and lets the user specify a location from where
     * to open a diary, which is indended for opening backups.
     *
     * The current diary is stored in diarypath/Backup/diaryname-temp.diaryending, except
     * the user specifies exactly this location for opening the backup.
     */
    void loadBackup();
    /**
     * @brief Opens the DDialogs::AdministrateTagsDialog.
     */
    void administrateTags();
    /**
     * @brief Displays an About Dialog.
     */
    void showAbout();
    /**
     * @brief Determines the night selected in the #nightlist and gives it to the
     * #dreamlist so that it displays the dreams around this night
     */
    void updateDreamList();
    /**
     * @brief sets the sizes of the AWindow and all widgets it contains
     * to the sizes specified in #settings.
     */
    void setSizesToSettings();
    /**
     * @brief Stores the sizes of the AWindow and all widgets it contains
     * to the #settings.
     */
    void storeSizesToSettings();
};

#endif // AWINDOW_HPP
