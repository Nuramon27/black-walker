#ifndef DSEARCHFORM_HPP
#define DSEARCHFORM_HPP


#include <dabstractsearchform.hpp>


#include <QString>
#include <QStringList>

class QSignalMapper;


#include "zdef.hpp"
class Master;


class DDream;
class DSearchFormLeaf;


/**
 * @brief The DSearchForm class is a DAbstractSearchForm which has children.
 *
 * These children are stored in #children and lie in the #childrenbox.
 *
 * @sa DSearchFormLeaf, MBasicInstance::DDreamCondition
 */
class DSearchForm : public DAbstractSearchForm
{
    Q_OBJECT

private:
    QSignalMapper *childrenmapper;
    /**
     * @brief The child search Forms of this search form.
     *
     * The condition of this DSearchForm is the composed condition of the conditions of the children,
     * each enclosed in brackets.
     */
    DSearchFormList children;

    Master *master;

public:
    /**
     * @brief Constructs a new DSearchForm with worker object newworker.
     *
     * @sa DAbstractSearchForm::DAbstractSearchForm(QWidget, bool, bool)
     */
    explicit DSearchForm(Master *newmaster, QWidget *parent = nullptr, bool first = false, bool withplus = true);
    DSearchForm(Master *newmaster, QStringList selectedEntities,
                QWidget *parent = nullptr, bool first = false, bool withplus = true);
    /**
     * @brief Destructs the DSearchForm.
     */
    virtual ~DSearchForm();

    virtual bool isEmpty() const;
    virtual QString getCondition(SearchSettings settings) const;

signals:

public slots:
    /**
     * @brief Adds a new child DSearchFormLeaf after the DSearchFormLeaf after
     */
    virtual void addchild(QWidget *after);
    /**
     * @brief Removes the child toremove.
     */
    virtual void removechild(QWidget *toremove);
};

#endif // DSEARCHFORM_HPP
