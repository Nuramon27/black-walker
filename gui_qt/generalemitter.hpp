#ifndef GENERALEMITTER_HPP
#define GENERALEMITTER_HPP


#include <QObject>


class QApplication;


#include <bdg/Bindings.h>

#include <scopedthread.hpp>


class GeneralEmitter : public QObject
{
    Q_OBJECT
public:
    QApplication &a;
    Master master;
    WorkerMaster workermaster;

public:
    explicit GeneralEmitter(ScopedThread &workerthread, QApplication &newapp);
    virtual ~GeneralEmitter();

signals:
    /// Simple signal for starting the worker thread
    void start_worker_thread();
    /**
     * Simple signal for finishing the execution of the worker thread
     *
     * A cleanup will be introduced before the worker thread shuts down.
     * */
    void finish();
};

extern "C" void init_worker_master(Master::Private *masterptr, WorkerMaster::Private *workerptr);

#endif // GENERALEMITTER_HPP
