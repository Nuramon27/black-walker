#ifndef DABSTRACTSEARCHFORM_HPP
#define DABSTRACTSEARCHFORM_HPP


#include <QWidget>


#include <QLinkedList>
#include <QString>

class QSignalMapper;

class QPushButton;
class QComboBox;

class QHBoxLayout;
class QVBoxLayout;

struct SearchSettings {
    bool withgroups;
    bool withweak;
};


#include "zdef.hpp"


/**
 * @class DAbstractSearchForm
 * @brief The DAbstractSearchForm class is an interface for entering a searchcondition.
 *
 * The instances of DAbstractSearchForm build a tree.
 * A DAbstractSearchForm can either be a DSearchFormLeaf which represents a single condition,
 * or a DSearchForm which represents a composition of more conditions by logical operators.
 * The conditions represented by each DSearchFormLeaf are then compound together, where connditions
 * of the same branch are embraced in brackets.
 *
 * The condition can be retrieved with getCondition() as a QString, that can be interpreted by
 * MBasicInstance::DDreamCondition. It also has a composition, accessible by getCompositionString(),
 * which represents its composition with the condition in the preceding DAbstractSearchForm.
 *
 * @ DSearchForm, DSearchFormLeaf, MBasicInstance::DDreamCondition
 */
class DAbstractSearchForm : public QWidget
{
    Q_OBJECT

public:
    /**
     * @brief A List of pointers to DAbstactSearchForm. Used in DDialogs::DSearchDialog for storing
     * the list of custom search forms.
     */
    typedef QLinkedList<DAbstractSearchForm *> DSearchFormList;
    /**
     * @brief An iterator for a DSearchFormList
     */
    typedef QLinkedList<DAbstractSearchForm *>::iterator DSearchFormListit;
    /**
     * @brief A const iterator for a DSearchFormList
     */
    typedef QLinkedList<DAbstractSearchForm *>::const_iterator DSearchFormListcit;

protected:
    /**
     * @brief QCombobox in which the composition with the preceding condition
     * can be chosen
     */
    QComboBox *compositioncombobox;
    /**
     * @brief By clicking the newsearchformbutton the user can create a new condition after this one.
     */
    QPushButton *newsearchformbutton;
    QPushButton *openbracketbutton;
    QPushButton *closebracketbutton;
    QHBoxLayout *mainbox;
    /**
     * @brief The box in which the children of the DAbstractSearchForm lie. If it is a leaf,
     * this box contains the QLineEdit DSearchFormLeaf::conditionentry for the condition.
     */
    QVBoxLayout *childrenbox;
    QHBoxLayout *buttonbox;

public:
    /**
     * @brief Constructs a new DAbstractSearchForm with parent parent.
     * @param first The first Form of several children of a SearchForm should not have a composition,
     * as there is no preceding condition for it. By setting first to true you prevent the
     * #compositioncombobox from being created.
     * @param withplus The root of a tree of DAbstractSearchForm should not have a plus button
     * as no new Searchforms may be created on the same level. By setting withplus to false
     * no plus button will be created.
     */
    explicit DAbstractSearchForm(QWidget *parent = nullptr, bool first = false, bool withplus = true);
    /**
     * @brief Destructs the DAbstractSearchForm
     */
    virtual ~DAbstractSearchForm();

    /**
     * @brief Returns wether the SearchForm contains any condition
     */
    virtual bool isEmpty() const = 0;
    /**
     * @brief Returns the condition represented by the DAbstractSearchForm
     */
    virtual QString getCondition(SearchSettings settings) const = 0;
    /**
     * @brief Returns the composition with the preceding condition.
     */
    virtual QString getCompositionString() const;

signals:
    void openbracket();
    void closebracket();
    /**
     * @brief Signals that a new leaf shall be created, i.e. the #newsearchformbutton has been pressed.
     */
    void newleaf();
};

#endif // DABSTRACTSEARCHFORM_HPP
