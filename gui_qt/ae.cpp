#include "ae.hpp"


#include <QChar>
#include <QString>
#include <QStringList>
#include <QTextStream>

#include <QWidget>
#include <QTreeView>
#include <QComboBox>


#include <bdg/Bindings.h>


namespace AE
{
void setSizeWidget(SettingsQt const *settings_qt, QWidget *widget, QString const &name)
{
    QSize size(settings_qt->sizeWindow(name, 0), settings_qt->sizeWindow(name, 1));
    widget->resize(size);
}
void storeSizeWidget(SettingsQt *settings_qt, QWidget const *widget, QString const &name)
{
    QSize size = widget->size();
    settings_qt->setSizesWindow(name, size.width(), size.height());
}
void setColumnSizes(SettingsQt const *settings_qt, QTreeView *view, QString const &name, quint32 nr)
{
    for (quint32 i = 0; i < nr; ++i)
    {
        view->setColumnWidth(i, settings_qt->sizeColumn(name, i));
    }
}
void storeColumnSizes(SettingsQt *settings_qt, QTreeView const *view, QString const &name, quint32 nr)
{
    for (quint32 i = 0; i < nr; ++i)
    {
        settings_qt->setSizeColumn(name, i, view->columnWidth(i));
    }
    settings_qt->cropSizeColumnToDefault(name, nr);
}

void set_combobox_with_model(QComboBox &box, QAbstractItemModel const &model)
{
    QString current_text = box.currentText();
    box.clear();
    for (int i = 0; i < model.rowCount(); ++i)
    {
        box.addItem(model.data(model.index(i, 0)).toString());
    }
    box.setEditable(true);
    box.setCurrentText(current_text);
}
void connect_combobox_with_model(QComboBox &box, QAbstractItemModel const &model)
{
    set_combobox_with_model(box, model);

    auto lambda_func = [&model, &box]() {
        set_combobox_with_model(box, model);
    };
    QObject::connect(&model, &QAbstractItemModel::modelReset, &box, lambda_func);
    QObject::connect(&model, &QAbstractItemModel::rowsInserted, &box, lambda_func);
    QObject::connect(&model, &QAbstractItemModel::rowsRemoved, &box, lambda_func);
    QObject::connect(&model, &QAbstractItemModel::rowsMoved, &box, lambda_func);
    QObject::connect(&model, &QAbstractItemModel::dataChanged, &box, lambda_func);
}
QStringList getStandardTagCategories(SettingsQt *settings_qt)
{
    auto res = QStringList();
    for(int i = 0; i < settings_qt->nrStandardTagCategories(); ++i)
    {
        res.append(settings_qt->standardTagCategory(i));
    }
    return res;
}
QStringList getAllTagCategories(TagTreeMaster *tag_tree_master)
{
    auto res = QStringList();
    for(int i = 0; i < tag_tree_master->rowCount(); ++i)
    {
        res.append(tag_tree_master->category(i));
    }
    return res;
}

bool isEndOfLatexInst(QChar c)
{
    return (!c.isLetterOrNumber());
}

QStringList getNextLatexInstruction(QTextStream &stream)
{
    //Buffer for the read character
    QChar c;
    //Search for the next \ character
    while (c != QChar('\\') && !stream.atEnd())
        stream >> c;

    if (stream.atEnd())
        return QStringList({QString()});

    return getCurrLatexInstruction(stream);
}

QStringList getCurrLatexInstruction(QTextStream &stream)
{
    //Buffer for the read character
    QChar c;
    /* Inst will store the name of the instruction, res the complete list that shall be returned
     * */
    QString inst;
    QStringList res;

    /* Read the first character of the instruction
     * */
    stream >> c;
    while (!isEndOfLatexInst(c) && !stream.atEnd())
    {
        /* Now we always check at first, whether the read character is not the end of a
         * latex instruction and whether we have read from the end of the stream.
         * If both is false, we can add the char to the instruction and read the next char.
         * */
        inst += c;
        stream >> c;
    }
    res += inst;
    /* As long as we find '{' chars, there are further arguments
     * */
    while (c == QChar('{') && !stream.atEnd())
    {
        //Read the first charatcor of the current argument (which should not be contaminated with the '{'
        stream >> c;
        QString arg;
        while (c != QChar('}') && !stream.atEnd())
        {
            //Now the same as above
            arg += c;
            stream >> c;
        }
        res += arg;
        //Read another character that may be the leading '{' of the next argument
        stream >> c;
    }

    return res;
}

QString getLatexArgument(QTextStream &stream)
{
    QChar c;
    stream >> c;
    if (c != QChar('{') && !stream.atEnd())
        return QString();

    stream >> c;
    QString res;
    while (c != QChar('}') && !stream.atEnd())
    {
        res += c;
        stream >> c;
    }

    return res;
}

QString getTextUntilLatexInstruction(QTextStream &stream, const QString &inst, const QStringList &arg)
{
    //Buffer for the read character
    QChar c;
    /* res will store the text until the next appearance of the given instruction
     *
     * temptext is used in order to temporarily store text that is a latex instruction.
     * Becaus then we interpret this instruction with its arguments and check if it matches
     * inst and arg. If it does, this instruction shall not be entered into res as text,
     * but if it does not, this instruction is just part of the text that should also be returned
     * */
    QString res, temptext;
    //further will mark if the given instruction has been found
    bool further = true;
    while (further == true && !stream.atEnd())
    {
        /* Read one character and check whether it is a \
         * */
        stream >> c;
        if (c == QChar('\\'))
        {
            /* If it does, there is a Latex instruction and we need to store
             * it in temptext
             * */
            temptext += c;
            /* currinst will store the name of the instruction we found,
             * currarg the complete list of its arguments
             * */
            QString currinst;
            QStringList currarg({QString()});

            /* Do the same as in getCurrLatexInstruction,
             * bot every time we reed a character, it is stored in temptext
             * */
            stream >> c;
            temptext += c;
            while (!isEndOfLatexInst(c) && !stream.atEnd())
            {
                currinst += c;
                stream >> c;
                temptext += c;
            }
            while (c == QChar('{') && !stream.atEnd())
            {
                stream >> c;
                temptext += c;
                QString arg;
                while (c != QChar('}') && !stream.atEnd())
                {
                    currarg.last() += c;
                    stream >> c;
                    temptext += c;
                }
                res += arg;
                stream >> c;
                temptext += c;
            }
            /* Now we check if the found instruction matches the given one.
             * If it does, we stpo adding text to res by setting further to false,
             * otherwise this was just some instruction, so we add it to res.
             * */
            if (currinst == inst && currarg == arg)
                further = false;
            else
                res += temptext;

            temptext.clear();
        }
        else
            //If there is no instruction at all, add the character to res
            res += c;
    }

    return res;
}
}
