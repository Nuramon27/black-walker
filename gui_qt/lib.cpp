#include <QCoreApplication>
#include <QApplication>
#include <QFont>
#include <QDir>


#include "awindow.hpp"
#include "bdg/Bindings.h"

//#include <QtPlugin>
//Q_IMPORT_PLUGIN (QWindowsIntegrationPlugin)

/**
 * @mainpage The BlackWalker application offers a graphical interface to write a dream diary.
 * The dream diary is a list of dreams in chronological order. Besides entering and reading dreams
 * it provides a search function provided by the class
 * DDialogs::SearchForm which can search dreams with respect to almost any criterium
 * in arbitrarily complex compositions and the several statistics about the dreams.
 *
 * The main Window of the application is the AWindow class, whereas all calculations (like searching dreams)
 * is done by an instance of the AWorker class. The application basically runs in three threads:
 * One gui thread in which the main window and all graphical objects operates, one Work thread in which the
 * worker object lives and one background thread, which does administrational tasks (like implementing
 * the undo function). These administrational tasks are managed by the ABackground class.
 *
 * The file in which the diary is stored to disk is specifyed at @ref SpecDiary.
 *
 * All Dialogs that are used by the AWindow class are stored in the DDialogs namespace.
 *
 * Settings are stored in the global instance GlobalSettings of the AGlobalSettings class.
 * The settings of a session are stored to disk when closing Black Walker,
 * the specification of this file can be found at @ref SpecGlobalSettings.
 *
 * The application depends on the ManuLib library, from which it mainly uses the MTerm class for the
 * interpretation of search conditions as well as functions from the MConvenience namespace.
 *
 * @section MainGeneralR Some General Remarks concerning this documentation
 *
 * @subsection MainGeneralRCommaSep Comma Separation
 *
 * Throughout this documentation I often talk about "comma separated lists" or "semicola separated lists".
 * This always means a String in which several entities are consecutiveley represented as a string and separated
 * by an arbitrary number of commata (resp. semicola), preceeded or followed by an arbitrary number of
 * spaces or "\n" characters.
 * */
#include "generalemitter.hpp"
#include <asetup.hpp>

#include <scopedthread.hpp>





void init(int argc, char *argv[]);

/**
 * @brief The main function of the application
 * @return Exit code of the event loop
 */
extern "C" int lib(int argc, char *argv[])
{
    QApplication a(argc, argv);
    setup_meta_qt();

    ScopedThread workerthread;
    int ret;
    {
        GeneralEmitter emitter(workerthread, a);
        AWindow w(&emitter.master);
        w.showMaximized();

        ret = a.exec();
    }
    return ret;
}
