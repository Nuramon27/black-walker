#include "dtagedit.hpp"


#include <QRegExp>
#include <QRegExpValidator>

#include <QComboBox>

#include <QGridLayout>


#include <ae.hpp>
#include <dsuggestionedit.hpp>


#include <bdg/Bindings.h>


DTagEdit::DTagEdit(Master *master, QWidget *parent) :
    DAbstractTagEdit(master, parent)
{
    categorycombobox = new QComboBox(parent);
    categorycombobox->setEditable(true);
    categorycombobox->setValidator(
                new QRegExpValidator(QRegExp(QStringLiteral("^[^\\\\,;:\"\'{}\\$%&|\\^(==)!\\(\\)\\[\\]]*$")),
                                     categorycombobox));
    categorycombobox->clearEditText();
    categorycombobox->lineEdit()->setPlaceholderText("Benutzerdefiniert");
    AE::connect_combobox_with_model(*categorycombobox, *master->tag_tree_master());

    QWidget::setTabOrder(categorycombobox, tagentry);

    connect(categorycombobox->lineEdit(), &QLineEdit::editingFinished,
                     categorycombobox->lineEdit(), &QLineEdit::returnPressed);
    connect(categorycombobox, &QComboBox::currentTextChanged,
                     tagentry, &DSuggestionEdit::setCategory);
}

DTagEdit::~DTagEdit()
{
    delete categorycombobox;
}


///MEMBER FUNCTIONS
QString DTagEdit::getCategory() const
{
    return categorycombobox->currentText();
}

void DTagEdit::addToGrid(QGridLayout *grid, int row, int column)
{
    grid->addWidget(categorycombobox, row, column);
    grid->addWidget(tagentry, row, column+1);
}

void DTagEdit::removeFromGrid(QGridLayout *grid)
{
    grid->removeWidget(categorycombobox);
    grid->removeWidget(tagentry);
}

void DTagEdit::setReadOnly(bool readonly)
{
    categorycombobox->setEnabled(!readonly);
    DAbstractTagEdit::setReadOnly(readonly);
}

void DTagEdit::setCategory(const QString &newcategory)
{
    categorycombobox->setCurrentText(newcategory);
}
