#ifndef DTAGEDIT_HPP
#define DTAGEDIT_HPP


#include "dabstracttagedit.hpp"


#include <QPair>
#include <QList>
#include <QLinkedList>
#include <QHash>
#include <QMultiHash>
#include <QSet>

class QComboBox;

class QGridLayout;


#include <zdef.hpp>

class Master;


/**
 * @brief The DTagEdit class provides an entry for entering one or more comma separated Tags
 * and a QCombobox #categorycombobox for choosing a tag category.
 */
class DTagEdit : public DAbstractTagEdit
{
public:
    /**
     * @brief The QCombobox where the tag category can be chosen.
     *
     * It contains all tags that are stored in #worker, sorted alphabetically.
     */
    QComboBox *categorycombobox;

public:
    /**
     * @brief Constructs a new DTagEdit with worker object newworker and parent parent.
     */
    explicit DTagEdit(Master *master, QWidget *parent = nullptr);
    /**
     * @brief Destructs the DTagEdit
     */
    virtual ~DTagEdit();

    virtual QString getCategory() const;

    /**
     * @brief Inserts the #categorycombobox into row and column of grid and the #tagentry to row + 1 and column.
     */
    virtual void addToGrid(QGridLayout *grid, int row, int column);
    /**
     * @brief Removes the Tag Edit from grid
     */
    virtual void removeFromGrid(QGridLayout *grid);
    /**
     * @brief Sets the tag-edit to read-only mode readonly.
     */
    virtual void setReadOnly(bool readonly);
    /**
     * @brief Selects the item newcategory in the #categorycombobox.
     *
     * The user editable field is used if newcategory does not yet exist
     */
    virtual void setCategory(QString const &newcategory);
};

#endif // DTAGEDIT_HPP
