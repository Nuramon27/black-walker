#ifndef DABSTRACTTAGEDIT_HPP
#define DABSTRACTTAGEDIT_HPP


#include <QObject>


#include <QPair>
#include <QList>
#include <QLinkedList>
#include <QString>
#include <QStringList>
#include <QHash>
#include <QMultiHash>
#include <QSet>

class QLineEdit;

class QGridLayout;


#include <zdef.hpp>

class DSuggestionEdit;

class Master;


/**
 * @brief The DAbstractTagEdit class provides an entry for entering a tag.
 *
 * In a DAbstractTagEdit, tags can be entered comma separated by the user. With getTags one can obtain
 * the enterd tags together with their category(-ies) as a AE::QStringMulHash.
 *
 * There are to kinds of Tag Edits: The DTagEdit, where the tag category of the tag can be chosen by the user,
 * and the DStaticTagEdit, where the category is fixed.
 *
 * @attention The class is not inherited from QWidget. as it shall not be a single widget.
 * In order to obtain equal dimensions of more DAbstractTagEdits together,
 * it is most convenient to add the entry for the category into one column of a combobox,
 * and the #tagentry into the next column. By imlementing this class as a QWidget
 * this could not be done, so me only implement it as a collection of a #tagentry and a category widget,
 * that can bo added to a QGridLayout with addToGrid(), distributing its widgets over more columns of the grid.
 */
class DAbstractTagEdit : public QObject
{
public:
    /**
     * @brief The Line Edit, where the tag name can be entered
     */
    DSuggestionEdit *tagentry;

protected:
    Master *master;

public:
    /**
     * @brief Constructs a new DAbstractTagEdit with worker object newworker and parent parent.
     */
    explicit DAbstractTagEdit(Master *newmaster, QWidget *parent = nullptr);
    /**
     * @brief Destructs the DAbstractTagEdit
     */
    virtual ~DAbstractTagEdit();

    /**
     * @brief Returns the entry where the tag name is entered.
     */
    inline DSuggestionEdit *getTagEntry() const {return tagentry;}
    /**
     * @brief Returns the category of the tag.
     */
    virtual QString getCategory() const = 0;
    /**
     * @brief Returns the entered tags as a AE::QStringMulHash, which may e.g. be joined with currently existing tags
     * of a DDream.
     */
    virtual QString text() const;

    /**
     * @brief Adds the Tag Edit into a QGridLayout. There it occupies the rows row to row + 2
     * and the column column.
     */
    virtual void addToGrid(QGridLayout *grid, int row, int column) = 0;
    /**
     * @brief Removes the Tag Edit from grid
     */
    virtual void removeFromGrid(QGridLayout *grid) = 0;
    /**
     * @brief Sets the #tagentry to readonly mode readonly.
     */
    virtual void setReadOnly(bool readonly);
    /**
     * @brief Sets the category of the DAbstractTagEdit to newcategory
     */
    virtual void setCategory(QString const &newcategory) = 0;
};


#endif // DABSTRACTTAGEDIT_HPP
