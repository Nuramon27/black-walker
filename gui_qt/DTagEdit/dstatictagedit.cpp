#include "dstatictagedit.hpp"


#include <QLabel>

#include <QGridLayout>


#include <dsuggestionedit.hpp>


DStaticTagEdit::DStaticTagEdit(Master *newmaster, QString const &category, QWidget *parent) :
    DAbstractTagEdit(newmaster, parent)
{
    tagentry->setCategory(category);
    categorylabel = new QLabel(category, parent);
}

DStaticTagEdit::~DStaticTagEdit()
{
    delete categorylabel;
}


///MEMBER FUNCTIONS
QString DStaticTagEdit::getCategory() const
{
    return categorylabel->text();
}

/*QPair<QString, QStringList> DStaticTagEdit::getTags() const
{
    QString text(tagentry->text());
    return QPair<QString, QStringList>(categorylabel->text(),
                                       text.split(AGlobalSettings::commaregexp, QString::SkipEmptyParts));
}*/

void DStaticTagEdit::addToGrid(QGridLayout *grid, int row, int column)
{
    grid->addWidget(categorylabel, row, column);
    grid->addWidget(tagentry, row, column+1);
}

void DStaticTagEdit::removeFromGrid(QGridLayout *grid)
{
    grid->removeWidget(categorylabel);
    grid->removeWidget(tagentry);
}

void DStaticTagEdit::setCategory(const QString &newcategory)
{
    categorylabel->setText(newcategory);
}
