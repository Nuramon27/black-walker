#include "dabstracttagedit.hpp"

#include <QSizePolicy>

#include <QWidget>

#include <QLineEdit>

#include <QGridLayout>


#include <dsuggestionedit.hpp>
#include <Enum/dallowed.hpp>
#include <Enum/dentitytype.hpp>
#include <Enum/dentertype.hpp>


#include <bdg/Bindings.h>


DAbstractTagEdit::DAbstractTagEdit(Master *newmaster, QWidget *parent) :
    QObject(parent),
    master(newmaster)
{
    tagentry = new DSuggestionEdit(master, AE::DEntityType::TAG,
                                   AE::DEnterType::COMMASEP,
                                   AE::DAllowed::ALL,
                                   parent);
    tagentry->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Minimum);
}

DAbstractTagEdit::~DAbstractTagEdit()
{
    delete tagentry;
}

QString DAbstractTagEdit::text() const
{
    return tagentry->text();
}

void DAbstractTagEdit::setReadOnly(bool readonly)
{
    tagentry->setReadOnly(readonly);
}
