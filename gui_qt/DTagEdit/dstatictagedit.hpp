#ifndef DSTATICTAGEDIT_HPP
#define DSTATICTAGEDIT_HPP


#include "dabstracttagedit.hpp"


#include <QPair>
#include <QList>
#include <QLinkedList>
#include <QHash>
#include <QMultiHash>
#include <QSet>

class QLabel;

class QGridLayout;


#include <zdef.hpp>

class Master;


/**
 * @brief The DStaticTagEdit class provides only an entry for entering names of tags.
 *
 * The category is fixed and can be given in the constructor
 * DStaticTagEdit(AWorker const &, QString const &, QWidget *)
 */
class DStaticTagEdit : public DAbstractTagEdit
{
public:
    /**
     * @brief Displays the name of the category
     */
    QLabel *categorylabel;

public:
    /**
     * @brief Constructs a new DStaticTagEdit with worker object newworker and parent parent.
     */
    DStaticTagEdit(Master *newmaster, QString const &category, QWidget *parent = nullptr);
    /**
     * @brief Destructs the DStaticTagEdit
     */
    virtual ~DStaticTagEdit();

    virtual QString getCategory() const;
    /*virtual QPair<QString, QStringList> getTags() const;*/

    /**
     * @brief Inserts the #categorylabel into row and column of grid and the #tagentry to row + 1 and column.
     */
    virtual void addToGrid(QGridLayout *grid, int row, int column);
    /**
     * @brief Removes the Tag Edit from grid
     */
    virtual void removeFromGrid(QGridLayout *grid);
    /**
     * @brief Sets the text of categorylabel to newcategory
     */
    virtual void setCategory(QString const &newcategory);
};


#endif // DSTATICTAGEDIT_HPP
