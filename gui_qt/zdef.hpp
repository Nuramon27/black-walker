#ifndef ZDEF_HPP
#define ZDEF_HPP


/**
 * @file
 *
 * @brief The zdef.h header file contains global typedefs for several specializations of Qts generic container classes
 * */

#include <QtGlobal>

#include <QDate>

#include <QPair>
#include <QList>
#include <QLinkedList>
#include <QHash>
#include <QMultiHash>
#include <QMap>
#include <QMultiMap>
#include <QSet>
#include <QString>
#include <QStringList>

#include <QReadWriteLock>
#include <QReadLocker>
#include <QWriteLocker>

#ifdef BLACKWALKER_DEBUG
#include <iostream>
#endif //BLACKWALKER_DEBUG


#include <ae.hpp>


/**
 * @brief Another name for a QReadWriteLock
 */
typedef QReadWriteLock rwMutex;


#endif // ZDEF_HPP
