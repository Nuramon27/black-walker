#include "generalemitter.hpp"


#include <QApplication>


#include <DModel/ddreamlistview.hpp>

GeneralEmitter::GeneralEmitter(ScopedThread &workerthread, QApplication &newapp) :
    QObject(nullptr),
    a(newapp),
    master(),
    workermaster()
{
    init_worker_master(master.m_d, workermaster.m_d);
    workermaster.moveToThread(&workerthread);
    QObject::connect(this, &GeneralEmitter::start_worker_thread, &workermaster, &WorkerMaster::work);
    workerthread.start();
    emit start_worker_thread();

    QObject::connect(&master, &Master::notify_begin_cmdChanged, &master, &Master::begin_cmd);
    QObject::connect(&master, &Master::notify_end_cmdChanged, &master, &Master::end_cmd);
    QObject::connect(master.dream_list_master(), &DreamListMaster::begin_cmdChanged, &master, &Master::begin_cmd);
    QObject::connect(master.dream_list_master(), &DreamListMaster::end_cmdChanged, &master, &Master::end_cmd);
    QObject::connect(master.dream_list_master()->dream_list_qt(), &DreamListQt::begin_cmdChanged,
                     &master, &Master::begin_cmd);
    QObject::connect(master.dream_list_master()->dream_list_qt(), &DreamListQt::end_cmdChanged,
                     &master, &Master::end_cmd);
    QObject::connect(master.tag_tree_master(), &TagTreeMaster::begin_cmdChanged, &master, &Master::begin_cmd);
    QObject::connect(master.tag_tree_master(), &TagTreeMaster::end_cmdChanged, &master, &Master::end_cmd);
    QObject::connect(master.entry_master(), &EntryMaster::begin_cmdChanged, &master, &Master::begin_cmd);
    QObject::connect(master.entry_master(), &EntryMaster::end_cmdChanged, &master, &Master::end_cmd);
    QObject::connect(master.settings_qt(), &SettingsQt::begin_cmdChanged, &master, &Master::begin_cmd);
    QObject::connect(master.settings_qt(), &SettingsQt::end_cmdChanged, &master, &Master::end_cmd);


    QObject::connect(&workermaster, &WorkerMaster::wakeupChanged, &master, &Master::end_cmd, Qt::QueuedConnection);
    QObject::connect(&master, &Master::wakeup_leaveChanged, &master, &Master::leave, Qt::QueuedConnection);

    QObject::connect(this, &GeneralEmitter::finish, &master, &Master::begin_leave, Qt::QueuedConnection);
    QObject::connect(&master, &Master::leave_signalChanged, &a, &QApplication::quit);
}

GeneralEmitter::~GeneralEmitter()
{
}
