//! Everything you need to obtain suggestions from a line of user-entered text.

use std::cmp::Ord;
use std::ops::Range;
use std::str::FromStr;

use super::config::{EnterType, EntityType};
use super::{AND_REG_EXP, COMMA_REG_EXP, DDOT_REG_EXP, SEMICOLON_REG_EXP};
use crate::TagName;

pub struct SuggestionFragment {
    /// Everything which has been found out about the given fragment of the String
    pub meta: SuggestionQuery,
    pub fragment: String,
    /// The range of interest of the String
    pub range: Range<usize>,
}

/// A draft for a search for suggestions, containing
/// every information about the fragment specified by the user.
///
/// This information is used as boundary conditions in the search.
#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub enum SuggestionQuery {
    Tag {
        category: Option<String>,
        group: Option<TagName>,
    },
    TagGroup {
        category: Option<String>,
        group: Option<TagName>,
    },
    Entry,
    TagCategory,
    EntryCategory,
}

/// Evaluate fragment `s` with respect to entity type `to_suggest` and `enter_type`,
/// in order to search for suggestions for the fragment.
///
/// `initial_cursor` is the position of the cursor in the String.
pub fn suggestion_fragment(
    s: &str,
    to_suggest: EntityType,
    enter_type: EnterType,
    initial_cursor: usize,
) -> SuggestionFragment {
    let mut cursor = initial_cursor;
    if enter_type.is_empty() {
        return SuggestionFragment {
            meta: match to_suggest {
                EntityType::Tag => SuggestionQuery::Tag {
                    category: None,
                    group: None,
                },
                EntityType::Entry => SuggestionQuery::Entry,
                EntityType::TagCategory => SuggestionQuery::TagCategory,
                EntityType::EntryCategory => SuggestionQuery::EntryCategory,
            },
            fragment: s.to_string(),
            range: 0..s.len(),
        };
    }
    let unitrange = if enter_type.intersects(EnterType::MULTIPLEUNITS) {
        let start = SEMICOLON_REG_EXP
            .find_iter(&s[..cursor])
            .last()
            .map(|m| m.start() + m.as_str().len())
            .unwrap_or(0);
        let end = SEMICOLON_REG_EXP
            .find(&s[cursor..])
            .map(|m| cursor + m.start())
            .unwrap_or(s.len());

        start..end
    } else {
        0..s.len()
    };
    let unit = &s[unitrange.clone()];
    cursor = cursor.saturating_sub(unitrange.start);

    let (category, restrange) = if enter_type.intersects(EnterType::WITHCATEGORY) {
        match DDOT_REG_EXP.find(unit) {
            Some(m) => {
                if cursor < m.start() {
                    return SuggestionFragment {
                        meta: match to_suggest {
                            EntityType::Tag | EntityType::TagCategory => {
                                SuggestionQuery::TagCategory
                            }
                            EntityType::Entry | EntityType::EntryCategory => {
                                SuggestionQuery::EntryCategory
                            }
                        },
                        fragment: unit[..m.start()].to_string(),
                        range: unitrange.start..m.start() + unitrange.start,
                    };
                }
                (
                    Some(&unit[..m.start()]),
                    m.start() + m.as_str().len()..unit.len(),
                )
            }
            None => (None, 0..unit.len()),
        }
    } else {
        (None, 0..unit.len())
    };
    cursor = cursor.saturating_sub(restrange.start);

    let rest = &unit[restrange.clone()];

    let (group, entityrange) = if enter_type.intersects(EnterType::WITHGROUPS) {
        let mut entityrange = 0..rest.len();
        let mut range = None;
        #[allow(clippy::reversed_empty_ranges)]
        let mut mit = vec![(0..0)]
            .into_iter()
            .chain(DDOT_REG_EXP.find_iter(rest).map(|m| m.start()..m.end()))
            .peekable();
        while let Some(m) = mit.next() {
            if let Some(next) = mit.peek() {
                if cursor >= m.end && cursor < next.start {
                    return SuggestionFragment {
                        meta: SuggestionQuery::TagGroup {
                            category: category.map(str::to_string),
                            group: range
                                .and_then(|r: Range<usize>| TagName::from_str(&rest[r]).ok()),
                        },
                        fragment: rest[m.end..next.start].to_string(),
                        range: (m.end + restrange.start + unitrange.start)
                            ..(next.start + restrange.start + unitrange.start),
                    };
                }
                range = Some((m.end)..next.start);
            } else {
                entityrange = m.end..rest.len();
                // We break one item earlier than the iterator ends
                break;
            }
        }
        match range {
            Some(range) => (Some(&rest[range]), entityrange),
            None => (None, entityrange),
        }
    } else {
        (None, 0..rest.len())
    };
    cursor = cursor.saturating_sub(entityrange.start);
    let entity = &rest[entityrange.clone()];

    let fragmentrange = if enter_type.intersects(EnterType::COMMASEP)
        && !enter_type.intersects(EnterType::ANDSEP)
    {
        let start = COMMA_REG_EXP
            .find_iter(&entity[..cursor])
            .last()
            .map(|m| m.start() + m.as_str().len())
            .unwrap_or(0);
        let end = COMMA_REG_EXP
            .find(&entity[cursor..])
            .map(|m| cursor + m.start())
            .unwrap_or(entity.len());

        start..end
    } else if enter_type.intersects(EnterType::ANDSEP)
        && !enter_type.intersects(EnterType::COMMASEP)
    {
        let start = AND_REG_EXP
            .find_iter(&entity[..cursor])
            .last()
            .map(|m| m.start() + m.as_str().len())
            .unwrap_or(0);
        let end = AND_REG_EXP
            .find(&entity[cursor..])
            .map(|m| cursor + m.start())
            .unwrap_or(entity.len());

        start..end
    } else if enter_type.intersects(EnterType::COMMASEP) && enter_type.intersects(EnterType::ANDSEP)
    {
        let comma_start = COMMA_REG_EXP
            .find_iter(&entity[..cursor])
            .last()
            .map(|m| m.start() + m.as_str().len())
            .unwrap_or(0);
        let comma_end = COMMA_REG_EXP
            .find(&entity[cursor..])
            .map(|m| cursor + m.start())
            .unwrap_or(entity.len());

        let and_start = AND_REG_EXP
            .find_iter(&entity[..cursor])
            .last()
            .map(|m| m.start() + m.as_str().len())
            .unwrap_or(0);
        let and_end = AND_REG_EXP
            .find(&entity[cursor..])
            .map(|m| cursor + m.start())
            .unwrap_or(entity.len());

        comma_start.max(and_start)..comma_end.min(and_end)
    } else {
        0..entity.len()
    };

    let mut res = SuggestionFragment {
        meta: match to_suggest {
            EntityType::Tag => SuggestionQuery::Tag {
                category: category.map(str::to_string),
                group: group.and_then(|s| TagName::from_str(s).ok()),
            },
            EntityType::Entry => SuggestionQuery::Entry,
            EntityType::EntryCategory => SuggestionQuery::EntryCategory,
            EntityType::TagCategory => SuggestionQuery::TagCategory,
        },
        fragment: entity[fragmentrange.clone()].to_string(),
        range: (fragmentrange.start + entityrange.start + restrange.start + unitrange.start)
            ..(fragmentrange.end + entityrange.start + restrange.start + unitrange.start),
    };
    cut_initial_condition(&mut res, enter_type);
    res
}

/// Cuts the initial condition specifier from `frag`, respecting the
/// `enter_type`.
fn cut_initial_condition(frag: &mut SuggestionFragment, enter_type: EnterType) {
    if enter_type.intersects(EnterType::CONDITION) {
        if frag.fragment.starts_with('!') {
            frag.fragment = frag.fragment["!".len()..].to_string();
            frag.range.start += "!".len();
        }
        if frag.fragment.starts_with('=') {
            frag.fragment = frag.fragment["=".len()..].to_string();
            frag.range.start += "=".len();
        }
        if frag.fragment.starts_with(':') {
            frag.fragment = frag.fragment[":".len()..].to_string();
            frag.range.start += ":".len();
        }
        if frag.fragment.starts_with('>') {
            frag.fragment = frag.fragment[">".len()..].to_string();
            frag.range.start += ">".len();
        }
    }
}

/// Parses a short form s of a condition for tags
/// and returns the full condition
/// that can be evaluated by the Term framework.
///
/// #Examples
///
/// ```
/// use black_walker_logic::parse::suggestion;
/// assert_eq!(
///     suggestion::condition_tag("Personen", "Albert Einstein, Isaac Newton=, :Mathematicians=", false),
///     "(Personen: \"Albert Einstein\" | Personen: \"Albert Einstein*\") | Personen: \"Isaac Newton\" | Personen:: \"Mathematicians\""
/// );
/// ```
pub fn condition_tag(leftside: &str, s: &str, withgroups: bool) -> String {
    let mut ires1: Vec<String> = Vec::new();
    /* The string is first splitted on the boundaries ',' and '&', so we obtain single entities.
     * */
    for spl1 in COMMA_REG_EXP.split(s) {
        let mut ires2: Vec<String> = Vec::new();
        for spl2 in AND_REG_EXP.split(spl1) {
            /* Then for each entity we evaluate prefixes and suffixes.
             * */
            let mut spl = spl2;
            let mut no = String::new();
            let mut prestring = if withgroups {
                "::".to_string()
            } else {
                ":".to_string()
            };
            if spl.starts_with('!') {
                no.push('!');
                spl = &spl[1..];
            }
            if spl.starts_with('=') {
                if withgroups {
                    prestring = prestring[1..].to_string();
                }
                spl = &spl[1..];
            }
            if spl.starts_with(':') {
                if !withgroups {
                    prestring.push(':');
                }
                spl = &spl[1..];
            }
            if spl.starts_with('>') {
                prestring.push('>');
                spl = &spl[1..];
            }

            spl = spl.trim();
            if spl.ends_with('=') {
                spl = &spl[..spl.len() - 1].trim();
                ires2.push(format!("{}{}{} \"{}\"", no, leftside, prestring, spl));
            } else if spl.ends_with('*') {
                spl = &spl[..spl.len() - 1].trim();
                ires2.push(format!("{}{}{} \"{}*\"", no, leftside, prestring, spl));
            } else {
                ires2.push(format!(
                    "{0}({1}{2} \"{3}\" | {1}{2} \"{3}*\")",
                    no, leftside, prestring, spl
                ));
            }
        }
        ires1.push(ires2.join(" & "));
    }

    ires1.join(" | ")
}

/// Does the same as condition_tag but without the special treatments.
/// Instead one can specify with the parameter containing, whether the default operator
/// is > or ==.
pub fn condition_simple(leftside: &str, s: &str, containing: bool) -> String {
    //For commentary see condition_tag()
    let mut ires1: Vec<String> = Vec::new();
    for spl1 in COMMA_REG_EXP.split(s) {
        let mut ires2: Vec<String> = Vec::new();
        for spl2 in AND_REG_EXP.split(spl1) {
            let mut spl = spl2;
            let mut no = String::new();
            let mut prestring = if containing {
                ">".to_string()
            } else {
                "==".to_string()
            };
            if spl.starts_with('!') {
                no.push('!');
                spl = &spl[1..];
            }
            if spl.starts_with('<') {
                prestring = "==".to_string();
                spl = &spl[1..];
            }
            if spl.starts_with('>') {
                prestring = ">".to_string();
                spl = &spl[1..];
            }

            spl = spl.trim();
            ires2.push(format!("{}{}{} \"{}\"", no, leftside, prestring, spl));
        }
        ires1.push(ires2.join(" & "));
    }

    ires1.join(" | ")
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn test_condition_tag() {
        assert_eq!(
            condition_tag("Personen", "Albert Einstein", true),
            "(Personen:: \"Albert Einstein\" | Personen:: \"Albert Einstein*\")"
        );
        assert_eq!(
            condition_tag("Personen", "Albert Einstein=", true),
            "Personen:: \"Albert Einstein\""
        );
        assert_eq!(
            condition_tag(
                "Personen",
                "Albert Einstein= , Isaac Newton= &Max Planck =",
                false
            ),
            "Personen: \"Albert Einstein\" | Personen: \"Isaac Newton\" & Personen: \"Max Planck\""
        );
        assert_eq!(
            condition_tag("Personen", "Albert Einstein   *", false),
            "Personen: \"Albert Einstein*\""
        );
        assert_eq!(
            condition_tag("Personen", ":>Albert Einstein     ", false),
            "(Personen::> \"Albert Einstein\" | Personen::> \"Albert Einstein*\")"
        );
        assert_eq!(
            condition_tag("Personen", "=>Albert Einstein", true),
            "(Personen:> \"Albert Einstein\" | Personen:> \"Albert Einstein*\")"
        );
    }

    #[test]
    fn test_condition_simple() {
        assert_eq!(
            condition_simple("Art", "Trübtraum", false),
            "Art== \"Trübtraum\""
        );
        assert_eq!(
            condition_simple("Text", "Trübtraum", true),
            "Text> \"Trübtraum\""
        );
        assert_eq!(
            condition_simple(
                "Art",
                "   Trübtraum, Klartraum   ,  Präluzider Traum",
                false
            ),
            "Art== \"Trübtraum\" | Art== \"Klartraum\" | Art== \"Präluzider Traum\""
        );
        assert_eq!(
            condition_simple("Art", "!Trübtraum", false),
            "!Art== \"Trübtraum\""
        );
        assert_eq!(
            condition_simple("Titel", "<Albert Einstein!", true),
            "Titel== \"Albert Einstein!\""
        );
        assert_eq!(condition_simple("Art", ">luzid", false), "Art> \"luzid\"");
    }

    #[test]
    fn regex_indices() {
        let s = "aber ,, doch";
        let m = crate::parse::COMMA_REG_EXP.find(s).unwrap();

        assert_eq!(m.start(), 4);
        assert_eq!(m.end(), 8);
        assert_eq!(m.start() + m.as_str().len(), 8);

        assert_eq!(&s[..m.start()], "aber");
        assert_eq!(&s[m.start() + m.as_str().len()..], "doch");
    }

    #[test]
    fn suggestions() {
        let r = suggestion_fragment("Alb", EntityType::Tag, EnterType::empty(), 3);
        assert_eq!(r.fragment, "Alb");
        assert_eq!(r.range, 0..3);

        let r = suggestion_fragment("Alb, Lord", EntityType::Tag, EnterType::COMMASEP, 6);
        assert_eq!(r.fragment, "Lord");
        assert_eq!(r.range, 5..9);

        let r = suggestion_fragment("Alb, Lord", EntityType::Tag, EnterType::COMMASEP, 2);
        assert_eq!(r.fragment, "Alb");
        assert_eq!(r.range, 0..3);

        let r = suggestion_fragment("Alb, Lord", EntityType::Tag, EnterType::TAGSWITHGROUP, 6);
        assert_eq!(r.fragment, "Lord");
        assert_eq!(r.range, 5..9);

        let r = suggestion_fragment("Alb, Lord", EntityType::Tag, EnterType::TAGSWITHGROUP, 2);
        assert_eq!(r.fragment, "Alb");
        assert_eq!(r.range, 0..3);

        let r = suggestion_fragment(
            "Personen: Alb",
            EntityType::Tag,
            EnterType::TAGSWITHGROUP,
            3,
        );
        assert_eq!(r.meta, SuggestionQuery::TagCategory);
        assert_eq!(r.fragment, "Personen");
        assert_eq!(r.range, 0..8);

        let r = suggestion_fragment(
            "Personen: Books: Harry Potter (books): Professors: Alb",
            EntityType::Tag,
            EnterType::TAGSWITHGROUP,
            12,
        );
        assert_eq!(
            r.meta,
            SuggestionQuery::TagGroup {
                category: Some("Personen".to_string()),
                group: None
            }
        );
        assert_eq!(r.fragment, "Books");
        assert_eq!(r.range, 10..15);

        let r = suggestion_fragment(
            "Personen: Books: Harry Potter (books): Professors: Alb",
            EntityType::Tag,
            EnterType::TAGSWITHGROUP,
            43,
        );
        assert_eq!(
            r.meta,
            SuggestionQuery::TagGroup {
                category: Some("Personen".to_string()),
                group: Some(TagName::from_str("Harry Potter (books)").unwrap())
            }
        );
        assert_eq!(r.fragment, "Professors");
        assert_eq!(r.range, 39..49);

        let r = suggestion_fragment(
            "Personen: Books: Harry Potter (books): Professors: Alb",
            EntityType::Tag,
            EnterType::TAGSWITHGROUP,
            52,
        );
        assert_eq!(
            r.meta,
            SuggestionQuery::Tag {
                category: Some("Personen".to_string()),
                group: Some(TagName::from_str("Professors").unwrap())
            }
        );
        assert_eq!(r.fragment, "Alb");
        assert_eq!(r.range, 51..54);
    }

    #[test]
    fn suggestions_complicated_unicode() {
        let text = "Λ√∫: Boóks: Harry Potter (books): Professorŝ: Al😀👨‍❤️‍👨us";
        let r = suggestion_fragment(text, EntityType::Tag, EnterType::TAGSWITHGROUP, "Λ√".len());
        assert_eq!(r.meta, SuggestionQuery::TagCategory);
        assert_eq!(r.fragment, "Λ√∫");
        assert_eq!(r.range, 0.."Λ√∫".len());

        let r = suggestion_fragment(
            text,
            EntityType::Tag,
            EnterType::TAGSWITHGROUP,
            "Λ√∫: Boóks: Harry Potter (books): Professorŝ: Al😀".len(),
        );
        assert_eq!(
            r.meta,
            SuggestionQuery::Tag {
                category: Some("Λ√∫".to_string()),
                group: Some(TagName::from_str("Professorŝ").unwrap())
            }
        );
        assert_eq!(r.fragment, "Al😀👨‍❤️‍👨us");
        assert_eq!(
            r.range,
            "Λ√∫: Boóks: Harry Potter (books): Professorŝ: ".len()
                .."Λ√∫: Boóks: Harry Potter (books): Professorŝ: Al😀👨‍❤️‍👨us".len()
        );

        let r = suggestion_fragment(
            text,
            EntityType::Tag,
            EnterType::TAGSWITHGROUP,
            "Λ√∫: Boóks: Harry Potter (books): Professorŝ: Al😀👨‍❤️‍👨u".len(),
        );
        assert_eq!(
            r.meta,
            SuggestionQuery::Tag {
                category: Some("Λ√∫".to_string()),
                group: Some(TagName::from_str("Professorŝ").unwrap())
            }
        );
        assert_eq!(r.fragment, "Al😀👨‍❤️‍👨us");
        assert_eq!(
            r.range,
            "Λ√∫: Boóks: Harry Potter (books): Professorŝ: ".len()
                .."Λ√∫: Boóks: Harry Potter (books): Professorŝ: Al😀👨‍❤️‍👨us".len()
        );
    }
}
