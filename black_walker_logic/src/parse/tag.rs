//! Everything you need to obtain the tags specified in a general tagline.

use std::iter;
use std::iter::FromIterator;

use super::config::EnterType;
use super::{FromCommaSep, DDOT_REG_EXP, SEMICOLON_REG_EXP};
use crate::elements::tag::OccurMode;
use crate::{TagName, TagSpec};

/// How a user specified a tag.
///
/// He may give a category (first `Option<String>`), he must give a name (second `TagSpec`)
/// and he may give a parent (second `Option<TagName>`).
pub type TagDenotation = (Option<String>, TagSpec, Option<TagName>);

/// Returns a list of specifications of all tags and groups appearing
/// in `s`, respecting the parsing mode `enter_type`.
///
/// The first returned list contains the leaf tags, the second one the specified
/// groups.
pub fn specified_tags(s: &str, enter_type: EnterType) -> (Vec<TagDenotation>, Vec<TagDenotation>) {
    let mut res_tag: Vec<TagDenotation> = Vec::new();
    let mut res_group: Vec<TagDenotation> = Vec::new();

    let units: Box<dyn Iterator<Item = &str>> = if enter_type.intersects(EnterType::MULTIPLEUNITS) {
        Box::new(SEMICOLON_REG_EXP.split(s).map(str::trim))
    } else {
        Box::new(iter::once(s.trim()))
    };

    for unit in units {
        let spec_parts: Vec<&str> = DDOT_REG_EXP.split(unit).map(str::trim).collect();
        let mut spec_parts_it = spec_parts.into_iter().rev();
        let tags: Vec<TagSpec>;
        if let Some(tag_spec) = spec_parts_it.next() {
            tags = if enter_type.intersects(EnterType::COMMASEP) {
                Vec::from_comma_sep(tag_spec)
            } else {
                vec![TagSpec::from(tag_spec)]
            }
        } else {
            continue;
        }
        let category = if enter_type.intersects(EnterType::WITHCATEGORY) {
            spec_parts_it.nth_back(0).map(str::to_string)
        } else {
            None
        };
        let group_chain = if enter_type.intersects(EnterType::WITHGROUPS) {
            Vec::from_iter(spec_parts_it.map(|s| TagSpec::from(s).into_base_name()))
        } else {
            Vec::new()
        };

        let mut group_iter = group_chain.into_iter();
        let mut next_group = group_iter.next();
        for tag in tags {
            res_tag.push((category.clone(), tag, next_group.clone()))
        }
        while let Some(current_group) = next_group {
            next_group = group_iter.next();
            res_group.push((
                category.clone(),
                TagSpec::new(current_group, OccurMode::Strong),
                next_group.clone(),
            ));
        }
    }

    (res_tag, res_group)
}

#[cfg(test)]
mod tests {
    use std::iter::FromIterator;
    use std::str::FromStr;

    use super::super::config::EnterType;
    use super::*;

    #[test]
    fn specified_tags() {
        assert_eq!(
            super::specified_tags(
                concat!(
                    "Personen: Books: Harry Potter (books): Professors: ",
                    "Minerva McGonagall, Albus Dumbledore; Orte: Daheim; ",
                    "Weihnachten"
                ),
                EnterType::TAGSWITHGROUP
            ),
            (
                Vec::from_iter(
                    vec![
                        (Some("Personen"), "Minerva McGonagall", Some("Professors")),
                        (Some("Personen"), "Albus Dumbledore", Some("Professors")),
                        (Some("Orte"), "Daheim", None),
                        (None, "Weihnachten", None)
                    ]
                    .into_iter()
                    .map(|(a, b, c)| (
                        a.map(str::to_string),
                        TagSpec::from(b),
                        c.map(|s| TagName::from_str(s).unwrap())
                    ))
                ),
                Vec::from_iter(
                    vec![
                        (Some("Personen"), "Professors", Some("Harry Potter (books)")),
                        (Some("Personen"), "Harry Potter (books)", Some("Books")),
                        (Some("Personen"), "Books", None),
                    ]
                    .into_iter()
                    .map(|(a, b, c)| (
                        a.map(str::to_string),
                        TagSpec::from(b),
                        c.map(|s| TagName::from_str(s).unwrap())
                    ))
                )
            )
        );
    }
}
