//! Traits for converting lists to comma-separated Strings and vice versa

use std::fmt;
use std::iter::FromIterator;

use super::COMMA_REG_EXP;

/// The ability to be represented as a String of comma-separated elements.
pub trait CommaSep {
    /// Represent `self` as a comma-separated list.
    fn comma_sep(self) -> String;
}

impl<T: IntoIterator<Item = Q>, Q: fmt::Display> CommaSep for T {
    fn comma_sep(self) -> String {
        let mut res = String::new();
        let mut it = self.into_iter();
        if let Some(first) = it.next() {
            res.push_str(&first.to_string());
        }
        for it in it {
            res.push_str(", ");
            res.push_str(&it.to_string());
        }
        res
    }
}

/// The ability to be parsed from a String of comma-separated elements.
pub trait FromCommaSep<'a, R> {
    /// Parse `s` as a comma-separated list of elements of type `Self`.
    fn from_comma_sep(s: &'a str) -> Self;
}

impl<'a, R: From<&'a str>, T: FromIterator<R>> FromCommaSep<'a, R> for T {
    fn from_comma_sep(s: &'a str) -> Self {
        T::from_iter(COMMA_REG_EXP.split(s).filter_map(|x| {
            if x.is_empty() {
                None
            } else {
                Some(R::from(x))
            }
        }))
    }
}

#[cfg(test)]
mod tests {
    use super::{CommaSep, FromCommaSep};
    #[test]
    fn to_commasep() {
        let v: Vec<i32> = vec![1, 5, 64, -3];
        assert_eq!(v.iter().comma_sep(), "1, 5, 64, -3".to_string());
    }

    #[test]
    fn from_comma_sep() {
        let s = "simple, nothing, complicated";
        assert_eq!(
            Vec::<String>::from_comma_sep(s),
            vec![
                "simple".to_string(),
                "nothing".to_string(),
                "complicated".to_string()
            ]
        );
        let s = "Herr, der, , Finsternis, ";
        assert_eq!(
            Vec::<String>::from_comma_sep(s),
            vec![
                "Herr".to_string(),
                "der".to_string(),
                "Finsternis".to_string()
            ]
        );
    }
    #[test]
    fn from_comma_sep_empty_sections() {
        let s = ", abc, def,  , l,";
        assert_eq!(Vec::<&str>::from_comma_sep(s), vec!["abc", "def", "l",]);
    }
}
