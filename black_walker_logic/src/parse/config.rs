//! Possible configurations which tell how to parse a String.

use std::convert::TryFrom;

use bitflags::bitflags;

/// The type of elements the String contains: Tags, Entries, Categories ….
#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub enum EntityType {
    /// The text contains tags.
    Tag = 0x00,
    /// The text contains textual entries
    Entry = 0x01,
    /// The text contains tag categories
    TagCategory = 0x02,
    /// The text contains entry categories
    EntryCategory = 0x03,
}

impl TryFrom<u8> for EntityType {
    type Error = String;
    fn try_from(o: u8) -> Result<EntityType, String> {
        match o {
            0x00 => Ok(EntityType::Tag),
            0x01 => Ok(EntityType::Entry),
            0x02 => Ok(EntityType::TagCategory),
            0x03 => Ok(EntityType::EntryCategory),
            x => Err(format!("Invalid discriminant value {}", x)),
        }
    }
}

bitflags! {
    /// These flags in which manner certain elements
    /// are contained in a String -- whether it contains only one single element or
    /// a comma-separated list, etc.
    pub struct EnterType: u8 {
        /// The String contains a single element, nothing special
        const NONE = 0x00;
        /// The String contains a comma-separated list of elements
        const COMMASEP = 0x04;
        /// The String contains a list of elements separated by '&'
        const ANDSEP = 0x08;
        /// The String contains elements preceded by their category.
        const WITHCATEGORY = 0x10;
        /// The String contains chains of grouped elements, each one being a child
        /// of the preceding one.
        const WITHGROUPS = 0x20;
        /// The String contains multiple independent units, separated by a ';'.
        const MULTIPLEUNITS = 0x40;
        /// The String contains a condition.
        const CONDITION = 0x80;
        /// The String contains tags, preceded their category and optional groups.
        const TAGSWITHGROUP = 0x40 | 0x20 | 0x10 | 0x04;
    }
}
