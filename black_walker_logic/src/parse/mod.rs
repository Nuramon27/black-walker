//! Functions and traits related to parsing Strings.

pub mod comma;
pub mod config;
pub mod suggestion;
pub mod tag;

use lazy_static::lazy_static;
use regex::Regex;

pub use comma::{CommaSep, FromCommaSep};
pub use config::{EnterType, EntityType};
pub use suggestion::suggestion_fragment;

lazy_static! {
    pub static ref NUMBERREGEXP: Regex = Regex::new(r"\A[+-]?\d+([.,]\d+)?([eE][+-]\d+)?").unwrap();
}
lazy_static! {
    pub static ref COMMA_REG_EXP: Regex = Regex::new(r"([\s\n]*,[\s\n]*)+").unwrap();
}
lazy_static! {
    pub static ref SEMICOLON_REG_EXP: Regex = Regex::new(r"([\s\n]*;[\s\n]*)+").unwrap();
}
lazy_static! {
    pub static ref AND_REG_EXP: Regex = Regex::new(r"([\s\n]*&[\s\n]*)+").unwrap();
}

lazy_static! {
    pub static ref DDOT_REG_EXP: Regex = Regex::new(r"([\s\n]*:[\s\n]*)+").unwrap();
}

lazy_static! {
    /// A regular expression for items that are separated by two or more semicola.
    static ref DOUBLE_SEMICOLON_REG_EXP : Regex = Regex::new(r"([\s\n]*;{2,}[\s\n]*)+").unwrap();
}

/// Returns whether `fragment` nearly starts with `s`,
/// thit is to say, whether it beginning differs from `s`
/// by at most `allowed_errors`.
///
/// `case_sensitive` controls whether case should be respected at all.
pub fn nearly_starts_with(
    s: &str,
    fragment: &str,
    allowed_errors: u16,
    case_sensitive: bool,
) -> bool {
    let mut errors = 0u16;
    let mut schar = s.chars();
    for frchar in fragment.chars() {
        match schar.next() {
            Some(c) => match case_sensitive {
                true => {
                    if frchar != c {
                        errors += 1
                    }
                }
                false => {
                    if frchar.to_lowercase().to_string() != c.to_lowercase().to_string() {
                        errors += 1
                    }
                }
            },
            // Every character too much in `fragment` counts as one error
            None => errors += 1,
        }
    }

    errors <= allowed_errors
}

#[cfg(test)]
mod test {
    use super::nearly_starts_with;

    #[test]
    fn fn_nearly_starts_with() {
        assert!(nearly_starts_with("Albus", "Alb", 1, true));
        assert!(nearly_starts_with("Albus", "alb", 1, false));
        assert!(nearly_starts_with("Albus", "Alcu", 1, true));
        assert!(nearly_starts_with("Albus", "alcu", 1, false));
        assert!(!nearly_starts_with("Albus", "alc", 1, true));
        assert!(!nearly_starts_with("Albus", "umm", 1, false));

        assert!(nearly_starts_with("Albus", "Albuss", 1, true));
        assert!(!nearly_starts_with("Albus", "Alcuss", 1, true));
    }
}
