//! The suggestion module contains everything related to the search of
//! suggestions.

use std::collections::{HashMap, HashSet};
use std::sync::mpsc::{channel, Receiver, Sender};
use std::sync::{Arc, Weak};

use bitflags::bitflags;
use regex::RegexBuilder;
use unicode_segmentation::UnicodeSegmentation;

use crate::elements::tag;
use crate::model::Query;
use crate::parse::nearly_starts_with;
use crate::read_unstaged;
use crate::settings;
use crate::{Diary, EntrySystem, TagForest, TagName, TagTree};

const INTERRUPT_CHECK_INTERVAL: usize = 32;

/// The result of a search for suggestions.
///
/// Contains the results as a vec of Strings.
#[derive(Debug, Default, Clone, PartialEq, Eq, Hash)]
pub struct SearchResult {
    /// The suggestions found.
    res: Vec<String>,
}

impl SearchResult {
    /// Returns the suggestions that where found.
    pub fn res(&self) -> &Vec<String> {
        &self.res
    }
}

/// The result of a tag search which has yet to be sorted.
#[derive(Debug, Default, Clone, PartialEq, Eq, Hash)]
struct SearchPreResult {
    /// The found tags together with their counts.
    res: Vec<(String, tag::Count)>,
}

impl From<SearchPreResult> for SearchResult {
    fn from(mut other: SearchPreResult) -> Self {
        other
            .res
            .sort_unstable_by(|lhs, rhs| lhs.1.cmp(&rhs.1).reverse().then_with(|| lhs.cmp(rhs)));
        let res: Vec<String> = other.res.into_iter().map(|(s, _)| s).collect();
        SearchResult { res }
    }
}

impl From<SearchResult> for Vec<String> {
    fn from(o: SearchResult) -> Vec<String> {
        o.res
    }
}

bitflags! {
    /// Describes which kind of tags are desired in the search.
    pub struct Allowed: u8 {
        /// A leaf tag, i.e. one without children, is desired
        const LEAF = 0x01;
        /// A tag group, i.e. one with children is desired.
        const GROUP = 0x02;
    }
}

impl Default for Allowed {
    fn default() -> Allowed {
        Allowed::all()
    }
}

/// Searches for tags, fitting to a certain `fragment`.
#[derive(Debug, Clone)]
pub struct SearchTags {
    category: Option<String>,
    fragment: String,
    group: Option<TagName>,
    allowed_flags: Allowed,
    settings: settings::Suggestion,
    ser: Sender<SearchResult>,
    further: Weak<()>,
}

impl SearchTags {
    /// Creates a new `SearchTags` Query which searches
    /// for tags that are compatible with `fragment`
    /// and belong to `group` in `category`.
    ///
    /// The result will be sent down `ser`.
    ///
    /// If `category` is `None`, searches in all categories.
    ///
    /// If `group` is `None`, ignores the group of the tag.
    ///
    /// `allowed_flags` determines for which kind of tags it will search:
    /// Leaf tags, groups, or both. See [`Allowed`] for details.
    ///
    /// Some settings controlling the search (like how long the `fragment`
    /// needs to be at least for any search to be performed) are taken from `settings`.
    pub fn new(
        category: Option<String>,
        fragment: String,
        group: Option<TagName>,
        allowed_flags: Allowed,
        settings: settings::Suggestion,
    ) -> (SearchTags, Receiver<SearchResult>, Arc<()>) {
        let (ser, rec) = channel();
        let further = Arc::new(());
        (
            SearchTags {
                category,
                fragment,
                group,
                allowed_flags,
                settings,
                ser,
                further: Arc::downgrade(&further),
            },
            rec,
            further,
        )
    }

    /// Performs the searchs, returning its result.
    ///
    /// Usually this method will be invoked via the trait method
    /// [`Query::ask`](crate::model::Query::ask).
    #[allow(clippy::blocks_in_if_conditions)]
    fn search_unsorted(&self, tagforest: &HashMap<String, TagTree>) -> SearchPreResult {
        let mut res = Vec::new();

        let grapheme_count = self.fragment.graphemes(true).count();

        let categories = if let Some(category) = &self.category {
            tagforest.get(category).map_or(Vec::new(), |c| vec![c])
        } else {
            tagforest.values().collect()
        };

        let starts_with_regex = if let Ok(ex) =
            RegexBuilder::new(&format!("^{}", regex::escape(&self.fragment)))
                .case_insensitive(true)
                .build()
        {
            ex
        // Should never happen
        } else {
            return SearchPreResult { res };
        };
        for cat in &categories {
            for (i, node) in cat.iter_unordered().enumerate() {
                if starts_with_regex.is_match(node.name().as_str())
                    && (self.allowed_flags.intersects(Allowed::LEAF) || node.children_count() != 0)
                    && (self.allowed_flags.intersects(Allowed::GROUP) || node.children_count() == 0)
                    && self
                        .group
                        .as_ref()
                        .map_or(true, |group| cat.is_indirect_child_of(node.name(), group))
                {
                    res.push((node.name().to_string(), node.count()));
                }
                if i % INTERRUPT_CHECK_INTERVAL == INTERRUPT_CHECK_INTERVAL - 1
                    && self.further.upgrade().is_none()
                {
                    return SearchPreResult::default();
                }
            }
            if self.further.upgrade().is_none() {
                return SearchPreResult::default();
            }
        }

        if !res.is_empty() {
            return SearchPreResult { res };
        }

        if self
            .settings
            .entered_chars_for_tolerance
            .map(|c| grapheme_count >= usize::from(c))
            .unwrap_or(false)
        {
            for cat in &categories {
                for (i, node) in cat.iter_unordered().enumerate() {
                    if nearly_starts_with(
                        node.name().as_str(),
                        &self.fragment,
                        self.settings.allowed_errors,
                        false,
                    ) && (self.allowed_flags.intersects(Allowed::LEAF)
                        || node.children_count() != 0)
                        && (self.allowed_flags.intersects(Allowed::GROUP)
                            || node.children_count() == 0)
                        && self
                            .group
                            .as_ref()
                            .map_or(true, |group| cat.is_indirect_child_of(node.name(), group))
                    {
                        res.push((node.name().to_string(), node.count()));
                    }
                    if i % INTERRUPT_CHECK_INTERVAL == INTERRUPT_CHECK_INTERVAL - 1
                        && self.further.upgrade().is_none()
                    {
                        return SearchPreResult::default();
                    }
                }
                if self.further.upgrade().is_none() {
                    return SearchPreResult::default();
                }
            }

            if !res.is_empty() {
                return SearchPreResult { res };
            }
        }

        let contains_regex = if let Ok(ex) = RegexBuilder::new(&regex::escape(&self.fragment))
            .case_insensitive(true)
            .build()
        {
            ex
        // Should never happen
        } else {
            return SearchPreResult { res };
        };
        if grapheme_count >= usize::from(self.settings.entered_chars_for_interim_suggestions) {
            for cat in &categories {
                for (i, node) in cat.iter_unordered().enumerate() {
                    if contains_regex.is_match(node.name().as_str())
                        && (self.allowed_flags.intersects(Allowed::LEAF)
                            || node.children_count() != 0)
                        && (self.allowed_flags.intersects(Allowed::GROUP)
                            || node.children_count() == 0)
                        && self
                            .group
                            .as_ref()
                            .map_or(true, |group| cat.is_indirect_child_of(node.name(), group))
                    {
                        res.push((node.name().to_string(), node.count()));
                    }
                    if i % INTERRUPT_CHECK_INTERVAL == INTERRUPT_CHECK_INTERVAL - 1
                        && self.further.upgrade().is_none()
                    {
                        return SearchPreResult::default();
                    }
                }
                if self.further.upgrade().is_none() {
                    return SearchPreResult::default();
                }
            }
        }

        SearchPreResult { res }
    }

    pub fn search(&self, tagforest: &HashMap<String, TagTree>) -> SearchResult {
        self.search_unsorted(tagforest).into()
    }
}

impl Query for SearchTags {
    fn ask(&mut self, mut diary: &mut Diary) {
        if let Ok(tagforest) = read_unstaged!(diary, TagForest) {
            match self.ser.send(self.search(&tagforest)) {
                Ok(_) => (),
                // When sending the result fails, this simply means that the receiver has
                // no interest in the result anymore, so this SendError can be ignored.
                Err(_) => (),
            }
        }
    }
}

#[derive(Debug, Clone)]
pub struct SearchEntries {
    category: Option<String>,
    fragment: String,
    settings: settings::Suggestion,
    ser: Sender<SearchResult>,
    further: Weak<()>,
}

impl SearchEntries {
    pub fn new(
        category: Option<String>,
        fragment: String,
        settings: settings::Suggestion,
    ) -> (SearchEntries, Receiver<SearchResult>, Arc<()>) {
        let (ser, rec) = channel();
        let further = Arc::new(());
        (
            SearchEntries {
                category,
                fragment,
                settings,
                ser,
                further: Arc::downgrade(&further),
            },
            rec,
            further,
        )
    }

    pub fn search(&self, entrysets: &EntrySystem) -> SearchResult {
        let mut res = Vec::new();

        let grapheme_count = self.fragment.graphemes(true).count();

        let categories = if let Some(category) = &self.category {
            entrysets.texts(category).map_or(Vec::new(), |c| vec![c])
        } else {
            entrysets.all_texts().values().collect()
        };

        let starts_with_regex = if let Ok(ex) =
            RegexBuilder::new(&format!("^{}", regex::escape(&self.fragment)))
                .case_insensitive(true)
                .build()
        {
            ex
        // Should never happen
        } else {
            return SearchResult { res };
        };
        for cat in &categories {
            for (i, s) in cat.iter().enumerate() {
                if starts_with_regex.is_match(s) {
                    res.push(s.clone());
                }
                if i % INTERRUPT_CHECK_INTERVAL == INTERRUPT_CHECK_INTERVAL - 1
                    && self.further.upgrade().is_none()
                {
                    return SearchResult::default();
                }
            }
            if self.further.upgrade().is_none() {
                return SearchResult::default();
            }
        }

        if !res.is_empty() {
            return SearchResult { res };
        }

        if self
            .settings
            .entered_chars_for_tolerance
            .map(|c| grapheme_count >= usize::from(c))
            .unwrap_or(false)
        {
            for cat in &categories {
                for (i, s) in cat.iter().enumerate() {
                    if nearly_starts_with(s, &self.fragment, self.settings.allowed_errors, false) {
                        res.push(s.clone());
                    }
                    if i % INTERRUPT_CHECK_INTERVAL == INTERRUPT_CHECK_INTERVAL - 1
                        && self.further.upgrade().is_none()
                    {
                        return SearchResult::default();
                    }
                }
                if self.further.upgrade().is_none() {
                    return SearchResult::default();
                }
            }

            if !res.is_empty() {
                return SearchResult { res };
            }
        }

        let contains_regex = if let Ok(ex) = RegexBuilder::new(&regex::escape(&self.fragment))
            .case_insensitive(true)
            .build()
        {
            ex
        // Should never happen
        } else {
            return SearchResult { res };
        };
        if grapheme_count >= usize::from(self.settings.entered_chars_for_interim_suggestions) {
            for cat in &categories {
                for (i, s) in cat.iter().enumerate() {
                    if contains_regex.is_match(s) {
                        res.push(s.clone());
                    }
                    if i % INTERRUPT_CHECK_INTERVAL == INTERRUPT_CHECK_INTERVAL - 1
                        && self.further.upgrade().is_none()
                    {
                        return SearchResult::default();
                    }
                }
                if self.further.upgrade().is_none() {
                    return SearchResult::default();
                }
            }
        }

        SearchResult { res }
    }
}

impl Query for SearchEntries {
    #[allow(unused_must_use)]
    fn ask(&mut self, mut diary: &mut Diary) {
        if let Ok(entrysets) = read_unstaged!(diary, EntrySystem) {
            let mut res = self.search(&entrysets);
            res.res.sort();
            self.ser.send(res);
        }
    }
}

/// Searches for elements of the Iterator `it` which are compatible with `fragment`.
///
/// Some settings controlling the search (like how long the `fragment`
/// needs to be at least for any search to be performed) are taken from `settings`.
pub fn search_keys<'a, I: Clone + Iterator<Item = &'a String>>(
    fragment: &str,
    settings: settings::Suggestion,
    it: I,
    further: &Weak<()>,
) -> SearchResult {
    let mut res = Vec::new();

    let grapheme_count = fragment.graphemes(true).count();

    let starts_with_regex = if let Ok(ex) =
        RegexBuilder::new(&format!("^{}", regex::escape(fragment)))
            .case_insensitive(true)
            .build()
    {
        ex
    // Should never happen
    } else {
        return SearchResult { res };
    };
    for (i, k) in it.clone().enumerate() {
        if starts_with_regex.is_match(k) {
            res.push(k.clone());
        }
        if i % INTERRUPT_CHECK_INTERVAL == INTERRUPT_CHECK_INTERVAL - 1
            && further.upgrade().is_none()
        {
            return SearchResult::default();
        }
    }

    if !res.is_empty() {
        return SearchResult { res };
    }

    if settings
        .entered_chars_for_tolerance
        .map(|c| grapheme_count >= usize::from(c))
        .unwrap_or(false)
    {
        for (i, k) in it.clone().enumerate() {
            if nearly_starts_with(k, fragment, settings.allowed_errors, false) {
                res.push(k.clone());
            }
            if i % INTERRUPT_CHECK_INTERVAL == INTERRUPT_CHECK_INTERVAL - 1
                && further.upgrade().is_none()
            {
                return SearchResult::default();
            }
        }

        if !res.is_empty() {
            return SearchResult { res };
        }
    }

    let contains_regex = if let Ok(ex) = RegexBuilder::new(&format!("^{}", regex::escape(fragment)))
        .case_insensitive(true)
        .build()
    {
        ex
    // Should never happen
    } else {
        return SearchResult { res };
    };
    if grapheme_count >= usize::from(settings.entered_chars_for_interim_suggestions) {
        for (i, k) in it.enumerate() {
            if contains_regex.is_match(k) {
                res.push(k.clone());
            }
            if i % INTERRUPT_CHECK_INTERVAL == INTERRUPT_CHECK_INTERVAL - 1
                && further.upgrade().is_none()
            {
                return SearchResult::default();
            }
        }
    }

    SearchResult { res }
}

#[derive(Debug, Clone)]
pub struct SearchTagCategories {
    fragment: String,
    settings: settings::Suggestion,
    ser: Sender<SearchResult>,
    further: Weak<()>,
}

impl SearchTagCategories {
    pub fn new(
        fragment: String,
        settings: settings::Suggestion,
    ) -> (SearchTagCategories, Receiver<SearchResult>, Arc<()>) {
        let (ser, rec) = channel();
        let further = Arc::new(());
        (
            SearchTagCategories {
                fragment,
                settings,
                ser,
                further: Arc::downgrade(&further),
            },
            rec,
            further,
        )
    }
}

impl Query for SearchTagCategories {
    #[allow(unused_must_use)]
    fn ask(&mut self, mut diary: &mut Diary) {
        if let Ok(tagforest) = read_unstaged!(diary, TagForest) {
            let mut res = search_keys(
                &self.fragment,
                self.settings,
                tagforest.keys(),
                &self.further,
            );
            res.res.sort_unstable_by(|lhs, rhs| {
                tagforest
                    .get(lhs)
                    .map(TagTree::len)
                    .unwrap_or_default()
                    .cmp(&tagforest.get(rhs).map(TagTree::len).unwrap_or_default())
                    .then_with(|| lhs.cmp(rhs))
            });
            self.ser.send(res);
        }
    }
}

#[derive(Debug, Clone)]
pub struct SearchEntryCategories {
    fragment: String,
    settings: settings::Suggestion,
    ser: Sender<SearchResult>,
    further: Weak<()>,
}

impl SearchEntryCategories {
    pub fn new(
        fragment: String,
        settings: settings::Suggestion,
    ) -> (SearchEntryCategories, Receiver<SearchResult>, Arc<()>) {
        let (ser, rec) = channel();
        let further = Arc::new(());
        (
            SearchEntryCategories {
                fragment,
                settings,
                ser,
                further: Arc::downgrade(&further),
            },
            rec,
            further,
        )
    }
}

impl Query for SearchEntryCategories {
    #[allow(unused_must_use)]
    fn ask(&mut self, mut diary: &mut Diary) {
        if let Ok(entrysets) = read_unstaged!(diary, EntrySystem) {
            let mut res = search_keys(
                &self.fragment,
                self.settings,
                entrysets.all_texts().keys(),
                &self.further,
            );
            res.res.sort_unstable_by(|lhs, rhs| {
                entrysets
                    .texts(lhs)
                    .map(HashSet::len)
                    .unwrap_or_default()
                    .cmp(&entrysets.texts(rhs).map(HashSet::len).unwrap_or_default())
                    .then_with(|| lhs.cmp(rhs))
            });
            self.ser.send(res);
        }
    }
}

#[cfg(test)]
mod tests {
    use std::iter::FromIterator;
    use std::str::FromStr;

    use crate::elements::tag::names::collation::init_collator;

    use super::super::tests::standard_diary;
    use super::*;
    #[test]
    fn allowed() {
        let a = Allowed::all();
        assert!(a.intersects(Allowed::LEAF));
        assert!(a.intersects(Allowed::GROUP));
    }

    #[test]
    fn search_tags() {
        init_collator();
        let settings = settings::Suggestion::default();
        let d = standard_diary();

        let (search, _, _further) = SearchTags::new(
            Some("Personen".to_string()),
            "H".to_string(),
            None,
            Allowed::all(),
            settings,
        );
        let mut res1 = SearchResult::from(search.search(&d.tag.read().unwrap())).res;
        res1.sort();
        assert_eq!(
            Vec::from_iter(res1.iter().map(|x| &x[..])),
            vec!["Harry Potter", "Harry Potter (books)", "Heino"]
        );

        let (search, _, _further) = SearchTags::new(
            Some("Personen".to_string()),
            "Harri Potter".to_string(),
            None,
            Allowed::all(),
            settings,
        );
        let mut res1 = SearchResult::from(search.search(&d.tag.read().unwrap())).res;
        res1.sort();
        assert_eq!(
            Vec::from_iter(res1.iter().map(|x| &x[..])),
            vec!["Harry Potter", "Harry Potter (books)"]
        );

        let (search, _, _further) = SearchTags::new(
            Some("Personen".to_string()),
            "Potter".to_string(),
            None,
            Allowed::all(),
            settings,
        );
        let mut res1 = SearchResult::from(search.search(&d.tag.read().unwrap())).res;
        res1.sort();
        assert_eq!(
            Vec::from_iter(res1.iter().map(|x| &x[..])),
            vec!["Harry Potter", "Harry Potter (books)"]
        );

        let (search, _, _further) = SearchTags::new(
            Some("Personen".to_string()),
            "Albus".to_string(),
            Some(TagName::from_str("Books").unwrap()),
            Allowed::all(),
            settings,
        );
        let mut res1 = SearchResult::from(search.search(&d.tag.read().unwrap())).res;
        res1.sort();
        assert_eq!(
            Vec::from_iter(res1.iter().map(|x| &x[..])),
            vec!["Albus Dumbledore"]
        );
    }

    #[test]
    fn search_tags_case_insensitive() {
        init_collator();
        let settings = settings::Suggestion::default();
        let d = standard_diary();

        let (search, _, _further) = SearchTags::new(
            Some("Personen".to_string()),
            "alBus".to_string(),
            None,
            Allowed::all(),
            settings,
        );
        let mut res1 = SearchResult::from(search.search(&d.tag.read().unwrap())).res;
        res1.sort();
        assert!(Vec::from_iter(res1.iter().map(|x| &x[..])).contains(&"Albus Dumbledore"),);
    }

    #[test]
    fn search_tags_interrupted() {
        init_collator();
        let settings = settings::Suggestion::default();
        let d = standard_diary();

        let (search, _, further) = SearchTags::new(
            Some("Personen".to_string()),
            "Albus".to_string(),
            None,
            Allowed::all(),
            settings,
        );
        drop(further);
        let mut res1 = SearchResult::from(search.search(&d.tag.read().unwrap())).res;
        res1.sort();
        assert!(res1.is_empty(),);
    }
}
