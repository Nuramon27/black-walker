//! Dummy version of [`search`] for compiling without the "search"-feature
use std::sync::mpsc::{channel, Receiver};
use std::sync::Arc;

use crate::model::Query;
use crate::Diary;
use crate::{DreamID, TagName};

#[derive(Debug, Clone)]
pub struct SearchDreamsWithTag {}

impl SearchDreamsWithTag {
    /// Creates a new query searching for dreams containing `tagname` in `category`.
    ///
    /// It returns the created `SearchDreamsWithTag` together with
    /// the `Receiver` through which the result will be available.
    pub fn new(_category: String, _tagname: TagName) -> (Self, Receiver<Vec<DreamID>>, Arc<()>) {
        let (_ser, rec) = channel();
        let further = Arc::new(());
        (
            Self {},
            rec,
            further,
        )
    }
}

impl Query for SearchDreamsWithTag {
    fn ask(&mut self, _diary: &mut Diary) {}
}

#[derive(Debug, Clone)]
pub struct Search {}

impl Search {
    pub fn new(_condition: String) -> (Search, Receiver<Vec<DreamID>>, Arc<()>) {
        let (_ser, rec) = channel();
        let further = Arc::new(());
        (
            Self {},
            rec,
            further,
        )
    }
}

impl Query for Search {
    fn ask(&mut self, _diary: &mut Diary) {}
}
