use std::collections::btree_map::Entry::{Occupied, Vacant};
use std::fmt;

use chrono::NaiveDate;

use super::core::{Applyable, CmdError};
use crate::model::{DreamListModel, Model, NotifyBegin, NotifyEnd, ToNotify};
use crate::sync::prelude::*;
use crate::{lock, stage_diary};
use crate::{Diary, DreamList, Night, NightList};

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct SetNight {
    date: NaiveDate,
    night: Option<Night>,
}

impl SetNight {
    /// Creates a command that sets night `date` to `night`,
    /// creating it if necessary.
    pub fn new(date: NaiveDate, night: Night) -> Self {
        Self {
            date,
            night: Some(night),
        }
    }
    /// Creates a command that removes night `date`.
    pub fn remove_night(date: NaiveDate) -> Self {
        Self { date, night: None }
    }
}

impl fmt::Display for SetNight {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        if self.night.is_some() {
            write!(f, "Set night {}", self.date)
        } else {
            write!(f, "Remove night {}", self.date)
        }
    }
}

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct SetNightNotifier {
    date: NaiveDate,
    night: Option<Night>,
    /// Contains whether the night did previously exist, e.g.
    /// in the dreamlist.
    night_new: bool,
}

impl From<SetNight> for SetNightNotifier {
    fn from(other: SetNight) -> Self {
        SetNightNotifier {
            date: other.date,
            night: other.night,
            night_new: false,
        }
    }
}

impl Applyable for SetNight {
    type To = Diary;
    type Res = SetNightNotifier;
    type Opposite = SetNight;
    fn apply(mut self, mut diary: &mut Diary) -> Result<(Self::Res, Self::Opposite), CmdError> {
        let mut res = SetNightNotifier::from(self.clone());
        let mut stage = stage_diary!(diary; DL; NL);
        if let (Ok(dreamlist), Ok(mut nightlist)) = lock!(stage; DreamList; NightList) {
            match nightlist.entry(self.date.into()) {
                Occupied(mut old_night) => {
                    if let Some(new_night) = &mut self.night {
                        std::mem::swap(old_night.get_mut(), new_night);
                        res.night_new = false;
                    } else {
                        old_night.remove();
                    }
                }
                Vacant(entry) => {
                    if let Some(new_night) = self.night.take() {
                        entry.insert(new_night);
                        res.night_new = dreamlist.dream_count(self.date) == 0;
                    }
                }
            }
            Ok((res, self))
        } else {
            Err(CmdError::Disconnected)
        }
    }
}

impl<M: Model> NotifyBegin<M> for SetNight {
    fn whom_begin(&self) -> ToNotify {
        ToNotify::empty()
    }
}

impl<M: Model> NotifyEnd<M> for SetNightNotifier {
    fn whom_end(&self) -> ToNotify {
        ToNotify::DREAMLIST
    }
    fn notify_dreamlist_end(&self, listener: &mut M::DreamListModel) {
        if self.night_new {
            listener.begin_create_night_pure(self.date);
            listener.end_create_night_pure(self.date);
        } else {
            listener.begin_remove_night_information(self.date);
            listener.end_remove_night_information(self.date);
        }
    }
}
