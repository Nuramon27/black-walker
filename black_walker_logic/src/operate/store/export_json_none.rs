//! Dummy version of [`export_json`] for compiling without the "export_json"-feature

use std::sync::mpsc::{channel, Receiver, Sender};
use std::sync::{Arc, Weak};
use std::path::PathBuf;

use crate::model::Query;
use crate::Diary;
use crate::store::StoreError;

#[derive(Debug, Clone)]
pub struct ExportJson {
}

impl ExportJson {
    pub fn new(_path: PathBuf) -> (Self, Receiver<Result<(), StoreError>>, Arc<()>) {
        let (_ser, rec) = channel();
        let further = Arc::new(());
        (Self {}, rec, further)
    }
}

impl Query for ExportJson {
    fn ask(&mut self, diary: &mut Diary) {}
}