use std::sync::mpsc::{channel, Receiver, Sender};
use std::sync::{Arc, Weak};
use std::path::PathBuf;

use crate::model::Query;
use crate::Diary;
use crate::store::StoreError;
use crate::store::export_json::export_json;

/// Stores the diary in json-format to [`path`](ExportJson::path).
#[derive(Debug, Clone)]
pub struct ExportJson {
    path: PathBuf,
    /// The sender through which the result will be sent.
    ser: Sender<Result<(), StoreError>>,
    _further: Weak<()>,
}

impl ExportJson {
    /// Creates a new query exporting the diary to `path` in json-format.
    pub fn new(path: PathBuf) -> (Self, Receiver<Result<(), StoreError>>, Arc<()>) {
        let (ser, rec) = channel();
        let further = Arc::new(());
        (ExportJson{
            path,
            ser,
            _further: Arc::downgrade(&further),
        }, rec, further)
    }
}

impl Query for ExportJson {
    fn ask(&mut self, diary: &mut Diary) {
        let res = export_json(diary, &self.path);
        match self.ser.send(res) {
            Ok(_) => (),
            // When sending the result fails, this simply means that the receiver has
            // no interest in the result anymore, so this SendError can be ignored.
            Err(_) => (),
        }
    }
}