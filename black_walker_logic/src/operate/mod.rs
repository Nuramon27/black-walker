//! The operate module is contains all [commands](undo::Command) that can
//! be executed in Black Walker.
//!
//! Constructors for [Queries](crate::model::Query) in this module usually
//! return tuples of three objects: The actual Query, a receiver
//! on which the result may be expected, and a `further`:
//! This is an `Arc<()>` to which the Query holds a corresponding
//! `Weak<()>`. It regularly checks whether its `further` is still
//! upgradeable and aborts the search if it is not. So the returned
//! `further` can be used to indicate that the result is not awaited
//! anymor by simply dropping it.
//!
//! On the contrary this means that it must still be held
//! as long as the result of the query is awaited.
//!
//! ## Attention
//!
//! Queries do not guarantee that they send any result back. So
//! a sender of a query must not wait infinitely for it's result.

pub mod all;
pub mod core;
pub mod dream;
pub mod misc;
pub mod night;
#[cfg(feature = "search")]
pub mod search;
pub mod store;
#[cfg(not(feature = "search"))]
#[doc(hidden)]
pub mod search_none;
#[cfg(not(feature = "search"))]
pub use search_none as search;
pub mod suggestion;
pub mod tag;

pub mod tests;
