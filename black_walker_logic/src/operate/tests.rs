use std::str::FromStr;

use chrono::{NaiveDate, NaiveTime};

use crate::operate::{dream::*, tag::*};
use crate::{Diary, Dream, Note, TagName, TagSpec};

use super::core::Applyable;

/// Emits two lists of commands that setup a diary containing some example
/// tags and trees.
pub fn standard_diary_cmd() -> (Vec<CreateTag>, Vec<CreateDream>) {
    let mut dreams = vec![
        Dream::from_oblig("Flying".to_string(), "Lucid Dream".to_string()),
        Dream::from_oblig("Albus Dumbledore".to_string(), "Ordinary Dream".to_string()),
        Dream::from_oblig("Landscape".to_string(), "Ordinary Dream".to_string()),
        Dream::from_oblig(
            "Game of Thrones stuff".to_string(),
            "Ordinary Dream".to_string(),
        ),
        Dream::from_oblig("Bat".to_string(), "Ordinary Dream".to_string()),
        Dream::from_oblig("Uncle Sam".to_string(), "Ordinary Dream".to_string()),
    ];

    dreams[0].add_tags(
        "Personen",
        vec![
            TagSpec::from_str("Michael Jackson").unwrap(),
            TagSpec::from_str("God").unwrap(),
        ],
    );
    dreams[0].set_entry_text("Technique", "DILD".to_string());
    dreams[0].set_entry_time("Falling asleep", NaiveTime::from_hms(4, 30, 0));
    dreams[0].set_entry_time("Waking up", NaiveTime::from_hms(6, 30, 0));
    dreams[0].set_entry_uint("Intensity", 2u32);
    dreams[0].text = "Das war wohl nichts. Hätte ich doch nur aufgepasst.".to_string();

    dreams[1].add_tags(
        "Personen",
        vec![
            TagSpec::from_str("Albus Dumbledore").unwrap(),
            TagSpec::from_str("Lord Voldemort").unwrap(),
            TagSpec::from_str("Harry Potter").unwrap(),
            TagSpec::from_str("Darth Vader").unwrap(),
        ],
    );
    dreams[1].set_entry_time("Falling asleep", NaiveTime::from_hms(23, 10, 0));
    dreams[1].set_entry_time("Waking up", NaiveTime::from_hms(8, 30, 0));
    dreams[1].set_entry_uint("Intensity", 4u32);
    dreams[1].text = "Albus Dumbledore does something.".to_owned();
    dreams[1].notes.insert(
        NaiveDate::from_ymd(2020, 12, 31),
        Note {
            time: NaiveTime::from_hms(8, 0, 0),
            place: "Frankfurt".to_string(),
            note: "Albus Dumbledore is actually Gandalf.".to_string(),
        },
    );
    dreams[1].notes.insert(
        NaiveDate::from_ymd(2021, 01, 01),
        Note {
            time: NaiveTime::from_hms(14, 30, 0),
            place: "Frankfurt".to_string(),
            note: "What I said last year is wrong.".to_string(),
        },
    );

    dreams[2].add_tags("Personen", vec![TagSpec::from_str("Heino").unwrap()]);
    dreams[2].add_tags("Personen", vec![TagSpec::from_str("Star Wars").unwrap()]);
    dreams[2].add_tags("Orte", vec![TagSpec::from_str("Mountains").unwrap()]);
    dreams[2].set_entry_time("Falling asleep", NaiveTime::from_hms(23, 45, 0));
    dreams[2].set_entry_time("Waking up", NaiveTime::from_hms(6, 00, 0));
    dreams[2].set_entry_uint("Intensity", 1u32);

    dreams[3].add_tags(
        "Personen",
        vec![
            TagSpec::from_str("Darth Vader*").unwrap(),
            TagSpec::from_str("Albus Dumbledore").unwrap(),
            TagSpec::from_str("Luke Skywalker").unwrap(),
        ],
    );
    dreams[3].set_entry_time("Falling asleep", NaiveTime::from_hms(3, 5, 0));
    dreams[3].set_entry_time("Waking up", NaiveTime::from_hms(5, 50, 0));
    dreams[3].set_entry_uint("Intensity", 2u32);

    dreams[4].add_tags("Personen", vec![TagSpec::from_str("Harry Potter").unwrap()]);
    dreams[4].set_entry_time("Falling asleep", NaiveTime::from_hms(4, 40, 0));
    dreams[4].set_entry_time("Waking up", NaiveTime::from_hms(7, 50, 0));
    dreams[4].set_entry_uint("Intensity", 5u32);

    dreams[5].add_tags(
        "Uncle Sam",
        vec![
            TagSpec::from_str("Uncle Sam").unwrap(),
            TagSpec::from_str("Luke Skywalker").unwrap(),
        ],
    );
    dreams[5].set_entry_time("Falling asleep", NaiveTime::from_hms(5, 10, 0));
    dreams[5].set_entry_time("Waking up", NaiveTime::from_hms(6, 45, 0));
    dreams[5].set_entry_uint("Intensity", 5u32);
    dreams[5].text = "Uncle Sam is a good man.".to_string();

    let dates = vec![
        NaiveDate::from_ymd(1054, 07, 06),
        NaiveDate::from_ymd(1054, 07, 05),
        NaiveDate::from_ymd(1054, 07, 05),
        NaiveDate::from_ymd(1054, 07, 05),
        NaiveDate::from_ymd(1054, 07, 04),
        NaiveDate::from_ymd(1054, 07, 04),
    ];

    let t = "Personen".to_string();
    let mut tagcmd = vec![
        CreateTag::new_at_root(t.clone(), TagName::from_str("Movies").unwrap()),
        CreateTag::new_at_root(t.clone(), TagName::from_str("Books").unwrap()),
        CreateTag::new(
            t.clone(),
            TagName::from_str("Star Wars").unwrap(),
            TagName::from_str("Movies").unwrap(),
        ),
        CreateTag::new(
            t.clone(),
            TagName::from_str("Yoda").unwrap(),
            TagName::from_str("Star Wars").unwrap(),
        ),
        CreateTag::new(
            t.clone(),
            TagName::from_str("Luke Skywalker").unwrap(),
            TagName::from_str("Star Wars").unwrap(),
        ),
        CreateTag::new(
            t.clone(),
            TagName::from_str("Darth Vader").unwrap(),
            TagName::from_str("Star Wars").unwrap(),
        ),
        CreateTag::new(
            t.clone(),
            TagName::from_str("Adventures of Brigsby Bear").unwrap(),
            TagName::from_str("Movies").unwrap(),
        ),
        CreateTag::new(
            t.clone(),
            TagName::from_str("Forest Gump (movie)").unwrap(),
            TagName::from_str("Movies").unwrap(),
        ),
        CreateTag::new(
            t.clone(),
            TagName::from_str("Forest Gump").unwrap(),
            TagName::from_str("Forest Gump (movie)").unwrap(),
        ),
        CreateTag::new(
            t.clone(),
            TagName::from_str("Jenny").unwrap(),
            TagName::from_str("Forest Gump (movie)").unwrap(),
        ),
        CreateTag::new(
            t.clone(),
            TagName::from_str("Harry Potter (books)").unwrap(),
            TagName::from_str("Books").unwrap(),
        ),
        CreateTag::new(
            t.clone(),
            TagName::from_str("Albus Dumbledore").unwrap(),
            TagName::from_str("Harry Potter (books)").unwrap(),
        ),
        CreateTag::new(
            t.clone(),
            TagName::from_str("Lord Voldemort").unwrap(),
            TagName::from_str("Harry Potter (books)").unwrap(),
        ),
        CreateTag::new(
            t.clone(),
            TagName::from_str("Harry Potter").unwrap(),
            TagName::from_str("Harry Potter (books)").unwrap(),
        ),
        CreateTag::new_at_root(t.clone(), TagName::from_str("Prominents").unwrap()),
        CreateTag::new(
            t.clone(),
            TagName::from_str("Heino").unwrap(),
            TagName::from_str("Prominents").unwrap(),
        ),
        CreateTag::new(
            t.clone(),
            TagName::from_str("Michael Jackson").unwrap(),
            TagName::from_str("Prominents").unwrap(),
        ),
        CreateTag::new_at_root(t.clone(), TagName::from_str("God").unwrap()),
        CreateTag::new_at_root(t.clone(), TagName::from_str("Uncle Sam").unwrap()),
        CreateTag::new_at_root(t, TagName::from_str("Unused Tag").unwrap()),
    ];

    let t = "Orte".to_string();
    tagcmd.push(CreateTag::new_at_root(
        t,
        TagName::from_str("Mountains").unwrap(),
    ));
    let mut dreamcmd = Vec::new();
    for (dream, date) in dreams.into_iter().zip(dates.into_iter()) {
        dreamcmd.push(CreateDream::new(dream, date));
    }

    (tagcmd, dreamcmd)
}

pub fn standard_diary() -> Diary {
    let mut diary = Diary::new();
    let (tagcmd, dreamcmd) = standard_diary_cmd();

    for it in tagcmd {
        it.apply(&mut diary).unwrap();
    }
    for it in dreamcmd {
        it.apply(&mut diary).unwrap();
    }

    /*{
        let mut stage = stage_diary!(diary; DL; ENTR);
        if let (Ok(dreamlist), Ok(mut entrysystem)) = lock!(stage; DreamList; EntrySystem) {
            entrysystem.add_all_entries(&dreamlist)
        } else { panic!("Mutexes are poisoned.") }
    }*/

    diary
}
