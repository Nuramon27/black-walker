//! Miscellaneous Commands that fit nowhere else.

use std::sync::mpsc::Sender;

use crate::model::Query;
use crate::Diary;

/// A Command that does nothing.
/// This simply works as a means of synchronization between
/// the GUI and the Worker thread.
///
/// When executed, it sends a unit down [`ser`](NoOp::ser).
#[derive(Debug, Clone)]
pub struct NoOp {
    pub ser: Sender<()>,
}

impl Query for NoOp {
    fn ask(&mut self, _diary: &mut Diary) {
        self.ser.send(()).expect("GUI thread has hung up.");
    }
}
