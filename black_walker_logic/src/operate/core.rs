//! Several helper traits and structs that simplify implementing the traits
//! [`Command`](undo::Command) [`NotifyBegin`](crate::model::NotifyBegin)
//! and [`NotifyEnd`](crate::model::NotifyEnd).
//!
//! The operate::core module is mainly built around the [`Undoable`] struct
//! which stores a command that can reside in an _applied_ or an _undone_ state.

use std::fmt::{Debug, Display, Formatter};

use crate::feature::undo::Command;

use crate::model::{Model, NotifyBegin, NotifyEnd, ToNotify};

#[cfg(not(feature = "search"))]
#[doc(hidden)]
pub mod termite {
    pub type Error = std::convert::Infallible;
}

/// A command that can be applied to a state [`To`](Applyable::To) and
/// has an [`Opposite`](Applyable::Opposite) command that undoes it.
pub trait Applyable {
    /// The 'state' to which the command will be applied.
    ///
    /// In Black Walker, this is the `Diary`.
    type To;
    /// A command that undoes `Self`.
    ///
    /// It is porposefully not demanded that the opposite of the opposite is `Self`,
    /// because one might want the opposite to be a more _general_ command,
    /// like in the most extreme case something that just stores the state before execution,
    /// which will not have something as specific as 'CreateDream' as its opposite.
    type Opposite: Applyable<To = Self::To>;
    type Res;
    /// Apply the command to `To`.
    ///
    /// The command shall return its opposite that will bring `To` to the state it had
    /// before execution. It his, however, not demanded that `Opposite` undoes
    /// `Self` from any arbitrary state of `To` (whatever that would mean),
    /// but only from the exact state `To` has after applying this command.
    fn apply(self, reciever: &mut Self::To) -> Result<(Self::Res, Self::Opposite), CmdError>;
}

/*/// A simplified version of [`Notify`](crate::worker::Notify) that only
/// has [`Applyable`] as supertrait.
///
/// It still represents a command that is able to inform severl [Models](crate::worker::Model)
/// about the beginning and end of its own execution.
///
/// Usually you will implement `Applyable` and `NotifyPure` for a command and
/// its [`Opposite`](Applyable::Opposite) and then use the corresponding
/// [`Undoable`], on which `Notify` and `Command` will be implemented.
pub trait NotifyPure<M: Model>: Applyable {
    /// A set of flags indicating which models need to be notified about
    /// the beginning of execution of this command
    fn whom_begin(&self) -> ToNotify;
    /// A set of flags indicating which models need to be notified about
    /// the end of execution of this command
    fn whom_end(&self) -> ToNotify {
        self.whom_begin()
    }
    /// Notify the DreamListModel of the beginning of execution.
    fn notify_dreamlist_begin(&self, _listener: &mut M::DreamListModel) -> Result<(), ()> {
        Ok(())
    }
    /// Notify the DreamListModel of the end of execution.
    fn notify_dreamlist_end(&self, _listener: &mut M::DreamListModel) -> Result<(), ()> {
        Ok(())
    }
    /// Notify the DreamForm of the beginning of execution.
    fn notify_dreamform_begin(&self, _listener: &mut M::DreamForm) -> Result<(), ()> {
        Ok(())
    }
    /// Notify the DreamForm of the end of execution.
    fn notify_dreamform_end(&self, _listener: &mut M::DreamForm) -> Result<(), ()> {
        Ok(())
    }
    /// Notify the TagTree of the beginning of execution.
    fn notify_tagtree_begin(&self, _listener: &mut M::TagTree) -> Result<(), ()> {
        Ok(())
    }
    /// Notify the TagTree of the end of execution.
    fn notify_tagtree_end(&self, _listener: &mut M::TagTree) -> Result<(), ()> {
        Ok(())
    }
}*/

/// A Command that can reside in two states: A state in which it can
/// be applied and a state in which it can be undone.
///
/// For some commands, the information necessary to undo it is not available
/// until it is actually applied. For example, when creating a dream, its
/// id is not known until the dream has actually been created,
/// but the exact id is necessary for undoing it, i. e. removing the correct
/// dream. In the same way, one might not want to store the complete
/// information necessary for applying it a command (like the whole dream to create)
/// after it has been applied.
///
/// Together with the [`Applyable`] trait, the `Undoable` enum helps with these cases.
/// It is a [`Command`](undo::Command) that can contain either the applyable
/// variant of a command or its undoable variant. Both variants must only implement
/// [`Applyable`] which requires one function: how to apply it.
///
/// When this command is applied or undone, it automatically changes its variant.
///
/// ## Usage
///
/// In order to implement an undoable command, create a struct `A` that implements
/// the [`Applyable`] and [`NotifyBegin`](crate::model::NotifyBegin). In doing so you need to
/// specify an [`Opposite`](Applyable::Opposite)
/// command `U` that undoes the original command. Furthermore, you need
/// to specify a [`Res`](Applyable::Res) which will be used for notification
/// about the completion of the operation. `Res` should implement
/// [`NotifyEnd`](crate::model::NotifyEnd).
///
/// Then the traits [`undo::Command`] and [`crate::model::NotifyBegin`]
/// will be automatically implemented for `Undoable<A, U>`. On applying it, it will return
/// the [`Res`](Applyable::Res), so that one can be used for notification.
#[derive(Debug, Clone, PartialEq, Eq)]
pub enum Undoable<A, U> {
    /// An `Undoable` in a state where it can be applied.
    Apply(A),
    /// An `Undoable` in a state where it can be undone
    Undo(U),
    /// An interim variant that does only occur during execution.
    ///
    /// When executing a command, it first must be taken out of the `Undoable`,
    /// but the `Undoable` cannot be left empty, so a third variart is needed
    /// for representing this 'empty' states.
    ///
    /// No public method on `Undoable` will ever leave it in this state.
    Neither,
}
pub use Undoable::{Apply, Neither, Undo};

impl<A, U> Undoable<A, U> {
    /// Move out the underlying command if the `Undoable` is in
    /// applyable state.
    fn take_apply(&mut self) -> Option<A> {
        let res = std::mem::replace(self, Neither);
        if let Apply(res) = res {
            Some(res)
        } else {
            None
        }
    }

    /// Move out the underlying command if the `Undoable` is in
    /// undoable state.
    fn take_undo(&mut self) -> Option<U> {
        let res = std::mem::replace(self, Neither);
        if let Undo(res) = res {
            Some(res)
        } else {
            None
        }
    }
}

impl<A, U, T> From<A> for Undoable<A, U>
where
    A: Applyable<To = T, Opposite = U>,
    U: Applyable<To = T, Opposite = A>,
{
    fn from(o: A) -> Undoable<A, U> {
        Apply(o)
    }
}

// Implement `Command` for an Undoable where A is the Opposite of U and vice versa.
impl<T, A, U, RA, RU> Command<T, CmdError, Undoable<RA, RU>> for Undoable<A, U>
where
    A: Applyable<To = T, Opposite = U, Res = RA> + Debug + Send + Sync,
    U: Applyable<To = T, Opposite = A, Res = RU> + Debug + Send + Sync,
{
    fn apply(&mut self, reciever: &mut T) -> Result<Undoable<RA, RU>, CmdError> {
        if let Some(cmd) = self.take_apply() {
            cmd.apply(reciever).map(|(res, op)| -> Undoable<RA, RU> {
                *self = Undo(op);
                Undoable::Apply(res)
            })
        } else {
            Err(CmdError::CannotApply)
        }
    }
    fn undo(&mut self, reciever: &mut T) -> Result<Undoable<RA, RU>, CmdError> {
        if let Some(cmd) = self.take_undo() {
            cmd.apply(reciever).map(|(res, op)| -> Undoable<RA, RU> {
                *self = Apply(op);
                Undoable::Undo(res)
            })
        } else {
            Err(CmdError::CannotUndo)
        }
    }
}

impl<T, A, U, M: Model, RA, RU> Command<T, CmdError, Box<dyn NotifyEnd<M>>> for Undoable<A, U>
where
    A: Applyable<To = T, Opposite = U, Res = RA> + Debug + Send + Sync,
    U: Applyable<To = T, Opposite = A, Res = RU> + Debug + Send + Sync,
    RA: NotifyEnd<M> + Clone + Debug + Send + Sync + 'static,
    RU: NotifyEnd<M> + Clone + Debug + Send + Sync + 'static,
{
    fn apply(&mut self, reciever: &mut T) -> Result<Box<dyn NotifyEnd<M>>, CmdError> {
        Command::<T, CmdError, Undoable<RA, RU>>::apply(self, reciever)
            .map(|res| -> Box<dyn NotifyEnd<M>> { Box::new(res) })
    }
    fn undo(&mut self, reciever: &mut T) -> Result<Box<dyn NotifyEnd<M>>, CmdError> {
        Command::<T, CmdError, Undoable<RA, RU>>::undo(self, reciever)
            .map(|res| -> Box<dyn NotifyEnd<M>> { Box::new(res) })
    }
}

impl<A, U, M: Model> NotifyBegin<M> for Undoable<A, U>
where
    A: NotifyBegin<M> + Clone + Debug + Send + Sync + 'static,
    U: NotifyBegin<M> + Clone + Debug + Send + Sync + 'static,
    //RA: Notify<M> + 'static, RU: Notify<M> + 'static,
{
    fn whom_begin(&self) -> ToNotify {
        match self {
            Apply(a) => a.whom_begin(),
            Undo(u) => u.whom_begin(),
            Neither => ToNotify::empty(),
        }
    }
    fn notify_dreamlist_begin(&self, listener: &mut M::DreamListModel) {
        match self {
            Apply(a) => a.notify_dreamlist_begin(listener),
            Undo(u) => u.notify_dreamlist_begin(listener),
            Neither => (),
        }
    }
    fn notify_dreamform_begin(&self, listener: &mut M::DreamForm) {
        match self {
            Apply(a) => a.notify_dreamform_begin(listener),
            Undo(u) => u.notify_dreamform_begin(listener),
            Neither => (),
        }
    }
    fn notify_tagtree_begin(&self, listener: &mut M::TagTree) {
        match self {
            Apply(a) => a.notify_tagtree_begin(listener),
            Undo(u) => u.notify_tagtree_begin(listener),
            Neither => (),
        }
    }
    fn notify_entry_begin(&self, listener: &mut M::EntryModel) {
        match self {
            Apply(a) => a.notify_entry_begin(listener),
            Undo(u) => u.notify_entry_begin(listener),
            Neither => (),
        }
    }
}

impl<A, U, M: Model> NotifyEnd<M> for Undoable<A, U>
where
    A: NotifyEnd<M> + Clone + Debug + Send + Sync + 'static,
    U: NotifyEnd<M> + Clone + Debug + Send + Sync + 'static,
    //RA: Notify<M> + 'static, RU: Notify<M> + 'static,
{
    fn whom_end(&self) -> ToNotify {
        match self {
            Apply(a) => a.whom_end(),
            Undo(u) => u.whom_end(),
            Neither => ToNotify::empty(),
        }
    }
    fn notify_dreamlist_end(&self, listener: &mut M::DreamListModel) {
        match self {
            Apply(a) => a.notify_dreamlist_end(listener),
            Undo(u) => u.notify_dreamlist_end(listener),
            Neither => (),
        }
    }
    fn notify_dreamform_end(&self, listener: &mut M::DreamForm) {
        match self {
            Apply(a) => a.notify_dreamform_end(listener),
            Undo(u) => u.notify_dreamform_end(listener),
            Neither => (),
        }
    }
    fn notify_tagtree_end(&self, listener: &mut M::TagTree) {
        match self {
            Apply(a) => a.notify_tagtree_end(listener),
            Undo(u) => u.notify_tagtree_end(listener),
            Neither => (),
        }
    }
    fn notify_entry_end(&self, listener: &mut M::EntryModel) {
        match self {
            Apply(a) => a.notify_entry_end(listener),
            Undo(u) => u.notify_entry_end(listener),
            Neither => (),
        }
    }
}

/// The general type of error that can occurr in the execution of a
/// command on [`Diary`](crate::Diary).
#[derive(Debug, Clone)]
pub enum CmdError {
    /// Indicates that one tried to apply the command while it already
    /// was in its 'Applied' states from which it can only be undone.
    ///
    /// Usually this will occur when the command is an [`Undoable`]
    CannotApply,
    /// Indicates that one tried to undo the command while it still
    /// was in its 'Undone' states from which it can only be applied.
    ///
    /// Usually this will occur when the command is an [`Undoable`]
    CannotUndo,
    /// Indicates that the thread that should execute the command
    /// has been disconnected, i.e. panicked.
    Disconnected,
    Unspec,
    WhileApply(String, String),
    Uncritical(String, String),
    /// Indicates that a query has been interrupted because
    /// its result has become irrevelant.
    Interrupted,
    TermiteParse(termite::Error),
}

impl<T> From<std::sync::mpsc::SendError<T>> for CmdError {
    fn from(_other: std::sync::mpsc::SendError<T>) -> Self {
        CmdError::Disconnected
    }
}

impl<T> From<std::sync::PoisonError<T>> for CmdError {
    fn from(_other: std::sync::PoisonError<T>) -> Self {
        CmdError::Disconnected
    }
}

impl From<termite::Error> for CmdError {
    fn from(other: termite::Error) -> Self {
        CmdError::TermiteParse(other)
    }
}

impl Display for CmdError {
    fn fmt(&self, f: &mut Formatter) -> Result<(), std::fmt::Error> {
        match self {
            CmdError::CannotApply => {
                write!(f, "Command is in applied state. It can only be undone!")
            }
            CmdError::CannotUndo => {
                write!(f, "Command is in unapplied state. It can only be applied!")
            }
            CmdError::Disconnected => write!(f, "Worker thread has been disconnected!"),
            CmdError::Unspec => write!(f, "Unspecified error"),
            CmdError::WhileApply(cmd, e) => {
                write!(f, r#"Error while applying Command "{}": {}"#, cmd, e)
            }
            CmdError::Uncritical(cmd, e) => write!(
                f,
                r#"Uncritical Error while applying Command "{}": {}"#,
                cmd, e
            ),
            CmdError::Interrupted => write!(
                f,
                "Command was interrupted because it’s result has become irrelevant."
            ),
            CmdError::TermiteParse(e) => {
                write!(f, "Error while interpreting a termite term: \n{:?}", e)
            }
        }
    }
}

// The CmdError never has a source, so we can use the default implementation
// of std::error::Error.
impl std::error::Error for CmdError {}

/// Provides a means for an error to signal whether it is critical or not.
///
/// There are some procedures where 'critical' errors can occur,
/// that leave no possibility to continue the process, but where there
/// are 'uncritical' errors, too, that allow a partial completion of the process.
///
/// By implementing this trait, an error can signal whether it is critical or not.
pub trait Criticality {
    /// Returns `true` if the error is critical/fatal, `false` otherwise.
    fn is_critical(&self) -> bool;
}

#[cfg(test)]
mod tests {
    use chrono::NaiveDate;
    use crate::feature::undo::Command;

    use super::super::dream::{CreateDream, RemoveDream};
    use super::{Applyable, CmdError, Undoable};
    use crate::{Diary, Dream};

    #[test]
    fn undoable_command() {
        let cmd = CreateDream::new(
            Dream::from_oblig("Flying".to_string(), "Trübtraum".to_string()),
            NaiveDate::from_ymd(2019, 03, 02),
        );
        let mut und = Undoable::from(cmd);
        let undclone = und.clone();
        let mut diary = Diary::new();
        match und {
            Undoable::Apply(_) => (),
            _ => panic!("Command is initially not in apply state."),
        }

        //assert_eq!(und.whom_begin(), cmd.whom_begin());
        Command::<
            Diary,
            CmdError,
            Undoable<<CreateDream as Applyable>::Res, <RemoveDream as Applyable>::Res>,
        >::apply(&mut und, &mut diary)
        .unwrap();
        match und {
            Undoable::Undo(_) => (),
            _ => panic!("Command is not in undo state after application."),
        }
        Command::<
            Diary,
            CmdError,
            Undoable<<CreateDream as Applyable>::Res, <RemoveDream as Applyable>::Res>,
        >::undo(&mut und, &mut diary)
        .unwrap();
        assert_eq!(und, undclone);
    }
}
