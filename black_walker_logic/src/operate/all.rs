use std::fmt::{Display, Formatter};
use std::sync::{Arc, RwLock};

use crate::feature::crossbeam::atomic::AtomicCell;

use super::core::{Applyable, CmdError};
use crate::diary::{Diary, SaveState};
use crate::model::{
    DreamFormModel, DreamListModel, EntryModel, Model, NotifyBegin, NotifyEnd, TagTreeModel,
    ToNotify,
};
use crate::settings::DiaryLocation;
use crate::sync::prelude::*;
use crate::EntrySystem;
use crate::{lock, stage_diary};
use crate::{DreamList, NightList, TagForest, GS};

#[derive(Debug, Default, Clone, PartialEq, Eq, Hash)]
pub struct ClearDiary {}

impl Display for ClearDiary {
    fn fmt(&self, f: &mut Formatter) -> Result<(), std::fmt::Error> {
        write!(f, "Clear the diary")
    }
}

impl ClearDiary {
    pub fn new() -> Self {
        ClearDiary {}
    }
}

#[derive(Debug, Clone)]
pub struct UndoClearDiary {
    old_location: Option<DiaryLocation>,
    old_state: SaveState,
    dl: DreamList,
    nl: NightList,
    tag: TagForest,
    entr: EntrySystem,
}

impl Display for UndoClearDiary {
    fn fmt(&self, f: &mut Formatter) -> Result<(), std::fmt::Error> {
        write!(f, "Restore the diary")
    }
}

impl Applyable for ClearDiary {
    type To = Diary;
    type Opposite = UndoClearDiary;
    type Res = Self;
    fn apply(self, mut diary: &mut Diary) -> Result<(Self::Res, Self::Opposite), CmdError> {
        let dl: DreamList;
        let nl: NightList;
        let tag: TagForest;
        let entr: EntrySystem;
        let old_location;
        {
            let mut stage = stage_diary!(diary;; DL, NL, TAG, ENTR, SET);
            if let (
                Ok(mut dreamlist),
                Ok(mut nightlist),
                Ok(mut tagforest),
                Ok(mut entrysystem),
                Ok(mut settings),
            ) = lock!(stage;; DreamList, NightList, TagForest, EntrySystem, GS)
            {
                let new_members = Diary::default_members(&settings);
                dl = std::mem::replace(&mut dreamlist, new_members.0);
                nl = std::mem::replace(&mut nightlist, new_members.1);
                tag = std::mem::replace(&mut tagforest, new_members.2);
                entr = std::mem::replace(&mut entrysystem, new_members.3);
                old_location = std::mem::take(&mut settings.diary_location);
            } else {
                return Err(CmdError::Disconnected);
            }
        }

        diary.state.store(SaveState::New);
        Ok((
            self,
            UndoClearDiary {
                old_location,
                old_state: SaveState::Modified,
                dl,
                nl,
                tag,
                entr,
            },
        ))
    }
}

impl Applyable for UndoClearDiary {
    type To = Diary;
    type Opposite = ClearDiary;
    type Res = ClearDiary;
    fn apply(self, mut diary: &mut Diary) -> Result<(Self::Res, Self::Opposite), CmdError> {
        {
            let mut stage = stage_diary!(diary;; DL, NL, TAG, ENTR, SET);
            if let (
                Ok(mut dreamlist),
                Ok(mut nightlist),
                Ok(mut tagforest),
                Ok(mut entrysystem),
                Ok(mut settings),
            ) = lock!(stage;; DreamList, NightList, TagForest, EntrySystem, GS)
            {
                *dreamlist = self.dl;
                *nightlist = self.nl;
                *tagforest = self.tag;
                *entrysystem = self.entr;
                settings.diary_location = self.old_location;
            } else {
                return Err(CmdError::Disconnected);
            }
        }

        diary.state.store(self.old_state);
        Ok((ClearDiary::new(), ClearDiary::new()))
    }
}

impl<M: Model> NotifyBegin<M> for ClearDiary {
    fn whom_begin(&self) -> ToNotify {
        ToNotify::DREAMLIST | ToNotify::TAGTREE | ToNotify::ENTRYSET | ToNotify::DREAMFORM
    }
    fn notify_dreamform_begin(&self, listener: &mut M::DreamForm) {
        listener.begin_reset();
    }
    fn notify_dreamlist_begin(&self, listener: &mut M::DreamListModel) {
        listener.begin_reset();
    }
    fn notify_tagtree_begin(&self, listener: &mut M::TagTree) {
        listener.begin_reset();
    }
    fn notify_entry_begin(&self, listener: &mut M::EntryModel) {
        listener.begin_reset();
    }
}

impl<M: Model> NotifyBegin<M> for UndoClearDiary {
    fn whom_begin(&self) -> ToNotify {
        ToNotify::DREAMLIST | ToNotify::TAGTREE | ToNotify::ENTRYSET | ToNotify::DREAMFORM
    }
    fn notify_dreamform_begin(&self, listener: &mut M::DreamForm) {
        listener.begin_reset();
    }
    fn notify_dreamlist_begin(&self, listener: &mut M::DreamListModel) {
        listener.begin_reset();
    }
    fn notify_tagtree_begin(&self, listener: &mut M::TagTree) {
        listener.begin_reset();
    }
    fn notify_entry_begin(&self, listener: &mut M::EntryModel) {
        listener.begin_reset();
    }
}

impl<M: Model> NotifyEnd<M> for ClearDiary {
    fn whom_end(&self) -> ToNotify {
        ToNotify::DREAMLIST | ToNotify::TAGTREE | ToNotify::ENTRYSET | ToNotify::DREAMFORM
    }
    fn notify_dreamform_end(&self, listener: &mut M::DreamForm) {
        listener.end_reset();
    }
    fn notify_dreamlist_end(&self, listener: &mut M::DreamListModel) {
        listener.end_reset();
    }
    fn notify_tagtree_end(&self, listener: &mut M::TagTree) {
        listener.end_reset();
    }
    fn notify_entry_end(&self, listener: &mut M::EntryModel) {
        listener.end_reset();
    }
}

#[derive(Debug, Clone)]
pub struct ReplaceWithLoadedDiary {
    dl: DreamList,
    nl: NightList,
    tag: TagForest,
    entr: EntrySystem,
    settings: GS,
    /// Stores the state of the old diary for undoing.
    /// This is not used when the command is applied
    /// first.
    old_state: Option<SaveState>,
}

impl Display for ReplaceWithLoadedDiary {
    fn fmt(&self, f: &mut Formatter) -> Result<(), std::fmt::Error> {
        write!(f, "Replace the current diary in memory with a new one.")
    }
}

impl ReplaceWithLoadedDiary {
    pub fn new(
        dl: DreamList,
        nl: NightList,
        tag: TagForest,
        entr: EntrySystem,
        settings: GS,
    ) -> Self {
        ReplaceWithLoadedDiary {
            dl,
            nl,
            tag,
            entr,
            settings,
            old_state: None,
        }
    }
    pub fn into_diary(self) -> Diary {
        Diary {
            state: Arc::new(AtomicCell::new(SaveState::Unmodified)),
            dl: Arc::new(RwLock::new(self.dl)),
            nl: Arc::new(RwLock::new(self.nl)),
            tag: Arc::new(RwLock::new(self.tag)),
            entr: Arc::new(RwLock::new(self.entr)),
            gl: Arc::new(RwLock::new(self.settings)),
        }
    }
}

impl Applyable for ReplaceWithLoadedDiary {
    type To = Diary;
    type Opposite = Self;
    // Everything must be reset anyway, so we can as well reuse
    // the beautiful ClearDiary struct for the end notification.
    type Res = ClearDiary;
    fn apply(mut self, mut diary: &mut Diary) -> Result<(Self::Res, Self::Opposite), CmdError> {
        {
            let mut stage = stage_diary!(diary;; DL, NL, TAG, ENTR, SET);
            if let (
                Ok(mut dreamlist),
                Ok(mut nightlist),
                Ok(mut tagforest),
                Ok(mut entrysystem),
                Ok(mut settings),
            ) = lock!(stage;; DreamList, NightList, TagForest, EntrySystem, GS)
            {
                std::mem::swap(&mut self.dl, &mut dreamlist);
                std::mem::swap(&mut self.nl, &mut nightlist);
                std::mem::swap(&mut self.tag, &mut tagforest);
                std::mem::swap(&mut self.entr, &mut entrysystem);
                std::mem::swap(&mut self.settings, &mut settings);
            } else {
                return Err(CmdError::Disconnected);
            }
        }
        diary
            .state
            .store(self.old_state.unwrap_or(SaveState::Unmodified));
        self.old_state = Some(SaveState::Modified);

        Ok((ClearDiary::new(), self))
    }
}

impl<M: Model> NotifyBegin<M> for ReplaceWithLoadedDiary {
    fn whom_begin(&self) -> ToNotify {
        ToNotify::DREAMLIST | ToNotify::TAGTREE | ToNotify::ENTRYSET | ToNotify::DREAMFORM
    }
    fn notify_dreamform_begin(&self, listener: &mut M::DreamForm) {
        listener.begin_reset();
    }
    fn notify_dreamlist_begin(&self, listener: &mut M::DreamListModel) {
        listener.begin_reset();
    }
    fn notify_tagtree_begin(&self, listener: &mut M::TagTree) {
        listener.begin_reset();
    }
    fn notify_entry_begin(&self, listener: &mut M::EntryModel) {
        listener.begin_reset();
    }
}
