//! Contains [Queries](crate::model::Query) which perform searches in the
//! [`DreamList`](crate::DreamList).

use std::sync::mpsc::{channel, Receiver, Sender};
use std::sync::{Arc, Weak};

use crate::model::Query;
use crate::search::search;
use crate::sync::prelude::*;
use crate::{lock, stage_diary};
use crate::{Diary, DreamList, TagForest};
use crate::{DreamID, TagName};

const INTERRUPT_CHECK_INTERVAL: usize = 64;

/// A [`Query`] which searches for dreams which have tag
/// [`tagname`](SearchDreamsWithTag::tagname)
/// in [`category`](SearchDreamsWithTag::category).
///
/// Search is performed with respect to all subtags for
/// strong and weak forms.
#[derive(Debug, Clone)]
pub struct SearchDreamsWithTag {
    /// The category of the tag to search
    category: String,
    /// The name of the tag to search
    tagname: TagName,
    /// The sender through which the result will be sent.
    ser: Sender<Vec<DreamID>>,
    further: Weak<()>,
}

impl SearchDreamsWithTag {
    /// Creates a new query searching for dreams containing `tagname` in `category`.
    ///
    /// It returns the created `SearchDreamsWithTag` together with
    /// the `Receiver` through which the result will be available.
    pub fn new(category: String, tagname: TagName) -> (Self, Receiver<Vec<DreamID>>, Arc<()>) {
        let (ser, rec) = channel();
        let further = Arc::new(());
        (
            Self {
                category,
                tagname,
                ser,
                further: Arc::downgrade(&further),
            },
            rec,
            further,
        )
    }
}

impl Query for SearchDreamsWithTag {
    fn ask(&mut self, mut diary: &mut Diary) {
        let mut stage = stage_diary!(diary; DL, TAG);
        let mut res = Vec::new();
        if let (Ok(dreamlist), Ok(tagforest)) = lock!(stage; DreamList, TagForest) {
            if let Some(tagtree) = tagforest.get(&self.category) {
                let subview = tagtree.subview(&self.tagname);
                for (i, (id, dream)) in dreamlist.iter().enumerate() {
                    let tags = subview.iter().flatten().map(|node| node.name());
                    for tag in tags {
                        if dream.d.has_tag_any(&self.category, tag.as_str()) {
                            res.push(*id);
                            break;
                        }
                    }
                    if i % INTERRUPT_CHECK_INTERVAL == INTERRUPT_CHECK_INTERVAL - 1
                        && self.further.upgrade().is_none()
                    {
                        return;
                    }
                }
            }
        }
        match self.ser.send(res) {
            Ok(_) => (),
            // When sending the result fails, this simply means that the receiver has
            // no interest in the result anymore, so this SendError can be ignored.
            Err(_) => (),
        }
    }
}

#[derive(Debug, Clone)]
pub struct Search {
    condition: String,
    /// The sender through which the result will be sent.
    ser: Sender<Vec<DreamID>>,
    further: Weak<()>,
}

impl Search {
    pub fn new(condition: String) -> (Search, Receiver<Vec<DreamID>>, Arc<()>) {
        let (ser, rec) = channel();
        let further = Arc::new(());
        (
            Self {
                condition,
                ser,
                further: Arc::downgrade(&further),
            },
            rec,
            further,
        )
    }
}

impl Query for Search {
    fn ask(&mut self, diary: &mut Diary) {
        if let Ok(res) = search(
            diary,
            &self.condition,
            INTERRUPT_CHECK_INTERVAL,
            self.further.clone(),
        ) {
            match self.ser.send(res) {
                Ok(_) => (),
                // When sending the result fails, this simply means that the receiver has
                // no interest in the result anymore, so this SendError can be ignored.
                Err(_) => (),
            }
        }
    }
}
