//! Contains undoable commands that mainly manipulate the
//! [`DreamList`](crate::DreamList).
//!
//! Each of the commands in this moule expects when it is undone,
//! that the `Diary` is in the exact state it had after the command had been applied.
//! I.e. thay can't be undone from any arbitrary state, but only from
//! the state they produced themselves. If one does not respect this, the commands
//! may do arbatriry things to the diary.

use std::cmp::Ordering;
use std::collections::hash_map::Entry;
use std::collections::{HashMap, HashSet};
use std::fmt::{Display, Formatter};

use chrono::NaiveDate;

use super::core::{Applyable, CmdError};
use crate::cache_tree::CacheTree;
use crate::elements::tag::names::tag_spec_forest_to_tag_name_forest;
use crate::model::{
    DreamFormModel, DreamListModel, EntryModel, Model, NotifyBegin, NotifyEnd, TagTreeModel,
    ToNotify,
};
use crate::sync::prelude::*;
use crate::{lock, stage_diary};
use crate::{Diary, Dream, DreamID, TagName, TagSpec, TagSpecification};
use crate::{DreamList, EntrySystem, TagForest};

/// Creates a [`Dream`](crate::Dream) an a given [`Date`](chrono::NaiveDate).
///
/// The created dream will get the next free id on this date, i.e. if there are already
/// two dreams on _date_, the newly created dream will have the id (_date_, 3).
///
/// *Opposite:* [`RemoveDream`]
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct CreateDream {
    d: Dream,
    date: NaiveDate,
}

impl CreateDream {
    /// Constructs a `CreateDream` command that will insert `d` at `date` to the `DreamList`.
    pub fn new(d: Dream, date: NaiveDate) -> CreateDream {
        CreateDream { d, date }
    }
}

impl Display for CreateDream {
    fn fmt(&self, f: &mut Formatter) -> Result<(), std::fmt::Error> {
        write!(f, "Insert dream {} at date {}", self.d.name, self.date)
    }
}

impl Applyable for CreateDream {
    type To = Diary;
    type Opposite = RemoveDream;
    type Res = SetDreamNotifier;
    fn apply(self, mut diary: &mut Diary) -> Result<(Self::Res, Self::Opposite), CmdError> {
        let mut stage = stage_diary!(diary;; DL, TAG, ENTR);
        if let (Ok(mut dreamlist), Ok(mut tagtree), Ok(mut entrysystem)) =
            lock!(stage;; DreamList, TagForest, EntrySystem)
        {
            let mut inserted_categories = HashSet::new();
            let mut inserted_tags = HashMap::new();
            let mut changed_tags: HashMap<String, HashSet<TagName>> = HashMap::new();
            let all_tags = self.d.get_all_tags().clone();
            entrysystem.add_entries_of_dream(&self.d);
            let id = dreamlist.insert_dream_at_end(self.d, self.date);
            for (k, v) in all_tags {
                let cat = match tagtree.entry(k.clone()) {
                    Entry::Occupied(e) => e.into_mut(),
                    Entry::Vacant(e) => {
                        inserted_categories.insert(k.clone());
                        e.insert(CacheTree::new())
                    }
                };
                for it in v {
                    let basename = it.base_name();
                    let mode = it.strength();
                    if !cat.occur(&basename, mode) {
                        inserted_tags
                            .entry(k.clone())
                            .or_insert(HashSet::new())
                            .insert(it.base_name().to_owned());
                    }
                        changed_tags
                            .entry(k.clone())
                            .or_insert(HashSet::new())
                            .insert(it.base_name().to_owned());
                }
            }
            Ok((
                SetDreamNotifier {
                    id: None,
                    new_id: Some(id),
                    removed_categories: HashSet::new(),
                    removed_tags: HashMap::new(),
                    inserted_categories,
                    inserted_tags,
                    changed_tags,
                    changed_ids: Vec::new(),
                },
                RemoveDream { id },
            ))
        } else {
            Err(CmdError::Disconnected)
        }
    }
}

impl<M: Model> NotifyBegin<M> for CreateDream {
    fn whom_begin(&self) -> ToNotify {
        ToNotify::DREAMLIST | ToNotify::ENTRYSET
    }
    fn notify_dreamlist_begin(&self, listener: &mut M::DreamListModel) {
        listener.begin_create_dream(self.date);
    }
    fn notify_entry_begin(&self, listener: &mut M::EntryModel) {
        listener.begin_reset();
    }
}

impl<M: Model> NotifyEnd<M> for CreateDream {
    fn whom_end(&self) -> ToNotify {
        ToNotify::DREAMLIST | ToNotify::ENTRYSET
    }
    fn notify_dreamlist_end(&self, listener: &mut M::DreamListModel) {
        listener.end_create_dream(self.date);
    }
    fn notify_entry_end(&self, listener: &mut M::EntryModel) {
        listener.end_reset();
    }
}

/// Removes the [`Dream`](crate::Dream) with a specified
/// [id](crate::DreamID).
///
/// *Opposite:* [`CreateDream`]
#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct RemoveDream {
    id: DreamID,
}

impl RemoveDream {
    /// Constructs a new `RemoveDream` command removing the `Dream` with `id`.
    pub fn new(id: DreamID) -> RemoveDream {
        RemoveDream { id }
    }
}

impl Display for RemoveDream {
    fn fmt(&self, f: &mut Formatter) -> Result<(), std::fmt::Error> {
        write!(f, "Remove dream {}", self.id)
    }
}

impl Applyable for RemoveDream {
    type To = Diary;
    type Opposite = CreateDream;
    type Res = SetDreamNotifier;
    fn apply(self, mut diary: &mut Diary) -> Result<(Self::Res, Self::Opposite), CmdError> {
        let mut stage = stage_diary!(diary;; DL, TAG);
        if let (Ok(mut dreamlist), Ok(mut tagtree)) = lock!(stage;; DreamList, TagForest) {
            if let Some(dream) = dreamlist.remove_dream(self.id) {
                let mut changed_ids = Vec::new();
                for i in (self.id.idx + 1)..(dreamlist.dream_count(self.id.date) + 1) {
                    changed_ids.push((
                        DreamID::new(self.id.date, i),
                        DreamID::new(self.id.date, i - 1),
                    ));
                }
                let mut changed_tags = HashMap::new();
                for (k, v) in dream.d.get_all_tags().clone() {
                    if let Some(cat) = tagtree.get_mut(&k) {
                        for it in v {
                            cat.disoccur(it.base_name(), it.strength());
                            changed_tags
                                .entry(k.clone())
                                .or_insert(HashSet::new())
                                .insert(it.base_name().to_owned());
                        }
                    }
                }
                Ok((
                    SetDreamNotifier {
                        id: Some(self.id),
                        new_id: None,
                        removed_categories: HashSet::new(),
                        removed_tags: HashMap::new(),
                        inserted_categories: HashSet::new(),
                        inserted_tags: HashMap::new(),
                        changed_tags,
                        changed_ids,
                    },
                    CreateDream {
                        date: dream.id().date,
                        d: dream.d,
                    },
                ))
            } else {
                Err(CmdError::Unspec)
            }
        } else {
            Err(CmdError::Disconnected)
        }
    }
}

impl<M: Model> NotifyBegin<M> for RemoveDream {
    fn whom_begin(&self) -> ToNotify {
        ToNotify::DREAMLIST | ToNotify::ENTRYSET
    }
    fn notify_dreamlist_begin(&self, listener: &mut M::DreamListModel) {
        listener.begin_remove_dream(self.id);
    }
    fn notify_entry_begin(&self, listener: &mut M::EntryModel) {
        listener.begin_reset();
    }
}

/// Moves the dream located at id `(date, oldidx)` to id `(date, newidx)`.
///
/// This command can only move dreams within the same date, so it is the correct
/// one for applying a move that results from a drag&drop-action.
#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub struct MoveDream {
    date: NaiveDate,
    oldidx: u32,
    newidx: u32,
}

impl Display for MoveDream {
    fn fmt(&self, f: &mut Formatter) -> Result<(), std::fmt::Error> {
        write!(
            f,
            "Move dream {} on date {} to position {}",
            self.oldidx, self.date, self.newidx
        )
    }
}

impl MoveDream {
    /// Creates a new command that moves
    /// the dream located at id `(date, oldidx)` to id `(date, newidx)`.
    pub fn new(date: NaiveDate, oldidx: u32, newidx: u32) -> Self {
        MoveDream {
            date,
            oldidx,
            newidx,
        }
    }
    /// Creates a command that fulfills a drag and drop operation.
    ///
    /// While [`new`](MoveDream::new) fulfills the guarantee of the underlying
    /// [`DreamList::move_dream`](crate::DreamList) that
    /// the dream from the old id will have the new id after the move operation,
    /// this function considers that all dreams between the old id
    /// and the new id move one step back if the dream is move forward.
    ///
    /// So when a dream is dropped onto the gap directly *before* `target_id`,
    /// use this function to move it there.
    pub fn from_drag_drop(date: NaiveDate, source_id: u32, target_id: u32) -> Option<Self> {
        match target_id.cmp(&source_id) {
            Ordering::Greater => Some(MoveDream {
                date,
                oldidx: source_id,
                // Won't overflow because target_id >= source_id +1 >= 1
                newidx: target_id - 1,
            }),
            Ordering::Less => Some(MoveDream {
                date,
                oldidx: source_id,
                newidx: target_id,
            }),
            Ordering::Equal => None,
        }
    }
    /// Returns the ids which will be changed by this Command.
    ///
    /// These are the id of the moved dream as well as all ids between the source
    /// and destination.
    ///
    /// The returned vec contains tuples (oldid, newid).
    #[allow(clippy::int_plus_one)]
    pub fn id_changes(&self) -> Vec<(DreamID, DreamID)> {
        let mut res = vec![(
            DreamID::new(self.date, self.oldidx),
            DreamID::new(self.date, self.newidx),
        )];
        if self.oldidx + 1 <= self.newidx {
            for i in (self.oldidx + 1)..=(self.newidx) {
                res.push((DreamID::new(self.date, i), DreamID::new(self.date, i - 1)));
            }
        }
        if self.newidx < self.oldidx {
            for i in (self.newidx)..(self.oldidx) {
                res.push((DreamID::new(self.date, i), DreamID::new(self.date, i + 1)));
            }
        }
        res
    }
}

impl Applyable for MoveDream {
    type To = Diary;
    type Opposite = MoveDream;
    type Res = Self;
    fn apply(mut self, diary: &mut Diary) -> Result<(Self::Res, Self::Opposite), CmdError> {
        if let Ok(mut dreamlist) = diary.dl.write() {
            if dreamlist.move_dream(
                DreamID::new(self.date, self.oldidx),
                DreamID::new(self.date, self.newidx),
            ) {
                let res = self.clone();
                std::mem::swap(&mut self.oldidx, &mut self.newidx);
                Ok((res, self))
            } else {
                Err(CmdError::Unspec)
            }
        } else {
            Err(CmdError::Disconnected)
        }
    }
}

impl<M: Model> NotifyBegin<M> for MoveDream {
    fn whom_begin(&self) -> ToNotify {
        ToNotify::DREAMLIST | ToNotify::ENTRYSET
    }
    fn notify_dreamlist_begin(&self, listener: &mut M::DreamListModel) {
        listener.begin_move_dream(
            DreamID::new(self.date, self.oldidx),
            self.date,
            Some(self.newidx),
        );
    }
    fn notify_entry_begin(&self, listener: &mut M::EntryModel) {
        listener.begin_reset();
    }
}

impl<M: Model> NotifyEnd<M> for MoveDream {
    fn whom_end(&self) -> ToNotify {
        ToNotify::DREAMLIST | ToNotify::DREAMFORM | ToNotify::ENTRYSET
    }
    fn notify_dreamlist_end(&self, listener: &mut M::DreamListModel) {
        listener.end_move_dream(
            DreamID::new(self.date, self.oldidx),
            self.date,
            Some(self.newidx),
        );
    }
    fn notify_dreamform_end(&self, listener: &mut M::DreamForm) {
        listener.ids_changed(&self.id_changes());
    }
    fn notify_entry_end(&self, listener: &mut M::EntryModel) {
        listener.end_reset();
    }
}

/// Sets a dream with a certain id to a new dream.
///
/// The date of this new dream may differ from the old date of the dream.
/// In this case, the dream is moved automatically to the last idx of the new date.
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct SetDream {
    id: DreamID,
    new_date: NaiveDate,
    dream: Dream,
    categories_to_remove: HashSet<String>,
    tags_to_remove: HashMap<String, HashSet<TagSpec>>,
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct SetDreamNotifier {
    id: Option<DreamID>,
    new_id: Option<DreamID>,
    removed_categories: HashSet<String>,
    removed_tags: HashMap<String, HashSet<TagName>>,
    inserted_categories: HashSet<String>,
    inserted_tags: HashMap<String, HashSet<TagName>>,
    changed_tags: HashMap<String, HashSet<TagName>>,
    changed_ids: Vec<(DreamID, DreamID)>,
}

impl Display for SetDream {
    fn fmt(&self, f: &mut Formatter) -> Result<(), std::fmt::Error> {
        write!(f, "Change dream with id {}", self.id)
    }
}

impl SetDream {
    /// Constructs a new `SetDream`, setting the dream at `id` to `dream`.
    ///
    /// The date of the dream will be left unchanged.
    pub fn new(id: DreamID, dream: Dream) -> SetDream {
        SetDream {
            id,
            new_date: id.date,
            dream,
            categories_to_remove: HashSet::new(),
            tags_to_remove: HashMap::new(),
        }
    }
    /// Constructs a new `SetDream`, setting the dream at `id` to `dream`,
    /// moving it to a new date `new_date`.
    ///
    /// After application of this command, the dream will be identical to `dream`
    /// and placed as the last dream in the night `new_date`.
    pub fn with_new_date(id: DreamID, new_date: NaiveDate, dream: Dream) -> SetDream {
        SetDream {
            id,
            new_date,
            dream,
            categories_to_remove: HashSet::new(),
            tags_to_remove: HashMap::new(),
        }
    }
}

impl Applyable for SetDream {
    type To = Diary;
    type Opposite = SetDream;
    type Res = SetDreamNotifier;
    fn apply(mut self, mut diary: &mut Diary) -> Result<(Self::Res, Self::Opposite), CmdError> {
        let mut stage = stage_diary!(diary;; DL, TAG, ENTR);
        if let (Ok(mut dreamlist), Ok(mut tagtree), Ok(mut entrysystem)) =
            lock!(stage;; DreamList, TagForest, EntrySystem)
        {
            let (new_tags, old_tags) = self.dream.tag_diff(
                &dreamlist
                    .get(self.id)
                    .ok_or(CmdError::WhileApply(
                        self.to_string(),
                        "Dream to set does not exist".to_string(),
                    ))?
                    .d,
            );
            let mut inserted_tags = HashMap::new();
            let mut changed_tags = HashMap::new();
            let mut inserted_categories = HashSet::new();
            for (k, v) in &self.tags_to_remove {
                if let Some(cat) = tagtree.get_mut(k) {
                    for it in v {
                        cat.remove(it.base_name());
                    }
                }
            }
            for it in &self.categories_to_remove {
                if let Some(cat) = tagtree.get(it) {
                    if cat.is_empty() {
                        tagtree.remove(it);
                    }
                }
            }
            for (k, v) in new_tags {
                let cat = match tagtree.entry(k.clone()) {
                    Entry::Occupied(e) => e.into_mut(),
                    Entry::Vacant(e) => {
                        inserted_categories.insert(k.clone());
                        e.insert(CacheTree::new())
                    }
                };
                for it in v {
                    if !cat.occur(it.base_name(), it.strength()) {
                        inserted_tags
                            .entry(k.clone())
                            .or_insert(HashSet::new())
                            .insert(it.clone());
                    }
                    changed_tags
                        .entry(k.clone())
                        .or_insert(HashSet::new())
                        .insert(it.base_name().to_owned());
                }
            }
            for (k, v) in old_tags {
                if let Some(cat) = tagtree.get_mut(&k) {
                    for it in v {
                        cat.disoccur(it.base_name(), it.strength());
                        changed_tags
                            .entry(k.clone())
                            .or_insert(HashSet::new())
                            .insert(it.base_name().to_owned());
                    }
                }
            }
            let mut res = SetDreamNotifier {
                id: Some(self.id),
                new_id: Some(self.id),
                removed_categories: self.categories_to_remove.clone(),
                removed_tags: tag_spec_forest_to_tag_name_forest(self.tags_to_remove.clone()),
                inserted_categories: inserted_categories.clone(),
                inserted_tags: tag_spec_forest_to_tag_name_forest(inserted_tags.clone()),
                changed_tags,
                changed_ids: Vec::new(),
            };
            self.categories_to_remove = inserted_categories;
            self.tags_to_remove = inserted_tags;
            entrysystem.add_entries_of_dream(&self.dream);
            if self.id.date == self.new_date {
                if let Some(dream) = dreamlist.get_mut(self.id) {
                    std::mem::swap(&mut self.dream, &mut dream.d);
                    Ok((res, self))
                } else {
                    Err(CmdError::WhileApply(
                        self.to_string(),
                        "Dream to set does not exist".to_string(),
                    ))
                }
            } else if let Some(mut dream) = dreamlist.remove_dream(self.id) {
                std::mem::swap(&mut self.dream, &mut dream.d);
                let new_id = dreamlist.insert_dream_at_end(dream.d, self.new_date);
                res.new_id = Some(new_id);
                res.changed_ids.push((self.id, new_id));
                for i in self.id.idx + 1..dreamlist.dream_count(self.id.date) + 1 {
                    res.changed_ids.push((
                        DreamID::new(self.id.date, i),
                        DreamID::new(self.id.date, i - 1),
                    ));
                }
                Ok((res, self))
            } else {
                Err(CmdError::WhileApply(
                    self.to_string(),
                    "Dream to set does not exist".to_string(),
                ))
            }
        } else {
            Err(CmdError::Disconnected)
        }
    }
}

impl<M: Model> NotifyBegin<M> for SetDream {
    fn whom_begin(&self) -> ToNotify {
        ToNotify::DREAMLIST | ToNotify::ENTRYSET
    }

    fn notify_dreamlist_begin(&self, listener: &mut M::DreamListModel) {
        if self.id.date == self.new_date {
            listener.begin_set_dream(self.id);
        } else {
            listener.begin_move_dream(self.id, self.new_date, None);
        }
    }
    fn notify_entry_begin(&self, listener: &mut M::EntryModel) {
        listener.begin_reset();
    }
}

impl<M: Model> NotifyEnd<M> for SetDreamNotifier {
    fn whom_end(&self) -> ToNotify {
        // If anything related to the ids changed (including deleting
        // and creating ids), the dreamform must be notified.
        let dreamform = if self.id != self.new_id {
            ToNotify::DREAMFORM
        } else {
            ToNotify::empty()
        };
        dreamform | ToNotify::DREAMLIST | ToNotify::TAGTREE | ToNotify::ENTRYSET
    }

    fn notify_dreamlist_end(&self, listener: &mut M::DreamListModel) {
        match (self.id, self.new_id) {
            (Some(oldid), Some(new_id)) => {
                if oldid.date == new_id.date {
                    listener.end_set_dream(oldid);
                } else {
                    listener.end_move_dream(oldid, new_id.date, Some(new_id.idx));
                    listener.end_set_dream(new_id);
                }
            }
            (None, Some(new_id)) => listener.end_create_dream(new_id.date),
            (Some(oldid), None) => listener.end_remove_dream(oldid),
            (None, None) => panic!("SetDreamNotifier must contain at least one id!"),
        }
    }
    fn notify_tagtree_end(&self, listener: &mut M::TagTree) {
        for it in &self.inserted_categories {
            listener.end_create_category(it);
        }
        for (k, v) in &self.inserted_tags {
            for it in v {
                listener.end_create_tag(&k, &it, &TagName::root());
            }
        }
        for it in &self.removed_categories {
            listener.end_remove_category(it);
        }
        for (k, v) in &self.removed_tags {
            for it in v {
                listener.end_remove_tag(&k, &it);
            }
        }

        for (k, v) in &self.changed_tags {
            for it in v {
                listener.end_set_tag(&k, &it);
            }
        }
    }
    fn notify_dreamform_end(&self, listener: &mut M::DreamForm) {
        if let Some(new_id) = self.new_id {
            listener.give_id(new_id);
        } else if let Some(old_id) = self.id {
            listener.dream_removed(old_id)
        } else {
            debug_assert!(false, "SetDreamNotifier does not contain any id.");
        }
        if !self.changed_ids.is_empty() {
            listener.ids_changed(&self.changed_ids);
        }
    }
    fn notify_entry_end(&self, listener: &mut M::EntryModel) {
        listener.end_reset();
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use chrono::NaiveDate;
    use std::ops::Deref;
    use std::str::FromStr;

    use super::super::core::Applyable;
    use super::super::tests::standard_diary;

    #[test]
    fn create_remove_dream() {
        let cmd1 = CreateDream::new(
            Dream::from_oblig("Flying".to_string(), "Trübtraum".to_string()),
            NaiveDate::from_ymd(2019, 03, 02),
        );
        let cmd1clone = cmd1.clone();
        let cmd2 = CreateDream::new(
            Dream::from_oblig("Albus Dumbledore".to_string(), "Trübtraum".to_string()),
            NaiveDate::from_ymd(2019, 03, 03),
        );
        let cmd2clone = cmd2.clone();
        let cmd3 = CreateDream::new(
            Dream::from_oblig("Lord of the dark".to_string(), "Trübtraum".to_string()),
            NaiveDate::from_ymd(2019, 03, 03),
        );
        let cmd3clone = cmd3.clone();
        let mut diary = Diary::new();
        let und1 = cmd1.apply(&mut diary).unwrap().1;
        assert_eq!(diary.dl.read().unwrap().len(), 1);
        assert!(diary
            .dl
            .read()
            .unwrap()
            .has_dream(DreamID::new(NaiveDate::from_ymd(2019, 03, 02), 0)));
        assert_eq!(
            und1,
            RemoveDream::new(DreamID::new(NaiveDate::from_ymd(2019, 03, 02), 0))
        );

        let und2 = cmd2.apply(&mut diary).unwrap().1;
        assert_eq!(diary.dl.read().unwrap().len(), 2);
        assert_eq!(
            und2,
            RemoveDream::new(DreamID::new(NaiveDate::from_ymd(2019, 03, 03), 0))
        );
        let state2 = diary.dl.read().unwrap().clone();

        let und3 = cmd3.apply(&mut diary).unwrap().1;
        assert_eq!(diary.dl.read().unwrap().len(), 3);
        {
            let lock = diary.dl.read().unwrap();
            let dream3 = lock
                .get(DreamID::new(NaiveDate::from_ymd(2019, 03, 03), 1))
                .unwrap();
            assert_eq!(
                dream3.id(),
                DreamID::new(NaiveDate::from_ymd(2019, 03, 03), 1)
            );
            assert_eq!(
                dream3.d,
                Dream::from_oblig("Lord of the dark".to_string(), "Trübtraum".to_string())
            );
        }

        assert_eq!(cmd3clone, und3.apply(&mut diary).unwrap().1);
        assert_eq!(diary.dl.read().unwrap().deref(), &state2);

        assert_eq!(cmd2clone, und2.apply(&mut diary).unwrap().1);
        assert_eq!(cmd1clone, und1.apply(&mut diary).unwrap().1);
        assert!(diary.dl.read().unwrap().is_empty());
    }
    #[test]
    fn move_dream() {
        let mut diary = standard_diary();
        {
            let lock = diary.dl.read().unwrap();
            assert_eq!(
                lock.get(DreamID::new(NaiveDate::from_ymd(1054, 07, 05), 0))
                    .unwrap()
                    .d
                    .name
                    .as_str(),
                "Albus Dumbledore"
            );
        }
        let cmd1 = MoveDream::new(NaiveDate::from_ymd(1054, 07, 05), 0, 2);
        cmd1.apply(&mut diary).expect("Dream could not be moved");
        {
            let lock = diary.dl.read().unwrap();
            assert_eq!(
                lock.get(DreamID::new(NaiveDate::from_ymd(1054, 07, 05), 2))
                    .unwrap()
                    .d
                    .name
                    .as_str(),
                "Albus Dumbledore"
            );
            assert_eq!(
                lock.get(DreamID::new(NaiveDate::from_ymd(1054, 07, 05), 0))
                    .unwrap()
                    .d
                    .name
                    .as_str(),
                "Landscape"
            );
        }
    }
    #[test]
    fn move_dream_to_same_position() {
        let mut diary = standard_diary();
        {
            let lock = diary.dl.read().unwrap();
            assert_eq!(
                lock.get(DreamID::new(NaiveDate::from_ymd(1054, 07, 05), 0))
                    .unwrap()
                    .d
                    .name
                    .as_str(),
                "Albus Dumbledore"
            );
        }
        let cmd1 = MoveDream::new(NaiveDate::from_ymd(1054, 07, 05), 0, 0);
        cmd1.apply(&mut diary).expect("Dream could not be moved");
        {
            let lock = diary.dl.read().unwrap();
            assert_eq!(
                lock.get(DreamID::new(NaiveDate::from_ymd(1054, 07, 05), 0))
                    .unwrap()
                    .d
                    .name
                    .as_str(),
                "Albus Dumbledore",
                "Move dream from 0 to 0 had an effect"
            );
            assert_eq!(
                lock.get(DreamID::new(NaiveDate::from_ymd(1054, 07, 05), 1))
                    .unwrap()
                    .d
                    .name
                    .as_str(),
                "Landscape",
                "Move dream from 0 to 0 had an effect"
            );
        }
    }
    #[test]
    fn move_dream_from_drag_drop() {
        let mut diary = standard_diary();
        {
            let lock = diary.dl.read().unwrap();
            assert_eq!(
                lock.get(DreamID::new(NaiveDate::from_ymd(1054, 07, 05), 0))
                    .unwrap()
                    .d
                    .name
                    .as_str(),
                "Albus Dumbledore"
            );
        }
        let cmd1 = MoveDream::from_drag_drop(NaiveDate::from_ymd(1054, 07, 05), 0, 1);
        cmd1.unwrap()
            .apply(&mut diary)
            .expect("Dream could not be moved");
        {
            let lock = diary.dl.read().unwrap();
            assert_eq!(
                lock.get(DreamID::new(NaiveDate::from_ymd(1054, 07, 05), 0))
                    .unwrap()
                    .d
                    .name
                    .as_str(),
                "Albus Dumbledore",
                "Drag&drop from 0 to 1 had an effect"
            );
            assert_eq!(
                lock.get(DreamID::new(NaiveDate::from_ymd(1054, 07, 05), 1))
                    .unwrap()
                    .d
                    .name
                    .as_str(),
                "Landscape",
                "Drag&drop from 0 to 1 had an effect"
            );
        }
        let cmd1 = MoveDream::from_drag_drop(NaiveDate::from_ymd(1054, 07, 05), 0, 3);
        cmd1.unwrap()
            .apply(&mut diary)
            .expect("Dream could not be moved");
        {
            let lock = diary.dl.read().unwrap();
            assert_eq!(
                lock.get(DreamID::new(NaiveDate::from_ymd(1054, 07, 05), 0))
                    .unwrap()
                    .d
                    .name
                    .as_str(),
                "Landscape"
            );
            assert_eq!(
                lock.get(DreamID::new(NaiveDate::from_ymd(1054, 07, 05), 2))
                    .unwrap()
                    .d
                    .name
                    .as_str(),
                "Albus Dumbledore"
            );
        }
    }
    #[test]
    fn move_dream_changed_ids() {
        let date = NaiveDate::from_ymd(2020, 05, 27);
        let cmd = MoveDream::new(date, 1, 4);
        assert_eq!(
            cmd.id_changes(),
            vec![
                (DreamID::new(date, 1), DreamID::new(date, 4)),
                (DreamID::new(date, 2), DreamID::new(date, 1)),
                (DreamID::new(date, 3), DreamID::new(date, 2)),
                (DreamID::new(date, 4), DreamID::new(date, 3)),
            ]
        );

        let date = NaiveDate::from_ymd(2020, 05, 27);
        let cmd = MoveDream::new(date, 5, 2);
        assert_eq!(
            cmd.id_changes(),
            vec![
                (DreamID::new(date, 5), DreamID::new(date, 2)),
                (DreamID::new(date, 2), DreamID::new(date, 3)),
                (DreamID::new(date, 3), DreamID::new(date, 4)),
                (DreamID::new(date, 4), DreamID::new(date, 5)),
            ]
        );
    }

    #[test]
    fn add_tags_to_dream() {
        let mut diary = standard_diary();

        let precount_albus;
        let precount_harry_books;
        {
            let tagforest = diary.tag.read().unwrap();
            let tagtree = tagforest.get("Personen").unwrap();
            precount_albus = tagtree.get("Albus Dumbledore").unwrap().count().strong;
            precount_harry_books = tagtree.get("Harry Potter (books)").unwrap().count().strong;
        }

        let mut dream =
            Dream::from_oblig("Albus Dumbledore".to_string(), "Ordinary Dream".to_string());
        dream.add_tags(
            "Personen",
            [
                "Harry Potter (books)",
                "Lord Voldemort",
                "Harry Potter",
                "Darth Vader",
            ]
            .iter()
            .map(|s| TagSpec::from_str(s).unwrap()),
        );
        let cmd = SetDream::new(DreamID::new(NaiveDate::from_ymd(1054, 07, 05), 0), dream);
        cmd.apply(&mut diary).unwrap();

        {
            let tagforest = diary.tag.read().unwrap();
            let tagtree = tagforest.get("Personen").unwrap();
            assert_eq!(
                precount_albus - 1,
                tagtree.get("Albus Dumbledore").unwrap().count().strong
            );
            assert_eq!(
                precount_harry_books,
                tagtree.get("Harry Potter (books)").unwrap().count().strong
            );
        }

        let mut dream =
            Dream::from_oblig("Albus Dumbledore".to_string(), "Ordinary Dream".to_string());
        dream.add_tags(
            "Personen",
            [
                "Albus Dumbledore",
                "Lord Voldemort",
                "Harry Potter",
                "Darth Vader",
            ]
            .iter()
            .map(|s| TagSpec::from_str(s).unwrap()),
        );
        let cmd = SetDream::new(DreamID::new(NaiveDate::from_ymd(1054, 07, 05), 0), dream);
        cmd.apply(&mut diary).unwrap();

        {
            let tagforest = diary.tag.read().unwrap();
            let tagtree = tagforest.get("Personen").unwrap();
            assert_eq!(
                precount_albus,
                tagtree.get("Albus Dumbledore").unwrap().count().strong
            );
            assert_eq!(
                precount_harry_books,
                tagtree.get("Harry Potter (books)").unwrap().count().strong
            );
        }
    }
}
