//! Contains undoable commands that mainly manipulate the
//! `TagTree`.
//!
//! Each of the commands in this moule expects when it is undone,
//! that the `Diary` is in the exact state it had after the command had been applied.
//! I.e. thay can't be undone from any arbitrary state, but only from
//! the state they produced themselves. If one does not respect this, the commands

use std::collections::hash_map::Entry;
use std::collections::HashMap;
use std::fmt::{Display, Formatter};

use super::core::{Applyable, CmdError};
use crate::cache_tree::CacheTree;
use crate::elements::tag::OccurMode;
use crate::model::{DreamListModel, Model, NotifyBegin, NotifyEnd, TagTreeModel, ToNotify};
use crate::sync::prelude::*;
use crate::write_single;
use crate::{lock, stage_diary};
use crate::{Diary, Tag};
use crate::{DreamID, TagName, TagSpec, TagStr, TagTree};
use crate::{DreamList, TagForest};

/// Creates a new tag with given name and parent in a certain category.
#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct CreateTag {
    /// The category in which the tag shall be created
    category: String,
    /// The parent of the tag to create
    parent: TagName,
    /// The name of the tag to create
    name: TagName,
    /// The additional content of the tag to create
    content: Tag,
    category_inserted: bool,
}

impl CreateTag {
    /// Construct a command creating a tag `name` as a child of `parent`
    /// in `category`.
    ///
    /// Its [`content`](crate::cache_tree::Node::content)
    /// will be default-constructed.
    pub fn new(category: String, name: TagName, parent: TagName) -> CreateTag {
        CreateTag {
            category,
            parent,
            name,
            content: Tag::default(),
            category_inserted: false,
        }
    }

    /// Construct a command creating a tag `name` as a child of the root tag
    /// in `category`.
    ///
    /// Its [`content`](crate::cache_tree::Node::content)
    /// will be default-constructed.
    pub fn new_at_root(category: String, name: TagName) -> CreateTag {
        CreateTag {
            category,
            parent: TagName::root(),
            name,
            content: Tag::default(),
            category_inserted: false,
        }
    }

    /// Returns the category of the tag to create.
    pub fn category(&self) -> &str {
        &self.category
    }

    /// Returns the name of the tag to create.
    pub fn name(&self) -> &TagStr {
        &self.name
    }
}

impl Display for CreateTag {
    fn fmt(&self, f: &mut Formatter) -> Result<(), std::fmt::Error> {
        write!(
            f,
            "Create tag {} in category {} as a child of {}.",
            self.name, self.category, self.parent
        )
    }
}

impl Applyable for CreateTag {
    type To = Diary;
    type Opposite = UndoCreateTag;
    type Res = Self;
    fn apply(self, diary: &mut Diary) -> Result<(Self::Res, Self::Opposite), CmdError> {
        if let Ok(mut tagforest) = diary.tag.write() {
            let mut res = self.clone();
            let (category, parent, name, content) =
                (self.category, self.parent, self.name, self.content);
            // If the category does not already exist, it is inserted.
            // But for undoing, this information must be saved in `category_inserted`
            let (tagtree, category_inserted) = match tagforest.entry(category.clone()) {
                Entry::Occupied(entr) => (entr.into_mut(), false),
                Entry::Vacant(entr) => {
                    res.category_inserted = true;
                    (entr.insert(TagTree::default()), true)
                }
            };
            // Check if the parent exists
            //
            // We cannot simply run `tagtree.insert_content` because this function
            // has two failure modes: When the parent does not exist or
            // when the tag itself does already exist.
            if !tagtree.node_exists(&parent) {
                return Err(CmdError::WhileApply(
                    CreateTag {
                        category,
                        parent,
                        name,
                        content,
                        category_inserted,
                    }
                    .to_string(),
                    "Parent of tag does not exist.".to_string(),
                ));
            }
            match tagtree.insert_content(name.clone(), content, &parent) {
                Some((name, _)) => Ok((
                    res,
                    UndoCreateTag {
                        category,
                        name,
                        remove_category: category_inserted,
                        remove_really: false,
                        parent,
                    }
                )),
                None => Ok((
                    res,
                    UndoCreateTag {
                        category,
                        name,
                        remove_category: category_inserted,
                        remove_really: true,
                        parent
                    },
                )),
            }
        } else {
            Err(CmdError::Disconnected)
        }
    }
}

impl<M: Model> NotifyBegin<M> for CreateTag {
    fn whom_begin(&self) -> ToNotify {
        ToNotify::TAGTREE
    }
    fn notify_tagtree_begin(&self, listener: &mut M::TagTree) {
        listener.begin_create_tag(&self.category, &self.name, &self.parent);
    }
}

impl<M: Model> NotifyEnd<M> for CreateTag {
    fn whom_end(&self) -> ToNotify {
        ToNotify::TAGTREE
    }
    fn notify_tagtree_end(&self, listener: &mut M::TagTree) {
        listener.end_create_tag(&self.category, &self.name, &self.parent);
        if self.category_inserted {
            listener.begin_create_category(&self.category);
            listener.end_create_category(&self.category);
        }
    }
}

/// Undoes the creation of a tag with a given name from a certain category.
#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct UndoCreateTag {
    /// The category in which the tag was created
    category: String,
    /// The name of the tag that was created
    name: TagName,
    /// Specifies whether the category of the tag shall also be removed
    /// as part of the removal of the tag.
    ///
    /// If the tag was inserted into category that did not already
    /// exist, the category itself was newly created.
    /// This creation must then also be undone when the
    /// tag is removed again.
    remove_category: bool,
    remove_really: bool,
    parent: TagName,
}

impl Display for UndoCreateTag {
    fn fmt(&self, f: &mut Formatter) -> Result<(), std::fmt::Error> {
        write!(f, "Remove tag {} in category {}.", self.name, self.category)
    }
}

impl Applyable for UndoCreateTag {
    type To = Diary;
    type Opposite = CreateTag;
    type Res = Self;
    fn apply(self, diary: &mut Diary) -> Result<(Self::Res, Self::Opposite), CmdError> {
        if let Ok(mut tagforest) = diary.tag.write() {
            let tagtree = tagforest.get_mut(&self.category).ok_or_else(|| {
                CmdError::WhileApply(self.to_string(), "Category does not exist.".to_string())
            })?;
            let mut res = self.clone();
            if self.remove_category {
                // Remove the whole category, but only if the
                // tag to remove is the only tag in it.
                if tagtree.len() == 2 {
                    if let Some(tag) = tagtree.remove(&self.name) {
                        tagforest.remove(&self.category);
                        let (category, name, parent, content) = (
                            self.category,
                            self.name,
                            tag.parent().to_owned(),
                            tag.content,
                        );
                        Ok((
                            res,
                            CreateTag {
                                category,
                                name,
                                parent,
                                content,
                                category_inserted: false,
                            },
                        ))
                    } else {
                        Err(CmdError::WhileApply(
                            self.to_string(),
                            "Tag to remove does not exist.".to_string(),
                        ))
                    }
                } else {
                    Err(CmdError::WhileApply(
                        self.to_string(),
                        "Category should be removed, \
                         but Tag to remove is not the only one in category."
                            .to_string(),
                    ))
                }
            } else {
                // Remove only the tag
                match if self.remove_really {
                    let parent = tagtree.parent_of(&self.name).cloned();
                    let ires = tagtree.remove(&self.name);
                    if let Some(parent) = parent {
                        res.parent = parent.name().to_owned();
                    }
                    ires
                } else {
                    tagtree.get(&self.name).cloned()
                } {
                    Some(node) => {
                        let (category, name) = (self.category, self.name);
                        Ok((
                            res,
                            CreateTag {
                                category,
                                name,
                                parent: node.parent().to_owned(),
                                content: node.content,
                                category_inserted: false,
                            },
                        ))
                    }
                    None => Err(CmdError::WhileApply(
                        self.to_string(),
                        "Tag to remove does not exist.".to_string(),
                    )),
                }
            }
        } else {
            Err(CmdError::Disconnected)
        }
    }
}

impl<M: Model> NotifyBegin<M> for UndoCreateTag {
    fn whom_begin(&self) -> ToNotify {
        ToNotify::TAGTREE
    }
    fn notify_tagtree_begin(&self, listener: &mut M::TagTree) {
        if self.remove_category {
            listener.begin_remove_category(&self.category);
        } else {
            listener.begin_remove_tag(&self.category, &self.name);
        }
    }
}

impl<M: Model> NotifyEnd<M> for UndoCreateTag {
    fn whom_end(&self) -> ToNotify {
        ToNotify::TAGTREE
    }
    fn notify_tagtree_end(&self, listener: &mut M::TagTree) {
        if self.remove_category {
            listener.end_remove_category(&self.category);
        } else {
            listener.end_remove_tag(&self.category, &self.name);
            listener.end_set_tag(&self.category, &self.parent);
        }
    }
}

/// Removes the tag `tagname` of `category` from all dreams in `dreamlist`.
///
/// Returns a list containing the ids of the dreams from which the tag has been removed
/// together with information about whether the tag was present in strong
/// or weak form. This information is given as a pair of booleans,
/// the first one indicating whether the tag was present in strong form,
/// the second one indicationg whether it was present in weak form.
///
/// By giving this list to [`restore_tag_in_dreams`], the removed
/// tags can be exactly restored.
pub fn delete_tag_in_all_dreams(
    dreamlist: &mut DreamList,
    category: &str,
    tagname: &TagStr,
) -> Vec<(DreamID, (bool, bool))> {
    let mut res = Vec::new();
    for (&id, dream) in dreamlist.iter_mut() {
        let occurence = dream.d.remove_tag(category, tagname);
        if occurence.0 || occurence.1 {
            res.push((id, occurence));
        }
    }

    res
}

/// Adds the tag `tagname` of `category` to all dreams in `dreamlist`
/// whose id is contained in `occurences`.
///
/// The parameter `occurences` further must contain information about
/// whether to add the strong or weak form of the tag.
/// This information is given as a pair of booleans,
/// the first one indicating whether the tag shall be added in strong form,
/// the second one indicationg whether it shall be added in weak form.
///
/// E.g. one can pass the list returned from [`delete_tag_in_all_dreams`]
/// in order restore all tags deleted by this function.
pub fn restore_tag_in_dreams(
    dreamlist: &mut DreamList,
    category: &str,
    tagname: &TagStr,
    occurences: &[(DreamID, (bool, bool))],
) {
    for &(id, (strong, weak)) in occurences {
        if let Some(dream) = dreamlist.get_mut(id) {
            if strong {
                dream.d.add_tag(
                    category,
                    TagSpec::new(tagname.to_owned(), OccurMode::Strong),
                );
            }
            if weak {
                dream
                    .d
                    .add_tag(category, TagSpec::new(tagname.to_owned(), OccurMode::Weak));
            }
        }
    }
}

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
/// A Command that deletes a a specified tag.
///
/// The children of the tag are either deleted together with it
/// or moved to the parent of the deleted tag (i.e. one level down in the tree).
///
/// The tag (and possibly its children) will also be removed from every
/// dream in the dreamlist that contains this tag, either in strong or in weak form.
pub struct DeleteTag {
    /// The category in which the tag shall be deleted
    category: String,
    /// The name of the tag to be deleted
    name: TagName,
    /// Indicates whether the tag is intended to be deleted together with its children.
    ///
    /// If this is `false` but the tag has children nevertheless, the command
    /// will refuse to do anything.
    with_children: bool,
}

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
/// A helper struct that takes care of notifying models about the changes
/// induced by a [`DeleteTag`] command.
pub struct DeleteTagNotifier {
    category: String,
    name: TagName,
    parent: TagName,
    changed_dreams: Vec<DreamID>,
}

impl DeleteTag {
    /// Constructs a new command deleting tag `name` in `category`
    /// together with all its children.
    pub fn new_with_children(category: String, name: TagName) -> DeleteTag {
        DeleteTag {
            category,
            name,
            with_children: true,
        }
    }
    /// Constructs a whole set of commands, which together will delete the tag `name`
    /// in `category` without deleting its children.
    ///
    /// This is achieved by first returning a vec of [`ReparentTag`] commands
    /// which move the children of the tag to delete up to its parents,
    /// and finally the `DeleteTag` command which deletes the tag itself.
    ///
    /// ## Attention
    ///
    /// __These commands must be executed in this order__.
    pub fn new_without_children(
        category: String,
        name: TagName,
        tagforest: &TagForest,
    ) -> Option<(Vec<ReparentTag>, DeleteTag)> {
        if let Some((parent, children)) = tagforest
            .get(&category)
            .and_then(|tree| tree.get(&name))
            .map(|node| (node.parent().to_owned(), node.children().clone()))
        {
            Some((
                children
                    .into_iter()
                    .map(|child| ReparentTag::new(category.clone(), child, parent.clone()))
                    .collect(),
                DeleteTag {
                    category,
                    name,
                    with_children: false,
                },
            ))
        } else {
            None
        }
    }
}

impl Display for DeleteTag {
    fn fmt(&self, f: &mut Formatter) -> Result<(), std::fmt::Error> {
        write!(f, "Delete tag {} in category {}", self.name, self.category)
    }
}

/// Takes a HashMap containing vecs of DreamID-Occurence-pairs and extracts
/// any `DreamID` found in this structure.
#[allow(clippy::type_complexity)]
fn flatten_changed_dreams(
    changed_dreams: &HashMap<TagName, Vec<(DreamID, (bool, bool))>>,
) -> Vec<DreamID> {
    let mut res = Vec::new();
    for it in changed_dreams.values() {
        res.extend(it.iter().map(|(id, _)| id));
    }
    res
}

impl Applyable for DeleteTag {
    type To = Diary;
    type Res = DeleteTagNotifier;
    type Opposite = UndoDeleteTag;
    fn apply(self, mut diary: &mut Self::To) -> Result<(Self::Res, Self::Opposite), CmdError> {
        let mut stage = stage_diary!(diary;; TAG, DL);
        if let (Ok(mut tagforest), Ok(mut dreamlist)) = lock!(stage;; TagForest, DreamList) {
            let tagtree = tagforest.get_mut(&self.category).ok_or_else(|| {
                CmdError::WhileApply(self.to_string(), "Category does not exist.".to_string())
            })?;

            if !self.with_children
                && tagtree
                    .children_count_of(&self.name)
                    .map(|c| c > 0)
                    .unwrap_or(false)
            {
                return Err(CmdError::WhileApply(
                    self.to_string(),
                    "Tag to delete still has children.".to_string(),
                ));
            }
            // For every tag which is deleted in the process, `changed_dreams`
            // will store the id of every dream that contained this tag,
            // together with the information whether the tag was present in strong
            // or weak form.
            let mut changed_dreams = HashMap::new();
            // Removal of the tag together with all its children
            let parent = tagtree
                .get(&self.name)
                .map(|n| n.parent().to_owned())
                // `parent` will only be used when the tag `self.name` exists,
                // in which case the option here will be `Some`
                // so this `unwrap_or` is fine.
                .unwrap_or(TagName::root());
            if let Some(subtree) = tagtree.take_subtree(&self.name) {
                // Remove the tag from all dreams containing it
                // and store the information about the changes to the dreams.
                for it in subtree.iter_unordered() {
                    changed_dreams.insert(
                        it.name().to_owned(),
                        delete_tag_in_all_dreams(&mut dreamlist, &self.category, it.name()),
                    );
                }

                Ok((
                    DeleteTagNotifier {
                        category: self.category.clone(),
                        name: self.name,
                        parent: parent.clone(),
                        changed_dreams: flatten_changed_dreams(&changed_dreams),
                    },
                    UndoDeleteTag {
                        category: self.category,
                        parent,
                        tags_to_restore: subtree,
                        dreams_to_restore: changed_dreams,
                    },
                ))
            } else {
                Err(CmdError::WhileApply(
                    self.to_string(),
                    "Tag to delete has not been found.".to_string(),
                ))
            }
        } else {
            Err(CmdError::Disconnected)
        }
    }
}

impl<M: Model> NotifyBegin<M> for DeleteTag {
    fn whom_begin(&self) -> ToNotify {
        // We do not yet know which dreams contain the tag to delete (and possibly
        // its children), so we can only notify the Tagtree.
        ToNotify::TAGTREE
    }
    fn notify_tagtree_begin(&self, listener: &mut M::TagTree) {
        listener.begin_remove_tag(&self.category, &self.name);
    }
}

impl<M: Model> NotifyEnd<M> for DeleteTagNotifier {
    fn whom_end(&self) -> ToNotify {
        ToNotify::DREAMLIST | ToNotify::TAGTREE
    }
    fn notify_dreamlist_end(&self, listener: &mut M::DreamListModel) {
        // For every dream that had this tag, we only need to inform
        // that something of the dream has just changed.
        //
        // For Qt, this completely suffices, since it does not want to be informed
        // about the "beginning of a change of an item".
        for &it in &self.changed_dreams {
            listener.end_set_dream(it)
        }
    }
    fn notify_tagtree_end(&self, listener: &mut M::TagTree) {
        listener.end_remove_tag(&self.category, &self.name);
        listener.end_set_tag(&self.category, &self.parent)
    }
}

#[derive(Debug, Clone)]
/// A Command undoing a [`DeleteTag`].
///
/// It will restore the tag itself and re-insert its children. This means,
/// if the tag has been deleted together with its children, they are created
/// newly. If the children had been moved to the parent of the deleted tag,
/// the will me moved to the tag itself again.
pub struct UndoDeleteTag {
    /// The category of the deleted tag
    category: String,
    /// The parent of the deleted tag
    parent: TagName,
    /// A structure holding tags that must be restored.
    ///
    /// If the tag had been deleted together with its children,
    /// this will contain the whole Subtree beginning with the deleted tag
    /// (as the [`Right`](either::Either::Right) variant of the enum).
    /// Otherwise this will contain the deleted tag and a list of names of its old children
    /// (as the [`Left`](either::Either::Left) variant of the enum).
    tags_to_restore: TagTree,
    /// A structure holding the dreams from which the tag had been deleted.
    ///
    /// This is a Map that holds for every deleted tag a list of dreams that had this tag.
    /// As we need to remember whether the tag had been present in strong or weak form
    /// (or in both!), the list contains pairs of `DreamID`s and a further pair of two
    /// booleans. The first element of this boolean pair specifies whether the tag
    /// was present in strong form. The second element specifies whether the tag
    /// was present in weak form.
    #[allow(clippy::type_complexity)]
    dreams_to_restore: HashMap<TagName, Vec<(DreamID, (bool, bool))>>,
}

impl UndoDeleteTag {
    /// Returns the name of the tag that has been deleted.
    ///
    /// If the tag has been deleted together with its children,
    /// this is the root of the subtree that has been removed.
    fn name(&self) -> &TagStr {
        self.tags_to_restore
            .get("")
            // Won't panic because root tag exists
            .unwrap()
            .child(0)
            // Won't panic because tagtree has been taken from a greater
            // TagTree, so it coantains at least one nontrivial tag.
            .expect("Trivial Subtree of non-empty TagTree")
    }
}

impl Display for UndoDeleteTag {
    fn fmt(&self, f: &mut Formatter) -> Result<(), std::fmt::Error> {
        write!(
            f,
            "Undo deletion of tag {} in category {}",
            self.name(),
            self.category
        )
    }
}

impl Applyable for UndoDeleteTag {
    type To = Diary;
    type Res = UndoDeleteTag;
    type Opposite = DeleteTag;
    fn apply(self, mut diary: &mut Self::To) -> Result<(Self::Res, Self::Opposite), CmdError> {
        let mut stage = stage_diary!(diary;; TAG, DL);
        if let (Ok(mut tagforest), Ok(mut dreamlist)) = lock!(stage;; TagForest, DreamList) {
            let copy = self.clone();
            let tagtree = tagforest.get_mut(&self.category).ok_or_else(|| {
                CmdError::WhileApply(self.to_string(), "Category does not exist.".to_string())
            })?;

            // Restore the deleted tags.
            //
            // The field `dreams_to_restore` already contains all information
            // we need, so no distinction between the deletion of a
            // tag with or without its children is necessary.
            for (name, idlist) in &self.dreams_to_restore {
                restore_tag_in_dreams(&mut dreamlist, &self.category, name, idlist);
            }
            let name = self.name().to_owned();
            // Tag had been removed with children

            // Insert the whole subtree that has been deleted
            tagtree.insert_subtree(self.tags_to_restore, &self.parent);
            Ok((
                copy,
                DeleteTag {
                    category: self.category,
                    name,
                    with_children: true,
                },
            ))
        } else {
            Err(CmdError::Disconnected)
        }
    }
}

impl<M: Model> NotifyBegin<M> for UndoDeleteTag {
    fn whom_begin(&self) -> ToNotify {
        ToNotify::DREAMLIST | ToNotify::TAGTREE
    }
    fn notify_dreamlist_begin(&self, listener: &mut M::DreamListModel) {
        for it in self.dreams_to_restore.values() {
            for (jt, _) in it {
                listener.begin_set_dream(*jt);
            }
        }
    }
    fn notify_tagtree_begin(&self, listener: &mut M::TagTree) {
        let mut it = self.tags_to_restore.iter();
        // Skip the root of the subtree
        // which is always the empty node.
        it.next();
        if let Some(base_tag) = it.next() {
            // The base tag of the subtree, i.e. the one that
            // should originally be deleted (together with all its children)
            // must be treated special, as it is inserted as a child
            // of its old parent, while its parent in the subtree
            // is the empty node "".
            listener.begin_create_tag(&self.category, base_tag.name(), &self.parent);
            for item in it {
                // Notify about the insertion of the remaining tags in the subtree.
                listener.begin_create_tag(&self.category, item.name(), item.parent());
            }
        }
    }
}

impl<M: Model> NotifyEnd<M> for UndoDeleteTag {
    // For commentary see the implementation of `NotifyBegin`
    fn whom_end(&self) -> ToNotify {
        ToNotify::DREAMLIST | ToNotify::TAGTREE
    }
    fn notify_dreamlist_end(&self, listener: &mut M::DreamListModel) {
        for it in self.dreams_to_restore.values() {
            for (jt, _) in it {
                listener.end_set_dream(*jt);
            }
        }
    }
    fn notify_tagtree_end(&self, listener: &mut M::TagTree) {
        let mut it = self.tags_to_restore.iter();
        it.next();
        if let Some(root_tag) = it.next() {
            listener.end_create_tag(&self.category, root_tag.name(), &self.parent);
            for item in it {
                listener.end_create_tag(&self.category, item.name(), item.parent());
            }
        }
    }
}

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
/// Creates a new tag category with a given name.
pub struct CreateCategory {
    /// The name of the category to create
    name: String,
}

impl CreateCategory {
    /// Constructs a new `CreateCategory` creating the tag
    /// category `name`.
    pub fn new(name: String) -> CreateCategory {
        CreateCategory { name }
    }
}

impl Display for CreateCategory {
    fn fmt(&self, f: &mut Formatter) -> Result<(), std::fmt::Error> {
        write!(f, "Create tag category {}", self.name)
    }
}

impl Applyable for CreateCategory {
    type To = Diary;
    type Res = Self;
    type Opposite = DeleteCategory;
    fn apply(self, diary: &mut Self::To) -> Result<(Self::Res, Self::Opposite), CmdError> {
        if let Ok(mut tagforest) = diary.tag.write() {
            if !tagforest.contains_key(&self.name) {
                tagforest.insert(self.name.clone(), CacheTree::new());
                let opp = DeleteCategory {
                    name: self.name.clone(),
                };
                Ok((self, opp))
            } else {
                Err(CmdError::WhileApply(
                    self.to_string(),
                    "Category to create already exists".to_string(),
                ))
            }
        } else {
            Err(CmdError::Disconnected)
        }
    }
}

impl<M: Model> NotifyBegin<M> for CreateCategory {
    fn whom_begin(&self) -> ToNotify {
        ToNotify::TAGTREE
    }
    fn notify_tagtree_begin(&self, listener: &mut M::TagTree) {
        listener.begin_create_category(&self.name);
    }
}

impl<M: Model> NotifyEnd<M> for CreateCategory {
    fn whom_end(&self) -> ToNotify {
        ToNotify::TAGTREE
    }
    fn notify_tagtree_end(&self, listener: &mut M::TagTree) {
        listener.end_create_category(&self.name);
    }
}

/// Deletes `category` in all dreams.
///
/// Returns the ids of dreams which contained the category.
fn delete_category_in_all_dreams(dreamlist: &mut DreamList, category: &str) -> Vec<DreamID> {
    let mut res = Vec::new();
    for (&id, dream) in dreamlist {
        if dream.d.remove_category(category).is_some() {
            res.push(id);
        }
    }
    res
}

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
/// Deletes an empty tag category with a given name.
///
/// The category to delete must exist, otherwise nothing is done
/// and an error is returned.
///
/// If the category contains nontrivial tags (i.e. tags that are not the
/// root tag), nothing will be done.
pub struct DeleteCategory {
    /// The name of the category to delete
    name: String,
}

impl Display for DeleteCategory {
    fn fmt(&self, f: &mut Formatter) -> Result<(), std::fmt::Error> {
        write!(f, "Delete tag category {}", self.name)
    }
}

impl DeleteCategory {
    /// Constructs a new `DeleteCategory` deleting the tag
    /// category `name`.
    pub fn new(name: String) -> DeleteCategory {
        DeleteCategory { name }
    }
}

impl Applyable for DeleteCategory {
    type To = Diary;
    type Res = Self;
    type Opposite = CreateCategory;
    fn apply(self, mut diary: &mut Self::To) -> Result<(Self::Res, Self::Opposite), CmdError> {
        let mut stage = stage_diary!(diary;; TAG, DL);
        if let (Ok(mut tagforest), Ok(mut dreamlist)) = lock!(stage;; TagForest, DreamList) {
            if tagforest
                .get_mut(&self.name)
                .ok_or(CmdError::WhileApply(
                    self.to_string(),
                    "Category to delete does not exist".to_string(),
                ))?
                .len()
                == 1
            {
                tagforest.remove(&self.name);
                delete_category_in_all_dreams(&mut dreamlist, &self.name);
                let opp = CreateCategory {
                    name: self.name.clone(),
                };
                Ok((self, opp))
            } else {
                Err(CmdError::WhileApply(
                    self.to_string(),
                    "Category to delete is not empty".to_string(),
                ))
            }
        } else {
            Err(CmdError::Disconnected)
        }
    }
}

impl<M: Model> NotifyBegin<M> for DeleteCategory {
    fn whom_begin(&self) -> ToNotify {
        ToNotify::TAGTREE
    }
    fn notify_tagtree_begin(&self, listener: &mut M::TagTree) {
        listener.begin_remove_category(&self.name);
    }
}

impl<M: Model> NotifyEnd<M> for DeleteCategory {
    fn whom_end(&self) -> ToNotify {
        ToNotify::TAGTREE
    }
    fn notify_tagtree_end(&self, listener: &mut M::TagTree) {
        listener.end_remove_category(&self.name);
    }
}

pub fn rename_tag_in_all_dreams(
    dreamlist: &mut DreamList,
    category: &str,
    old_name: &TagStr,
    new_name: &TagStr,
) -> Vec<DreamID> {
    let mut res = Vec::new();
    for (&id, dream) in dreamlist.iter_mut() {
        if dream.d.rename_tag(category, old_name, new_name.to_owned()) {
            res.push(id);
        }
    }

    res
}

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct RenameTag {
    category: String,
    old_name: TagName,
    new_name: TagName,
}

impl Display for RenameTag {
    fn fmt(&self, f: &mut Formatter) -> Result<(), std::fmt::Error> {
        write!(
            f,
            "Rename tag {} in category {} to {}",
            self.old_name, self.category, self.new_name
        )
    }
}

impl RenameTag {
    pub fn new(category: String, old_name: TagName, new_name: TagName) -> Self {
        RenameTag {
            category,
            old_name,
            new_name,
        }
    }
}

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct RenameTagNotifier {
    category: String,
    old_name: TagName,
    new_name: TagName,
    affected_dreams: Vec<DreamID>,
}

impl Applyable for RenameTag {
    type To = Diary;
    type Res = RenameTagNotifier;
    type Opposite = Self;

    fn apply(self, mut diary: &mut Self::To) -> Result<(Self::Res, Self::Opposite), CmdError> {
        let mut res = self.clone();
        let new_name = self.new_name.clone();
        let mut stage = stage_diary!(diary;; TAG, DL);
        if let (Ok(mut tagforest), Ok(mut dreamlist)) = lock!(stage;; TagForest, DreamList) {
            if let Some(tagtree) = tagforest.get_mut(&self.category) {
                if !tagtree.rename(&self.old_name, new_name) {
                    return Err(CmdError::WhileApply(
                        self.to_string(),
                        "Tag to rename does not exist".to_string(),
                    ));
                }
                let affected_dreams = rename_tag_in_all_dreams(
                    &mut dreamlist,
                    &self.category,
                    &self.old_name,
                    &self.new_name,
                );
                std::mem::swap(&mut res.old_name, &mut res.new_name);
                Ok((
                    RenameTagNotifier {
                        category: self.category,
                        old_name: self.old_name,
                        new_name: self.new_name,
                        affected_dreams,
                    },
                    res,
                ))
            } else {
                Err(CmdError::WhileApply(
                    self.to_string(),
                    "Category in which to rename tag does not exist.".to_string(),
                ))
            }
        } else {
            Err(CmdError::Disconnected)
        }
    }
}

impl<M: Model> NotifyBegin<M> for RenameTag {
    fn whom_begin(&self) -> ToNotify {
        ToNotify::TAGTREE
    }
    fn notify_tagtree_begin(&self, listener: &mut M::TagTree) {
        listener.begin_rename_tag(&self.category, &self.old_name, &self.new_name);
    }
}

impl<M: Model> NotifyEnd<M> for RenameTagNotifier {
    fn whom_end(&self) -> ToNotify {
        ToNotify::TAGTREE | ToNotify::DREAMLIST
    }
    fn notify_tagtree_end(&self, listener: &mut M::TagTree) {
        listener.end_rename_tag(&self.category, &self.old_name, &self.new_name);
    }
    fn notify_dreamlist_end(&self, listener: &mut M::DreamListModel) {
        for &id in &self.affected_dreams {
            listener.end_set_dream(id);
        }
    }
}

pub fn change_tag_category_in_all_dreams(
    dreamlist: &mut DreamList,
    old_category: &str,
    new_category: &str,
    tagname: &TagStr,
) -> Vec<DreamID> {
    let mut res = Vec::new();
    for (&id, dream) in dreamlist.iter_mut() {
        if dream
            .d
            .change_tag_category(old_category, new_category, tagname)
        {
            res.push(id);
        }
    }

    res
}

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct ChangeTagCategory {
    old_category: String,
    new_category: String,
    tagname: TagName,
}

impl Display for ChangeTagCategory {
    fn fmt(&self, f: &mut Formatter) -> Result<(), std::fmt::Error> {
        write!(
            f,
            "Change category of tag {} from {} to {}",
            self.tagname, self.old_category, self.new_category
        )
    }
}

impl ChangeTagCategory {
    pub fn new(old_category: String, new_category: String, tagname: TagName) -> Self {
        ChangeTagCategory {
            old_category,
            new_category,
            tagname,
        }
    }
}

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct ChangeTagCategoryNotifier {
    old_category: String,
    new_category: String,
    tagname: TagName,
    affected_dreams: Vec<DreamID>,
    old_parent: TagName,
}

impl Applyable for ChangeTagCategory {
    type To = Diary;
    type Res = ChangeTagCategoryNotifier;
    type Opposite = Self;
    fn apply(self, mut diary: &mut Self::To) -> Result<(Self::Res, Self::Opposite), CmdError> {
        let mut stage = stage_diary!(diary;; TAG, DL);
        if let (Ok(mut tagforest), Ok(mut dreamlist)) = lock!(stage;; TagForest, DreamList) {
            let affected_dreams;
            let old_parent;
            let tree_to_change = if let Some(tagtree) = tagforest.get_mut(&self.old_category) {
                affected_dreams = change_tag_category_in_all_dreams(
                    &mut dreamlist,
                    &self.old_category,
                    &self.new_category,
                    &self.tagname,
                );
                let ires = tagtree.take_subtree(&self.tagname);

                old_parent = tagtree
                    .get(&self.tagname)
                    .map(|n| n.parent().to_owned())
                    // `parent` will only be used when the tag `self.name` exists,
                    // in which case the option here will be `Some`
                    // so this `unwrap_or` is fine.
                    .unwrap_or_default();
                ires
            } else {
                return Err(CmdError::WhileApply(
                    self.to_string(),
                    "Category to take tag from does not exist".to_string(),
                ));
            };
            if let (Some(tree_to_change), Some(new_tagtree)) =
                (tree_to_change, tagforest.get_mut(&self.new_category))
            {
                new_tagtree.insert_subtree(tree_to_change, &TagName::root());
            }
            let res = ChangeTagCategoryNotifier {
                old_category: self.old_category.clone(),
                new_category: self.new_category.clone(),
                tagname: self.tagname.clone(),
                affected_dreams,
                old_parent
            };
            Ok((
                res,
                ChangeTagCategory {
                    old_category: self.new_category,
                    new_category: self.old_category,
                    tagname: self.tagname,
                },
            ))
        } else {
            Err(CmdError::Disconnected)
        }
    }
}

impl<M: Model> NotifyBegin<M> for ChangeTagCategory {
    fn whom_begin(&self) -> ToNotify {
        ToNotify::TAGTREE
    }
    fn notify_tagtree_begin(&self, listener: &mut M::TagTree) {
        listener.begin_remove_tag(&self.old_category, &self.tagname);
        listener.begin_create_tag(&self.new_category, &self.tagname, &TagName::root());
    }
}

impl<M: Model> NotifyEnd<M> for ChangeTagCategoryNotifier {
    fn whom_end(&self) -> ToNotify {
        ToNotify::TAGTREE | ToNotify::DREAMLIST
    }
    fn notify_tagtree_end(&self, listener: &mut M::TagTree) {
        listener.end_remove_tag(&self.old_category, &self.tagname);
        listener.end_create_tag(&self.new_category, &self.tagname, &TagName::root());
    }
    fn notify_dreamlist_end(&self, listener: &mut M::DreamListModel) {
        for &id in &self.affected_dreams {
            listener.end_set_dream(id);
        }
    }
}

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct ReparentTag {
    category: String,
    name: TagName,
    new_parent: TagName,
    old_parent: Option<TagName>,
}

impl Display for ReparentTag {
    fn fmt(&self, f: &mut Formatter) -> Result<(), std::fmt::Error> {
        write!(
            f,
            "Reparent tag {} in category {} to new parent {}",
            self.name, self.category, self.new_parent
        )
    }
}

impl ReparentTag {
    pub fn new(category: String, name: TagName, new_parent: TagName) -> Self {
        ReparentTag {
            category,
            name,
            new_parent,
            old_parent: None
        }
    }
}

impl Applyable for ReparentTag {
    type To = Diary;
    type Res = Self;
    type Opposite = Self;
    fn apply(self, mut diary: &mut Self::To) -> Result<(Self::Res, Self::Opposite), CmdError> {
        let mut stage = stage_diary!(diary;; TAG);
        if let Ok(mut tagforest) = write_single!(stage, TagForest) {
            if let Some(cat) = tagforest.get_mut(&self.category) {
                let mut res = self.clone();
                let old_parent = cat
                    .parent_of(&self.name)
                    .ok_or_else(|| {
                        CmdError::WhileApply(
                            self.to_string(),
                            "Tag cannot be reparented. Tag does not exist.".to_string(),
                        )
                    })?
                    .name()
                    .to_owned();


                if cat.reparent(&self.name, &self.new_parent) {
                    let mut opposite = self;
                    opposite.new_parent = old_parent.clone();
                    res.old_parent = Some(old_parent);
                    Ok((res, opposite))
                } else {
                    Err(CmdError::WhileApply(
                        self.to_string(),
                        "Tag cannot be reparented. Tag or new parent do not exist.".to_string(),
                    ))
                }
            } else {
                Err(CmdError::WhileApply(
                    self.to_string(),
                    "Given category does not exist.".to_string(),
                ))
            }
        } else {
            panic!("GUI thread has hung up")
        }
    }
}

impl<M: Model> NotifyBegin<M> for ReparentTag {
    fn whom_begin(&self) -> ToNotify {
        ToNotify::TAGTREE
    }
    fn notify_tagtree_begin(&self, listener: &mut M::TagTree) {
        listener.begin_reparent_tag(&self.category, &self.name, &self.new_parent);
    }
}

impl<M: Model> NotifyEnd<M> for ReparentTag {
    fn whom_end(&self) -> ToNotify {
        ToNotify::TAGTREE
    }
    fn notify_tagtree_end(&self, listener: &mut M::TagTree) {
        listener.end_reparent_tag(&self.category, &self.name, &self.new_parent);
        if let Some(old_parent) = &self.old_parent {
            listener.end_set_tag(&self.category, old_parent);
        }
    }
}

/// Sets the content of a [`Tag`](crate::Tag), like the tag description.
#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct SetTag {
    /// The category of the tag
    category: String,
    /// The name of the tag
    name: TagName,
    /// The new content
    content: Tag,
}

impl SetTag {
    /// Creates a new `SetTag` command which sets the content of
    /// tag `name` in `category` to `content`.
    pub fn new(category: String, name: TagName, content: Tag) -> Self {
        SetTag {
            category,
            name,
            content,
        }
    }
}

impl Display for SetTag {
    fn fmt(&self, f: &mut Formatter) -> Result<(), std::fmt::Error> {
        write!(f, "Set tag {} in category {}", self.name, self.category)
    }
}

impl Applyable for SetTag {
    type To = Diary;
    type Res = Self;
    type Opposite = Self;
    fn apply(mut self, mut diary: &mut Self::To) -> Result<(Self::Res, Self::Opposite), CmdError> {
        let mut stage = stage_diary!(diary;; TAG);
        if let Ok(mut tagforest) = write_single!(stage, TagForest) {
            let tagtree = tagforest.get_mut(&self.category).ok_or_else(|| {
                CmdError::WhileApply(self.to_string(), "Category does not exist".to_string())
            })?;
            let tag = tagtree.get_mut(&self.name).ok_or_else(|| {
                CmdError::WhileApply(self.to_string(), "Tag to edit does not exist".to_string())
            })?;
            let res = self.clone();
            std::mem::swap(&mut tag.content, &mut self.content);
            Ok((res, self))
        } else {
            Err(CmdError::Disconnected)
        }
    }
}

impl<M: Model> NotifyBegin<M> for SetTag {
    fn whom_begin(&self) -> ToNotify {
        ToNotify::empty()
    }
}

impl<M: Model> NotifyEnd<M> for SetTag {
    fn whom_end(&self) -> ToNotify {
        ToNotify::empty()
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use chrono::NaiveDate;
    use std::str::FromStr;

    use crate::TagForest;

    use super::super::core::Applyable;
    use super::super::tests::standard_diary;
    use super::{CreateCategory, CreateTag, DeleteCategory, DeleteTag};
    use crate::read_unstaged;
    use crate::DreamID;

    #[test]
    fn create_tag() {
        let mut diary = Diary::new();
        let cmd = vec![
            CreateTag::new_at_root(
                "NichtStandardPersonen".to_string(),
                TagName::from_str("Movies").unwrap(),
            ),
            CreateTag::new(
                "NichtStandardPersonen".to_string(),
                TagName::from_str("Jaime Lannister").unwrap(),
                TagName::from_str("Movies").unwrap(),
            ),
            CreateTag::new(
                "NichtStandardPersonen".to_string(),
                TagName::from_str("Brienne von Tarth").unwrap(),
                TagName::from_str("Movies").unwrap(),
            ),
        ];
        let mut red = Vec::new();
        for (i, it) in cmd.into_iter().enumerate() {
            red.push(
                it.apply(&mut diary)
                    .expect(&format!("Could not apply cmd {}", i))
                    .1,
            );
            let tree = read_unstaged!(diary, TagForest).unwrap();
            let t = &tree["NichtStandardPersonen"];
            match i {
                0 => {
                    assert!(t.node_exists("Movies"));
                }
                1 => {
                    assert_eq!(
                        t.parent_of("Jaime Lannister")
                            .expect("Second tag not created.")
                            .name(),
                        "Movies"
                    );
                }
                2 => {
                    assert_eq!(
                        t.parent_of("Brienne von Tarth")
                            .expect("Third tag not created.")
                            .name(),
                        "Movies"
                    );
                }
                _ => unreachable!(),
            }
        }

        assert!(red[0].remove_category);
        assert!(!red[1].remove_category);
        for (i, it) in red.into_iter().enumerate().rev() {
            let thisred = it
                .apply(&mut diary)
                .expect(&format!("Could not redo cmd {}", i))
                .1;
            let tree = read_unstaged!(diary, TagForest).unwrap();
            match i {
                2 => {
                    let t = &tree["NichtStandardPersonen"];
                    assert_eq!(
                        t.parent_of("Jaime Lannister")
                            .expect("Second tag already removed.")
                            .name(),
                        "Movies"
                    );
                    assert!(!t.node_exists("Brienne von Tarth"));
                }
                1 => {
                    let t = &tree["NichtStandardPersonen"];
                    assert!(t.node_exists("Movies"));
                    assert!(!t.node_exists("Jaime Lannister"));
                    assert_eq!(t.len(), 2);
                    assert_eq!(&thisred.parent, "Movies");
                    assert_eq!(&thisred.name, "Jaime Lannister");
                }
                0 => {
                    assert!(tree.get("NichtStandardPersonen").is_none());
                    assert_eq!(&thisred.category, "NichtStandardPersonen");
                    assert_eq!(&thisred.name, "Movies");
                }
                _ => unreachable!(),
            }
        }
    }

    #[test]
    fn delete_tag_with_children() {
        let mut diary = standard_diary();

        {
            let forest = diary.tag.read().unwrap();
            let t = &forest["Personen"];
            let dreamlist = diary.dl.read().unwrap();

            assert!(t.node_exists("Movies"));
            assert!(t.node_exists("Star Wars"));
            assert!(t.node_exists("Darth Vader"));
            assert!(t.node_exists("Adventures of Brigsby Bear"));
            assert!(t.node_exists("Books"));

            let date = NaiveDate::from_ymd(1054, 07, 05);
            assert!(dreamlist
                .get(DreamID::new(date, 0))
                .unwrap()
                .d
                .has_tag("Personen", &TagSpec::from_str("Darth Vader").unwrap()));
            assert!(dreamlist
                .get(DreamID::new(date, 1))
                .unwrap()
                .d
                .has_tag("Personen", &TagSpec::from_str("Star Wars").unwrap()));
            assert!(dreamlist
                .get(DreamID::new(date, 2))
                .unwrap()
                .d
                .has_tag("Personen", &TagSpec::from_str("Darth Vader*").unwrap()));
            assert!(dreamlist
                .get(DreamID::new(date, 1))
                .unwrap()
                .d
                .has_tag("Personen", &TagSpec::from_str("Heino").unwrap()));
        }

        let cmd = DeleteTag::new_with_children(
            "Personen".to_string(),
            TagName::from_str("Star Wars").unwrap(),
        );
        let (_, red) = cmd.apply(&mut diary).unwrap();

        {
            let forest = diary.tag.read().unwrap();
            let t = &forest["Personen"];
            let dreamlist = diary.dl.read().unwrap();

            assert!(t.node_exists("Movies"));
            assert!(!t.node_exists("Star Wars"));
            assert!(!t.node_exists("Darth Vader"));
            assert!(t.node_exists("Adventures of Brigsby Bear"));
            assert!(t.node_exists("Books"));

            let date = NaiveDate::from_ymd(1054, 07, 05);
            assert!(!dreamlist
                .get(DreamID::new(date, 0))
                .unwrap()
                .d
                .has_tag("Personen", &TagSpec::from_str("Darth Vader").unwrap()));
            assert!(!dreamlist
                .get(DreamID::new(date, 1))
                .unwrap()
                .d
                .has_tag("Personen", &TagSpec::from_str("Star Wars").unwrap()));
            assert!(!dreamlist
                .get(DreamID::new(date, 2))
                .unwrap()
                .d
                .has_tag("Personen", &TagSpec::from_str("Darth Vader*").unwrap()));
            assert!(dreamlist
                .get(DreamID::new(date, 1))
                .unwrap()
                .d
                .has_tag("Personen", &TagSpec::from_str("Heino").unwrap()));
        }

        red.apply(&mut diary).unwrap();

        {
            let forest = diary.tag.read().unwrap();
            let t = &forest["Personen"];
            let dreamlist = diary.dl.read().unwrap();

            assert!(t.node_exists("Movies"));
            assert!(t.node_exists("Star Wars"));
            assert!(t.node_exists("Darth Vader"));
            assert!(t.node_exists("Adventures of Brigsby Bear"));
            assert!(t.node_exists("Books"));

            let date = NaiveDate::from_ymd(1054, 07, 05);
            assert!(dreamlist
                .get(DreamID::new(date, 0))
                .unwrap()
                .d
                .has_tag("Personen", &TagSpec::from_str("Darth Vader").unwrap()));
            assert!(dreamlist
                .get(DreamID::new(date, 1))
                .unwrap()
                .d
                .has_tag("Personen", &TagSpec::from_str("Star Wars").unwrap()));
            assert!(dreamlist
                .get(DreamID::new(date, 2))
                .unwrap()
                .d
                .has_tag("Personen", &TagSpec::from_str("Darth Vader*").unwrap()));
            assert!(dreamlist
                .get(DreamID::new(date, 1))
                .unwrap()
                .d
                .has_tag("Personen", &TagSpec::from_str("Heino").unwrap()));
        }
    }

    #[test]
    fn delete_tag_without_children() {
        let mut diary = standard_diary();

        {
            let forest = diary.tag.read().unwrap();
            let t = &forest["Personen"];
            let dreamlist = diary.dl.read().unwrap();

            assert!(t.node_exists("Movies"));
            assert!(t.node_exists("Star Wars"));
            assert!(t.node_exists("Darth Vader"));
            assert!(t.node_exists("Adventures of Brigsby Bear"));
            assert!(t.node_exists("Books"));

            let date = NaiveDate::from_ymd(1054, 07, 05);
            assert!(dreamlist
                .get(DreamID::new(date, 0))
                .unwrap()
                .d
                .has_tag("Personen", &TagSpec::from_str("Darth Vader").unwrap()));
            assert!(dreamlist
                .get(DreamID::new(date, 1))
                .unwrap()
                .d
                .has_tag("Personen", &TagSpec::from_str("Star Wars").unwrap()));
            assert!(dreamlist
                .get(DreamID::new(date, 2))
                .unwrap()
                .d
                .has_tag("Personen", &TagSpec::from_str("Darth Vader*").unwrap()));
            assert!(dreamlist
                .get(DreamID::new(date, 1))
                .unwrap()
                .d
                .has_tag("Personen", &TagSpec::from_str("Heino").unwrap()));
        }

        let (pre, cmd) = DeleteTag::new_without_children(
            "Personen".to_string(),
            TagName::from_str("Star Wars").unwrap(),
            &diary.tag.read().unwrap(),
        )
        .unwrap();
        let mut red_pre = Vec::new();
        for it in pre {
            red_pre.push(it.apply(&mut diary).unwrap().1);
        }
        let (_, red) = cmd.apply(&mut diary).unwrap();

        {
            let forest = diary.tag.read().unwrap();
            let t = &forest["Personen"];
            let dreamlist = diary.dl.read().unwrap();

            assert!(t.node_exists("Movies"));
            assert!(!t.node_exists("Star Wars"));
            assert!(t.node_exists("Darth Vader"));
            assert_eq!(t.parent_of("Darth Vader").unwrap().name(), "Movies");
            assert_eq!(t.parent_of("Movies").unwrap().name(), "");
            assert!(t.node_exists("Adventures of Brigsby Bear"));
            assert!(t.node_exists("Books"));

            let date = NaiveDate::from_ymd(1054, 07, 05);
            assert!(dreamlist
                .get(DreamID::new(date, 0))
                .unwrap()
                .d
                .has_tag("Personen", &TagSpec::from_str("Darth Vader").unwrap()));
            assert!(!dreamlist
                .get(DreamID::new(date, 1))
                .unwrap()
                .d
                .has_tag("Personen", &TagSpec::from_str("Star Wars").unwrap()));
            assert!(dreamlist
                .get(DreamID::new(date, 2))
                .unwrap()
                .d
                .has_tag("Personen", &TagSpec::from_str("Darth Vader*").unwrap()));
            assert!(dreamlist
                .get(DreamID::new(date, 1))
                .unwrap()
                .d
                .has_tag("Personen", &TagSpec::from_str("Heino").unwrap()));
        }
        red.apply(&mut diary).unwrap();
        for it in red_pre {
            it.apply(&mut diary).unwrap();
        }

        {
            let forest = diary.tag.read().unwrap();
            let t = &forest["Personen"];
            let dreamlist = diary.dl.read().unwrap();

            assert!(t.node_exists("Movies"));
            assert!(t.node_exists("Star Wars"));
            assert!(t.node_exists("Darth Vader"));
            assert!(t.node_exists("Adventures of Brigsby Bear"));
            assert!(t.node_exists("Books"));

            let date = NaiveDate::from_ymd(1054, 07, 05);
            assert!(dreamlist
                .get(DreamID::new(date, 0))
                .unwrap()
                .d
                .has_tag("Personen", &TagSpec::from_str("Darth Vader").unwrap()));
            assert!(dreamlist
                .get(DreamID::new(date, 1))
                .unwrap()
                .d
                .has_tag("Personen", &TagSpec::from_str("Star Wars").unwrap()));
            assert!(dreamlist
                .get(DreamID::new(date, 2))
                .unwrap()
                .d
                .has_tag("Personen", &TagSpec::from_str("Darth Vader*").unwrap()));
            assert!(dreamlist
                .get(DreamID::new(date, 1))
                .unwrap()
                .d
                .has_tag("Personen", &TagSpec::from_str("Heino").unwrap()));
        }
    }

    #[test]
    fn delete_tag_with_children_chain() {
        let mut diary = standard_diary();
        let cmd = DeleteTag::new_with_children(
            "Personen".to_string(),
            TagName::from_str("Movies").unwrap(),
        );
        let (_, red) = cmd.apply(&mut diary).unwrap();

        {
            let forest = diary.tag.read().unwrap();
            let t = &forest["Personen"];

            assert!(!t.node_exists("Movies"));
            assert!(!t.node_exists("Star Wars"));
            assert!(!t.node_exists("Darth Vader"));
            assert!(t.node_exists("Books"));
        }

        red.apply(&mut diary).unwrap();

        {
            let forest = diary.tag.read().unwrap();
            let t = &forest["Personen"];

            assert!(t.node_exists("Movies"));
            assert!(t.node_exists("Star Wars"));
            assert!(t.node_exists("Darth Vader"));
            assert!(t.node_exists("Books"));

            assert_eq!(t.parent_of("Star Wars").unwrap().name(), "Movies");
        }
    }

    #[test]
    fn delete_tag_without_children_chain() {
        let mut diary = standard_diary();
        let (pre, cmd) = DeleteTag::new_without_children(
            "Personen".to_string(),
            TagName::from_str("Movies").unwrap(),
            &diary.tag.read().unwrap(),
        )
        .unwrap();
        let mut red_pre = Vec::new();
        for it in pre {
            red_pre.push(it.apply(&mut diary).unwrap().1);
        }
        let (_, red) = cmd.apply(&mut diary).unwrap();

        {
            let forest = read_unstaged!(diary, TagForest).unwrap();
            let t = &forest["Personen"];

            assert!(!t.node_exists("Movies"));
            assert!(t.node_exists("Star Wars"));
            assert!(t.node_exists("Darth Vader"));
            assert_eq!(t.parent_of("Star Wars").unwrap().name(), "");
            assert_eq!(t.parent_of("Darth Vader").unwrap().name(), "Star Wars");
            assert!(t.node_exists("Adventures of Brigsby Bear"));
            assert!(t.node_exists("Books"));
        }

        red.apply(&mut diary).unwrap();
        for it in red_pre {
            it.apply(&mut diary).unwrap();
        }

        {
            let forest = read_unstaged!(diary, TagForest).unwrap();
            let t = &forest["Personen"];

            assert!(t.node_exists("Movies"));
            assert!(t.node_exists("Star Wars"));
            assert!(t.node_exists("Darth Vader"));
            assert!(t.node_exists("Adventures of Brigsby Bear"));
            assert!(t.node_exists("Books"));

            assert_eq!(t.parent_of("Movies").unwrap().name(), "");
            assert_eq!(t.parent_of("Star Wars").unwrap().name(), "Movies");
            assert_eq!(t.parent_of("Luke Skywalker").unwrap().name(), "Star Wars");
        }
    }

    #[test]
    fn create_delete_category() {
        let mut diary = standard_diary();
        let cmd = CreateCategory::new("Exotic category".to_string());

        let (_, opp) = cmd.apply(&mut diary).unwrap();

        {
            let forest = read_unstaged!(diary, TagForest).unwrap();
            assert!(forest.contains_key("Exotic category"));
        }

        opp.apply(&mut diary).unwrap();

        {
            let forest = read_unstaged!(diary, TagForest).unwrap();
            assert!(!forest.contains_key("Exotic category"));
            assert!(forest.contains_key("Personen"));
        }
    }

    #[test]
    fn try_create_nonempty_category() {
        let mut diary = standard_diary();
        let cmd = CreateCategory::new("Personen".to_string());

        assert!(cmd.apply(&mut diary).is_err());

        {
            let forest = read_unstaged!(diary, TagForest).unwrap();
            assert!(forest
                .get("Personen")
                .expect("Existing category has been overridden")
                .node_exists("Albus Dumbledore"));
        }
    }

    #[test]
    fn try_delete_nonempty_category() {
        let mut diary = standard_diary();
        let cmd = DeleteCategory::new("Personen".to_string());

        assert!(cmd.apply(&mut diary).is_err());

        {
            let forest = read_unstaged!(diary, TagForest).unwrap();
            assert!(forest
                .get("Personen")
                .expect("Nonemtpy category has been deleted")
                .node_exists("Albus Dumbledore"));
        }
    }

    #[test]
    fn set_tag() {
        let mut diary = standard_diary();
        let cmd = SetTag::new(
            "Personen".to_string(),
            TagName::from_str("Albus Dumbledore").unwrap(),
            Tag {
                description: "Headmaster of Hogwarts".to_string(),
            },
        );

        let red = cmd.apply(&mut diary).unwrap().1;

        {
            let tagforest = diary.tag.read().unwrap();
            assert_eq!(
                tagforest
                    .get("Personen")
                    .unwrap()
                    .get("Albus Dumbledore")
                    .unwrap()
                    .content
                    .description
                    .as_str(),
                "Headmaster of Hogwarts"
            );
        }

        red.apply(&mut diary).unwrap();

        {
            let tagforest = diary.tag.read().unwrap();
            assert_eq!(
                tagforest
                    .get("Personen")
                    .unwrap()
                    .get("Albus Dumbledore")
                    .unwrap()
                    .content
                    .description
                    .as_str(),
                ""
            );
        }
    }
}
