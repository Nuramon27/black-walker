//! The program logic of the Black Walker application

#![allow(clippy::or_fun_call)]
#![allow(clippy::unit_arg)]
#![allow(clippy::clone_on_copy)]
#![allow(clippy::match_bool)]
#![allow(clippy::zero_prefixed_literal)]
#![allow(clippy::single_match)]
#![allow(clippy::blocks_in_if_conditions)]
#![deny(clippy::dbg_macro)]
// Removed as soon as we have proper error handling
#![allow(clippy::match_wild_err_arm)]
#![deny(broken_intra_doc_links)]

pub mod cache_tree;
pub mod diary;
pub mod elements;
#[allow(unused)]
mod mconvenience;
pub mod model;
pub mod operate;
pub mod parse;
pub mod prelude;
#[cfg(feature = "search")]
pub mod search;
pub mod settings;
#[cfg(feature = "stat")]
pub mod stat;
pub mod store;
pub mod sync;
pub mod worker;
pub mod feature;

use model::NotifyEnd;

pub use cache_tree::{CacheTree, Node};
pub use diary::{Diary, DreamList, EntrySystem, NightList, TagForest, TagTree};
pub use elements::{
    cmp_coll, Count, Dream, DreamID, DreamItem, Night, Note, RevDate, Tag, TagName, TagSpec,
    TagSpecification, TagStr,
};
pub use operate::core::CmdError;
pub use settings::GS;

/// The [`Diary`] wrapped into an [`undo::Record`]
pub type DiaryRecord<M> = crate::feature::undo::Record<Diary, CmdError, Box<dyn NotifyEnd<M>>>;
