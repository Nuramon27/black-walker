use crate::parse::NUMBERREGEXP;

pub fn compareat(s: &str, tocomp: &str) -> bool {
    if s.len() < tocomp.len() {
        return false;
    }

    for (lt, rt) in Iterator::zip(s.chars(), tocomp.chars()) {
        if lt != rt {
            return false;
        }
    }
    true
}

pub fn numberat(s: &str) -> Option<&str> {
    match NUMBERREGEXP.find(s) {
        Some(m) => Some(m.as_str()),
        None => None,
    }
}

pub mod latex {
    use std::fmt;
    use std::fmt::{Display, Formatter};

    use lazy_static::lazy_static;
    use regex::{Captures, Regex, Match};

    use crate::operate::core::Criticality;
    use crate::parse::CommaSep;

    #[derive(Debug, Clone, PartialEq, Eq, Hash)]
    pub enum TeXError {
        ArgumentNrError { exp: usize, got: usize },
        InstructionError { exp: Vec<String>, got: String },
        InstructionNotFound,
        GotEnv { got: String },
        GotInst { got: String },
        IncompleteArgument { instname: String, nr: u32 },
        IncompleteEnvironment { envname: String },
    }

    impl Display for TeXError {
        fn fmt(&self, f: &mut Formatter) -> Result<(), fmt::Error> {
            match self {
                TeXError::ArgumentNrError { exp, got } => writeln!(
                    f,
                    "Wrong number of arguments. Expected {}, got {}",
                    exp, got
                ),
                TeXError::InstructionError { exp, got } => writeln!(
                    f,
                    "Unexpected Instruction. Expected one of {}, got {}",
                    exp.comma_sep(),
                    got
                ),
                TeXError::InstructionNotFound => writeln!(
                    f,
                    "Did not find a single latex instruction, where one was expected."
                ),
                TeXError::GotEnv { got } => {
                    writeln!(f, "Expected instruction, got environment {}.", got)
                }
                TeXError::GotInst { got } => {
                    writeln!(f, "Expected envirnoment, got instruction {}.", got)
                }
                TeXError::IncompleteArgument { instname, nr } => writeln!(
                    f,
                    "Argument nr {} of instruction {} incomplete.",
                    nr, instname
                ),
                TeXError::IncompleteEnvironment { envname } => {
                    writeln!(f, "Environment {} incomplete.", envname)
                }
            }
        }
    }

    impl std::error::Error for TeXError {
        fn cause(&self) -> Option<&(dyn std::error::Error + 'static)> {
            None
        }
    }

    impl Criticality for TeXError {
        fn is_critical(&self) -> bool {
            match self {
                TeXError::ArgumentNrError { .. } => false,
                TeXError::GotEnv { .. } => false,
                TeXError::GotInst { .. } => false,
                TeXError::InstructionError { .. } => false,
                TeXError::InstructionNotFound => false,
                _ => true,
            }
        }
    }

    #[derive(Debug, Clone, PartialEq, Eq, Hash)]
    pub enum Instruction {
        Inst {
            name: String,
            args: Vec<String>,
        },
        Environment {
            name: String,
            args: Vec<String>,
            content: String,
        },
    }

    impl Display for Instruction {
        fn fmt(&self, f: &mut Formatter) -> Result<(), fmt::Error> {
            match self {
                Instruction::Inst { name, args } => {
                    write!(f, "{}", name)?;
                    for arg in args {
                        write!(f, "{{{}}}", arg)?;
                    }
                }
                Instruction::Environment {
                    name,
                    args,
                    content,
                } => {
                    write!(f, r"\begin{{{}}}", name)?;
                    for arg in args {
                        write!(f, "{{{}}}", arg)?;
                    }
                    writeln!(f)?;
                    if content.ends_with('\n') {
                        write!(f, "{}", content)?;
                    } else {
                        writeln!(f, "{}", content)?;
                    }
                    write!(f, r"\end{{{}}}", name)?;
                }
            }
            Ok(())
        }
    }

    impl Instruction {
        pub fn inst(name: String, args: Vec<String>) -> Instruction {
            Instruction::Inst { name, args }
        }
        pub fn env(name: String, args: Vec<String>, content: String) -> Instruction {
            Instruction::Environment {
                name,
                args,
                content,
            }
        }
    }

    lazy_static! {
        pub static ref ESCAPE_REG_EX: Regex = Regex::new(concat!(
            r#"(?P<to_escape>[&\$_#%\{\}])|"#,
            r#"(?P<backslash>\\)(?P<space_backslash>\s?)|"#,
            r#"(?P<tilde>\~)(?P<space_tilde>\s?)|"#,
            r#"(?P<circum>\^)(?P<space_circum>\s?)|"#,
            r#"(?P<euro>€)(?P<space_euro>\s?)|"#,
            r#"(?P<infinity>∞)|"#,
            r#"(?P<number_pre_circle>\d+[\.,]?\d*\s*)?(?P<circle>°)"#
        ))
        .unwrap();
        pub static ref QUOTE_OPENING_REG_EX: Regex =
            Regex::new(r#"^(?P<quote_start>["'])|(?P<rest>[\s\p{Ps}\p{Pd}])(?P<quote>["'])"#)
                .unwrap();
        pub static ref QUOTE_CLOSING_REG_EX: Regex =
            Regex::new(r#"(?P<quote_end>["'])$|(?P<quote>["'])(?P<rest>[\s\p{Po}\p{Pe}\p{Pd}])"#)
                .unwrap();
        pub static ref QUOTE_UNSPEC_REG_EX: Regex = Regex::new(r#"(?P<quote>["'])"#).unwrap();
    }

    /// Returns a backslash if `s` is a single space, returns an empty str
    /// otherwise.
    fn space_to_backslash(s: regex::Match) -> &'static str {
        if s.as_str() == " " {
            r"\"
        } else {
            r""
        }
    }

    /// Replaces the quotation mark substitution characters " and '
    /// by corresponding opening or closing quotes and escapes
    /// the ten characters '&' '%' '$' '#' '_' '{' '}' '~' '^' '\'
    /// that need escaping in LaTeX.
    pub fn esc_for_latex(s: &str) -> String {
        let s = QUOTE_OPENING_REG_EX.replace_all(s, |captures: &Captures| {
            let quote = if let Some(quote) = captures.name("quote") {
                quote
            } else if let Some(quote) = captures.name("quote_start") {
                quote
            } else {
                return String::new();
            };
            let mut dst = String::new();
            if quote.as_str() == r#"""# {
                captures.expand("$rest„", &mut dst);
            } else if quote.as_str() == r#"'"# {
                captures.expand("$rest‚", &mut dst);
            }
            dst
        });
        let s = QUOTE_CLOSING_REG_EX.replace_all(&s, |captures: &Captures| {
            let quote = if let Some(quote) = captures.name("quote") {
                quote
            } else if let Some(quote) = captures.name("quote_end") {
                quote
            } else {
                return String::new();
            };
            let mut dst = String::new();
            if quote.as_str() == r#"""# {
                captures.expand("“$rest", &mut dst);
            } else if quote.as_str() == r#"'"# {
                captures.expand("‘$rest", &mut dst);
            }
            dst
        });
        let s = QUOTE_UNSPEC_REG_EX.replace_all(&s, |captures: &Captures| {
            if let Some(quote) = captures.name("quote") {
                if quote.as_str() == r#"""# {
                    r#"\""#.to_string()
                } else if quote.as_str() == r#"'"# {
                    "’".to_string()
                } else {
                    String::new()
                }
            } else {
                String::new()
            }
        });
        ESCAPE_REG_EX
            .replace_all(&s, |captures: &Captures| {
                if let Some(to_escape) = captures.name("to_escape") {
                    format!(r"\{}", to_escape.as_str())
                } else if captures.name("backslash").is_some() {
                    // `space_subst` will contain a single backslash if the character
                    // is followed by a space in the original text.
                    // This will then insert the sequence "\ " after the command
                    // which will insert a space in LaTeX.
                    let space_subst = captures
                        .name("space_backslash")
                        .map(space_to_backslash)
                        .unwrap_or_default();
                    format!(r"\textbackslash{} ", space_subst)
                } else if captures.name("tilde").is_some() {
                    let space_subst = captures
                        .name("space_tilde")
                        .map(space_to_backslash)
                        .unwrap_or_default();
                    format!(r"\textasciitilde{} ", space_subst)
                } else if captures.name("circum").is_some() {
                    let space_subst = captures
                        .name("space_circum")
                        .map(space_to_backslash)
                        .unwrap_or_default();
                    format!(r"\textasciicircum{} ", space_subst)
                } else if captures.name("euro").is_some() {
                    let space_subst = captures
                        .name("space_euro")
                        .map(space_to_backslash)
                        .unwrap_or_default();
                    format!(r"\euro{} ", space_subst)
                } else if captures.name("infinity").is_some() {
                    format!(r"$\infty$")
                } else if captures.name("circle").is_some() {
                    let number = captures
                        .name("number_pre_circle")
                        .map(|m| m.as_str());
                    if let Some(number) = number {
                        let number = number.replace(",", "{,}");
                        format!(r"${}\,\circ$", number.trim())
                    } else {
                        r"$\circ$".to_string()
                    }
                } else {
                    String::new()
                }
            })
            .to_string()
    }

    pub(crate) trait AsLaTeX {
        fn to_latex(&self) -> Instruction;
    }

    impl AsLaTeX for () {
        fn to_latex(&self) -> Instruction {
            Instruction::Inst {
                name: r"\\".to_string(),
                args: Vec::new(),
            }
        }
    }

    enum State {
        InstName,
        Arg(u32, u32),
    }

    pub fn get_next_latex_instruction(s: &mut &str) -> Option<Result<Instruction, TeXError>> {
        while let Some(idx) = s.find('\\') {
            let mut it = s[idx..].char_indices();
            it.next();
            if let Some((i, '\\')) = it.next() {
                *s = &s[idx + i + r"\".len()..];
            } else {
                *s = &s[idx..];
                return Some(get_curr_latex_instruction(s));
            }
        }
        None
    }

    pub fn get_curr_latex_instruction(s: &mut &str) -> Result<Instruction, TeXError> {
        let mut it = s.char_indices();
        if it.next() != Some((0, '\\')) {
            return Err(TeXError::InstructionNotFound);
        }
        let mut st = State::InstName;
        let mut inst = String::new();
        let mut args = Vec::new();
        let mut start = 0;
        loop {
            match st {
                State::InstName => {
                    match it.next() {
                        Some((i, c)) => {
                            if is_end_of_latex_inst(c) {
                                inst = String::from(&s[..i]);
                                if c == '{' {
                                    // Arguments follow
                                    st = State::Arg(0, 1);
                                    start = i + 1;
                                } else {
                                    // No arguments follow
                                    *s = &s[i..];
                                    return Ok(Instruction::Inst { name: inst, args });
                                }
                            }
                        }
                        // String ends after the name of the command
                        None => {
                            inst = String::from(*s);
                            *s = &s[s.len()..];
                            return Ok(Instruction::Inst { name: inst, args });
                        }
                    }
                }
                State::Arg(idx, ref mut nesting) => {
                    match it.next() {
                        Some((i, '{')) => {
                            // Start of an argument to the instruction
                            if *nesting == 0 {
                                start = match s[i..].char_indices().next() {
                                    Some((j, _)) => i + j + 1,
                                    // String ends with trailing '{' that belongs to
                                    // an argument
                                    None => {
                                        *s = &s[s.len()..];
                                        return Err(TeXError::IncompleteArgument {
                                            instname: inst,
                                            nr: idx,
                                        });
                                    }
                                };
                            }
                            *nesting += 1;
                        }
                        Some((i, '}')) if *nesting != 0 => {
                            *nesting -= 1;
                            // End of an argument to the instruction
                            if *nesting == 0 {
                                st = State::Arg(idx + 1, 0);
                                args.push(String::from(&s[start..i]))
                            }
                        }
                        // No further arguments
                        Some((i, _)) => {
                            if *nesting == 0 {
                                *s = &s[i..];
                                break;
                            }
                        }
                        None => {
                            *s = &s[s.len()..];
                            match *nesting {
                                // String ends directly after last argument
                                0 => break,
                                // String ends before argument is complete
                                _ => {
                                    return Err(TeXError::IncompleteArgument {
                                        instname: inst,
                                        nr: idx,
                                    });
                                }
                            }
                        }
                    }
                }
            }
        }
        if inst == r"\begin" {
            if let Some(envname) = args.get(0) {
                let end = format!(r"\end{{{}}}", envname);
                if let Some(idx) = s.find(&end) {
                    let content = s[..idx].trim().to_string();
                    // Won't panic because args as an element 0.
                    let envname = args.remove(0);
                    *s = &s[idx + end.len()..];
                    Ok(Instruction::Environment {
                        name: envname,
                        args,
                        content,
                    })
                } else {
                    Err(TeXError::IncompleteEnvironment {
                        envname: envname.to_string(),
                    })
                }
            } else {
                Err(TeXError::IncompleteArgument {
                    instname: inst,
                    nr: 0,
                })
            }
        } else {
            Ok(Instruction::Inst { name: inst, args })
        }
    }

    fn is_end_of_latex_inst(c: char) -> bool {
        !c.is_alphanumeric()
    }
}

#[cfg(test)]
mod tests {
    use super::latex::{
        esc_for_latex, get_curr_latex_instruction, get_next_latex_instruction, Instruction,
        ESCAPE_REG_EX, QUOTE_CLOSING_REG_EX, QUOTE_OPENING_REG_EX, QUOTE_UNSPEC_REG_EX,
    };
    use super::*;
    #[test]
    fn t_compareat() {
        let s1 = "a * sin x".to_string();
        let s2 = "u + lo".to_string();
        let s3 = "cos ( hypεrp )".to_string();
        assert!(compareat(&s1[4..], "sin"));
        assert!(!compareat(&s1[2..], "sin"));
        assert!(compareat(&s2[4..], "lo"));
        assert!(!compareat(&s2[4..], "log"));
        assert!(compareat(&s3[6..], "hypεrp"));
        assert!(!compareat(&s3[6..], "hypεrpol"));
        assert!(!compareat(&s3[6..], "hyperp"));
    }

    #[test]
    fn t_numberat() {
        let s1 = "a * sin 23.4";
        let s2 = "u + 2.405e-45";
        let s3 = "-204";
        assert_eq!(numberat(&s1[8..]).unwrap().to_string(), "23.4".to_string());
        assert_eq!(
            numberat(&s2[4..]).unwrap().to_string(),
            "2.405e-45".to_string()
        );
        assert_eq!(numberat(&s3[..]).unwrap().to_string(), "-204".to_string());
        assert_eq!(numberat(&s1[3..]), None);
        assert_eq!(
            numberat(&s2[7..]).unwrap().to_string(),
            "05e-45".to_string()
        );

        let s4 = "u + 2.405.308";
        let s5 = "u + 3e-e-23";
        assert_eq!(numberat(&s4[4..]).unwrap().to_string(), "2.405".to_string());
        assert_eq!(numberat(&s5[5..]), None);
    }

    #[test]
    fn it_to_slice() {
        let h = "hello world";
        let mut g = h.chars();
        g.next();
        g.next();
        assert_eq!(g.as_str(), "llo world");
    }

    fn inst(name: String, args: Vec<String>) -> Instruction {
        Instruction::Inst { name, args }
    }

    #[test]
    fn write_latex_instruction() {
        let a = inst(
            r"\DreamHeader".to_string(),
            vec!["2018-01-01".to_string(), "Name".to_string()],
        );
        assert_eq!(format!("{}", a), r"\DreamHeader{2018-01-01}{Name}");

        let b = Instruction::Environment {
            name: "Dream".to_string(),
            args: vec!["Arg1".to_string(), "Arg2".to_string()],
            content: "Yet another dream.\nNext Line\n\n".to_string(),
        };
        assert_eq!(
            format!("{}", b),
            concat!(
                r"\begin{Dream}{Arg1}{Arg2}",
                '\n',
                "Yet another dream.\nNext Line\n\n",
                r"\end{Dream}"
            )
        );

        let c = Instruction::Environment {
            name: "Dream".to_string(),
            args: vec!["Arg1".to_string(), "Arg2".to_string()],
            content: "Yet another dream.\nNext Line".to_string(),
        };
        assert_eq!(
            format!("{}", c),
            concat!(
                r"\begin{Dream}{Arg1}{Arg2}",
                '\n',
                "Yet another dream.\nNext Line\n",
                r"\end{Dream}"
            )
        );
    }

    #[test]
    fn curr_latex_instruction() {
        let a = r"\glqq";
        let mut tmp = &a[..];
        assert_eq!(
            get_curr_latex_instruction(&mut tmp),
            Ok(inst(r"\glqq".to_string(), vec![]))
        );
        assert_eq!(tmp, "");

        let b = r"\DreamHeader{2018-01-01}{Flying}";
        let mut tmp = &b[..];
        assert_eq!(
            get_curr_latex_instruction(&mut tmp),
            Ok(inst(
                r"\DreamHeader".to_string(),
                vec!["2018-01-01".to_string(), "Flying".to_string()]
            ))
        );
        assert_eq!(tmp, "");

        let c = r"\DreamHeader{2018-01-01}{Flying}     ";
        let mut tmp = &c[..];
        assert_eq!(
            get_curr_latex_instruction(&mut tmp),
            Ok(inst(
                r"\DreamHeader".to_string(),
                vec!["2018-01-01".to_string(), "Flying".to_string()]
            ))
        );
        assert_eq!(tmp, "     ");

        let d = r"\DreamHeader{2018-01-01}{Flying}}  ";
        let mut tmp = &d[..];
        assert_eq!(
            get_curr_latex_instruction(&mut tmp),
            Ok(inst(
                r"\DreamHeader".to_string(),
                vec!["2018-01-01".to_string(), "Flying".to_string()]
            ))
        );
        assert_eq!(tmp, "}  ");

        let e = r"\DreamHeader{2018-01-01}{Flying}}";
        let mut tmp = &e[..];
        assert_eq!(
            get_curr_latex_instruction(&mut tmp),
            Ok(inst(
                r"\DreamHeader".to_string(),
                vec!["2018-01-01".to_string(), "Flying".to_string()]
            ))
        );
        assert_eq!(tmp, "}");

        let f = r"\DreamHeader{2018-01-01}{\Text{Flying}}";
        let mut tmp = &f[..];
        assert_eq!(
            get_curr_latex_instruction(&mut tmp),
            Ok(inst(
                r"\DreamHeader".to_string(),
                vec!["2018-01-01".to_string(), r"\Text{Flying}".to_string()]
            ))
        );
        assert_eq!(tmp, "");

        let g = r"\DreamHeader{2018-01-01}{\Text{Flying}  ";
        let mut tmp = &g[..];
        assert!(get_curr_latex_instruction(&mut tmp).is_err());
        assert_eq!(tmp, "");

        let h = r"{}essen{}";
        let mut tmp = &h[..];
        assert!(get_curr_latex_instruction(&mut tmp).is_err());
        assert_eq!(tmp, "{}essen{}");

        let i = r"\DreamHeader{2018-01-01} {\Text{Flying}}";
        let mut tmp = &i[..];
        assert_eq!(
            get_curr_latex_instruction(&mut tmp),
            Ok(inst(
                r"\DreamHeader".to_string(),
                vec!["2018-01-01".to_string()]
            ))
        );
        assert_eq!(tmp, r" {\Text{Flying}}");
    }

    #[test]
    fn next_latex_instruction() {
        let a = concat!(
            r"Nichts",
            '\n',
            r"\Entry{Kind}{\Text{Trübtraum}}",
            '\n',
            r"further nothing",
            r"\glqq   ",
            r"\DreamHeader{2018-01-01}{Flying}"
        );
        let mut tmp = &a[..];
        assert_eq!(
            get_next_latex_instruction(&mut tmp),
            Some(Ok(inst(
                r"\Entry".to_string(),
                vec!["Kind".to_string(), r"\Text{Trübtraum}".to_string()]
            )))
        );
        assert_eq!(
            get_next_latex_instruction(&mut tmp),
            Some(Ok(inst(r"\glqq".to_string(), Vec::new())))
        );
        assert_eq!(
            get_next_latex_instruction(&mut tmp),
            Some(Ok(inst(
                r"\DreamHeader".to_string(),
                vec!["2018-01-01".to_string(), "Flying".to_string()]
            )))
        );
        assert!(get_next_latex_instruction(&mut tmp).is_none());

        let b = r"   \\  \glqq";
        let mut tmp = &b[..];
        assert_eq!(
            get_next_latex_instruction(&mut tmp),
            Some(Ok(inst(r"\glqq".to_string(), Vec::new())))
        );
    }

    #[test]
    fn get_latex_environment() {
        let a = concat!(
            r"Some Text",
            '\n',
            r"\begin{Dream}{argument}{further argument}",
            r"\complicatedlatexcode \\",
            '\n',
            r"Text after newline",
            r"\end{Dream}",
            r"text after environment",
        );
        let mut tmp = &a[..];
        assert_eq!(
            get_next_latex_instruction(&mut tmp),
            Some(Ok(Instruction::Environment {
                name: "Dream".to_string(),
                args: vec!["argument".to_string(), "further argument".to_string()],
                content: concat!(r"\complicatedlatexcode \\", '\n', r"Text after newline",)
                    .to_string()
            }))
        );
        assert_eq!(tmp, "text after environment");
        let b = concat!(
            r"Some Text",
            '\n',
            r"\begin{Dream}{argument}{further argument}",
            '\n',
            '\n',
            r"   \complicatedlatexcode after much whitespace \\",
            '\n',
            r"Text after newline",
            "more whitespace   \n",
            r"\end{Dream}",
            r"text after environment",
        );
        let mut tmp = &b[..];
        assert_eq!(
            get_next_latex_instruction(&mut tmp),
            Some(Ok(Instruction::Environment {
                name: "Dream".to_string(),
                args: vec!["argument".to_string(), "further argument".to_string()],
                content: concat!(
                    r"\complicatedlatexcode after much whitespace \\",
                    '\n',
                    r"Text after newline",
                    "more whitespace",
                )
                .to_string()
            }))
        );
        assert_eq!(tmp, "text after environment");
    }

    #[test]
    fn escape_reg_ex() {
        assert!(ESCAPE_REG_EX
            .captures(r"&")
            .unwrap()
            .name("to_escape")
            .is_some());
        assert!(ESCAPE_REG_EX
            .captures(r"$")
            .unwrap()
            .name("to_escape")
            .is_some());
        assert!(ESCAPE_REG_EX
            .captures(r"_")
            .unwrap()
            .name("to_escape")
            .is_some());
        assert!(ESCAPE_REG_EX
            .captures(r"#")
            .unwrap()
            .name("to_escape")
            .is_some());
        assert!(ESCAPE_REG_EX
            .captures(r"%")
            .unwrap()
            .name("to_escape")
            .is_some());

        assert!(ESCAPE_REG_EX
            .captures(r"\")
            .unwrap()
            .name("backslash")
            .is_some());
        assert!(ESCAPE_REG_EX
            .captures(r"~")
            .unwrap()
            .name("tilde")
            .is_some());
        assert!(ESCAPE_REG_EX
            .captures(r"^")
            .unwrap()
            .name("circum")
            .is_some());
    }

    #[test]
    fn quote_single_regex() {
        assert!(QUOTE_OPENING_REG_EX
            .captures(r#" ""#)
            .unwrap()
            .name("quote")
            .is_some());
        assert!(QUOTE_CLOSING_REG_EX
            .captures(r#"." "#)
            .unwrap()
            .name("quote")
            .is_some());
        assert!(QUOTE_UNSPEC_REG_EX
            .captures(r#"bin's"#)
            .unwrap()
            .name("quote")
            .is_some());
    }

    #[test]
    fn escaping() {
        let s = r"ab & cd $1$ _bitte_ #2^3% \ ~e€25,5 °C";
        assert_eq!(
            &esc_for_latex(s),
            r"ab \& cd \$1\$ \_bitte\_ \#2\textasciicircum 3\% \textbackslash\ \textasciitilde e\euro $25{,}5\,\circ$C"
        );
    }
    #[test]
    fn quote_substitution() {
        let s = r#""hallo" "ich bins" ("der Manuel") "ehem""#;
        assert_eq!(
            &esc_for_latex(s),
            r"„hallo“ „ich bins“ („der Manuel“) „ehem“"
        );
        let s = r#"'hallo' 'ich bin's' ('der Manuel') 'ehem'"#;
        assert_eq!(
            &esc_for_latex(s),
            r"‚hallo‘ ‚ich bin’s‘ (‚der Manuel‘) ‚ehem‘"
        );
    }
}
