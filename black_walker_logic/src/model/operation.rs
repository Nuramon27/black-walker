//! Contains The wrappers around operations and queries.

use crate::feature::undo::Command;

use crate::{CmdError, Diary};

use super::notify::{NotifyBegin, NotifyEnd};
use super::{Emitter, Model};

/// A pure helper trait representing an object that can be executed
/// and notify models about the changes it made to the Diary.
pub trait NarratorT<M: Model>:
    NotifyBegin<M> + Command<Diary, CmdError, Box<dyn NotifyEnd<M>>>
{
}

impl<M: Model, T> NarratorT<M> for T where
    T: NotifyBegin<M> + Command<Diary, CmdError, Box<dyn NotifyEnd<M>>>
{
}

/// A [`NarratorT`] as a trait object.
pub type Narrator<M> = Box<dyn NarratorT<M>>;

/// The main type of operation that can be performed in Black Walker
///
/// Nearly every user interaction is represented by an instance of this
/// `enum`.
///
/// The two main variants are `Narr` for commands and `Query` for
/// queries on the dream diary.
pub enum Operation<M: Model> {
    /// A general command, i.e. an operation that somehow mutates the state
    /// of Black Walker
    Narr(Narrator<M>),
    /// A query, i.e. pure retrieval of information that is performed in the
    /// worker thread and thus represented by an own object.
    ///
    /// Only the last entry holds the actual query. The second one is
    /// a sender through which the result of the Query will be sent once
    /// the query is completed, and the first entry is an emitter
    /// for "waking up" the GUI thread.
    ///
    /// This is necessary as most GUIs will
    /// use a different method than rust message passing for inter thread
    /// communication (like Qts signal slot system), so firs this method
    /// must be used in order to inform the GUI thread that he must
    /// see whether he has recieved a message from the sender.
    Query(Box<dyn Emitter>, Box<dyn Query>),
    /// The signal that the application is about to be closed and the worker
    /// thread shall shut down.
    Finish,
}

impl<M: Model> From<Narrator<M>> for Operation<M> {
    fn from(o: Narrator<M>) -> Operation<M> {
        Operation::Narr(o)
    }
}

/// This trait represents a query, i.e. pure retrieval of information
/// that is performed in the
/// worker thread and thus represented by an own object.
pub trait Query: Send + std::fmt::Debug {
    /// Perform the query and return its result.
    fn ask(&mut self, diary: &mut Diary);
}

/// This trait represents the result of a [`Query`].
///
/// Once a query has been performed, the corresponding GUI element which sent
/// the query must be told the result. This is done by calling
/// [`present`](QueryResult::present) on the result.
pub trait QueryResult: Send {
    /// Update the GUI element which sent the query in order to present
    /// its result.
    ///
    /// The existence of this method means that each Query knows itself
    /// who must be notified of the result.
    fn present(&mut self);
}
