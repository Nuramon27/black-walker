//! Contains the notification traits [`NotifyBegin`] and [`NotifyEnd`].

use bitflags::bitflags;

use super::Model;

bitflags! {
    /// Contains one flag for each of the Models in Black Walker.
    ///
    /// It is currently used by commands to signal who must be notified about a command.
    #[derive(Default)]
    pub struct ToNotify: u8 {
        const DREAMLIST = 0x01;
        const DREAMFORM = 0x02;
        const TAGTREE   = 0x04;
        const ENTRYSET  = 0x08;
    }
}

/// The `Notify` trait represents the ability of a [`Command`](undo::Command) to
/// notify a "listener" of the beginning and the end of the execution
/// of a command.
///
/// Three function must be called on every command that is executed.
//// First, the GUI thread calls [`notify_begin`](Notify::notify_begin),
//// then sends the command to the worker thread, which will apply
//// it and store it in its [`History`](undo::History).
//// Then the result is sent back to the GUI thread, where
//// [`notify_end`](Notify::notify_end) is called.
///
/// One could also make the "listener" part of the command, which then
/// notifies him by itself, but as there will be several listeners
/// in BlackWalker, of which most commands will only notify
/// one or two, we chose a way in which the listener is only
/// given before and after the command is executed.
///
/// One might argue that `T` should be an associated type rather
/// than a trait parameter, but structs implementing `Notify` must
/// be usable as trait objects, so associated types are not possible.
pub trait NotifyBegin<M: Model>: core::fmt::Debug {
    fn whom_begin(&self) -> ToNotify;
    /// Notify the DreamListModel of the beginning of execution.
    fn notify_dreamlist_begin(&self, _listener: &mut M::DreamListModel) {}
    /// Notify the DreamForm of the beginning of execution.
    fn notify_dreamform_begin(&self, _listener: &mut M::DreamForm) {}
    /// Notify the DreamListModel of the beginning of execution.
    fn notify_tagtree_begin(&self, _listener: &mut M::TagTree) {}
    /// Notify the EntryModel of the beginning of execution.
    fn notify_entry_begin(&self, _listener: &mut M::EntryModel) {}
}

pub trait NotifyEnd<M: Model>: core::fmt::Debug {
    /// A set of flags indicating which models need to be notified about
    /// the end of execution of this command
    fn whom_end(&self) -> ToNotify;
    /// Notify the DreamListModel of the end of execution.
    fn notify_dreamlist_end(&self, _listener: &mut M::DreamListModel) {}
    /// Notify the DreamForm of the end of execution.
    fn notify_dreamform_end(&self, _listener: &mut M::DreamForm) {}
    /// Notify the DreamListModel of the end of execution.
    fn notify_tagtree_end(&self, _listener: &mut M::TagTree) {}
    /// Notify the EntryModel of the end of execution.
    fn notify_entry_end(&self, _listener: &mut M::EntryModel) {}
}
