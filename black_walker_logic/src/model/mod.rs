//! Contains the _model traits_ which govern how the instances controlling
//! parts of the Diary are notified about changes.

pub mod notify;
pub mod operation;

pub use notify::{NotifyBegin, NotifyEnd, ToNotify};
pub use operation::{Narrator, NarratorT, Operation, Query, QueryResult};

use chrono::NaiveDate;

use crate::{DreamID, TagStr};

/// An object which can emit a simple signal.
pub trait Emitter: Send {
    /// Emit your signal.
    fn emit(&mut self);
}

pub trait DreamListModel {
    fn begin_reset(&mut self);
    fn end_reset(&mut self);
    fn begin_create_dream(&mut self, date: NaiveDate);
    fn end_create_dream(&mut self, date: NaiveDate);

    fn begin_create_night_pure(&mut self, date: NaiveDate);
    fn end_create_night_pure(&mut self, date: NaiveDate);

    fn begin_remove_dream(&mut self, id: DreamID);
    fn end_remove_dream(&mut self, id: DreamID);

    fn begin_remove_night_information(&mut self, date: NaiveDate);
    fn end_remove_night_information(&mut self, date: NaiveDate);

    fn begin_move_dream(&mut self, oldid: DreamID, newdate: NaiveDate, newidx: Option<u32>);
    fn end_move_dream(&mut self, oldid: DreamID, newdate: NaiveDate, newidx: Option<u32>);

    fn begin_set_dream(&mut self, id: DreamID);
    fn end_set_dream(&mut self, id: DreamID);
}

pub trait DreamFormModel {
    fn begin_reset(&mut self);
    fn end_reset(&mut self);

    fn ids_changed(&mut self, changes: &[(DreamID, DreamID)]);
    fn dream_removed(&mut self, id: DreamID);
    fn give_id(&mut self, id: DreamID);
}

pub trait TagTreeModel {
    fn begin_reset(&mut self);
    fn end_reset(&mut self);
    fn begin_reset_category(&mut self, category: &str);
    fn end_reset_category(&mut self, category: &str);
    fn begin_rename_tag(&mut self, _category: &str, old_name: &TagStr, new_name: &TagStr);
    fn end_rename_tag(&mut self, category: &str, old_name: &TagStr, new_name: &TagStr);
    fn begin_set_tag(&mut self, category: &str, name: &TagStr);
    fn end_set_tag(&mut self, category: &str, name: &TagStr);
    fn begin_create_tag(&mut self, category: &str, name: &TagStr, parent: &TagStr);
    fn end_create_tag(&mut self, category: &str, name: &TagStr, parent: &TagStr);
    /// Indicates that the tag `name` will be removed _together with all its children_.
    fn begin_remove_tag(&mut self, category: &str, name: &TagStr);
    /// Indicates that the tag `name` has been removed _together with all its children_.
    fn end_remove_tag(&mut self, category: &str, name: &TagStr);
    fn begin_reparent_tag(&mut self, category: &str, name: &TagStr, newparent: &TagStr);
    fn end_reparent_tag(&mut self, category: &str, name: &TagStr, newparent: &TagStr);
    fn begin_create_category(&mut self, category: &str);
    fn end_create_category(&mut self, category: &str);
    fn begin_remove_category(&mut self, category: &str);
    fn end_remove_category(&mut self, category: &str);
}

pub trait EntryModel {
    fn begin_reset(&mut self);
    fn end_reset(&mut self);
    fn text_added(&mut self, category: &str, entry: String);
}

pub trait Model {
    type DreamListModel: DreamListModel;
    type DreamForm: DreamFormModel;
    type TagTree: TagTreeModel;
    type EntryModel: EntryModel;
}
