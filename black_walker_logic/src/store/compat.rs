//! Contains compatibility functions which transform diaries
//! from previous versions in a way specific to the version.

use chrono::{NaiveDate, NaiveDateTime, NaiveTime};

use crate::elements::night::SleepSection;
use crate::Night;

use super::*;

/// Associates a time in a night either to `day` or to the previous
/// day, depending on how late the time is.
///
/// This is based on heuristics about when normal people tend
/// to go to bed. Currently times earlier than 15:00 are associated
/// with the current day, later times with the previous day.
fn associate_time_to_day(time: NaiveTime, day: NaiveDate) -> NaiveDateTime {
    if time < NaiveTime::from_hms(15, 00, 00) {
        NaiveDateTime::new(day, time)
    } else {
        NaiveDateTime::new(day - chrono::Duration::days(1), time)
    }
}

#[allow(clippy::blocks_in_if_conditions)]
/// Uses the fallasleep und wakeup entries of the dream
/// in order to derive initial night information.
///
/// In version 0.0.5, the possibility to add information to nights was introduced,
/// partially due to the fact, that the entries “fallasleep” and “wakeup”
/// belong to a night and not to a single dream. This function
/// takes these entries from the dreams (removing them there) and
/// creates a night section for each distinct pair of times.
pub fn transform_night_information(diary: &mut PreDiary) {
    let mut nightlist = std::mem::take(&mut diary.nl);
    // Create the full nights.
    for (_, dream) in &diary.dl {
        let night = nightlist
            .entry(dream.id.date.into())
            .or_insert(Night::default());
        if let (Some(fallasleep), Some(wakeup)) = (
            dream.d.get_entry_time("Eingeschlafen"),
            dream.d.get_entry_time("Aufgewacht"),
        ) {
            let sleep_sections = night.sleep_sections_mut();
            if fallasleep != wakeup
                && !sleep_sections.iter().any(|section| {
                    section.start().time() == fallasleep && section.end().time() == wakeup
                })
            {
                sleep_sections.push(SleepSection::new(
                    associate_time_to_day(fallasleep, dream.id.date),
                    associate_time_to_day(wakeup, dream.id.date),
                    Vec::new(),
                    None,
                ));
                sleep_sections.sort_unstable_by_key(|section| section.start());
            }
        }
    }
    // Then use this information to associate the correct night section to each
    // dream.
    for (_, dream) in &mut diary.dl {
        if let (Some(night), Some(fallasleep), Some(wakeup)) = (
            nightlist.get(&dream.id.date.into()),
            dream.d.remove_entry_time("Eingeschlafen"),
            dream.d.remove_entry_time("Aufgewacht"),
        ) {
            let fallasleep = associate_time_to_day(fallasleep, dream.id.date);
            let wakeup = associate_time_to_day(wakeup, dream.id.date);
            if let Some(position) = night
                .sleep_sections()
                .iter()
                .position(|sec| sec.start() == fallasleep && sec.end() == wakeup)
            {
                dream.d.set_entry_uint("Schlafphase", position as u32);
            }
        }
    }
    diary.nl = nightlist;
}

pub fn add_default_entry_ranges(diary: &mut PreDiary) {
    diary
        .entr
        .ranges_u
        .extend(crate::diary::entry_system::default_slider_ranges());
}

pub fn correct_umlauts_in_sliders(diary: &mut PreDiary) {
    for (_, dream) in diary.dl.iter_mut() {
        if let Some(value) = dream.d.remove_entry_uint("PersÃ¶nliche Relevanz") {
            if dream.d.get_entry_uint("Persönliche Relevanz").unwrap_or(0) == 0 {
                dream.d.set_entry_uint("Persönliche Relevanz", value);
            }
        }
        if let Some(value) = dream.d.remove_entry_uint("FÃ¤higkeiten") {
            if dream.d.get_entry_uint("Fähigkeiten").unwrap_or(0) == 0 {
                dream.d.set_entry_uint("Fähigkeiten", value);
            }
        }
    }
}

pub fn correct_einschlafleichtigkeit(diary: &mut PreDiary) {
    if let Some(range) = diary.entr.ranges_u.remove("Einschalfleichtigkeit") {
        diary
            .entr
            .ranges_u
            .insert("Einschlafleichtigkeit".to_string(), range);
    }
}
