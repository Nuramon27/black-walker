//! This module contains the structures and functions necessary
//! for storing the diary.
//!
//! For every struct that holds a part of the state of the diary,
//! there is a corresponding versioned struct, which has the same name
//! but with a version-number attached (like `DreamList2_0_0`).
//! This struct is freely convertible to and from the original struct, but
//! has a data layout adapted for serialization.
//!
//! So when the diary shall be saved, it is first converted into these
//! versioned structs, and then these are saved. For loading, we go
//! the other way round.
//!
//! For compatibility with different versions of the application, it is
//! important that these versioned structs never change. Instead, when
//! a new version adds information to a part of the diary, a new corresponding
//! versioned struct must be created, containing the new information as well.
//! From then on, this new struct is used for saving, but when loading,
//! we use the old or the new struct, depending on the version with which
//! the diary had been saved.

pub mod backup;
#[cfg(feature = "export_json")]
pub mod export_json;
mod compat;
mod diary;

use std::convert::TryFrom;
use std::fs;
use std::fs::OpenOptions;
use std::io::Write;
use std::path::{Path, PathBuf};
use std::str::FromStr;

use semver::Version;
use serde::{Deserialize, Serialize};

use crate::operate::all::ReplaceWithLoadedDiary;
use crate::settings::{DiaryLocation, BOOTSTRAP_PATH};
use crate::{Diary, GS};

pub use crate::diary::{dreamlist::store::*, entry_system::store::*};
pub use crate::elements::{night::store::*, tag::store::*};
pub use crate::settings::store::*;
pub use diary::*;

///
/// # Attention
///
/// In order for a version of BlackWalker to be compatible with older
/// versions, __the fields of this struct may never change__
/// neither in name nor in type.
#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub struct Bootstrap {
    /// The path where the diary can be found.
    pub path: PathBuf,
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub struct VersionFile {
    pub version: Version,
}

impl From<Version> for VersionFile {
    fn from(other: Version) -> Self {
        VersionFile { version: other }
    }
}

impl From<VersionFile> for Version {
    fn from(other: VersionFile) -> Self {
        other.version
    }
}

fn current_version() -> VersionFile {
    Version::from_str(env!("CARGO_PKG_VERSION"))
        // Won't panic because parsing the version string is tested
        // in `parse_package_version`.
        .expect("Could not parse crate version.")
        .into()
}

/// Returns whether the current crate version is different to `version`
fn is_outdated(version: VersionFile) -> bool {
    version != current_version()
}

fn read_version(path: &Path) -> Result<VersionFile, StoreError> {
    let version_content = fs::read_to_string(path)?;
    let version = toml::from_str(&version_content)?;
    Ok(version)
}

fn save_version(version: &VersionFile, path: &Path) -> Result<(), StoreError> {
    let mut version_file = OpenOptions::new()
        .write(true)
        .create(true)
        .truncate(true)
        .open(path)?;
    let version_content = toml::to_string_pretty(version)?;
    version_file.write_all(version_content.as_bytes())?;
    version_file.flush()?;
    Ok(())
}

impl Bootstrap {
    /// Constructs a new `Bootstrap` pointing to `path` and holding the
    /// current version of weddedederr.
    pub fn new(path: PathBuf) -> Self {
        Bootstrap { path }
    }
    /// Returns whether the diary_location of `settings`
    /// is different from the path of `self`.
    ///
    /// If no diary location is set in `settings`, the
    /// behaviour is unspecified.
    fn is_wrong(&self, settings: &GS) -> bool {
        settings
            .diary_location
            .as_ref()
            .map(|loc| loc.full_path() != self.path)
            .unwrap_or(true)
    }
    /// Load the bootstrap file from its standard location.
    fn try_bootstrap_file<P: AsRef<Path>>(path: P) -> Result<Self, StoreError> {
        let content = fs::read_to_string(path)?;
        toml::from_str(&content).map_err(StoreError::from)
    }
    /// Save the bootstrap file to its standard location.
    fn save_bootstrap<P: AsRef<Path>>(&self, path: P) -> Result<(), StoreError> {
        let mut bootstrap_file = OpenOptions::new()
            .write(true)
            .create(true)
            .truncate(true)
            .open(path)?;
        let content = toml::to_string_pretty(self)?;
        bootstrap_file.write_all(content.as_bytes())?;
        bootstrap_file.flush()?;
        Ok(())
    }
}

#[derive(Debug)]
/// The general type of error that can occur when loading or saving the `Diary`.
pub enum StoreError {
    /// A problem with io, like non-existant file.
    IO(std::io::Error),
    /// The json of a file cannot be parsed.
    TomlSer(toml::ser::Error),
    TomlDe(toml::de::Error),
    /// An  invariant demanded by a weddedederr struct
    /// is not uphold.
    Internal(InternalError),
    #[cfg(feature = "export_json")]
    Json(serde_json::Error)
}

impl From<std::io::Error> for StoreError {
    fn from(other: std::io::Error) -> Self {
        StoreError::IO(other)
    }
}

impl From<toml::ser::Error> for StoreError {
    fn from(other: toml::ser::Error) -> Self {
        StoreError::TomlSer(other)
    }
}

impl From<toml::de::Error> for StoreError {
    fn from(other: toml::de::Error) -> Self {
        StoreError::TomlDe(other)
    }
}

#[cfg(feature = "export_json")]
impl From<serde_json::Error> for StoreError {
    fn from(other: serde_json::Error) -> Self {
        StoreError::Json(other)
    }
}

#[derive(Debug, Clone)]
pub enum InternalError {
    WrongPath(String),
    PathNotSet,
    IncompatibleVersion { expected: Version, got: Version },
    Utf8,
}

impl std::fmt::Display for InternalError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> Result<(), std::fmt::Error> {
        match self {
            InternalError::WrongPath(s) => write!(f, "Diary cannot be loaded from path {}", s),
            InternalError::PathNotSet => write!(f, "A path for the diary is not set."),
            InternalError::IncompatibleVersion { expected, got } => write!(
                f,
                concat!(
                    "Tried to open incompatible version of diary",
                    "Expected {}",
                    "Got {}"
                ),
                expected, got
            ),
            InternalError::Utf8 => write!(f, "Unable to cope with non-utf8 data."),
        }
    }
}

impl From<InternalError> for StoreError {
    fn from(other: InternalError) -> Self {
        StoreError::Internal(other)
    }
}

pub fn read(path: Option<PathBuf>) -> Result<ReplaceWithLoadedDiary, StoreError> {
    let location;
    if let Some(path) = path {
        location =
            DiaryLocation::try_from(path).map_err(|_| InternalError::WrongPath("".to_string()))?;
    } else {
        let original_boostrap = Bootstrap::try_bootstrap_file(BOOTSTRAP_PATH)?;
        location = DiaryLocation::try_from(original_boostrap.path)
            .map_err(|_| InternalError::WrongPath("".to_string()))?;
    };
    let version_path = location
        .dir()
        .join(format!("{}_version.toml", location.filename()));
    let version = read_version(&version_path)
        .ok()
        .unwrap_or_else(current_version);

    let settings_content;
    let diary_content;
    {
        let settings_path = location
            .dir()
            .join(format!("{}_settings.toml", location.filename()));
        settings_content = fs::read_to_string(&settings_path).unwrap_or_default();
    }
    {
        let diary_path = location.full_path();
        diary_content = fs::read_to_string(&diary_path)?;
    }
    let mut pre_diary = if version.version >= Version::new(2, 3, 1) {
        let settings: Option<GS2_2_0> = toml::from_str(&settings_content).ok();
        let diary: Diary2_3_1 = toml::from_str(&diary_content)?;
        PreDiary::from((diary, settings, location))
    } else if version.version >= Version::new(2, 3, 0) {
        let settings: Option<GS2_2_0> = toml::from_str(&settings_content).ok();
        let diary: Diary2_3_0 = toml::from_str(&diary_content)?;
        PreDiary::from((diary, settings, location))
    } else if version.version >= Version::new(2, 2, 0) {
        let settings: Option<GS2_2_0> = toml::from_str(&settings_content).ok();
        let diary: Diary2_2_0 = toml::from_str(&diary_content)?;
        PreDiary::from((diary, settings, location))
    } else if version.version > Version::new(0, 0, 1) {
        let settings: Option<GS2_0_0> = toml::from_str(&settings_content).ok();
        let diary: Diary2_0_0 = toml::from_str(&diary_content)?;
        PreDiary::from((diary, settings, location))
    } else {
        return Err(InternalError::IncompatibleVersion {
            expected: current_version().version,
            got: version.version,
        }
        .into());
    };

    if version.version < Version::new(2, 0, 0) {
        compat::transform_night_information(&mut pre_diary);
    }
    if version.version < Version::new(2, 0, 3) {
        pre_diary.settings.standard_categories.tag = GS::default().standard_categories.tag;
    }
    if version.version < Version::new(2, 2, 0) {
        compat::add_default_entry_ranges(&mut pre_diary);
    }
    if version.version < Version::new(2, 2, 1) {
        compat::correct_umlauts_in_sliders(&mut pre_diary);
    }
    if version.version < Version::new(2, 3, 0) {
        compat::correct_einschlafleichtigkeit(&mut pre_diary);
    }

    Ok(pre_diary.replacement_command())
}

pub fn write(diary: &Diary, path: Option<PathBuf>) -> Result<(), StoreError> {
    let location;
    if let Some(path) = path {
        location =
            DiaryLocation::try_from(path).map_err(|_| InternalError::WrongPath("".to_string()))?;
    } else {
        location = diary
            .gl
            .read()
            .unwrap()
            .diary_location
            .clone()
            .ok_or(InternalError::PathNotSet)?;

        let original_bootstrap = Bootstrap::try_bootstrap_file(BOOTSTRAP_PATH);
        if original_bootstrap
            .map(|b| b.is_wrong(&diary.gl.read().unwrap()))
            .unwrap_or(true)
        {
            Bootstrap::new(location.full_path()).save_bootstrap(BOOTSTRAP_PATH)?;
        }
    };
    let version_path = location
        .dir()
        .join(format!("{}_version.toml", location.filename()));
    let version = read_version(&version_path).ok();

    let (diary_to_store, settings): (Diary2_3_1, GS2_2_0) = diary.clone().into();
    {
        let settings_path = location
            .dir()
            .join(format!("{}_settings.toml", location.filename()));
        let mut settings_file = OpenOptions::new()
            .write(true)
            .create(true)
            .truncate(true)
            .open(&settings_path)?;
        let settings_content = toml::to_string_pretty(&settings)?;
        settings_file.write_all(settings_content.as_bytes())?;
        settings_file.flush()?;
    }
    {
        let diary_path = location.full_path();
        let mut diary_file = OpenOptions::new()
            .write(true)
            .create(true)
            .truncate(true)
            .open(&diary_path)?;
        let diary_content = toml::to_string_pretty(&diary_to_store)?;
        diary_file.write_all(diary_content.as_bytes())?;
        diary_file.flush()?;
    }

    if version.map(is_outdated).unwrap_or(true) {
        save_version(&current_version(), &version_path)?;
    }

    Ok(())
}

#[cfg(test)]
mod tests {
    use std::str::FromStr;

    use semver::Version;

    #[test]
    fn parse_package_version() {
        let current_version_string = env!("CARGO_PKG_VERSION");
        assert!(
            Version::from_str(current_version_string).is_ok(),
            concat!(
                "The version string of the weddedederr could not be parsed",
                "Please change the version to a string that can be parsed",
                "by the `semver` crate.\n\n",
                "Current version string: {}"
            ),
            current_version_string
        );
    }

    #[test]
    fn toml_version() {
        let version = super::current_version();
        let version_content = toml::to_string_pretty(&version).unwrap();
        let loaded_version = toml::from_str(&version_content).unwrap();

        assert_eq!(version, loaded_version);
    }
}
