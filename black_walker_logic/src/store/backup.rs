//! Contains functions that are executed in the background thread.
//!
//! Currently everything here is related to backups.
//!
//! # The Backup shedule
//!
//! In order to provide maximum safety, Black Walker does not keep only
//! a single backup of the diary, but many backups from different amounts
//! of times ago.
//!
//! Currently, the following backups are held at every time:
//!
//! * The two most recent backups
//!
//! * The most recent backup of the preceding hour
//!
//! * The most recent backup of the pre-preceding hour
//!
//! * The latest backup of the preceding day
//!
//! * The latest backup of the pre-preceding day
//!
//! * The latest backup of the preceding week
//!
//! * The latest backup of the pre-preceding week
//!
//! * The latest backup of the preceding month
//!
//! * The latest backup of the pre-preceding month
//!
//! The function `outdated_backups` takes care over determining
//! which backups from a list of DateTimes are still necessary
//! and which are to be removed when the next backup is made.

use std::fs;
use std::iter::FromIterator;
use std::path::PathBuf;

use chrono::{Datelike, Duration, NaiveDate, NaiveDateTime, NaiveTime, Timelike};
use lazy_static::lazy_static;
use regex::Regex;

use crate::prelude::*;
use crate::read_unstaged;

lazy_static! {
    pub static ref BACKUP_REG_EXP: Regex =
        Regex::new(r"^(?P<name>.*)_(?P<date>\d{4}-\d{2}-\d{2})--(?P<time>\d{2}-\d{2}-\d{2})(?P<appendix>[^\.]*).[^\.]*").unwrap();
}

/// Sets `lhs` to the maximum of both parameters or to `rhs` if `lhs` is `None`.
fn set_left_to_max(lhs: &mut Option<NaiveDateTime>, rhs: NaiveDateTime) {
    match lhs {
        Some(lhs) => {
            if *lhs < rhs {
                *lhs = rhs
            }
        }
        None => *lhs = Some(rhs),
    }
}

/// Retrieves those DateTimes from `times` which mark _outdated_ backups, i.e.
/// backups that are not necessary anymore.
///
/// A time is outdated if it does not belong to the list of kept backups
/// described in [The Backup shedule].
///
/// It takes `now` as a parameter in order to allow simpler testing.
/// Production code shoud always pass the current DateTime.
fn outdated_backups(
    times: Vec<(NaiveDateTime, Option<String>, String)>,
    now: NaiveDateTime,
) -> Vec<(NaiveDateTime, Option<String>, String)> {
    let mut most_recent = None;
    let mut most_recent_preceding_hour = None;
    let mut most_recent_preceding_tow_hours = None;
    let mut most_recent_preceding_day = None;
    let mut most_recent_preceding_two_days = None;
    let mut most_recent_preceding_week = None;
    let mut most_recent_preceding_two_weeks = None;
    let mut most_recent_preceding_month = None;
    let mut most_recent_preceding_two_months = None;

    for (time, appendix, _) in &times {
        if appendix.as_ref().map(String::is_empty).unwrap_or(true) {
            let time = *time;

            set_left_to_max(&mut most_recent, time);

            if (time - Duration::minutes(time.time().minute().into()))
                < (now - Duration::minutes(now.time().minute().into()))
            {
                set_left_to_max(&mut most_recent_preceding_hour, time);
            }
            if (time - Duration::minutes(time.time().minute().into()))
                < (now - Duration::minutes((60 + now.time().minute()).into()))
            {
                set_left_to_max(&mut most_recent_preceding_tow_hours, time);
            }
            if time.date() < now.date() {
                set_left_to_max(&mut most_recent_preceding_day, time);
            }
            if time.date() < now.date() - Duration::days(1) {
                set_left_to_max(&mut most_recent_preceding_two_days, time);
            }
            if (time.date() - Duration::days(time.date().weekday().num_days_from_monday().into()))
                < (now.date() - Duration::days(now.date().weekday().num_days_from_monday().into()))
            {
                set_left_to_max(&mut most_recent_preceding_week, time);
            }
            if (time.date() - Duration::days(time.date().weekday().num_days_from_monday().into()))
                < (now.date()
                    - Duration::days((7 + now.date().weekday().num_days_from_monday()).into()))
            {
                set_left_to_max(&mut most_recent_preceding_two_weeks, time);
            }
            if (time.date() - Duration::days(time.date().day0().into()))
                < (now.date() - Duration::days(now.date().day0().into()))
            {
                set_left_to_max(&mut most_recent_preceding_month, time);
            }
            if (time.date() - Duration::days(time.date().day0().into()))
                < (now.date() - Duration::days((28 + now.date().day0()).into()))
                    .with_day(1)
                    .unwrap_or(time.date() - Duration::days(time.date().day0().into()))
            {
                set_left_to_max(&mut most_recent_preceding_two_months, time);
            }
        }
    }

    let surviving_backups = Vec::from_iter(
        vec![
            most_recent,
            most_recent_preceding_hour,
            most_recent_preceding_tow_hours,
            most_recent_preceding_day,
            most_recent_preceding_two_days,
            most_recent_preceding_week,
            most_recent_preceding_two_weeks,
            most_recent_preceding_month,
            most_recent_preceding_two_months,
        ]
        .into_iter()
        .filter_map(|time| time),
    );
    times
        .into_iter()
        .filter(|time| !surviving_backups.contains(&time.0))
        .collect()
}

/// Stores a backups of `diary` to a "Backup"-directory in the diaries
/// standard location and removes outdated backups.
///
///
/// See [The Backup shedule] for details on when a backup
/// is seen as outdated.
///
/// It takes `now` as a parameter in order to allow simpler testing.
/// Production code shoud always pass the current DateTime.
pub fn store_backup(mut diary: &mut Diary, now: NaiveDateTime) -> Result<(), super::StoreError> {
    let diary_dir;
    let diary_name;
    let diary_extension;
    // Get the diary location.
    {
        let settings = read_unstaged!(diary, GS).unwrap();
        let location = settings
            .diary_location
            .as_ref()
            .ok_or(super::InternalError::PathNotSet)?;
        diary_dir = location.dir().to_owned();
        diary_name = location.filename().to_owned();
        diary_extension = location.extension().to_owned();
    }

    let backup_dir = diary_dir.join("Backup");
    fs::create_dir_all(&backup_dir)?;

    // Scan the files in the diary location in order to determine
    // existing backups.
    //
    // The main work is done by the BACKUP_REG_EXP.
    let backup_times = Vec::from_iter(
        fs::read_dir(&backup_dir)?
            .filter_map(|entry| {
                if let Ok(entry) = entry {
                    Some(entry)
                } else {
                    None
                }
            })
            .filter_map(|entry| {
                if entry
                    .file_type()
                    .map(|entry| entry.is_file())
                    .unwrap_or(false)
                {
                    if let Ok(full_name) = entry.file_name().into_string() {
                        if let Some(captures) = BACKUP_REG_EXP.captures(&full_name) {
                            if let (Some(filename), Some(date), Some(time)) = (
                                captures.name("name"),
                                captures.name("date"),
                                captures.name("time"),
                            ) {
                                if filename.as_str() == diary_name.as_str() {
                                    if let (Ok(date), Ok(time)) = (
                                        NaiveDate::parse_from_str(date.as_str(), "%Y-%m-%d"),
                                        NaiveTime::parse_from_str(&time.as_str(), "%H-%M-%S"),
                                    ) {
                                        return Some((
                                            NaiveDateTime::new(date, time),
                                            captures
                                                .name("appendix")
                                                .map(|s| s.as_str().to_owned()),
                                            full_name,
                                        ));
                                    }
                                }
                            }
                        }
                    }
                }
                None
            }),
    );

    // Write the current backup.
    super::write(
        diary,
        Some(backup_dir.join(PathBuf::from(format!(
            "{}_{}--{}.{}",
            diary_name,
            now.date().format("%Y-%m-%d"),
            now.time().format("%H-%M-%S"),
            diary_extension,
        )))),
    )?;

    // Remove outdated backups.
    for (_, _, name) in outdated_backups(backup_times, now) {
        std::fs::remove_file(backup_dir.join(name))?;
    }

    Ok(())
}

#[cfg(test)]
mod tests {
    use std::iter::FromIterator;

    use chrono::{NaiveDate, NaiveDateTime, NaiveTime};

    #[test]
    fn backup_reg_exp() {
        let res = super::BACKUP_REG_EXP
            .captures("diary_2019-01-27--11-24-01.toml")
            .unwrap();
        assert_eq!(res.name("name").unwrap().as_str(), "diary");
        assert_eq!(res.name("date").unwrap().as_str(), "2019-01-27");
        assert_eq!(res.name("time").unwrap().as_str(), "11-24-01");
        assert_eq!(res.name("appendix").unwrap().as_str(), "");

        let res = super::BACKUP_REG_EXP
            .captures("diary_2019-01-27--11-24-01_backup.toml")
            .unwrap();
        assert_eq!(res.name("name").unwrap().as_str(), "diary");
        assert_eq!(res.name("date").unwrap().as_str(), "2019-01-27");
        assert_eq!(res.name("time").unwrap().as_str(), "11-24-01");
        assert_eq!(res.name("appendix").unwrap().as_str(), "_backup");
    }

    #[test]
    fn outdated_backups() {
        let backups = vec![
            NaiveDateTime::new(
                NaiveDate::from_ymd(2020, 03, 18),
                NaiveTime::from_hms(12, 30, 00),
            ),
            NaiveDateTime::new(
                NaiveDate::from_ymd(2020, 03, 18),
                NaiveTime::from_hms(12, 26, 00),
            ),
            NaiveDateTime::new(
                NaiveDate::from_ymd(2020, 03, 18),
                NaiveTime::from_hms(12, 22, 00),
            ),
            NaiveDateTime::new(
                NaiveDate::from_ymd(2020, 03, 18),
                NaiveTime::from_hms(11, 58, 00),
            ),
            // 4
            NaiveDateTime::new(
                NaiveDate::from_ymd(2020, 03, 18),
                NaiveTime::from_hms(11, 54, 00),
            ),
            NaiveDateTime::new(
                NaiveDate::from_ymd(2020, 03, 18),
                NaiveTime::from_hms(10, 58, 00),
            ),
            NaiveDateTime::new(
                NaiveDate::from_ymd(2020, 03, 18),
                NaiveTime::from_hms(09, 56, 00),
            ),
            NaiveDateTime::new(
                NaiveDate::from_ymd(2020, 03, 17),
                NaiveTime::from_hms(22, 00, 00),
            ),
            // 8
            NaiveDateTime::new(
                NaiveDate::from_ymd(2020, 03, 17),
                NaiveTime::from_hms(21, 00, 00),
            ),
            NaiveDateTime::new(
                NaiveDate::from_ymd(2020, 03, 16),
                NaiveTime::from_hms(22, 00, 00),
            ),
            // Preceding week
            NaiveDateTime::new(
                NaiveDate::from_ymd(2020, 03, 13),
                NaiveTime::from_hms(22, 00, 00),
            ),
            // Pre-Preceding week
            NaiveDateTime::new(
                NaiveDate::from_ymd(2020, 03, 08),
                NaiveTime::from_hms(22, 00, 00),
            ),
            // Pre-Pre-Preceding week
            // 12
            NaiveDateTime::new(
                NaiveDate::from_ymd(2020, 03, 01),
                NaiveTime::from_hms(22, 00, 00),
            ),
            // Preceding month
            NaiveDateTime::new(
                NaiveDate::from_ymd(2020, 02, 28),
                NaiveTime::from_hms(22, 00, 00),
            ),
            // Pre-Preceding month
            NaiveDateTime::new(
                NaiveDate::from_ymd(2020, 01, 15),
                NaiveTime::from_hms(22, 00, 00),
            ),
            // Pre-Pre-Preceding month
            NaiveDateTime::new(
                NaiveDate::from_ymd(2019, 12, 31),
                NaiveTime::from_hms(22, 00, 00),
            ),
        ];

        let backups: Vec<(NaiveDateTime, Option<String>, String)> =
            Vec::from_iter(backups.into_iter().map(|time| (time, None, String::new())));
        let backups_to_remove = super::outdated_backups(
            backups.clone(),
            NaiveDateTime::new(
                NaiveDate::from_ymd(2020, 03, 18),
                NaiveTime::from_hms(12, 34, 00),
            ),
        );

        assert_eq!(
            backups_to_remove.as_slice(),
            &[
                backups[1].clone(),
                backups[2].clone(),
                backups[4].clone(),
                backups[6].clone(),
                backups[8].clone(),
                backups[12].clone(),
                backups[15].clone()
            ]
        );
    }
}
