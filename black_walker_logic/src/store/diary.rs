use serde::{Deserialize, Serialize};

use crate::elements::tag;
use crate::operate::all::ReplaceWithLoadedDiary;
use crate::settings::DiaryLocation;
use crate::sync::prelude::*;
use crate::{lock, stage_diary};
use crate::{Diary, DreamList, EntrySystem, NightList, TagForest, GS};

use super::{
    DreamList2_0_0, DreamList2_2_0, DreamList2_3_0, DreamList2_3_1, EntrySystem2_0_0, NightList2_0_0,
    TagForest2_0_0, GS2_0_0, GS2_2_0,
};

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub struct Diary2_3_1 {
    pub entr: EntrySystem2_0_0,
    pub tag: TagForest2_0_0,
    pub dl: DreamList2_3_1,
    pub nl: NightList2_0_0,
}

impl From<Diary> for (Diary2_3_1, GS2_2_0) {
    fn from(mut other: Diary) -> Self {
        let mut stage = stage_diary!(other; TAG, DL, NL, ENTR, SET;);
        if let (Ok(tagforest), Ok(dreamlist), Ok(nightlist), Ok(entrysystem), Ok(settings)) =
            lock!(stage; TagForest, DreamList, NightList, EntrySystem, GS;)
        {
            (
                Diary2_3_1 {
                    dl: DreamList::clone(&dreamlist).into(),
                    nl: NightList::clone(&nightlist).into(),
                    tag: TagForest::clone(&tagforest).into(),
                    entr: EntrySystem::clone(&entrysystem).into(),
                },
                GS::clone(&settings).into(),
            )
        } else {
            panic!("Mutexes are poissoned.")
        }
    }
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub struct Diary2_3_0 {
    pub entr: EntrySystem2_0_0,
    pub tag: TagForest2_0_0,
    pub dl: DreamList2_3_0,
    pub nl: NightList2_0_0,
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub struct Diary2_2_0 {
    pub entr: EntrySystem2_0_0,
    pub tag: TagForest2_0_0,
    pub dl: DreamList2_2_0,
    pub nl: NightList2_0_0,
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub struct Diary2_0_0 {
    pub entr: EntrySystem2_0_0,
    pub tag: TagForest2_0_0,
    pub dl: DreamList2_0_0,
    pub nl: NightList2_0_0,
}

pub struct PreDiary {
    pub dl: DreamList,
    pub nl: NightList,
    pub tag: TagForest,
    pub entr: EntrySystem,
    pub settings: GS,
}

impl From<(Diary2_3_1, Option<GS2_2_0>, DiaryLocation)> for PreDiary {
    /// Creates a `PreDiary` from a versioned [`Diary`], a versioned [`GS`] and a
    /// [`DiaryLocation`].
    ///
    /// If the `GS` is not present, the `Default` of `GS` is used.
    fn from((diary, settings, location): (Diary2_3_1, Option<GS2_2_0>, DiaryLocation)) -> Self {
        PreDiary {
            dl: DreamList::from(diary.dl),
            nl: NightList::from(diary.nl),
            tag: TagForest::from(diary.tag),
            entr: EntrySystem::from(diary.entr),
            settings: settings
                .map(|settings| GS::from((location, settings)))
                .unwrap_or_default(),
        }
    }
}

impl From<(Diary2_3_0, Option<GS2_2_0>, DiaryLocation)> for PreDiary {
    /// Creates a `PreDiary` from a versioned [`Diary`], a versioned [`GS`] and a
    /// [`DiaryLocation`].
    ///
    /// If the `GS` is not present, the `Default` of `GS` is used.
    fn from((diary, settings, location): (Diary2_3_0, Option<GS2_2_0>, DiaryLocation)) -> Self {
        PreDiary {
            dl: DreamList::from(diary.dl),
            nl: NightList::from(diary.nl),
            tag: TagForest::from(diary.tag),
            entr: EntrySystem::from(diary.entr),
            settings: settings
                .map(|settings| GS::from((location, settings)))
                .unwrap_or_default(),
        }
    }
}

impl From<(Diary2_2_0, Option<GS2_2_0>, DiaryLocation)> for PreDiary {
    /// Creates a `PreDiary` from a versioned [`Diary`], a versioned [`GS`] and a
    /// [`DiaryLocation`].
    ///
    /// If the `GS` is not present, the `Default` of `GS` is used.
    fn from((diary, settings, location): (Diary2_2_0, Option<GS2_2_0>, DiaryLocation)) -> Self {
        PreDiary {
            dl: DreamList::from(diary.dl),
            nl: NightList::from(diary.nl),
            tag: TagForest::from(diary.tag),
            entr: EntrySystem::from(diary.entr),
            settings: settings
                .map(|settings| GS::from((location, settings)))
                .unwrap_or_default(),
        }
    }
}

impl From<(Diary2_0_0, Option<GS2_0_0>, DiaryLocation)> for PreDiary {
    fn from((diary, settings, location): (Diary2_0_0, Option<GS2_0_0>, DiaryLocation)) -> Self {
        PreDiary {
            dl: DreamList::from(diary.dl),
            nl: NightList::from(diary.nl),
            tag: TagForest::from(diary.tag),
            entr: EntrySystem::from(diary.entr),
            settings: settings
                .map(|settings| GS::from((location, settings)))
                .unwrap_or_default(),
        }
    }
}

impl PreDiary {
    pub fn replacement_command(mut self) -> ReplaceWithLoadedDiary {
        self.entr.add_all_entries(&self.dl);
        tag::occur_tags_in_dreamlist(&mut self.tag, &self.dl);

        ReplaceWithLoadedDiary::new(self.dl, self.nl, self.tag, self.entr, self.settings)
    }
}
