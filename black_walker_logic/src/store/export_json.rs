use std::path::Path;
use std::io::Write;
use std::fs::OpenOptions;

use serde_json::to_string_pretty;

use crate::prelude::*;
use super::{Diary2_3_1, GS2_2_0, StoreError};

/// Stores the `diary` in json-format to `path`.
pub fn export_json(diary: &Diary, path: &Path) -> Result<(), StoreError> {
    let (diary_to_store, _): (Diary2_3_1, GS2_2_0) = diary.clone().into();

    let mut diary_file = OpenOptions::new()
        .write(true)
        .create(true)
        .truncate(true)
        .open(&path)?;
    let diary_content = to_string_pretty(&diary_to_store)?;
    diary_file.write_all(diary_content.as_bytes())?;
    diary_file.flush()?;

    Ok(())
}

#[cfg(test)]
mod tests {
    use std::ops::Deref;
    use std::fs;

    use crate::operate::tests::standard_diary;
    use crate::settings::DiaryLocation;
    use crate::store::{self, Diary2_3_1, PreDiary};
    use crate::store::GS2_2_0;
    use crate::sync::diary::DiaryPart::{DL, TAG};
    use crate::{DreamList, TagForest, GS};

    use crate::{lock, stage_diary};

    #[test]
    // Ignored because it modifies the disk.
    #[ignore]
    fn store_export_standard_diary_json() {
        crate::elements::tag::names::collation::init_collator();

        let mut diary = standard_diary();

        let diary_dir = std::env::temp_dir().join("black_walker_play/diary/");
        std::fs::create_dir_all(&diary_dir).unwrap();
        let diary_path = diary_dir.join("diary.toml");
        store::export_json::export_json(&diary, &diary_path).unwrap();

        let diary_loaded: PreDiary;
        {
            let file = fs::OpenOptions::new()
                .read(true)
                .open(&diary_path)
                .unwrap();
            let diary_versioned: Diary2_3_1 = serde_json::from_reader(file).unwrap();
            diary_loaded = (diary_versioned, Some(GS2_2_0::from(GS::default())), DiaryLocation::default()).into();
        }

        {
            {
                let mut stage = stage_diary!(diary; DL, TAG;);
                if let (Ok(dreamlist_std), Ok(tagforest_std)) =
                    lock!(stage; DreamList, TagForest;)
                {
                    assert_eq!(&diary_loaded.dl, dreamlist_std.deref());
                    assert_eq!(&diary_loaded.tag, tagforest_std.deref());
                };
            };
        }
    }

}