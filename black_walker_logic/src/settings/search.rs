#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Search {
    pub searchwithgroups: bool,
    pub searchweaktags: bool,
}

impl Default for Search {
    fn default() -> Self {
        Search {
            searchwithgroups: true,
            searchweaktags: true
        }
    }
}
