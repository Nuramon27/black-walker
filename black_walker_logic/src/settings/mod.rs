pub mod search;
pub mod sizes;
pub(crate) mod store;
pub mod suggestion;

use std::ops::Range;
use std::path::{Path, PathBuf};
use std::time::Duration;

use serde::{Deserialize, Serialize};

pub use search::Search;
pub use sizes::Sizes;
pub use suggestion::Suggestion;

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct GS {
    pub diary_location: Option<DiaryLocation>,
    pub standard_categories: StandardCategories,
    pub suggestion: Suggestion,
    pub sizes: Sizes,
    pub backup_interval: Duration,
    pub place_last_used: String,
    pub slider_range_default: Range<i32>,
    pub search: Search,
}

impl Default for GS {
    fn default() -> Self {
        Self {
            diary_location: None,
            standard_categories: StandardCategories::default(),
            suggestion: Suggestion::default(),
            sizes: Sizes::default(),
            backup_interval: Duration::from_secs(4 * 60),
            place_last_used: String::new(),
            slider_range_default: 0..5,
            search: Search::default(),
        }
    }
}

impl GS {
    /// Returns the standard entry categories
    pub fn standard_entry_categories(&self) -> &Vec<String> {
        &self.standard_categories.entry
    }
    /// Returns the standard tag categories
    pub fn standard_tag_categories(&self) -> &Vec<String> {
        &self.standard_categories.tag
    }
}

#[derive(Debug, Default, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub struct DiaryLocation {
    dir: PathBuf,
    filename: String,
    extension: String,
}

impl std::convert::TryFrom<PathBuf> for DiaryLocation {
    type Error = ();
    fn try_from(mut p: PathBuf) -> Result<Self, ()> {
        if p.extension().is_none() {
            p.set_extension("toml");
        }
        if let (Some(dir), Some(filename), Some(extension)) = (
            p.parent(),
            p.file_stem().and_then(std::ffi::OsStr::to_str),
            p.extension().and_then(std::ffi::OsStr::to_str),
        ) {
            Ok(DiaryLocation {
                dir: dir.to_owned(),
                filename: filename.to_owned(),
                extension: extension.to_owned(),
            })
        } else {
            Err(())
        }
    }
}

impl DiaryLocation {
    /// Returns the parent directory in which the diary is to be stored.
    pub fn dir(&self) -> &Path {
        &self.dir
    }
    /// Reutrns the raw filename of the diary, without its parent directory and without
    /// its extension.
    pub fn filename(&self) -> &String {
        &self.filename
    }
    /// Returns the extension of the filename of the diary.
    pub fn extension(&self) -> &String {
        &self.extension
    }
    /// Returns the full path in which the diary is to be stored,
    /// incuding filename and extension.
    pub fn full_path(&self) -> PathBuf {
        self.dir
            .join(format!("{}.{}", self.filename, self.extension))
    }
    /*/// Sets the full path in which the diary is to be stored to `p`.
    ///
    /// This only succeeds if `p` can be decomposed into a parent directory
    /// and a filename. Returns true if it succeeds.
    /// Otherwise, all elements of the location
    /// are set to `None` and false is returned.
    pub fn set_full_path(&mut self, mut p: PathBuf) -> Result<(), ()> {
        if p.extension().is_none() {
            p.set_extension("toml");
        }
        if let (Some(dir), Some(filename), Some(extension)) =
        (
            p.parent(),
            p.file_stem().and_then(std::ffi::OsStr::to_str),
            p.extension().and_then(std::ffi::OsStr::to_str)
        ) {
            self.dir = dir.to_owned();
            self.filename = filename.to_owned();
            self.extension = extension.to_owned();
            Ok(())
        } else {
            self.dir = None;
            self.filename = None;
            self.extension = None;
            false
        }
    }*/
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct StandardCategories {
    pub tag: Vec<String>,
    pub entry: Vec<String>,
}

impl Default for StandardCategories {
    fn default() -> Self {
        StandardCategories {
            entry: vec![
                "Art".to_string(),
                "Technik".to_string(),
                "Intensität".to_string(),
            ],
            tag: vec![
                "Personen".to_string(),
                "Orte".to_string(),
                "Plätze".to_string(),
                "Traumzeichen".to_string(),
                "Gegenstände".to_string(),
                "Essen & Getränke".to_string(),
                "Emotionen".to_string(),
            ],
        }
    }
}

impl StandardCategories {
    pub fn sort_tag_categories(&self, categories: Vec<String>) -> Vec<String> {
        let mut res = self.tag.clone();
        let mut nonstandard_categories = Vec::new();
        for cat in categories {
            if !res.contains(&cat) {
                nonstandard_categories.push(cat);
            }
        }
        nonstandard_categories
            .as_mut_slice()
            .sort_by(|lhs, rhs| crate::cmp_coll(lhs, rhs));
        res.extend(nonstandard_categories);
        res
    }
}

#[cfg(debug_assertions)]
pub const BOOTSTRAP_PATH: &str = "/home/Manuel/tmp/black_walker_play/bootstrap.toml";
#[cfg(not(debug_assertions))]
pub const BOOTSTRAP_PATH: &str = "bootstrap.toml";
