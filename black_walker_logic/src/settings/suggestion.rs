#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct Suggestion {
    pub entered_chars_for_suggestions: u16,
    pub entered_chars_for_interim_suggestions: u16,
    pub entered_chars_for_tolerance: Option<u16>,
    pub allowed_errors: u16,
}

impl Default for Suggestion {
    fn default() -> Self {
        Suggestion {
            entered_chars_for_suggestions: 2,
            entered_chars_for_interim_suggestions: 5,
            entered_chars_for_tolerance: Some(6),
            allowed_errors: 1,
        }
    }
}
