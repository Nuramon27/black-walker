use std::collections::HashMap;

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Sizes {
    pub(super) window: HashMap<String, [u32; 2]>,
    pub(super) columns: HashMap<String, (Vec<u32>, u32)>,
}

impl Default for Sizes {
    fn default() -> Self {
        Self {
            window: [
                ("AWindow", [0x380, 0x200]),
                ("DTagAdministration", [0x300, 0x200]),
                ("DUnknownTagsDialog", [0x200, 0x200]),
                ("DDreamsWithTagDialog", [0x400, 0x280]),
                ("DDreamInputDialog", [0x400, 0x280]),
                ("DNightDialog", [0x240, 0xC0]),
                ("DSearchDialog", [0x300, 0x200]),
            ]
            .iter()
            .map(|(k, v)| ((*k).to_string(), v.clone()))
            .collect(),
            columns: [
                ("DTagAdministration", (&[0x180, 0x50, 0x50][..], 0x8)),
                ("DDreamList", (&[0xB0, 0x100][..], 0x80)),
                ("DNightList", (&[0x84, 0x80, 0x60, 0x28][..], 0x8)),
            ]
            .iter()
            .map(|(k, (direct, def))| ((*k).to_string(), (Vec::from(*direct), *def)))
            .collect(),
        }
    }
}

impl Sizes {
    const SIZE_COLUMN_DEFAULT: u32 = 0x60;
    /// # Panics
    ///
    /// Panics if `i >= 2`.
    pub fn size_window(&self, name: &str, i: usize) -> Option<u32> {
        self.window.get(name).map(|a| a[i])
    }
    pub fn set_sizes_window(&mut self, name: String, value: [u32; 2]) {
        self.window.insert(name, value);
    }
    pub fn size_column(&self, name: &str, i: usize) -> u32 {
        self.columns
            .get(name)
            .map_or(Self::SIZE_COLUMN_DEFAULT, |(v, d)| *v.get(i).unwrap_or(d))
    }
    pub fn set_size_column(&mut self, name: String, i: usize, value: u32) {
        let entry = self
            .columns
            .entry(name)
            .or_insert((Vec::new(), Self::SIZE_COLUMN_DEFAULT));
        if let Some(elem) = entry.0.get_mut(i) {
            *elem = value
        } else {
            entry.0.extend(
                std::iter::repeat(entry.1)
                    .take(i - entry.0.len())
                    .chain(std::iter::once(value)),
            )
        }
    }
    pub fn crop_size_column_to_default(&mut self, name: String, from: usize) {
        let entry = self
            .columns
            .entry(name)
            .or_insert((Vec::new(), Self::SIZE_COLUMN_DEFAULT));
        while entry.0.len() > from {
            entry.0.pop();
        }
    }
    pub fn set_size_column_default(&mut self, name: String, from: usize, value: u32) {
        let entry = self
            .columns
            .entry(name)
            .or_insert((Vec::new(), Self::SIZE_COLUMN_DEFAULT));
        entry.1 = value;
        while entry.0.len() > from {
            entry.0.pop();
        }
    }
}
