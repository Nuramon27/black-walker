use std::collections::HashMap;
use std::ops::Range;
use std::time::Duration;

use serde::{Deserialize, Serialize};

use crate::settings::{DiaryLocation, Search, Sizes, StandardCategories, Suggestion, GS};

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub struct GS2_2_0 {
    pub place_last_used: String,
    pub backup_interval: Duration,
    pub standard_categories: StandardCategories2_0_0,
    pub suggestion: Suggestion2_0_0,
    pub sizes: Sizes2_0_0,
    pub slider_range_default: Range<i32>,
    #[serde(default)]
    pub search: Search2_3_0,
}

impl From<(DiaryLocation, GS2_2_0)> for GS {
    fn from(other: (DiaryLocation, GS2_2_0)) -> Self {
        GS {
            diary_location: Some(other.0),
            standard_categories: other.1.standard_categories.into(),
            suggestion: other.1.suggestion.into(),
            sizes: other.1.sizes.into(),
            backup_interval: other.1.backup_interval,
            place_last_used: other.1.place_last_used,
            slider_range_default: other.1.slider_range_default,
            search: other.1.search.into(),
        }
    }
}

impl From<GS> for GS2_2_0 {
    fn from(other: GS) -> Self {
        GS2_2_0 {
            standard_categories: other.standard_categories.into(),
            suggestion: other.suggestion.into(),
            sizes: other.sizes.into(),
            backup_interval: other.backup_interval,
            place_last_used: other.place_last_used,
            slider_range_default: other.slider_range_default,
            search: other.search.into(),
        }
    }
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub struct GS2_0_0 {
    pub place_last_used: String,
    pub backup_interval: Duration,
    pub standard_categories: StandardCategories2_0_0,
    pub suggestion: Suggestion2_0_0,
    pub sizes: Sizes2_0_0,
}

impl From<(DiaryLocation, GS2_0_0)> for GS {
    fn from(other: (DiaryLocation, GS2_0_0)) -> Self {
        GS {
            diary_location: Some(other.0),
            standard_categories: other.1.standard_categories.into(),
            suggestion: other.1.suggestion.into(),
            sizes: other.1.sizes.into(),
            backup_interval: other.1.backup_interval,
            place_last_used: other.1.place_last_used,
            slider_range_default: 0..5,
            search: Search::default(),
        }
    }
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub struct StandardCategories2_0_0 {
    pub tag: Vec<String>,
    pub entry: Vec<String>,
}

impl From<StandardCategories> for StandardCategories2_0_0 {
    fn from(other: StandardCategories) -> Self {
        StandardCategories2_0_0 {
            tag: other.tag,
            entry: other.entry,
        }
    }
}

impl From<StandardCategories2_0_0> for StandardCategories {
    fn from(other: StandardCategories2_0_0) -> Self {
        StandardCategories {
            tag: other.tag,
            entry: other.entry,
        }
    }
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub struct Suggestion2_0_0 {
    pub entered_chars_for_suggestions: u16,
    pub entered_chars_for_interim_suggestions: u16,
    pub entered_chars_for_tolerance: Option<u16>,
    pub allowed_errors: u16,
}

impl From<Suggestion> for Suggestion2_0_0 {
    fn from(other: Suggestion) -> Self {
        Suggestion2_0_0 {
            entered_chars_for_suggestions: other.entered_chars_for_suggestions,
            entered_chars_for_interim_suggestions: other.entered_chars_for_interim_suggestions,
            entered_chars_for_tolerance: other.entered_chars_for_tolerance,
            allowed_errors: other.allowed_errors,
        }
    }
}

impl From<Suggestion2_0_0> for Suggestion {
    fn from(other: Suggestion2_0_0) -> Self {
        Suggestion {
            entered_chars_for_suggestions: other.entered_chars_for_suggestions,
            entered_chars_for_interim_suggestions: other.entered_chars_for_interim_suggestions,
            entered_chars_for_tolerance: other.entered_chars_for_tolerance,
            allowed_errors: other.allowed_errors,
        }
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
#[derive(Serialize, Deserialize)]
pub struct Search2_3_0 {
    pub searchwithgroups: bool,
    pub searchweaktags: Option<bool>,
}

impl Default for Search2_3_0 {
    fn default() -> Self {
        Search::default().into()
    }
}

impl From<Search> for Search2_3_0 {
    fn from(other: Search) -> Self {
        Self {
            searchwithgroups: other.searchwithgroups,
            searchweaktags: Some(other.searchweaktags)
        }
    }
}

impl From<Search2_3_0> for Search {
    fn from(other: Search2_3_0) -> Self {
        Self {
            searchwithgroups: other.searchwithgroups,
            searchweaktags: other.searchweaktags.unwrap_or(Search::default().searchweaktags)
        }
    }
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub struct Sizes2_0_0 {
    window: HashMap<String, [u32; 2]>,
    columns_direct: HashMap<String, Vec<u32>>,
    columns_default: HashMap<String, u32>,
}

impl From<Sizes> for Sizes2_0_0 {
    fn from(other: Sizes) -> Self {
        let mut columns_direct = HashMap::with_capacity(other.columns.len());
        let mut columns_default = HashMap::with_capacity(other.columns.len());
        for (key, (sizes_direct, size_default)) in other.columns {
            columns_direct.insert(key.clone(), sizes_direct);
            columns_default.insert(key, size_default);
        }
        Self {
            window: other.window,
            columns_direct,
            columns_default,
        }
    }
}

impl From<Sizes2_0_0> for Sizes {
    fn from(mut other: Sizes2_0_0) -> Self {
        let mut default_prototype = Sizes::default();
        let mut columns = HashMap::with_capacity(other.columns_default.len());
        for (key, size_default) in other.columns_default {
            let value = (
                other.columns_direct.remove(&key).unwrap_or_default(),
                size_default,
            );
            columns.insert(key, value);
        }
        // This adds all entries of columns with a key not in default_prototype,
        // replaces entries present in default_prototype with the one in columns
        // and keeps all entries only present in default_prototype.
        // Thus, we "update" the default with the special settings found in the file.
        default_prototype.columns.extend(columns);
        // The same for window.
        default_prototype.window.extend(other.window);
        Self {
            window: default_prototype.window,
            columns: default_prototype.columns,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::GS2_2_0;
    use crate::settings::{DiaryLocation, GS};
    #[test]
    fn save_settings() {
        let mut set_original = GS::default();
        set_original.diary_location = Some(DiaryLocation::default());
        set_original
            .sizes
            .window
            .insert("AWindow".to_string(), [2, 2]);
        let set = GS2_2_0::from(set_original.clone());
        let set_toml = toml::to_string_pretty(&set).expect("Settings could not be saved.");
        let set_loaded: GS2_2_0 = toml::from_str(&set_toml).expect("Settings could not be loaded.");

        assert_eq!(
            set_original,
            GS::from((DiaryLocation::default(), set_loaded)),
            "loaded settings were not correct."
        );
    }
}
