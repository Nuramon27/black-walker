pub fn init_collator() {}

pub fn equals_col(lhs: &str, rhs: &str) -> bool {
    lhs == rhs
}

pub fn contains_icu(pattern: &[u16], text: &[u16]) -> bool {
    for window in text.windows(pattern.len()) {
        if pattern == window {
            return true;
        }
    }
    false
}