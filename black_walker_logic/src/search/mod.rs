#[cfg(feature = "icu")]
#[doc(hidden)]
pub mod collation_icu;
#[cfg(feature = "icu")]
pub use collation_icu as collation;
#[cfg(not(feature = "icu"))]
#[doc(hidden)]
pub mod collation_none;
#[cfg(not(feature = "icu"))]
pub use collation_none as collation;

mod diary_input;

use std::borrow::Cow;
use std::cmp::Ordering;
use std::collections::HashSet;
use std::iter::FromIterator;
use std::sync::Weak;

use num_enum::IntoPrimitive;

use lazy_static::lazy_static;
use regex::Regex;
use termite::graph::parse::custom::*;
use termite::graph::parse::parse;
use termite::nodes::Constant;
use termite::shared_node::{SharedCalc, SharedNode};
use termite::{Side, LEFT, RIGHT};

use crate::sync::prelude::*;
use crate::{lock, stage_diary};
use crate::{CmdError, Diary, DreamID, DreamList, TagForest, TagSpec, TagSpecification, TagTree};
use diary_input::DiaryInput;

#[derive(Debug, Clone)]
enum OperatorDreamBool<'r> {
    Str(SharedNode<'r, Cow<'r, str>>),
    Bool(SharedNode<'r, bool>),
}

impl<'r> AnyOperator<'r> for OperatorDreamBool<'r> {
    type Out = bool;
    fn to_calc(&self) -> SharedCalc<'r> {
        match self {
            OperatorDreamBool::Str(node) => SharedNode::to_calc(node),
            OperatorDreamBool::Bool(node) => SharedNode::to_calc(node),
        }
    }
    fn to_final(&self) -> Result<SharedNode<'r, Self::Out>, Error> {
        match self {
            OperatorDreamBool::Str(_) => Err(Error::WrongFinalType {
                got: "str".to_string(),
                exp: "bool".to_string(),
            }),
            OperatorDreamBool::Bool(op) => Ok(op.clone()),
        }
    }
}

#[derive(Debug)]
enum PreOperatorDreamBool<'r> {
    DreamStr(Multifix<'r, DiaryInput<'r>, Cow<'r, str>>),
    StrBool(Multifix<'r, Cow<'r, str>, bool>),
    DreamBool(Multifix<'r, DiaryInput<'r>, bool>),
    BoolBool(Multifix<'r, bool, bool>),
}

impl<'r> From<Multifix<'r, DiaryInput<'r>, Cow<'r, str>>> for PreOperatorDreamBool<'r> {
    fn from(other: Multifix<'r, DiaryInput<'r>, Cow<'r, str>>) -> Self {
        PreOperatorDreamBool::DreamStr(other)
    }
}

impl<'r> From<Multifix<'r, DiaryInput<'r>, bool>> for PreOperatorDreamBool<'r> {
    fn from(other: Multifix<'r, DiaryInput<'r>, bool>) -> Self {
        PreOperatorDreamBool::DreamBool(other)
    }
}

impl<'r> From<Multifix<'r, Cow<'r, str>, bool>> for PreOperatorDreamBool<'r> {
    fn from(other: Multifix<'r, Cow<'r, str>, bool>) -> Self {
        PreOperatorDreamBool::StrBool(other)
    }
}

impl<'r> From<Multifix<'r, bool, bool>> for PreOperatorDreamBool<'r> {
    fn from(other: Multifix<'r, bool, bool>) -> Self {
        PreOperatorDreamBool::BoolBool(other)
    }
}

impl<'r> PreAnyOperator<'r> for PreOperatorDreamBool<'r> {
    type In = DiaryInput<'r>;
    type Final = OperatorDreamBool<'r>;
    fn edge(&mut self, parent: &Self::Final, side: Side) -> Result<(), Error> {
        match self {
            PreOperatorDreamBool::DreamStr(_) | PreOperatorDreamBool::DreamBool(_) => {
                match parent {
                    OperatorDreamBool::Str(_) => Err(Error::WrongOperandType {
                        got: "str".to_string(),
                        exp: "Dream".to_string(),
                        side,
                    }),
                    OperatorDreamBool::Bool(_) => Err(Error::WrongOperandType {
                        got: "bool".to_string(),
                        exp: "Dream".to_string(),
                        side,
                    }),
                }
            }
            PreOperatorDreamBool::StrBool(op) => match parent {
                OperatorDreamBool::Str(parent) => op.edge(parent.clone(), side),
                OperatorDreamBool::Bool(_) => Err(Error::WrongOperandType {
                    got: "bool".to_string(),
                    exp: "str".to_string(),
                    side,
                }),
            },
            PreOperatorDreamBool::BoolBool(op) => match parent {
                OperatorDreamBool::Str(_) => Err(Error::WrongOperandType {
                    got: "str".to_string(),
                    exp: "bool".to_string(),
                    side,
                }),
                OperatorDreamBool::Bool(parent) => op.edge(parent.clone(), side),
            },
        }
    }
    fn edge_with_input(
        &mut self,
        parent: SharedNode<'r, Self::In>,
        side: Side,
    ) -> Result<(), Error> {
        match self {
            PreOperatorDreamBool::DreamBool(op) => op.edge(parent, side),
            PreOperatorDreamBool::DreamStr(op) => op.edge(parent, side),
            PreOperatorDreamBool::StrBool(_) => Err(Error::WrongOperandType {
                got: "Dream".to_string(),
                exp: "str".to_string(),
                side,
            }),
            PreOperatorDreamBool::BoolBool(_) => Err(Error::WrongOperandType {
                got: "Dream".to_string(),
                exp: "bool".to_string(),
                side,
            }),
        }
    }
    fn try_finalize(self) -> Result<Self::Final, (Self, Error)> {
        match self {
            PreOperatorDreamBool::BoolBool(op) => op
                .try_finalize()
                .map(OperatorDreamBool::Bool)
                .map_err(|(s, e)| (PreOperatorDreamBool::BoolBool(s), e)),
            PreOperatorDreamBool::StrBool(op) => op
                .try_finalize()
                .map(OperatorDreamBool::Bool)
                .map_err(|(s, e)| (PreOperatorDreamBool::StrBool(s), e)),
            PreOperatorDreamBool::DreamStr(op) => op
                .try_finalize()
                .map(OperatorDreamBool::Str)
                .map_err(|(s, e)| (PreOperatorDreamBool::DreamStr(s), e)),
            PreOperatorDreamBool::DreamBool(op) => op
                .try_finalize()
                .map(OperatorDreamBool::Bool)
                .map_err(|(s, e)| (PreOperatorDreamBool::DreamBool(s), e)),
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash, IntoPrimitive)]
#[repr(u8)]
// Allow unused enum variants because these are fixed variants
// which all might be used in the future.
#[allow(dead_code)]
enum DreamPrecedence {
    Zero = 0x0,
    BooleanOr = 0x1,
    BooleanAnd = 0x2,
    BooleanPrefix = 0x3,
    Comparison = 0x4,
    DreamExtract = 0x5,
    DreamPrefix = 0x6,
    DreamPostfix = 0x7,
}

impl PartialOrd<Self> for DreamPrecedence {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl Ord for DreamPrecedence {
    fn cmp(&self, other: &Self) -> Ordering {
        u8::from(*self).cmp(&u8::from(*other))
    }
}

impl Fix for DreamPrecedence {
    const ZERO: Self = DreamPrecedence::Zero;
    fn position(&self) -> Position {
        match self {
            DreamPrecedence::Zero => Position::PREFIX,
            DreamPrecedence::BooleanPrefix
            | DreamPrecedence::DreamExtract
            | DreamPrecedence::DreamPrefix => Position::PREFIX,
            DreamPrecedence::DreamPostfix => Position::POSTFIX,
            DreamPrecedence::BooleanOr
            | DreamPrecedence::BooleanAnd
            | DreamPrecedence::Comparison => Position::INFIX,
        }
    }
    fn first(&self) -> Side {
        match self {
            DreamPrecedence::Zero => LEFT,
            DreamPrecedence::BooleanPrefix
            | DreamPrecedence::DreamExtract
            | DreamPrecedence::DreamPrefix => RIGHT,
            DreamPrecedence::DreamPostfix => LEFT,
            DreamPrecedence::BooleanOr
            | DreamPrecedence::BooleanAnd
            | DreamPrecedence::Comparison => LEFT,
        }
    }
}

fn wrap_infix<'r, In: 'r, Out: 'r, F>(
    precedence: DreamPrecedence,
    f: F,
) -> Result<Option<PreNode<DreamPrecedence, OperatorDreamBool<'r>, PreOperatorDreamBool<'r>>>, Error>
where
    PreOperatorDreamBool<'r>: From<Multifix<'r, In, Out>>,
    F: for<'a, 'b> Fn(&'a In, &'b In) -> Out + 'static,
{
    Ok(Some(PreNode::PreOperator(PreOperator {
        precedence,
        ty: Multifix::<In, Out>::infix(Box::new(f)).into(),
    })))
}

fn wrap_prefix<'r, In: 'r, Out: 'r, F>(
    precedence: DreamPrecedence,
    f: F,
) -> Result<Option<PreNode<DreamPrecedence, OperatorDreamBool<'r>, PreOperatorDreamBool<'r>>>, Error>
where
    PreOperatorDreamBool<'r>: From<Multifix<'r, In, Out>>,
    F: for<'a> Fn(&'a In) -> Out + 'static,
{
    Ok(Some(PreNode::PreOperator(PreOperator {
        precedence,
        ty: Multifix::<In, Out>::prefix(Box::new(f)).into(),
    })))
}

fn wrap_postfix<'r, In: 'r, Out: 'r, F>(
    precedence: DreamPrecedence,
    f: F,
) -> Result<Option<PreNode<DreamPrecedence, OperatorDreamBool<'r>, PreOperatorDreamBool<'r>>>, Error>
where
    PreOperatorDreamBool<'r>: From<Multifix<'r, In, Out>>,
    F: for<'a> Fn(&'a In) -> Out + 'static,
{
    Ok(Some(PreNode::PreOperator(PreOperator {
        precedence,
        ty: Multifix::<In, Out>::postfix(Box::new(f)).into(),
    })))
}

lazy_static! {
    pub static ref CONSTANT_REGEX: Regex = Regex::new(r#"^c"(?P<value>[^"]*)""#).unwrap();
    pub static ref CONTAINS_NAME_REGEX: Regex =
        Regex::new(r#"^Name\s*>\s*c"(?P<value>[^"]*)""#).unwrap();
    pub static ref CONTAINS_TEXT_REGEX: Regex =
        Regex::new(r#"^Text\s*>\s*c"(?P<value>[^"]*)""#).unwrap();
    pub static ref CONTAINS_NOTES_REGEX: Regex =
        Regex::new(r#"^Anmerkungen\s*>\s*c"(?P<value>[^"]*)""#).unwrap();
    pub static ref HAS_PARENT_CONTAINING_REGEX: Regex =
        Regex::new(r#"^t"(?P<category>[^"]*)"\s*::>\s*c"(?P<value>[^"]*)""#).unwrap();
    pub static ref HAS_PARENT_REGEX: Regex =
        Regex::new(r#"^t"(?P<category>[^"]*)"\s*::\s*c"(?P<value>[^"]*)""#).unwrap();
    pub static ref TAG_IS_CONTAINING_REGEX: Regex =
        Regex::new(r#"^t"(?P<category>[^"]*)"\s*:>\s*c"(?P<value>[^"]*)""#).unwrap();
    pub static ref TAG_IS_REGEX: Regex =
        Regex::new(r#"^t"(?P<category>[^"]*)"\s*:\s*c"(?P<value>[^"]*)""#).unwrap();
}

fn has_parent_containing<'r, I: IntoIterator<Item = (&'r String, &'r TagTree)>>(
    value: &TagSpec,
    d: &DiaryInput<'r>,
    categories: I,
) -> bool {
    for (category, tagtree) in categories.into_iter() {
        for tagspec in d.dream().d.get_tags(&category).into_iter().flatten() {
            if tagtree
                .parent_chain(tagspec.base_name())
                .into_iter()
                .flatten()
                .any(|tag| {
                    tag.name().as_str().contains(value.base_name().as_str())
                        && tagspec.strength() == value.strength()
                })
            {
                return true;
            }
        }
    }
    false
}

fn has_parent<'r, I: IntoIterator<Item = (&'r String, &'r TagTree)>>(
    value: &TagSpec,
    d: &DiaryInput<'r>,
    categories: I,
) -> bool {
    for (category, tagtree) in categories.into_iter() {
        for tagspec in d.dream().d.get_tags(&category).into_iter().flatten() {
            if tagtree
                .parent_chain(tagspec.base_name())
                .into_iter()
                .flatten()
                .any(|tag| {
                    tag.name() == value.base_name() && tagspec.strength() == value.strength()
                })
            {
                return true;
            }
        }
    }
    false
}

fn get_operand<'r>(
    s: &mut &str,
    variables: &HashSet<String>,
) -> Result<Option<PreNode<DreamPrecedence, OperatorDreamBool<'r>, PreOperatorDreamBool<'r>>>, Error>
{
    use DreamPrecedence::*;

    if s.starts_with('(') {
        *s = &s["(".len()..];
        Ok(Some(PreNode::BracketOpen))
    } else if s.starts_with(')') {
        *s = &s[")".len()..];
        Ok(Some(PreNode::BracketClose))
    } else if s.starts_with("&") {
        *s = &s["&".len()..];
        wrap_infix(BooleanAnd, |a, b| *a && *b)
    } else if s.starts_with("|") {
        *s = &s["|".len()..];
        wrap_infix(BooleanOr, |a, b| *a || *b)
    } else if s.starts_with('!') {
        *s = &s["!".len()..];
        wrap_prefix(BooleanPrefix, |a: &bool| !a)
    } else if let Some(op) = CONTAINS_NAME_REGEX.captures(s) {
        if let Some(value) = op.name("value") {
            *s = &s[op.get(0).unwrap().as_str().len()..];
            let value = Vec::<u16>::from_iter(value.as_str().encode_utf16());
            wrap_postfix(DreamPostfix, move |d: &DiaryInput<'r>| {
                collation::contains_icu(&value, &d.name())
            })
        } else {
            Err(Error::InvalidOperator(
                CONTAINS_NAME_REGEX
                    .find(s)
                    // Won't panic because the regex does match
                    .unwrap()
                    .as_str()
                    .to_owned(),
            ))
        }
    } else if let Some(op) = CONTAINS_TEXT_REGEX.captures(s) {
        if let Some(value) = op.name("value") {
            *s = &s[op.get(0).unwrap().as_str().len()..];
            let value = Vec::<u16>::from_iter(value.as_str().encode_utf16());
            wrap_postfix(DreamPostfix, move |d: &DiaryInput<'r>| {
                collation::contains_icu(&value, &d.text())
            })
        } else {
            Err(Error::InvalidOperator(
                CONTAINS_TEXT_REGEX
                    .find(s)
                    // Won't panic because the regex does match
                    .unwrap()
                    .as_str()
                    .to_owned(),
            ))
        }
    } else if let Some(op) = CONTAINS_NOTES_REGEX.captures(s) {
        if let Some(value) = op.name("value") {
            *s = &s[op.get(0).unwrap().as_str().len()..];
            let value = Vec::<u16>::from_iter(value.as_str().encode_utf16());
            wrap_postfix(DreamPostfix, move |d: &DiaryInput<'r>| {
                collation::contains_icu(&value, &d.notes())
            })
        } else {
            Err(Error::InvalidOperator(
                CONTAINS_NOTES_REGEX
                    .find(s)
                    // Won't panic because the regex does match
                    .unwrap()
                    .as_str()
                    .to_owned(),
            ))
        }
    } else if s.starts_with("Art") {
        *s = &s["Art".len()..];
        wrap_prefix(DreamExtract, |d: &DiaryInput<'r>| {
            Cow::from(
                d.dream()
                    .d
                    .get_entry_text("Art")
                    .map(String::as_str)
                    .unwrap_or_default(),
            )
        })
    } else if s.starts_with("==") {
        *s = &s["==".len()..];
        wrap_infix(Comparison, |a: &Cow<'r, str>, b: &Cow<'r, str>| collation::equals_col(a, b))
    } else if s.starts_with(">") {
        *s = &s[">".len()..];
        wrap_infix(Comparison, |a: &Cow<'r, str>, b: &Cow<'r, str>| {
            a.contains(b.as_ref())
        })
    } else if let Some(op) = HAS_PARENT_CONTAINING_REGEX.captures(s) {
        if let (Some(category), Some(value)) = (op.name("category"), op.name("value")) {
            *s = &s[op.get(0).unwrap().as_str().len()..];
            let category = category.as_str().to_owned();
            let value = TagSpec::from(value.as_str().to_owned());
            wrap_postfix(DreamPostfix, move |d: &DiaryInput<'r>| {
                if category.is_empty() {
                    has_parent_containing(&value, d, d.tag.iter())
                } else {
                    has_parent_containing(&value, d, d.tag.get_key_value(&category).into_iter())
                }
            })
        } else {
            Err(Error::InvalidOperator(
                HAS_PARENT_CONTAINING_REGEX
                    .find(s)
                    // Won't panic because the regex does match
                    .unwrap()
                    .as_str()
                    .to_owned(),
            ))
        }
    } else if let Some(op) = HAS_PARENT_REGEX.captures(s) {
        if let (Some(category), Some(value)) = (op.name("category"), op.name("value")) {
            *s = &s[op.get(0).unwrap().as_str().len()..];
            let category = category.as_str().to_owned();
            let value = TagSpec::from(value.as_str().to_owned());
            wrap_postfix(DreamPostfix, move |d: &DiaryInput<'r>| {
                if category.is_empty() {
                    has_parent(&value, d, d.tag.iter())
                } else {
                    has_parent(&value, d, d.tag.get_key_value(&category).into_iter())
                }
            })
        } else {
            Err(Error::InvalidOperator(
                HAS_PARENT_REGEX
                    .find(s)
                    // Won't panic because the regex does match
                    .unwrap()
                    .as_str()
                    .to_owned(),
            ))
        }
    } else if let Some(op) = TAG_IS_CONTAINING_REGEX.captures(s) {
        if let (Some(category), Some(value)) = (op.name("category"), op.name("value")) {
            *s = &s[op.get(0).unwrap().as_str().len()..];
            let category = category.as_str().to_owned();
            let value = TagSpec::from(value.as_str().to_owned());
            wrap_postfix(DreamPostfix, move |d: &DiaryInput<'r>| {
                if category.is_empty() {
                    d.dream()
                        .d
                        .get_all_tags()
                        .values()
                        .map(|cat| cat.iter())
                        .flatten()
                        .any(|tag| {
                            tag.base_name().contains(&value.base_name().as_str())
                                && tag.strength() == value.strength()
                        })
                } else {
                    d.dream()
                        .d
                        .get_tags(&category)
                        .into_iter()
                        .flatten()
                        .any(|tag| {
                            tag.base_name().contains(&value.base_name().as_str())
                                && tag.strength() == value.strength()
                        })
                }
            })
        } else {
            Err(Error::InvalidOperator(
                TAG_IS_CONTAINING_REGEX
                    .find(s)
                    // Won't panic because the regex does match
                    .unwrap()
                    .as_str()
                    .to_owned(),
            ))
        }
    } else if let Some(op) = TAG_IS_REGEX.captures(s) {
        if let (Some(category), Some(value)) = (op.name("category"), op.name("value")) {
            *s = &s[op.get(0).unwrap().as_str().len()..];
            let category = category.as_str().to_owned();
            let value = TagSpec::from(value.as_str().to_owned());
            wrap_postfix(DreamPostfix, move |d: &DiaryInput<'r>| {
                if category.is_empty() {
                    d.dream().d.has_tag_somewhere(&value)
                } else {
                    d.dream().d.has_tag(&category, &value)
                }
            })
        } else {
            Err(Error::InvalidOperator(
                TAG_IS_REGEX
                    .find(s)
                    // Won't panic because the regex does match
                    .unwrap()
                    .as_str()
                    .to_owned(),
            ))
        }
    } else if s.starts_with(' ') {
        *s = s.trim_start();
        Ok(None)
    } else if let Some(constant) = CONSTANT_REGEX.captures(s) {
        if let Some(value) = constant.name("value") {
            *s = &s[constant.get(0).unwrap().as_str().len()..];
            Ok(Some(PreNode::Constant(OperatorDreamBool::Str(
                SharedNode::from(Constant::new(Cow::from(value.as_str().to_owned()))),
            ))))
        } else {
            Err(Error::InvalidOperator(
                CONSTANT_REGEX
                    .find(s)
                    // Won't panic because the regex does match
                    .unwrap()
                    .as_str()
                    .to_owned(),
            ))
        }
    } else if let Some(varname) = variables.iter().find(|varname| s.starts_with(*varname)) {
        *s = &s[varname.len()..];
        Ok(Some(PreNode::Variable {
            name: varname.to_string(),
        }))
    } else {
        Err(Error::InvalidOperator(s.to_string()))
    }
}

pub fn search(
    mut diary: &mut Diary,
    condition: &str,
    interrupt_check_interval: usize,
    further: Weak<()>,
) -> Result<Vec<DreamID>, CmdError> {
    let mut stage = stage_diary!(diary; DL, TAG);
    if let (Ok(dreamlist), Ok(tagforest)) = lock!(stage; DreamList, TagForest) {
        let mut graph =
            parse::<DiaryInput, bool, DreamPrecedence, OperatorDreamBool, PreOperatorDreamBool, _>(
                condition,
                |s| {
                    let variables = HashSet::from_iter(std::iter::once("#".to_string()));
                    get_operand(s, &variables)
                },
            )?;

        let mut res = Vec::new();
        for (i, (&id, dream)) in dreamlist.iter().enumerate() {
            graph.assign_variable_with("#", |var: &mut Option<DiaryInput>| match var {
                Some(value) => value.assign_dream(dream),
                None => *var = Some(DiaryInput::new(dream, &tagforest)),
            });
            if graph.calc().ok_or_else(|| CmdError::Unspec)? {
                res.push(id);
            }
            if i % interrupt_check_interval == interrupt_check_interval - 1
                && further.upgrade().is_none()
            {
                return Err(CmdError::Interrupted);
            }
        }
        Ok(res)
    } else {
        panic!()
    }
}

#[cfg(test)]
mod tests {
    use std::sync::Arc;

    use crate::operate::tests::standard_diary;

    use super::*;

    #[test]
    fn tag_regexes() {
        assert!(CONSTANT_REGEX.is_match(r#"c"Personen""#));
        assert!(HAS_PARENT_CONTAINING_REGEX.is_match(r#"t"Personen" ::> c"God""#));
        assert!(HAS_PARENT_REGEX.is_match(r#"t"Personen" :: c"God""#));
        assert!(TAG_IS_CONTAINING_REGEX.is_match(r#"t"Personen" :> c"God""#));
        assert!(TAG_IS_REGEX.is_match(r#"t"Personen" : c"God""#));
    }

    #[test]
    fn test_search() {
        crate::elements::tag::names::collation::init_collator();
        let mut diary = standard_diary();
        let further = Arc::new(());
        let res = search(
            &mut diary,
            r#"#t"Personen" : c"God""#,
            std::usize::MAX,
            Arc::downgrade(&further),
        )
        .unwrap();
        assert!(
            res.contains(&DreamID::new(chrono::NaiveDate::from_ymd(1054, 07, 06), 0)),
            "res not as expected {:?}",
            res
        );
        assert!(
            !res.contains(&DreamID::new(chrono::NaiveDate::from_ymd(1054, 07, 05), 0)),
            "res not as expected {:?}",
            res
        );
    }

    #[cfg(feature = "icu")]
    #[test]
    fn test_search_dreamtext() {
        super::collation::init_collator();
        crate::elements::tag::names::collation::init_collator();
        let mut diary = standard_diary();
        let further = Arc::new(());
        let res = search(
            &mut diary,
            r#"#Text > c"good""#,
            std::usize::MAX,
            Arc::downgrade(&further),
        )
        .unwrap();
        assert!(
            res.contains(&DreamID::new(chrono::NaiveDate::from_ymd(1054, 07, 04), 1)),
            "res not as expected {:?}",
            res
        );
        let res = search(
            &mut diary,
            r#"#Text > c"hatt""#,
            std::usize::MAX,
            Arc::downgrade(&further),
        )
        .unwrap();
        assert!(
            res.contains(&DreamID::new(chrono::NaiveDate::from_ymd(1054, 07, 06), 0)),
            "res not as expected {:?}",
            res
        );
    }
}
