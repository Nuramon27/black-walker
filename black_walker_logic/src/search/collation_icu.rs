use std::convert::TryFrom;
use std::sync::Once;
use std::cmp::Ordering;

use rust_icu_sys::{UBreakIterator, UChar, UColAttributeValue, UCollator, UErrorCode};

#[repr(C)]
struct UStringSearch {
    _unused: [u8; 0],
}
extern "C" {
    fn usearch_openFromCollator_72(
        pattern: *const UChar,
        patternlength: i32,
        text: *const UChar,
        textlength: i32,
        collator: *const UCollator,
        breakiter: *mut UBreakIterator,
        status: *mut UErrorCode,
    ) -> *mut UStringSearch;
    fn usearch_first_72(srsrch: *mut UStringSearch, status: *mut UErrorCode) -> i32;
    fn usearch_close_72(searchiter: *mut UStringSearch);
}

static mut COLLATOR_SEARCH_RAW: Option<*mut UCollator> = None;
pub(super) static mut COLLATOR_SEARCH: Option<rust_icu_ucol::UCollator> = None;

static COLLATOR_INIT: Once = Once::new();

pub fn init_collator() {
    #[allow(unused_unsafe)]
    COLLATOR_INIT.call_once(|| unsafe {
        let mut status = UErrorCode::U_ZERO_ERROR;
        let col_raw =
            rust_icu_sys::ucol_open_72(b"DE_de\0".as_ptr() as *const libc::c_char, &mut status);
        assert_ne!(col_raw, std::ptr::null_mut());
        rust_icu_sys::ucol_setStrength_72(col_raw, UColAttributeValue::UCOL_PRIMARY);
        COLLATOR_SEARCH_RAW = Some(col_raw);
        let mut col = rust_icu_ucol::UCollator::try_from("DE_de").unwrap();
        col.set_strength(UColAttributeValue::UCOL_PRIMARY);
        COLLATOR_SEARCH = Some(col);
    });
}

pub fn equals_col(lhs: &str, rhs: &str) -> bool {
    unsafe {
        if let Ok(Ordering::Equal) = COLLATOR_SEARCH.as_ref().unwrap().strcoll_utf8(&lhs, &rhs) {
            true
        } else {
            false
        }
    }
}

pub fn contains_icu(pattern: &[u16], text: &[u16]) -> bool {
    let mut status = UErrorCode::U_ZERO_ERROR;
    unsafe {
        let searcher = usearch_openFromCollator_72(
            pattern.as_ptr() as *const UChar,
            pattern.len() as i32,
            text.as_ptr() as *const UChar,
            text.len() as i32,
            COLLATOR_SEARCH_RAW.unwrap(),
            std::ptr::null_mut(),
            &mut status,
        );
        if searcher.is_null() {
            return false;
        }
        let res = usearch_first_72(searcher, &mut status) >= 0;
        usearch_close_72(searcher);
        res
    }
}

#[cfg(test)]
mod tests {
    use std::iter::FromIterator;

    use super::*;

    #[test]
    fn test_usearch() {
        init_collator();
        let mut status = UErrorCode::U_ZERO_ERROR;
        let pattern = Vec::<u16>::from_iter("hatt".encode_utf16());
        let text = Vec::<u16>::from_iter(
            "Das war wohl nichts. Hätte ich doch nur aufgepasst.".encode_utf16(),
        );
        unsafe {
            let searcher = usearch_openFromCollator_72(
                pattern.as_ptr() as *const UChar,
                pattern.len() as i32,
                text.as_ptr() as *const UChar,
                text.len() as i32,
                COLLATOR_SEARCH_RAW.unwrap(),
                std::ptr::null_mut(),
                &mut status,
            );
            let result = usearch_first_72(searcher, &mut status);
            assert!(result >= 0);
            usearch_close_72(searcher);
        }
    }
}
