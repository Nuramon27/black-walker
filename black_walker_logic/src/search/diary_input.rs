use std::cell::{RefCell, RefMut};

use crate::{DreamItem, TagForest};

#[derive(Debug, Clone)]
struct StringCloneUtf16 {
    s: Vec<u16>,
    insync: bool,
}

impl Default for StringCloneUtf16 {
    fn default() -> Self {
        Self {
            s: Default::default(),
            insync: false,
        }
    }
}

impl StringCloneUtf16 {
    pub fn assign(&mut self, s: &str) {
        self.s.clear();
        self.s.extend(s.encode_utf16());
        self.insync = true;
    }
    #[inline]
    pub fn get(&mut self, original: &str) -> &mut [u16] {
        if !self.insync {
            self.assign(original);
        }
        &mut self.s
    }
    pub fn get_with<F: FnOnce(&mut Vec<u16>)>(&mut self, original_setter: F) -> &mut [u16] {
        if !self.insync {
            original_setter(&mut self.s);
            self.insync = true;
        }
        &mut self.s
    }
    #[inline]
    pub fn desync(&mut self) {
        self.insync = false;
    }
}

#[derive(Debug, Default, Clone)]
struct DreamStringsUtf16 {
    pub name: StringCloneUtf16,
    pub text: StringCloneUtf16,
    pub notes: StringCloneUtf16,
}

impl DreamStringsUtf16 {
    pub fn desync(&mut self) {
        self.name.desync();
        self.text.desync();
        self.notes.desync();
    }
}

#[derive(Debug, Clone)]
pub struct DiaryInput<'r> {
    dream: &'r DreamItem,
    pub tag: &'r TagForest,
    strings_utf16: RefCell<DreamStringsUtf16>,
}

impl<'r> DiaryInput<'r> {
    pub fn new(dream: &'r DreamItem, tag: &'r TagForest) -> Self {
        DiaryInput {
            dream,
            tag,
            strings_utf16: RefCell::new(DreamStringsUtf16::default()),
        }
    }
    #[inline(always)]
    pub fn dream(&self) -> &'r DreamItem {
        &self.dream
    }
    #[inline(always)]
    pub fn assign_dream<'a>(&mut self, dream: &'r DreamItem)
    where
        'r: 'a,
    {
        self.dream = dream;
        self.strings_utf16.borrow_mut().desync();
    }
    #[inline]
    pub fn name(&self) -> RefMut<[u16]> {
        RefMut::map(self.strings_utf16.borrow_mut(), |cache| {
            cache.name.get(&self.dream.d.name)
        })
    }
    #[inline]
    pub fn text(&self) -> RefMut<[u16]> {
        RefMut::map(self.strings_utf16.borrow_mut(), |cache| {
            cache.text.get(&self.dream.d.text)
        })
    }
    #[inline]
    pub fn notes(&self) -> RefMut<[u16]> {
        RefMut::map(self.strings_utf16.borrow_mut(), |cache| {
            cache.notes.get_with(|s| {
                s.clear();
                for note in self.dream.d.notes.values() {
                    s.extend(note.note.encode_utf16())
                }
            })
        })
    }
}
