use std::cmp::Ordering;
use std::fmt;

use chrono::NaiveDate;
use serde::{Deserialize, Serialize};

use super::Dream;

/// A Pair of a night and a number that can be used to uniquely identify a dream.
#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash, Serialize, Deserialize)]
pub struct DreamID {
    /// The index of the dream it its night.
    ///
    /// I.e. the third dream in a night has `idx = 2`.
    pub idx: u32,
    /// The date of the day that followed the dream
    pub date: NaiveDate,
}

impl fmt::Display for DreamID {
    /// Displays the DreamID in the format "{`date`} ({`idx`})".
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{} ({})", self.date, self.idx)
    }
}

impl PartialOrd for DreamID {
    /// `DreamID` a is greater than `DreamID` b, if the night of a is
    /// earlier (!) than the night of b or if its index is greater than
    /// that of b.
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl Ord for DreamID {
    /// `DreamID` a is greater than `DreamID` b, if the night of a is
    /// earlier (!) than the night of b or if its index is greater than
    /// that of b.
    fn cmp(&self, other: &Self) -> Ordering {
        match self.date.cmp(&other.date) {
            Ordering::Greater => Ordering::Less,
            Ordering::Less => Ordering::Greater,
            Ordering::Equal => match self.idx.cmp(&other.idx) {
                Ordering::Greater => Ordering::Greater,
                Ordering::Less => Ordering::Less,
                Ordering::Equal => Ordering::Equal,
            },
        }
    }
}

impl DreamID {
    /// Constructs a new `DreamID` with date `date` and `idx` set to zero.
    pub fn from_date(date: NaiveDate) -> DreamID {
        DreamID { date, idx: 0 }
    }

    /// Constructs a new `DreamID` from `date` and `idx`.
    pub fn new(date: NaiveDate, idx: u32) -> DreamID {
        DreamID { date, idx }
    }
}

/// The `DreamItem` struct is the base item of the `DreamList`.
///
/// Its content is a dream [`d`](DreamItem::d).
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct DreamItem {
    /// The date of the day that followed the dream
    pub(crate) id: DreamID,
    /// The [`Dream`] represented by this `DreamItem`
    pub d: Dream,
}

impl Default for DreamItem {
    /// Constructs a `DreamItem` with `date` set to today
    /// and index set to zero.
    ///
    /// The [dream](DreamItem::d) is default constructed.
    fn default() -> DreamItem {
        DreamItem {
            id: DreamID {
                date: chrono::Local::today().naive_local(),
                idx: 0,
            },
            d: Dream::default(),
        }
    }
}

impl DreamItem {
    /// Constructs a new `DreamItem` from `id` and dream `d`.
    pub fn new(id: DreamID, d: Dream) -> DreamItem {
        DreamItem { id, d }
    }

    /// Returns the date of the `DreamItem`.
    pub fn date(&self) -> NaiveDate {
        self.id.date
    }

    /// Returns the index of the `DreamItem`.
    pub fn idx(&self) -> u32 {
        self.id.idx
    }

    /// Returns the `id` of the `DreamItem`.
    pub fn id(&self) -> DreamID {
        DreamID {
            date: self.id.date,
            idx: self.id.idx,
        }
    }
}
