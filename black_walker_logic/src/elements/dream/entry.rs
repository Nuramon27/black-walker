//! Implementations of functions on [`Dream`](super::Dream) for
//! the handling of entries.

use std::collections::{HashMap, HashSet};
use std::iter::FromIterator;

use chrono::{NaiveDate, NaiveTime};

use crate::elements::tag;
use crate::mconvenience::latex::{esc_for_latex, Instruction};
use crate::parse::CommaSep;
use crate::TagForest;

use super::Dream;

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
/// The type of an entry of a [`Dream`](super::Dream).
#[allow(dead_code)]
pub(crate) enum EntryType {
    Text,
    List,
    Date,
    Time,
    UInt,
    Int,
}

impl Dream {
    /// Returns the entries of `self` in `category`
    pub fn get_entry_text(&self, category: &str) -> Option<&String> {
        self.entry_text.get(category)
    }
    pub fn get_entry_list(&self, category: &str) -> Option<&HashSet<String>> {
        self.entry_list.get(category)
    }
    pub fn get_entry_date(&self, category: &str) -> Option<NaiveDate> {
        self.entry_date.get(category).copied()
    }
    pub fn get_entry_time(&self, category: &str) -> Option<NaiveTime> {
        self.entry_time.get(category).copied()
    }
    pub fn get_entry_uint(&self, category: &str) -> Option<u32> {
        self.entry_uint.get(category).copied()
    }
    pub fn get_entry_int(&self, category: &str) -> Option<i32> {
        self.entry_int.get(category).copied()
    }
    pub fn get_entry_bool(&self, category: &str) -> bool {
        self.entry_bool.contains(category)
    }

    /// Returns the entries of `self`.
    pub fn get_all_entries_text(&self) -> &HashMap<String, String> {
        &self.entry_text
    }
    pub fn get_all_entries_list(&self) -> &HashMap<String, HashSet<String>> {
        &self.entry_list
    }
    pub fn get_all_entries_date(&self) -> &HashMap<String, NaiveDate> {
        &self.entry_date
    }
    pub fn get_all_entries_time(&self) -> &HashMap<String, NaiveTime> {
        &self.entry_time
    }
    pub fn get_all_entries_uint(&self) -> &HashMap<String, u32> {
        &self.entry_uint
    }
    pub fn get_all_entries_int(&self) -> &HashMap<String, i32> {
        &self.entry_int
    }
    pub fn get_all_entries_bool(&self) -> &HashSet<String> {
        &self.entry_bool
    }

    /// Sets the entry with name `category` to `entryval`.
    pub fn set_entry_text(&mut self, category: &str, entryval: String) {
        self.entry_text.insert(category.to_string(), entryval);
    }
    pub fn set_entry_list<I: IntoIterator<Item = String>>(&mut self, category: &str, entryval: I) {
        self.entry_list
            .insert(category.to_string(), HashSet::from_iter(entryval));
    }
    pub fn set_entry_date(&mut self, category: &str, entryval: NaiveDate) {
        self.entry_date.insert(category.to_string(), entryval);
    }
    pub fn set_entry_time(&mut self, category: &str, entryval: NaiveTime) {
        self.entry_time.insert(category.to_string(), entryval);
    }
    pub fn set_entry_uint(&mut self, category: &str, entryval: u32) {
        self.entry_uint.insert(category.to_string(), entryval);
    }
    pub fn set_entry_int(&mut self, category: &str, entryval: i32) {
        self.entry_int.insert(category.to_string(), entryval);
    }
    pub fn set_entry_bool(&mut self, category: &str, entryval: bool) {
        if entryval {
            self.entry_bool.insert(category.to_string());
        } else {
            self.entry_bool.remove(category);
        }
    }

    /// Removes the entry with name `category`.
    ///
    /// The item is returned if it exists.
    pub fn remove_entry_text(&mut self, category: &str) -> Option<String> {
        self.entry_text.remove(category)
    }
    pub fn remove_entry_list(&mut self, category: &str) -> Option<HashSet<String>> {
        self.entry_list.remove(category)
    }
    pub fn remove_entry_date(&mut self, category: &str) -> Option<NaiveDate> {
        self.entry_date.remove(category)
    }
    pub fn remove_entry_time(&mut self, category: &str) -> Option<NaiveTime> {
        self.entry_time.remove(category)
    }
    pub fn remove_entry_uint(&mut self, category: &str) -> Option<u32> {
        self.entry_uint.remove(category)
    }
    pub fn remove_entry_int(&mut self, category: &str) -> Option<i32> {
        self.entry_int.remove(category)
    }
    // Returns an `Instruction` containing the text of `self`.
    pub fn inst_text(&self) -> Instruction {
        Instruction::Environment {
            name: "DreamText".to_string(),
            args: Vec::new(),
            content: esc_for_latex(&self.text),
        }
    }
    // Returns an `Instruction` containing the notes of `self`.
    pub fn inst_notes(&self) -> Vec<Instruction> {
        self.notes
            .iter()
            .filter_map(|(date, notes)| {
                if !notes.note.trim().is_empty() {
                    Some(Instruction::Environment {
                        name: "Notes".to_string(),
                        args: vec![date.format("%d.%m.%Y").to_string()],
                        content: esc_for_latex(&notes.note),
                    })
                } else {
                    None
                }
            })
            .collect()
    }
    #[cfg(feature = "stat")]
    // Returns the entry of `self` in `category` of type `ty` as a latex instruction.
    pub(crate) fn inst_entry(&self, category: &str, ty: EntryType) -> Option<Instruction> {
        let value = match ty {
            EntryType::Text => esc_for_latex(self.get_entry_text(category)?),
            EntryType::List => esc_for_latex(&self.get_entry_list(category)?.comma_sep()),
            EntryType::Date => self
                .get_entry_date(category)?
                .format("%dd.%mm.%YYYY")
                .to_string(),
            EntryType::Time => self.get_entry_time(category)?.format("%HH:%MM").to_string(),
            EntryType::UInt => self.get_entry_uint(category)?.to_string(),
            EntryType::Int => self.get_entry_int(category)?.to_string(),
        };
        Some(Instruction::inst(
            r"\Entry".to_string(),
            vec![esc_for_latex(category), value],
        ))
    }
    // Returns the tags of `self` as instructions.
    pub fn inst_tags(&self, tagforest: &TagForest) -> HashMap<String, Instruction> {
        let mut res = HashMap::new();

        for (k, v) in self.tag.iter() {
            let mut tagvec = Vec::from_iter(v);
            if let Some(tagtree) = tagforest.get(k) {
                tag::sort_by_relevance(tagtree, &mut tagvec);
            }
            res.insert(
                k.clone(),
                Instruction::inst(
                    r"\Tag".to_string(),
                    vec![esc_for_latex(k), esc_for_latex(&tagvec.comma_sep())],
                ),
            );
        }
        res
    }
}
