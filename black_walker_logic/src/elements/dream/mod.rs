//! Contains anything related to the representation of dreams.
//!
//! A single dream is represented by a [`Dream`](Dream), while the [`DreamList`](crate::DreamList)
//! stores a list of dreams. An element of the `DreamList` is represented by a
//! [`DreamItem`], which additionally has a [`Date`](chrono::NaiveDate)
//! and an Index.

use std::collections::{BTreeMap, HashMap, HashSet};
use std::iter::FromIterator;

use chrono::prelude::*;

use crate::{TagName, TagSpec, TagSpecification, TagStr};

pub mod dream_item;
pub mod entry;
pub(crate) mod store;

pub use dream_item::{DreamID, DreamItem};

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct Note {
    pub note: String,
    pub place: String,
    pub time: NaiveTime,
}

/// The `Dream` struct is a representation of a dream.
///
///
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Dream {
    /// The title of the dream
    pub name: String,
    /// The dream text
    pub text: String,
    /// A place to take notes to a dream
    pub notes: BTreeMap<NaiveDate, Note>,

    entry_text: HashMap<String, String>,
    entry_list: HashMap<String, HashSet<String>>,
    entry_date: HashMap<String, NaiveDate>,
    entry_time: HashMap<String, NaiveTime>,
    entry_uint: HashMap<String, u32>,
    entry_int: HashMap<String, i32>,
    entry_bool: HashSet<String>,
    tag: HashMap<String, HashSet<TagSpec>>,
}

impl Default for Dream {
    fn default() -> Dream {
        let mut entry_text = HashMap::new();
        entry_text.insert("Art".to_string(), "Trübtraum".to_string());
        Dream {
            name: String::new(),
            text: String::new(),
            notes: BTreeMap::new(),
            entry_text: HashMap::new(),
            entry_list: HashMap::new(),
            entry_date: HashMap::new(),
            entry_time: HashMap::new(),
            entry_uint: HashMap::new(),
            entry_int: HashMap::new(),
            entry_bool: HashSet::new(),
            tag: HashMap::new(),
        }
    }
}

impl Dream {
    /// Constructs an empty `Dream` with title `name`, dreamed on `date`
    /// and of `kind`.
    pub fn from_oblig(name: String, kind: String) -> Dream {
        let mut entry_text = HashMap::new();
        entry_text.insert("Art".to_string(), kind);
        Dream {
            name,
            text: String::new(),
            notes: BTreeMap::new(),
            entry_text,
            entry_date: HashMap::new(),
            entry_list: HashMap::new(),
            entry_time: HashMap::new(),
            entry_uint: HashMap::new(),
            entry_int: HashMap::new(),
            entry_bool: HashSet::new(),
            tag: HashMap::new(),
        }
    }

    /// Returns whether the dream is tagged with `tagname` in `category`
    ///
    /// `tagspec` is matched in the exact given form.
    /// For determining, whether the dream has a tag with the same name as `tagspec`
    /// in strong or weak form, use [`has_tag_any`](Dream::has_tag_any)
    ///
    /// *See also:*
    /// [`has_tag_somewhere`](Dream::has_tag_somewhere)
    pub fn has_tag(&self, category: &str, tagspec: &TagSpec) -> bool {
        if let Some(c) = self.tag.get(category) {
            c.contains(tagspec)
        } else {
            false
        }
    }

    /// Returns whether the dream is tagged with `tagname` in `category`
    /// in strong or weak form.
    ///
    /// This means the function checks whether the tags in `category` contain
    /// `tagname` with or without a trailing '*'.
    ///
    /// ## Examples
    ///
    /// ```
    /// use std::convert::TryInto;
    /// use std::str::FromStr;
    /// use black_walker_logic::{Dream, TagSpec};
    ///
    /// let mut d = Dream::from_oblig("A".to_string(), "Ordinary Dream".to_string());
    /// d.add_tags("Personen", vec![
    ///     TagSpec::from_str("Albus Dumbledore").unwrap(),
    ///     TagSpec::from_str("Hermione Granger*").unwrap(),
    /// ]);
    ///
    /// assert!(d.has_tag_any("Personen", "Albus Dumbledore"));
    /// assert!(d.has_tag_any("Personen", "Albus Dumbledore*"));
    /// assert!(d.has_tag_any("Personen", "Hermione Granger"));
    /// assert!(d.has_tag_any("Personen", "Hermione Granger*"));
    ///
    /// assert!(!d.has_tag_any("Personen", "Ronald Weasley"));
    /// assert!(!d.has_tag_any("Personen", "Ronald Weasley*"));
    /// ```
    ///
    /// *See also:*
    /// [`has_tag_somewhere`](Dream::has_tag_somewhere),
    /// [`has_tag_somewhere_any`](Dream::has_tag_somewhere_any)
    pub fn has_tag_any(&self, category: &str, tagname: impl TagSpecification) -> bool {
        if let Some(c) = self.tag.get(category) {
            c.iter().any(|v| v.base_name() == tagname.base_name())
        } else {
            false
        }
    }

    /// Returns whether the dream is tagged with `tagname` in any category
    ///
    /// `tagspec` is matched in the exact given form.
    /// For determining, whether the dream has a tag with the same name as `tagspec`
    /// in strong or weak form, use [`has_tag_somewhere_any`](Dream::has_tag_somewhere_any)
    ///
    /// *See also:*
    /// [`has_tag`](Dream::has_tag)
    pub fn has_tag_somewhere(&self, tagspec: &TagSpec) -> bool {
        for cat in self.tag.values() {
            if cat.contains(tagspec) {
                return true;
            }
        }
        false
    }

    /// Returns whether the dream is tagged with `tagname` in any category
    /// in strong or weak form.
    ///
    /// This means the function checks whether the tags contain
    /// `tagname` with or without a trailing '*'.
    ///
    /// ## Examples
    /// ```
    /// use std::convert::TryInto;
    /// use std::str::FromStr;
    /// use black_walker_logic::{Dream, TagSpec};

    /// let mut d = Dream::from_oblig("A".to_string(), "Ordinary Dream".to_string());
    /// d.add_tags("Personen", vec![
    ///     TagSpec::from_str("Albus Dumbledore").unwrap(),
    ///     TagSpec::from_str("Hermione Granger*").unwrap(),
    /// ]);
    /// d.add_tags("Orte", vec![
    ///     TagSpec::from_str("Heidelberg").unwrap()
    /// ]);
    ///
    /// assert!(d.has_tag_somewhere_any("Albus Dumbledore"));
    /// assert!(d.has_tag_somewhere_any("Albus Dumbledore*"));
    /// assert!(d.has_tag_somewhere_any("Hermione Granger"));
    /// assert!(d.has_tag_somewhere_any("Hermione Granger*"));
    ///
    /// assert!(d.has_tag_somewhere_any("Heidelberg"));
    ///
    /// assert!(!d.has_tag_somewhere_any("Ronald Weasley"));
    /// assert!(!d.has_tag_somewhere_any("Ronald Weasley*"));
    /// ```
    ///
    /// *See also:*
    /// [`has_tag_somewhere`](Dream::has_tag_somewhere),
    /// [`has_tag_any`](Dream::has_tag_any)
    pub fn has_tag_somewhere_any(&self, tagname: impl TagSpecification) -> bool {
        for cat in self.tag.values() {
            if cat.iter().any(|v| v.base_name() == tagname.base_name()) {
                return true;
            }
        }
        false
    }

    /// Returns all tags of `self` in `category`.
    ///
    /// *See also:*
    /// [`get_all_tags`](Dream::get_all_tags)
    pub fn get_tags(&self, category: &str) -> Option<&HashSet<TagSpec>> {
        self.tag.get(category)
    }

    /// Returns the tags of `self`.
    ///
    /// *See also:*
    /// [`get_tags`](Dream::get_tags)
    pub fn get_all_tags(&self) -> &HashMap<String, HashSet<TagSpec>> {
        &self.tag
    }

    /// Sets the tags of `self` to `newtag`.
    ///
    /// The old tags are dropped.
    ///
    /// *See also:*
    /// [`add_to_all_tags`](Dream::add_to_all_tags),
    /// [`set_tags`](Dream::set_tags)
    pub fn set_all_tags(&mut self, newtag: HashMap<String, HashSet<TagSpec>>) {
        self.tag = newtag
    }

    /// Adds all tags in `newtag` to the tags of the dream.
    ///
    /// This means that for every category in `newtag`, all contained
    /// tags are added to the same category in the dreams tags.
    /// If a category does not exist, it is created.
    ///
    /// *See also:*
    /// [`set_all_tags`](Dream::set_all_tags),
    /// [`add_tags`](Dream::add_tags)
    pub fn add_to_all_tags<T: IntoIterator<Item = TagSpec>>(&mut self, newtag: HashMap<String, T>) {
        for (key, val) in newtag {
            let cat = self.tag.entry(key).or_insert(HashSet::new());
            for t in val {
                cat.insert(t);
            }
        }
    }

    /// Sets the tags in `category` to  the elements of `newtags`.
    ///
    /// *See also:*
    /// [`set_all_tags`](Dream::set_all_tags),
    /// [`add_tags`](Dream::add_tags)
    pub fn set_tags<T: IntoIterator<Item = TagSpec>>(&mut self, category: &str, newtags: T) {
        let cat = self
            .tag
            .entry(category.to_string())
            .or_insert(HashSet::new());
        cat.clear();
        for t in newtags {
            cat.insert(t);
        }
    }

    /// Adds the tag `newtag` to the tags in `category`.
    ///
    /// *See also:*
    /// [`add_tags`](Dream::add_tags)
    /// [`add_to_all_tags`](Dream::add_to_all_tags),
    /// [`set_tags`](Dream::set_tags)
    pub fn add_tag(&mut self, category: &str, newtag: TagSpec) {
        let cat = self
            .tag
            .entry(category.to_string())
            .or_insert(HashSet::new());
        cat.insert(newtag);
    }

    /// Adds the elements of `newtags` to the tags in `category`.
    ///
    /// *See also:*
    /// [`add_tag`](Dream::add_tag)
    /// [`add_to_all_tags`](Dream::add_to_all_tags),
    /// [`set_tags`](Dream::set_tags)
    pub fn add_tags<T: IntoIterator<Item = TagSpec>>(&mut self, category: &str, newtags: T) {
        let cat = self
            .tag
            .entry(category.to_string())
            .or_insert(HashSet::new());
        for t in newtags {
            cat.insert(t);
        }
    }

    /// Renames tag `oldname` in `category` to `newname` if a tag of that name exists
    ///
    /// Returns `true` on success, false otherwise.
    ///
    /// If the tag exists in weak form, it is renamed to the weak form of `newname`.
    /// `oldname` must be the basename of the tag.
    pub fn rename_tag(&mut self, category: &str, oldname: &TagStr, newname: TagName) -> bool {
        let mut res = false;
        if let Some(cat) = self.tag.get_mut(category) {
            let matching_tags = Vec::from_iter(cat.iter().filter_map(|tagspec| {
                if tagspec.base_name() == oldname {
                    Some(tagspec.clone())
                } else {
                    None
                }
            }));

            for matching_tag in matching_tags {
                if let Some(mut spec) = cat.take(&matching_tag) {
                    spec.set_base_name(newname.clone());
                    cat.insert(spec);
                    res = true;
                }
            }
        }
        res
    }

    /// Removes the strong and weak form of tag `tagname` in `category`.
    ///
    /// If the category is empty after the removal, the category itself is deleted.
    /// Nothing is done if the tag does not exist in `category`.
    ///
    /// Returns a tuple of two booleans: The first one indicating, whether
    /// `tagname` was a strong tag of the dream, the second one
    /// indicating whether `tagname` was a weak tag of the dream.
    ///
    /// ## Examples
    ///
    /// ```
    /// use std::convert::TryInto;
    /// use std::str::FromStr;
    /// use black_walker_logic::{Dream, TagSpec};
    ///
    /// let mut d = Dream::from_oblig("A".to_string(), "Ordinary Dream".to_string());
    /// d.add_tags("Personen", vec![
    ///     TagSpec::from_str("Albus Dumbledore").unwrap(),
    ///     TagSpec::from_str("Hermione Granger*").unwrap(),
    ///     TagSpec::from_str("Lord Voldemort").unwrap(),
    ///     TagSpec::from_str("Lord Voldemort*").unwrap(),
    /// ]);
    ///
    /// assert_eq!(d.remove_tag("Personen", "Albus Dumbledore".try_into().unwrap()), (true, false));
    /// assert_eq!(d.remove_tag("Personen", "Hermione Granger".try_into().unwrap()), (false, true));
    /// assert_eq!(d.remove_tag("Personen", "Lord Voldemort".try_into().unwrap()), (true, true));
    /// assert_eq!(d.remove_tag("Personen", "Ronald Weasley".try_into().unwrap()), (false, false));
    ///
    /// assert!(d.get_all_tags().is_empty());
    /// ```
    pub fn remove_tag(&mut self, category: &str, tagname: &TagStr) -> (bool, bool) {
        if let Some(cat) = self.tag.get_mut(category) {
            let all_strengths = TagSpec::in_all_strengths(tagname.to_owned());
            let res = (cat.remove(&all_strengths.0), cat.remove(&all_strengths.1));
            if cat.is_empty() {
                self.tag.remove(category);
            }
            res
        } else {
            (false, false)
        }
    }

    /// Removes `category` from the dream.
    ///
    /// If the category existed, its content is returned.
    pub fn remove_category(&mut self, category: &str) -> Option<HashSet<TagSpec>> {
        self.tag.remove(category)
    }

    /// Changes the category of tag `tagname` from `old_category` to `new_category`.
    ///
    /// Returns whether the tag existed in the old category.
    ///
    /// ## Examples
    /// ```
    /// use std::str::FromStr;
    /// use std::convert::TryInto;
    /// use std::collections::HashSet;
    ///
    /// use black_walker_logic::{Dream, TagSpec};

    /// let mut d = Dream::from_oblig("A".to_string(), "Ordinary Dream".to_string());
    /// d.add_tags("Personen", vec![
    ///     TagSpec::from_str("Albus Dumbledore").unwrap(),
    ///     TagSpec::from_str("Hermione Granger*").unwrap(),
    ///     TagSpec::from_str("Lord Voldemort").unwrap(),
    ///     TagSpec::from_str("Lord Voldemort*").unwrap(),
    /// ]);
    ///
    /// assert!(d.change_tag_category("Personen", "P", "Albus Dumbledore".try_into().unwrap()));
    /// assert!(d.change_tag_category("Personen", "P", "Hermione Granger".try_into().unwrap()));
    /// assert_eq!(d.get_tags("Personen").map(HashSet::len), Some(2));
    /// assert!(d.change_tag_category("Personen", "P", "Lord Voldemort".try_into().unwrap()));
    /// assert!(!d.change_tag_category("Personen", "P", "Ronald Weasley".try_into().unwrap()));
    ///
    /// assert_eq!(d.get_tags("Personen"), None);
    /// assert_eq!(d.get_tags("P").map(HashSet::len), Some(4));
    /// ```
    pub fn change_tag_category(
        &mut self,
        old_category: &str,
        new_category: &str,
        tagname: &TagStr,
    ) -> bool {
        if let Some(old_cat) = self.tag.get_mut(old_category) {
            let all_strengths = TagSpec::in_all_strengths(tagname.to_owned());
            let tags = (
                old_cat.take(&all_strengths.0),
                old_cat.take(&all_strengths.1),
            );
            let res = tags.0.is_some() || tags.1.is_some();
            if old_cat.is_empty() {
                self.tag.remove(old_category);
            }
            let new_cat = self
                .tag
                .entry(new_category.to_string())
                .or_insert(HashSet::new());
            if let Some(strong_form) = tags.0 {
                new_cat.insert(strong_form);
            }
            if let Some(weak_form) = tags.1 {
                new_cat.insert(weak_form);
            }
            res
        } else {
            false
        }
    }

    /// Returns the difference between the tags of `other` and the tags of this dream.
    ///
    /// The first field of the returned tuple are the tags of this dream that are
    /// not present in `other` (the "insertions"). The second field are the tags
    /// of `other` that are not present in `self` (the "deletions").
    pub fn tag_diff(
        &self,
        other: &Self,
    ) -> (
        HashMap<String, HashSet<TagSpec>>,
        HashMap<String, HashSet<TagSpec>>,
    ) {
        let mut insertions = HashMap::new();
        for (cat, v) in &self.tag {
            let del = HashSet::from_iter(
                v.difference(&other.tag.get(cat).unwrap_or(&HashSet::new()))
                    .cloned(),
            );
            if !del.is_empty() {
                insertions.insert(cat.clone(), del);
            }
        }
        let mut deletions = HashMap::new();
        for (cat, v) in &other.tag {
            let ins = HashSet::from_iter(
                v.difference(&self.tag.get(cat).unwrap_or(&HashSet::new()))
                    .cloned(),
            );
            if !ins.is_empty() {
                deletions.insert(cat.clone(), ins);
            }
        }
        (insertions, deletions)
    }
}

#[cfg(test)]
mod tests {
    use std::collections::{HashMap, HashSet};
    use std::iter::FromIterator;

    use super::super::tag::names::TagSpec;
    use super::Dream;

    #[test]
    fn tag_diff() {
        let mut d = Dream::from_oblig("A".to_string(), "Ordinary Dream".to_string());
        d.add_tags(
            "Personen",
            vec!["Albus Dumbledore", "Lord Voldemort"]
                .into_iter()
                .map(TagSpec::from),
        );
        d.add_tags("Orte", vec!["Heidelberg"].into_iter().map(TagSpec::from));
        d.add_tags("Animals", vec!["Rabbit"].into_iter().map(TagSpec::from));
        let mut e = Dream::from_oblig("B".to_string(), "Ordinary Dream".to_string());
        e.add_tags(
            "Personen",
            vec!["Albus Dumbledore", "Harry Potter"]
                .into_iter()
                .map(TagSpec::from),
        );
        e.add_tags(
            "Dream Signs",
            vec!["Many-fingered Hand"].into_iter().map(TagSpec::from),
        );
        e.add_tags("Animals", vec!["Rabbit"].into_iter().map(TagSpec::from));

        let mut insertions = HashMap::new();
        let mut deletions = HashMap::new();
        insertions.insert(
            "Personen".to_string(),
            HashSet::from_iter(vec!["Lord Voldemort"].into_iter().map(TagSpec::from)),
        );
        insertions.insert(
            "Orte".to_string(),
            HashSet::from_iter(vec!["Heidelberg"].into_iter().map(TagSpec::from)),
        );
        deletions.insert(
            "Personen".to_string(),
            HashSet::from_iter(vec!["Harry Potter"].into_iter().map(TagSpec::from)),
        );
        deletions.insert(
            "Dream Signs".to_string(),
            HashSet::from_iter(vec!["Many-fingered Hand"].into_iter().map(TagSpec::from)),
        );
        assert_eq!(d.tag_diff(&e), (insertions, deletions));
    }
}
