use std::collections::{BTreeMap, HashMap, HashSet};
use std::iter::FromIterator;

use chrono::{NaiveDate, NaiveTime};
use serde::{Deserialize, Serialize};

use crate::TagSpec;

use super::{Dream, DreamID, DreamItem, Note};

#[derive(Debug, Clone, PartialEq, Eq)]
#[derive(Serialize, Deserialize)]
pub struct Note2_3_2 {
    pub date: NaiveDate,
    pub note: String,
    pub place: String,
    pub time: NaiveTime,
}

impl From<(NaiveDate, Note)> for Note2_3_2 {
    fn from((date, other): (NaiveDate, Note)) -> Self {
        Self {
            date,
            note: other.note,
            place: other.place,
            time: other.time
        }
    }
}

impl From<Note2_3_2> for (NaiveDate, Note) {
    fn from(other: Note2_3_2) -> (NaiveDate, Note) {
        (other.date, Note {
            note: other.note,
            place: other.place,
            time: other.time
        })
    }
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub struct DreamItem2_3_1 {
    pub name: String,
    pub text: String,

    pub entry_bool: Vec<String>,

    pub notes: Vec<Note2_3_2>,

    pub id: DreamID,

    #[serde(serialize_with = "toml::ser::tables_last")]
    pub entry_text: HashMap<String, String>,
    #[serde(serialize_with = "toml::ser::tables_last")]
    pub entry_uint: HashMap<String, u32>,
    #[serde(serialize_with = "toml::ser::tables_last")]
    pub entry_int: HashMap<String, i32>,
    #[serde(serialize_with = "toml::ser::tables_last")]
    pub entry_date: HashMap<String, NaiveDate>,
    #[serde(serialize_with = "toml::ser::tables_last")]
    pub entry_time: HashMap<String, NaiveTime>,
    #[serde(serialize_with = "toml::ser::tables_last")]
    pub entry_list: HashMap<String, Vec<String>>,
    #[serde(serialize_with = "toml::ser::tables_last")]
    pub tag: HashMap<String, Vec<TagSpec>>,
}

impl From<DreamItem> for DreamItem2_3_1 {
    fn from(other: DreamItem) -> Self {
        DreamItem2_3_1 {
            id: other.id,
            name: other.d.name,
            text: other.d.text,
            notes: other
                .d
                .notes
                .into_iter()
                .map(Into::into)
                .collect(),
            entry_text: other.d.entry_text,
            entry_list: other
                .d
                .entry_list
                .into_iter()
                .map(|(k, v)| (k, Vec::from_iter(v)))
                .collect(),
            entry_date: other.d.entry_date,
            entry_time: other.d.entry_time,
            entry_uint: other.d.entry_uint,
            entry_int: other.d.entry_int,
            entry_bool: {
                let mut res = Vec::from_iter(other.d.entry_bool);
                res.sort();
                res
            },
            tag: other
                .d
                .tag
                .into_iter()
                .map(|(k, v)| (k, Vec::from_iter(v)))
                .collect(),
        }
    }
}

impl From<DreamItem2_3_1> for DreamItem {
    fn from(other: DreamItem2_3_1) -> Self {
        DreamItem {
            id: other.id,
            d: Dream {
                name: other.name,
                text: other.text,
                notes: other
                    .notes
                    .into_iter()
                    .map(Into::into)
                    .collect(),
                entry_text: other.entry_text,
                entry_list: other
                    .entry_list
                    .into_iter()
                    .map(|(k, v)| (k, HashSet::from_iter(v)))
                    .collect(),
                entry_date: other.entry_date,
                entry_time: other.entry_time,
                entry_uint: other.entry_uint,
                entry_int: other.entry_int,
                entry_bool: HashSet::from_iter(other.entry_bool),
                tag: other
                    .tag
                    .into_iter()
                    .map(|(k, v)| (k, HashSet::from_iter(v)))
                    .collect(),
            },
        }
    }
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub struct DreamItem2_3_0 {
    pub name: String,
    pub text: String,
    pub notes: Vec<(NaiveDate, (NaiveTime, String))>,

    pub entry_bool: Vec<String>,

    pub id: DreamID,

    #[serde(serialize_with = "toml::ser::tables_last")]
    pub entry_text: HashMap<String, String>,
    #[serde(serialize_with = "toml::ser::tables_last")]
    pub entry_uint: HashMap<String, u32>,
    #[serde(serialize_with = "toml::ser::tables_last")]
    pub entry_int: HashMap<String, i32>,
    #[serde(serialize_with = "toml::ser::tables_last")]
    pub entry_date: HashMap<String, NaiveDate>,
    #[serde(serialize_with = "toml::ser::tables_last")]
    pub entry_time: HashMap<String, NaiveTime>,
    #[serde(serialize_with = "toml::ser::tables_last")]
    pub entry_list: HashMap<String, Vec<String>>,
    #[serde(serialize_with = "toml::ser::tables_last")]
    pub tag: HashMap<String, Vec<TagSpec>>,
}

impl From<DreamItem2_3_0> for DreamItem {
    fn from(other: DreamItem2_3_0) -> Self {
        DreamItem {
            id: other.id,
            d: Dream {
                name: other.name,
                text: other.text,
                notes: other
                    .notes
                    .into_iter()
                    .map(|(date, (time, note))| (date, Note { time, place: String::new(), note }))
                    .collect(),
                entry_text: other.entry_text,
                entry_list: other
                    .entry_list
                    .into_iter()
                    .map(|(k, v)| (k, HashSet::from_iter(v)))
                    .collect(),
                entry_date: other.entry_date,
                entry_time: other.entry_time,
                entry_uint: other.entry_uint,
                entry_int: other.entry_int,
                entry_bool: HashSet::from_iter(other.entry_bool),
                tag: other
                    .tag
                    .into_iter()
                    .map(|(k, v)| (k, HashSet::from_iter(v)))
                    .collect(),
            },
        }
    }
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub struct DreamItem2_2_0 {
    /// The title of the dream
    pub name: String,
    /// The dream text
    pub text: String,
    /// A place to take notes to a dream
    pub notes: String,

    pub entry_bool: Vec<String>,

    pub id: DreamID,

    #[serde(serialize_with = "toml::ser::tables_last")]
    pub entry_text: HashMap<String, String>,
    #[serde(serialize_with = "toml::ser::tables_last")]
    pub entry_uint: HashMap<String, u32>,
    #[serde(serialize_with = "toml::ser::tables_last")]
    pub entry_int: HashMap<String, i32>,
    #[serde(serialize_with = "toml::ser::tables_last")]
    pub entry_date: HashMap<String, NaiveDate>,
    #[serde(serialize_with = "toml::ser::tables_last")]
    pub entry_time: HashMap<String, NaiveTime>,
    #[serde(serialize_with = "toml::ser::tables_last")]
    pub entry_list: HashMap<String, Vec<String>>,
    #[serde(serialize_with = "toml::ser::tables_last")]
    pub tag: HashMap<String, Vec<TagSpec>>,
}

impl From<DreamItem2_2_0> for DreamItem {
    fn from(other: DreamItem2_2_0) -> Self {
        DreamItem {
            id: other.id,
            d: Dream {
                name: other.name,
                text: other.text,
                notes: {
                    let mut res = BTreeMap::new();
                    if !other.notes.trim().is_empty() {
                        res.insert(
                            other
                                .entry_date
                                .get("Verfasst am")
                                .copied()
                                .unwrap_or(other.id.date),
                            Note {
                                time: other
                                    .entry_time
                                    .get("Verfasst um")
                                    .copied()
                                    .unwrap_or(NaiveTime::from_hms(0, 0, 0)),
                                place: String::new(),
                                note: other.notes,
                            },
                        );
                    }
                    res
                },
                entry_text: other.entry_text,
                entry_list: other
                    .entry_list
                    .into_iter()
                    .map(|(k, v)| (k, HashSet::from_iter(v)))
                    .collect(),
                entry_date: other.entry_date,
                entry_time: other.entry_time,
                entry_uint: other.entry_uint,
                entry_int: other.entry_int,
                entry_bool: HashSet::from_iter(other.entry_bool),
                tag: other
                    .tag
                    .into_iter()
                    .map(|(k, v)| (k, HashSet::from_iter(v)))
                    .collect(),
            },
        }
    }
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub struct DreamItem2_0_0 {
    /// The title of the dream
    pub name: String,
    /// The dream text
    pub text: String,
    /// A place to take notes to a dream
    pub notes: String,

    pub id: DreamID,

    #[serde(serialize_with = "toml::ser::tables_last")]
    pub entry_text: HashMap<String, String>,
    #[serde(serialize_with = "toml::ser::tables_last")]
    pub entry_uint: HashMap<String, u32>,
    #[serde(serialize_with = "toml::ser::tables_last")]
    pub entry_int: HashMap<String, i32>,
    #[serde(serialize_with = "toml::ser::tables_last")]
    pub entry_date: HashMap<String, NaiveDate>,
    #[serde(serialize_with = "toml::ser::tables_last")]
    pub entry_time: HashMap<String, NaiveTime>,
    #[serde(serialize_with = "toml::ser::tables_last")]
    pub entry_list: HashMap<String, Vec<String>>,
    #[serde(serialize_with = "toml::ser::tables_last")]
    pub tag: HashMap<String, Vec<TagSpec>>,
}

impl From<DreamItem2_0_0> for DreamItem {
    fn from(other: DreamItem2_0_0) -> Self {
        DreamItem {
            id: other.id,
            d: Dream {
                name: other.name,
                text: other.text,
                notes: {
                    let mut res = BTreeMap::new();
                    if !other.notes.trim().is_empty() {
                        res.insert(
                            other
                                .entry_date
                                .get("Verfasst am")
                                .copied()
                                .unwrap_or(other.id.date),
                            Note {
                                time: other
                                    .entry_time
                                    .get("Verfasst um")
                                    .copied()
                                    .unwrap_or(NaiveTime::from_hms(0, 0, 0)),
                                place: String::new(),
                                note: other.notes,
                            },
                        );
                    }
                    res
                },
                entry_text: other.entry_text,
                entry_list: other
                    .entry_list
                    .into_iter()
                    .map(|(k, v)| (k, HashSet::from_iter(v)))
                    .collect(),
                entry_date: other.entry_date,
                entry_time: other.entry_time,
                entry_uint: other.entry_uint,
                entry_int: other.entry_int,
                entry_bool: HashSet::new(),
                tag: other
                    .tag
                    .into_iter()
                    .map(|(k, v)| (k, HashSet::from_iter(v)))
                    .collect(),
            },
        }
    }
}
