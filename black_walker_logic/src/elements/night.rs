use std::cmp::Ordering;

use chrono::{NaiveDate, NaiveDateTime};

/// A date with inverse ordering.
///
/// This is a simple wrapper around [`chrono::NaiveDate`] which implements
/// the comparison traits in inverse manner, i.e.
/// `revdate_1 > revdate_2` if and only if `revdate_1` is earlier
/// than `revdate_2`.
#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub struct RevDate {
    pub d: NaiveDate,
}

impl From<NaiveDate> for RevDate {
    fn from(other: NaiveDate) -> Self {
        RevDate { d: other }
    }
}

impl From<RevDate> for NaiveDate {
    fn from(other: RevDate) -> Self {
        other.d
    }
}

impl PartialOrd for RevDate {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl Ord for RevDate {
    fn cmp(&self, other: &Self) -> Ordering {
        Ord::cmp(&other.d, &self.d)
    }
}

impl PartialEq<NaiveDate> for RevDate {
    fn eq(&self, other: &NaiveDate) -> bool {
        PartialEq::eq(&self.d, other)
    }
}

impl PartialOrd<NaiveDate> for RevDate {
    fn partial_cmp(&self, other: &NaiveDate) -> Option<Ordering> {
        PartialOrd::partial_cmp(other, &self.d)
    }
}

impl PartialEq<RevDate> for NaiveDate {
    fn eq(&self, other: &RevDate) -> bool {
        PartialEq::eq(self, &other.d)
    }
}

impl PartialOrd<RevDate> for NaiveDate {
    fn partial_cmp(&self, other: &RevDate) -> Option<Ordering> {
        PartialOrd::partial_cmp(&other.d, self)
    }
}

impl std::fmt::Display for RevDate {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> Result<(), std::fmt::Error> {
        self.d.fmt(f)
    }
}

#[derive(Debug, Default, Clone, PartialEq, Eq, Hash)]
pub struct Night {
    sleep_sections: Vec<SleepSection>,
}

impl Night {
    /// Creates a `Night` from a vec of sleep sections.
    pub fn from_sleep_sections(sleep_sections: Vec<SleepSection>) -> Self {
        Self { sleep_sections }
    }
    /// Returns the sleep section at `index`.
    pub fn sleep_section(&self, index: usize) -> Option<&SleepSection> {
        self.sleep_sections.get(index)
    }
    /// Obtain the list of sleep sections.
    pub fn sleep_sections(&self) -> &Vec<SleepSection> {
        &self.sleep_sections
    }
    /// Obtain a mutable reference to the list of sleep sections.
    pub fn sleep_sections_mut(&mut self) -> &mut Vec<SleepSection> {
        &mut self.sleep_sections
    }
}

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct SleepSection {
    start: NaiveDateTime,
    end: NaiveDateTime,
    applied_techniques: Vec<String>,
    ease_of_fallasleep: Option<u32>,
}

impl SleepSection {
    pub fn new(
        start: NaiveDateTime,
        end: NaiveDateTime,
        applied_techniques: Vec<String>,
        ease_of_fallasleep: Option<u32>,
    ) -> Self {
        Self {
            start,
            end,
            applied_techniques,
            ease_of_fallasleep,
        }
    }
    pub fn from_basic(start: NaiveDateTime, end: NaiveDateTime) -> Self {
        Self {
            start,
            end,
            applied_techniques: Vec::new(),
            ease_of_fallasleep: None,
        }
    }
    pub fn start(&self) -> NaiveDateTime {
        self.start
    }
    pub fn end(&self) -> NaiveDateTime {
        self.end
    }
    pub fn start_end(&self) -> (NaiveDateTime, NaiveDateTime) {
        (self.start, self.end)
    }
    pub fn ease_of_fallasleep(&self) -> Option<u32> {
        self.ease_of_fallasleep
    }
    pub fn set_start(&mut self, value: NaiveDateTime) {
        self.start = value
    }
    pub fn set_end(&mut self, value: NaiveDateTime) {
        self.end = value
    }
    pub fn set_ease_of_fallasleep(&mut self, value: Option<u32>) {
        self.ease_of_fallasleep = value;
    }
}

pub mod store {
    use serde::{Deserialize, Serialize};

    use crate::NightList;

    use super::*;

    #[derive(Debug, Clone, PartialEq, Eq, Hash, Serialize, Deserialize)]
    pub struct SleepSection2_0_0 {
        pub start: NaiveDateTime,
        pub end: NaiveDateTime,
        pub applied_techniques: Vec<String>,
        ease_of_fallasleep: Option<u32>,
    }

    impl From<SleepSection> for SleepSection2_0_0 {
        fn from(other: SleepSection) -> Self {
            Self {
                start: other.start,
                end: other.end,
                applied_techniques: other.applied_techniques,
                ease_of_fallasleep: other.ease_of_fallasleep,
            }
        }
    }

    impl From<SleepSection2_0_0> for SleepSection {
        fn from(other: SleepSection2_0_0) -> Self {
            Self {
                start: other.start,
                end: other.end,
                applied_techniques: other.applied_techniques,
                ease_of_fallasleep: other.ease_of_fallasleep,
            }
        }
    }

    #[derive(Debug, Clone, PartialEq, Eq, Hash, Serialize, Deserialize)]
    pub struct Night2_0_0 {
        sleep_sections: Vec<SleepSection2_0_0>,
    }

    impl From<Night> for Night2_0_0 {
        fn from(other: Night) -> Self {
            Self {
                sleep_sections: other
                    .sleep_sections
                    .into_iter()
                    .map(|elem| elem.into())
                    .collect(),
            }
        }
    }

    impl From<Night2_0_0> for Night {
        fn from(other: Night2_0_0) -> Self {
            Self {
                sleep_sections: other
                    .sleep_sections
                    .into_iter()
                    .map(|elem| elem.into())
                    .collect(),
            }
        }
    }

    #[derive(Debug, Clone, PartialEq, Eq, Hash, Serialize, Deserialize)]
    pub struct NightItem2_0_0 {
        pub date: NaiveDate,
        pub night: Night2_0_0,
    }

    impl From<(RevDate, Night)> for NightItem2_0_0 {
        fn from(other: (RevDate, Night)) -> Self {
            Self {
                date: other.0.into(),
                night: other.1.into(),
            }
        }
    }

    impl From<NightItem2_0_0> for (RevDate, Night) {
        fn from(other: NightItem2_0_0) -> Self {
            (other.date.into(), other.night.into())
        }
    }

    #[derive(Debug, Default, Clone, PartialEq, Eq, Hash, Serialize, Deserialize)]
    pub struct NightList2_0_0 {
        pub list: Vec<NightItem2_0_0>,
    }

    impl From<NightList> for NightList2_0_0 {
        fn from(other: NightList) -> Self {
            Self {
                list: other.into_iter().map(Into::into).collect(),
            }
        }
    }

    impl From<NightList2_0_0> for NightList {
        fn from(other: NightList2_0_0) -> Self {
            other.list.into_iter().map(Into::into).collect()
        }
    }
}
