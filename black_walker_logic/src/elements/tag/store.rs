use std::iter::FromIterator;

use serde::{Deserialize, Serialize};

use crate::Tag;
use crate::{TagForest, TagName, TagTree};

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub struct TagItem2_0_0 {
    pub parent: TagName,
    pub name: TagName,
    pub description: String,
}

impl From<(TagName, TagName, Tag)> for TagItem2_0_0 {
    fn from(other: (TagName, TagName, Tag)) -> Self {
        TagItem2_0_0 {
            parent: other.0,
            name: other.1,
            description: other.2.description,
        }
    }
}

impl From<TagItem2_0_0> for (TagName, TagName, Tag) {
    fn from(other: TagItem2_0_0) -> Self {
        (
            other.parent,
            other.name,
            Tag {
                description: other.description,
            },
        )
    }
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub struct TagTree2_0_0 {
    pub category: String,
    pub tree: Vec<TagItem2_0_0>,
}

impl From<(String, &TagTree)> for TagTree2_0_0 {
    fn from(other: (String, &TagTree)) -> Self {
        TagTree2_0_0 {
            category: other.0,
            tree: other
                .1
                .iter()
                .map(|node| {
                    TagItem2_0_0::from((
                        node.parent().to_owned(),
                        node.name().to_owned(),
                        node.content().clone(),
                    ))
                })
                .collect(),
        }
    }
}

impl From<TagTree2_0_0> for (String, TagTree) {
    fn from(other: TagTree2_0_0) -> Self {
        let mut res = TagTree::new();
        for node in other.tree {
            let (parent, name, tag) = node.into();
            res.insert_content(name, tag, &parent);
        }
        (other.category, res)
    }
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub struct TagForest2_0_0 {
    pub trees: Vec<TagTree2_0_0>,
}

impl From<TagForest> for TagForest2_0_0 {
    fn from(other: TagForest) -> Self {
        TagForest2_0_0 {
            trees: other
                .into_iter()
                .map(|(k, v)| TagTree2_0_0::from((k, &v)))
                .collect(),
        }
    }
}

impl From<TagForest2_0_0> for TagForest {
    fn from(other: TagForest2_0_0) -> Self {
        TagForest::from_iter(other.trees.into_iter().map(|tree| tree.into()))
    }
}
