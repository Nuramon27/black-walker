//! Contains anything necessary for working with tags,
//! in particular the [`Tag`] struct.

pub mod names;
pub(crate) mod store;

use std::borrow::Borrow;
use std::cmp::{Ord, Ordering};
use std::ops::{Add, AddAssign, Sub, SubAssign};

use num_traits::Zero;

use crate::{DreamList, TagForest, TagTree};

use names::{TagSpec, TagSpecification, TagStr};

/// The meta information to a tag, i.e. everything which goes
/// beyond its name.
#[derive(Debug, Default, Clone, PartialEq, Eq, Hash)]
pub struct Tag {
    /// A possibly lenthy description of the tag.
    pub description: String,
}

#[derive(Clone, Copy, Debug, PartialEq, Eq, Hash)]
/// Represents a 'Strong' or a 'Weak' count.
pub enum OccurMode {
    Strong,
    Weak,
}

/// The `Count` represents the two counts of a tag: A strong one
/// and a weak one.
///
/// It implements the necessary traits like `AddAssign`
/// `SubAssign` and [`IncDec`](crate::cache_tree::IncDec)
/// in order to be used as a count in [`cache_tree::Node`](crate::cache_tree::Node).
#[derive(Clone, Copy, Debug, Default, PartialEq, Eq, Hash)]
pub struct Count {
    pub strong: u32,
    pub weak: u32,
}

impl Add<Count> for Count {
    type Output = Count;
    /// Adds both counts of `rhs` to `self`.
    fn add(self, rhs: Self) -> Count {
        Count {
            strong: self.strong + rhs.strong,
            weak: self.weak + rhs.weak,
        }
    }
}

impl Sub<Count> for Count {
    type Output = Count;
    /// Subtracts both counts of `rhs` from `self` but saturates on the zero,
    /// i.e. uses `saturating_sub`.
    fn sub(self, rhs: Self) -> Count {
        Count {
            strong: self.strong.saturating_sub(rhs.strong),
            weak: self.weak.saturating_sub(rhs.weak),
        }
    }
}

impl AddAssign<Count> for Count {
    /// Addassigns both counts of `rhs` to `self
    fn add_assign(&mut self, rhs: Count) {
        self.strong += rhs.strong;
        self.weak += rhs.weak;
    }
}

impl SubAssign<Count> for Count {
    /// Subtracts both counts of `rhs` from `self` and assigns the result to `self`
    /// but saturates on the zero,
    /// i.e. uses `saturating_sub`.
    fn sub_assign(&mut self, rhs: Count) {
        self.strong = self.strong.saturating_sub(rhs.strong);
        self.weak = self.weak.saturating_sub(rhs.weak);
    }
}

impl AddAssign<&Count> for Count {
    /// Addassigns both counts of `rhs` to `self
    fn add_assign(&mut self, rhs: &Count) {
        self.strong += rhs.strong;
        self.weak += rhs.weak;
    }
}

impl SubAssign<&Count> for Count {
    /// Subtracts both counts of `rhs` from `self` and assigns the result to `self`
    /// but saturates on the zero,
    /// i.e. uses `saturating_sub`.
    fn sub_assign(&mut self, rhs: &Count) {
        self.strong = self.strong.saturating_sub(rhs.strong);
        self.weak = self.weak.saturating_sub(rhs.weak);
    }
}

impl Zero for Count {
    fn zero() -> Count {
        Count { strong: 0, weak: 0 }
    }
    fn is_zero(&self) -> bool {
        self.strong == 0 && self.weak == 0
    }
}

impl PartialOrd for Count {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl Ord for Count {
    fn cmp(&self, other: &Self) -> Ordering {
        (2 * self.strong + self.weak).cmp(&(2 * other.strong + other.weak))
    }
}

impl crate::cache_tree::IncDec for Count {
    type Mode = OccurMode;
    /// If `which` is [`OccurMode::Strong`], increments [`strong`](Count::strong),
    /// otherwise increments [`weak`](Count::weak).
    fn inc(&mut self, which: Self::Mode) {
        match which {
            OccurMode::Strong => self.strong += 1,
            OccurMode::Weak => self.weak += 1,
        }
    }
    /// If `which` is [`OccurMode::Strong`], decrements [`strong`](Count::strong),
    /// otherwise decrements [`weak`](Count::weak).
    ///
    /// Decrementing is done saturating at the zero,
    /// i.e. using `saturating_sub`.
    fn dec(&mut self, which: Self::Mode) {
        match which {
            OccurMode::Strong => self.strong = self.strong.saturating_sub(1),
            OccurMode::Weak => self.weak += self.weak.saturating_sub(1),
        }
    }
}

/// Checks whether `tagname` exists in exactly one category of `tagforest` and returns
/// this category if it is found.
///
/// Returns `None` if no category contains a tag named `tagname` or
/// if more than two categories do.
pub fn forest_contains_tag_in_one_category<'a>(
    tagforest: &'a TagForest,
    tagname: &TagStr,
) -> Option<&'a String> {
    let mut res = None;
    for (cat, tree) in tagforest {
        if tree.node_exists(tagname) {
            if res.is_some() {
                // The name is contained at least twice.
                return None;
            } else {
                // The name is found for the first time.
                res = Some(cat);
            }
        }
    }
    res
}

/// Counts the occurrences of all tags of all dreams in `dreamlist` and adds these
/// to the tags in `tagforest`.
///
/// This means that `occur` is called on every tag in each dream of `dreamlist`.
/// If a tag does not exist yet, it is created with a count of 1 in the corresponding
/// category.
pub fn occur_tags_in_dreamlist(tagforest: &mut TagForest, dreamlist: &DreamList) {
    for (_, dream) in dreamlist {
        for (catname, tagset) in dream.d.get_all_tags() {
            let category = if tagforest.contains_key(catname) {
                tagforest
                    .get_mut(catname)
                    // Won't panic because tagforest contains catname
                    .unwrap()
            } else {
                tagforest
                    .entry(catname.clone())
                    .or_insert_with(TagTree::new)
            };
            for tag in tagset {
                category.occur(tag.base_name(), tag.strength());
            }
        }
    }
}

/// Sorts `tags` by their “relevance”.
///
/// This relevance tries to estimate how important the occurrence of a tag is.
/// Currently, less frequent tags are considered more relevant than
/// more frequent tags, but tags with only one occurrenc (or less than four
/// weak occorrences) are considered unimportant. Furthermore, weak
/// forms are strictly less important than strong form.
///
/// Equally important tags are sorted alphabetically.
pub fn sort_by_relevance<S: Borrow<TagSpec>>(tagtree: &TagTree, tags: &mut [S]) {
    tags.sort_unstable_by(|lhs: &S, rhs: &S| {
        let lhs = lhs.borrow();
        let rhs = rhs.borrow();
        if lhs.strength() == OccurMode::Strong && rhs.strength() == OccurMode::Weak {
            return Ordering::Less;
        }
        if rhs.strength() == OccurMode::Strong && lhs.strength() == OccurMode::Weak {
            return Ordering::Greater;
        }
        match tagtree.get(lhs.base_name()) {
            Some(lhs) => match tagtree.get(rhs.base_name()) {
                Some(rhs) => {
                    let leftcount = 2 * lhs.count().strong + lhs.count().weak;
                    let rightcount = 2 * rhs.count().strong + rhs.count().weak;
                    if leftcount >= 4 {
                        if rightcount >= 4 {
                            leftcount.cmp(&rightcount).reverse()
                        } else {
                            Ordering::Less
                        }
                    } else {
                        leftcount.cmp(&rightcount)
                    }
                }
                None => Ordering::Less,
            },
            None => Ordering::Equal,
        }
        .reverse()
        .then_with(|| lhs.base_name().cmp(rhs.base_name()))
    })
}

#[cfg(test)]
mod tests {
    use crate::TagTree;
    use std::convert::TryInto;
    use std::iter::FromIterator;
    use std::str::FromStr;

    use super::names::TagName;

    use super::*;
    #[test]
    fn sort_by_count() {
        names::collation::init_collator();
        let mut t = TagTree::new();
        t.insert_content_at_root(TagName::from_str("Yoda").unwrap(), Tag::default());
        t.insert_content_at_root(TagName::from_str("Luke Skywalker").unwrap(), Tag::default());
        t.insert_content_at_root(TagName::from_str("Darth Vader").unwrap(), Tag::default());
        t.insert_content_at_root(TagName::from_str("Forest Gump").unwrap(), Tag::default());
        t.insert_content_at_root(TagName::from_str("Jenny").unwrap(), Tag::default());
        t.insert_content_at_root(
            TagName::from_str("Albus Dumbledore").unwrap(),
            Tag::default(),
        );
        t.insert_content_at_root(TagName::from_str("Lord Voldemort").unwrap(), Tag::default());
        for _ in 0..12 {
            t.occur("Jenny".try_into().unwrap(), OccurMode::Strong);
        }
        for _ in 0..8 {
            t.occur("Albus Dumbledore".try_into().unwrap(), OccurMode::Strong);
        }
        for _ in 0..4 {
            t.occur("Luke Skywalker".try_into().unwrap(), OccurMode::Strong);
            t.occur("Darth Vader".try_into().unwrap(), OccurMode::Strong);
        }
        for _ in 0..3 {
            t.occur("Forest Gump".try_into().unwrap(), OccurMode::Strong);
        }
        for _ in 0..2 {
            t.occur("Yoda".try_into().unwrap(), OccurMode::Strong);
        }

        let mut tags = Vec::from_iter(
            [
                "Yoda*",
                "Darth Vader*",
                "Luke Skywalker",
                "Albus Dumbledore",
                "Jenny",
                "Forest Gump",
                "Lord Voldemort",
            ]
            .iter()
            .map(|s| TagSpec::from_str(s).unwrap()),
        );

        super::sort_by_relevance(&t, &mut tags);

        assert_eq!(
            tags,
            Vec::from_iter(
                [
                    "Forest Gump",
                    "Luke Skywalker",
                    "Albus Dumbledore",
                    "Jenny",
                    "Lord Voldemort",
                    "Yoda*",
                    "Darth Vader*",
                ]
                .iter()
                .map(|s| TagSpec::from_str(s).unwrap())
            )
        );
    }
}
