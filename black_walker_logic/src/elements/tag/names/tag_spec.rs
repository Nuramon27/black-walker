//! Implementation of `TagSpec`

use std::collections::{HashMap, HashSet};
use std::convert::TryInto;
use std::fmt;
use std::str::FromStr;

use serde::de;
use serde::{Deserialize, Deserializer, Serialize, Serializer};

use super::super::OccurMode;
use super::{TagName, TagStr};

/// An object that specifies a tag of a dream.
///
/// In order to exactly specify a tag, a valid [`base_name`](TagSpecification::base_name)
/// is necessary and a [`strength`](TagSpecification::strength).
pub trait TagSpecification {
    /// Returns the base name of the tag specification.
    fn base_name(&self) -> &TagStr;
    /// Returns the strength of the tag specification.
    fn strength(&self) -> OccurMode;
}

impl TagSpecification for str {
    fn base_name(&self) -> &TagStr {
        let mut s = self;
        while s.ends_with('*') {
            s = &s[..(s.len() - "*".len())];
        }
        s.try_into().unwrap()
    }
    fn strength(&self) -> OccurMode {
        if self.ends_with('*') {
            OccurMode::Weak
        } else {
            OccurMode::Strong
        }
    }
}

impl<'a> TagSpecification for &'a str {
    fn base_name(&self) -> &TagStr {
        (*self).base_name()
    }
    fn strength(&self) -> OccurMode {
        (*self).strength()
    }
}

impl TagSpecification for String {
    fn base_name(&self) -> &TagStr {
        let mut s = self.as_str();
        while s.ends_with('*') {
            s = &s[..(s.len() - "*".len())];
        }
        s.try_into().unwrap()
    }
    fn strength(&self) -> OccurMode {
        if self.ends_with('*') {
            OccurMode::Weak
        } else {
            OccurMode::Strong
        }
    }
}

/// A specification of a Tag, either in strong or weak form.
///
/// It can be constructed from and converted to a `String`. Hereby,
/// the String-representation contains a trailing asterisk '*' for
/// indicating the weak form.
#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct TagSpec {
    /// The base name of the tag, i.e. without an eventual trailing '*'.
    name: TagName,
    /// The strength in which the tag occurred.
    strength: OccurMode,
}

impl fmt::Display for TagSpec {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self.strength {
            OccurMode::Strong => write!(f, "{}", self.name.0),
            OccurMode::Weak => write!(f, "{}*", self.name.0),
        }
    }
}

impl From<String> for TagSpec {
    /// Constructs a `TagSpec` from `other`.
    ///
    /// If `other` does not end in an asterisk, its basename
    /// will be identical to `other` and it will have strong form.
    ///
    /// If `other` ends in one or more asterisks, all trailing asterisks will be stripped
    /// to construct the base name and it will have weak form.
    fn from(mut other: String) -> Self {
        let mut strength = OccurMode::Strong;
        while other.ends_with('*') {
            other.pop();
            strength = OccurMode::Weak;
        }
        Self {
            name: TagName(other),
            strength,
        }
    }
}

impl<'a> From<&'a str> for TagSpec {
    /// Constructs a `TagSpec` from `s`.
    ///
    /// If `s` does not end in an asterisk, its basename
    /// will be identical to `s` and it will have strong form.
    ///
    /// If `s` ends in one or more asterisks, all trailing asterisks will be stripped
    /// to construct the base name and it will have weak form.
    fn from(mut s: &'a str) -> Self {
        let mut strength = OccurMode::Strong;
        while s.ends_with('*') {
            s = &s[..(s.len() - "*".len())];
            strength = OccurMode::Weak;
        }
        Self {
            name: TagName(s.to_string()),
            strength,
        }
    }
}

impl From<TagSpec> for TagName {
    fn from(other: TagSpec) -> Self {
        other.name
    }
}

impl FromStr for TagSpec {
    type Err = std::convert::Infallible;
    /// Constructs a `TagSpec` from `s`.
    ///
    /// If `s` does not end in an asterisk, its basename
    /// will be identical to `s` and it will have strong form.
    ///
    /// If `s` ends in one or more asterisks, all trailing asterisks will be stripped
    /// to construct the base name and it will have weak form.
    fn from_str(mut s: &str) -> Result<Self, std::convert::Infallible> {
        let mut strength = OccurMode::Strong;
        while s.ends_with('*') {
            s = &s[..(s.len() - "*".len())];
            strength = OccurMode::Weak;
        }
        Ok(Self {
            name: TagName(s.to_string()),
            strength,
        })
    }
}

impl TagSpecification for TagSpec {
    /// Returns the base name of the tag specification.
    fn base_name(&self) -> &TagStr {
        &self.name
    }
    /// Returns the strength of the tag specification.
    fn strength(&self) -> OccurMode {
        self.strength
    }
}

impl TagSpecification for &TagSpec {
    /// Returns the base name of the tag specification.
    fn base_name(&self) -> &TagStr {
        &self.name
    }
    /// Returns the strength of the tag specification.
    fn strength(&self) -> OccurMode {
        self.strength
    }
}

impl TagSpec {
    /// Creates a new `TagSpec` with `name` and `strength`.
    pub fn new(name: TagName, strength: OccurMode) -> Self {
        Self { name, strength }
    }
    /// Turns the `TagSpec` into its base_name.
    pub fn into_base_name(self) -> TagName {
        self.name
    }
    /// Sets the base name of the tag specification to `new_name`.
    pub fn set_base_name(&mut self, new_name: TagName) {
        self.name = new_name
    }
    /// Returns a new `TagSpec` with the strength of `old` but
    /// `new_name` as name.
    pub fn with_new_name(old: &TagSpec, new_name: TagName) -> Self {
        TagSpec {
            name: new_name,
            strength: old.strength,
        }
    }
    /// Produces `TagSpecs` with `name` in strong and weak form.
    pub fn in_all_strengths(name: TagName) -> (TagSpec, TagSpec) {
        (
            TagSpec {
                name: name.clone(),
                strength: OccurMode::Strong,
            },
            TagSpec {
                name,
                strength: OccurMode::Weak,
            },
        )
    }
}

impl Serialize for TagSpec {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        match self.strength {
            OccurMode::Strong => serializer.serialize_str(self.name.as_str()),
            OccurMode::Weak => serializer.serialize_str(&format!("{}*", self.name)),
        }
    }
}
struct TagSpecVisitor;
impl<'de> de::Visitor<'de> for TagSpecVisitor {
    type Value = TagSpec;
    fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        write!(formatter, "A String.")
    }

    fn visit_str<E>(self, v: &str) -> Result<Self::Value, E>
    where
        E: de::Error,
    {
        // Won't panic because the error type is !.
        Ok(TagSpec::from(v))
    }
    fn visit_string<E>(self, v: String) -> Result<Self::Value, E>
    where
        E: de::Error,
    {
        Ok(TagSpec::from(v))
    }
}

impl<'de> Deserialize<'de> for TagSpec {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        deserializer.deserialize_str(TagSpecVisitor)
    }
}

/// Extracts the `TagName` from a forest of `TagSpec`s.
///
/// This is simply a conversion of the input type `HashMap<String, HashSet<TagSpec>>`
/// to the output type `HashMap<String, HashSet<TagName>>`.
pub fn tag_spec_forest_to_tag_name_forest(
    tag_spec_forest: HashMap<String, HashSet<TagSpec>>,
) -> HashMap<String, HashSet<TagName>> {
    tag_spec_forest
        .into_iter()
        .map(|(catname, set)| {
            (
                catname,
                set.into_iter().map(|tagspec| tagspec.name).collect(),
            )
        })
        .collect()
}
