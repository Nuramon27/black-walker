//! Implementation of [`TagName`].

use std::borrow::Borrow;
use std::cmp::Ordering;
use std::convert::{TryFrom, TryInto};
use std::fmt;
use std::hash::{Hash, Hasher};
use std::str::FromStr;

use serde::de;
use serde::{Deserialize, Deserializer, Serialize, Serializer};

use super::TagStr;

/// A valid name for a tag.
///
/// This is a newtype wrapper around `String` which ensures
/// that it never ends in a '*'. It can be constructed
/// from a `String` via its `TryFrom` implementation.
///
/// Furthermore it ensures correct locale-aware sorting.
#[derive(Debug, Default, Clone)]
pub struct TagName(pub(super) String);

impl fmt::Display for TagName {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.0)
    }
}

/// The type of error which can occur when a [`TagName`] is created directly
/// from a `String`.
#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub enum TagNameError {
    /// Raised when the `String` ends in an asterisk '*' since this is not allowed for
    /// tag names.
    TrailingAsterisk,
}

impl TryFrom<String> for TagName {
    type Error = TagNameError;
    /// Returns a `TagName` identical to `value` if `value` does
    /// not end in a '*'.
    ///
    /// Otherwise, `Err(TagNameError::TrailingAsterisk)` is returned.
    fn try_from(value: String) -> Result<Self, TagNameError> {
        if value.ends_with('*') {
            Err(TagNameError::TrailingAsterisk)
        } else {
            Ok(TagName(value))
        }
    }
}

impl From<TagName> for String {
    fn from(TagName(other): TagName) -> Self {
        other
    }
}

impl FromStr for TagName {
    type Err = TagNameError;
    /// Returns a `TagName` identical to `s` if `s` does
    /// not end in a '*'.
    ///
    /// Otherwise, `Err(TagNameError::TrailingAsterisk)` is returned.
    fn from_str(s: &str) -> Result<Self, TagNameError> {
        if s.ends_with('*') {
            Err(TagNameError::TrailingAsterisk)
        } else {
            Ok(TagName(s.to_string()))
        }
    }
}

impl std::borrow::Borrow<str> for TagName {
    fn borrow(&self) -> &str {
        self.0.as_str()
    }
}

impl std::borrow::Borrow<TagStr> for TagName {
    fn borrow(&self) -> &TagStr {
        // Won't panic because inner value already is a valid TagName.
        self.0.as_str().try_into().unwrap()
    }
}

impl std::ops::Deref for TagName {
    type Target = TagStr;
    fn deref(&self) -> &Self::Target {
        self.borrow()
    }
}

impl TagName {
    /// Returns the contained `String` as a `str`.
    pub fn as_str(&self) -> &str {
        self.0.as_str()
    }
    /// Returns a `TagName` with the empty String as name.
    ///
    /// Does not allocate.
    pub fn root() -> Self {
        TagName(String::new())
    }
    /// Checks whether the `TagName` is the root tag, i.e. whether
    /// it's name is empty.
    pub fn is_root(&self) -> bool {
        self.0.is_empty()
    }
}

impl PartialEq<TagName> for TagName {
    fn eq(&self, other: &TagName) -> bool {
        self.0.eq(&other.0)
    }
}

impl PartialEq<str> for TagName {
    fn eq(&self, other: &str) -> bool {
        self.0.eq(other)
    }
}

impl PartialEq<TagName> for str {
    fn eq(&self, other: &TagName) -> bool {
        other.eq(self)
    }
}

impl Eq for TagName {}

impl Hash for TagName {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.0.hash(state);
    }
}

impl PartialOrd<TagName> for TagName {
    /// Tries to perform a locale-aware string comparison with `other`.
    ///
    /// If an error occurs in the helper library for string collation,
    /// `rust_icu_ucol`, it falls back to the `Ord` implementation of `String`.
    fn partial_cmp(&self, other: &TagName) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl Ord for TagName {
    /// Tries to perform a locale-aware string comparison with `other`.
    ///
    /// If an error occurs in the helper library for string collation,
    /// `rust_icu_ucol`, it falls back to the `Ord` implementation of `String`.
    fn cmp(&self, other: &Self) -> Ordering {
        super::collation::cmp_coll(&self.0, &other.0)
    }
}

impl Serialize for TagName {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        serializer.serialize_str(&self.0)
    }
}

struct TagNameVisitor;
impl<'de> de::Visitor<'de> for TagNameVisitor {
    type Value = TagName;
    fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        write!(formatter, "A String which does not end in an asterisk.")
    }

    fn visit_str<E>(self, v: &str) -> Result<Self::Value, E>
    where
        E: de::Error,
    {
        TagName::from_str(v).map_err(|_| {
            de::Error::custom("The name of a tag may not end in an asterisk.".to_string())
        })
    }
    fn visit_string<E>(self, v: String) -> Result<Self::Value, E>
    where
        E: de::Error,
    {
        TagName::try_from(v).map_err(|_| {
            de::Error::custom("The name of a tag may not end in an asterisk.".to_string())
        })
    }
}

impl<'de> Deserialize<'de> for TagName {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        deserializer.deserialize_str(TagNameVisitor)
    }
}
