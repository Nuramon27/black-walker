//! Implementation of `TagStr`.

use std::cmp::Ordering;
use std::convert::TryFrom;
use std::fmt;
use std::hash::{Hash, Hasher};
use std::ops::Deref;

use super::{TagName, TagNameError};

/// The borrowed form of a `TagName`.
///
/// `TagStr` relates to `TagName` like `str` to `String`. Indeed, it is only
/// a newtype wrapper around a `str`, with the additional guarantees
/// and methods of a `TagName`.
///
/// A `TagStr` can be created from a `str` via its `TryFrom` implementation,
/// or from a [`TagSpec`](super::tag_spec::TagSpec) via the method
/// [`TagSpecification::base_name`](super::TagSpecification::base_name).
#[derive(Debug)]
#[repr(transparent)]
pub struct TagStr(str);

impl fmt::Display for TagStr {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", &self.0)
    }
}

impl std::borrow::Borrow<str> for TagStr {
    fn borrow(&self) -> &str {
        &self.0
    }
}

impl Deref for TagStr {
    type Target = str;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl<'a> TryFrom<&'a str> for &'a TagStr {
    type Error = TagNameError;
    /// Returns a `TagName` identical to `value` if `value` does
    /// not end in a '*'.
    ///
    /// Otherwise, `Err(TagNameError::TrailingAsterisk)` is returned.
    fn try_from(value: &'a str) -> Result<Self, TagNameError> {
        if value.ends_with('*') {
            Err(TagNameError::TrailingAsterisk)
        } else {
            Ok(unsafe { &*(value as *const str as *const TagStr) })
        }
    }
}

impl<'a> TryFrom<&'a String> for &'a TagStr {
    type Error = TagNameError;
    /// Returns a `TagName` identical to `value` if `value` does
    /// not end in a '*'.
    ///
    /// Otherwise, `Err(TagNameError::TrailingAsterisk)` is returned.
    fn try_from(value: &'a String) -> Result<Self, TagNameError> {
        TryFrom::try_from(value.as_str())
    }
}

impl<'a> From<&'a TagStr> for &'a str {
    fn from(TagStr(other): &'a TagStr) -> Self {
        other
    }
}

impl<'a> From<&'a TagName> for &'a TagStr {
    fn from(other: &'a TagName) -> Self {
        // Won't panic because `other` is already a valid TagName.
        Self::try_from(other.0.as_str()).unwrap()
    }
}

impl ToOwned for TagStr {
    type Owned = TagName;
    fn to_owned(&self) -> Self::Owned {
        TagName(self.0.to_owned())
    }
}

impl PartialEq<TagStr> for TagStr {
    fn eq(&self, other: &TagStr) -> bool {
        self.0.eq(&other.0)
    }
}

impl PartialEq<TagName> for TagStr {
    fn eq(&self, other: &TagName) -> bool {
        self.0.eq(&other.0)
    }
}

impl PartialEq<TagStr> for TagName {
    fn eq(&self, other: &TagStr) -> bool {
        self.0.eq(&other.0)
    }
}

impl PartialEq<str> for TagStr {
    fn eq(&self, other: &str) -> bool {
        self.0.eq(other)
    }
}

impl PartialEq<TagStr> for str {
    fn eq(&self, other: &TagStr) -> bool {
        other.eq(self)
    }
}

impl Eq for TagStr {}

impl Hash for TagStr {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.0.hash(state)
    }
}

impl PartialOrd<TagStr> for TagStr {
    /// Tries to perform a locale-aware string comparison with `other`.
    ///
    /// If an error occurs in the helper library for string collation,
    /// `rust_icu_ucol`, it falls back to the `Ord` implementation of `String`.
    fn partial_cmp(&self, other: &TagStr) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl Ord for TagStr {
    /// Tries to perform a locale-aware string comparison with `other`.
    ///
    /// If an error occurs in the helper library for string collation,
    /// `rust_icu_ucol`, it falls back to the `Ord` implementation of `String`.
    fn cmp(&self, other: &Self) -> Ordering {
        super::collation::cmp_coll(&self.0, &other.0)
    }
}

impl TagStr {
    /// Returns the contained `String` as a `str`.
    pub fn as_str(&self) -> &str {
        &self.0
    }
    /// Checks whether the `TagName` is the root tag, i.e. whether
    /// it's name is empty.
    pub fn is_root(&self) -> bool {
        self.0.is_empty()
    }
}
