//! Contains structs representing only the names of tags.
//!
//! The name of a tag currently needs to fulfill one constraint: It must not
//! end in an asterisk. So tag names should always be represented as a [`TagName`]
//! which ensures this property.
//!
//! When referencing a tag in a [`Dream`](crate::Dream), it can appear
//! in weak and strong form. So a reference to a tag is expressed by
//! the struct [`TagSpec`].

#[cfg(feature = "icu")]
#[doc(hidden)]
pub mod collation_icu;
#[cfg(feature = "icu")]
pub use collation_icu as collation;
#[cfg(not(feature = "icu"))]
#[doc(hidden)]
pub mod collation_none;
#[cfg(not(feature = "icu"))]
pub use collation_none as collation;

pub use tag_name::*;
pub use tag_spec::*;
pub use tag_str::*;

pub mod tag_name;
pub mod tag_spec;
pub mod tag_str;



#[cfg(test)]
mod test {
    use std::str::FromStr;

    use super::TagName;

    #[cfg(feature = "icu")]
    #[test]
    fn compare_tagnames() {
        super::collation::init_collator();
        let a = TagName::from_str("Ab").unwrap();
        let ba = TagName::from_str("Äa").unwrap();
        let bb = TagName::from_str("Äb").unwrap();
        let b = TagName::from_str("Äc").unwrap();
        let c = TagName::from_str("cz").unwrap();
        let d = TagName::from_str("Da").unwrap();

        assert!(a < b);
        assert!(a > ba);
        assert!(ba < b);
        assert!(a < bb);
        assert!(ba < bb);
        assert!(bb < b);
        assert!(a < c);
        assert!(a < d);
        assert!(b < c);
        assert!(b < d);
        assert!(c < d);
    }
}
