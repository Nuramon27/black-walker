use std::cmp::Ordering;

pub fn init_collator() {}

/// Does a simple comparison based on the `Ord` implementation
/// of `str`.
pub fn cmp_coll(lhs: &str, rhs: &str) -> Ordering {
    lhs.cmp(rhs)
}