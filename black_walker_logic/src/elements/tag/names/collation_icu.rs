use std::cmp::{Ord, Ordering};
use std::convert::TryFrom;
use std::sync::Once;

use rust_icu_ucol::UCollator;

/// The collator that is used for comparisons of tag-related
/// strings throughout the application.
///
/// # Safety
///
/// __Do not mutate it!__ Simply don't, ensure that [`init_collator`] is called
/// at least once at startup and then only use this collator for comparisons.
static mut COLLATOR: Option<UCollator> = None;

/// A Guard which ensures that [`COLLATOR`] is initialized exactly once.
static COLLATOR_INIT: Once = Once::new();

/// Initializes the `UCollator` which is used for String comparisons
/// by [`TagName`], [`TagStr`] and the function [`cmp_coll].
/// This function must be called (at least) once
/// at startup before one of these items is used.
pub fn init_collator() {
    #[allow(unused_unsafe)]
    COLLATOR_INIT.call_once(|| unsafe {
        COLLATOR = Some(UCollator::try_from("de_DE").unwrap());
    });
}

pub fn cmp_coll(lhs: &str, rhs: &str) -> Ordering {
    // Safe because COLLATOR is only mutated in init_collator, which
    // is guarded by `Once`.
    unsafe {
        COLLATOR
            .as_ref()
            .unwrap()
            .strcoll_utf8(&lhs, &rhs)
            .unwrap_or_else(|_| lhs.cmp(&rhs))
            .then_with(|| lhs.cmp(&rhs))
    }
}

#[cfg(test)]
mod tests {
    use std::cmp::Ordering;

    use std::convert::TryFrom;

    use rust_icu_ucol::UCollator;

    #[test]
    fn collator() {
        let col = UCollator::try_from("de_DE").expect("Collator could not be created.");

        assert_eq!(Ord::cmp("bz", "äb"), Ordering::Less);
        assert_eq!(col.strcoll_utf8("bz", "äb").unwrap(), Ordering::Greater)
    }
}