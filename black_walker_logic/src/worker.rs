//! The worker module contains the core part of the logic side of Black Walker
//!
//! There is the [`Diary`](crate::Diary) struct which is the central piece of data
//! in Black Walker, and the function [`work`] in which the worker
//! thread resides as long as it is idle.
//!
//! In this state, it processes [`Operation`]s and returns notifications
//! and results back to the GUI thread.

use std::sync::mpsc::{Receiver, Sender};
use std::sync::{Arc, RwLock};

use crate::model::{Emitter, Model, NotifyEnd, Operation};
use crate::{CmdError, DiaryRecord};

/// The basic function in which the worker thread will hang while waiting for
/// [commands](crate::model::Narrator) and [queries](crate::model::Query).
pub fn work<M: Model + 'static>(
    history: Arc<RwLock<DiaryRecord<M>>>,
    rec: Receiver<Operation<M>>,
    model_notifier: Sender<Box<dyn NotifyEnd<M>>>,
    mut wakeup_emitter: Box<dyn Emitter>,
) {
    let mut diary = RwLock::read(&history).unwrap().as_receiver().clone();
    loop {
        match rec.recv() {
            Ok(op) => {
                match op {
                    Operation::Narr(cmd) => {
                        // Send the command back to the GUI thread, which
                        // will call notify_end on it.
                        // Keep in mind that cmd is the "unapplied" version of the command,
                        // that maybe does not know what exactly happened when applying it.
                        match RwLock::write(&history)
                            .expect("GUI thread has hung up")
                            .apply(cmd)
                        {
                            Ok(res) => model_notifier.send(res).expect("GUI thread has hung up!"),
                            Err(CmdError::Uncritical(..)) => (),
                            Err(e) => panic!("{:?}", e),
                        };
                        wakeup_emitter.emit();
                    }
                    Operation::Query(mut alarm, mut qu) => {
                        // Execute the query and send its result down the corresponding
                        // channel
                        qu.ask(&mut diary);
                        // This wakes um the GUI thread in order to wait for the
                        // result of the query.
                        alarm.emit();
                    }
                    Operation::Finish => return,
                }
            }
            Err(_) => panic!("GUI thread has hung up!"),
        };
    }
}
