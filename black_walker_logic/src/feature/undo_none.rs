use std::fmt;
use std::error::Error;
use std::marker::PhantomData;
use std::ops::{Deref, DerefMut};

pub trait Command<R, E = Box<dyn Error + Send + Sync + 'static>, T = ()>: fmt::Debug + Send + Sync {
    fn apply(&mut self, receiver: &mut R) -> Result<T, E>;

    fn undo(&mut self, receiver: &mut R) -> Result<T, E>;

    fn redo(&mut self, receiver: &mut R) -> Result<T, E> {
        self.apply(receiver)
    }

    fn is_dead(&self) -> bool {
        false
    }
}

impl<S: Command<R, E, T> + ?Sized, R, E, T> Command<R, E, T> for Box<S> {
    fn apply(&mut self, receiver: &mut R) -> Result<T, E> {
        self.deref_mut().apply(receiver)
    }

    fn undo(&mut self, receiver: &mut R) -> Result<T, E> {
        self.deref_mut().undo(receiver)
    }

    fn redo(&mut self, receiver: &mut R) -> Result<T, E> {
        self.deref_mut().redo(receiver)
    }

    fn is_dead(&self) -> bool {
        self.deref().is_dead()
    }
}

#[derive(Debug)]
pub struct Record<R, E = Box<dyn Error + Send + Sync + 'static>, T = ()> {
    inner: R,
    _phantom: PhantomData<(E, T)>
}

impl<R: 'static, E: 'static, T: 'static> Record<R, E, T> {
    pub fn new(inner: R) -> Self {
        Record {
            inner,
            _phantom: PhantomData::default()
        }
    }
    pub fn apply(&mut self, mut command: impl Command<R, E, T> + 'static) -> Result<T, E> {
        command.apply(&mut self.inner)
    }
    pub fn as_receiver(&self) -> &R {
        &self.inner
    }
    pub fn as_mut_receiver(&mut self) -> &mut R {
        &mut self.inner
    }
}

pub type History<R, E = Box<dyn Error + Send + Sync + 'static>, T = ()> = Record<R, E, T>;