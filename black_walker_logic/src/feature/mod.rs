#[cfg(feature = "crossbeam")]
pub(crate) mod crossbeam;

#[cfg(not(feature = "crossbeam"))]
#[doc(hidden)]
pub(crate) mod crossbeam_none;
#[cfg(not(feature = "crossbeam"))]
pub(crate) use crossbeam_none as crossbeam;


#[cfg(feature = "undo")]
pub mod undo;

#[cfg(not(feature = "undo"))]
#[doc(hidden)]
pub mod undo_none;
#[cfg(not(feature = "undo"))]
pub use undo_none as undo;