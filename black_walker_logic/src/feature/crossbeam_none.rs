pub mod atomic {
    use std::sync::{RwLock, Arc};

    #[derive(Debug, Clone)]
    pub struct AtomicCell<T: ?Sized> {
        val: Arc<RwLock<T>>
    }

    impl<T> AtomicCell<T> {
        pub fn new(val: T) -> Self {
            Self {
                val: Arc::new(RwLock::new(val))
            }
        }
    }

    impl<T: Copy> AtomicCell<T> {
        pub fn load(&self) -> T {
            *self.val.read().unwrap()
        }
        pub fn store(&self, val: T) {
            *self.val.write().unwrap() = val;
        }
    }
}