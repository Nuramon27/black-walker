//! Synchronization structs and implementations for the dream diary.

use std::sync::{LockResult, RwLockReadGuard, RwLockWriteGuard};

use crate::{Diary, DreamList, EntrySystem, NightList, TagForest, GS};

use super::{
    Acquire, AcquireSingle, FromPreStageElem, GeneralOrder, PartAsValue, PreStageElem, Stage,
    StageElem,
};

/// A simple enum for representing a part of the [`Diary`](crate::Diary).
#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub enum DiaryPart {
    TAG,
    DL,
    NL,
    ENTR,
    SET,
}

/// This determines the order in which parts of the diard may be locked.
const GENERAL_ORDER: [DiaryPart; 5] = [
    DiaryPart::TAG,
    DiaryPart::DL,
    DiaryPart::NL,
    DiaryPart::ENTR,
    DiaryPart::SET,
];

impl GeneralOrder for DiaryPart {
    fn general_order() -> &'static [DiaryPart] {
        &GENERAL_ORDER[..]
    }
}

impl PartAsValue<DiaryPart> for TagForest {
    fn part_as_value() -> DiaryPart {
        DiaryPart::TAG
    }
}

impl PartAsValue<DiaryPart> for DreamList {
    fn part_as_value() -> DiaryPart {
        DiaryPart::DL
    }
}

impl PartAsValue<DiaryPart> for NightList {
    fn part_as_value() -> DiaryPart {
        DiaryPart::NL
    }
}

impl PartAsValue<DiaryPart> for EntrySystem {
    fn part_as_value() -> DiaryPart {
        DiaryPart::ENTR
    }
}

impl PartAsValue<DiaryPart> for GS {
    fn part_as_value() -> DiaryPart {
        DiaryPart::SET
    }
}

#[derive(Debug)]
pub struct PreDiaryStage<'a> {
    pub tag: PreStageElem<'a, TagForest>,
    pub dl: PreStageElem<'a, DreamList>,
    pub nl: PreStageElem<'a, NightList>,
    pub entr: PreStageElem<'a, EntrySystem>,
    pub gl: PreStageElem<'a, GS>,
}

impl<'a> PreDiaryStage<'a> {
    pub fn new(diary: &'a mut Diary) -> PreDiaryStage<'a> {
        PreDiaryStage {
            tag: PreStageElem::Pre(&mut diary.tag),
            dl: PreStageElem::Pre(&mut diary.dl),
            nl: PreStageElem::Pre(&mut diary.nl),
            entr: PreStageElem::Pre(&mut diary.entr),
            gl: PreStageElem::Pre(&mut diary.gl),
        }
    }
    pub fn stage<'b>(&'b mut self, part: DiaryPart, write: bool)
    where
        'a: 'b,
    {
        match part {
            DiaryPart::TAG => self.tag.stage_single(write),
            DiaryPart::DL => self.dl.stage_single(write),
            DiaryPart::NL => self.nl.stage_single(write),
            DiaryPart::ENTR => self.entr.stage_single(write),
            DiaryPart::SET => self.gl.stage_single(write),
        }
    }
}

#[derive(Debug)]
pub struct DiaryStage<'a> {
    tag: StageElem<'a, TagForest>,
    dl: StageElem<'a, DreamList>,
    nl: StageElem<'a, NightList>,
    entr: StageElem<'a, EntrySystem>,
    gl: StageElem<'a, GS>,
}

impl<'a> Stage for DiaryStage<'a> {
    type PartSpec = DiaryPart;
    fn lock<'b>(&'b mut self, part: DiaryPart) -> Result<(), ()>
    where
        Self: 'b,
    {
        match part {
            DiaryPart::TAG => self.tag.lock(),
            DiaryPart::DL => self.dl.lock(),
            DiaryPart::NL => self.nl.lock(),
            DiaryPart::ENTR => self.entr.lock(),
            DiaryPart::SET => self.gl.lock(),
        }
    }

    fn lock_predecessors<'b, P: PartAsValue<Self::PartSpec>>(&'b mut self) -> Result<(), ()>
    where
        Self: 'b,
    {
        let mut err = Vec::new();
        for it in P::predecessors() {
            if let Err(e) = self.lock(*it) {
                err.push(e);
            }
        }
        if err.is_empty() {
            Ok(())
        } else {
            Err(())
        }
    }
}

impl<'a> From<PreDiaryStage<'a>> for DiaryStage<'a> {
    fn from(other: PreDiaryStage<'a>) -> Self {
        DiaryStage {
            tag: StageElem::from_pre(other.tag),
            dl: StageElem::from_pre(other.dl),
            nl: StageElem::from_pre(other.nl),
            entr: StageElem::from_pre(other.entr),
            gl: StageElem::from_pre(other.gl),
        }
    }
}

impl<'a> Acquire<'a, TagForest> for DiaryStage<'a> {
    fn read<'b>(&'b mut self) -> LockResult<RwLockReadGuard<'a, TagForest>>
    where
        'a: 'b,
    {
        //DANGER OF PANIC!!!
        self.lock_predecessors::<TagForest>().unwrap();
        self.tag.read()
    }
    fn write<'b>(&'b mut self) -> LockResult<RwLockWriteGuard<'a, TagForest>>
    where
        'a: 'b,
    {
        //DANGER OF PANIC!!!
        self.lock_predecessors::<TagForest>().unwrap();
        self.tag.write()
    }
}

impl<'a> Acquire<'a, DreamList> for DiaryStage<'a> {
    fn read<'b>(&'b mut self) -> LockResult<RwLockReadGuard<'a, DreamList>>
    where
        'a: 'b,
    {
        //DANGER OF PANIC!!!
        self.lock_predecessors::<DreamList>().unwrap();
        self.dl.read()
    }
    fn write<'b>(&'b mut self) -> LockResult<RwLockWriteGuard<'a, DreamList>>
    where
        'a: 'b,
    {
        //DANGER OF PANIC!!!
        self.lock_predecessors::<DreamList>().unwrap();
        self.dl.write()
    }
}

impl<'a> Acquire<'a, NightList> for DiaryStage<'a> {
    fn read<'b>(&'b mut self) -> LockResult<RwLockReadGuard<'a, NightList>>
    where
        'a: 'b,
    {
        //DANGER OF PANIC!!!
        self.lock_predecessors::<NightList>().unwrap();
        self.nl.read()
    }
    fn write<'b>(&'b mut self) -> LockResult<RwLockWriteGuard<'a, NightList>>
    where
        'a: 'b,
    {
        //DANGER OF PANIC!!!
        self.lock_predecessors::<NightList>().unwrap();
        self.nl.write()
    }
}

impl<'a> Acquire<'a, EntrySystem> for DiaryStage<'a> {
    fn read<'b>(&'b mut self) -> LockResult<RwLockReadGuard<'a, EntrySystem>>
    where
        'a: 'b,
    {
        //DANGER OF PANIC!!!
        self.lock_predecessors::<EntrySystem>().unwrap();
        self.entr.read()
    }
    fn write<'b>(&'b mut self) -> LockResult<RwLockWriteGuard<'a, EntrySystem>>
    where
        'a: 'b,
    {
        //DANGER OF PANIC!!!
        self.lock_predecessors::<EntrySystem>().unwrap();
        self.entr.write()
    }
}

impl<'a> Acquire<'a, GS> for DiaryStage<'a> {
    fn read<'b>(&'b mut self) -> LockResult<RwLockReadGuard<'a, GS>>
    where
        'a: 'b,
    {
        //DANGER OF PANIC!!!
        self.lock_predecessors::<GS>().unwrap();
        self.gl.read()
    }
    fn write<'b>(&'b mut self) -> LockResult<RwLockWriteGuard<'a, GS>>
    where
        'a: 'b,
    {
        //DANGER OF PANIC!!!
        self.lock_predecessors::<GS>().unwrap();
        self.gl.write()
    }
}

impl AcquireSingle<TagForest> for Diary {
    fn read<'a>(&'a mut self) -> LockResult<RwLockReadGuard<'a, TagForest>> {
        self.tag.read()
    }
    fn write<'a>(&'a mut self) -> LockResult<RwLockWriteGuard<'a, TagForest>> {
        self.tag.write()
    }
}

impl AcquireSingle<DreamList> for Diary {
    fn read<'a>(&'a mut self) -> LockResult<RwLockReadGuard<'a, DreamList>> {
        self.dl.read()
    }
    fn write<'a>(&'a mut self) -> LockResult<RwLockWriteGuard<'a, DreamList>> {
        self.dl.write()
    }
}

impl AcquireSingle<NightList> for Diary {
    fn read<'a>(&'a mut self) -> LockResult<RwLockReadGuard<'a, NightList>> {
        self.nl.read()
    }
    fn write<'a>(&'a mut self) -> LockResult<RwLockWriteGuard<'a, NightList>> {
        self.nl.write()
    }
}

impl AcquireSingle<EntrySystem> for Diary {
    fn read<'a>(&'a mut self) -> LockResult<RwLockReadGuard<'a, EntrySystem>> {
        self.entr.read()
    }
    fn write<'a>(&'a mut self) -> LockResult<RwLockWriteGuard<'a, EntrySystem>> {
        self.entr.write()
    }
}

impl AcquireSingle<GS> for Diary {
    fn read<'a>(&'a mut self) -> LockResult<RwLockReadGuard<'a, GS>> {
        self.gl.read()
    }
    fn write<'a>(&'a mut self) -> LockResult<RwLockWriteGuard<'a, GS>> {
        self.gl.write()
    }
}

#[macro_export]
macro_rules! stage_diary {
    ($diary:expr; $( $fieldread:expr ),* ) => {
        $crate::stage!($diary, $crate::sync::diary::PreDiaryStage, $crate::sync::diary::DiaryStage; $( $fieldread),* )
    };
    ($diary:expr; $( $fieldread:expr ),* ; $( $fieldwrite:expr ),* ) => {
        $crate::stage!($diary, $crate::sync::diary::PreDiaryStage, $crate::sync::diary::DiaryStage; $( $fieldread),*; $( $fieldwrite ),* )
    };
}

#[cfg(test)]
mod tests {
    use crate::{lock, read_single, read_unstaged, stage_diary, write_single, write_unstaged};
    use crate::{Diary, DreamList};

    use super::DiaryPart::{DL, ENTR, NL, SET, TAG};
    use super::{EntrySystem, NightList, TagForest, GS};
    #[test]
    fn stage_test_read_only() {
        let mut diary = Diary::new();
        let diary2 = diary.clone();
        let mut stage = stage_diary!(diary; TAG, DL, ENTR);
        let tagforest = read_single!(stage, TagForest);
        assert!(tagforest.is_ok());

        assert!(diary2.tag.try_write().is_err());
        assert!(diary2.dl.try_write().is_ok());

        let dreamlist = read_single!(stage, EntrySystem);
        assert!(dreamlist.is_ok());
        assert!(diary2.dl.try_write().is_err());
        assert!(read_single!(stage, DreamList).is_ok());

        assert!(diary2.gl.try_write().is_ok());
    }

    #[test]
    fn stage_test_read_write() {
        let mut diary = Diary::new();
        let diary2 = diary.clone();
        let mut stage = stage_diary!(diary; TAG; DL, ENTR);
        let entrysystem = write_single!(stage, EntrySystem);
        assert!(entrysystem.is_ok());

        assert!(diary2.dl.try_read().is_err());
        assert!(diary2.tag.try_read().is_ok());
        assert!(diary2.tag.try_write().is_err());
    }

    #[test]
    fn read_multi() {
        let mut diary = Diary::new();
        let mut stage = stage_diary!(diary; TAG; DL, ENTR);
        if let (Ok(tagforest), Ok(mut entrysystem)) = lock!(stage; TagForest; EntrySystem) {
            assert!(tagforest.contains_key("Personen"));
            entrysystem.add_text("A", "B".to_string());
        } else {
            panic!("Locks could not be obtained");
        };
    }

    #[test]
    fn read_unstaged() {
        let mut diary = Diary::new();
        if let Ok(tagforest) = read_unstaged!(diary, TagForest) {
            assert!(tagforest.contains_key("Personen"));
        };
    }

    #[test]
    fn write_unstaged() {
        let mut diary = Diary::new();
        if let Ok(mut entrysystem) = write_unstaged!(diary, EntrySystem) {
            entrysystem.add_text("A", "B".to_string());
        } else {
            panic!("Lock could not be obtained.");
        };
    }

    #[test]
    #[should_panic]
    fn stage_wrong_read() {
        let mut diary = Diary::new();
        let mut stage = stage_diary!(diary; TAG, DL, ENTR);
        let _tagforest = read_single!(stage, crate::GS);
    }

    #[test]
    fn read_nightlist() {
        let mut diary = Diary::new();
        let mut stage = stage_diary!(diary; DL, NL;);
        let _nightlist = lock!(stage; DreamList, NightList);
    }

    #[test]
    fn read_all_out_of_order() {
        let mut diary = Diary::new();
        let mut stage = stage_diary!(diary; ENTR, DL, NL, TAG, SET;);
        let locks = lock!(stage; TagForest, DreamList, NightList, EntrySystem, GS);
        let _nightlist = locks.1.unwrap();
    }
}
