//! The `sync` module contains macros that enable dead-lock-free locking of
//! `RwLock`s if used according to some simple rules.
//!
//! It assumes that all `RwLock`s to be synchronized are collected
//! in one single struct, hereafter called the “source”.
//!
//! The process of obtaining a `ReadGuard` or `WriteGuard` (in the following
//! simply called “guard”) from the source consists of two phases:
//! At first a [`Stage`]
//! must be created that declares which part of the source shall be used.
//! The important rule here is thate at every moment there is only one `Stage`
//! in each thread.
//!
//! From this stage, items of the source can be locked for read or for write
//! by using the corresponding methods of the [`Acquire`]
//! trait. When these methods are used, the stage itself ensures that
//! the locks are synchronized in a way that prevents dead-locks.
//!
//! ## How to obtain `Stage`s and guards
//!
//! The way to create a `Stage` from the source is via the [`stage`](crate::stage) macro.
//! The returned `Stage` then borrows the source mutably
//! Be aware that the source usually consists `Arc<RwLock>`s, so it will be
//! possible to obtain a complete shallow copy of the source, which could again
//! be staged although the old one is still mutably borrowed -- if this happens
//! in the same thread, one can introduce a dead-lock. So the general rule of thump
//! is: If you create a shallow copy of the source, directly move it into another thread.
//!
//! From a `Stage`, there are two possibilities to obtain guards: [`read_single`](crate::read_single)
//! and [`write_single`](crate::write_single) for obtaining a single guard and [`lock`](crate::lock)
//! for obtaining a tuple of guards.

pub mod diary;
pub mod prelude;

use std::sync::{Arc, LockResult, RwLock, RwLockReadGuard, RwLockWriteGuard};

/// A type whose values have a total ordering which is hardcoded
/// via a constant array.
pub trait GeneralOrder: Copy + Sized + Eq + 'static {
    /// Returns a slice that represents a total ordering of all possible values
    /// of `Self`.
    ///
    /// ## Attention
    ///
    /// The slice returned by this function must contain all possible values of `Self`,
    /// otherwise, functions relying on this trait may panic. This means that usually
    /// `Self` will be an enum.
    fn general_order() -> &'static [Self];
}

/// This trait provides a mapping between the type of an item of the source
/// and a value of type `P` that represents this item.
pub trait PartAsValue<P: GeneralOrder> {
    /// Returns the part associated to `Self`.
    ///
    /// There should be a one-to-one mapping between the types that implement `PartAsValue`
    /// and the values returned by this function.
    fn part_as_value() -> P;

    /// Returns all elements that precede [`part_as_value()`](PartAsValue::part_as_value) in
    /// [`general_order()`](GeneralOrder::general_order), excluding the item itself.
    ///
    /// The default implementation must not be changed.
    ///
    /// ## Panics
    ///
    /// If `Self::part_as_value()` cannot be found in `general_order`.
    /// This can only happen if `Self::general_order()` does not contain all possible values
    /// of `P` which should not be the case.
    fn predecessors() -> &'static [P] {
        let i = P::general_order()
            .iter()
            .enumerate()
            .find(|(_, &part)| part == Self::part_as_value())
            .map(|(i, _)| i)
            .expect("General order does not contain all possible parts!");
        &P::general_order()[..i]
    }
}

/// One element of a [`Stage`].
///
/// You can obtain guards to the element via methods [`read`](StageElem::read)
/// and [`write`](StageElem::write), but be aware that an element staged for
/// writing can only be acquired for writing (i.e. `read` will panic) and vice
/// versa and -- most importantly -- a guard to an element can only
/// be obtained once! You have to take care of this guard so it won't get lost.
#[derive(Debug)]
pub enum StageElem<'a, T> {
    /// The element is not staged for locking, i.e. is unavialable.
    ///
    /// This can be because it has never been staged or because
    /// the corresponding guard has already been obtained
    /// and was moved out of the `StageElem`.
    Never,
    /// The element is staged for reading, but currently unlocked.
    AvailRead(&'a mut Arc<RwLock<T>>),
    /// The element is staged for writing, but currently unlocked.
    AvailWrite(&'a mut Arc<RwLock<T>>),
    /// The element is locked for reading but the corresponding
    /// guard has not been obtained.
    ///
    /// This usually means the element was locked because it is a predecessor
    /// of another staged element whose guard has been demanded.
    LockedRead(RwLockReadGuard<'a, T>),
    /// The element is locked for writing but the corresponding
    /// guard has not been obtained.
    ///
    /// This usually means the element was locked because it is a predecessor
    /// of another staged element whose guard has been demanded.
    LockedWrite(RwLockWriteGuard<'a, T>),
}

use StageElem::{AvailRead, AvailWrite, LockedRead, LockedWrite, Never};

impl<'a, T: std::fmt::Debug> StageElem<'a, T> {
    /// Lock the element for reading, returning a guard.
    ///
    /// As a `StageElem` can only be locked once, after calling this
    /// function `Self` will have the value [`Never`](StageElem::Never).
    ///
    /// ## Panics
    ///
    /// If the element has never been staged; if the element
    /// has been staged for write; or if the guard
    /// corresponding to the element has already been obtained.
    pub fn read(&mut self) -> LockResult<RwLockReadGuard<'a, T>> {
        let se = std::mem::replace(self, Never);
        match se {
            Never => panic!("Element has not been staged for lock"),
            AvailRead(a) => a.read(),
            AvailWrite(_) => {
                panic!("Element has been staged for write, now attempt to lock for read")
            }
            LockedRead(lock) => Ok(lock),
            LockedWrite(_) => {
                panic!("Element has been locked for write, now attempt to lock for read")
            }
        }
    }

    /// Lock the element for writing, returning a guard.
    ///
    /// As a `StageElem` can only be locked once, after calling this
    /// function `Self` will have the value [`Never`](StageElem::Never).
    ///
    /// ## Panics
    ///
    /// If the element has never been staged; if the element
    /// has been staged for read; or if the guard
    /// corresponding to the element has already been obtained.
    pub fn write(&mut self) -> LockResult<RwLockWriteGuard<'a, T>> {
        let se = std::mem::replace(self, Never);
        match se {
            Never => panic!("Element has not been staged for lock"),
            AvailRead(_) => {
                panic!("Element has been staged for read, now attempt to lock for write")
            }
            AvailWrite(a) => a.write(),
            LockedRead(_) => {
                panic!("Element has been locked for read, now attempt to lock for write")
            }
            LockedWrite(lock) => Ok(lock),
        }
    }

    /// Locks the element, keeping the corresponding guard stored.
    ///
    /// After calling this function, `Self` will be one of
    /// [`AvailRead`](StageElem::AvailRead) or [`AvailWrite`](StageElem::AvailWrite).
    pub fn lock(&mut self) -> Result<(), ()> {
        let se = std::mem::replace(self, Never);
        match se {
            Never => (),
            AvailRead(a) => *self = LockedRead(a.read().map_err(|_| ())?),
            AvailWrite(a) => *self = LockedWrite(a.write().map_err(|_| ())?),
            a @ LockedRead(_) => *self = a,
            a @ LockedWrite(_) => *self = a,
        };
        Ok(())
    }
}

#[derive(Debug)]
pub enum PreStageElem<'a, T> {
    Empty,
    Pre(&'a mut Arc<RwLock<T>>),
    StageRead(&'a mut Arc<RwLock<T>>),
    StageWrite(&'a mut Arc<RwLock<T>>),
}

impl<'a, T> PreStageElem<'a, T> {
    pub fn stage_single<'b>(&'b mut self, write: bool)
    where
        'a: 'b,
    {
        let se = std::mem::replace(self, PreStageElem::Empty);
        match se {
            PreStageElem::Empty => panic!("PreStageElem should never be empty!"),
            PreStageElem::Pre(content)
            | PreStageElem::StageRead(content)
            | PreStageElem::StageWrite(content) => {
                *self = if write {
                    PreStageElem::StageWrite(content)
                } else {
                    PreStageElem::StageRead(content)
                }
            }
        }
    }
}

pub trait FromPreStageElem<'a, T> {
    fn from_pre(other: PreStageElem<'a, T>) -> Self;
}

impl<'a, T> FromPreStageElem<'a, T> for StageElem<'a, T> {
    fn from_pre(other: PreStageElem<'a, T>) -> Self {
        match other {
            PreStageElem::Empty => Never,
            PreStageElem::Pre(_) => Never,
            PreStageElem::StageRead(a) => AvailRead(a),
            PreStageElem::StageWrite(a) => AvailWrite(a),
        }
    }
}

/// A collection of items of the source “staged” for locking.
pub trait Stage {
    /// The type with which one can specify which staged item to lock.
    ///
    /// Usually this will be a small enum.
    type PartSpec: GeneralOrder;

    /// Locks `part` of the `RwLock`.
    fn lock<'b>(&'b mut self, part: Self::PartSpec) -> Result<(), ()>
    where
        Self: 'b;

    /// Locks all predecessors of [`Self::part_as_value()`](PartAsValue::part_as_value)
    /// according to the ordering represented by [`GeneralOrder`], excluding
    /// the item itself.
    fn lock_predecessors<'b, P: PartAsValue<Self::PartSpec>>(&'b mut self) -> Result<(), ()>
    where
        Self: 'b;
}

/// The methods to aquire a guard to the item `T` of the source.
///
/// Usually you will call these methods via the macros [`read_single`](crate::read_single) and
/// [`write_single`](crate::write_single) so you can write the type argument `T` like a normal argument.
pub trait Acquire<'a, T> {
    /// Locks the item `T` of the source and all its predecessors for reading,
    /// returng the guard to the item `T`.
    fn read<'b>(&'b mut self) -> LockResult<RwLockReadGuard<'a, T>>
    where
        'a: 'b;
    /// Locks the item `T` of the source and all its predecessors for writing,
    /// returng the guard to the item `T`.
    fn write<'b>(&'b mut self) -> LockResult<RwLockWriteGuard<'a, T>>
    where
        'a: 'b;
}

pub trait AcquireSingle<T> {
    fn read<'a>(&'a mut self) -> LockResult<RwLockReadGuard<'a, T>>;
    fn write<'a>(&'a mut self) -> LockResult<RwLockWriteGuard<'a, T>>;
}

impl<T: AcquireSingle<P>, P> AcquireSingle<P> for &mut T {
    fn read<'a>(&'a mut self) -> LockResult<RwLockReadGuard<'a, P>> {
        (*self).read()
    }
    fn write<'a>(&'a mut self) -> LockResult<RwLockWriteGuard<'a, P>> {
        (*self).write()
    }
}

/// Stages the selected items of the source for read respectively for write.
///
/// This borrows the source mutably, so as long as there is only one instance of the resource
/// per thread,
/// there can only be one stage per thread.
///
/// Usage: `stage!(source, items_to_stage_for_read, ..; items_to_stage_for_write, ..)`
#[macro_export]
macro_rules! stage {
    ($diary:expr, $PreStage:ty, $Stage:ty; $( $fieldread:expr ),* ) => {
        {
            let mut prestage = <$PreStage>::new(std::borrow::BorrowMut::borrow_mut(&mut $diary));
            $(
                prestage.stage($fieldread, false);
            )*
            <$Stage>::from(prestage)
        }
    };
    ($diary:expr, $PreStage:ty, $Stage:ty; $( $fieldread:expr ),* ; $( $fieldwrite:expr ),* ) => {
        {
            let mut prestage = <$PreStage>::new(std::borrow::BorrowMut::borrow_mut(&mut $diary));
            $(
                prestage.stage($fieldread, false);
            )*
            $(
                prestage.stage($fieldwrite, true);
            )*
            <$Stage>::from(prestage)
        }
    };
}

#[macro_export]
/// Locks one element of the stage and all its predecessors for reading,
/// returning the corresponding guard.
///
/// Usage: `read_single!(stage, element)`
macro_rules! read_single {
    ( $diarystage:expr, $field:ty ) => {
        $crate::sync::Acquire::<$field>::read(&mut $diarystage)
    };
}

/// Locks one element of the stage and all its predecessors for writing,
/// returning the corresponding guard.
///
/// Usage: `write_single!(stage, element)`
#[macro_export]
macro_rules! write_single {
    ( $diarystage:expr, $field:ty ) => {
        $crate::sync::Acquire::<$field>::write(&mut $diarystage)
    };
}

/// Obtains a read guard to an item of the source without staging first.
///
/// This borrows the source mutably, so no further guards can be obtained.
///
/// ## Examples
///
/// This fails because it is an attempt to borrow `diary` mutably twice.
/// ```compile_fail
/// use black_walker_logic::read_unstaged;
/// use black_walker_logic::{TagForest, DreamList, Diary};
///
/// let mut diary = Diary::new();
/// let firstlock = read_unstaged!(diary, TagForest);
/// let secondlock = read_unstaged!(diary, DreamList);
///
/// if let Ok(tagforest) = firstlock {
///     assert!(tagforest.is_empty());
/// }
/// ```
#[macro_export]
macro_rules! read_unstaged {
    ( $diarystage:expr, $field:ty ) => {
        $crate::sync::AcquireSingle::<$field>::read(&mut $diarystage)
    };
}

/// Obtains a write guard to an item of the source without staging first.
///
/// This borrows the source mutably, so no further guards can be obtained.
///
/// ## Examples
///
/// This fails because it is an attempt to borrow `diary` mutably twice.
/// ```compile_fail
/// use black_walker_logic::write_unstaged;
/// use black_walker_logic::{TagForest, DreamList, Diary};
///
/// let mut diary = Diary::new();
/// let firstlock = write_unstaged!(diary, TagForest);
/// let secondlock = write_unstaged!(diary, DreamList);
///
/// if let Ok(tagforest) = firstlock {
///     assert!(tagforest.is_empty());
/// }
/// ```
#[macro_export]
macro_rules! write_unstaged {
    ( $diarystage:expr, $field:ty ) => {
        $crate::sync::AcquireSingle::<$field>::write(&mut $diarystage)
    };
}

/// Locks the selected elements and all their predecessors for reading
/// respectively for writing, returning guards to the elements
/// as one tuple of `LockResult`s.
///
/// Usage: `lock!(stage, elements_to_lock_for_read; elements_to_lock_for_write)`
#[macro_export]
macro_rules! lock {
    ( $diarystage:expr; $( $fieldread:ty ),*) => {
        {
            (
                $(
                    $crate::read_single!($diarystage, $fieldread ),
                )*
            )
        }
    };
    ( $diarystage:expr; $( $fieldread:ty ),* ; $( $fieldwrite:ty ),* ) => {
        {
            (
                $(
                    $crate::read_single!($diarystage, $fieldread ),
                )*
                $(
                    $crate::write_single!($diarystage, $fieldwrite ),
                )*
            )
        }
    };
}
