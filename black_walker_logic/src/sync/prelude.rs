pub use super::diary::{
    DiaryPart,
    DiaryPart::{DL, ENTR, NL, SET, TAG},
};
