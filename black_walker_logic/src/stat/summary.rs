use std::collections::HashMap;
use std::iter::FromIterator;

use linalg_bw::NumericError;
use ndarray::Array2;

use crate::elements::dream::entry::EntryType;
use crate::sync::prelude::*;
use crate::{cmp_coll, Diary, TagName};
use crate::{lock, stage_diary};

use super::{create_tag_assoc, tag_correlation_matrix};

#[derive(Debug, Clone)]
pub struct Summary {
    pub tag_categories: Vec<String>,
    pub(super) entry_categories: Vec<(String, EntryType)>,
    pub tag_assoc: HashMap<String, HashMap<TagName, usize>>,
    pub tag_correlation: Array2<f32>,
    pub tag_subspace_dimension: usize,
    //pub tag_projection: Array2<f32>,
}

impl Summary {
    pub fn new(mut diary: &mut Diary, subspace_dimension: usize) -> Result<Self, NumericError> {
        let (tag_categories, entry_categories) = {
            let mut stage = stage_diary!(diary; DiaryPart::TAG, DiaryPart::SET);
            if let (Ok(settings), Ok(tagforest)) = lock!(stage; crate::GS, crate::TagForest) {
                let mut standard_tag_categories = settings.standard_tag_categories().clone();
                let mut tag_categories = Vec::from_iter(
                    tagforest
                        .keys()
                        .filter(|c| !standard_tag_categories.contains(c))
                        .cloned(),
                );
                tag_categories.sort_by(|lhs, rhs| cmp_coll(lhs, rhs));
                standard_tag_categories.extend(tag_categories);
                (
                    standard_tag_categories,
                    [
                        ("Art", EntryType::Text),
                        ("Technik", EntryType::Text),
                        ("Intensität", EntryType::UInt),
                        ("Erinnerung", EntryType::UInt),
                        ("Klarheit", EntryType::UInt),
                        ("Stabilität", EntryType::UInt),
                    ]
                    .iter()
                    .map(|(a, b)| ((*a).to_string(), *b))
                    .collect(),
                )
            } else {
                panic!("Worker thread has disconnected.")
            }
        };
        let tag_assoc = create_tag_assoc(diary);
        let tag_correlation = tag_correlation_matrix(diary, &tag_assoc);
        /*let (_, tag_projection) = eigenvec_arnoldi_symmetric(
            &tag_correlation.dot(&tag_correlation.t()),
            &Array1::ones([tag_correlation.shape()[0]]),
            subspace_dimension,
        )?;*/

        Ok(Summary {
            tag_categories,
            entry_categories,
            tag_assoc,
            tag_correlation,
            tag_subspace_dimension: subspace_dimension,
            //tag_projection,
        })
    }

    /*pub fn project_tag(&self, category: &str, name: &str) -> Option<Array1<f32>> {
        let name = crate::elements::dream::tag_strength(name).0;
        if let Some(assoc) = self.tag_assoc.get(category).and_then(|cat| cat.get(name)) {
            Some(self.tag_projection.slice(s![*assoc, ..]).to_owned())
        } else {
            None
        }
    }

    pub fn tag_vector_draft(&self) -> Array1<f32> {
        Array1::zeros([self.tag_subspace_dimension])
    }*/
}
