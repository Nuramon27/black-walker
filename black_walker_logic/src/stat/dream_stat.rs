#![allow(dead_code)]
use std::borrow::Borrow;
use std::collections::HashMap;
use std::fmt::Write;

use chrono::{Datelike, NaiveDate};
use ndarray::Array1;
use norman::{desc::PNorm, Distance};

use crate::mconvenience::latex::{esc_for_latex, AsLaTeX, Instruction};
use crate::{Dream, DreamID, DreamItem, DreamList, TagForest};

use super::summary::Summary;

#[derive(Debug, Clone, PartialEq)]
pub struct NumericDream {
    d: Dream,
    id: DreamID,
    tag: HashMap<String, Array1<f32>>,
}

impl NumericDream {
    pub fn new(dream: DreamItem, _summary: &Summary) -> Self {
        /*let tag = HashMap::from_iter(dream.d.get_all_tags().iter().map(|(category, tags)| {
            let mut res = summary.tag_vector_draft();
            for tag in tags {
                res = res + summary.project_tag(category, tag).unwrap_or_else(|| summary.tag_vector_draft());
                //res = res + summary.tag_vector_draft();
            }
            (category.to_string(), normalized(res, PNorm::eucl()))
        }));*/
        let tag = HashMap::new();
        NumericDream {
            d: dream.d,
            id: dream.id,
            tag,
        }
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct DreamStat {
    d: Dream,
    id: DreamID,
    similar_dream_forward: Option<DreamID>,
    similar_dream_backward: Option<DreamID>,
    special_tag: Vec<(String, String)>,
}

impl DreamStat {
    pub fn convert_list(dreamlist: Vec<NumericDream>, _summary: &Summary) -> Vec<Self> {
        let mut similar_dreams_forward = Vec::new();
        let mut similar_dreams_backward = Vec::new();
        for (i, dream) in dreamlist.iter().enumerate() {
            let mut min_distance = 0.0;
            let mut min_dream = None;
            let mut dreamiter = dreamlist.iter().enumerate();
            while let Some((j, dream2)) = dreamiter.next() {
                if j == i {
                    break;
                }
                let distance = dream2
                    .tag
                    .iter()
                    .map(|(k, v)| {
                        if let Some(tag) = dream.tag.get(k) {
                            eprintln!("match found");
                            1.0 / v.distance(tag, PNorm::eucl())
                        } else {
                            eprintln!("no match {} {}", k, v);
                            0.0
                        }
                    })
                    .sum();
                if distance > min_distance {
                    min_distance = distance;
                    min_dream = Some(dream2.id);
                }
            }
            similar_dreams_forward.push(min_dream);

            let mut min_distance = 0.0;
            let mut min_dream = None;
            while let Some((_, dream2)) = dreamiter.next() {
                let distance = dream2
                    .tag
                    .iter()
                    .map(|(k, v)| {
                        if let Some(tag) = dream.tag.get(k) {
                            1.0 / v.distance(tag, PNorm::eucl())
                        } else {
                            0.0
                        }
                    })
                    .sum();
                if distance > min_distance {
                    min_distance = distance;
                    min_dream = Some(dream2.id);
                }
            }
            similar_dreams_backward.push(min_dream);
        }

        dreamlist
            .into_iter()
            .zip(similar_dreams_forward)
            .zip(similar_dreams_backward)
            .map(|((dream, forward), backward)| DreamStat {
                d: dream.d,
                id: dream.id,
                similar_dream_forward: forward,
                similar_dream_backward: backward,
                special_tag: Vec::new(),
            })
            .collect()
        /*DreamStat {
            d: other.d.clone(),
            id: other.id(),
            similar_dream_forward: None,
            similar_dream_backward: None,
            special_tag: Vec::new(),
        }*/
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub enum PresItem {
    Newline,
    Inst(Instruction),
    Header(NaiveDate, String),
    Section(String),
    Heading(String),
    Text(String, Formatting),
    Link(DreamID, String),
}

impl PresItem {
    pub fn to_latex(&self) -> Instruction {
        match self {
            PresItem::Newline => Instruction::inst("\n".to_string(), vec![]),
            PresItem::Inst(inst) => inst.clone(),
            PresItem::Header(date, name) => Instruction::inst(
                r"\DreamHeader".to_string(),
                vec![
                    date.year().to_string(),
                    date.month().to_string(),
                    date.day().to_string(),
                    esc_for_latex(name),
                ],
            ),
            PresItem::Section(s) => Instruction::inst(r"\Text".to_string(), vec![esc_for_latex(s)]),
            PresItem::Heading(s) => Instruction::inst(
                r"\Text".to_string(),
                vec![format!(r"\bfseries {}: ", esc_for_latex(s))],
            ),
            PresItem::Text(s, format) => Instruction::inst(
                r"\Text".to_string(),
                vec![format!("{} {}", format.to_latex_string(), esc_for_latex(s))],
            ),
            PresItem::Link(id, name) => Instruction::inst(
                r"\hypertarget".to_string(),
                vec![
                    id.date.format("%Y%m%d").to_string(),
                    format!(
                        "Traum {} vom {}: {}",
                        id.idx,
                        id.date.format(r"%d.\,%m.\,%Y"),
                        esc_for_latex(name),
                    ),
                ],
            ),
        }
    }
}

pub struct DreamPresentation {
    #[allow(unused)]
    d: Dream,
    #[allow(unused)]
    id: DreamID,
    content: Vec<PresItem>,
}

impl DreamPresentation {
    pub fn from_dream_stat(
        dream: DreamStat,
        summary: &Summary,
        dreamlist: impl Borrow<DreamList>,
        tagforest: impl Borrow<TagForest>,
    ) -> Self {
        let mut content = Vec::new();
        content.push(PresItem::Header(dream.id.date, dream.d.name.clone()));

        for (entrycat, entrytype) in &summary.entry_categories {
            if let Some(entry) = dream.d.inst_entry(entrycat, *entrytype) {
                content.push(PresItem::Inst(entry.clone()));
            }
        }
        let tags = dream.d.inst_tags(tagforest.borrow());
        for tagcat in &summary.tag_categories {
            if let Some(tag) = tags.get(tagcat) {
                content.push(PresItem::Inst(tag.clone()));
            }
        }

        content.push(PresItem::Text(dream.d.text.clone(), Formatting::default()));
        content.push(PresItem::Newline);
        for (date, notes) in &dream.d.notes {
            if !notes.note.trim().is_empty() {
                content.push(PresItem::Section(format!(
                    "Anmerkungen vom {}:",
                    date.format("%d.%m.%Y")
                )));
                content.push(PresItem::Newline);
                content.push(PresItem::Text(
                    notes.note.to_owned(),
                    Formatting::default().shape(Shape::Italic),
                ));
                content.push(PresItem::Newline);
            }
        }

        if let Some((id, name)) = dream
            .similar_dream_forward
            .and_then(|id| dreamlist.borrow().get(id).map(|d| (id, d.d.name.clone())))
        {
            content.push(PresItem::Newline);
            content.push(PresItem::Heading(
                "Traum mit ähnlichen Tags in Zukunft".to_string(),
            ));
            content.push(PresItem::Link(id, name));
        }
        if let Some((id, name)) = dream
            .similar_dream_backward
            .and_then(|id| dreamlist.borrow().get(id).map(|d| (id, d.d.name.clone())))
        {
            content.push(PresItem::Newline);
            content.push(PresItem::Heading(
                "Traum mit ähnlichen Tags in Vergangenheit".to_string(),
            ));
            content.push(PresItem::Link(id, name));
        }

        DreamPresentation {
            d: dream.d,
            id: dream.id,
            content,
        }
    }

    pub fn to_latex(&self) -> Instruction {
        let mut root_content = String::new();
        for c in &self.content {
            writeln!(root_content, "{}", c.to_latex())
                // Won't panic because writing to a string never fails.
                .unwrap();
        }
        Instruction::Environment {
            name: "Dream".to_string(),
            args: Vec::new(),
            content: root_content,
        }
    }
}

#[derive(Debug, Clone, Default, PartialEq, Eq, Hash)]
pub struct Formatting {
    pub shape: Shape,
    pub series: Series,
    pub family: Family,
}

impl Formatting {
    pub fn shape(mut self, new_shape: Shape) -> Self {
        self.shape = new_shape;
        self
    }

    pub fn series(mut self, new_series: Series) -> Self {
        self.series = new_series;
        self
    }

    pub fn family(mut self, new_family: Family) -> Self {
        self.family = new_family;
        self
    }

    pub fn to_latex_string(&self) -> String {
        let mut res = self.shape.to_latex().to_string();
        res.push_str(&self.series.to_latex().to_string());
        res.push_str(&self.family.to_latex().to_string());
        res
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub enum Shape {
    Up,
    Italic,
}

impl Default for Shape {
    fn default() -> Self {
        Shape::Up
    }
}

impl AsLaTeX for Shape {
    fn to_latex(&self) -> Instruction {
        match self {
            Shape::Up => Instruction::inst(r"\upshape".to_string(), Vec::new()),
            Shape::Italic => Instruction::inst(r"\itshape".to_string(), Vec::new()),
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub enum Series {
    Md,
    Bf,
}

impl Default for Series {
    fn default() -> Self {
        Series::Md
    }
}

impl AsLaTeX for Series {
    fn to_latex(&self) -> Instruction {
        match self {
            Series::Md => Instruction::inst(r"\mdseries".to_string(), Vec::new()),
            Series::Bf => Instruction::inst(r"\bfseries".to_string(), Vec::new()),
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub enum Family {
    Roman,
    Sans,
    Mono,
}

impl Default for Family {
    fn default() -> Self {
        Family::Roman
    }
}

impl AsLaTeX for Family {
    fn to_latex(&self) -> Instruction {
        match self {
            Family::Roman => Instruction::inst(r"\rmfamily".to_string(), Vec::new()),
            Family::Sans => Instruction::inst(r"\sffamily".to_string(), Vec::new()),
            Family::Mono => Instruction::inst(r"\ttfamily".to_string(), Vec::new()),
        }
    }
}
