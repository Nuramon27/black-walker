//! Several functions for statistical evaluation of the dream diary.

use std::collections::{HashMap, HashSet};

use ndarray::Array2;

use crate::sync::diary::DiaryPart;
use crate::{lock, read_single, stage_diary};
use crate::{Diary, DreamList, TagForest, TagName, TagSpecification};

mod dream_stat;
pub mod pretty_print;
mod summary;

pub fn create_tag_assoc(mut diary: &mut Diary) -> HashMap<String, HashMap<TagName, usize>> {
    let mut stage = stage_diary!(diary; DiaryPart::TAG);
    if let Ok(tagforest) = read_single!(stage, TagForest) {
        let mut res = HashMap::with_capacity(tagforest.len());
        let mut i = 0usize;
        for (cat, tagtree) in tagforest.iter() {
            let mut cat_assoc = HashMap::with_capacity(tagtree.len());
            for tag in tagtree {
                cat_assoc.insert(tag.name().to_owned(), i);
                i += 1;
            }
            res.insert(cat.to_string(), cat_assoc);
        }
        res.shrink_to_fit();
        res
    } else {
        panic!("Threads have disconnected")
    }
}

fn calc_assoc_len<S: std::hash::BuildHasher, T: std::hash::BuildHasher>(
    assoc: &HashMap<String, HashMap<TagName, usize, S>, T>,
) -> usize {
    let mut res = 0;
    for v in assoc.values() {
        for _ in v {
            res += 1;
        }
    }
    res
}

pub fn tag_correlation_matrix<S: std::hash::BuildHasher, T: std::hash::BuildHasher>(
    mut diary: &mut Diary,
    assoc: &HashMap<String, HashMap<TagName, usize, S>, T>,
) -> Array2<f32> {
    let mut stage = stage_diary!(diary; DiaryPart::TAG, DiaryPart::DL);
    if let (Ok(tagforest), Ok(dreamlist)) = lock!(stage; TagForest, DreamList) {
        let total_tag_count = calc_assoc_len(assoc);
        let mut res: Array2<f32> = Array2::zeros([total_tag_count, total_tag_count]);
        let mut tagset = HashSet::new();
        for (_, dream) in dreamlist.iter() {
            for (cat, tags) in dream.d.get_all_tags() {
                let tagtree = &tagforest[cat];
                for tag in tags {
                    let tag_pure = tag.base_name();
                    for p in tagtree.parent_chain(tag_pure).unwrap() {
                        tagset.insert((cat, p.name()));
                    }
                }
            }
            for (cati, tagi) in &tagset {
                for (catj, tagj) in &tagset {
                    res[[assoc[*cati][*tagi], assoc[*catj][*tagj]]] += 1.0;
                }
            }
            tagset.clear();
        }
        for (i, mut row) in res.outer_iter_mut().enumerate() {
            if row[[i]] > 0.0 {
                row /= row[[i]];
            } else {
                row[[i]] = 1.0;
            }
        }
        res
    } else {
        panic!("Threads have disconnected")
    }
}

#[cfg(test)]
mod tests {
    use crate::operate::tests::standard_diary;

    #[test]
    fn tag_correlation_matrix() {
        let mut diary = standard_diary();
        let assoc = super::create_tag_assoc(&mut diary);
        let matrix = super::tag_correlation_matrix(&mut diary, &assoc);

        assert_eq!(
            matrix[[
                assoc["Personen"]["Albus Dumbledore"],
                assoc["Personen"]["Albus Dumbledore"],
            ]],
            1.0
        );
        assert_eq!(
            matrix[[
                assoc["Personen"]["Unused Tag"],
                assoc["Personen"]["Unused Tag"],
            ]],
            1.0
        );
        assert!(
            matrix[[
                assoc["Personen"]["Albus Dumbledore"],
                assoc["Personen"]["Lord Voldemort"]
            ]] > 0.0
        );
        assert!(
            matrix[[
                assoc["Personen"]["Albus Dumbledore"],
                assoc["Personen"]["Lord Voldemort"]
            ]] <= 1.0
        );
        assert!(
            matrix[[
                assoc["Personen"]["Albus Dumbledore"],
                assoc["Personen"]["God"]
            ]] == 0.0
        );
    }
}
