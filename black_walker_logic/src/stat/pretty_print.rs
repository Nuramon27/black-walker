use std::io::Write;
use std::iter::FromIterator;

use crate::sync::prelude::*;
use crate::Diary;
use crate::{lock, stage_diary};
use crate::{DreamList, TagForest};

use super::dream_stat::{DreamPresentation, DreamStat, NumericDream};
use super::summary::Summary;

pub struct Settings {
    pub fontsize: u8,
    //pub standard_tag_categories: Vec<String>,
    //pub standard_entry_categories: Vec<String>,
}

impl Default for Settings {
    fn default() -> Self {
        Settings { fontsize: 12 }
    }
}

fn preamble<W: Write>(arg: &Settings, res: &mut W) -> Result<(), std::io::Error> {
    //let mut res = Vec::<u8>::new();

    // Documentclass
    writeln!(
        res,
        r"\documentclass[{}pt, a4paper]{{scrartcl}}",
        arg.fontsize
    )?;
    writeln!(res, r"\pagestyle{{plain}}")?;

    // Command definitions
    writeln!(
        res,
        r"\newcommand{{\BlackWalker}}[1]{{\subtitle{{Erstellt von Black Walker Version #1}}}}"
    )?;
    writeln!(
        res,
        concat!(
            r"\newcommand{{\DreamHeader}}[4]{{{{\noindent\itshape ",
            r"\hypertarget{{\thenummer}}{{Traum}} vom #3.#2.#1}}",
            r"\hfill{{\ttfamily\arabic{{nummer}}\stepcounter{{nummer}}}}\\[0.5ex]",
            r"{{\bfseries\sffamily\LARGE #4}}\par}}",
            '\n'
        )
    )?;
    writeln!(
        res,
        concat!(
            r"\usepackage[ngerman]{{babel}}",
            '\n',
            r"\usepackage{{xcolor}}",
            '\n',
            r"\usepackage{{siunitx}}",
            '\n',
            r"\usepackage{{eurosym}}",
            '\n',
            r"\definecolor{{greenblue}}{{RGB}}{{00,104,40}}",
            '\n',
            r"\usepackage[pdftoolbar=false, pdfmenubar=true, pdffitwindow=true, ",
            r"pdfstartview={{FitB}}, colorlinks=true, linkcolor=greenblue]{{hyperref}}"
        )
    )?;
    writeln!(
        res,
        r"\newcommand{{\Entry}}[2]{{\noindent{{\bfseries #1:}} #2\par}}"
    )?;
    writeln!(res, r"\newcommand{{\Text}}[1]{{{{#1}}}}")?;
    //writeln!(res, r"\newcommand{{\List}}[1]{{#1}}")?;
    writeln!(
        res,
        r"\newcommand{{\Tag}}[2]{{\noindent{{\bfseries #1:}} #2\par}}"
    )?;
    writeln!(res, r"\newenvironment{{DreamText}}{{\rmfamily}}{{\par}}")?;
    writeln!(
        res,
        concat!(
            r"\newenvironment{{Notes}}{{{{\bfseries\sffamily Bemerkungen:\par}}",
            r"\rmfamily\itshape }}{{\par}}"
        )
    )?;
    writeln!(res, r"\newenvironment{{Dream}}{{}}{{\clearpage}}")?;

    Ok(())
}

fn body<W: Write>(
    _arg: &Settings,
    res: &mut W,
    dreamlist: &DreamList,
    tagforest: &TagForest,
    summary: &Summary,
) -> Result<(), std::io::Error> {
    writeln!(res, r"\newcounter{{nummer}}")?;
    writeln!(res, r"\setcounter{{nummer}}{{1}}")?;
    writeln!(res, r"\begin{{document}}")?;
    let numeric_dreams = Vec::from_iter(
        dreamlist
            .iter()
            .map(|(_, dream)| NumericDream::new(dream.clone(), &summary)),
    );
    let dreamstatlist = DreamStat::convert_list(numeric_dreams, &summary);
    for dream in dreamstatlist {
        let presentation = DreamPresentation::from_dream_stat(dream, summary, dreamlist, tagforest);
        writeln!(res, "{}", presentation.to_latex())?;
    }
    writeln!(res, r"\end{{document}}")?;

    Ok(())
}

pub fn pretty_print<W: Write>(
    arg: &Settings,
    res: &mut W,
    mut diary: &mut Diary,
) -> Result<(), std::io::Error> {
    let summary = Summary::new(diary, 24);
    let mut stage = stage_diary!(diary; DL, TAG);
    if let ((Ok(dreamlist), Ok(tagforest)), Ok(summary)) =
        (lock!(stage; DreamList, TagForest), summary)
    {
        preamble(arg, res)?;
        body(arg, res, &dreamlist, &tagforest, &summary)?;
    } else {
        panic!("Could not write diary");
    }
    Ok(())
}
