//! The diary module contains the [`Diary`] struct and the
//! structs of which it consists:
//! [`DreamList`], [`TagTree`], [`NightList`], [`EntrySystem`] and [`GS`].

use std::collections::{BTreeMap, HashMap};
use std::sync::{Arc, RwLock};

use crate::feature::crossbeam::atomic::AtomicCell;
use num_enum::IntoPrimitive;

use crate::CacheTree;
use crate::GS;
use crate::{Count, Night, RevDate, Tag};

pub mod dreamlist;
pub mod entry_system;

pub use dreamlist::DreamList;
pub use entry_system::EntrySystem;
pub type TagTree = CacheTree<Tag, Count>;
pub type TagForest = HashMap<String, TagTree>;
pub type NightList = BTreeMap<RevDate, Night>;

#[repr(u8)]
#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash, IntoPrimitive)]
pub enum SaveState {
    Unmodified = 0x0,
    Modified = 0x1,
    New = 0x2,
}

impl Default for SaveState {
    fn default() -> Self {
        SaveState::New
    }
}
/// The main structure of the application that stores the dream diary
///
/// All fields are protected by an RwLock and `Send` and `Sync`,
/// so the Diary is `Send` and `Sync` as well, which is necessary
/// for Black Walker to be multithreaded.
#[derive(Debug, Clone)]
pub struct Diary {
    pub state: Arc<AtomicCell<SaveState>>,
    /// A TagTree for every category of tags.
    pub tag: Arc<RwLock<TagForest>>,
    /// The central list of dreams
    pub dl: Arc<RwLock<DreamList>>,
    /// The list of nightls with dreams
    pub nl: Arc<RwLock<NightList>>,
    /// A track of all entries that are in use.
    pub entr: Arc<RwLock<EntrySystem>>,
    pub gl: Arc<RwLock<GS>>,
}

impl Default for Diary {
    fn default() -> Diary {
        Diary::new()
    }
}

impl Diary {
    /// Creates a new empty diary.
    pub fn new() -> Diary {
        let settings = GS::default();
        let members = Self::default_members(&settings);
        Diary {
            state: Arc::new(AtomicCell::new(SaveState::default())),
            tag: Arc::new(RwLock::new(members.2)),
            dl: Arc::new(RwLock::new(members.0)),
            nl: Arc::new(RwLock::new(members.1)),
            entr: Arc::new(RwLock::new(members.3)),
            gl: Arc::new(RwLock::new(settings)),
        }
    }
    pub fn set_modified(&self) {
        self.state.store(SaveState::Modified);
    }
    pub fn default_members(settings: &GS) -> (DreamList, NightList, TagForest, EntrySystem) {
        let mut tag = TagForest::new();
        for cat in settings.standard_tag_categories() {
            tag.insert(cat.clone(), TagTree::new());
        }
        (
            DreamList::new(),
            NightList::new(),
            tag,
            EntrySystem::default(),
        )
    }
}
