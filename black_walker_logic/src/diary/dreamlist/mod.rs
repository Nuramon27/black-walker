pub(crate) mod store;

use std::collections::BTreeMap;

use chrono::NaiveDate;

use crate::elements::dream::{Dream, DreamID, DreamItem};

/// The `DreamList` struct collects dreams in different nights.
#[derive(Debug, Clone, Default, PartialEq, Eq)]
pub struct DreamList {
    l: BTreeMap<DreamID, DreamItem>,
}

impl DreamList {
    /// Constructs a new empty `DreamList`
    pub fn new() -> DreamList {
        DreamList { l: BTreeMap::new() }
    }
    /// Returns the number of dreams in the list.
    pub fn len(&self) -> usize {
        self.l.len()
    }

    /// Returns whether the list is empty.
    pub fn is_empty(&self) -> bool {
        self.l.is_empty()
    }

    /// Clears the list, removing all dreams.
    pub fn clear(&mut self) {
        self.l.clear()
    }

    /// Returns the `DreamList` in form of a `BTreeMap`.
    pub fn get_list(&self) -> &BTreeMap<DreamID, DreamItem> {
        &self.l
    }

    /// Returns whether a dream with `id` is present in the list.
    pub fn has_dream(&self, id: DreamID) -> bool {
        self.l.contains_key(&id)
    }

    /// Returns a reference to the dream identified by `id`.
    ///
    /// `None` is returned if there is no such dream.
    pub fn get(&self, id: DreamID) -> Option<&DreamItem> {
        self.l.get(&id)
    }

    /// Returns a mutable reference to the dream identified by `id`.
    ///
    /// `None` is returned if there is no such dream.
    pub fn get_mut(&mut self, id: DreamID) -> Option<&mut DreamItem> {
        self.l.get_mut(&id)
    }

    /// Returns the number of dreams in the night preceding `date`.
    pub fn dream_count(&self, date: NaiveDate) -> u32 {
        let rang = DreamID { date, idx: 0 }..DreamID {
            date: date.pred(),
            idx: 0,
        };
        let r = self.l.range(rang);
        let mut i = 0;
        for _it in r {
            i += 1;
        }
        i
    }

    /// Inserts a dream in the night given by `id.date`.
    ///
    /// The insertion only suceeds, if there already are at least `id.idx`
    /// dreams in this night. Otherwise, nothing is done and the `dream` is returned
    /// again.
    ///
    /// If there are dreams with ids greater or equal to `id`, `dream` is
    /// inserted before them and they are moved further, so their ids inrcease by one.
    pub fn insert_dream(&mut self, dream: Dream, id: DreamID) -> Option<Dream> {
        let id = DreamID {
            date: id.date,
            idx: id.idx,
        };
        if id.idx > self.dream_count(id.date) {
            return Some(dream);
        }
        // Move the dreams following id one step further.
        let mut swapdream = DreamItem::new(id, dream);
        let mut nextid = id;
        // Here we use that `BTreeMap::insert` returns the old item
        // if such one existed. This 'old item' the the next dream
        // that must be moved further.
        while let Some(swap) = self.l.insert(nextid, swapdream) {
            swapdream = swap;
            swapdream.id.idx += 1;
            nextid.idx += 1;
        }
        None
    }

    /// Inserts `dream` as the last dream in `date`.
    ///
    /// It is not a problem if there is not yet a dream in `date`
    /// -- the dream ist then just inserted as the only dream in this night.
    ///
    /// Returns the id the new dream obtains.
    pub fn insert_dream_at_end(&mut self, dream: Dream, date: NaiveDate) -> DreamID {
        let id = DreamID {
            date,
            idx: self.dream_count(date),
        };
        let tmp_res = self.l.insert(id, DreamItem::new(id, dream));
        debug_assert!(tmp_res.is_none());
        id
    }

    /// Removes the `dream` identified by `id` and returns it, if such a dream exists.
    ///
    /// Otherwise, nothing is done and `None` is returned.
    ///
    /// The dreams eventually following the dream are moved on step back
    /// (decreasing their ids) in order not to leave a gap in the ids of
    /// the night of the removed dream.
    pub fn remove_dream(&mut self, id: DreamID) -> Option<DreamItem> {
        let res = self.l.remove(&id);
        let mut nextid = id;
        // Move the dreams following id back.
        while let Some(mut swap) = self.l.remove(&DreamID {
            date: nextid.date,
            idx: nextid.idx + 1,
        }) {
            swap.id.idx -= 1;
            self.l.insert(nextid, swap);
            nextid.idx += 1;
        }
        res
    }

    /// Moves the dream from id `from` to id `to`.
    ///
    /// After the move, the dream will be found on id `to`.
    ///
    /// Dreams following the old position in the same night
    /// are moved one step back,
    /// dreams following the new position in the same night
    /// are moved one step further.
    ///
    /// If the move takes place in one night, dreams in between `from`
    /// and `to` are moved one step in the right direction.
    ///
    /// If the move succeeds, true is returned.
    /// If no dream exists at position `from` or the index of `to`
    /// is higher than the number of dreams in this night, nothing is
    /// done and false is returned.
    pub fn move_dream(&mut self, from: DreamID, to: DreamID) -> bool {
        if to.idx > self.dream_count(to.date) {
            return false;
        }
        if let Some(mut dream) = self.l.remove(&from) {
            dream.id = to;
            if from.date != to.date || from.idx < to.idx {
                let mut nextid = from;
                // Move the dreams that follow `from` in the same night
                // one step back.
                // But break when to is reached.
                //
                // This must only be done, when the dream lie in different nights
                // (because then, the following dreams must always be moved)
                // or when `from` < `to` because then dreams must be moved back.
                while let Some(mut swap) = self.l.remove(&DreamID {
                    date: nextid.date,
                    idx: nextid.idx + 1,
                }) {
                    swap.id.idx -= 1;
                    self.l.insert(nextid, swap);
                    nextid.idx += 1;
                    if nextid == to {
                        break;
                    }
                }
            }

            let mut swapdream = dream;
            let mut nextid = to;
            // Move the dreams that follow `to` in the same night
            // one step forward.
            // But break when `from` is reached.
            while let Some(swap) = self.l.insert(nextid, swapdream) {
                swapdream = swap;
                swapdream.id.idx += 1;
                nextid.idx += 1;
            }
            true
        } else {
            false
        }
    }

    pub fn iter(&self) -> std::collections::btree_map::Iter<DreamID, DreamItem> {
        self.into_iter()
    }

    pub fn iter_mut(&mut self) -> std::collections::btree_map::IterMut<DreamID, DreamItem> {
        self.into_iter()
    }
}

impl IntoIterator for DreamList {
    type Item = (DreamID, DreamItem);
    type IntoIter = std::collections::btree_map::IntoIter<DreamID, DreamItem>;
    fn into_iter(self) -> Self::IntoIter {
        self.l.into_iter()
    }
}

impl<'a> IntoIterator for &'a DreamList {
    type Item = (&'a DreamID, &'a DreamItem);
    type IntoIter = std::collections::btree_map::Iter<'a, DreamID, DreamItem>;
    fn into_iter(self) -> Self::IntoIter {
        self.l.iter()
    }
}

impl<'a> IntoIterator for &'a mut DreamList {
    type Item = (&'a DreamID, &'a mut DreamItem);
    type IntoIter = std::collections::btree_map::IterMut<'a, DreamID, DreamItem>;
    fn into_iter(self) -> Self::IntoIter {
        self.l.iter_mut()
    }
}

#[cfg(test)]
mod tests;
