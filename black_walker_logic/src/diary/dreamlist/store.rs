use serde::{Deserialize, Serialize};

use crate::elements::dream::store::*;
use crate::DreamList;

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub struct DreamList2_3_1 {
    pub l: Vec<DreamItem2_3_1>,
}

impl From<DreamList> for DreamList2_3_1 {
    fn from(other: DreamList) -> Self {
        DreamList2_3_1 {
            l: other.l.into_iter().map(|(_, v)| v.into()).collect(),
        }
    }
}

impl From<DreamList2_3_1> for DreamList {
    fn from(other: DreamList2_3_1) -> Self {
        DreamList {
            l: other.l.into_iter().map(|v| (v.id, v.into())).collect(),
        }
    }
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub struct DreamList2_3_0 {
    pub l: Vec<DreamItem2_3_0>,
}

impl From<DreamList2_3_0> for DreamList {
    fn from(other: DreamList2_3_0) -> Self {
        DreamList {
            l: other.l.into_iter().map(|v| (v.id, v.into())).collect(),
        }
    }
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub struct DreamList2_2_0 {
    pub l: Vec<DreamItem2_2_0>,
}

impl From<DreamList2_2_0> for DreamList {
    fn from(other: DreamList2_2_0) -> Self {
        DreamList {
            l: other.l.into_iter().map(|v| (v.id, v.into())).collect(),
        }
    }
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub struct DreamList2_0_0 {
    pub l: Vec<DreamItem2_0_0>,
}

impl From<DreamList2_0_0> for DreamList {
    fn from(other: DreamList2_0_0) -> Self {
        DreamList {
            l: other.l.into_iter().map(|v| (v.id, v.into())).collect(),
        }
    }
}

#[cfg(test)]
mod tests {
    use std::collections::HashMap;

    use chrono::NaiveDate;
    use toml::ser::to_string_pretty;

    use crate::DreamID;

    use super::DreamItem2_0_0;
    /*#[derive(Debug, Clone, PartialEq, Eq)]
    #[derive(Serialize, Deserialize)]
    pub struct DreamItem2_0_0 {
        /// The title of the dream
        pub name: String,
        /// The dream text
        pub text: String,
        /// A place to take notes to a dream
        pub notes: String,
        pub id: DreamID,

        //#[serde(serialize_with = "toml::ser::tables_last")]
        //pub entry_text: HashMap<String, String>,
        //#[serde(serialize_with = "toml::ser::tables_last")]
        //pub entry_uint: HashMap<String, u32>,
        //#[serde(serialize_with = "toml::ser::tables_last")]
        //pub entry_int: HashMap<String, i32>,
        //#[serde(serialize_with = "toml::ser::tables_last")]
        //pub entry_date: HashMap<String, NaiveDate>,
        //#[serde(serialize_with = "toml::ser::tables_last")]
        //pub entry_time: HashMap<String, NaiveTime>,
        //#[serde(serialize_with = "toml::ser::tables_last")]
        //pub entry_list: HashMap<String, Vec<String>>,
        //#[serde(serialize_with = "toml::ser::tables_last")]
        //pub tag: HashMap<String, Vec<String>>,
    }*/

    #[test]
    fn toml_dream_item() {
        let mut a = DreamItem2_0_0 {
            id: DreamID::from_date(NaiveDate::from_ymd(2002, 02, 02)),
            name: "A".to_string(),
            text: "Lorem ipsum".to_string(),
            notes: "Some notes".to_string(),
            entry_text: HashMap::new(),
            entry_list: HashMap::new(),
            entry_time: HashMap::new(),
            entry_date: HashMap::new(),
            entry_uint: HashMap::new(),
            entry_int: HashMap::new(),
            tag: HashMap::new(),
        };
        to_string_pretty(&a).expect("Could not serialize anything.");

        a.entry_text
            .insert("Art".to_string(), "Trübtraum".to_string());
        assert!(
            to_string_pretty(&a).is_ok(),
            "Could not serialize simple Map"
        );

        a.entry_date.insert(
            "Eingeschlafen".to_string(),
            NaiveDate::from_ymd(2002, 03, 03),
        );
        assert!(
            to_string_pretty(&a).is_ok(),
            "Could not serialize Map of dates"
        );

        a.tag.insert(
            "Personen".to_string(),
            vec!["Albus Dumbledore", "Lord Voldemort"]
                .into_iter()
                .map(|s| crate::TagSpec::from(s))
                .collect(),
        );
        assert!(
            to_string_pretty(&a).is_ok(),
            "Could not serialize map of vecs."
        )
    }
}
