use chrono::NaiveDate;

use super::{DreamID, DreamList};
use crate::Dream;

#[test]
fn cmp_dreamid() {
    let a = DreamID {
        date: NaiveDate::from_ymd(1996, 10, 31),
        idx: 3,
    };
    let b = DreamID {
        date: NaiveDate::from_ymd(1996, 10, 31),
        idx: 4,
    };
    let c = DreamID {
        date: NaiveDate::from_ymd(1996, 10, 30),
        idx: 3,
    };
    let d = DreamID {
        date: NaiveDate::from_ymd(1996, 11, 01),
        idx: 2,
    };
    let e = DreamID {
        date: NaiveDate::from_ymd(1996, 10, 31),
        idx: 3,
    };

    assert!(a < b);
    assert!(a < c);
    assert!(a > d);
    assert!(a == e);
    assert!(a >= e);
    assert!(a <= e);
    assert!(b >= a);
    assert!(c >= d);
}

fn standard_dreamlist() -> DreamList {
    let d1 = NaiveDate::from_ymd(1054, 7, 4);
    let d2 = NaiveDate::from_ymd(1990, 10, 3);
    let d3 = NaiveDate::from_ymd(1980, 11, 1);
    let mut l = DreamList::default();
    assert!(l
        .insert_dream(
            Dream::from_oblig("Flying".to_string(), "Trübtraum".to_string()),
            DreamID::from_date(d1)
        )
        .is_none());
    assert!(l
        .insert_dream(
            Dream::from_oblig("Yoda and Voldemort".to_string(), "Trübtraum".to_string()),
            DreamID::from_date(d1)
        )
        .is_none());
    assert!(l
        .insert_dream(
            Dream::from_oblig("Inside".to_string(), "Trübtraum".to_string()),
            DreamID::new(d1, 2)
        )
        .is_none());
    l.insert_dream_at_end(
        Dream::from_oblig("Later dream".to_string(), "Trübtraum".to_string()),
        d2,
    );
    l.insert_dream_at_end(
        Dream::from_oblig("Boundary of the world".to_string(), "Klartraum".to_string()),
        d3,
    );
    l
}

#[test]
fn insert_dream() {
    let d1 = NaiveDate::from_ymd(1054, 7, 4);
    let d2 = NaiveDate::from_ymd(1990, 10, 3);
    let mut l = DreamList::default();
    assert_eq!(l.dream_count(d1), 0);
    assert!(l
        .insert_dream(
            Dream::from_oblig("Flying".to_string(), "Trübtraum".to_string()),
            DreamID::from_date(d1)
        )
        .is_none());
    assert_eq!(l.dream_count(d1), 1);
    assert!(l
        .insert_dream(
            Dream::from_oblig("Yoda and Voldemort".to_string(), "Trübtraum".to_string()),
            DreamID::from_date(d1)
        )
        .is_none());
    assert!(l
        .insert_dream(
            Dream::from_oblig("Wrong dream".to_string(), "Trübtraum".to_string()),
            DreamID::new(d1, 3)
        )
        .is_some());
    assert!(l
        .insert_dream(
            Dream::from_oblig("Inside".to_string(), "Trübtraum".to_string()),
            DreamID::new(d1, 2)
        )
        .is_none());
    assert_eq!(
        l.insert_dream_at_end(
            Dream::from_oblig("Later dream".to_string(), "Trübtraum".to_string()),
            d2
        ),
        DreamID::new(d2, 0)
    );
    assert_eq!(
        l.get(DreamID::new(d1, 0))
            .expect("Dream on d1 not inserted")
            .d
            .name,
        "Yoda and Voldemort"
    );
    assert_eq!(l.get(DreamID::new(d1, 1)).unwrap().d.name, "Flying");
    assert_eq!(l.get(DreamID::new(d1, 1)).unwrap().id().idx, 1);
    assert_eq!(l.get(DreamID::new(d1, 2)).unwrap().d.name, "Inside");
    assert_eq!(
        l.get(DreamID::new(d1, 2)).unwrap().id(),
        DreamID::new(d1, 2)
    );
    assert_eq!(l.get(DreamID::new(d2, 0)).unwrap().d.name, "Later dream");
    assert_eq!(
        l.get(DreamID::new(d2, 0)).unwrap().id(),
        DreamID::new(d2, 0)
    );

    assert_eq!(l.dream_count(d1), 3);
    assert_eq!(l.dream_count(d2), 1);
    assert_eq!(l.dream_count(NaiveDate::from_ymd(2018, 01, 01)), 0);

    assert_eq!(
        l.insert_dream_at_end(Dream::default(), d1),
        DreamID::new(d1, 3)
    );
}

#[test]
fn remove_dream() {
    let d1 = NaiveDate::from_ymd(1054, 7, 4);
    let d3 = NaiveDate::from_ymd(1980, 11, 1);
    let mut l = standard_dreamlist();
    assert!(l
        .remove_dream(DreamID::from_date(NaiveDate::from_ymd(2010, 5, 23)))
        .is_none());
    assert_eq!(
        l.remove_dream(DreamID::new(d1, 1)).unwrap().d.name,
        "Flying"
    );
    assert_eq!(l.get(DreamID::new(d1, 1)).unwrap().d.name, "Inside");
    assert_eq!(l.get(DreamID::new(d1, 1)).unwrap().id().idx, 1);
    assert_eq!(
        l.get(DreamID::from_date(d1)).unwrap().d.name,
        "Yoda and Voldemort"
    );
    assert_eq!(l.get(DreamID::from_date(d1)).unwrap().id().idx, 0);
    assert_eq!(
        l.get(DreamID::from_date(d3)).unwrap().d.name,
        "Boundary of the world"
    );
}

#[test]
fn move_dream() {
    let d1 = NaiveDate::from_ymd(1054, 7, 4);
    let d2 = NaiveDate::from_ymd(1990, 10, 3);
    let d3 = NaiveDate::from_ymd(1980, 11, 1);
    let mut l = standard_dreamlist();

    assert!(l.move_dream(DreamID::new(d1, 1), DreamID::new(d3, 0)));

    assert_eq!(l.get(DreamID::new(d3, 0)).unwrap().d.name, "Flying");
    assert_eq!(
        l.get(DreamID::new(d3, 0)).unwrap().id(),
        DreamID::new(d3, 0)
    );
    assert_eq!(
        l.get(DreamID::new(d3, 1)).unwrap().d.name,
        "Boundary of the world"
    );
    assert_eq!(
        l.get(DreamID::new(d3, 1)).unwrap().id(),
        DreamID::new(d3, 1)
    );

    assert!(l.move_dream(DreamID::new(d3, 1), DreamID::new(d1, 2)));

    assert_eq!(
        l.get(DreamID::new(d1, 2)).unwrap().d.name,
        "Boundary of the world"
    );
    assert_eq!(
        l.get(DreamID::new(d1, 2)).unwrap().id(),
        DreamID::new(d1, 2)
    );

    assert_eq!(
        l.get(DreamID::new(d1, 0)).unwrap().d.name,
        "Yoda and Voldemort"
    );

    assert!(l.move_dream(DreamID::new(d1, 0), DreamID::new(d1, 2)));

    assert_eq!(
        l.get(DreamID::new(d1, 2)).unwrap().d.name,
        "Yoda and Voldemort"
    );
    assert_eq!(
        l.get(DreamID::new(d1, 2)).unwrap().id(),
        DreamID::new(d1, 2)
    );
    assert_eq!(
        l.get(DreamID::new(d1, 1)).unwrap().d.name,
        "Boundary of the world"
    );
    assert_eq!(
        l.get(DreamID::new(d1, 1)).unwrap().id(),
        DreamID::new(d1, 1)
    );
    assert_eq!(l.get(DreamID::new(d1, 0)).unwrap().d.name, "Inside");
    assert_eq!(
        l.get(DreamID::new(d1, 0)).unwrap().id(),
        DreamID::new(d1, 0)
    );

    assert!(l.move_dream(DreamID::new(d1, 1), DreamID::new(d1, 0)));

    assert_eq!(l.get(DreamID::new(d1, 1)).unwrap().d.name, "Inside");
    assert_eq!(
        l.get(DreamID::new(d1, 1)).unwrap().id(),
        DreamID::new(d1, 1)
    );
    assert_eq!(
        l.get(DreamID::new(d1, 0)).unwrap().d.name,
        "Boundary of the world"
    );
    assert_eq!(
        l.get(DreamID::new(d1, 0)).unwrap().id(),
        DreamID::new(d1, 0)
    );

    assert!(!(l.move_dream(DreamID::new(d1, 0), DreamID::new(d2, 2))));
}

#[test]
fn move_dream_forward_in_same_night() {
    let date = NaiveDate::from_ymd(1054, 07, 04);
    let dreams = vec![
        Dream::from_oblig("A".to_string(), "Ordinary Dream".to_string()),
        Dream::from_oblig("B".to_string(), "Ordinary Dream".to_string()),
        Dream::from_oblig("C".to_string(), "Ordinary Dream".to_string()),
        Dream::from_oblig("D".to_string(), "Ordinary Dream".to_string()),
    ];
    let mut dl = DreamList::new();
    for it in dreams {
        dl.insert_dream_at_end(it, date);
    }
    dl.move_dream(DreamID::new(date, 0), DreamID::new(date, 2));
    assert_eq!(
        dl.get(DreamID::new(date, 0)).unwrap().d.name,
        "B",
        "Dream after moved dream has not been moved back"
    );
    assert_eq!(
        dl.get(DreamID::new(date, 1)).unwrap().d.name,
        "C",
        "Moved dream was inserted to early."
    );
    assert_eq!(
        dl.get(DreamID::new(date, 2)).unwrap().d.name,
        "A",
        "Moved dream was inserted to early."
    );
    assert_eq!(
        dl.get(DreamID::new(date, 3)).unwrap().d.name,
        "D",
        "Untouched dream has been moved incorrectly."
    );
}
#[test]
fn move_dream_back_in_same_night() {
    let date = NaiveDate::from_ymd(1054, 07, 04);
    let dreams = vec![
        Dream::from_oblig("A".to_string(), "Ordinary Dream".to_string()),
        Dream::from_oblig("B".to_string(), "Ordinary Dream".to_string()),
        Dream::from_oblig("C".to_string(), "Ordinary Dream".to_string()),
        Dream::from_oblig("D".to_string(), "Ordinary Dream".to_string()),
    ];
    let mut dl = DreamList::new();
    for it in dreams {
        dl.insert_dream_at_end(it, date);
    }
    dl.move_dream(DreamID::new(date, 2), DreamID::new(date, 0));
    assert_eq!(
        dl.get(DreamID::new(date, 0)).unwrap().d.name,
        "C",
        "Dream has not been moved correctly."
    );
    assert_eq!(
        dl.get(DreamID::new(date, 1)).unwrap().d.name,
        "A",
        "Dream on new id of moved dream has not been moved forward."
    );
    assert_eq!(
        dl.get(DreamID::new(date, 2)).unwrap().d.name,
        "B",
        "Dream B has not been moved."
    );
    assert_eq!(
        dl.get(DreamID::new(date, 3)).unwrap().d.name,
        "D",
        "Untouched dream has been moved incorrectly."
    );
}
