use std::collections::{HashMap, HashSet};

use crate::{Dream, DreamList};

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct EntrySystem {
    text: HashMap<String, HashSet<String>>,
    list: HashMap<String, HashSet<String>>,
    // crate-public because it needs to be manipulated in the store::compat module
    pub(crate) ranges_u: HashMap<String, (i32, i32)>,
    ranges_i: HashMap<String, (i32, i32)>,
}

impl Default for EntrySystem {
    fn default() -> Self {
        EntrySystem {
            text: HashMap::new(),
            list: HashMap::new(),
            ranges_u: default_slider_ranges(),
            ranges_i: HashMap::new(),
        }
    }
}

impl EntrySystem {
    pub fn contains_text(&self, category: &str) -> bool {
        self.text
            .get(category)
            .map(|set| set.contains(category))
            .unwrap_or(false)
    }
    pub fn contains_text_of_list(&self, category: &str) -> bool {
        self.list
            .get(category)
            .map(|set| set.contains(category))
            .unwrap_or(false)
    }
    pub fn texts(&self, category: &str) -> Option<&HashSet<String>> {
        self.text.get(category)
    }
    pub fn lists(&self, category: &str) -> Option<&HashSet<String>> {
        self.list.get(category)
    }
    pub fn range_u(&self, category: &str) -> Option<(i32, i32)> {
        self.ranges_u.get(category).copied()
    }
    pub fn range_i(&self, category: &str) -> Option<(i32, i32)> {
        self.ranges_i.get(category).copied()
    }
    pub fn all_texts(&self) -> &HashMap<String, HashSet<String>> {
        &self.text
    }
    pub fn all_lists(&self) -> &HashMap<String, HashSet<String>> {
        &self.list
    }

    pub fn add_text(&mut self, category: &str, text: String) -> bool {
        self.text
            .entry(category.to_string())
            .or_insert(HashSet::new())
            .insert(text)
    }

    pub fn add_entries_of_dream(&mut self, dream: &Dream) {
        for (cat, entry) in dream.get_all_entries_text() {
            self.text
                .entry(cat.clone())
                .or_insert(HashSet::new())
                .insert(entry.clone());
        }
        for (cat, entry) in dream.get_all_entries_list() {
            self.list
                .entry(cat.clone())
                .or_insert(HashSet::new())
                .extend(entry.iter().cloned());
        }
    }

    pub fn add_all_entries(&mut self, list: &DreamList) {
        for (_, dream) in list {
            self.add_entries_of_dream(&dream.d)
        }
    }
}

pub fn default_slider_ranges() -> HashMap<String, (i32, i32)> {
    [
        ("Intensität", 0..5),
        ("Erinnerung", 0..5),
        ("Länge", 0..5),
        ("Phantastischkeit", 0..5),
        ("Angst", 0..5),
        ("Persönliche Relevanz", 0..5),
        ("Stabilität", 0..5),
        ("Klarheit", 0..5),
        ("Einschlafleichtigkeit", 0..5),
    ]
    .iter()
    .map(|(name, range)| (name.to_string(), (range.start, range.end)))
    .collect()
}

pub(crate) mod store {
    use std::collections::HashMap;

    use serde::{Deserialize, Serialize};

    use crate::EntrySystem;

    #[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
    pub struct Range2_0_0<T> {
        start: T,
        end: T,
    }

    impl<T> From<(T, T)> for Range2_0_0<T> {
        fn from(other: (T, T)) -> Self {
            Range2_0_0 {
                start: other.0,
                end: other.1,
            }
        }
    }

    impl<T> From<Range2_0_0<T>> for (T, T) {
        fn from(other: Range2_0_0<T>) -> Self {
            (other.start, other.end)
        }
    }

    #[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
    pub struct EntrySystem2_0_0 {
        #[serde(serialize_with = "toml::ser::tables_last")]
        ranges_u: HashMap<String, Range2_0_0<i32>>,
        #[serde(serialize_with = "toml::ser::tables_last")]
        ranges_i: HashMap<String, Range2_0_0<i32>>,
    }

    impl From<EntrySystem> for EntrySystem2_0_0 {
        fn from(other: EntrySystem) -> Self {
            EntrySystem2_0_0 {
                ranges_u: other
                    .ranges_u
                    .into_iter()
                    .map(|(k, v)| (k, Range2_0_0::from(v)))
                    .collect(),
                ranges_i: other
                    .ranges_i
                    .into_iter()
                    .map(|(k, v)| (k, Range2_0_0::from(v)))
                    .collect(),
            }
        }
    }

    impl From<EntrySystem2_0_0> for EntrySystem {
        fn from(other: EntrySystem2_0_0) -> Self {
            EntrySystem {
                text: HashMap::new(),
                list: HashMap::new(),
                ranges_u: other
                    .ranges_u
                    .into_iter()
                    .map(|(k, v)| (k, v.into()))
                    .collect(),
                ranges_i: other
                    .ranges_i
                    .into_iter()
                    .map(|(k, v)| (k, v.into()))
                    .collect(),
            }
        }
    }
}
