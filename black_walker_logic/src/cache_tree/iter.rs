//! Iterators for the [`CacheTree`](CacheTree).

use std::collections::hash_map;
use std::ops::{AddAssign, SubAssign};

use num_traits::Zero;

use super::*;

// Iterators for CacheTree

/// An immutable iterator through the [nodes](Node) of a [`CacheTree`].
///
/// ## Ordering of Tags in a `CacheTree`
/// It visits the elements of the `CacheTree` in the following order:
/// Every `Node` is visited directly before its children are visited.
/// Those of the children, that have own children, are visited before
/// the children that are "leaves". Within these two groups, the children
/// are visited in alphabetical order.
#[derive(Debug)]
pub struct CacheTreeIter<'a, T: Default + Eq, C: Clone + Zero + IncDec>
where
    for<'k, 'l> C: AddAssign<&'k C> + SubAssign<&'l C>,
{
    target: &'a CacheTree<T, C>,
    curr_parent: Option<&'a Node<T, C>>,
    child: usize,
    child_chain: Vec<usize>,
}

impl<'a, T: Default + Eq, C: Clone + Zero + IncDec> Iterator for CacheTreeIter<'a, T, C>
where
    for<'k, 'l> C: AddAssign<&'k C> + SubAssign<&'l C>,
{
    type Item = &'a Node<T, C>;
    fn next(&mut self) -> Option<Self::Item> {
        if self.curr_parent == None {
            self.curr_parent = Some(self.target.cache.get("").expect("Root does not exist"));
            return self.curr_parent;
        }
        let next = loop {
            if let Some(next) = self.curr_parent.unwrap().children.get(self.child) {
                self.child += 1;
                break self
                    .target
                    .cache
                    .get(next)
                    //Won't panic because the children of a node are in cache
                    .expect("Next node does not exist");
            } else if let Some(child) = self.child_chain.pop() {
                self.curr_parent = Some(
                    self.target
                        .cache
                        .get(&self.curr_parent.unwrap().parent)
                        // Won't panic because the parent of a node is in cache
                        .expect("Parent of node does not exist"),
                );
                self.child = child;
            } else {
                return None;
            }
        };
        if !next.children.is_empty() {
            self.child_chain.push(self.child);
            self.child = 0;
            self.curr_parent = Some(next);
        }
        Some(next)
    }
}

pub struct ParentChainIter<'a, T: Default + Eq, C: Clone + Zero + IncDec>
where
    for<'k, 'l> C: AddAssign<&'k C> + SubAssign<&'l C>,
{
    target: &'a CacheTree<T, C>,
    curr_tag: Option<&'a Node<T, C>>,
}

impl<'a, T: Default + Eq, C: Clone + Zero + IncDec> Iterator for ParentChainIter<'a, T, C>
where
    for<'k, 'l> C: AddAssign<&'k C> + SubAssign<&'l C>,
{
    type Item = &'a Node<T, C>;
    fn next(&mut self) -> Option<Self::Item> {
        if let Some(curr_tag) = self.curr_tag {
            // If the current node is the root node, the iteration stops
            // after emitting this root node.
            if curr_tag.name.is_root() {
                self.curr_tag = None;
            } else {
                let newparent = self
                    .target
                    .cache
                    .get(&curr_tag.parent)
                    // Won't panic because the parent of a node exists.
                    .expect("Parent of node does not exist!");
                self.curr_tag = Some(newparent);
            }
            Some(curr_tag)
        } else {
            None
        }
    }
}

pub struct ParentChainPseudoIterMut<'a, T: Default + Eq, C: Clone + Zero + IncDec>
where
    for<'k, 'l> C: AddAssign<&'k C> + SubAssign<&'l C>,
{
    target: &'a mut CacheTree<T, C>,
    curr_tag: Option<TagName>,
}

impl<'a, T: Default + Eq, C: Clone + Zero + IncDec> ParentChainPseudoIterMut<'a, T, C>
where
    for<'k, 'l> C: AddAssign<&'k C> + SubAssign<&'l C>,
{
    #[allow(clippy::should_implement_trait)]
    pub fn next<'b>(&'b mut self) -> Option<&'b mut Node<T, C>> where {
        if let Some(curr_tag) = &self.curr_tag {
            let res = self
                .target
                .cache
                .get_mut(curr_tag)
                // Won't panic because the curr_tag is the parent of
                // another node.
                .expect("Current node does not exist!");
            if res.name.is_root() {
                self.curr_tag = None
            } else {
                self.curr_tag = Some(res.parent.clone());
            }
            Some(res)
        } else {
            None
        }
    }
}

// Implementation of iterator methods on CacheTree
impl<T: Default + Eq, C: Clone + Zero + IncDec> CacheTree<T, C>
where
    for<'k, 'l> C: AddAssign<&'k C> + SubAssign<&'l C>,
{
    /// Returns an iterator that visits the whole tree of nodes,
    /// beginning with the root, based on the rule:
    /// "After a node is visited, all of its children are visited."
    ///
    /// # Examples
    /// ```
    /// use std::str::FromStr;
    /// use std::convert::TryInto;
    ///
    /// use black_walker_logic::cache_tree::{Node, CacheTree};
    /// use black_walker_logic::TagName;
    ///
    /// black_walker_logic::elements::tag::names::collation::init_collator();
    ///
    /// let mut t = CacheTree::<(), i32>::new();
    /// t.insert_content_at_root(TagName::from_str("Movies").unwrap(), ());
    /// t.insert_content_at_root(TagName::from_str("Books").unwrap(), ());
    /// t.insert_content(TagName::from_str("Albus Dumbledore").unwrap(), (), "Movies".try_into().unwrap());
    /// t.insert_content(TagName::from_str("Lord Voldemort").unwrap(), (), "Movies".try_into().unwrap());
    /// t.insert_content(TagName::from_str("Mortimer").unwrap(), (), "Books".try_into().unwrap());
    ///
    /// let mut it = t.iter();
    ///
    /// assert_eq!(it.next().unwrap().name(), "");
    /// assert_eq!(it.next().unwrap().name(), "Movies");
    /// assert_eq!(it.next().unwrap().name(), "Albus Dumbledore");
    /// assert_eq!(it.next().unwrap().name(), "Lord Voldemort");
    /// assert_eq!(it.next().unwrap().name(), "Books");
    /// assert_eq!(it.next().unwrap().name(), "Mortimer");
    /// assert_eq!(it.next(), None);
    /// ```
    pub fn iter(&self) -> CacheTreeIter<T, C> {
        CacheTreeIter {
            target: self,
            curr_parent: None,
            child: 0,
            child_chain: Vec::new(),
        }
    }

    /// Gives in iterator that visits all nodes in an arbitrary order.
    ///
    /// It is simply implemented as the iterator over the internal `HashMap`.
    pub fn iter_unordered(&self) -> hash_map::Values<TagName, Node<T, C>> {
        self.cache.values()
    }

    /// Gives an iterator through the parents of the node `name`.
    ///
    /// The Iterator visits the parents of the node in descending order. This means
    /// first the node itself is visited, then its parent, then the parent of its parent
    /// and so on. At last, the root is visited.
    ///
    /// ## Examples
    ///
    /// ```
    /// use std::str::FromStr;
    /// use std::convert::TryInto;
    ///
    /// use black_walker_logic::cache_tree::{Node, CacheTree};
    /// use black_walker_logic::TagName;
    ///
    /// let mut t = CacheTree::<(), i32>::new();
    /// t.insert_content_at_root(TagName::from_str("Fiktive Personen").unwrap(), ());
    /// t.insert_content(TagName::from_str("Books").unwrap(), (), "Fiktive Personen".try_into().unwrap());
    /// t.insert_content(TagName::from_str("Harry Potter (books)").unwrap(), (), "Books".try_into().unwrap());
    /// t.insert_content(TagName::from_str("Albus Dumbledore").unwrap(), (), "Harry Potter (books)".try_into().unwrap());
    ///
    /// let mut it = t.parent_chain("Albus Dumbledore")
    ///     .expect("Node Albus Dumbledore was not created");
    /// assert_eq!(it.next().unwrap().name(), "Albus Dumbledore");
    /// assert_eq!(it.next().unwrap().name(), "Harry Potter (books)");
    /// assert_eq!(it.next().unwrap().name(), "Books");
    /// assert_eq!(it.next().unwrap().name(), "Fiktive Personen");
    /// assert_eq!(it.next().unwrap().name(), "");
    /// assert_eq!(it.next(), None);
    /// ```
    pub fn parent_chain<'n, N: TryInto<&'n TagStr>>(
        &self,
        name: N,
    ) -> Option<ParentChainIter<T, C>> {
        let name = name.try_into().ok()?;
        if let Some(start) = self.cache.get(name) {
            Some(ParentChainIter {
                target: self,
                curr_tag: Some(start),
            })
        } else {
            None
        }
    }

    /// Returns a mutable iterator through the parents of the node `name`.
    ///
    /// This is basically a mutable version of [`parent_chain`](CacheTree::parent_chain),
    /// with the only difference that it is no Iterator. As the [Iterator] trait is written
    /// in a way that ensures that all items of the Iterator may coexist at the
    /// same time, this design can only be realised in a mutable iterator
    /// by using raw pointers. I did not want to do that, so this function
    /// yields only a pseudo iterator, that has a [`next`](ParentChainPseudoIterMut::next)-
    /// method, but with a different lifetime semantic than [`Iterator::next`].
    /// Thus it cannot be used in loops.
    ///
    /// ## Examples
    ///
    ///
    /// use std::str::FromStr;
    /// use std::convert::TryInto;
    ///
    /// use black_walker_logic::cache_tree::{Node, CacheTree};
    /// use black_walker_logic::TagName;
    ///
    /// let mut t = CacheTree::<(), i32>::new();
    /// t.insert_content_at_root(TagName::from_str("Fiktive Personen").unwrap(), ());
    /// t.insert_content(TagName::from_str("Books").unwrap(), (), "Fiktive Personen".try_into().unwrap());
    ///
    /// let mut it = t.parent_chain_mut("Books".try_into().unwrap()).unwrap();
    ///
    /// while let Some(p) = it.next() {
    ///     p.count += 1;
    /// }
    ///
    ///
    pub fn parent_chain_mut<'n, N: TryInto<&'n TagStr>>(
        &mut self,
        name: N,
    ) -> Option<ParentChainPseudoIterMut<T, C>> {
        let name = name.try_into().ok()?;
        if self.cache.contains_key(name) {
            Some(ParentChainPseudoIterMut {
                target: self,
                curr_tag: Some(name.to_owned()),
            })
        } else {
            None
        }
    }
}

impl<'a, T: Default + Eq, C: Clone + Zero + IncDec> IntoIterator for &'a CacheTree<T, C>
where
    for<'k, 'l> C: AddAssign<&'k C> + SubAssign<&'l C>,
{
    type Item = &'a Node<T, C>;
    type IntoIter = CacheTreeIter<'a, T, C>;
    fn into_iter(self) -> Self::IntoIter {
        self.iter()
    }
}

// Iterators for CacheTreeSubViewMut
pub struct CacheTreeSubIterator<'a, T: Default + Eq, C: Clone + Zero + IncDec>
where
    for<'k, 'l> C: AddAssign<&'k C> + SubAssign<&'l C>,
{
    helper: Option<CacheTreeIter<'a, T, C>>,
    target: &'a CacheTree<T, C>,
    root: TagName,
}

impl<'a, T: Default + Eq, C: Clone + Zero + IncDec> Iterator for CacheTreeSubIterator<'a, T, C>
where
    for<'k, 'l> C: AddAssign<&'k C> + SubAssign<&'l C>,
{
    type Item = &'a Node<T, C>;
    fn next(&mut self) -> Option<Self::Item> {
        match &mut self.helper {
            Some(ref mut helper) => helper.next(),
            None => {
                if let Some(node) = self.target.get(&self.root) {
                    self.helper = Some(CacheTreeIter {
                        target: self.target,
                        curr_parent: Some(node),
                        child: 0,
                        child_chain: Vec::new(),
                    });
                    Some(node)
                } else {
                    None
                }
            }
        }
    }
}

impl<'a, T: Default + Eq, C: Clone + Zero + IncDec> CacheTreeSubView<'a, T, C>
where
    for<'k, 'l> C: AddAssign<&'k C> + SubAssign<&'l C>,
{
    /// Returns an immutable iterator visiting only the root of this subview and
    /// its tree of children.
    ///
    /// The children are visited in the order specified in
    /// [Ordering of tags in a CacheTree](iter/struct.CacheTreeIter.html#ordering-of-tags-in-a-cachetree)
    pub fn iter(&self) -> CacheTreeSubIterator<T, C> {
        CacheTreeSubIterator {
            helper: None,
            target: &self.target,
            root: self.root.clone(),
        }
    }
}

impl<'a, 'b, T: Default + Eq, C: Clone + Zero + IncDec> IntoIterator
    for &'b CacheTreeSubView<'a, T, C>
where
    'b: 'a,
    for<'k, 'l> C: AddAssign<&'k C> + SubAssign<&'l C>,
{
    type Item = &'a Node<T, C>;
    type IntoIter = CacheTreeSubIterator<'a, T, C>;
    fn into_iter(self) -> CacheTreeSubIterator<'a, T, C> {
        self.iter()
    }
}

// Iterators for CacheTreeSubViewMut
pub struct CacheTreeSubIteratorMut<'a, T: Default + Eq, C: Clone + Zero + IncDec>
where
    for<'k, 'l> C: AddAssign<&'k C> + SubAssign<&'l C>,
{
    target: CacheTreeSubViewMut<'a, T, C>,
    curr_parent: Option<TagName>,
    curr_child: usize,
    child_chain: Vec<usize>,
}

impl<'a, T: Default + Eq, C: Clone + Zero + IncDec> CacheTreeSubIteratorMut<'a, T, C>
where
    for<'k, 'l> C: AddAssign<&'k C> + SubAssign<&'l C>,
{
    fn take_last_leaf<'n, N: TryInto<&'n TagStr>>(&mut self, name: N) -> Option<Node<T, C>> {
        let name = name.try_into().ok()?;
        let tree = &mut self.target.target;
        if let Some(mut curr_tag) = tree.get(name) {
            curr_tag = tree
                .get(&curr_tag.children[self.curr_child])
                .expect("Child of tag does not exist!");
            while let Some(last_child) = curr_tag.children.last() {
                self.child_chain.push(self.curr_child);
                // curr_tag.children.last() is Some(_), so len() is at least positive
                // -- len() - 1 is nonnegative
                self.curr_child = curr_tag.children.len() - 1;
                curr_tag = tree.get(last_child).expect("Child of node does not exist!");
            }
            let name = curr_tag.name.clone();
            Some(
                tree.cache
                    .remove(&name)
                    .expect("Child of node does not exist!"),
            )
        } else {
            None
        }
    }
}

impl<'a, T: Default + Eq, C: Clone + Zero + IncDec> Iterator for CacheTreeSubIteratorMut<'a, T, C>
where
    for<'k, 'l> C: AddAssign<&'k C> + SubAssign<&'l C>,
{
    type Item = Node<T, C>;
    fn next(&mut self) -> Option<Self::Item> {
        if let Some(curr_parent) = self.curr_parent.clone() {
            if self.curr_child == 0 {
                let next = self
                    .target
                    .target
                    .cache
                    .remove(&curr_parent)
                    .expect("Current parent does not exist!");
                if let Some(c) = self.child_chain.pop() {
                    self.curr_child = c;
                    self.curr_parent = Some(next.parent.clone());
                } else {
                    self.curr_parent = None;
                }
                return Some(next);
            } else {
                self.curr_child -= 1;
            }
            let next = self
                .take_last_leaf(&curr_parent)
                .expect("Parent of node does not exist!");
            if next.name == self.target.root {
                self.curr_parent = None;
            } else {
                self.curr_parent = Some(next.parent.clone());
            }
            Some(next)
        } else {
            None
        }
    }
}

// Implementation of IntoIterator trait on CacheTreeSubViewMut.
impl<'a, T: Default + Eq, C: Clone + Zero + IncDec> IntoIterator for CacheTreeSubViewMut<'a, T, C>
where
    for<'k, 'l> C: AddAssign<&'k C> + SubAssign<&'l C>,
{
    type Item = Node<T, C>;
    type IntoIter = CacheTreeSubIteratorMut<'a, T, C>;
    fn into_iter(self) -> Self::IntoIter {
        let curr_parent = Some(self.root.clone());
        CacheTreeSubIteratorMut {
            curr_child: self
                .target
                .cache
                .get(&self.root)
                .expect("Starting node does not exist")
                .children
                .len(),
            target: self,
            curr_parent,
            child_chain: Vec::new(),
        }
    }
}
