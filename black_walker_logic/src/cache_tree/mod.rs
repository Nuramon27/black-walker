//! Contains the [`CacheTree`](CacheTree) structure which represents a tree with nodes
//! accessible by their name.

pub mod iter;

use std::borrow::Borrow;
use std::cmp::Ordering;
use std::collections::HashMap;
use std::convert::TryInto;
use std::iter::FromIterator;

use std::fmt::{Debug, Write};
use std::ops::{AddAssign, SubAssign};

use num_traits::Zero;

use crate::mconvenience::latex::{AsLaTeX, Instruction};
use crate::{TagName, TagStr};

/// A type that can be [incremented](IncDec::inc) and [decremented](IncDec::dec)
/// in different [modes](IncDec::Mode).
///
/// The easiest case are primitive numeric types like `i32`. For them,
/// [`Mode`](IncDec::Mode) can be chosen as `()` and then the methods
/// do a simple increment and decrement. Indeed, there is an implementation
/// for them that does exactly this.
///
/// But one can also imagine a type that consists of two values, where
/// the first one or the second one is be incremented, depending on `mode`.
pub trait IncDec {
    /// The mode to be given to incrementation and dercementation.
    type Mode: Copy;
    /// Increments the type with mode `which`.
    fn inc(&mut self, which: Self::Mode);
    /// Decrements the type with mode `which`.
    fn dec(&mut self, which: Self::Mode);
}

impl<T: From<u8> + AddAssign<T> + SubAssign<T>> IncDec for T {
    type Mode = ();
    fn inc(&mut self, _other: ()) {
        *self += T::from(1u8);
    }
    fn dec(&mut self, _other: ()) {
        *self -= T::from(1u8);
    }
}

/// The `Node` struct represents a node in the [`CacheTree`].
///
/// The actual identity of a node in a tree is given by its `name`.
/// Every node has one `parent` and a `Vec` of `children`. These are
/// only represented as Strings -- nodes are identified by their names.
///
/// It furthermore can have a `count`, that may count something like
/// occurrences. If you does not need a count, set the type parameter
/// `C` to `()`.
///
/// The actual data of a `Node` lies in the `content`.
#[derive(Clone, Debug)]
pub struct Node<T: Default + Eq, C: Clone + Zero + IncDec>
where
    for<'k, 'l> C: AddAssign<&'k C> + SubAssign<&'l C>,
{
    name: TagName,
    parent: TagName,
    children: Vec<TagName>,
    count: C,
    /// The actual data the node holds.
    pub content: T,
}

impl<T: Default + Eq, C: Clone + Zero + IncDec> Default for Node<T, C>
where
    for<'k, 'l> C: AddAssign<&'k C> + SubAssign<&'l C>,
{
    fn default() -> Node<T, C> {
        Node {
            name: TagName::root(),
            parent: TagName::root(),
            children: Vec::new(),
            count: C::zero(),
            content: T::default(),
        }
    }
}

impl<T: Default + Eq, C: Clone + Zero + IncDec> Node<T, C>
where
    for<'k, 'l> C: AddAssign<&'k C> + SubAssign<&'l C>,
{
    /// Constructs a new `Node` with `name` and `content`.
    pub fn new(name: TagName, content: T) -> Node<T, C> {
        Node {
            name,
            parent: TagName::root(),
            children: Vec::new(),
            count: C::zero(),
            content,
        }
    }

    /// Returns the number of chicdren the `Node` has.
    pub fn children_count(&self) -> usize {
        self.children.len()
    }

    /// Returns the name of the `Node`.
    pub fn name(&self) -> &TagName {
        &self.name
    }

    /// Returns the parent of the `Node`.
    pub fn parent(&self) -> &TagName {
        &self.parent
    }

    pub fn content(&self) -> &T {
        &self.content
    }

    /// Returns the children of the `Node`.
    pub fn children(&self) -> &Vec<TagName> {
        &self.children
    }

    /// Returns the index-th child of the `Node` if existing.
    ///
    /// Otherwise, `None` is returned.
    pub fn child(&self, index: usize) -> Option<&TagName> {
        self.children.get(index)
    }

    /// Returns the count of the `Node`.
    pub fn count(&self) -> C {
        self.count.clone()
    }

    /// Returns whether `child` is in the `children` of this `Node`.
    pub fn has_child<'n, N: TryInto<&'n TagStr>>(&self, child: N) -> bool {
        if let Ok(child) = child.try_into() {
            for it in &self.children {
                if it == child {
                    return true;
                }
            }
        }
        false
    }

    /// If `child` exists in the `children` of the `Node`,
    /// returns its index in the `Vec` of children.
    ///
    /// Otherwise, `None` is returned.
    pub fn index_of_child<'n, N: TryInto<&'n TagStr>>(&self, child: N) -> Option<usize> {
        if let Ok(child) = child.try_into() {
            self.children.iter().position(|it| it == child)
        } else {
            None
        }
    }
}

impl<T: Default + Eq, C: Clone + Zero + IncDec> PartialEq for Node<T, C>
where
    for<'k, 'l> C: AddAssign<&'k C> + SubAssign<&'l C>,
{
    fn eq(&self, other: &Node<T, C>) -> bool {
        self.name == other.name
            && self.parent == other.parent
            && self.children == other.children
            && self.content == other.content
    }
}

impl<T: Default + Eq + std::hash::Hash, C: Clone + Zero + IncDec> std::hash::Hash for Node<T, C>
where
    for<'k, 'l> C: AddAssign<&'k C> + SubAssign<&'l C>,
{
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.name.hash(state);
        self.parent.hash(state);
        self.children.hash(state);
        self.content.hash(state);
    }
}

impl<T: Default + Eq, C: Clone + Zero + IncDec> Eq for Node<T, C> where
    for<'k, 'l> C: AddAssign<&'k C> + SubAssign<&'l C>
{
}

impl<T: Default + Eq, C: Clone + Zero + IncDec> PartialOrd for Node<T, C>
where
    for<'k, 'l> C: AddAssign<&'k C> + SubAssign<&'l C>,
{
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl<T: Default + Eq, C: Clone + Zero + IncDec> Ord for Node<T, C>
where
    for<'k, 'l> C: AddAssign<&'k C> + SubAssign<&'l C>,
{
    fn cmp(&self, other: &Self) -> Ordering {
        if self.children.is_empty() {
            if other.children.is_empty() {
                self.name.cmp(&other.name)
            } else {
                Ordering::Greater
            }
        } else if other.children.is_empty() {
            Ordering::Less
        } else {
            self.name.cmp(&other.name)
        }
    }
}

impl<T: Default + Eq + AsLaTeX + 'static, C: Clone + Zero + IncDec> AsLaTeX for Node<T, C>
where
    for<'k, 'l> C: AddAssign<&'k C> + SubAssign<&'l C>,
{
    fn to_latex(&self) -> Instruction {
        Instruction::Inst {
            name: r"\Node".to_string(),
            args: vec![
                self.parent.to_string(),
                self.name.to_string(),
                self.content.to_latex().to_string(),
            ],
        }
    }
}

/// The `CacheTree` is a tree of [nodes](Node), which can be accessed by their name
/// or through their parent. There is always a root node, which has the empty String
/// as name. It is not allowed to mutate the root node or insert another node with
/// an empty name.
///
/// # Integrity Guarantees
/// For each node, its [parent](Node::parent)
/// as well as all of its [children](Node::children) exist, too.
///
/// Each node is contained in the list of [children](Node::children) of its [parent](Node::parent).
///
/// Each node is set to be the [parent](Node::parent) of all of its [children](Node::children).
///
/// The name of each node is always identcal with its key in [`cache`](CacheTree::cache).
///
/// There is always exactly one node with the empty String as name, the 'root' node.
/// For the root node, [`parent`](Node::parent) is always set to the empty String,
/// although this is not to be understood as "The root node is its own parent.",
/// but rather as "The root node has no parent.".
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct CacheTree<T: Default + Eq, C: Clone + Zero + IncDec>
where
    for<'k, 'l> C: AddAssign<&'k C> + SubAssign<&'l C>,
{
    /// There place where all the nodes are stored. The parent-child-relationships between
    /// the nodes are only expressed by strings (not by pointers/ownership),
    /// so when obtaining the parent or a child of a node, one always has to make the way
    /// through the `cache`.
    cache: HashMap<TagName, Node<T, C>>,
}

impl<T: Default + Eq, C: Clone + Zero + IncDec> Default for CacheTree<T, C>
where
    for<'k, 'l> C: AddAssign<&'k C> + SubAssign<&'l C>,
{
    fn default() -> Self {
        CacheTree::new()
    }
}

impl<T: Default + Eq, C: Clone + Zero + IncDec> CacheTree<T, C>
where
    for<'k, 'l> C: AddAssign<&'k C> + SubAssign<&'l C>,
{
    /// Creates a new `CacheTree` with `category` and the root node as only node.
    pub fn new() -> Self {
        let mut cache = HashMap::new();
        // Insert a root node
        cache.insert(TagName::root(), Node::default());
        CacheTree::<T, C> { cache }
    }

    /// Returns the number of tags in the `CacheTree`
    pub fn len(&self) -> usize {
        self.cache.len()
    }

    /// Returns whether the tree is empty
    pub fn is_empty(&self) -> bool {
        self.cache.is_empty()
    }

    /// Clears the tree, removing all nodes.
    pub fn clear(&mut self) {
        self.cache.clear();
    }

    /// Returns a reference to the node with name `name` if there is any.
    ///
    /// *See also:*
    /// [`get_mut`](CacheTree::get_mut)
    pub fn get<'n, N: TryInto<&'n TagStr>>(&self, name: N) -> Option<&Node<T, C>> {
        let name = name.try_into().ok()?;
        self.cache.get(name)
    }

    /// Returns a mutable reference to the node with name `name` if there is any.
    ///
    /// # Panics
    ///
    /// If `name` is the root node.
    ///
    /// *See also:*
    /// [`get`](CacheTree::get)
    pub fn get_mut<'n, N: TryInto<&'n TagStr>>(&mut self, name: N) -> Option<&mut Node<T, C>> {
        let name = name.try_into().ok()?;
        assert!(!name.is_root(), "Cannot mutate root node!");
        self.cache.get_mut(name)
    }

    /// Returns whether a node with name `name` exists.
    pub fn node_exists<'n, N: TryInto<&'n TagStr>>(&self, name: N) -> bool {
        if let Ok(name) = name.try_into() {
            self.cache.contains_key(name)
        } else {
            false
        }
    }

    /// Returns the number of children of node `name`.
    pub fn children_of<'n, N: TryInto<&'n TagStr>>(&self, name: N) -> Option<Vec<&Node<T, C>>> {
        let name = name.try_into().ok()?;
        let node = self.cache.get(name)?;
        Some(Vec::from_iter(node.children.iter().map(|child| self.cache
                .get(child)
                // Won't panic because `child` is an existing node.
                .unwrap())))
    }

    /// Returns whether `node` is an indicret child of `parent`,
    /// i.e. whether somewhere in the chain of parents of `node`,
    /// `parent` occurs.
    ///
    /// ## Examples
    /// ```
    /// use std::str::FromStr;
    /// use std::convert::TryInto;
    ///
    /// use black_walker_logic::cache_tree::CacheTree;
    /// use black_walker_logic::TagName;
    ///
    /// let mut t = CacheTree::<(), i32>::new();
    /// t.insert_content_at_root(TagName::from_str("Books").unwrap(), ());
    /// t.insert_content(TagName::from_str("Cloud Atlas").unwrap(), (), "Books".try_into().unwrap());
    /// t.insert_content(TagName::from_str("Luisa Rey").unwrap(), (), "Cloud Atlas".try_into().unwrap());

    /// assert!(t.is_indirect_child_of("Luisa Rey", "Cloud Atlas"));
    /// assert!(t.is_indirect_child_of("Luisa Rey", "Books"));
    /// ```
    pub fn is_indirect_child_of<'a, 'b, N1: TryInto<&'a TagStr>, N2: TryInto<&'b TagStr>>(
        &self,
        node: N1,
        parent: N2,
    ) -> bool {
        if let Ok(parent) = parent.try_into() {
            if let Some(parent_chain) = self.parent_chain(node) {
                for it in parent_chain.skip(1) {
                    if it.name() == parent {
                        return true;
                    }
                }
            }
        }
        false
    }

    /// Returns a reference to the parent of node `name`.
    ///
    /// *See also:*
    /// [`parent_of_mut`](CacheTree::parent_of_mut)
    pub fn parent_of<'n, N: TryInto<&'n TagStr>>(&self, name: N) -> Option<&Node<T, C>> {
        let name = name.try_into().ok()?;
        let node = self.cache.get(name)?;
        Some(
            self.cache
                .get(&node.parent)
                .expect("Parent of node does not exist"),
        )
    }

    /// Returns a mutable reference to the parent of node `name`.
    /// *See also:*
    /// [`parent_of`](CacheTree::parent_of)
    pub fn parent_of_mut<'n, N: TryInto<&'n TagStr>>(
        &mut self,
        name: N,
    ) -> Option<&mut Node<T, C>> {
        let name = name.try_into().ok()?;
        let parentname = self.cache.get_mut(name)?.parent.clone();
        Some(self.cache.get_mut(&parentname).unwrap())
    }

    pub fn children_count_of<'n, N: TryInto<&'n TagStr>>(&self, name: N) -> Option<usize> {
        let name = name.try_into().ok()?;
        let node = self.cache.get(name)?;
        Some(node.children_count())
    }

    /// Compares the nodes with names `left` and `right` according to the `Ord`
    /// implementation of [`Node`].
    pub fn cmp_nodes<'n1, 'n2, N1: TryInto<&'n1 TagStr>, N2: TryInto<&'n2 TagStr>>(&self, left: N1, right: N2) -> Option<Ordering> {
        let (left, right) = (left.try_into().ok()?, right.try_into().ok()?);
        let (left, right) = (self.cache.get(left)?, self.cache.get(right)?);
        Some(left.cmp(right))
    }

    /// Increases the [`count`](Node::count), of
    /// the node with name `name`, depending on `mode`, as well as the corresponding
    /// count of all of its parents.
    ///
    /// If the node does not yet exist, a default-constructed tag is
    /// inserted with the corresponding count
    /// set to 1 and `false` is returned.
    ///
    /// If the node already exists, its count is increased and `true` is returned.
    pub fn occur(&mut self, name: &TagStr, mode: C::Mode) -> bool {
        if self.cache.contains_key(name) {
            // Increase the count of the node and all of its parents
            let mut it = self
                .parent_chain_mut(name)
                // Won't panic because name exists
                .unwrap();
            while let Some(p) = it.next() {
                p.count.inc(mode);
            }
            true
        } else {
            // Create a new node with correct count
            let mut node = Node::<T, C>::new(name.to_owned(), T::default());
            node.count.inc(mode);
            self.insert_at_root(node);
            false
        }
    }

    /// Decreases the [`count`](Node::count), of
    /// the node with name `name`, depending on `mode`, as well as the corresponding
    /// count of all of its parents. If one count already is zero, it
    /// is left zero.
    ///
    /// If the node does not exist, nothing is done.
    pub fn disoccur(&mut self, name: &TagStr, mode: C::Mode) {
        if self.cache.contains_key(name) {
            // Increase the count of the node and all of its parents
            let mut it = self
                .parent_chain_mut(name)
                // Won't panic because name exists
                .unwrap();
            while let Some(p) = it.next() {
                p.count.dec(mode);
            }
        }
    }

    /// Renames the node `oldname` to `name`.
    ///
    /// If there is no such node, the function does nothing and returns `false`,
    /// otherwise the node is renamed and moved to the correct place in its parent
    /// according to the sorting rule.
    ///
    /// If `name` already exists in the tree, the function does nothing
    /// and return `false`.
    pub fn rename<'n, N: TryInto<&'n TagStr>>(&mut self, oldname: N, name: TagName) -> bool {
        let oldname = if let Ok(oldname) = oldname.try_into() {
            oldname
        } else {
            return false;
        };
        assert!(!oldname.is_root(), "Cannot rename root node");
        if self.cache.contains_key(&name) {
            return false;
        }
        let mut node = if let Some(node) = self.cache.remove(oldname) {
            node
        } else {
            return false;
        };
        node.name = name.clone();
        // Change name in the children of parent
        {
            let parent = self
                .cache
                .get_mut(&node.parent)
                // Won't panic because the parent of a node is in cache
                .expect("Parent of node does not exist");
            let idx = parent
                .index_of_child(oldname)
                // Won't panic because every node is a child of its parent
                .expect("Node is not a child of its parent");
            parent.children[idx] = name.clone();
        }
        // Change name of parent of children
        for childname in &node.children {
            let child = self
                .cache
                .get_mut(childname)
                // Won't panic because every child of a node is in cache
                .expect("Child of node does not exists");
            child.parent = name.clone();
        }
        self.cache.insert(name, node);
        true
    }

    /// Inserts `node` into the tree as a child of the node `parentname`.
    ///
    /// The counts of all parents of the node are updated and if this node
    /// is the first child of its parent, the parent is moved
    /// to the correct place in the children of its parent
    ///
    /// If you want to only insert a node at the root of the tree,
    /// use [`insert_at_root`](CacheTree::insert_at_root).
    ///
    /// If the node already exists, or the parent does not exist,
    /// `node` is returned again. Otherwise, `None` is returned.
    ///
    /// *See also:*
    /// [`remove`](CacheTree::remove)
    pub fn insert(&mut self, mut node: Node<T, C>, parentname: &TagStr) -> Option<Node<T, C>> {
        // Do nothing if node already exists
        if self.cache.contains_key(&node.name) {
            return Some(node);
        }
        {
            // No child of `node` will be in the tree, so we reset
            // its children in order to be consistent.
            node.children.clear();
            let parent = self
                .cache
                .get_mut(parentname)
                // Won't panic because parent exists.
                .unwrap();
            parent.children.push(node.name.clone());
            // Adapt the counts of the parents
            let mut it = self
                .parent_chain_mut(parentname)
                // Won't panic because parent exists
                .unwrap();
            while let Some(p) = it.next() {
                p.count += &node.count;
            }
        }
        node.parent = parentname.to_owned();
        self.cache.insert(node.name.clone(), node);
        None
    }

    /// Inserts `newnode` at the root of the Tree.
    ///
    /// For further documentation, see [`insert`](CacheTree::insert).
    ///
    /// *See also:*
    /// [`remove`](CacheTree::remove)
    pub fn insert_at_root(&mut self, newnode: Node<T, C>) -> Option<Node<T, C>> {
        self.insert(newnode, TagName::root().borrow())
    }

    /// Inserts a [`Node`] with `name` and `content` into the tree,
    /// as a child of `parent.
    ///
    /// For further documentation, see [`insert`](CacheTree::insert).
    pub fn insert_content(
        &mut self,
        name: TagName,
        content: T,
        parent: &TagStr,
    ) -> Option<(TagName, T)> {
        if let Some(ret) = self.insert(Node::new(name, content), parent) {
            Some((ret.name, ret.content))
        } else {
            None
        }
    }

    /// Inserts a [`Node`] with `name` and `content` at the root of the tree.
    ///
    /// For further documentation, see [`insert`](CacheTree::insert).
    pub fn insert_content_at_root(&mut self, name: TagName, content: T) -> Option<(TagName, T)> {
        if let Some(ret) = self.insert_at_root(Node::new(name, content)) {
            Some((ret.name, ret.content))
        } else {
            None
        }
    }

    /// Removes node `name` from the tree.
    ///
    /// The children of `name` become children of the parent of `name`,
    /// i.e. they move one level downwards in the tree.
    ///
    /// Returns the removed node if it has been found. Only its count
    /// is adapted (to the count it would have without its children), but
    /// the rest of the removed node is unchanged. This means that you can still
    /// query the parent or children from the returned and you will
    /// obtain the parent and children the node had before being removed.
    ///
    /// *See also:*
    /// [`insert`](CacheTree::insert)
    /// [`insert_at_root`](CacheTree::insert_at_root)
    pub fn remove(&mut self, name: &TagStr) -> Option<Node<T, C>> {
        assert!(!name.is_root(), "Cannot remove root node");
        let mut node = self.cache.remove(name)?;
        let parent = self
            .cache
            .get_mut(&node.parent)
            // Won't panic because the parent of a node is in cache
            .expect("Parent of node to remove does not exist");
        let idx = parent
            .index_of_child(name)
            // Won't panic because every node is a child of its parent
            .expect("Node is not a child of its parent");
        parent.children.remove(idx);
        let parentname = parent.name.clone();
        let mut countdiff = node.count.clone();
        // Move the children to the parent of the deleted tag.
        for child in &node.children {
            {
                let childnode = self
                    .cache
                    .get_mut(child)
                    // Won't panic because the child of a node is in cache
                    .unwrap();
                childnode.parent = parentname.clone();
                countdiff -= &childnode.count;
            };
            let parent = self
                .cache
                .get_mut(&node.parent)
                // Won't panic because node.parent exists
                .unwrap();
            parent.children.push(child.clone());
        }
        // Adapt the counts of the parents
        let mut it = self
            .parent_chain_mut(&parentname)
            // Won't panic because parent exists
            .unwrap();
        while let Some(p) = it.next() {
            p.count -= &countdiff;
        }
        node.count = countdiff;
        Some(node)
    }

    pub fn reparent(&mut self, nodename: &TagStr, parent: &TagStr) -> bool {
        assert!(!nodename.is_root(), "Cannot reparent root node");
        // Check if parent exists
        if !self.cache.contains_key(parent) {
            return false;
        }
        // Change parent of node
        let oldparentname;
        let count;
        if let Some(node) = self.cache.get_mut(nodename) {
            oldparentname = node.parent.clone();
            node.parent = parent.to_owned();
            count = node.count.clone();
        } else {
            return false;
        };
        // Remove the node from the children of its old parent
        let oldparent = self
            .cache
            .get_mut(&oldparentname)
            // Won't panic because the parent of a node is in cache
            .expect("Old parent of node does not exist");
        let idx = oldparent
            .index_of_child(nodename)
            // Won't panic because every node is a child of its parent
            .expect("Node is not a child of its parent");
        let name = oldparent.children.remove(idx);
        let mut it = self
            .parent_chain_mut(&oldparentname)
            // oldparent exists
            .unwrap();
        while let Some(p) = it.next() {
            p.count -= &count;
        }
        // Add the node to the children of its new parent
        let newparent = self
            .cache
            .get_mut(parent)
            // Won't panic because parent exists
            .unwrap();
        newparent.children.push(name);
        let newparentname = newparent.name.clone();
        let mut it = self
            .parent_chain_mut(&newparentname)
            // oldparent exists
            .unwrap();
        while let Some(p) = it.next() {
            p.count += &count;
        }
        true
    }

    /// Returns a [subview](CacheTreeSubView) of this `CacheTree`
    /// beginning at node `from`.
    pub fn subview<'n, N: TryInto<&'n TagStr>>(&self, from: N) -> Option<CacheTreeSubView<T, C>> {
        let from = from.try_into().ok()?;
        let node = self.get(from)?;
        Some(CacheTreeSubView {
            root: node.name.clone(),
            target: self,
        })
    }

    /// Returns a mutable [subview](CacheTreeSubView) of this `CacheTree`
    /// beginning at node `from`.
    pub fn subview_mut<'n, N: TryInto<&'n TagStr>>(
        &mut self,
        from: N,
    ) -> Option<CacheTreeSubViewMut<T, C>> {
        let from = from.try_into().ok()?;
        let node = self.get(from)?;
        Some(CacheTreeSubViewMut {
            root: node.name.clone(),
            target: self,
        })
    }

    /// Sorts the nodes with names in `nodes` acording to their counts
    /// in descending order.
    ///
    /// That means nodes with high counts will be at the beginning.
    /// Nodes with equal counts are sorted alphabetically.
    pub fn sort_nodes_by_count<S: Borrow<TagStr>>(&self, nodes: &mut [S])
    where
        C: Ord,
    {
        nodes.sort_unstable_by(|lhs, rhs| {
            match self.get(lhs.borrow()) {
                Some(lhs) => self
                    .get(rhs.borrow())
                    .map(|rhs| lhs.count.cmp(&rhs.count))
                    .unwrap_or(Ordering::Greater),
                None => self
                    .get(rhs.borrow())
                    .map(|rhs| rhs.count.cmp(&C::zero()))
                    .unwrap_or(Ordering::Less),
            }
            .reverse()
            .then_with(|| lhs.borrow().cmp(rhs.borrow()))
        });
    }

    /// # Panics
    ///
    /// If `self` and `tree` contain nodes of the same name
    pub fn insert_subtree(
        &mut self,
        mut tree: CacheTree<T, C>,
        parentname: &TagStr,
    ) -> Option<CacheTree<T, C>> {
        if !self.cache.contains_key(parentname) {
            return Some(tree);
        }

        // By obtaining the children of the root node of `tree` we
        // find out which nodes must be set as children of node `parentname`.
        // The remaining nodes can simply be copied, as they already form a consistent
        // tree together with these 'head children'.
        let childrenlist = tree
            .cache
            .remove("")
            .expect("Other tree does not contain root node!")
            .children;
        for ch in childrenlist {
            let mut node = tree
                .cache
                .remove(&ch)
                // Won't panic because a child of a node must exist.
                .expect("Child of root node of other tree does not exist in other tree!");
            // We will insert `node` via `CacheTree::insert` into the tree, but this
            // will reset its children. So we first store those children
            // in a temporary, then insert the node and finally set the children again.
            let children = std::mem::take(&mut node.children);
            {
                let tmp_res = self.insert(node, parentname);
                debug_assert!(tmp_res.is_none());
            }
            self.cache
                .get_mut(&ch)
                // Won't panic because `ch` has just been inserted.
                .unwrap()
                .children = children;
        }
        for (k, v) in tree.cache {
            assert!(
                !self.cache.contains_key(&k),
                "Node with a name contained in tree already exists!"
            );
            self.cache.insert(k, v);
        }

        None
    }

    pub fn take_subtree<'n, N: TryInto<&'n TagStr>>(&mut self, name: N) -> Option<CacheTree<T, C>> {
        let name = name.try_into().ok()?;
        if let Some(subview) = self.subview_mut(name) {
            let mut res = CacheTree {
                cache: HashMap::new(),
            };
            for node in subview {
                res.cache.insert(node.name.clone(), node);
            }
            // The root of the subtree must be obtained in order to reset its
            // parent and cut its bounds to the old tree.
            let firstnode = res
                .cache
                .get_mut(name)
                // Won't panic because we just inserted a node with this name,
                // because the Iterator on CacheTreeSubViewMut visits the
                // root of the subview last.
                .unwrap();
            // Remove the tag from the children of its parent and adapt counts
            if let Some(mut p_iter) = self.parent_chain_mut(&firstnode.parent) {
                if let Some(parent) = p_iter.next() {
                    let idx = parent
                        .index_of_child(name)
                        .expect("Tag `name` is not in the children of its parent");
                    parent.children.remove(idx);
                    parent.count -= &firstnode.count;
                }
                while let Some(p) = p_iter.next() {
                    p.count -= &firstnode.count;
                }
            }
            firstnode.parent = TagName::root();
            // The subtree gets a root tag with correct children and count.
            let root = Node {
                name: TagName::root(),
                parent: TagName::root(),
                children: vec![name.to_owned()],
                count: firstnode.count.clone(),
                content: T::default(),
            };
            res.cache.insert(TagName::root(), root);
            Some(res)
        } else {
            None
        }
    }
}

impl<T: Default + Eq + AsLaTeX + 'static, C: Clone + Zero + IncDec + 'static> AsLaTeX
    for CacheTree<T, C>
where
    for<'k, 'l> C: AddAssign<&'k C> + SubAssign<&'l C>,
{
    fn to_latex(&self) -> Instruction {
        let mut content = String::new();
        for it in self.iter() {
            writeln!(content, "{}", it.to_latex())
                // Won't panic because writing to a string never fails
                .unwrap();
        }
        Instruction::Environment {
            name: "CacheTree".to_string(),
            args: Vec::new(),
            content,
        }
    }
}

/// A subview of a [`CacheTree`], beginning at a `root` parent.
///
/// It behaves like a `CacheTree` that contains only the
/// children of this `root` parent.
///
/// Especially useful is its `IntoIterator` implementation, that
/// allows to take a whole subtree of a `CacheTree` (removing it from this one)
/// and e.g. insert it into another tree.
pub struct CacheTreeSubView<'a, T: Default + Eq, C: Clone + Zero + IncDec>
where
    for<'k, 'l> C: AddAssign<&'k C> + SubAssign<&'l C>,
{
    target: &'a CacheTree<T, C>,
    root: TagName,
}

impl<'a, T: Default + Eq, C: Clone + Zero + IncDec> CacheTreeSubView<'a, T, C>
where
    for<'k, 'l> C: AddAssign<&'k C> + SubAssign<&'l C>,
{
    /// Returns whether a node with name `name` exists in the subtree.
    pub fn tag_exists<'n, N: TryInto<&'n TagStr>>(&self, name: N) -> bool {
        if let Ok(name) = name.try_into() {
            if let Some(iter) = self.target.parent_chain(name) {
                for it in iter {
                    if it.name() == &self.root {
                        return true;
                    }
                }
            }
        }
        false
    }

    pub fn get<'n, N: TryInto<&'n TagStr>>(&self, name: N) -> Option<&Node<T, C>> {
        let name = name.try_into().ok()?;
        if self.tag_exists(name) {
            self.target.get(name)
        } else {
            None
        }
    }

    pub fn target(&self) -> &CacheTree<T, C> {
        self.target
    }
}

/// A mutable subview of a [`CacheTree`], beginning at a `root` parent.
///
/// It behaves like a `CacheTree` that contains only the
/// children of this `root` parent.
///
/// Especially useful is its `IntoIterator` implementation, that
/// allows to take a whole subtree of a `CacheTree` (removing it from this one)
/// and e.g. insert it into another tree.
pub struct CacheTreeSubViewMut<'a, T: Default + Eq, C: Clone + Zero + IncDec>
where
    for<'k, 'l> C: AddAssign<&'k C> + SubAssign<&'l C>,
{
    target: &'a mut CacheTree<T, C>,
    root: TagName,
}

impl<'a, T: Default + Eq, C: Clone + Zero + IncDec> CacheTreeSubViewMut<'a, T, C>
where
    for<'k, 'l> C: AddAssign<&'k C> + SubAssign<&'l C>,
{
    /// Returns whether a node with name `name` exists in the subtree.
    pub fn tag_exists<'n, N: TryInto<&'n TagStr>>(&self, name: N) -> bool {
        if let Ok(name) = name.try_into() {
            if let Some(iter) = self.target.parent_chain(name) {
                for it in iter {
                    if it.name() == &self.root {
                        return true;
                    }
                }
            }
        }
        false
    }

    pub fn get<'n, N: TryInto<&'n TagStr>>(&self, name: N) -> Option<&Node<T, C>> {
        let name = name.try_into().ok()?;
        if self.tag_exists(name) {
            self.target.get(name)
        } else {
            None
        }
    }

    pub fn get_mut<'n, N: TryInto<&'n TagStr>>(&mut self, name: N) -> Option<&mut Node<T, C>> {
        let name = name.try_into().ok()?;
        if self.tag_exists(name) {
            self.target.get_mut(name)
        } else {
            None
        }
    }

    pub fn target(&self) -> &CacheTree<T, C> {
        self.target
    }
}

#[cfg(test)]
mod tests;
