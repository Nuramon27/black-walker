use std::convert::TryInto;
use std::iter::FromIterator;
use std::str::FromStr;

use crate::elements::tag::names::collation::init_collator;
use crate::{TagName, TagStr};

use super::{CacheTree, Node};

type SimpleTree = CacheTree<(), i32>;

#[test]
fn is_indirect_parent_complic() {
    init_collator();
    let mut t = SimpleTree::new();
    t.insert_content_at_root(TagName::from_str("Books").unwrap(), ());
    t.insert_content(
        TagName::from_str("Cloud Atlas").unwrap(),
        (),
        "Books".try_into().unwrap(),
    );
    t.insert_content(
        TagName::from_str("Luisa Rey").unwrap(),
        (),
        "Cloud Atlas".try_into().unwrap(),
    );
    t.insert_content_at_root(TagName::from_str("Star Wars").unwrap(), ());

    assert!(t.is_indirect_child_of("Luisa Rey", ""));
    assert!(!t.is_indirect_child_of("Luisa Rey", "Star Wars"));
    assert!(!t.is_indirect_child_of("Luisa Rey", "Luisa Rey"));
}

#[test]
fn cache_tree_default() {
    init_collator();
    let x = SimpleTree::default();
    assert!(x.node_exists(""));
    assert_eq!(x.len(), 1);
}

fn standard_tree() -> SimpleTree {
    let mut t = SimpleTree::new();
    t.insert_content_at_root(TagName::from_str("Movies").unwrap(), ());
    t.insert_content_at_root(TagName::from_str("Books").unwrap(), ());
    t.insert_content(
        TagName::from_str("Star Wars").unwrap(),
        (),
        "Movies".try_into().unwrap(),
    );
    t.insert_content(
        TagName::from_str("Yoda").unwrap(),
        (),
        "Star Wars".try_into().unwrap(),
    );
    t.insert_content(
        TagName::from_str("Luke Skywalker").unwrap(),
        (),
        "Star Wars".try_into().unwrap(),
    );
    t.insert_content(
        TagName::from_str("Darth Vader").unwrap(),
        (),
        "Star Wars".try_into().unwrap(),
    );
    t.insert_content(
        TagName::from_str("Adventures of Brigsby Bear").unwrap(),
        (),
        "Movies".try_into().unwrap(),
    );
    t.insert_content(
        TagName::from_str("Forest Gump (movie)").unwrap(),
        (),
        "Movies".try_into().unwrap(),
    );
    t.insert_content(
        TagName::from_str("Forest Gump").unwrap(),
        (),
        "Forest Gump (movie)".try_into().unwrap(),
    );
    t.insert_content(
        TagName::from_str("Jenny").unwrap(),
        (),
        "Forest Gump (movie)".try_into().unwrap(),
    );
    t.insert_content(
        TagName::from_str("Harry Potter (books)").unwrap(),
        (),
        "Books".try_into().unwrap(),
    );
    t.insert_content(
        TagName::from_str("Albus Dumbledore").unwrap(),
        (),
        "Harry Potter (books)".try_into().unwrap(),
    );
    t.insert_content(
        TagName::from_str("Lord Voldemort").unwrap(),
        (),
        "Harry Potter (books)".try_into().unwrap(),
    );

    t
}
#[test]
fn insert_and_remove() {
    init_collator();
    let mut t = SimpleTree::new();
    let mut films = Node::new(TagName::from_str("Filme").unwrap(), ());
    films.count += 3;
    assert!(t.insert(films, "".try_into().unwrap()).is_none());
    assert_eq!(
        &t.get("Filme").unwrap().name,
        "Filme",
        "Could not insert simple node"
    );
    let mut albus = Node::new(TagName::from_str("Albus Dumbledore").unwrap(), ());
    albus.count = 42;
    let mut tom = Node::new(TagName::from_str("Lord Voldemort").unwrap(), ());
    tom.count = 10;
    assert!(t.insert(albus, "Filme".try_into().unwrap()).is_none());
    assert!(t.insert(tom, "Filme".try_into().unwrap()).is_none());

    assert_eq!(
        &t.get("Albus Dumbledore").unwrap().name,
        "Albus Dumbledore",
        "Simple insertion"
    );
    assert!(t.get("Jehova").is_none(), "Non-existing node");
    assert_eq!(t.get("Filme").unwrap().count(), 55, "Wrong counts!");
    assert_eq!(t.get("").unwrap().count(), 55, "Wrong counts!");
    let u = t
        .remove("Filme".try_into().unwrap())
        .expect("Could not remove node 'Filme' ");
    assert_eq!(&u.name, "Filme");
    assert_eq!(u.count, 3);
    assert_eq!(
        &t.parent_of("Lord Voldemort").unwrap().name,
        "",
        "Node with removed parent was not reparented"
    );
    assert_eq!(t.get("").unwrap().count(), 52, "Wrong counts!");
}

#[test]
fn reparent_and_rename() {
    init_collator();
    let mut t = SimpleTree::new();
    assert!(t
        .insert_at_root(Node::new(TagName::from_str("Filme").unwrap(), ()))
        .is_none());
    assert_eq!(
        &t.get("Filme").unwrap().name,
        "Filme",
        "Could not insert simple node"
    );
    let mut albus = Node::new(TagName::from_str("Albus Dumbledore").unwrap(), ());
    albus.count = 27;
    assert!(t.insert(albus, "Filme".try_into().unwrap()).is_none());
    let mut tom = Node::new(TagName::from_str("Lord Voldemort").unwrap(), ());
    tom.count = 4;
    assert!(t.insert(tom, "Filme".try_into().unwrap()).is_none());

    assert!(t
        .insert(
            Node::new(TagName::from_str("Old HP-Films").unwrap(), ()),
            "Filme".try_into().unwrap()
        )
        .is_none());
    assert_eq!(&t.parent_of("Albus Dumbledore").unwrap().name, "Filme");
    assert!(t.reparent(
        "Albus Dumbledore".try_into().unwrap(),
        "Old HP-Films".try_into().unwrap()
    ));
    assert_eq!(
        &t.parent_of("Albus Dumbledore").unwrap().name,
        "Old HP-Films"
    );
    assert_eq!(t.get("Old HP-Films").unwrap().count, 27);
    assert!(t.reparent(
        "Lord Voldemort".try_into().unwrap(),
        "Old HP-Films".try_into().unwrap()
    ));
    assert_eq!(&t.parent_of("Lord Voldemort").unwrap().name, "Old HP-Films");
    assert_eq!(t.get("Old HP-Films").unwrap().count, 31);
    assert!(t.rename(
        "Albus Dumbledore",
        TagName::from_str("Albus Percival Wulfric Brian Dumbledore").unwrap()
    ));
    assert_eq!(
        t.children_of("Old HP-Films").unwrap()[0].name(),
        "Albus Percival Wulfric Brian Dumbledore"
    );
    assert!(t.rename(
        "Albus Percival Wulfric Brian Dumbledore",
        TagName::from_str("XAlbus").unwrap()
    ));
    assert!(t.children_of("Old HP-Films").unwrap().iter().any(|it| it.name() == "XAlbus"));
    assert!(t.rename(
        "XAlbus",
        TagName::from_str("Albus Percival Wulfric Brian Dumbledore").unwrap()
    ));
    assert_eq!(t.get("Old HP-Films").unwrap().count, 31);
    assert!(t
        .insert(
            Node::new(TagName::from_str("New HP-Films").unwrap(), ()),
            "Filme".try_into().unwrap()
        )
        .is_none());
    assert!(t.rename(
        "New HP-Films",
        TagName::from_str("Fantastic Beasts").unwrap()
    ));
    let mut newt = Node::new(TagName::from_str("Newt Scamander").unwrap(), ());
    newt.count += 2;
    assert!(t
        .insert(newt, "Fantastic Beasts".try_into().unwrap())
        .is_none());
    assert!(t.rename(
        "Old HP-Films",
        TagName::from_str("Harry Potter and …").unwrap()
    ));

    assert_eq!(&t.parent_of("Fantastic Beasts").unwrap().name, "Filme");
    assert_eq!(t.get("Filme").unwrap().count, 33);
    assert_eq!(
        &t.parent_of("Albus Percival Wulfric Brian Dumbledore")
            .unwrap()
            .name,
        "Harry Potter and …"
    );
    assert_eq!(
        &t.parent_of("Newt Scamander").unwrap().name,
        "Fantastic Beasts"
    );
    let mut children = Vec::new();
    for it in t.children_of("Fantastic Beasts").unwrap() {
        children.push(&it.name);
    }
    assert_eq!(
        children,
        vec![&TagName::from_str("Newt Scamander").unwrap()]
    );
    assert_eq!(t.children_count_of("Harry Potter and …").unwrap(), 2);
}

#[test]
fn rename_to_existing_name() {
    init_collator();
    let mut t = SimpleTree::new();
    assert!(t
        .insert_at_root(Node::new(TagName::from_str("Filme").unwrap(), ()))
        .is_none());
    assert_eq!(
        &t.get("Filme").unwrap().name,
        "Filme",
        "Could not insert simple node"
    );
    let mut albus = Node::new(TagName::from_str("Albus Dumbledore").unwrap(), ());
    albus.count = 27;
    assert!(t.insert(albus, "Filme".try_into().unwrap()).is_none());
    let mut tom = Node::new(TagName::from_str("Lord Voldemort").unwrap(), ());
    tom.count = 4;
    assert!(t.insert(tom, "Filme".try_into().unwrap()).is_none());
    assert!(!t.rename(
        "Albus Dumbledore",
        TagName::from_str("Lord Voldemort").unwrap()
    ));
}

#[test]
fn parent_chain_mut() {
    init_collator();
    let mut t = SimpleTree::new();
    t.insert_at_root(Node::new(
        TagName::from_str("Fiktive Personen").unwrap(),
        (),
    ));
    t.insert(
        Node::new(TagName::from_str("Books").unwrap(), ()),
        "Fiktive Personen".try_into().unwrap(),
    );
    t.insert(
        Node::new(TagName::from_str("Harry Potter (books)").unwrap(), ()),
        "Books".try_into().unwrap(),
    );
    t.insert(
        Node::new(TagName::from_str("Albus Dumbledore").unwrap(), ()),
        "Harry Potter (books)".try_into().unwrap(),
    );

    let mut it = t
        .parent_chain_mut("Albus Dumbledore")
        .expect("Node Albus Dumbledore was not created");
    assert_eq!(it.next().unwrap().name(), "Albus Dumbledore");
    assert_eq!(it.next().unwrap().name(), "Harry Potter (books)");
    assert_eq!(it.next().unwrap().name(), "Books");
    assert_eq!(it.next().unwrap().name(), "Fiktive Personen");
    assert_eq!(it.next().unwrap().name(), "");
    assert_eq!(it.next(), None);
}

#[test]
fn occur_and_disoccur() {
    init_collator();
    let mut t = standard_tree();

    for _i in 0..2 {
        assert!(t.occur("Albus Dumbledore".try_into().unwrap(), ()));
    }
    for _i in 0..6 {
        assert!(t.occur("Yoda".try_into().unwrap(), ()));
    }
    t.occur("Movies".try_into().unwrap(), ());
    for _i in 0..12 {
        assert!(t.occur("Jenny".try_into().unwrap(), ()));
    }
    assert!(t.occur("Forest Gump".try_into().unwrap(), ()));
    assert_eq!(t.get("Albus Dumbledore").unwrap().count(), 2);
    assert_eq!(t.get("Yoda").unwrap().count(), 6);
    assert_eq!(t.get("Star Wars").unwrap().count(), 6);
    assert_eq!(t.get("Forest Gump (movie)").unwrap().count(), 13);
    assert_eq!(t.get("Movies").unwrap().count(), 20);
    for _i in 0..2 {
        t.disoccur("Yoda".try_into().unwrap(), ());
    }
    assert_eq!(t.get("Yoda").unwrap().count(), 4);
    assert_eq!(t.get("Movies").unwrap().count(), 18);
    t.disoccur("Forest Gump (movie)".try_into().unwrap(), ());
    assert_eq!(t.get("Forest Gump (movie)").unwrap().count(), 12);
    assert_eq!(t.get("Movies").unwrap().count(), 17);
    assert_eq!(t.get("").unwrap().count(), 19);

    assert!(!t.occur("Unknown tag".try_into().unwrap(), ()));
}

#[test]
fn take_subtree() {
    init_collator();
    let mut t = standard_tree();

    let mut r = SimpleTree::new();
    r.insert_content_at_root(TagName::from_str("Jedi").unwrap(), ());
    r.occur("Jedi".try_into().unwrap(), ());
    for _i in 0..10 {
        t.occur("Yoda".try_into().unwrap(), ());
    }
    for _i in 0..5 {
        t.occur("Lord Voldemort".try_into().unwrap(), ());
    }
    r.insert_subtree(
        t.take_subtree("Movies").unwrap(),
        "Jedi".try_into().unwrap(),
    );

    assert_eq!(
        t.get("").unwrap().children(),
        &vec![TagName::from_str("Books").unwrap()]
    );
    assert_eq!(
        r.get("Jedi").unwrap().children(),
        &vec![TagName::from_str("Movies").unwrap()]
    );
    assert_eq!(
        r.get("Movies").unwrap().children(),
        &vec![
            TagName::from_str("Star Wars").unwrap(),
            TagName::from_str("Adventures of Brigsby Bear").unwrap(),
            TagName::from_str("Forest Gump (movie)").unwrap(),
        ]
    );
    assert_eq!(r.get("Star Wars").unwrap().parent(), "Movies");
    assert_eq!(r.parent_of("Yoda").unwrap().name(), "Star Wars");
    assert!(!t.node_exists("Luke Skywalker"));

    assert_eq!(t.get("").unwrap().count, 5);
    assert_eq!(r.get("Yoda").unwrap().count, 10);
    assert_eq!(r.get("Movies").unwrap().count, 10);
    assert_eq!(r.get("Jedi").unwrap().count, 11);
    assert_eq!(r.get("").unwrap().count, 11);
}

#[test]
fn subview() {
    init_collator();
    let t = standard_tree();
    let s = t.subview("Star Wars").unwrap();

    assert!(s.tag_exists("Yoda"));
    assert!(s.get("Luke Skywalker").is_some());
    assert!(s.get("").is_none());
    assert!(s.get("Lord Voldemort").is_none());
}

#[test]
fn subiterator_trivial() {
    init_collator();
    let t = standard_tree();
    let s = t.subview("").unwrap();
    for (i, (it, jt)) in s.iter().zip(t.iter()).enumerate() {
        assert_eq!(it.name(), jt.name(), "Wrong iteration item in run {}", i);
    }
}

#[test]
fn subiterator() {
    init_collator();
    let t = standard_tree();
    let s = t.subview("Star Wars").unwrap();

    let mut it = s.iter();
    assert_eq!(it.next().unwrap().name(), "Star Wars");
    assert_eq!(it.next().unwrap().name(), "Yoda");
    assert_eq!(it.next().unwrap().name(), "Luke Skywalker");
    assert_eq!(it.next().unwrap().name(), "Darth Vader");
    assert!(it.next().is_none());
}

#[test]
fn sort_by_count() {
    init_collator();
    let mut t = standard_tree();
    for _ in 0..8 {
        t.occur("Albus Dumbledore".try_into().unwrap(), ());
    }
    for _ in 0..4 {
        t.occur("Darth Vader".try_into().unwrap(), ());
    }
    for _ in 0..1 {
        t.occur("Yoda".try_into().unwrap(), ());
    }

    let mut nodes: Vec<&TagStr> = Vec::from_iter(
        ["Yoda", "Darth Vader", "Luke Skywalker", "Albus Dumbledore"]
            .iter()
            .map(|&a| a.try_into().unwrap()),
    );
    t.sort_nodes_by_count(&mut nodes);
    assert_eq!(
        nodes,
        ["Albus Dumbledore", "Darth Vader", "Yoda", "Luke Skywalker"]
    );
}
