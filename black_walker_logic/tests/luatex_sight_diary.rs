#![cfg(feature = "stat")]

use std::fs::OpenOptions;
use std::io::{BufWriter, Write};
use std::process::Command;

use black_walker_logic::elements::tag::names::collation::init_collator;
use black_walker_logic::operate::tests::standard_diary;
use black_walker_logic::stat::pretty_print::{pretty_print, Settings};

// Ignored because it is a sight test.
#[test]
#[ignore]
fn luatex_sight_diary() {
    init_collator();
    let tex_dir = std::env::temp_dir();
    std::fs::create_dir_all(&tex_dir).unwrap();
    let tex_path = tex_dir.join("test_pretty_print.tex");
    {
        let tmpfile = OpenOptions::new()
            .write(true)
            .create(true)
            .truncate(true)
            .open(&tex_path)
            .expect("Temporary file could not be opened");
        let mut tmpbuffer = BufWriter::new(tmpfile);

        let arg = Settings { fontsize: 11 };

        let mut diary = standard_diary();
        pretty_print(&arg, &mut tmpbuffer, &mut diary).expect("Pretty print failed with error.");
        tmpbuffer.flush().unwrap();
    }
    assert!(Command::new("lualatex")
        .current_dir(std::env::temp_dir())
        .args(&["--interaction=nonstopmode"])
        .args(tex_path.file_name())
        .status()
        .expect("LuaLaTeX could not be invoked")
        .success());
    assert!(Command::new("okular")
        .current_dir(std::env::temp_dir())
        .args(&["test_pretty_print.pdf"])
        .status()
        .expect("Okular could not be invoked")
        .success());
    {
        //remove_file(&tex_path).expect("Could not delete temporary file");
    }
}
