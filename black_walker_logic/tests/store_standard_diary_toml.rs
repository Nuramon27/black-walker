use std::convert::TryFrom;
use std::ops::Deref;

use black_walker_logic::operate::tests::standard_diary;
use black_walker_logic::settings::DiaryLocation;
use black_walker_logic::store;
use black_walker_logic::sync::diary::DiaryPart::{DL, ENTR, SET, TAG};
use black_walker_logic::{Diary, DreamList, EntrySystem, TagForest, GS};

use black_walker_logic::operate::core::Applyable;
use black_walker_logic::{lock, stage_diary};

#[test]
// Ignored because it modifies the disk.
#[ignore]
fn store_write_standard_diary_toml() {
    black_walker_logic::elements::tag::names::collation::init_collator();

    let mut diary = standard_diary();

    {
        let mut set = diary.gl.write().unwrap();
        let diary_dir = std::env::temp_dir().join("black_walker_play/diary");
        std::fs::create_dir_all(&diary_dir).unwrap();
        set.diary_location = DiaryLocation::try_from(diary_dir.join("diary.toml")).ok();
    }
    store::write(&diary, None).unwrap();

    let mut diary_loaded = Diary::new();
    store::read(None).unwrap().apply(&mut diary_loaded).unwrap();

    {
        let mut stage = stage_diary!(diary_loaded; DL, TAG, ENTR, SET;);
        if let (Ok(dreamlist), Ok(tagforest), Ok(entrysystem), Ok(settings)) =
            lock!(stage; DreamList, TagForest, EntrySystem, GS;)
        {
            let mut stage = stage_diary!(diary; DL, TAG, ENTR, SET;);
            if let (Ok(dreamlist_std), Ok(tagforest_std), Ok(entrysystem_std), Ok(settings_std)) =
                lock!(stage; DreamList, TagForest, EntrySystem, GS;)
            {
                assert_eq!(dreamlist.deref(), dreamlist_std.deref());
                assert_eq!(tagforest.deref(), tagforest_std.deref());
                assert_eq!(entrysystem.deref(), entrysystem_std.deref());
                assert_eq!(settings.deref(), settings_std.deref());
            };
        };
    }
}
