black_walker_logic
===

This library crate is not meant to be used on its own, but belongs to the _Black Walker_ dream diary software. It is a rewrite of the applications logic part in the programming language [Rust](rust-lang.org).

In the future this repository may be merged into the main repository of the Black Walker project.


## License

The crate is licensed under the GNU General Public License, Version 3 ([LICENSE](LICENSE) or https://www.gnu.org/licenses/ ). The licensing may change in the future, but all parts of _Black Walker_ will always be licensed under an at least GPL-3-compatible license.
