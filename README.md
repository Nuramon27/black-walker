Black Walker
===

Black Walker is a dream diary application which aims to provide sophisticated means for lucid dreamers to investigate their dreams.

It provides a searchable dream database and specially focuses on _Tags_, small additional information to a dream like who appeared in it and where the plot took place.

## Dependencies

Black Walker depends on Qt 5 for its graphical user interface.
It furthermore depends on several libraries of the rust ecosystem. Exact specifications of such dependencies can be found in the several Cargo.toml files.

## Building Black Walker

### Prerequisites
- A working developer distribution of Qt 5, to be obtained under [https://www.qt.io/developers]
- A working rust installation, to be obtained under [https://www.rust-lang.org/tools/install]
- A C++ compiler
- A make tool (_make_ or _nmake_)

### The build process
- Clone the repository and enter its directory:
```
    git clone --depth 1 https://gitlab.com/Nuramon27/black-walker.git
    cd black-walker
```
- Set the environment variable `QMAKE_PATH` to the qmake-executable of your distribution of Qt 5. This will look something like
```
    QMAKE_PATH=Path_to_your_qmake_executable/qmake
```
- Build the application with
```
    cargo build
```
or
```
    cargo build --release
```
if you want an optimized build. The application can the be found in "target/debug" or "target/release" as the executable "black_walker_bin_qt". This executable will be dynamically linked to the Qt libraries (but statically to all rust libraries), so in order to execute it, the dynamic libraries "Qt5Core", "Qt5Widgets", "Qt5GUI" must either be known by the system or distributed in a folder together with the executable.

## License

Black Walker is licensed under the GNU General Public License version 3 or later (see [LICENSE](LICENSE) or https://www.gnu.org/licenses/ ).

## Contributing

The project is currently developed by me alone and I do not expect contributors, but if your fingertips are burning to implement a feature you cannot miss, I will not forbid you to send a pull request.
