# Version 2.4.0

## Features

* Das Traumtagebuch kann jetzt im json-Format exportiert werden

## Fixes

* Linux: Das Fenster von Black Walker trägt jetzt auch das Programmicon

## Internes

* Im Falle eines Absturzes wird eine backtrace in die Datei log.txt im selben Verzeichnis wie die executable geschrieben, um schwerwiegende Fehler besser nachvollziehbar zu machen
* Die ICU-Bibliothek wurde auf Version 72 angehoben

# Version 2.3.4

## Internes

* Die ICU-Bibliothek wurde auf Version 69 angehoben

# Version 2.3.3

## Features

* In der Tagverwaltung ist es jetzt auch möglich, einen Tag durch „Rechtsklick“ -> „In Gruppe verschieben“ einer neune Gruppe zuzuordnen. Das Verschieben per Drag&Drop wird teilweise schwierig, wenn Tag und Gruppe weit auseinander liegen.

## Fixes

* Diverse Bugs in der Tagverwaltung, die teilweise zu Programmabstürzen geführt haben, wurden behoben.

# Version 2.3.2

## Fixes

* In version 2.3.1 konnte das Traumtagebuch durch einen internen Fehler nicht gespeichert werden. Dieser Fehler wurde behoben.

# Version 2.3.1

# Features

* Man kann jetzt auch einen Ort zu einer Anmerkung angeben.

## Fixes

* In der Suchfunktion wird der Status der Häkchen „Kind-tags mitsuchen“ und „Erwähnte tags mitsuchen“ gespeichert, sowie die Suchresultate der vorigen Suche beim Schließen gelöscht.

* Beim Drucken des Traumtagebuchs werden die Zeichen ‚°‘ und ‚∞‘ richtig dargestellt.

# Version 2.3.0

## Features

* Es gibt wieder eine Suchfunktion.

* In der Suchfunktion ist es nun möglich, Tags ausschließlich in erwähnter oder ausschließlich in voller Form zu suchen.

* Die Suchfunktion ignoriert jetzt Groß- und Kleinschreibung sowie Akzente an Buchstaben und den Unterschied zwischen Umlauten und der zugehörigen einfachen Vokale. Dies geschieht vorerst nur bei der Suche im Traumtitel und im Traumtext.

* Beim Drucken des Traumtagebuchs werden die Zeichen ‚&‘, ‚\‘, ‚$‘, ‚_‘, ‚#‘, ‚%‘, ‚{‘, ‚}‘, ‚^‘, ‚~‘ und ‚€‘ nun durch entsprechende LaTeX-Befehle ersetzt. Anführungszeichen werden Kontextabhängig ersetzt, d. h. am Anfang eines Zitates durch Anführungszeichen unten, am Ende ducrh Anführungszeichen oben.

* Zu einem Traum können jetzt mehrere Anmerkungen von verschiedenen Tagen gemacht werden.

* Black Walker hat jetzt ein Icon.

## Fixes

* Das Wort "Einschlafleichtigkeit" ist jetzt richtig geschrieben.

* Unter sehr unwahrscheinlichen Bedingungen konnte es bei der Suche nach Träumen mit einem Tag zum Programmabsturz kommen, wenn zu viele Suchen zu schnell hintereinander ausgeführt werden. Das wurde behoben.

* Ein mögliches Performanceproblem beim Speichern des Tagebuchs wurde behoben.

# Version 2.2.1

## Fixes

* Die Werte des Sliders "Persönliche Relevanz" wurden mit einem falsch kodierten 'ö' gespeichert. Die Kodierung wurde bereits in Version 2.2.0 korrigiert. Beim Übergang von einer früheren Version zur Version 2.2.1 schließlich werden die Werte, die unter dem falsch kodierten Umlaut gespeichert waren, übernommen.

# Version 2.2.0

## Features

* Entferne Slider  "Fidelity Verhalten", "Fidelity Umgebung", "Symbolismus" und "Fähigkeiten" und verschiebe alle Slider zurück in den "Grundlegendes"-Tab der Dreamform

* Die maximalen und minimalen Werte für Slider können nun durch Editieren der Traumtagebuchdatei verändert werden. Der Standard bleibt 0–5.

* Für einen Traum können nun die Eigenschaften "Land", "Stadt", "Drinnen", "Draußen", "Tag", und "Nacht" durch Checkboxen angegeben werden.

## Fixes

* Korrigiere die alphabetische Sortierung von Text, sodass nun kleingeschriebene Wörter und Umlaute nicht mehr ganz ans Ende gestellt werden.

* Ein bug, durch den manchmal die Traumliste und die Tagliste nicht auf eine Änderung eines Traums/Tags nicht reagieren würden, wurde behoben.

## Interne Änderungen

* Black Walker hängt jetzt von der ICU-Bibliothek ab, die für die lexikalisch korrekte Sortierung von Strings zuständig ist.

# Version 2.1.0

## Features

* Füge Slider  "Fidelity Verhalten", "Fidelity Umgebung", "Symbolismus" und "Fähigkeiten" hinzu und verschiebe alle Slider in einen eigenen Tab in der Dreamform.

# Version 2.0.3

## Fixes

* Benenne Standardkategorie "Essen & Trinken" in "Essen & Getränke" um (wie es auch im alten Black Walker hieß).

## Features

* Füge Slider "Angst" und "Persönliche Relevanz" hinzu

# Version 2.0.2

## Fixes

* Wenn einem Traum ein noch nicht vorhandener Tag in erwähnter Form (mit '*') hinzugefügt wurde, und der Traum anschließend mit "Speichern & Schließen" gespeichert wurde, wurde der Tag in direkter Form gespeichert. Dies ist behoben.
* Das Löschen einer Kategorie funktioniert jetzt dauerhaft, indem die Kategorie auch aus allen Träumen entfernt wird (und nicht als leere Kategorie verbleibt)

# Version 2.0.1

## Fixes

* Zeilenumbrüche in Tagbeschreibungen gehen nicht mehr verloren.
