#-------------------------------------------------
#
# Project created by QtCreator 2017-10-03T20:35:41
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = black_walker_gui_cpp
TEMPLATE = lib
CONFIG += staticlib

CONFIG += c++14
#unix {
#    CONFIG += precompile_header
#    PRECOMPILED_HEADER = gui_qt/zstable.hpp
#}


CONFIG(debug, debug|release): DEFINES += BLACKWALKER_DEBUG
#DEFINES += QT_NO_VERSION_TAGGING


#INCLUDEPATH += $$PWD/bdg
INCLUDEPATH += $$PWD/gui_qt

RESOURCES += gui_qt/resource/resource.qrc

RC_ICONS = gui_qt/resource/icon/appicon128x128.ico

HEADERS += \
    bdg/Bindings.h \
    gui_qt/DDialogs/daboutdialog.hpp \
    gui_qt/DDialogs/ddreaminputdialog.hpp \
    gui_qt/DDialogs/ddreamswithtagdialog.hpp \
    gui_qt/DDialogs/dnightdialog.hpp \
    gui_qt/DDialogs/dunknowntagsdialog.hpp \
    gui_qt/DModel/dnightlistview.hpp \
    gui_qt/DModel/dtagtreeproxymodel.hpp \
    gui_qt/Enum/dallowed.hpp \
    gui_qt/Enum/dentertype.hpp \
    gui_qt/Enum/dentitytype.hpp \
    gui_qt/Enum/savestate.hpp \
    gui_qt/ae.hpp \
    gui_qt/aglobalsettings.hpp \
    gui_qt/asetup.hpp \
    gui_qt/awindow.hpp \
    gui_qt/ddreamform.hpp \
    gui_qt/mspinboxslider.hpp \
    gui_qt/zdef.hpp \
    gui_qt/zstable.hpp \
    gui_qt/DModel/ddreamlistview.hpp \
    gui_qt/DModel/dtagtreeview.hpp \
    gui_qt/scopedthread.hpp \
    gui_qt/DModel/tagtreemodel.hpp \
    gui_qt/DDialogs/dtagadministration.hpp \
    gui_qt/DDialogs/dsearchdialog.hpp \
    gui_qt/dsearchform.hpp \
    gui_qt/dsearchformleaf.hpp \
    gui_qt/dabstractsearchform.hpp \
    gui_qt/dsuggestionedit.hpp \
    gui_qt/DDialogs/dcreatetagsdialog.hpp \
    gui_qt/DDialogs/dchangetagcategorydialog.hpp \
    gui_qt/generalemitter.hpp \
    gui_qt/DTagEdit/dabstracttagedit.hpp \
    gui_qt/DTagEdit/dtagedit.hpp \
    gui_qt/DTagEdit/dstatictagedit.hpp \
    gui_qt/mdateeditopt.hpp

SOURCES += \
    bdg/Bindings.cpp \
    gui_qt/DDialogs/daboutdialog.cpp \
    gui_qt/DDialogs/ddreaminputdialog.cpp \
    gui_qt/DDialogs/ddreamswithtagdialog.cpp \
    gui_qt/DDialogs/dnightdialog.cpp \
    gui_qt/DDialogs/dunknowntagsdialog.cpp \
    gui_qt/DModel/dnightlistview.cpp \
    gui_qt/DModel/dreamlistmodel_additional.cpp \
    gui_qt/DModel/dreamlistmodel_dragdrop.cpp \
    gui_qt/DModel/dtagtreeproxymodel.cpp \
    gui_qt/DModel/nightlistmodel_additional.cpp \
    gui_qt/DModel/tagtreemodel_dragdrop.cpp \
    gui_qt/asetup.cpp \
    gui_qt/awindow.cpp \
    gui_qt/ddreamform.cpp \
    gui_qt/lib.cpp \
    gui_qt/ae.cpp \
    gui_qt/aglobalsettings.cpp \
    gui_qt/DModel/ddreamlistview.cpp \
    gui_qt/DModel/dtagtreeview.cpp \
    gui_qt/mspinboxslider.cpp \
    gui_qt/scopedthread.cpp \
    gui_qt/DModel/tagtreemodel.cpp \
    gui_qt/DDialogs/dtagadministration.cpp \
    gui_qt/DDialogs/dsearchdialog.cpp \
    gui_qt/dsearchform.cpp \
    gui_qt/dsearchformleaf.cpp \
    gui_qt/dabstractsearchform.cpp \
    gui_qt/dsuggestionedit.cpp \
    gui_qt/DDialogs/dcreatetagsdialog.cpp \
    gui_qt/DDialogs/dchangetagcategorydialog.cpp \
    gui_qt/generalemitter.cpp \
    gui_qt/DTagEdit/dabstracttagedit.cpp \
    gui_qt/DTagEdit/dtagedit.cpp \
    gui_qt/DTagEdit/dstatictagedit.cpp \
    gui_qt/mdateeditopt.cpp

rust_testing {
    HEADERS += gui_qt/tests/dreamlist.hpp \
        gui_qt/tests/sight_awindow.hpp \
        gui_qt/tests/sight_dreamform.hpp \
        gui_qt/tests/sight_dreamlist.hpp \
        gui_qt/tests/testutil.hpp \
        gui_qt/tests/sight_dsuggestionedit.hpp \
        gui_qt/tests/sight_tagtree.hpp
    SOURCES += gui_qt/tests/dreamlist.cpp \
        gui_qt/tests/sight_awindow.cpp \
        gui_qt/tests/sight_dreamform.cpp \
        gui_qt/tests/sight_dreamlist.cpp \
        gui_qt/tests/testutil.cpp \
        gui_qt/tests/sight_dsuggestionedit.cpp \
        gui_qt/tests/sight_tagtree.cpp \
}


# Suppress warnings for gcc
*-g++: {
    QMAKE_CXXFLAGS += -Wno-sign-compare \
        -Wno-sign-conversion \
        -Wno-deprecated-copy
}
