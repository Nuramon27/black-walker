// When compiling in debug mode or testing, the appication should
// launch together with a console, which is necessary printing to stdout and stderr on windows.
//
// Only in a release build the application shall open without a console window.
#![cfg_attr(not(any(debug_assertions, test)), windows_subsystem = "windows")]
#![deny(clippy::dbg_macro)]

use std::env;
use std::ffi;

use libc::{c_char, c_int};

extern crate black_walker_model_qt;

extern "C" {
    fn lib(argc: c_int, argv: *mut *mut c_char) -> c_int;
}

fn main() {
    let argv: Vec<ffi::CString> = env::args_os()
        .map(|arg| ffi::CString::new(Vec::<u8>::from(arg.into_string().unwrap())).unwrap())
        .collect();
    let args: Vec<*const c_char> = argv.into_iter().map(|arg| arg.as_ptr()).collect();

    unsafe {
        lib(args.len() as c_int, args.as_ptr() as *mut *mut c_char);
    }
}
