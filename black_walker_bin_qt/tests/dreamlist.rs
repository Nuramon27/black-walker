/****************************************************************************
** The BlackWalker application offers a graphical interface for writing a dream diary.
** Copyright 2017-2018  Manuel Simon
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <https://www.gnu.org/licenses/>.
**
****************************************************************************/
//mod setup;

use std::env;
use std::ffi;

use libc::{c_char, c_int};

extern crate black_walker_model_qt;

extern "C" {
    #[allow(improper_ctypes)]
    fn test_standard_diary(argc: c_int, argv: *mut *mut c_char, err: *mut String) -> i32;
}

#[test]
fn dreamlist() {
    let argv: Vec<ffi::CString> = env::args_os()
        .into_iter()
        .map(|arg| ffi::CString::new(Vec::<u8>::from(arg.into_string().unwrap())).unwrap())
        .collect();
    let args: Vec<*const c_char> = argv.into_iter().map(|arg| arg.as_ptr()).collect();

    let mut err = String::new();
    unsafe {
        match test_standard_diary(
            args.len() as c_int,
            args.as_ptr() as *mut *mut c_char,
            &mut err as *mut String,
        ) {
            0 => (),
            _ => panic!(err),
        };
    }
}
