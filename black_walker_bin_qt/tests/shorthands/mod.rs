#[macro_export]
macro_rules! call_test {
    ( $rust_test:ident, $cpp_test:ident ) => {
        extern "C" {
            #[allow(improper_ctypes)]
            fn $cpp_test(argc: libc::c_int, argv: *mut *mut libc::c_char, err: *mut String) -> i32;
        }

        // Ignored because it is a sight-test.
        #[test]
        #[ignore]
        fn $rust_test() {
            let argv: Vec<std::ffi::CString> = std::env::args_os()
                .into_iter()
                .map(|arg| {
                    std::ffi::CString::new(Vec::<u8>::from(arg.into_string().unwrap())).unwrap()
                })
                .collect();
            let args: Vec<*const libc::c_char> = argv.into_iter().map(|arg| arg.as_ptr()).collect();

            let mut err = String::new();
            unsafe {
                match $cpp_test(
                    args.len() as libc::c_int,
                    args.as_ptr() as *mut *mut libc::c_char,
                    &mut err as *mut String,
                ) {
                    0 => (),
                    _ => panic!(err),
                };
            }
        }
    };
}
