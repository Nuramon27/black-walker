use std::env;
use std::ffi;

use libc::{c_char, c_int};

extern crate black_walker_model_qt;

extern "C" {
    #[allow(improper_ctypes)]
    fn test_sight_tagtree(argc: c_int, argv: *mut *mut c_char, err: *mut String) -> i32;
}

// Ignored because it is a sight-test.
#[test]
#[ignore]
fn sight_tagtree() {
    let argv: Vec<ffi::CString> = env::args_os()
        .into_iter()
        .map(|arg| ffi::CString::new(Vec::<u8>::from(arg.into_string().unwrap())).unwrap())
        .collect();
    let args: Vec<*const c_char> = argv.into_iter().map(|arg| arg.as_ptr()).collect();

    let mut err = String::new();
    unsafe {
        match test_sight_tagtree(
            args.len() as c_int,
            args.as_ptr() as *mut *mut c_char,
            &mut err as *mut String,
        ) {
            0 => (),
            _ => panic!(err),
        };
    }
}
